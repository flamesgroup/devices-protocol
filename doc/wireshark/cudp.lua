cudp_proto = Proto("cudp", "CUDP Protocol")

local errcode_descriptions = {
  [0x00] = "none",
  [0x80] = "internal error",
  [0x01] = "incompatible version"
}

local f = cudp_proto.fields
f.version = ProtoField.uint8("cudp.version", "version")
f.type = ProtoField.uint8("cudp.type", "type", base.HEX)
f.flags = ProtoField.uint8("cudp.flags", "flags", base.HEX)
f.flags_bt = ProtoField.uint8("cudp.flags_bt", "BT", nil, nil, 0x80)
f.flags_ack = ProtoField.uint8("cudp.flags_ack", "ACK", nil, nil, 0x40)
f.flags_err = ProtoField.uint8("cudp.flags_err", "ERR", nil, nil, 0x20)
f.errcode = ProtoField.uint8("cudp.errcode", "errcode", base.HEX, errcode_descriptions)
f.suid = ProtoField.uint32("cudp.suid", "suid", base.HEX)
f.duid = ProtoField.uint32("cudp.duid", "duid", base.HEX)
f.typedata = ProtoField.bytes("cudp.typedata", "typedata")

f.typedata_timestamp = ProtoField.uint64("cudp.typedata_timestamp", "timestamp", base.HEX)

f.typedata_ip = ProtoField.ipv4("cudp.typedata_ip", "ip")
f.typedata_mask = ProtoField.ipv4("cudp.typedata_mask", "mask")
f.typedata_gateway = ProtoField.ipv4("cudp.typedata_gateway", "gateway")
f.typedata_master_ip = ProtoField.ipv4("cudp.typedata_master_ip", "master ip")
f.typedata_uuid = ProtoField.bytes("cudp.typedata_uuid", "uuid")
f.typedata_current_time = ProtoField.absolute_time("cudp.typedata_current_time", "current time")

f.typedata_delay = ProtoField.uint32("cudp.typedata_delay", "delay")

f.typedata_iv = ProtoField.bytes("cudp.typedata_iv", "iv")
f.typedata_ciphered_data = ProtoField.bytes("cudp.typedata_ciphered_data", "ciphered data")

f.typedata_expire_time = ProtoField.absolute_time("cudp.typedata_expire_time", "expire time")

function timems2nstime(buffer_chunk)
  local msecs = buffer_chunk:le_uint64()
  local secs  = (msecs / 1000):tonumber()
  local nsecs = (msecs % 1000):tonumber() * 1000000
  return NSTime.new(secs, nsecs)
end

function cudp_proto.dissector(buffer, pinfo, tree)
  pinfo.cols.protocol = "CUDP"

  local version_buffer = buffer(0, 1)
  local type_buffer = buffer(1, 1)
  local flags_buffer = buffer(2, 1)
  local errcode_buffer = buffer(3, 1)

  local suid_buffer = buffer(4, 4)
  local duid_buffer = buffer(8, 4)

  local type_uint = type_buffer:uint()
  local flags_ack = flags_buffer:bitfield(1)
  local suid = suid_buffer:le_uint()
  local duid = duid_buffer:le_uint()

  local pakcet_info = "<UNKNOWN>"

  if type_uint == 0x01 then
    pakcet_info = "PING"
  elseif type_uint == 0x02 then
    pakcet_info = "SET IP CONFIG"
  elseif type_uint == 0x03 then
    pakcet_info = "GET IP CONFIG"
  elseif type_uint == 0x04 then
    pakcet_info = "RESET"
  elseif type_uint == 0x06 then
    pakcet_info = "UPDATE EXPIRE TIME"
  elseif type_uint == 0x07 then
    pakcet_info = "GET EXPIRE TIME"
  end
  if flags_ack == 0 then
  else
    pakcet_info = pakcet_info .. " ACK"
  end

  pakcet_info = pakcet_info .. " - suid: " .. string.format("0x%08X", suid) .. " duid: " .. string.format("0x%08X", duid)

  pinfo.cols.info = pakcet_info

  local subtree = tree:add(cudp_proto, buffer(), "CUDP Protocol, " .. pakcet_info)

  subtree:add(f.version, version_buffer)
  subtree:add(f.type, type_buffer)
  subtree_flags = subtree:add(f.flags, flags_buffer)
    subtree_flags:add(f.flags_bt, flags_buffer)
    subtree_flags:add(f.flags_ack, flags_buffer)
    subtree_flags:add(f.flags_err, flags_buffer)
  subtree:add(f.errcode, errcode_buffer)

  subtree:add_le(f.suid, suid_buffer)
  subtree:add_le(f.duid, duid_buffer)

  if type_uint == 0x01 then
    subtree_typedata = subtree:add(f.typedata, buffer(12))
      subtree_typedata:add_le(f.typedata_timestamp, buffer(12, 8))
  elseif type_uint == 0x02 then
    if flags_ack == 0 then
      subtree_typedata = subtree:add(f.typedata, buffer(12))
        subtree_typedata:add_le(f.typedata_ip, buffer(12, 4))
        subtree_typedata:add_le(f.typedata_mask, buffer(16, 4))
        subtree_typedata:add_le(f.typedata_gateway, buffer(20, 4))
        subtree_typedata:add_le(f.typedata_master_ip, buffer(24, 4))
        subtree_typedata:add_le(f.typedata_uuid, buffer(28, 16))
        subtree_typedata:add_le(f.typedata_current_time, buffer(44, 8), timems2nstime(buffer(44, 8)))
    end
  elseif type_uint == 0x03 then
    if flags_ack > 0 then
      subtree_typedata = subtree:add(f.typedata, buffer(12))
        subtree_typedata:add_le(f.typedata_ip, buffer(12, 4))
        subtree_typedata:add_le(f.typedata_mask, buffer(16, 4))
        subtree_typedata:add_le(f.typedata_gateway, buffer(20, 4))
        subtree_typedata:add_le(f.typedata_master_ip, buffer(24, 4))
        subtree_typedata:add_le(f.typedata_uuid, buffer(28, 16))
    end
  elseif type_uint == 0x04 then
    if flags_ack == 0 then
      subtree_typedata = subtree:add(f.typedata, buffer(12))
        subtree_typedata:add_le(f.typedata_delay, buffer(12, 4))
    end
  elseif type_uint == 0x06 then
    if flags_ack == 0 then
      subtree_typedata = subtree:add(f.typedata, buffer(12))
        subtree_typedata:add_le(f.typedata_iv, buffer(12, 16))
        subtree_typedata:add_le(f.typedata_ciphered_data, buffer(28, 32))
    end
  elseif type_uint == 0x07 then
    if flags_ack > 0 then
      subtree_typedata = subtree:add(f.typedata, buffer(12))
        subtree_typedata:add_le(f.typedata_expire_time, buffer(12, 8), timems2nstime(buffer(12, 8)))
    end
  end
end

udp_table = DissectorTable.get("udp.port")

udp_table:add(9010, cudp_proto)
