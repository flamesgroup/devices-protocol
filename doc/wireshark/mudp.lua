mudp_proto = Proto("mudp", "MUDP Protocol")

local errcode_descriptions = {
  [0x00] = "none",
  [0x80] = "internal error",
  [0x01] = "incompatible version",
  [0x02] = "incorrect data",
  [0x10] = "channel not open",
  [0x11] = "channel unenslaved",
  [0x12] = "channel bussy",
}

local f = mudp_proto.fields
f.version = ProtoField.uint8("mudp.version", "version")
f.channel = ProtoField.uint8("mudp.channel", "channel", base.HEX)
f.flags = ProtoField.uint8("mudp.flags", "flags", base.HEX)
f.flags_ms = ProtoField.uint8("mudp.flags_ms", "MS", nil, nil, 0x80)
f.flags_ee = ProtoField.uint8("mudp.flags_ee", "EE", nil, nil, 0x40)
f.flags_ack = ProtoField.uint8("mudp.flags_ack", "ACK", nil, nil, 0x20)
f.flags_err = ProtoField.uint8("mudp.flags_err", "ERR", nil, nil, 0x10)
f.flags_re = ProtoField.uint8("mudp.flags_re", "RE", nil, nil, 0x02)
f.flags_seq = ProtoField.uint8("mudp.flags_seq", "seq", nil, nil, 0x01)
f.errcode = ProtoField.uint8("mudp.errcode", "errcode", base.HEX, errcode_descriptions)
f.type = ProtoField.uint8("mudp.type", "type", base.HEX)
f.length = ProtoField.uint16("mudp.length", "length")
f.data = ProtoField.bytes("mudp.data", "data")
f.audio_timestamp = ProtoField.uint32("mudp.audio_timestamp", "audio_timestamp")
f.audio_data = ProtoField.bytes("mudp.audio_data", "audio_data")

f.data_ee_attempt_count = ProtoField.uint8("mudp.data_ee_attempt_count", "attempt count")
f.data_ee_response_timeout = ProtoField.uint16("mudp.data_ee_response_timeout", "response timeout")
f.data_ee_retransmit_timeout = ProtoField.uint16("mudp.data_ee_retransmit_timeout", "retransmit count")

function mudp_proto.dissector(buffer, pinfo, tree)
  pinfo.cols.protocol = "MUDP"

  local service_version_buffer = buffer(0, 1)
  local service_channel_buffer = buffer(1, 1)
  local service_flags_buffer = buffer(2, 1)
  local service_errcode_buffer = buffer(3, 1)

  local channel = service_channel_buffer:le_uint()

  local flags_ms = service_flags_buffer:bitfield(0)
  local flags_ee = service_flags_buffer:bitfield(1)
  local flags_ack = service_flags_buffer:bitfield(2)
  local flags_err = service_flags_buffer:bitfield(3)
  local flags_re = service_flags_buffer:bitfield(6)
  local flags_seq = service_flags_buffer:bitfield(7)

  local pakcet_info = ""

  if flags_ee ~= 0 then
    if pakcet_info ~= "" then pakcet_info = pakcet_info .. " + " end
    pakcet_info = pakcet_info .. "EE"
  end

  if flags_re ~= 0 then
    if pakcet_info ~= "" then pakcet_info = pakcet_info .. " + " end
    pakcet_info = pakcet_info .. "RE"
  end

  if flags_ack ~= 0 then
    if pakcet_info ~= "" then pakcet_info = pakcet_info .. " + " end
    pakcet_info = pakcet_info .. "ACK"
  end

  if flags_err ~= 0 then
    if pakcet_info ~= "" then pakcet_info = pakcet_info .. " + " end
    pakcet_info = pakcet_info .. "ERR"
  end

  if (flags_ee ~= 0 or flags_re ~= 0) then
    pakcet_info = pakcet_info .. " + S:" .. flags_seq
  else
    pakcet_info = pakcet_info .. "notRE"
  end

  pakcet_info = "channel: " .. string.format("0x%02X", channel) .. " - " .. pakcet_info

  local subtree = tree:add(mudp_proto, buffer(), "MUDP Protocol, " .. pakcet_info)

  subtree_service = subtree:add(buffer(0, 4), "service")
    subtree_service:add(f.version, service_version_buffer)
    subtree_service:add(f.channel, service_channel_buffer)
    subtree_service_flags = subtree_service:add(f.flags, service_flags_buffer)
      subtree_service_flags:add(f.flags_ms, service_flags_buffer)
      subtree_service_flags:add(f.flags_ee, service_flags_buffer)
      subtree_service_flags:add(f.flags_ack, service_flags_buffer)
      subtree_service_flags:add(f.flags_err, service_flags_buffer)
      subtree_service_flags:add(f.flags_re, service_flags_buffer)
      subtree_service_flags:add(f.flags_seq, service_flags_buffer)
    subtree_service:add(f.errcode, service_errcode_buffer)

  if flags_ack == 0 then
    local data_type_buffer = buffer(4, 1)
    local data_length_buffer = buffer(6, 2)

    local data_type = data_type_buffer:uint()
    local data_length = data_length_buffer:le_uint()

    subtree_data = subtree:add(buffer(4, 4), nil)
      subtree_data:add(f.type, data_type_buffer)
      subtree_data:add_le(f.length, data_length_buffer)

    if flags_ee == 0 then
      if data_length > 0 then
        if (channel == 0x04 or channel == 0x05 or channel == 0x13 or channel == 0x93) and (data_type == 0x00) then
          pakcet_info = pakcet_info .. " / audio"

          subtree_data:set_text("data / audio")
          subtree_data:add_le(f.audio_timestamp, buffer(8, 4))
          subtree_data:add(f.audio_data, buffer(12))
        else
          subtree_data:set_text("data")
          subtree_data:add(f.data, buffer(8))
        end
      else
        subtree_data:set_text("data")
      end
    else
      local data_ee_attempt_count_buffer = buffer(8, 1)
      local data_ee_response_timeout_buffer = buffer(9, 2)
      local data_ee_retransmit_timeout_buffer = buffer(11, 2)

      subtree_data:set_text("data EE")

      subtree_data:add_le(f.data_ee_attempt_count, data_ee_attempt_count_buffer)
      subtree_data:add_le(f.data_ee_response_timeout, data_ee_response_timeout_buffer)
      subtree_data:add_le(f.data_ee_retransmit_timeout, data_ee_retransmit_timeout_buffer)
    end
  end

  pinfo.cols.info = pakcet_info
end

udp_table = DissectorTable.get("udp.port")

udp_table:add(9020, mudp_proto)
