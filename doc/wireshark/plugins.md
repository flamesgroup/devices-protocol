# Wireshark plugins

To have the possibility to analyze and troubleshoot data exchange between **devices-protocol** library and hardware was introduced two [Wireshark Lua](https://wiki.wireshark.org/Lua) plugins:
* [CUDP](doc/wireshark/cudp.lua)
* [MUDP](doc/wireshark/mudp.lua)

Wireshark tries to load Lua files from all `plugins` directories (specified in the [Wireshark manual](http://www.wireshark.org/docs/wsug_html_chunked/ChAppFilesConfigurationSection.html)). The file extensions must be ".lua". Directories under plugins are searched recursively for Lua scripts.

The most common places to put Lua plugins are `$HOME/.config/wireshark/plugins` or `$HOME/.wireshark/plugins` for Unix/Linux and `C:\Documents and Settings\<username>\Application Data\Wireshark\plugins` for Windows.
