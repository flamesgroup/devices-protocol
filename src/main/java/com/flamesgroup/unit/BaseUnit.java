/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BaseUnit {

  protected enum State {
    TURNED_OFF, TURNING_ON, TURNED_ON, TURNING_OFF
  }

  private State state = State.TURNED_OFF;

  protected final Lock unitLock = new ReentrantLock(true);

  protected void setStateWithinUnitLock(final State newState) {
    unitLock.lock();
    try {
      state = newState;
    } finally {
      unitLock.unlock();
    }
  }

  protected void checkAndSetStateAndExceptionOnFailWithinUnitLock(final State expectedState, final State newState) {
    unitLock.lock();
    try {
      if (state == expectedState) {
        state = newState;
      } else {
        throw new IllegalStateException("[" + this + "] - can't change state to [" + newState + "] from state [" + state + "], expected state [" + expectedState + "]");
      }
    } finally {
      unitLock.unlock();
    }
  }

  protected void checkStateAndExceptionOnFail(final State expectedState, final String exceptionMessage) {
    if (state != expectedState) {
      throw new IllegalStateException("[" + this + "] - " + exceptionMessage + " in state [" + state + "]");
    }
  }

}
