/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sc;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.helper.OneOf;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUCommandException;
import com.flamesgroup.device.sc.APDUException;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.APDUTimeoutException;
import com.flamesgroup.device.sc.BinaryOffset;
import com.flamesgroup.device.sc.DataResponse;
import com.flamesgroup.device.sc.FileID;
import com.flamesgroup.device.sc.FileType;
import com.flamesgroup.device.sc.RecordOperation;
import com.flamesgroup.device.sc.RecordOperation.RecordOperationMode;
import com.flamesgroup.device.sc.SelectEFResponse;
import com.flamesgroup.device.sc.SelectMFDFResponse;
import com.flamesgroup.device.simb.ISCReaderChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderErrorStateException;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.unit.BaseUnit;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.CHVStatus;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.ISIMCardUnit;
import com.flamesgroup.unit.IllegalCHVStateError;
import com.flamesgroup.unit.InvalidCHVError;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.UnblockCHV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.EnumSet;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SIMCardUnit extends BaseUnit implements ISIMCardUnit {

  private static final Logger logger = LoggerFactory.getLogger(SIMCardUnit.class);

  private enum APDUResponseState {
    IDLE, WAITING, RECEIVED
  }

  private enum CHVType {

    CHV1((byte) 1, (byte) 0),
    CHV2((byte) 2, (byte) 2);

    private final byte chvNumber;
    private final byte unblockCHVNumber;

    CHVType(final byte chvNumber, final byte unblockCHVNumber) {
      this.chvNumber = chvNumber;
      this.unblockCHVNumber = unblockCHVNumber;
    }

    private byte getCHVNumber() {
      return chvNumber;
    }

    private byte getUnblockCHVNumber() {
      return unblockCHVNumber;
    }
  }

  private static final int RESPONSE_TIMEOUT = 5000;

  private final ISCReaderChannel scReaderChannel;
  private final ISCReaderSubChannelHandler scReaderSubChannelHandler = new SCReaderSubChannelHandler();

  private final Lock responseLock = new ReentrantLock();
  private final Condition responseCondition = responseLock.newCondition();

  private final ByteBuffer byteBuffer = ByteBuffer.allocate(260).order(ByteOrder.BIG_ENDIAN);

  private final EnumSet<APDUResponse.Status> correctlyExecutedResponses = EnumSet.noneOf(APDUResponse.Status.class);

  private APDUResponseState responseState = APDUResponseState.IDLE;
  private APDUResponse response;
  private SCReaderErrorStateException responseException;

  private final String toS;

  public SIMCardUnit(final ISCReaderChannel scReaderChannel) {
    this.scReaderChannel = scReaderChannel;

    this.correctlyExecutedResponses.add(APDUResponse.Status.NORMAL_ENDING);
    this.correctlyExecutedResponses.add(APDUResponse.Status.NORMAL_ENDING_WITH_ADDITIONAL_DATA);
    this.correctlyExecutedResponses.add(APDUResponse.Status.NORMAL_ENDING_WITH_ADDITIONAL_SIM_DATA);
    this.correctlyExecutedResponses.add(APDUResponse.Status.NORMAL_ENDING_WITH_SIM_COMMAND);

    this.toS = String.format("%s@%x:%s", getClass().getSimpleName(), hashCode(), scReaderChannel);
  }

  @Override
  public void turnOn() throws ChannelException, InterruptedException {
    checkAndSetStateAndExceptionOnFailWithinUnitLock(BaseUnit.State.TURNED_OFF, BaseUnit.State.TURNING_ON);
    try {
      scReaderChannel.enslave();
      try {
        scReaderChannel.getSCReaderSubChannel().start(scReaderSubChannelHandler);
      } catch (ChannelException e) {
        scReaderChannel.free();
        throw e;
      }
    } catch (ChannelException | InterruptedException e) {
      logger.error("[{}] - can't turn on SIM card unit", this, e);
      setStateWithinUnitLock(BaseUnit.State.TURNED_OFF);
      throw e;
    }
    setStateWithinUnitLock(BaseUnit.State.TURNED_ON);
  }

  @Override
  public void turnOff() throws ChannelException, InterruptedException {
    checkAndSetStateAndExceptionOnFailWithinUnitLock(BaseUnit.State.TURNED_ON, BaseUnit.State.TURNING_OFF);
    try {
      Exception le = null;
      try {
        scReaderChannel.getSCReaderSubChannel().stop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      try {
        scReaderChannel.free();
      } catch (ChannelException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      responseLock.lock();
      try {
        responseState = APDUResponseState.IDLE;
        response = null;
        responseException = null;
      } finally {
        responseLock.unlock();
      }
      if (le != null) {
        if (le instanceof ChannelException) {
          throw (ChannelException) le;
        } else {
          throw (InterruptedException) le;
        }
      }
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't correctly turn off SIM card unit", this, e);
      throw e;
    } finally {
      setStateWithinUnitLock(BaseUnit.State.TURNED_OFF);
    }
  }

  @Override
  public ICCID readICCID() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read ICCID");

      select(FileID.MF);
      SelectEFResponse selectResponse = select(FileID.EF_ICCID).second();
      DataResponse dataResponse = readBinary(new BinaryOffset(0), selectResponse.getFileSize());
      return new ICCID(bcdToString(dataResponse.getBinaryData()));
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public IMSI readIMSI() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read IMSI");

      select(FileID.MF);
      select(FileID.DF_GSM);
      SelectEFResponse selectResponse = select(FileID.EF_IMSI).second();
      DataResponse dataResponse = readBinary(new BinaryOffset(0), selectResponse.getFileSize());
      byte[] bcdIMSI = new byte[8];
      System.arraycopy(dataResponse.getBinaryData(), 1, bcdIMSI, 0, dataResponse.getBinaryData().length - 1);
      return new IMSI(bcdToString(bcdIMSI).substring(1));
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public String readOperatorName() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read Operator Name");

      select(FileID.MF);
      select(FileID.DF_GSM);
      SelectEFResponse response = select(FileID.EF_SPN).second();
      DataResponse dataResponse = readBinary(new BinaryOffset(0), response.getFileSize());
      byte[] operatorArr = trimToFF(dataResponse.getBinaryData(), 1, dataResponse.getBinaryData().length);
      return new String(operatorArr);
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public PhoneNumber readPhoneNumber() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read Phone Number");

      select(FileID.MF);
      select(FileID.DF_TELECOM);
      int recordLength = select(FileID.EF_MSISDN).second().getRecordLength();
      DataResponse dataResponse = readRecord(new RecordOperation(RecordOperationMode.ABSOLUTE_MODE, 1), recordLength);
      byte[] bcdNumber = trimToFF(dataResponse.getBinaryData(), recordLength - 12, recordLength - 2);
      String stringNumber = bcdToString(bcdNumber);
      try {
        return new PhoneNumber(stringNumber);
      } catch (IllegalArgumentException e) {
        logger.trace("[{}] - wrong Phone Number on SIM card", this, e);
        return null;
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public boolean isCHV1Enabled() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't check CHV1 enabled");

      SelectMFDFResponse response = select(FileID.MF).first();
      return response.getFileCharacteristics().isCHV1Enabled();
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public CHVStatus readCHV1status() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read CHV1 status");

      SelectMFDFResponse response = select(FileID.MF).first();
      return response.getChv1Status();
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public CHVStatus readUnblockCHV1status() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read unblock CHV1 status");

      SelectMFDFResponse response = select(FileID.MF).first();
      return response.getUnblockChv1Status();
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public void disableCHV1(final CHV chv1) throws ChannelException, APDUException, InterruptedException {
    Objects.requireNonNull(chv1, "CHV1 must not be null");
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't disable CHV1");

      try {
        byteBuffer.clear();
        CHV.encoding(chv1, byteBuffer);
        byteBuffer.flip();
        byte[] data = new byte[byteBuffer.remaining()];
        byteBuffer.get(data);
        executeAPDU(new APDUCommand(APDUCommand.Instruction.DISABLE_CHV, (byte) 0x00, (byte) 0x01, (byte) 0x08, data));
      } catch (APDUCommandException e) {
        switch (e.getAPDUResponse().getStatus()) {
          case CONTRADICTION_WITH_CHV_STATUS:
            throw new IllegalCHVStateError("Can't disable CHV1, it is already disabled or blocked");
          case ACCESS_CONDITION_NOT_FULFILLED:
            throw new InvalidCHVError("Invalid CHV1 value: " + chv1);
          default:
            throw e;
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public void enableCHV1(final CHV chv1) throws ChannelException, APDUException, InterruptedException {
    Objects.requireNonNull(chv1, "CHV1 must not be null");
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't enable CHV1");

      try {
        byteBuffer.clear();
        CHV.encoding(chv1, byteBuffer);
        byteBuffer.flip();
        byte[] data = new byte[byteBuffer.remaining()];
        byteBuffer.get(data);
        executeAPDU(new APDUCommand(APDUCommand.Instruction.ENABLE_CHV, (byte) 0x00, (byte) 0x01, (byte) 0x08, data));
      } catch (APDUCommandException e) {
        switch (e.getAPDUResponse().getStatus()) {
          case CONTRADICTION_WITH_CHV_STATUS:
            throw new IllegalCHVStateError("Can't enable CHV1, it is already enabled or blocked");
          case ACCESS_CONDITION_NOT_FULFILLED:
            throw new InvalidCHVError("Invalid CHV1 value: " + chv1);
          default:
            throw e;
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public void verifyCHV1(final CHV chv1) throws ChannelException, APDUException, InterruptedException {
    verifyCHV(CHVType.CHV1, chv1);
  }

  @Override
  public void changeCHV1(final CHV oldCHV1, final CHV newCHV1) throws ChannelException, APDUException, InterruptedException {
    changeCHV(CHVType.CHV1, oldCHV1, newCHV1);
  }

  @Override
  public void unblockCHV1(final UnblockCHV unblockCHV1, final CHV newCHV1) throws ChannelException, APDUException, InterruptedException {
    unblockCHV(CHVType.CHV1, unblockCHV1, newCHV1);
  }

  @Override
  public CHVStatus readCHV2status() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read CHV2 status");

      SelectMFDFResponse response = select(FileID.MF).first();
      return response.getChv2Status();
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public CHVStatus readUnblockCHV2status() throws ChannelException, APDUException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't read unblock CHV2 status");

      SelectMFDFResponse response = select(FileID.MF).first();
      return response.getUnblockChv2Status();
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public void verifyCHV2(final CHV chv2) throws ChannelException, APDUException, InterruptedException {
    verifyCHV(CHVType.CHV2, chv2);
  }

  @Override
  public void changeCHV2(final CHV oldCHV2, final CHV newCHV2) throws ChannelException, APDUException, InterruptedException {
    changeCHV(CHVType.CHV2, oldCHV2, newCHV2);
  }

  @Override
  public void unblockCHV2(final UnblockCHV unblockCHV2, final CHV newCHV2) throws ChannelException, APDUException, InterruptedException {
    unblockCHV(CHVType.CHV2, unblockCHV2, newCHV2);
  }

  @Override
  public String toString() {
    return toS;
  }

  private void verifyCHV(final CHVType chvType, final CHV chv) throws ChannelException, APDUException, InterruptedException {
    Objects.requireNonNull(chv, chvType + " must not be null");
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't verify CHV");

      try {
        byteBuffer.clear();
        CHV.encoding(chv, byteBuffer);
        byteBuffer.flip();
        byte[] data = new byte[byteBuffer.remaining()];
        byteBuffer.get(data);
        executeAPDU(new APDUCommand(APDUCommand.Instruction.VERIFY_CHV, (byte) 0x00, chvType.getCHVNumber(), (byte) 0x08, data));
      } catch (APDUCommandException e) {
        switch (e.getAPDUResponse().getStatus()) {
          case CONTRADICTION_WITH_CHV_STATUS:
            throw new IllegalCHVStateError("Can't verify " + chvType + ", it is already disabled or blocked");
          case ACCESS_CONDITION_NOT_FULFILLED:
            throw new InvalidCHVError("Invalid " + chvType + " value: " + chv);
          default:
            throw e;
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  private void changeCHV(final CHVType chvType, final CHV oldCHV, final CHV newCHV) throws ChannelException, APDUException, InterruptedException {
    Objects.requireNonNull(oldCHV, "old " + chvType + " must not be null");
    Objects.requireNonNull(newCHV, "new " + chvType + " must not be null");
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't change CHV");

      try {
        byteBuffer.clear();
        CHV.encoding(oldCHV, byteBuffer);
        CHV.encoding(newCHV, byteBuffer);
        byteBuffer.flip();
        byte[] data = new byte[byteBuffer.remaining()];
        byteBuffer.get(data);
        executeAPDU(new APDUCommand(APDUCommand.Instruction.CHANGE_CHV, (byte) 0x00, chvType.getCHVNumber(), (byte) 0x10, data));
      } catch (APDUCommandException e) {
        switch (e.getAPDUResponse().getStatus()) {
          case CONTRADICTION_WITH_CHV_STATUS:
            throw new IllegalCHVStateError("Can't change " + chvType + ", it is already disabled or blocked");
          case ACCESS_CONDITION_NOT_FULFILLED:
            throw new InvalidCHVError("Invalid old " + chvType + " value: " + oldCHV);
          default:
            throw e;
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  private void unblockCHV(final CHVType chvType, final UnblockCHV unblockCHV, final CHV newCHV) throws ChannelException, APDUException, InterruptedException {
    Objects.requireNonNull(unblockCHV, "unblock " + chvType + " must not be null");
    Objects.requireNonNull(newCHV, "new " + chvType + " must not be null");
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't unblock CHV");

      try {
        byteBuffer.clear();
        CHV.encoding(unblockCHV, byteBuffer);
        CHV.encoding(newCHV, byteBuffer);
        byteBuffer.flip();
        byte[] data = new byte[byteBuffer.remaining()];
        byteBuffer.get(data);
        executeAPDU(new APDUCommand(APDUCommand.Instruction.UNBLOCK_CHV, (byte) 0x00, chvType.getUnblockCHVNumber(), (byte) 0x10, data));
      } catch (APDUCommandException e) {
        switch (e.getAPDUResponse().getStatus()) {
          case ACCESS_CONDITION_NOT_FULFILLED:
            throw new InvalidCHVError("Invalid unblock " + chvType + " value: " + unblockCHV);
          default:
            throw e;
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  private OneOf<SelectMFDFResponse, SelectEFResponse> select(final FileID fileID) throws ChannelException, APDUException, InterruptedException {
    byteBuffer.clear();
    FileID.encoding(fileID, byteBuffer);
    byteBuffer.flip();
    byte[] data = new byte[byteBuffer.remaining()];
    byteBuffer.get(data);
    APDUCommand apduCommand = new APDUCommand(APDUCommand.Instruction.SELECT, (byte) 0x00, (byte) 0x00, (byte) 0x02, data);
    APDUResponse response = executeAPDU(apduCommand);
    return parseResponse(getResponse(response.getSW2()));
  }

  private DataResponse readBinary(final BinaryOffset offset, final int length) throws ChannelException, APDUException, InterruptedException {
    APDUCommand apduCommand = new APDUCommand(APDUCommand.Instruction.READ_BINARY, offset.getHigh(), offset.getLow(), (byte) length);
    APDUResponse response = executeAPDU(apduCommand);
    return new DataResponse(response);
  }

  private DataResponse readRecord(final RecordOperation operation, final int length) throws ChannelException, APDUException, InterruptedException {
    APDUCommand apduCommand = new APDUCommand(APDUCommand.Instruction.READ_RECORD, (byte) operation.getNumber(), operation.getMode().getCode(), (byte) length);
    APDUResponse response = executeAPDU(apduCommand);
    return new DataResponse(response);
  }

  private APDUResponse getResponse(final int length) throws ChannelException, APDUException, InterruptedException {
    APDUCommand apduCommand = new APDUCommand(APDUCommand.Instruction.GET_RESPONSE, (byte) 0x00, (byte) 0x00, (byte) length);
    return executeAPDU(apduCommand);
  }

  private OneOf<SelectMFDFResponse, SelectEFResponse> parseResponse(final APDUResponse response) throws ChannelException, APDUException {
    if (response.getData().length < 7) {
      throw new APDUException("Illegal length for SELECT response: " + response);
    }

    switch (FileType.valueOf(response.getData()[6])) {
      case DF:
      case MF:
        return OneOf.isTheFirst(new SelectMFDFResponse(response));
      case EF:
        return OneOf.isTheSecond(new SelectEFResponse(response));
      default:
        throw new IllegalStateException(String.format("Unsupported type of file: %s", FileType.RFU));
    }
  }

  private APDUResponse executeAPDU(final APDUCommand command) throws ChannelException, APDUException, InterruptedException {
    logger.debug("[{}] - sending {}", this, command);

    responseLock.lock();
    try {
      responseState = APDUResponseState.WAITING;
    } finally {
      responseLock.unlock();
    }

    try {
      ISCReaderSubChannel scReaderSubChannel = scReaderChannel.getSCReaderSubChannel();
      scReaderSubChannel.writeAPDUCommand(command);
    } catch (ChannelException | InterruptedException e) {
      responseLock.lock();
      try {
        responseState = APDUResponseState.IDLE;
      } finally {
        responseLock.unlock();
      }
      throw e;
    }

    APDUResponse localResponse;
    responseLock.lock();
    try {
      if (responseException != null) {
        throw responseException;
      }
      if (responseState == APDUResponseState.WAITING) {
        if (!responseCondition.await(RESPONSE_TIMEOUT, TimeUnit.MILLISECONDS)) {
          logger.debug("[{}] - timeout elapsed for {} ", this, command);
          throw new APDUTimeoutException();
        }
        if (responseException != null) {
          throw responseException;
        }
      } else if (responseState != APDUResponseState.RECEIVED) {
        throw new AssertionError(); // must not be here
      }
      localResponse = response;
      response = null;
    } finally {
      responseState = APDUResponseState.IDLE;
      responseLock.unlock();
    }
    logger.debug("[{}] - received {}", this, localResponse);

    if (correctlyExecutedResponses.contains(localResponse.getStatus())) {
      return localResponse;
    } else {
      logger.debug("[{}] - sending failed for {}", this, command);
      throw new APDUCommandException(localResponse);
    }
  }

  private String bcdToString(final byte[] bcd) {
    int arrLength = bcd.length * 2;
    int[] intArray = new int[arrLength];
    for (int i = 0; i < arrLength; i += 2) {
      intArray[i] = bcd[i / 2] & 0xf;
      intArray[i + 1] = (bcd[i / 2] & 0xf0) >> 4;
    }
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < arrLength; i++) {
      if (intArray[i] == 0x0F) {
        break;
      }
      stringBuilder.append(Integer.toHexString(intArray[i]));
    }
    return stringBuilder.toString();
  }

  private byte[] trimToFF(final byte[] data, final int offset, final int length) {
    int lastIndex = offset;
    for (int i = offset; i < length; i++) {
      lastIndex = i;
      if (data[i] == (byte) -1) {
        break;
      }
    }
    byte[] retVal = new byte[lastIndex - offset];
    System.arraycopy(data, offset, retVal, 0, lastIndex - offset);
    return retVal;
  }

  private class SCReaderSubChannelHandler implements ISCReaderSubChannelHandler {

    @Override
    public void handleAPDUResponse(final APDUResponse localResponse) {
      responseLock.lock();
      try {
        if (responseState != APDUResponseState.WAITING) {
          logger.debug("[{}] - received APDU Response [{}] without waiting for it - maybe delayed, response dropped", SIMCardUnit.this, response);
        } else {
          responseState = APDUResponseState.RECEIVED;
          response = localResponse;
          responseCondition.signalAll();
        }
      } finally {
        responseLock.unlock();
      }
    }

    @Override
    public void handleErrorState(final SCReaderError error, final SCReaderState state) {
      logger.debug("[{}] - handled error state: {}({})", SIMCardUnit.this, error, state);
      responseLock.lock();
      try {
        responseException = new SCReaderErrorStateException(error, state);
        responseCondition.signalAll();
      } finally {
        responseLock.unlock();
      }
    }
  }

}
