/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

public final class SMSCodec {

  private SMSCodec() {
  }

  public static final char[][] grcAlphabetRemapping = {
      {'\u0386', '\u0041'}, // GREEK CAPITAL LETTER ALPHA WITH TONOS
      {'\u0388', '\u0045'}, // GREEK CAPITAL LETTER EPSILON WITH TONOS
      {'\u0389', '\u0048'}, // GREEK CAPITAL LETTER ETA WITH TONOS
      {'\u038A', '\u0049'}, // GREEK CAPITAL LETTER IOTA WITH TONOS
      {'\u038C', '\u004F'}, // GREEK CAPITAL LETTER OMICRON WITH TONOS
      {'\u038E', '\u0059'}, // GREEK CAPITAL LETTER UPSILON WITH TONOS
      {'\u038F', '\u03A9'}, // GREEK CAPITAL LETTER OMEGA WITH TONOS
      {'\u0390', '\u0049'}, // GREEK SMALL LETTER IOTA WITH DIALYTIKA AND TONOS
      {'\u0391', '\u0041'}, // GREEK CAPITAL LETTER ALPHA
      {'\u0392', '\u0042'}, // GREEK CAPITAL LETTER BETA
      {'\u0393', '\u0393'}, // GREEK CAPITAL LETTER GAMMA
      {'\u0394', '\u0394'}, // GREEK CAPITAL LETTER DELTA
      {'\u0395', '\u0045'}, // GREEK CAPITAL LETTER EPSILON
      {'\u0396', '\u005A'}, // GREEK CAPITAL LETTER ZETA
      {'\u0397', '\u0048'}, // GREEK CAPITAL LETTER ETA
      {'\u0398', '\u0398'}, // GREEK CAPITAL LETTER THETA
      {'\u0399', '\u0049'}, // GREEK CAPITAL LETTER IOTA
      {'\u039A', '\u004B'}, // GREEK CAPITAL LETTER KAPPA
      {'\u039B', '\u039B'}, // GREEK CAPITAL LETTER LAMDA
      {'\u039C', '\u004D'}, // GREEK CAPITAL LETTER MU
      {'\u039D', '\u004E'}, // GREEK CAPITAL LETTER NU
      {'\u039E', '\u039E'}, // GREEK CAPITAL LETTER XI
      {'\u039F', '\u004F'}, // GREEK CAPITAL LETTER OMICRON
      {'\u03A0', '\u03A0'}, // GREEK CAPITAL LETTER PI
      {'\u03A1', '\u0050'}, // GREEK CAPITAL LETTER RHO
      {'\u03A3', '\u03A3'}, // GREEK CAPITAL LETTER SIGMA
      {'\u03A4', '\u0054'}, // GREEK CAPITAL LETTER TAU
      {'\u03A5', '\u0059'}, // GREEK CAPITAL LETTER UPSILON
      {'\u03A6', '\u03A6'}, // GREEK CAPITAL LETTER PHI
      {'\u03A7', '\u0058'}, // GREEK CAPITAL LETTER CHI
      {'\u03A8', '\u03A8'}, // GREEK CAPITAL LETTER PSI
      {'\u03A9', '\u03A9'}, // GREEK CAPITAL LETTER OMEGA
      {'\u03AA', '\u0049'}, // GREEK CAPITAL LETTER IOTA WITH DIALYTIKA
      {'\u03AB', '\u0059'}, // GREEK CAPITAL LETTER UPSILON WITH DIALYTIKA
      {'\u03AC', '\u0041'}, // GREEK SMALL LETTER ALPHA WITH TONOS
      {'\u03AD', '\u0045'}, // GREEK SMALL LETTER EPSILON WITH TONOS
      {'\u03AE', '\u0048'}, // GREEK SMALL LETTER ETA WITH TONOS
      {'\u03AF', '\u0049'}, // GREEK SMALL LETTER IOTA WITH TONOS
      {'\u03B0', '\u0059'}, // GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS
      {'\u03B1', '\u0041'}, // GREEK SMALL LETTER ALPHA
      {'\u03B2', '\u0042'}, // GREEK SMALL LETTER BETA
      {'\u03B3', '\u0393'}, // GREEK SMALL LETTER GAMMA
      {'\u03B4', '\u0394'}, // GREEK SMALL LETTER DELTA
      {'\u03B5', '\u0045'}, // GREEK SMALL LETTER EPSILON
      {'\u03B6', '\u005A'}, // GREEK SMALL LETTER ZETA
      {'\u03B7', '\u0048'}, // GREEK SMALL LETTER ETA
      {'\u03B8', '\u0398'}, // GREEK SMALL LETTER THETA
      {'\u03B9', '\u0049'}, // GREEK SMALL LETTER IOTA
      {'\u03BA', '\u004B'}, // GREEK SMALL LETTER KAPPA
      {'\u03BB', '\u039B'}, // GREEK SMALL LETTER LAMDA
      {'\u03BC', '\u004D'}, // GREEK SMALL LETTER MU
      {'\u03BD', '\u004E'}, // GREEK SMALL LETTER NU
      {'\u03BE', '\u039E'}, // GREEK SMALL LETTER XI
      {'\u03BF', '\u004F'}, // GREEK SMALL LETTER OMICRON
      {'\u03C0', '\u03A0'}, // GREEK SMALL LETTER PI
      {'\u03C1', '\u0050'}, // GREEK SMALL LETTER RHO
      {'\u03C2', '\u03A3'}, // GREEK SMALL LETTER FINAL SIGMA
      {'\u03C3', '\u03A3'}, // GREEK SMALL LETTER SIGMA
      {'\u03C4', '\u0054'}, // GREEK SMALL LETTER TAU
      {'\u03C5', '\u0059'}, // GREEK SMALL LETTER UPSILON
      {'\u03C6', '\u03A6'}, // GREEK SMALL LETTER PHI
      {'\u03C7', '\u0058'}, // GREEK SMALL LETTER CHI
      {'\u03C8', '\u03A8'}, // GREEK SMALL LETTER PSI
      {'\u03C9', '\u03A9'}, // GREEK SMALL LETTER OMEGA
      {'\u03CA', '\u0049'}, // GREEK SMALL LETTER IOTA WITH DIALYTIKA
      {'\u03CB', '\u0059'}, // GREEK SMALL LETTER UPSILON WITH DIALYTIKA
      {'\u03CC', '\u004F'}, // GREEK SMALL LETTER OMICRON WITH TONOS
      {'\u03CD', '\u0059'}, // GREEK SMALL LETTER UPSILON WITH TONOS
      {'\u03CE', '\u03A9'} // GREEK SMALL LETTER OMEGA WITH TONOS
  };

  public static final char[] GSM_03_38_CODE_TABLE = {
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0040, 0x00A3, 0x0024, 0x00A5, 0x00E8, 0x00E9, 0x00F9, 0x00EC, 0x00F2, 0x00C7, 0x000A, 0x00D8, 0x00F8, 0x000D, 0x00C5, 0x00E5,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0394, 0x005F, 0x03A6, 0x0393, 0x039B, 0x03A9, 0x03A0, 0x03A8, 0x03A3, 0x0398, 0x039E, 0x00A0, 0x00C6, 0x00E6, 0x00DF, 0x00C9,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0020, 0x0021, 0x0022, 0x0023, 0x00A4, 0x0025, 0x0026, 0x0027, 0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x00A1, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047, 0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059, 0x005A, 0x00C4, 0x00D6, 0x00D1, 0x00DC, 0x00A7,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x00BF, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067, 0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077, 0x0078, 0x0079, 0x007A, 0x00E4, 0x00F6, 0x00F1, 0x00FC, 0x00E0
      /* --------------------------------------------------------------------------------------------------------------------------*/
  };
  public static final char[] GSM_03_38_EXT_CODE_TABLE = {
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x000C, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x005E, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x007B, 0x007D, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x005C,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x005B, 0x007E, 0x005D, 0x0000,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x007C, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x20AC, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
      /* --------------------------------------------------------------------------------------------------------------------------*/
      0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
  };
  private static final char DOUBLE_GSM_CHARS[] = {0x000C, 0x005E, 0x007B, 0x007D, '\\', 0x005B, 0x007E, 0x005D, 0x007C, 0x20AC};

  private static int getIndex(final char[] array, final char symbol) {
    int retval = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] == symbol) {
        retval = i;
        break;
      }
    }
    return retval;
  }

  private synchronized static boolean isInGSM0338Alphabet(final char symbol) {
    boolean found = false;
    if (getIndex(GSM_03_38_CODE_TABLE, symbol) >= 0) {
      found = true;
    }

    if (!found && getIndex(DOUBLE_GSM_CHARS, symbol) >= 0) {
      found = true;
    }
    return found;
  }

  public static SMSEncoding getTextEncoding(final String txt) {
    SMSEncoding retval = SMSEncoding.ENC7BIT;
    char charArr[] = txt.toCharArray();
    for (char c : charArr) {
      if (!isInGSM0338Alphabet(c)) {
        retval = SMSEncoding.ENCUCS2;
        break;
      }
    }
    return retval;
  }

  public static boolean isDouble7BitSymbol(final char symbol) {
    boolean found = false;
    if (getIndex(DOUBLE_GSM_CHARS, symbol) >= 0) {
      found = true;
    }
    return found;
  }

}
