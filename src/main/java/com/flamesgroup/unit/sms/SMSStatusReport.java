/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public final class SMSStatusReport extends SMS implements Serializable {

  private static final long serialVersionUID = 4262654397754744574L;

  private final String recipientPhoneNumber;
  private final Date senderTime;
  private final Date receivedTime;
  private final DeliveryStatuses status;

  public SMSStatusReport(final String recipientPhoneNumber, final String smsCenterNumber, final Date senderTime, final Date receivedTime, final DeliveryStatuses status, final int referenceNumber) {
    super(smsCenterNumber, referenceNumber);
    this.recipientPhoneNumber = recipientPhoneNumber;
    this.senderTime = senderTime;
    this.receivedTime = receivedTime;
    this.status = status;
  }

  public String getRecipientPhoneNumber() {
    return recipientPhoneNumber;
  }

  public Date getSenderTime() {
    return senderTime;
  }

  public Date getReceivedTime() {
    return receivedTime;
  }

  public DeliveryStatuses getStatus() {
    return status;
  }

  @Override
  protected void appendToStringProperty(final StringBuffer sb) {
    super.appendToStringProperty(sb);
    sb.append(" rpn:'").append(recipientPhoneNumber).append('\'');
    sb.append(" st:").append(senderTime);
    sb.append(" rt:").append(receivedTime);
    sb.append(" s:").append(status);
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SMSStatusReport)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final SMSStatusReport that = (SMSStatusReport) object;

    return Objects.equals(recipientPhoneNumber, that.recipientPhoneNumber)
        && Objects.equals(senderTime, that.senderTime)
        && Objects.equals(receivedTime, that.receivedTime)
        && status == that.status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(recipientPhoneNumber);
    result = prime * result + Objects.hashCode(senderTime);
    result = prime * result + Objects.hashCode(receivedTime);
    result = prime * result + Objects.hashCode(status);
    return result;
  }

}
