/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import java.io.Serializable;
import java.util.Objects;

abstract class SMS implements Serializable {

  private static final long serialVersionUID = 8154418780674517122L;

  private final String smsCenterNumber;
  private final int referenceNumber;

  public SMS(final String smsCenterNumber, final int referenceNumber) {
    this.smsCenterNumber = smsCenterNumber;
    this.referenceNumber = referenceNumber;
  }

  public String getSmsCenterNumber() {
    return smsCenterNumber;
  }

  public int getReferenceNumber() {
    return referenceNumber;
  }

  protected void appendToStringProperty(final StringBuffer sb) {
    sb.append("smscn:'").append(smsCenterNumber).append('\'');
    sb.append(" rn:").append(referenceNumber);
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SMS)) {
      return false;
    }
    final SMS that = (SMS) object;

    return referenceNumber == that.referenceNumber
        && Objects.equals(smsCenterNumber, that.smsCenterNumber);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(smsCenterNumber);
    result = prime * result + referenceNumber;
    return result;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[");
    appendToStringProperty(sb);
    sb.append(']');
    return sb.toString();
  }

}
