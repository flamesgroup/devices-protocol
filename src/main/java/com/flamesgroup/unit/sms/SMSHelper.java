/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import com.flamesgroup.unit.PhoneNumber;
import org.ajwcc.pduUtils.gsm3040.PduFactory;
import org.ajwcc.pduUtils.gsm3040.PduGenerator;
import org.ajwcc.pduUtils.gsm3040.PduUtils;
import org.ajwcc.pduUtils.gsm3040.SmsSubmitPdu;

import java.util.List;

public final class SMSHelper {

  private SMSHelper() {
  }

  public static int getPduSize(final String pdu) throws PDUException {
    int retval;
    int smscLength;
    if (pdu.length() >= 2) {
      //ignore octets with SMSC number and the first octet with SMSC length
      smscLength = Integer.parseInt(pdu.substring(0, 2));
      retval = pdu.length() / 2 - smscLength - 1;
    } else {
      throw new PDUException("Wrong PDU value: " + pdu);
    }
    return retval;
  }

  public static List<String> createStringPdus(final PhoneNumber smscNumber, final PhoneNumber recipientPhoneNumber, final String messageText, final int referenceNumber,
      final SMSMessageClass messageClass, final int validityPeriod, final boolean needStatusReport) {
    PduGenerator pduGenerator = new PduGenerator();
    SmsSubmitPdu pdu = needStatusReport ? PduFactory.newSmsSubmitPdu(PduUtils.TP_SRR_REPORT | PduUtils.TP_VPF_INTEGER) : PduFactory.newSmsSubmitPdu();

    // smscInfo
    if (smscNumber != null) {
      String smscNumberStr = smscNumber.getValue();
      // address type field + #octets for smscNumber
      // NOTE: make sure the + is not present when computing the smscInfoLength
      String smscNumberForLengthCheck = smscNumberStr;
      if (smscNumberStr.startsWith("+")) {
        smscNumberForLengthCheck = smscNumberStr.substring(1);
      }
      pdu.setSmscInfoLength(1 + (smscNumberForLengthCheck.length() / 2) + ((smscNumberForLengthCheck.length() % 2 == 1) ? 1 : 0));

      // set address
      pdu.setSmscAddress(smscNumberStr);

      // set address type using the address (+ needs to be passed with it, if present)
      pdu.setSmscAddressType(PduUtils.getAddressTypeFor(smscNumberStr));
    } else {
      // set address
      pdu.setSmscAddress("");
    }

    // message reference
    // just use 0 since this is not tracked by the ModemGateway
    pdu.setMessageReference(0);

    // destination address info
    pdu.setAddress(recipientPhoneNumber.getValue());

    // protocol id
    pdu.setProtocolIdentifier(0);

    // data coding scheme
    if (!pdu.isBinary()) {
      int dcs = 0;
      switch (SMSCodec.getTextEncoding(messageText)) {
        case ENC7BIT:
          dcs = PduUtils.DCS_ENCODING_7BIT;
          break;
        case ENC8BIT:
          dcs = PduUtils.DCS_ENCODING_8BIT;
          break;
        case ENCUCS2:
          dcs = PduUtils.DCS_ENCODING_UCS2;
          break;
        case ENCCUSTOM:
          dcs = PduUtils.DCS_ENCODING_7BIT;
          break;
      }

      switch (messageClass) {
        case CLASS0_FLASH:
          dcs = dcs | PduUtils.DCS_MESSAGE_CLASS_FLASH;
          break;
        case CLASS1_ME:
          dcs = dcs | PduUtils.DCS_MESSAGE_CLASS_ME;
          break;
        case CLASS2_SIM:
          dcs = dcs | PduUtils.DCS_MESSAGE_CLASS_SIM;
          break;
        case CLASS3_TE:
          dcs = dcs | PduUtils.DCS_MESSAGE_CLASS_TE;
          break;
      }
      pdu.setDataCodingScheme(dcs);
    }

    pdu.setValidityPeriod(validityPeriod);
    pdu.setDecodedText(messageText);

    return pduGenerator.generatePduList(pdu, referenceNumber);
  }

  static SMSMessageClass getSMSMessageClassFromDataCodingScheme(final int dataCodingScheme) throws UnsupportedSMSCodingSchemeException {
    int dcsClass = PduUtils.extractDcsClass(dataCodingScheme);
    switch (dcsClass) {
      case PduUtils.DCS_MESSAGE_CLASS_FLASH:
        return SMSMessageClass.CLASS0_FLASH;
      case PduUtils.DCS_MESSAGE_CLASS_ME:
        return SMSMessageClass.CLASS1_ME;
      case PduUtils.DCS_MESSAGE_CLASS_SIM:
        return SMSMessageClass.CLASS2_SIM;
      case PduUtils.DCS_MESSAGE_CLASS_TE:
        return SMSMessageClass.CLASS3_TE;
      case PduUtils.DCS_MESSAGE_CLASS_NONE:
        return SMSMessageClass.NONE;
      default:
        return SMSMessageClass.NONE; // bit 4 is 0, but bits 1 to 0 is not 0 - from doc this bits is reserved and have no message class meaning
    }
  }

  static SMSEncoding getSMSEncodingFromDataCodingScheme(final int dataCodingScheme) throws UnsupportedSMSCodingSchemeException {
    int dcsEncoding = PduUtils.extractDcsEncoding(dataCodingScheme);
    switch (dcsEncoding) {
      case PduUtils.DCS_ENCODING_7BIT:
        return SMSEncoding.ENC7BIT;
      case PduUtils.DCS_ENCODING_8BIT:
        return SMSEncoding.ENC8BIT;
      case PduUtils.DCS_ENCODING_UCS2:
        return SMSEncoding.ENCUCS2;
      default:
        throw new UnsupportedSMSCodingSchemeException("Unknown encoding value: " + dcsEncoding);
    }
  }

  static DeliveryStatuses getDeliveryStatuses(final int status) {
    if ((status & 0x60) == 0) {
      return DeliveryStatuses.DELIVERED;
    }
    if ((status & 0x20) == 0x20) {
      return DeliveryStatuses.KEEPTRYING;
    }
    if ((status & 0x46) == 0x46) {
      return DeliveryStatuses.EXPIRED;
    }
    if ((status & 0x47) == 0x47) {
      return DeliveryStatuses.DELETED;
    }
    if ((status & 0x48) == 0x48) {
      return DeliveryStatuses.DELETED;
    }
    if ((status & 0x40) == 0x40) {
      return DeliveryStatuses.ABORTED;
    }
    if ((status & 0x60) == 0x60) {
      return DeliveryStatuses.ABORTED;
    }
    return DeliveryStatuses.UNKNOWN;
  }

}
