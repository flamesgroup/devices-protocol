/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import java.util.Objects;

public final class SMSMessageOutbound extends SMSMessage {

  private static final long serialVersionUID = -1612609398315323836L;

  private final String recipientPhoneNumber;
  private final boolean receiptRequested;

  public SMSMessageOutbound(final String recipientPhoneNumber, final String smsCenterNumber, final String message, final SMSMessageClass messageClass, final SMSEncoding smsEncoding,
      final int referenceNumber, final int numberOfSmsPart, final int countOfSmsParts, final boolean receiptRequested) {
    super(smsCenterNumber, message, messageClass, smsEncoding, referenceNumber, numberOfSmsPart, countOfSmsParts);
    Objects.requireNonNull(recipientPhoneNumber, "recipientPhoneNumber mustn't be null");
    this.recipientPhoneNumber = recipientPhoneNumber;
    this.receiptRequested = receiptRequested;
  }

  public String getRecipientPhoneNumber() {
    return recipientPhoneNumber;
  }

  public boolean isReceiptRequested() {
    return receiptRequested;
  }

  @Override
  protected void appendToStringProperty(final StringBuffer sb) {
    super.appendToStringProperty(sb);
    sb.append(" rpn:'").append(recipientPhoneNumber).append('\'');
    sb.append(" rr:").append(receiptRequested);
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SMSMessageOutbound)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final SMSMessageOutbound that = (SMSMessageOutbound) object;

    return Objects.equals(recipientPhoneNumber, that.recipientPhoneNumber)
        && receiptRequested == that.receiptRequested;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(recipientPhoneNumber);
    result = prime * result + Boolean.hashCode(receiptRequested);
    return result;
  }

}
