/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import java.io.Serializable;
import java.util.Objects;

abstract class SMSMessage extends SMS implements Serializable {

  private static final long serialVersionUID = -4037999518483195078L;

  private final String message;
  private final SMSMessageClass messageClass;
  private final SMSEncoding encoding;
  private final int numberOfSmsPart;
  private final int countOfSmsParts;

  public SMSMessage(final String smsCenterNumber, final String message, final SMSMessageClass messageClass, final SMSEncoding encoding, final int referenceNumber, final int numberOfSmsPart,
      final int countOfSmsParts) {
    super(smsCenterNumber, referenceNumber);
    Objects.requireNonNull(message, "message mustn't be null");
    Objects.requireNonNull(messageClass, "messageClass mustn't be null");
    Objects.requireNonNull(encoding, "encoding mustn't be null");
    this.message = message;
    this.messageClass = messageClass;
    this.encoding = encoding;
    this.numberOfSmsPart = numberOfSmsPart;
    this.countOfSmsParts = countOfSmsParts;
  }

  public String getMessage() {
    return message;
  }

  public SMSMessageClass getMessageClass() {
    return messageClass;
  }

  public SMSEncoding getEncoding() {
    return encoding;
  }

  public int getNumberOfSmsPart() {
    return numberOfSmsPart;
  }

  public int getCountOfSmsParts() {
    return countOfSmsParts;
  }

  @Override
  protected void appendToStringProperty(final StringBuffer sb) {
    super.appendToStringProperty(sb);
    sb.append(" m:'").append(message).append('\'');
    sb.append(" mc:").append(messageClass);
    sb.append(" e:").append(encoding);
    sb.append(" n:").append(numberOfSmsPart);
    sb.append(" c:").append(countOfSmsParts);
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SMSMessage)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final SMSMessage that = (SMSMessage) object;

    return numberOfSmsPart == that.numberOfSmsPart
        && countOfSmsParts == that.countOfSmsParts
        && message.equals(that.message)
        && messageClass == that.messageClass
        && encoding == that.encoding;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + message.hashCode();
    result = prime * result + messageClass.hashCode();
    result = prime * result + encoding.hashCode();
    result = prime * result + numberOfSmsPart;
    result = prime * result + countOfSmsParts;
    return result;
  }

}
