/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import com.flamesgroup.unit.PhoneNumber;
import org.ajwcc.pduUtils.gsm3040.Pdu;
import org.ajwcc.pduUtils.gsm3040.PduParser;
import org.ajwcc.pduUtils.gsm3040.SmsDeliveryPdu;
import org.ajwcc.pduUtils.gsm3040.SmsStatusReportPdu;

public final class SMSFactory {

  private SMSFactory() {
  }

  public static SMSMessageInbound createInboundSMSFromPdu(final SmsDeliveryPdu smsDeliveryPdu) throws UnsupportedSMSCodingSchemeException {
    return new SMSMessageInbound(smsDeliveryPdu.getAddress(), smsDeliveryPdu.getSmscAddress(), smsDeliveryPdu.getDecodedText(),
        SMSHelper.getSMSMessageClassFromDataCodingScheme(smsDeliveryPdu.getDataCodingScheme()),
        SMSHelper.getSMSEncodingFromDataCodingScheme(smsDeliveryPdu.getDataCodingScheme()), smsDeliveryPdu.getTimestamp(),
        smsDeliveryPdu.getMpRefNo(), smsDeliveryPdu.getMpSeqNo(), smsDeliveryPdu.getMpMaxNo());
  }

  public static SMSStatusReport createSMSStatusReportFromPdu(final SmsStatusReportPdu pdu) {
    return new SMSStatusReport(pdu.getAddress(), pdu.getSmscAddress(), pdu.getTimestamp(), pdu.getDischargeTime(),
        SMSHelper.getDeliveryStatuses(pdu.getStatus()), pdu.getMessageReference());
  }

  public static SMSMessageOutbound createSmsMessageOutbound(final PhoneNumber number, final String smsPduString, final int referenceNumber, final int numberOfSmsPart, final int countOfSmsParts,
      final boolean receiptRequested) throws UnsupportedSMSCodingSchemeException {
    Pdu pdu = new PduParser().parsePdu(smsPduString);
    return new SMSMessageOutbound(number.getValue(), null, pdu.getDecodedText(),
        SMSHelper.getSMSMessageClassFromDataCodingScheme(pdu.getDataCodingScheme()),
        SMSHelper.getSMSEncodingFromDataCodingScheme(pdu.getDataCodingScheme()),
        receiptRequested ? referenceNumber : -1, numberOfSmsPart, countOfSmsParts, receiptRequested);
  }

}
