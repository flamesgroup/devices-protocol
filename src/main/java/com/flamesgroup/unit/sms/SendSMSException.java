/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

public class SendSMSException extends SMSException {

  private static final long serialVersionUID = -5025914855327677341L;

  private int totalSmsParts;
  private int amountOfSendSmsParts;

  public SendSMSException() {
    super();
  }

  public SendSMSException(final String message) {
    super(message);
  }

  public SendSMSException(final Throwable cause) {
    super(cause);
  }

  public SendSMSException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public SendSMSException(final String message, final int totalSmsParts, final int amountOfSendSmsParts) {
    this(message, totalSmsParts, amountOfSendSmsParts, null);
  }

  public SendSMSException(final String message, final int totalSmsParts, final int amountOfSendSmsParts, final Throwable cause) {
    super(message, cause);
    this.totalSmsParts = totalSmsParts;
    this.amountOfSendSmsParts = amountOfSendSmsParts;
  }

  public int getTotalSmsParts() {
    return totalSmsParts;
  }

  public int getAmountOfSendSmsParts() {
    return amountOfSendSmsParts;
  }

}
