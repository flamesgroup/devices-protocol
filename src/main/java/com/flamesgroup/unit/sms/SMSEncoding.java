/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

public enum SMSEncoding {

  /**
   * 7 bit encoding - standard GSM alphabet.
   */
  ENC7BIT(160, 153),
  /**
   * 8 bit encoding.
   */
  ENC8BIT(140, 134),
  /**
   * UCS2 (Unicode) encoding.
   */
  ENCUCS2(70, 67),
  /**
   * Custom encoding. Currently just defaults to 7-bit.
   */
  ENCCUSTOM(160, 153);

  private final int length;
  private final int concatLength;

  SMSEncoding(final int len, final int concatLen) {
    length = len;
    concatLength = concatLen;
  }

  public int getLength() {
    return length;
  }

  public int getConcatLength() {
    return concatLength;
  }

}
