/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

/**
 * Enumeration representing delivery (status report) message status.
 */
public enum DeliveryStatuses {

  /**
   * Unknown delivery status.
   */
  UNKNOWN,
  /**
   * Delivery in progress
   */
  INPROGRESS,
  /**
   * Message has been delivered.
   */
  DELIVERED,
  /**
   * Message has not been delivered yet - will keep trying.
   */
  KEEPTRYING,
  /**
   * Message expired.
   */
  EXPIRED,
  /**
   * Message deleted.
   */
  DELETED,
  /**
   * Message has failed to be delivered.
   */
  ABORTED

}
