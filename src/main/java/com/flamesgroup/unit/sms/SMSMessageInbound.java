/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import java.util.Date;
import java.util.Objects;

public final class SMSMessageInbound extends SMSMessage {

  private static final long serialVersionUID = -8001083466971919565L;

  private final String senderPhoneNumber;
  private final Date senderTime;

  public SMSMessageInbound(final String senderPhoneNumber, final String smsCenterNumber, final String message, final SMSMessageClass messageClass, final SMSEncoding smsEncoding,
      final Date senderTime, final int referenceNumber, final int numberOfSmsPart, final int countOfSmsParts) {
    super(smsCenterNumber, message, messageClass, smsEncoding, referenceNumber, numberOfSmsPart, countOfSmsParts);
    this.senderPhoneNumber = senderPhoneNumber;
    this.senderTime = senderTime;
  }

  public String getSenderPhoneNumber() {
    return senderPhoneNumber;
  }

  public Date getSenderTime() {
    return senderTime;
  }

  @Override
  protected void appendToStringProperty(final StringBuffer sb) {
    super.appendToStringProperty(sb);
    sb.append(" spn:'").append(senderPhoneNumber).append('\'');
    sb.append(" st:").append(senderTime);
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SMSMessageInbound)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final SMSMessageInbound that = (SMSMessageInbound) object;

    return Objects.equals(senderPhoneNumber, that.senderPhoneNumber)
        && Objects.equals(senderTime, that.senderTime);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(senderPhoneNumber);
    result = prime * result + Objects.hashCode(senderTime);
    return result;
  }

}
