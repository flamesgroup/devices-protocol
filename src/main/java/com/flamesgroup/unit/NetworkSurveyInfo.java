/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.io.Serializable;
import java.util.List;

public final class NetworkSurveyInfo implements Serializable {

  private static final long serialVersionUID = 5658112991168982619L;

  private final Integer arfcn;
  private final Integer bsic;
  private final Integer rxLev;
  private final Float ber;
  private final Short mcc;
  private final Short mnc;
  private final Integer lac;
  private final Integer cellId;
  private final Integer cellStatus;
  private final List<Integer> validChannels;
  private final List<Integer> neighbourChannels;

  private transient String toS;

  public NetworkSurveyInfo(final Integer arfcn, final Integer bsic, final Integer rxLev, final Float ber, final Short mcc, final Short mnc, final Integer lac, final Integer cellId,
      final Integer cellStatus, final List<Integer> validChannels, final List<Integer> neighbourChannels) {
    this.arfcn = arfcn;
    this.bsic = bsic;
    this.rxLev = rxLev;
    this.ber = ber;
    this.mcc = mcc;
    this.mnc = mnc;
    this.lac = lac;
    this.cellId = cellId;
    this.cellStatus = cellStatus;
    this.validChannels = validChannels;
    this.neighbourChannels = neighbourChannels;
  }

  public NetworkSurveyInfo(final Integer arfcn, final Integer rxLev) {
    this(arfcn, null, rxLev, null, null, null, null, null, null, null, null);
  }

  public Integer getArfcn() {
    return arfcn;
  }

  public Integer getBsic() {
    return bsic;
  }

  public Integer getRxLev() {
    return rxLev;
  }

  public Float getBer() {
    return ber;
  }

  public Short getMcc() {
    return mcc;
  }

  public Short getMnc() {
    return mnc;
  }

  public Integer getLac() {
    return lac;
  }

  public Integer getCellId() {
    return cellId;
  }

  public Integer getCellStatus() {
    return cellStatus;
  }

  public List<Integer> getValidChannels() {
    return validChannels;
  }

  public List<Integer> getNeighbourChannels() {
    return neighbourChannels;
  }

  @Override
  public String toString() {
    if (toS == null) {
      toS = String.format("%s@%x:[%d,%d,%d,%f,%d,%d,%d,%d,%d,%s,%s]", getClass().getSimpleName(), hashCode(), arfcn, bsic, rxLev, ber, mcc, mnc, lac, cellId, cellStatus, validChannels,
          neighbourChannels);
    }
    return toS;
  }

}
