/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IAudioSubChannel;
import com.flamesgroup.unit.IAudioStream;
import com.flamesgroup.unit.IAudioStreamHandler;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class AudioSubChannel2AudioStreamAdapter implements IAudioStream {

  private final IAudioSubChannel audioSubChannel;

  private final Lock audioStreamLock = new ReentrantLock();
  private boolean open;

  public AudioSubChannel2AudioStreamAdapter(final IAudioSubChannel audioSubChannel) {
    Objects.requireNonNull(audioSubChannel, "audioSubChannel should not be null");

    this.audioSubChannel = audioSubChannel;
  }

  @Override
  public void open(final IAudioStreamHandler audioStreamHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(audioStreamHandler, "audioStreamHandler should not be null");

    audioStreamLock.lock();
    try {
      if (open) {
        throw new IllegalStateException("Audio stream has not been closed");
      }

      audioSubChannel.start(null, audioStreamHandler::handleAudio);
      open = true;
    } finally {
      audioStreamLock.unlock();
    }
  }

  @Override
  public void close() throws ChannelException, InterruptedException {
    audioStreamLock.lock();
    try {
      if (!open) {
        throw new IllegalStateException("Cannot close audio stream, because it has not been opened");
      }

      open = false;
      audioSubChannel.stop();
    } finally {
      audioStreamLock.unlock();
    }
  }

  @Override
  public boolean isOpen() {
    audioStreamLock.lock();
    try {
      return open;
    } finally {
      audioStreamLock.unlock();
    }
  }

  @Override
  public void write(final long timestamp, final byte[] data, final int offset, final int length) throws ChannelException {
    audioSubChannel.writeAudioData((int) timestamp, data, offset, length);
  }

}
