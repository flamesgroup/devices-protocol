/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannel;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannelHandler;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.protocol.remotesim.IRemoteSimConnection;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;

public final class RemoteSimConnection2SCEmulatorSubChannelAdapter implements ISCEmulatorSubChannel {

  private final IRemoteSimConnection remoteSimConnection;

  public RemoteSimConnection2SCEmulatorSubChannelAdapter(final IRemoteSimConnection remoteSimConnection) {
    this.remoteSimConnection = remoteSimConnection;
  }

  @Override
  public void start(final ATR atr, final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler) throws ChannelException, InterruptedException {
    remoteSimConnection.connect(atr, scEmulatorSubChannelHandler::handleAPDUCommand);
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    remoteSimConnection.disconnect();
  }

  @Override
  public void writeAPDUResponse(final APDUResponse response) throws ChannelException, InterruptedException {
    remoteSimConnection.transferAPDUResponse(response);
  }

  @Override
  public SCEmulatorState getState() throws ChannelException, InterruptedException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

}
