/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import static com.flamesgroup.unit.ms.ExtendedErrorReportHelper.SIEMENS_EXTENDED_CALL_ERROR_REPORT_CAUSES_PATH;
import static com.flamesgroup.unit.ms.ExtendedErrorReportHelper.getExtendedErrorReportCauses;

import com.flamesgroup.unit.IExtendedCallErrorReport;

import java.util.Map;

public final class SiemensExtendedCallErrorReport implements IExtendedCallErrorReport {

  private static final long serialVersionUID = 6101771017247967535L;

  private final static Map<Integer, String> causes = getExtendedErrorReportCauses(SIEMENS_EXTENDED_CALL_ERROR_REPORT_CAUSES_PATH);

  private final int locationID;
  private final int reason;
  private final int ssRelease;

  private transient String causeDescription;
  private transient String toS;

  public SiemensExtendedCallErrorReport(final int locationID, final int reason, final int ssRelease) {
    this.locationID = locationID;
    this.reason = reason;
    this.ssRelease = ssRelease;
  }

  @Override
  public int getCallControlConnectionManagementCause() {
    return (locationID == 8) ? reason : -1;
  }

  @Override
  public String getCallControlConnectionManagementCauseDescription() {
    if (causeDescription == null) {
      causeDescription = String.format("%s: LocationID: [%d], reason: [%d], ssRelease: [%d]", causes.get(reason), locationID, reason, ssRelease);
    }
    return causeDescription;
  }

  @Override
  public String toString() {
    if (toS == null) {
      toS = String.format("%s@%x:[%d],[%d],[%d]", getClass().getSimpleName(), hashCode(), locationID, reason, ssRelease);
    }
    return toS;
  }

}
