/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.unit.RegistrationStatus;

final class RegistrationInfo {

  private final RegistrationStatus status;
  private final String locationAreaCode;
  private final String cellID;

  public RegistrationInfo(final RegistrationStatus status, final String locationAreaCode, final String cellID) {
    this.status = status;
    this.locationAreaCode = locationAreaCode;
    this.cellID = cellID;
  }

  public RegistrationStatus getStatus() {
    return status;
  }

  public String getLocationAreaCode() {
    return locationAreaCode;
  }

  public String getCellID() {
    return cellID;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof RegistrationInfo)) {
      return false;
    }
    RegistrationInfo registrationInfo = (RegistrationInfo) object;

    boolean result = status == registrationInfo.getStatus();
    if (!result) {
      return false;
    }
    if (locationAreaCode != null) {
      result = locationAreaCode.equals(registrationInfo.getLocationAreaCode()) && cellID.equals(registrationInfo.getCellID());
    }
    return result;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + status.hashCode();
    result = prime * result + (locationAreaCode == null ? 0 : locationAreaCode.hashCode());
    result = prime * result + (cellID == null ? 0 : cellID.hashCode());
    return result;
  }

}
