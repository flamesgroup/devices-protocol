/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPort;

public class TelitMobileStationCMUXVirtualPortTraceCollector extends BaseTelitMobileStationTraceCollector {

  private final ICMUXVirtualPort traceCmuxVirtualPort;

  public TelitMobileStationCMUXVirtualPortTraceCollector(final ICMUXVirtualPort traceCmuxVirtualPort) {
    this.traceCmuxVirtualPort = traceCmuxVirtualPort;
  }

  @Override
  protected void traceStart(final ITelitMobileStationTraceHandler traceHandler) throws ChannelException, InterruptedException {
    traceCmuxVirtualPort.open(traceHandler::handleTraceData);
  }

  @Override
  protected void traceStop() throws ChannelException, InterruptedException {
    traceCmuxVirtualPort.close();
  }

}
