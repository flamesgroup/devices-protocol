/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhEQCELLWrite;
import com.flamesgroup.unit.CellAdjustment;
import com.flamesgroup.unit.ICellEqualizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class GSMTelitCachedCellEqualizer implements ICellEqualizer {

  private final Lock writtenCellAdjustmentsLock = new ReentrantLock();
  private final Map<Integer, Integer> writtenCellAdjustments = new HashMap<>();

  private final GSMTelitMobileStationUnit gsmTelitMobileStationUnit;

  GSMTelitCachedCellEqualizer(final GSMTelitMobileStationUnit gsmTelitMobileStationUnit) {
    Objects.requireNonNull(gsmTelitMobileStationUnit, "gsmTelitMobileStationUnit mustn't be null");
    this.gsmTelitMobileStationUnit = gsmTelitMobileStationUnit;
  }

  @Override
  public void write(final CellAdjustment cellAdjustment) throws ChannelException, InterruptedException {
    writtenCellAdjustmentsLock.lock();
    try {
      gsmTelitMobileStationUnit.executeExtra(new AThhEQCELLWrite(cellAdjustment.getArfcn(), cellAdjustment.getRxLevAdjust()));
      if (cellAdjustment.getRxLevAdjust() == 0) {
        writtenCellAdjustments.remove(cellAdjustment.getArfcn());
      } else {
        writtenCellAdjustments.put(cellAdjustment.getArfcn(), cellAdjustment.getRxLevAdjust());
      }
    } finally {
      writtenCellAdjustmentsLock.unlock();
    }
  }

  @Override
  public void clear() throws ChannelException, InterruptedException {
    writtenCellAdjustmentsLock.lock();
    try {
      gsmTelitMobileStationUnit.executeExtra(new AThhEQCELLWrite());
      writtenCellAdjustments.clear();
    } finally {
      writtenCellAdjustmentsLock.unlock();
    }
  }

  @Override
  public List<CellAdjustment> read() throws ChannelException, InterruptedException {
    writtenCellAdjustmentsLock.lock();
    try {
      return writtenCellAdjustments.entrySet().stream().map(entry -> new CellAdjustment(entry.getKey(), entry.getValue())).collect(Collectors.toCollection(ArrayList::new));
    } finally {
      writtenCellAdjustmentsLock.unlock();
    }
  }

}
