/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.SMSInfo;
import com.flamesgroup.device.gsmb.atengine.at.SMSStatus;
import com.flamesgroup.unit.IIncomingSMSHandler;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.unit.sms.SMSFactory;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import com.flamesgroup.unit.sms.SMSStatusReport;
import org.ajwcc.pduUtils.gsm3040.Pdu;
import org.ajwcc.pduUtils.gsm3040.PduParser;
import org.ajwcc.pduUtils.gsm3040.SmsDeliveryPdu;
import org.ajwcc.pduUtils.gsm3040.SmsStatusReportPdu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

abstract class BaseMobileIncomingSMSHandler {

  static final Logger logger = LoggerFactory.getLogger(BaseMobileIncomingSMSHandler.class);

  private IIncomingSMSHandler incomingSMSHandler;

  private final Queue<Integer> smsIndexToProcessBlockingQueue = new LinkedBlockingDeque<>(); // blocking because can be changed from another thread through enqueue* methods
  private final Queue<Integer> smsIndexToDeleteQueue = new LinkedList<>();
  private final Queue<SMSInfo> smsInfoQueue = new LinkedList<>();
  private final Queue<String> smsPduToProcessBlockingQueue = new LinkedBlockingDeque<>();

  private static final long TURN_ON_SMS_OVERFLOW_NOTIFICATION_PERIOD = 60000;
  private static final long READ_ALL_SMS_PERIOD = 10000;

  private long nextTimeToEnableSMSOverflowNotificationMillis;
  private long nextTimeToTryReadAllSMSMillis;

  void init(final IIncomingSMSHandler incomingSMSHandler) {
    Objects.requireNonNull(incomingSMSHandler, "incomingSMSHandler mustn't be null");

    this.incomingSMSHandler = incomingSMSHandler;

    smsIndexToProcessBlockingQueue.clear();
    smsIndexToDeleteQueue.clear();
    smsInfoQueue.clear();
    smsPduToProcessBlockingQueue.clear();
    nextTimeToEnableSMSOverflowNotificationMillis = System.currentTimeMillis() + TURN_ON_SMS_OVERFLOW_NOTIFICATION_PERIOD;
    nextTimeToTryReadAllSMSMillis = System.currentTimeMillis() + READ_ALL_SMS_PERIOD;
  }

  void processSMSRoutine() {
    enableIfNotEnabledSMSOverflowNotification();
    if (nextTimeToTryReadAllSMSMillis != 0) {
      if (System.currentTimeMillis() > nextTimeToTryReadAllSMSMillis) {
        List<SMSInfo> smsList;
        try {
          smsList = getStoredSMSList(SMSStatus.ALL);
          try {
            enableRouteSMSDirectlyToTE();
            readAndDeleteAllSMS(smsList);
            nextTimeToTryReadAllSMSMillis = 0;
          } catch (ChannelException | InterruptedException e) {
            logger.warn("[{}] - can't enable flush SMS to TE [{}]", this, e);
          }
        } catch (ChannelException e) {
          nextTimeToTryReadAllSMSMillis = System.currentTimeMillis() + READ_ALL_SMS_PERIOD;
        } catch (InterruptedException e) {
          throw new IllegalStateException(e);
        }
      }
    } else {
      String pdu = smsPduToProcessBlockingQueue.poll();
      if (pdu != null) {
        try {
          confirmSuccessfulSMSReceipt();
        } catch (ChannelException e) {
          logger.warn("[{}] - can't confirm receipt of SMS", this, e);
        } catch (InterruptedException e) {
          throw new IllegalStateException(e);
        }
        processSMSPdu(pdu);
      }
      if (!processSMSInfo()) {
        if (!processSMSIndexesToDelete()) {
          processSMSIndexesToProcess();
        }
      }
    }
  }

  void enqueueIncomingSMSIndex(final int index) {
    smsIndexToProcessBlockingQueue.add(index);
  }

  void enqueueIncomingSMSOverflow() {
    logger.info("[{}] - SMS buffer is full, try to clear it", this);
    smsIndexToProcessBlockingQueue.add(0);
  }

  void processIncomingSMSPdu(final String pdu) {
    smsPduToProcessBlockingQueue.add(pdu);
  }

  private void readAndDeleteAllSMS(final List<SMSInfo> list) {
    if (list.size() > 0) {
      logger.debug("[{}] - try to read [{}] SMS", this, list.size());
      for (SMSInfo smsInfo : list) {
        try {
          logger.debug("[{}] - try to delete and store SMS with index [{}]", this, smsInfo.getIndex());
          deleteStoredSMS(smsInfo.getIndex());
          processSMSPdu(smsInfo.getPDU());
          smsIndexToProcessBlockingQueue.remove(smsInfo.getIndex());
        } catch (ChannelException e) {
          logger.warn("[{}] - can't delete SMS with index [{}]", this, smsInfo.getIndex(), e);
        } catch (InterruptedException e) {
          throw new IllegalStateException(e);
        }
      }
    }
  }

  private boolean processSMSIndexesToProcess() {
    Integer index = smsIndexToProcessBlockingQueue.poll();
    if (index == null) {
      return false;
    } else {
      try {
        if (index == 0) {
          logger.debug("[{}] - try to get stored SMS list", this);
          try {
            List<SMSInfo> list = getStoredSMSList(SMSStatus.ALL);
            for (SMSInfo smsInfo : list) {
              if (!checkSMSIndexesToDelete(smsInfo.getIndex())) {
                enqueueSMSInfo(smsInfo);
              }
            }
          } catch (ChannelException | InterruptedException e) {
            logger.warn("[{}] - can't get stored SMS list", this, e);
            throw e;
          }
        } else {
          logger.debug("[{}] - try to get stored SMS with index [{}]", this, index);
          try {
            enqueueSMSInfo(getStoredSMS(index));
          } catch (ChannelException | InterruptedException e) {
            logger.warn("[{}] - can't get stored SMS with index [{}]", this, index, e);
            throw e;
          }
        }
        return true;
      } catch (ChannelException e) {
        smsIndexToProcessBlockingQueue.add(index);
        return false;
      } catch (InterruptedException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  private void enqueueSMSIndexesToDelete(final int index) {
    smsIndexToDeleteQueue.add(index);
  }

  private boolean checkSMSIndexesToDelete(final int index) {
    return smsIndexToDeleteQueue.contains(index);
  }

  private boolean processSMSIndexesToDelete() {
    Integer smsToDeleteIndex = smsIndexToDeleteQueue.poll();
    if (smsToDeleteIndex == null) {
      return false;
    } else {
      try {
        logger.debug("[{}] - try to delete stored SMS with index [{}]", this, smsToDeleteIndex);
        deleteStoredSMS(smsToDeleteIndex);
        return true;
      } catch (ChannelException e) {
        logger.warn("[{}] - can't delete SMS with index [{}]", this, smsToDeleteIndex, e);
        smsIndexToDeleteQueue.add(smsToDeleteIndex);
        return false;
      } catch (InterruptedException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  private void enqueueSMSInfo(final SMSInfo sms) {
    smsInfoQueue.add(sms);
  }

  private boolean processSMSInfo() {
    SMSInfo smsInfo = smsInfoQueue.poll();
    if (smsInfo == null) {
      return false;
    } else {
      if (smsInfo.getStatus() == SMSStatus.UNREAD) {
        processSMSPdu(smsInfo.getPDU());
      }
      enqueueSMSIndexesToDelete(smsInfo.getIndex());
      return true;
    }
  }

  private void processSMSPdu(final String pduStr) {
    logger.debug("[{}] - try to handle SMS  with pdu [{}]", BaseMobileIncomingSMSHandler.this, pduStr);
    PduParser pduParser = new PduParser();
    try {
      Pdu pdu = pduParser.parsePdu(pduStr);
      if (pdu.isBinary()) {// dirty fix to save binary sms: IssueID #961
        if (pdu.getMpRefNo() == 0 || pdu.getMpSeqNo() == pdu.getMpMaxNo()) {
          logger.trace("[{}] - received new binary SMS: {}", BaseMobileIncomingSMSHandler.this, pduStr);

          incomingSMSHandler.handleSMSBinary(pdu.getAddress(), "binary SMS", ((SmsDeliveryPdu) pdu).getTimestamp());
        }
        return;
      }

      if (pdu instanceof SmsDeliveryPdu) {
        logger.trace("[{}] - received new SMS: {}", BaseMobileIncomingSMSHandler.this, pduStr);
        SMSMessageInbound smsMessageInbound = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

        incomingSMSHandler.handleSMS(smsMessageInbound);
      } else if (pdu instanceof SmsStatusReportPdu) {
        logger.trace("[{}] - received SMS status report: {}", BaseMobileIncomingSMSHandler.this, pduStr);
        SMSStatusReport smsStatusReport = SMSFactory.createSMSStatusReportFromPdu((SmsStatusReportPdu) pdu);

        incomingSMSHandler.handleSMSDeliveryStatus(smsStatusReport);
      } else {
        logger.warn("[{}] - wrong type of PDU detected: {}", BaseMobileIncomingSMSHandler.this, pdu.getClass().getName());
        logger.warn("[{}] - error PDU: {}", BaseMobileIncomingSMSHandler.this, pduStr);
      }
    } catch (SMSException e) {
      logger.warn("[{}] - handled unknown incoming SMS pdu: {}", BaseMobileIncomingSMSHandler.this, pduStr, e);
    }
  }

  private void enableIfNotEnabledSMSOverflowNotification() {
    if (nextTimeToEnableSMSOverflowNotificationMillis != 0) {
      if (System.currentTimeMillis() > nextTimeToEnableSMSOverflowNotificationMillis) {
        try {
          enableSMSOverflowNotification();
        } catch (ChannelException e) {
          nextTimeToEnableSMSOverflowNotificationMillis = System.currentTimeMillis() + TURN_ON_SMS_OVERFLOW_NOTIFICATION_PERIOD;
          logger.warn("[{}] - can't enable SMS overflow notification", this, e);
          return;
        } catch (InterruptedException e) {
          throw new IllegalStateException(e);
        }
        nextTimeToEnableSMSOverflowNotificationMillis = 0;
        logger.debug("[{}] - SMS overflow notification has been enabled", this);
      }
    }
  }

  abstract void enableSMSOverflowNotification() throws ChannelException, InterruptedException;

  abstract SMSInfo getStoredSMS(int index) throws ChannelException, InterruptedException;

  abstract List<SMSInfo> getStoredSMSList(SMSStatus status) throws ChannelException, InterruptedException;

  abstract void deleteStoredSMS(int index) throws ChannelException, InterruptedException;

  abstract void enableRouteSMSDirectlyToTE() throws ChannelException, InterruptedException;

  abstract void confirmSuccessfulSMSReceipt() throws ChannelException, InterruptedException;

}
