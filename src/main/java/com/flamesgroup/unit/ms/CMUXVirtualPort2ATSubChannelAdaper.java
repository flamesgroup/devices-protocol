/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;
import com.flamesgroup.device.gsmb.IATSubChannelHandler;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPort;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPortHandler;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class CMUXVirtualPort2ATSubChannelAdaper implements IATSubChannel {

  private final ICMUXVirtualPort cmuxVirtualPort;

  private final Charset atCharset = Charset.forName("ISO-8859-1");

  public CMUXVirtualPort2ATSubChannelAdaper(final ICMUXVirtualPort cmuxVirtualPort) {
    this.cmuxVirtualPort = cmuxVirtualPort;
  }

  @Override
  public void start(final IATSubChannelHandler atSubChannelHandler) throws ChannelException, InterruptedException {
    cmuxVirtualPort.open(new ICMUXVirtualPortHandler() {

      @Override
      public void handleData(final ByteBuffer data) {
        atSubChannelHandler.handleATData(atCharset.decode(data).toString());
      }

    });
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    cmuxVirtualPort.close();
  }

  @Override
  public void writeATData(final String atData) throws ChannelException, InterruptedException {
    cmuxVirtualPort.sendData(atCharset.encode(atData));
  }

}
