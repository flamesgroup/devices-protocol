/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import static com.flamesgroup.unit.ms.ExtendedErrorReportHelper.TELIT_EXTENDED_NETWORK_ERROR_REPORT_CAUSES_PATH;
import static com.flamesgroup.unit.ms.ExtendedErrorReportHelper.getExtendedErrorReportCauses;

import com.flamesgroup.unit.IExtendedNetworkErrorReport;

import java.util.Map;

public final class TelitExtendedNetworkErrorReport implements IExtendedNetworkErrorReport {

  private static final long serialVersionUID = -970803146872642582L;

  private final static Map<Integer, String> causes = getExtendedErrorReportCauses(TELIT_EXTENDED_NETWORK_ERROR_REPORT_CAUSES_PATH);

  private final int code;

  private transient String causeDescription;
  private transient String toS;

  public TelitExtendedNetworkErrorReport(final int code) {
    this.code = code;
  }

  @Override
  public int getMobilityOrSessionManagementCause() {
    return code;
  }

  @Override
  public String getMobilityOrSessionManagementCauseDescription() {
    if (causeDescription == null) {
      causeDescription = String.format("%s: code: [%d]", causes.get(code), code);
    }
    return causeDescription;
  }

  @Override
  public String toString() {
    if (toS == null) {
      toS = String.format("%s@%x:[%d]", getClass().getSimpleName(), hashCode(), code);
    }
    return toS;
  }

}
