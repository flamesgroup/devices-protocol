/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhBCCHLOCKWrite;
import com.flamesgroup.unit.ICellLocker;

import java.util.Objects;

public class GSMTelitCellLocker implements ICellLocker {

  private final GSMTelitMobileStationUnit gsmTelitMobileStationUnit;

  GSMTelitCellLocker(final GSMTelitMobileStationUnit gsmTelitMobileStationUnit) {
    Objects.requireNonNull(gsmTelitMobileStationUnit, "gsmTelitMobileStationUnit mustn't be null");
    this.gsmTelitMobileStationUnit = gsmTelitMobileStationUnit;
  }

  @Override
  public void lock(final int arfcn) throws ChannelException, InterruptedException {
    gsmTelitMobileStationUnit.execute(new AThhBCCHLOCKWrite(arfcn));
  }

  @Override
  public void unlock() throws ChannelException, InterruptedException {
    gsmTelitMobileStationUnit.execute(new AThhBCCHLOCKWrite());
  }

}
