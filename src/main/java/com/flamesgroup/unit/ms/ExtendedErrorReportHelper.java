/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class ExtendedErrorReportHelper {

  public static final String SIEMENS_EXTENDED_CALL_ERROR_REPORT_CAUSES_PATH = "/siemens.extended.call.error.report.causes.properties";
  public static final String SIEMENS_EXTENDED_NETWORK_ERROR_REPORT_CAUSES_PATH = "/siemens.extended.network.error.report.causes.properties";

  public static final String TELIT_EXTENDED_CALL_ERROR_REPORT_CAUSES_PATH = "/telit.extended.call.error.report.causes.properties";
  public static final String TELIT_EXTENDED_NETWORK_ERROR_REPORT_CAUSES_PATH = "/telit.extended.network.error.report.causes.properties";

  private ExtendedErrorReportHelper() {
  }

  public static Map<Integer, String> getExtendedErrorReportCauses(final String path) {
    InputStream inputStream = ExtendedErrorReportHelper.class.getResourceAsStream(path);
    if (inputStream == null) {
      throw new IllegalStateException("Can't find extended error report [" + path + "] in classpath");
    }

    Properties properties = new Properties();
    try {
      properties.load(inputStream);
    } catch (IOException e) {
      throw new IllegalStateException("Can't load extended error report [" + path + "]");
    }

    Map<Integer, String> extendedErrorReport = new HashMap<>();
    for (Map.Entry<Object, Object> entry : properties.entrySet()) {
      extendedErrorReport.put(Integer.valueOf((String) entry.getKey()), (String) entry.getValue());
    }
    return Collections.unmodifiableMap(extendedErrorReport);
  }

}
