/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGDWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGLWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGLWriteResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGRWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGRWriteResult;
import com.flamesgroup.device.gsmb.atengine.at.SMSInfo;
import com.flamesgroup.device.gsmb.atengine.at.SMSStatus;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSMOVWrite;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhSMOVHandler;

import java.util.List;
import java.util.Objects;

class GSMTelitMobileIncomingSMSHandler extends BaseMobileIncomingSMSHandler implements IURChhSMOVHandler {

  private final GSMTelitMobileStationUnit gsmTelitMobileStationUnit;

  GSMTelitMobileIncomingSMSHandler(final GSMTelitMobileStationUnit gsmTelitMobileStationUnit) {
    Objects.requireNonNull(gsmTelitMobileStationUnit, "gsmTelitMobileStationUnit mustn't be null");

    this.gsmTelitMobileStationUnit = gsmTelitMobileStationUnit;
  }

  @Override
  public void handleSMSOverflow(final String memory) {
    enqueueIncomingSMSOverflow();
  }

  @Override
  void enableSMSOverflowNotification() throws ChannelException, InterruptedException {
    AThhSMOVWrite athhSMOVWrite = new AThhSMOVWrite(true);
    gsmTelitMobileStationUnit.execute(athhSMOVWrite);
  }

  @Override
  SMSInfo getStoredSMS(final int index) throws ChannelException, InterruptedException {
    ATpsCMGRWrite atpsCMGRWrite = new ATpsCMGRWrite(index);
    ATpsCMGRWriteResult atpsCMGRWriteResult = gsmTelitMobileStationUnit.execute(atpsCMGRWrite);
    return new SMSInfo(index, atpsCMGRWriteResult.getStatus(), atpsCMGRWriteResult.getAlpha(), atpsCMGRWriteResult.getLength(), atpsCMGRWriteResult.getPDU());
  }

  @Override
  List<SMSInfo> getStoredSMSList(final SMSStatus status) throws ChannelException, InterruptedException {
    ATpsCMGLWrite atpsCMGLWrite = new ATpsCMGLWrite(status);
    ATpsCMGLWriteResult atpsCMGLWriteResult = gsmTelitMobileStationUnit.execute(atpsCMGLWrite);
    return atpsCMGLWriteResult.getSmsList();
  }

  @Override
  void deleteStoredSMS(final int index) throws ChannelException, InterruptedException {
    ATpsCMGDWrite atCMGDWrite = new ATpsCMGDWrite(index);
    gsmTelitMobileStationUnit.execute(atCMGDWrite);
  }

  @Override
  void enableRouteSMSDirectlyToTE() throws ChannelException, InterruptedException {
  }

  @Override
  void confirmSuccessfulSMSReceipt() throws ChannelException, InterruptedException {
  }

}
