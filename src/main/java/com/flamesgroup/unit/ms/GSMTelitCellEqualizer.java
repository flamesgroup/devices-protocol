/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhEQCELLRead;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhEQCELLReadResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhEQCELLWrite;
import com.flamesgroup.unit.CellAdjustment;
import com.flamesgroup.unit.ICellEqualizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GSMTelitCellEqualizer implements ICellEqualizer {

  private final GSMTelitMobileStationUnit gsmTelitMobileStationUnit;

  GSMTelitCellEqualizer(final GSMTelitMobileStationUnit gsmTelitMobileStationUnit) {
    Objects.requireNonNull(gsmTelitMobileStationUnit, "gsmTelitMobileStationUnit mustn't be null");
    this.gsmTelitMobileStationUnit = gsmTelitMobileStationUnit;
  }

  @Override
  public void write(final CellAdjustment cellAdjustment) throws ChannelException, InterruptedException {
    gsmTelitMobileStationUnit.execute(new AThhEQCELLWrite(cellAdjustment.getArfcn(), cellAdjustment.getRxLevAdjust()));
  }

  @Override
  public void clear() throws ChannelException, InterruptedException {
    gsmTelitMobileStationUnit.execute(new AThhEQCELLWrite());
  }

  @Override
  public List<CellAdjustment> read() throws ChannelException, InterruptedException {
    AThhEQCELLReadResult result = gsmTelitMobileStationUnit.execute(new AThhEQCELLRead());
    return result.getEqCellList().stream().map(eqCellInfo -> new CellAdjustment(eqCellInfo.getArfcn(), eqCellInfo.getRxLevAdjust())).collect(Collectors.toCollection(ArrayList::new));
  }

}
