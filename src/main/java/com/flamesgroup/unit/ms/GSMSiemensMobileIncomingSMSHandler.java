/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGDWrite;
import com.flamesgroup.device.gsmb.atengine.at.SMSInfo;
import com.flamesgroup.device.gsmb.atengine.at.SMSStatus;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMGLWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMGLWriteResult;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMGOWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMGRWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMGRWriteResult;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCNMAExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCNMIWrite;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSMGO;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.IURCctSMGOHandler;

import java.util.List;
import java.util.Objects;

final class GSMSiemensMobileIncomingSMSHandler extends BaseMobileIncomingSMSHandler implements IURCctSMGOHandler {

  private final GSMSiemensMobileStationUnit gsmSiemensMobileStationUnit;

  GSMSiemensMobileIncomingSMSHandler(final GSMSiemensMobileStationUnit gsmSiemensMobileStationUnit) {
    Objects.requireNonNull(gsmSiemensMobileStationUnit, "gsmSiemensMobileStationUnit mustn't be null");

    this.gsmSiemensMobileStationUnit = gsmSiemensMobileStationUnit;
  }

  @Override
  public void handleSMSOverflow(final PctSMGO.SMSOverflowStatus status) {
    if (status.getValue() > 0) {
      enqueueIncomingSMSOverflow();
    }
  }

  @Override
  void enableSMSOverflowNotification() throws ChannelException, InterruptedException {
    ATctSMGOWrite atSMGOWrite = new ATctSMGOWrite(true);
    gsmSiemensMobileStationUnit.execute(atSMGOWrite);
  }

  @Override
  SMSInfo getStoredSMS(final int index) throws ChannelException, InterruptedException {
    ATctSMGRWrite atSMGRWrite = new ATctSMGRWrite(index);
    ATctSMGRWriteResult atSMGRWriteResult = gsmSiemensMobileStationUnit.execute(atSMGRWrite);
    return new SMSInfo(index, atSMGRWriteResult.getStatus(), atSMGRWriteResult.getAlpha(), atSMGRWriteResult.getLength(), atSMGRWriteResult.getPDU());
  }

  @Override
  List<SMSInfo> getStoredSMSList(final SMSStatus status) throws ChannelException, InterruptedException {
    ATctSMGLWrite atSMGLWrite = new ATctSMGLWrite(SMSStatus.ALL);
    ATctSMGLWriteResult atctSMGLWriteResult = gsmSiemensMobileStationUnit.execute(atSMGLWrite);
    return atctSMGLWriteResult.getSmsList();
  }

  @Override
  void deleteStoredSMS(final int index) throws ChannelException, InterruptedException {
    ATpsCMGDWrite atCMGDWrite = new ATpsCMGDWrite(index);
    gsmSiemensMobileStationUnit.execute(atCMGDWrite);
  }

  @Override
  void enableRouteSMSDirectlyToTE() throws ChannelException, InterruptedException {
    gsmSiemensMobileStationUnit.execute(new ATpsCNMIWrite(PpsCNMI.UrcBufferingOption.BUFFER_URCS_IN_TA_AND_FLUSH_TO_TE,
        PpsCNMI.SMSDeliveringOption.SMS_DELIVERS_ROUTED_DIRECTLY_TO_TE,
        PpsCNMI.BroadcastReportingOption.NO_CBM_INDICATIONS_ROUTED_TO_TE,
        PpsCNMI.SMSStatusReportingOption.SMS_STATUS_REPORT_ROUTED_TO_TE,
        PpsCNMI.BufferedURCHandlingMethod.CLEAR));
  }

  @Override
  void confirmSuccessfulSMSReceipt() throws ChannelException, InterruptedException {
    gsmSiemensMobileStationUnit.execute(new ATpsCNMAExec());
  }

}
