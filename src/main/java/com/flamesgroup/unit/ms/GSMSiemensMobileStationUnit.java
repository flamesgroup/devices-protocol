/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.gsmb.IAudioChannel;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.ISCEmulatorChannel;
import com.flamesgroup.device.gsmb.atengine.ATEngine;
import com.flamesgroup.device.gsmb.atengine.ATEngineException;
import com.flamesgroup.device.gsmb.atengine.ATEngineHelper;
import com.flamesgroup.device.gsmb.atengine.at.ATEExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCCWAWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCINDWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCLCCExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCLCCExecResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMEEWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMERWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGFWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCOPSWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCREGWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCSSNWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMIExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMIExecResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMMExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMMExecResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMRExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMRExecResult;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSAICWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSM20Write;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMONCExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMONCExecResult;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMSOExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSNFDExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSNFIWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSNFOWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSNFSWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSNFWExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSSYNCWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCEERExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCEERExecResult;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCLVLWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCNMIWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCSCSWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsCSMSWrite;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATpsVTSWrite;
import com.flamesgroup.device.gsmb.atengine.http.IHTTPSession;
import com.flamesgroup.device.gsmb.atengine.param.PpsCCWA;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMEE;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMER;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMGF;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSAIC;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSNFS;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSSYNC;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PpsCSCS;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PpsCSMS;
import com.flamesgroup.device.gsmb.atengine.urc.IURCctSHUTDOWNHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCctSYSSTARTHandler;
import com.flamesgroup.device.gsmb.atengine.urc.URCctSHUTDOWN;
import com.flamesgroup.device.gsmb.atengine.urc.URCctSYSSTART;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.IURCEXITHandler;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCEXIT;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCctSMGO;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCMEERRORPhoneBusy;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCMEERRORSSNotExecuted;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCMEERRORServiceOptionNotSupported;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCMEERRORUnknown;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCUSD;
import com.flamesgroup.device.gsmb.atengine.ussd.IUSSDSession;
import com.flamesgroup.device.gsmb.atengine.ussd.siemens.USSDSession;
import com.flamesgroup.device.gsmb.atengine.ussd.siemens.USSDSessionStarHashWrapper;
import com.flamesgroup.device.sc.SCSubChannelsJoining;
import com.flamesgroup.device.simb.ISCReaderChannel;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.ICellEqualizer;
import com.flamesgroup.unit.ICellLocker;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IExtendedNetworkErrorReport;
import com.flamesgroup.unit.IMobileStationUnitInternalErrorHandler;
import com.flamesgroup.unit.MobileStationUnitException;
import com.flamesgroup.unit.MobileStationUnitExitException;
import com.flamesgroup.unit.MobileStationUnitStartTimeoutException;
import com.flamesgroup.unit.MobileStationUnitStopTimeoutException;
import com.flamesgroup.unit.ms.event.ActiveCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.AlertingCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.HeldCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.IncomingCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.MobileStationEvent;
import com.flamesgroup.unit.ms.event.WaitingCallMobileStationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class GSMSiemensMobileStationUnit extends BaseMobileStationUnit {

  private static final Logger logger = LoggerFactory.getLogger(GSMSiemensMobileStationUnit.class);

  private static final long SYSSTART_OR_EXIT_TIMEOUT = 5 * 60000;
  private static final long SHUTDOWN_TIMEOUT = 60000;

  private final ISCEmulatorChannel scEmulatorChannel;
  private final ISCReaderChannel scReaderChannel;

  private final Lock atEngineLock = new ReentrantLock(true);

  private final Semaphore sysstartOrExitSemaphore = new Semaphore(0);
  private final Semaphore shutdownSemaphore = new Semaphore(0);

  private String exitCode;

  private ATpsCLCCExecResult.CallInfo callInfoFirstPrevious;

  public GSMSiemensMobileStationUnit(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCEmulatorChannel scEmulatorChannel, final ISCReaderChannel scReaderChannel,
      final IMobileStationUnitInternalErrorHandler internalErrorHandler, final String description) {
    super(gsmChannel, audioChannel, internalErrorHandler, description);
    Objects.requireNonNull(scEmulatorChannel, "scEmulatorChannel mustn't be null");
    Objects.requireNonNull(scReaderChannel, "scReaderChannel mustn't be null");

    this.scEmulatorChannel = scEmulatorChannel;
    this.scReaderChannel = scReaderChannel;

    atEngine = new ATEngine(this.gsmChannel.getATSubChannel(), description);

    USSDSession siemensUSSDSession = new USSDSession(atEngine);
    ussdSession = new USSDSessionWithAtEngineLockKeepingWrapper(new USSDSessionStarHashWrapper(siemensUSSDSession));

    GSMSiemensMobileIncomingSMSHandler gsmSiemensMobileIncomingSMSHandler = new GSMSiemensMobileIncomingSMSHandler(this);
    mobileIncomingSMSHandler = gsmSiemensMobileIncomingSMSHandler;

    atEngineURCs.add(new URCEXIT(new URCEXITHandler()));
    atEngineURCs.add(new URCctSYSSTART(new SYSSTARTHandler()));
    atEngineURCs.add(new URCctSHUTDOWN(new SHUTDOWNHandler()));
    atEngineURCs.add(new URCctSMGO(gsmSiemensMobileIncomingSMSHandler));
    atEngineURCs.add(new URCpsCUSD(siemensUSSDSession));
    atEngineURCs.add(new URCpsCMEERRORUnknown(siemensUSSDSession));
    atEngineURCs.add(new URCpsCMEERRORSSNotExecuted(siemensUSSDSession));
    atEngineURCs.add(new URCpsCMEERRORServiceOptionNotSupported(siemensUSSDSession));
    atEngineURCs.add(new URCpsCMEERRORPhoneBusy(siemensUSSDSession));

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}]/[{}]/[{}]/[{}]", toString(), this.gsmChannel, this.audioChannel, this.scEmulatorChannel, this.scReaderChannel);
    }
  }

  public GSMSiemensMobileStationUnit(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCEmulatorChannel scEmulatorChannel, final ISCReaderChannel scReaderChannel,
      final String description) {
    this(gsmChannel, audioChannel, scEmulatorChannel, scReaderChannel, null, description);
  }

  @Override
  public ICellEqualizer getCellEqualizer() {
    throw new UnsupportedOperationException();
  }

  @Override
  public ICellLocker getCellLocker() {
    throw new UnsupportedOperationException();
  }

  @Override
  public IHTTPSession getHTTPSession() {
    throw new UnsupportedOperationException();
  }

  @Override
  IChannel[] getAllChannels() {
    return new IChannel[] {gsmChannel, audioChannel, scEmulatorChannel, scReaderChannel};
  }

  @Override
  protected MobileStationEvent pollMobileStationEvent() {
    if (mobileStationEvents.isEmpty()) {
      ATpsCLCCExecResult atpsCLCCExecResult = processReadActiveCallList();
      if (atpsCLCCExecResult != null) {
        List<ATpsCLCCExecResult.CallInfo> callInfoList = atpsCLCCExecResult.getCallList();
        if (callInfoList.isEmpty()) {
          callInfoFirstPrevious = null;
        } else {
          int callId = 0;
          ATpsCLCCExecResult.CallInfo callInfoFirst = callInfoList.get(callId);

          if (callInfoFirst.equals(callInfoFirstPrevious)) {
            // same call
          } else if (callInfoFirstPrevious == null ||
              (callInfoFirst.isOriginated() == callInfoFirstPrevious.isOriginated() &&
                  Objects.equals(callInfoFirst.getNumber(), callInfoFirstPrevious.getNumber()))) {
            // new call or call changed state
            callInfoFirstPrevious = callInfoFirst;

            MobileStationEvent mobileStationEvent;
            switch (callInfoFirst.getState()) {
              case ACTIVE:
                mobileStationEvent = new ActiveCallMobileStationEvent(callId);
                break;
              case HELD:
                mobileStationEvent = new HeldCallMobileStationEvent(callId);
                break;
              case ALERTING:
                mobileStationEvent = new AlertingCallMobileStationEvent(callId);
                break;
              case INCOMING:
                mobileStationEvent = new IncomingCallMobileStationEvent(callId, callInfoFirst.getNumber());
                break;
              case WAITING:
                mobileStationEvent = new WaitingCallMobileStationEvent(callId);
                break;
              default:
                mobileStationEvent = null;
            }
            if (mobileStationEvent != null) {
              if (!mobileStationEvents.offer(mobileStationEvent)) {
                throw new AssertionError();
              }
            }
          }
        }
      }
    }
    return mobileStationEvents.poll();
  }

  @Override
  void preAtEngineStart() throws ChannelException, InterruptedException {
    scSubChannelsJoining = new SCSubChannelsJoining(scReaderChannel.getSCReaderSubChannel(), scEmulatorChannel.getSCEmulatorSubChannel(), internalErrorHandler, options.isDisablePhase2pSupport());
  }

  @Override
  void waitForMobileStationStart() throws ChannelException, InterruptedException {
    waitForSysstartOrExit();
  }

  @Override
  void init() throws ChannelException, InterruptedException {
    execute(new ATEExec(false));

    ATpsGMIExecResult atpsGMIExecResult = execute(new ATpsGMIExec());
    ATpsGMMExecResult atpsGMMExecResult = execute(new ATpsGMMExec());
    ATpsGMRExecResult atpsGMRExecResult = execute(new ATpsGMRExec());
    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - init [{}]/[{}]/[{}]", this,
          atpsGMIExecResult.getManufacturer(), atpsGMMExecResult.getModel(), atpsGMRExecResult.getRevision());
    }

    //configure audio
    execute(new ATctSSYNCWrite(PctSSYNC.Mode.LED));
    execute(new ATctSNFDExec());
    switch (atpsGMMExecResult.getModel()) {
      case TC35i: {
        execute(new ATctSNFSWrite(PctSNFS.AudioMode.AUDIO_MODE5));
        execute(new ATctSAICWrite(PctSAIC.IO.ANALOG_IO, PctSAIC.Microphone.MICROPHONE1, PctSAIC.EarpieceAmplifier.AMPLIFIER1));
        break;
      }
      case MC55i: {
        execute(new ATctSNFSWrite(PctSNFS.AudioMode.AUDIO_MODE6));
        execute(new ATctSAICWrite(PctSAIC.IO.DIGITAL_IO));
        break;
      }
      default: {
        throw new MobileStationUnitException("Can't init gsm mobile station unit - unsupported model [" + atpsGMMExecResult.getModel() + "]");
      }
    }
    execute(new ATpsCLVLWrite(1));
    execute(new ATctSNFOWrite(0, 16384, 16384, 16384, 16384, 16384, 0, 0));
    execute(new ATctSNFIWrite(1, 32767));
    execute(new ATctSNFWExec());

    if (options.getOperatorCode() != null) {
      ATEngineHelper.execute(this, this::execute, new ATpsCOPSWrite(PpsCOPS.Mode.MANUAL_OPERATOR_SELECTION, PpsCOPS.Format.NUMERIC, options.getOperatorCode()), 5, 3000);
      execute(new ATpsCOPSWrite(PpsCOPS.Mode.SET_ONLY_FORMAT, PpsCOPS.Format.LONG_ALPHANUMERIC));
    } else {
      execute(new ATpsCOPSWrite(PpsCOPS.Mode.AUTOMATIC));
    }

    execute(new ATctSM20Write(PctSM20.CallMode.COMPATIBILITY_TO_SIEMENS));
    execute(new ATpsCMEEWrite(PpsCMEE.ErrorMode.ENABLE_WITH_STRING_VALUES));
    execute(new ATpsCMGFWrite(PpsCMGF.SMSFormat.PDU));
    execute(new ATpsCSCSWrite(PpsCSCS.Charset.UCS2));
    execute(new ATpsCINDWrite(true, true));
    execute(new ATpsCMERWrite(PpsCMER.Mode.BUFFER_CIEV, Boolean.TRUE, Boolean.TRUE));
  }

  @Override
  void deinit() throws ChannelException, InterruptedException {
    execute(new ATpsCNMIWrite(PpsCNMI.UrcBufferingOption.BUFFER_URCS_IN_TA_AND_FLUSH_TO_TE,
        PpsCNMI.SMSDeliveringOption.NO_SMS_DELIVER_ROUTED_TO_TA,
        PpsCNMI.BroadcastReportingOption.NO_CBM_INDICATIONS_ROUTED_TO_TE,
        PpsCNMI.SMSStatusReportingOption.NO_SMS_STATUS_REPORT_ROUTED_TO_TE,
        PpsCNMI.BufferedURCHandlingMethod.CLEAR));
    execute(new ATpsCSSNWrite(false));
    execute(new ATpsCREGWrite(PpsCREG.Mode.DISABLE_CREG_URC));
    execute(new ATpsCMERWrite(PpsCMER.Mode.DISCARD_CIEV, Boolean.FALSE, Boolean.TRUE));
    execute(new ATpsCINDWrite(false, false));

    execute(new ATctSMSOExec());
  }

  @Override
  void waitForMobileStationStop() throws ChannelException, InterruptedException {
    waitForShutdown();
  }

  @Override
  void postAtEngineStop() throws ChannelException, InterruptedException {
    scSubChannelsJoining = null;
  }

  @Override
  void postInit() throws ChannelException, InterruptedException {
    unitLock.lock();
    try {
      execute(new ATpsCREGWrite(PpsCREG.Mode.ENABLE_CREG_URC));
      execute(new ATpsCSSNWrite(true));
      execute(new ATpsCSMSWrite(PpsCSMS.MessagingService.COMPATIBLE_WITH_GSM_07_05_PHASE_2_PLUS_VERSION));
      execute(new ATpsCNMIWrite(PpsCNMI.UrcBufferingOption.BUFFER_URCS_IN_TA_AND_FLUSH_TO_TE,
          PpsCNMI.SMSDeliveringOption.IF_SMS_DELIVER_STORED_IN_META_IOML_ROUTED_TO_TE,
          PpsCNMI.BroadcastReportingOption.NO_CBM_INDICATIONS_ROUTED_TO_TE,
          PpsCNMI.SMSStatusReportingOption.IF_SMS_STATUS_REPORT_ROUTED_INTO_META_IOML_ROUTED_TO_TE,
          PpsCNMI.BufferedURCHandlingMethod.CLEAR));

      if (options.isSuppressIncomingCallSignal()) {
        try {
          execute(new ATpsCCWAWrite(null, PpsCCWA.Mode.DISABLE_CALL_WAITING, EnumSet.of(PpsCCWA.Class.VOICE)));
        } catch (ChannelException e) {
          logger.warn("[{}] - can't execute ATpsCCWAWrite command", this, e);
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  IExtendedCallErrorReport executeReadExtendedCallErrorReport() throws ChannelException, InterruptedException {
    ATpsCEERExecResult atpsCEERExecResult = execute(new ATpsCEERExec());
    return new SiemensExtendedCallErrorReport(atpsCEERExecResult.getLocationID(), atpsCEERExecResult.getReason(), atpsCEERExecResult.getSSRelease());
  }

  @Override
  IExtendedNetworkErrorReport executeReadExtendedNetworkErrorReport() throws ChannelException, InterruptedException {
    ATpsCEERExecResult atpsCEERExecResult = execute(new ATpsCEERExec());
    return new SiemensExtendedNetworkErrorReport(atpsCEERExecResult.getLocationID(), atpsCEERExecResult.getReason(), atpsCEERExecResult.getSSRelease());
  }

  @Override
  void executeDTMF(final String dtmfString) throws ChannelException, InterruptedException {
    execute(new ATpsVTSWrite(dtmfString));
  }

  @Override
  void executeDTMF(final char dtmf, final int duration) throws ChannelException, InterruptedException {
    execute(new ATpsVTSWrite(dtmf, duration));
  }

  @Override
  CellInfo[] executeReadCellMonitoring() throws ChannelException, InterruptedException {
    ATctSMONCExecResult atctSMONCExecResult = execute(new ATctSMONCExec());
    if (logger.isTraceEnabled()) {
      StringBuilder sb = new StringBuilder("^SMONC: ").append(System.lineSeparator());
      sb.append("<<").append(System.lineSeparator());
      for (ATctSMONCExecResult.CellInfo cellInfo : atctSMONCExecResult.getCellList()) {
        sb.append(cellInfo.getMcc()).append("/")
            .append(cellInfo.getMnc()).append("/")
            .append(cellInfo.getLac()).append("/")
            .append(cellInfo.getCell()).append("/")
            .append(cellInfo.getBsic()).append("/")
            .append(cellInfo.getChann()).append("/")
            .append(cellInfo.getRssi()).append("/")
            .append(cellInfo.getC1()).append("/")
            .append(cellInfo.getC2()).append(System.lineSeparator());
      }
      sb.append(">>").append(System.lineSeparator());
      logger.trace("[{}] - {}", this, sb.toString());
    }
    return convertToCellInfo(atctSMONCExecResult.getCellList());
  }

  private CellInfo[] convertToCellInfo(final List<ATctSMONCExecResult.CellInfo> siemensCellInfos) {
    CellInfo[] cellInfos = new CellInfo[siemensCellInfos.size()];
    int i = 0;
    for (ATctSMONCExecResult.CellInfo cellInfo : siemensCellInfos) {
      cellInfos[i++] = new CellInfo(cellInfo.getBsic(), cellInfo.getLac(), cellInfo.getCell(), cellInfo.getChann(), rssiToDBm(cellInfo.getRssi()), cellInfo.getC1(), cellInfo.getC2());
    }
    return cellInfos;
  }

  private ATpsCLCCExecResult executeReadActiveCallList() throws ChannelException, InterruptedException {
    return execute(new ATpsCLCCExec());
  }

  private ATpsCLCCExecResult processReadActiveCallList() {
    try {
      return executeReadActiveCallList();
    } catch (ChannelException ex) {
      logger.warn("[{}] - can't get list of current calls", this, ex);
      internalErrorHandler.handleATEngineExecutionError(ex);
    } catch (InterruptedException ex) {
      throw new AssertionError(ex);
    }
    return null;
  }

  private int rssiToDBm(final int rssi) {
    return -113 + rssi * 2;
  }

  private void waitForSysstartOrExit() throws ChannelException, InterruptedException {
    if (!sysstartOrExitSemaphore.tryAcquire(SYSSTART_OR_EXIT_TIMEOUT, TimeUnit.MILLISECONDS)) {
      throw new MobileStationUnitStartTimeoutException("[" + this + "] - SYSSTART waiting timeout elapsed");
    }
    if (exitCode != null) {
      String localExitCode = exitCode;
      exitCode = null;
      throw new MobileStationUnitExitException(localExitCode);
    }
  }

  private void notifyAboutSysstartOrExit() {
    sysstartOrExitSemaphore.release();
  }

  private void waitForShutdown() throws ChannelException, InterruptedException {
    if (!shutdownSemaphore.tryAcquire(SHUTDOWN_TIMEOUT, TimeUnit.MILLISECONDS)) {
      throw new MobileStationUnitStopTimeoutException("[" + this + "] - SHUTDOWN waiting timeout elapsed");
    }
  }

  private void notifyAboutShutdown() {
    shutdownSemaphore.release();
  }

  // internal helpers methods
  @Override
  <T extends IATResult> T execute(final IAT<T> command) throws ChannelException, InterruptedException {
    atEngineLock.lock();
    try {
      return super.execute(command);
    } finally {
      atEngineLock.unlock();
    }
  }

  // internal handler's classes

  private class URCEXITHandler implements IURCEXITHandler {

    @Override
    public void handleModuleExit(final String exitCode) {
      logger.trace("[{}] - gsm mobile station received an EXIT with code [{}]", GSMSiemensMobileStationUnit.this, exitCode);
      GSMSiemensMobileStationUnit.this.exitCode = exitCode;
      notifyAboutSysstartOrExit();
    }

  }

  private class SYSSTARTHandler implements IURCctSYSSTARTHandler {

    @Override
    public void handleSysstart() {
      logger.trace("[{}] - gsm mobile station unit has been started", GSMSiemensMobileStationUnit.this);
      notifyAboutSysstartOrExit();
    }

  }

  private class SHUTDOWNHandler implements IURCctSHUTDOWNHandler {

    @Override
    public void handleShutdown() {
      logger.trace("[{}] - gsm mobile station unit has been stopped", GSMSiemensMobileStationUnit.this);
      notifyAboutShutdown();
    }

  }

  // internal USSD class

  /*
   This class in general and atEngineLock in particular are necessary because of odd implementation
   of USSD session mechanism in Siemens modules, when during execution of USSD session module can't
   execute any other AT commands.
   */
  private class USSDSessionWithAtEngineLockKeepingWrapper implements IUSSDSession {

    private final IUSSDSession ussdSession;

    public USSDSessionWithAtEngineLockKeepingWrapper(final IUSSDSession ussdSession) {
      this.ussdSession = ussdSession;
    }

    @Override
    public void open(final String ussdString) throws ChannelException, InterruptedException {
      atEngineLock.lock();
      try {
        ussdSession.open(ussdString);
      } catch (ChannelException | InterruptedException e) {
        atEngineLock.unlock();
        throw e;
      }
    }

    @Override
    public String readRequest() throws ATEngineException, InterruptedException {
      return ussdSession.readRequest();
    }

    @Override
    public void writeResponse(final String response) throws ChannelException, InterruptedException {
      ussdSession.writeResponse(response);
    }

    @Override
    public void close() throws ChannelException, InterruptedException {
      try {
        ussdSession.close();
      } finally {
        atEngineLock.unlock();
      }
    }

  }

}
