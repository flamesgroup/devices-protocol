/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.ATDExec;
import com.flamesgroup.unit.IMobileOriginatingCall;
import com.flamesgroup.unit.IMobileOriginatingCallHandler;
import com.flamesgroup.unit.PhoneNumber;

final class OriginatedCall extends Call implements IMobileOriginatingCall {

  private boolean firstHandleActiveOccur;

  public OriginatedCall(final BaseMobileStationUnit mobileStationUnit) {
    super(mobileStationUnit);
  }

  public void dial(final PhoneNumber number, final IMobileOriginatingCallHandler originatedCallHandler) throws ChannelException, InterruptedException {
    callLock.lock();
    try {
      checkCurrentStateWithExpectedAndExceptionOnFail(State.DIALING);

      callHandler = originatedCallHandler;
      mobileStationUnit.execute(new ATDExec(number.getValue()));
      setState(State.DIALING);
    } finally {
      callLock.unlock();
    }
  }

  public void handleAlerting() {
    IMobileOriginatingCallHandler mobileOriginatingCallHandlerLocal;
    callLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(State.ALERTING)) {
        return;
      }

      mobileOriginatingCallHandlerLocal = (IMobileOriginatingCallHandler) callHandler;

      setState(State.ALERTING);
    } finally {
      callLock.unlock();
    }

    mobileOriginatingCallHandlerLocal.handleAlerting();
  }

  @Override
  public void handleActive() {
    IMobileOriginatingCallHandler mobileOriginatingCallHandlerLocal;
    callLock.lock();
    try {
      if (firstHandleActiveOccur) {
        mobileOriginatingCallHandlerLocal = null;
      } else {
        if (!checkCurrentStateWithExpectedAndWarnOnFail(State.ACTIVE)) {
          return;
        }

        mobileOriginatingCallHandlerLocal = (IMobileOriginatingCallHandler) callHandler;

        firstHandleActiveOccur = true;
        setState(State.ACTIVE);
      }
    } finally {
      callLock.unlock();
    }

    if (mobileOriginatingCallHandlerLocal == null) {
      super.handleActive();
    } else {
      mobileOriginatingCallHandlerLocal.handleAnswer();
    }
  }

}
