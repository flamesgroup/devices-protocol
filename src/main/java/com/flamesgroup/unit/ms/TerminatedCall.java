/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.ATAExec;
import com.flamesgroup.unit.IMobileTerminatingCall;
import com.flamesgroup.unit.IMobileTerminatingCallHandler;

final class TerminatedCall extends Call implements IMobileTerminatingCall {

  private boolean firstHandleHeldOccur;

  public TerminatedCall(final BaseMobileStationUnit mobileStationUnit) {
    super(mobileStationUnit);
  }

  public void accept(final IMobileTerminatingCallHandler terminatedCallHandler) {
    callLock.lock();
    try {
      checkCurrentStateWithExpectedAndExceptionOnFail(State.ACCEPTED);

      callHandler = terminatedCallHandler;
      setState(State.ACCEPTED);
    } finally {
      callLock.unlock();
    }
  }

  @Override
  public void answer() throws ChannelException, InterruptedException {
    callLock.lock();
    try {
      checkCurrentStateWithExpectedAndExceptionOnFail(State.ACTIVE);

      mobileStationUnit.execute(new ATAExec()); // execute before state change to prevent changing in case of AT execution problem

      setState(State.ACTIVE);
    } finally {
      callLock.unlock();
    }
  }

  @Override
  public void handleHeld() {
    callLock.lock();
    try {
      firstHandleHeldOccur = true;
    } finally {
      callLock.unlock();
    }

    super.handleHeld();
  }

  @Override
  public void handleActive() {
    boolean firstHandleHeldOccurLocal;
    callLock.lock();
    try {
      firstHandleHeldOccurLocal = firstHandleHeldOccur;
    } finally {
      callLock.unlock();
    }

    if (firstHandleHeldOccurLocal) {
      super.handleActive();
    }
  }

}
