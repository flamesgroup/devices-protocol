/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.RawATSubChannel2ATSubChannelStackedAdapter;
import com.flamesgroup.device.gsmb.Telit;
import com.flamesgroup.device.gsmb.atengine.ATEngine;
import com.flamesgroup.device.gsmb.atengine.ATEngineHelper;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATEExec;
import com.flamesgroup.device.gsmb.atengine.at.ATVExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMEEWrite;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATadDExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATadKExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhBCCHLOCKWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCPUMODEWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURBaseExecResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURVCExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURVEXTWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURVNLFWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSLEDWrite;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMEE;
import com.flamesgroup.device.gsmb.atengine.param.telit.PadD;
import com.flamesgroup.device.gsmb.atengine.param.telit.PadK;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhCPUMODE;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhCSURVEXT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSLED;
import com.flamesgroup.unit.BaseUnit;
import com.flamesgroup.unit.NetworkSurveyInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GSMTelitEasyScanUnit extends BaseUnit {

  private static final Logger logger = LoggerFactory.getLogger(GSMTelitEasyScanUnit.class);

  private final IGSMChannel gsmChannel;
  private final IATEngine atEngine;

  public GSMTelitEasyScanUnit(final IGSMChannel gsmChannel, final String description) {
    Objects.requireNonNull(gsmChannel, "gsmChannel mustn't be null");

    this.gsmChannel = gsmChannel;

    RawATSubChannel2ATSubChannelStackedAdapter rawATSubChannel = new RawATSubChannel2ATSubChannelStackedAdapter(this.gsmChannel.getRawATSubChannel());
    atEngine = new ATEngine(rawATSubChannel, description);
  }

  public void turnOn() throws ChannelException, InterruptedException {
    checkAndSetStateAndExceptionOnFailWithinUnitLock(BaseUnit.State.TURNED_OFF, BaseUnit.State.TURNING_ON);
    try {
      gsmChannel.enslave();
      try {
        atEngine.start(new ArrayList<>());
        try {
          init();
        } catch (ChannelException | InterruptedException e) {
          try {
            atEngine.stop();
          } catch (ChannelException | InterruptedException ce) {
            ce.addSuppressed(e);
            throw ce;
          }
          throw e;
        }
      } catch (ChannelException | InterruptedException e) {
        try {
          gsmChannel.free();
        } catch (ChannelException ce) {
          ce.addSuppressed(e);
          throw ce;
        }
        throw e;
      }
    } catch (ChannelException | InterruptedException e) {
      logger.error("[{}] - can't turn on gsm mobile station unit", this, e);
      setStateWithinUnitLock(BaseUnit.State.TURNED_OFF);
      throw e;
    }
    setStateWithinUnitLock(BaseUnit.State.TURNED_ON);
  }

  public void turnOff() throws ChannelException, InterruptedException {
    checkAndSetStateAndExceptionOnFailWithinUnitLock(BaseUnit.State.TURNED_ON, BaseUnit.State.TURNING_OFF);
    try {
      Exception le = null;
      try {
        atEngine.stop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      try {
        gsmChannel.free();
      } catch (ChannelException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      if (le != null) {
        if (le instanceof ChannelException) {
          throw (ChannelException) le;
        } else {
          throw (InterruptedException) le;
        }
      }
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't correctly turn off gsm mobile station unit", this, e);
      throw e;
    } finally {
      setStateWithinUnitLock(BaseUnit.State.TURNED_OFF);
    }
  }

  public List<NetworkSurveyInfo> executeNetworkSurvey() throws ChannelException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't execute nNetwork survey");

      AThhCSURBaseExecResult result = ATEngineHelper.execute(this, this::execute, new AThhCSURVCExec(), 3, 5_000);
      List<NetworkSurveyInfo> networkSurveyInfos = result.getBcchCarrierInfos().stream()
          .map(e -> new NetworkSurveyInfo(e.getArfcn(), e.getBsic(), e.getRxLev(), e.getBer(), (short) e.getMcc(), (short) e.getMnc(), e.getLac(),
              e.getCellId(), e.getCellStatus().getValue(), e.getValidChannels(), e.getNeighbourChannels()))
          .collect(Collectors.toList());
      networkSurveyInfos.addAll(result.getNonBcchCarrierInfos().stream().map(e -> new NetworkSurveyInfo(e.getArfcn(), e.getRxLev())).collect(Collectors.toList()));
      return networkSurveyInfos;
    } finally {
      unitLock.unlock();
    }
  }

  private void init() throws ChannelException, InterruptedException {
    Thread.sleep(Telit.GL865_TURN_ON_DELAY);

    // check if telit module has started by execution valuable command instead of simple AT echo
    execute(new ATEExec(false));

    execute(new AThhCPUMODEWrite(PhhCPUMODE.Mode.CPU_CLOCK_104_MHZ));
    execute(new ATpsCMEEWrite(PpsCMEE.ErrorMode.ENABLE_WITH_STRING_VALUES));
    execute(new ATVExec(true));
    execute(new ATadKExec(PadK.FlowControl.HARDWARE_BI_DIRECTIONAL_FLOW_CONTROL));
    execute(new ATadDExec(PadD.DTRMode.HIGH_TO_LOW_TRANSITION_CURRENT_CONNECTION_CLOSED));
    execute(new AThhSLEDWrite(PhhSLED.Mode.GPIO_HANDLED_BY_MODULE_SOFTWARE));

    execute(new AThhCSURVNLFWrite(true));
    execute(new AThhCSURVEXTWrite(PhhCSURVEXT.Mode.ENABLE_EXTENDED_NETWORK_SURVEY));

    execute(new AThhBCCHLOCKWrite());

    Thread.sleep(12_000); // magic delay
  }

  <T extends IATResult> T execute(final IAT<T> command) throws ChannelException, InterruptedException {
    return atEngine.execute(command);
  }

}
