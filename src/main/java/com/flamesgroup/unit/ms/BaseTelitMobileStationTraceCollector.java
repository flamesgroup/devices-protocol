/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;

// currently class sync by BaseUnit state (if needed in future internal sync mechanism could be implemented)
public abstract class BaseTelitMobileStationTraceCollector implements ITelitMobileStationTraceCollector {

  private ITelitMobileStationTraceHandler traceHandler;

  protected abstract void traceStart(ITelitMobileStationTraceHandler traceHandler) throws ChannelException, InterruptedException;

  protected abstract void traceStop() throws ChannelException, InterruptedException;

  @Override
  public void start(final ITelitMobileStationTraceHandler traceHandler) throws ChannelException, InterruptedException {
    if (traceHandler != null) {
      traceHandler.handleOpen();
      try {
        traceStart(traceHandler);
        this.traceHandler = traceHandler;
      } catch (ChannelException | InterruptedException e) {
        traceHandler.handleClose();
        throw e;
      }
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    if (traceHandler != null) {
      try {
        traceStop();
      } finally {
        ITelitMobileStationTraceHandler traceHandler = this.traceHandler;
        this.traceHandler = null;
        traceHandler.handleClose();
      }
    }
  }

}
