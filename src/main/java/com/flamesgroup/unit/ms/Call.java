/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.ATHExec;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IMobileCall;
import com.flamesgroup.unit.IMobileCallHandler;
import com.flamesgroup.unit.MobileCallDropReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Call implements IMobileCall {

  private static final Logger logger = LoggerFactory.getLogger(Call.class);

  enum State {
    INITIAL, DIALING, ACCEPTED, ALERTING, HELD, ACTIVE, DROPPED
  }

  final BaseMobileStationUnit mobileStationUnit;

  Lock callLock = new ReentrantLock();
  private State state = State.INITIAL;

  IMobileCallHandler callHandler;

  public Call(final BaseMobileStationUnit mobileStationUnit) {
    Objects.requireNonNull(mobileStationUnit, "mobileStationUnit mustn't be null");

    this.mobileStationUnit = mobileStationUnit;
  }

  void setState(final State state) {
    this.state = state;
  }

  void checkCurrentStateWithExpectedAndExceptionOnFail(final State expectedState) {
    int r = state.compareTo(expectedState);
    if (r > 0) {
      throw new IllegalStateException("[" + this + "] has illegal state: " + state + ", when expected state before: " + expectedState);
    } else if (r == 0) {
      throw new IllegalStateException("[" + this + "] already in state: " + state);
    }
  }

  boolean checkCurrentStateWithExpectedAndWarnOnFail(final State expectedState) {
    int r = state.compareTo(expectedState);
    if (r > 0) {
      logger.warn("[{}] has illegal state: {}, when expected state before: {}", this, state, expectedState);
    } else if (r == 0) {
      logger.warn("[{}] already in state: {}", this, state);
    } else {
      return true;
    }
    return false;
  }

  @Override
  public void sendDTMF(final String dtmfString) throws ChannelException, InterruptedException {
    callLock.lock();
    try {
      checkCurrentStateWithExpectedAndExceptionOnFail(State.DROPPED);

      mobileStationUnit.executeDTMF(dtmfString);
    } finally {
      callLock.unlock();
    }
  }

  @Override
  public void sendDTMF(final char dtmf, final int duration) throws ChannelException, InterruptedException {
    callLock.lock();
    try {
      checkCurrentStateWithExpectedAndExceptionOnFail(State.DROPPED);

      mobileStationUnit.executeDTMF(dtmf, duration);
    } finally {
      callLock.unlock();
    }
  }

  @Override
  public void drop() throws ChannelException, InterruptedException {
    mobileStationUnit.compareAndSetCallWithinUnitLock(this, null);

    callLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(State.DROPPED)) {
        return;
      }

      callHandler = null;
      setState(State.DROPPED);

      mobileStationUnit.execute(new ATHExec()); // execute after state change to be sure that it changed even in case of AT execution problem
    } finally {
      callLock.unlock();
    }
  }

  public void handleDTMF(final char dtmf) {
    IMobileCallHandler callHandlerLocal;
    callLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(State.DROPPED)) {
        return;
      }

      callHandlerLocal = callHandler;
    } finally {
      callLock.unlock();
    }

    callHandlerLocal.handleDTMF(dtmf);
  }

  public void handleHeld() {
    IMobileCallHandler callHandlerLocal;
    callLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(State.DROPPED)) {
        return;
      }

      callHandlerLocal = callHandler;

      setState(State.HELD);
    } finally {
      callLock.unlock();
    }

    callHandlerLocal.handleHeld();
  }

  public void handleActive() {
    IMobileCallHandler callHandlerLocal;
    callLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(State.DROPPED)) {
        return;
      }

      callHandlerLocal = callHandler;

      setState(State.ACTIVE);
    } finally {
      callLock.unlock();
    }

    callHandlerLocal.handleUnheld();
  }

  public void handleDropped(final MobileCallDropReason reason, final IExtendedCallErrorReport report) {
    IMobileCallHandler callHandlerLocal;
    callLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(State.DROPPED)) {
        return;
      }

      callHandlerLocal = callHandler;

      callHandler = null;
      setState(State.DROPPED);
    } finally {
      callLock.unlock();
    }

    callHandlerLocal.handleDropped(reason, report);
  }

}
