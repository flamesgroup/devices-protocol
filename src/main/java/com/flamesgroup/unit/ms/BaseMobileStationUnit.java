/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.gsmb.IAudioChannel;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATBusyStatusException;
import com.flamesgroup.device.gsmb.atengine.at.ATHExec;
import com.flamesgroup.device.gsmb.atengine.at.ATNoCarrierStatusException;
import com.flamesgroup.device.gsmb.atengine.at.ATNoDialtoneStatusException;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGSWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGSWriteResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCOPSRead;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCOPSReadResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCREGRead;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCREGReadResult;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.PpsCIEV;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSSI;
import com.flamesgroup.device.gsmb.atengine.urc.IURC;
import com.flamesgroup.device.gsmb.atengine.urc.IURCBUSYHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCNOCARRIERHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCNODIALTONEHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCRINGHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCDSHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCDSIHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCIEVHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCMTHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCMTIHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCREGHandler;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCSSIHandler;
import com.flamesgroup.device.gsmb.atengine.urc.URCBUSY;
import com.flamesgroup.device.gsmb.atengine.urc.URCNOCARRIER;
import com.flamesgroup.device.gsmb.atengine.urc.URCNODIALTONE;
import com.flamesgroup.device.gsmb.atengine.urc.URCRING;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCDS;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCDSI;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCIEV;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCMT;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCMTI;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCREG;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCSSI;
import com.flamesgroup.device.gsmb.atengine.ussd.IUSSDSession;
import com.flamesgroup.device.sc.SCSubChannelsJoining;
import com.flamesgroup.unit.BaseUnit;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.IATInternalErrorHandler;
import com.flamesgroup.unit.IAudioStream;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IExtendedNetworkErrorReport;
import com.flamesgroup.unit.IIncomingMobileCallHandler;
import com.flamesgroup.unit.IIncomingSMSHandler;
import com.flamesgroup.unit.IMobileOriginatingCall;
import com.flamesgroup.unit.IMobileOriginatingCallHandler;
import com.flamesgroup.unit.IMobileStationUnit;
import com.flamesgroup.unit.IMobileStationUnitInternalErrorHandler;
import com.flamesgroup.unit.IMobileTerminatingCallHandler;
import com.flamesgroup.unit.IServiceInfoHandler;
import com.flamesgroup.unit.MobileCallDropReason;
import com.flamesgroup.unit.Options;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.RegistrationStatus;
import com.flamesgroup.unit.RegistrationStatus.InternalRegistrationStatus;
import com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus;
import com.flamesgroup.unit.SupplementaryServiceNotificationCode;
import com.flamesgroup.unit.ms.event.ActiveCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.AlertingCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.DropMobileStationEvent;
import com.flamesgroup.unit.ms.event.DtmfMobileStationEvent;
import com.flamesgroup.unit.ms.event.HeldCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.IncomingCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.MobileStationEvent;
import com.flamesgroup.unit.sms.PDUException;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.unit.sms.SMSFactory;
import com.flamesgroup.unit.sms.SMSHelper;
import com.flamesgroup.unit.sms.SMSMessageClass;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.unit.sms.SendSMSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BaseMobileStationUnit extends BaseUnit implements IMobileStationUnit {

  private static final Logger logger = LoggerFactory.getLogger(BaseMobileStationUnit.class);

  private static final EnumMap<PpsCREG.Status, NetworkRegistrationStatus> registrationStats = new EnumMap<>(PpsCREG.Status.class);
  private static final EnumMap<PpsCSSI.Code1, SupplementaryServiceNotificationCode> supplementaryServiceNotificationCodes = new EnumMap<>(PpsCSSI.Code1.class);

  final IGSMChannel gsmChannel;
  final IAudioChannel audioChannel;
  final IMobileStationUnitInternalErrorHandler internalErrorHandler;

  IATEngine atEngine; // must be init in inheritor constructor

  IUSSDSession ussdSession; // must be init in inheritor constructor

  SCSubChannelsJoining scSubChannelsJoining; // must be init in inheritor constructor

  BaseMobileIncomingSMSHandler mobileIncomingSMSHandler; // must be init in inheritor constructor

  private final IAudioStream audioStream;

  private final Queue<IChannel> enslavedChannels = new LinkedList<>();

  Options options;

  final List<IURC> atEngineURCs = new ArrayList<>();

  private IIncomingMobileCallHandler incomingCallHandler;
  private IIncomingSMSHandler incomingSMSHandler;
  private IServiceInfoHandler serviceInfoHandler;

  private Call call;

  private volatile Thread callInfoThread;

  private final Queue<RegistrationInfo> registrationInfos = new ConcurrentLinkedQueue<>();

  protected final BlockingQueue<MobileStationEvent> mobileStationEvents = new LinkedBlockingQueue<>();

  private final AtomicInteger smsSendReferenceNumber = new AtomicInteger(new Random().nextInt(255 + 1)); // 255 - the maximum number can store a byte

  private final String toS;

  BaseMobileStationUnit(final IGSMChannel gsmChannel, final IAudioChannel audioChannel,
      final IMobileStationUnitInternalErrorHandler internalErrorHandler, final String description) {
    Objects.requireNonNull(gsmChannel, "gsmChannel mustn't be null");
    Objects.requireNonNull(audioChannel, "audioChannel mustn't be null");

    this.gsmChannel = gsmChannel;
    this.audioChannel = audioChannel;
    if (internalErrorHandler != null) {
      this.internalErrorHandler = internalErrorHandler;
    } else {
      this.internalErrorHandler = new IMobileStationUnitInternalErrorHandler() {
      };
    }

    audioStream = new AudioSubChannel2AudioStreamAdapter(audioChannel.getAudioSubChannel());

    // init base ATEngine URCs
    atEngineURCs.add(new URCpsCIEV(new CIEVHandler()));
    atEngineURCs.add(new URCpsCREG(new CREGHandler()));
    atEngineURCs.add(new URCRING(new RINGHandler()));
    atEngineURCs.add(new URCNOCARRIER(new NOCARRIERHandler()));
    atEngineURCs.add(new URCNODIALTONE(new NODIALTONEHandler()));
    atEngineURCs.add(new URCBUSY(new BUSYHandler()));
    atEngineURCs.add(new URCpsCSSI(new CSSIHandler()));
    atEngineURCs.add(new URCpsCMTI(new CMTIHandler()));
    atEngineURCs.add(new URCpsCMT(new CMTHandler()));
    atEngineURCs.add(new URCpsCDSI(new CDSIHandler()));
    atEngineURCs.add(new URCpsCDS(new CDSHandler()));

    toS = String.format("%s@%x[%s]", getClass().getSimpleName(), hashCode(), description);
  }

  @Override
  public byte[] getIMEI() throws ChannelException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_OFF, "can't get IMEI");

      gsmChannel.enslave();
      try {
        return gsmChannel.getIMEI();
      } finally {
        gsmChannel.free();
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public void setIMEI(final byte[] imei) throws ChannelException, InterruptedException {
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_OFF, "can't set IMEI");

      gsmChannel.enslave();
      try {
        gsmChannel.setIMEI(imei);
      } finally {
        gsmChannel.free();
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public void turnOn(final Options options, final IIncomingMobileCallHandler incomingCallHandler, final IIncomingSMSHandler incomingSMSHandler,
      final IServiceInfoHandler serviceInfoHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(options, "options mustn't be null");
    Objects.requireNonNull(incomingCallHandler, "incomingCallHandler mustn't be null");
    Objects.requireNonNull(incomingSMSHandler, "incomingSMSHandler mustn't be null");
    Objects.requireNonNull(serviceInfoHandler, "serviceInfoHandler mustn't be null");

    checkAndSetStateAndExceptionOnFailWithinUnitLock(BaseUnit.State.TURNED_OFF, BaseUnit.State.TURNING_ON);

    this.options = options;
    this.incomingCallHandler = incomingCallHandler;
    this.incomingSMSHandler = incomingSMSHandler;
    this.serviceInfoHandler = serviceInfoHandler;

    try {
      enslaveChannels(getAllChannels());
      try {
        preAtEngineStart();
        try {
          scSubChannelsJoining.join();
          try {
            atEngine.start(atEngineURCs);
            try {
              try {
                waitForMobileStationStart();
                init();
              } catch (ChannelException | InterruptedException e) {
                throw e;
              }
            } catch (ChannelException | InterruptedException e) {
              try {
                atEngine.stop();
              } catch (ChannelException | InterruptedException ce) {
                ce.addSuppressed(e);
                throw ce;
              }
              throw e;
            }
          } catch (ChannelException | InterruptedException e) {
            try {
              scSubChannelsJoining.disjoin();
            } catch (ChannelException | InterruptedException ce) {
              ce.addSuppressed(e);
              throw ce;
            }
            throw e;
          }
        } catch (ChannelException | InterruptedException e) {
          try {
            postAtEngineStop();
          } catch (ChannelException | InterruptedException ce) {
            ce.addSuppressed(e);
            throw ce;
          }
          throw e;
        }
      } catch (ChannelException | InterruptedException e) {
        try {
          freeEnslavedChannels();
        } catch (ChannelException ce) {
          ce.addSuppressed(e);
          throw ce;
        }
        throw e;
      }

      callInfoThread = new CallInfoThread(internalErrorHandler);
      callInfoThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

        @Override
        public void uncaughtException(final Thread t, final Throwable e) {
          logger.error("[{}] - unexpected exception during execution of [{}]", BaseMobileStationUnit.this, t, e);
        }

      });
      callInfoThread.start();
    } catch (ChannelException | InterruptedException e) {
      logger.error("[{}] - can't turn on gsm mobile station unit", this, e);

      setStateWithinUnitLock(BaseUnit.State.TURNED_OFF);
      throw e;
    }

    setStateWithinUnitLock(BaseUnit.State.TURNED_ON);
  }

  @Override
  public void turnOff() throws ChannelException, InterruptedException {
    checkAndSetStateAndExceptionOnFailWithinUnitLock(BaseUnit.State.TURNED_ON, BaseUnit.State.TURNING_OFF);

    try {
      Thread localCallInfoThread = callInfoThread;
      callInfoThread = null;
      localCallInfoThread.join();

      Exception le = null;
      try {
        deinit();
        waitForMobileStationStop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      try {
        scSubChannelsJoining.disjoin();
      } catch (ChannelException | InterruptedException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      try {
        atEngine.stop();
      } catch (ChannelException | InterruptedException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      try {
        postAtEngineStop();
      } catch (ChannelException | InterruptedException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      try {
        freeEnslavedChannels();
      } catch (ChannelException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      if (le != null) {
        if (le instanceof ChannelException) {
          throw (ChannelException) le;
        } else {
          throw (InterruptedException) le;
        }
      }
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't correctly turn off gsm mobile station unit", this, e);

      throw e;
    } finally {
      setStateWithinUnitLock(BaseUnit.State.TURNED_OFF);
    }
  }

  @Override
  public IMobileOriginatingCall dial(final PhoneNumber number, final IMobileOriginatingCallHandler originatedCallHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(originatedCallHandler, "Originated call handler mustn't be null");

    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't dial");

      if (getCall() != null) {
        throw new IllegalStateException("Can't dial, because another call is active");
      }

      OriginatedCall localOriginatedCall = new OriginatedCall(this);

      try {
        localOriginatedCall.dial(number, originatedCallHandler);
        logger.trace("[{}] - dial {} successful", this, number);
      } catch (ATNoDialtoneStatusException e) {
        logger.trace("[{}] - dial {} return NO DIALTONE", this, number);
        mobileStationEvents.put(new DropMobileStationEvent(MobileCallDropReason.NO_DIALTONE));
      } catch (ATNoCarrierStatusException e) {
        logger.trace("[{}] - dial {} return NO CARRIER", this, number);
        mobileStationEvents.put(new DropMobileStationEvent(MobileCallDropReason.NO_CARRIER));
      } catch (ATBusyStatusException e) {
        logger.trace("[{}] - dial {} return BUSY", this, number);
        mobileStationEvents.put(new DropMobileStationEvent(MobileCallDropReason.BUSY));
      }

      setCall(localOriginatedCall);
      return localOriginatedCall;
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public List<SMSMessageOutbound> sendSMS(final PhoneNumber number, final String text, final SMSMessageClass smsMessageClass, final int validityPeriod,
      final boolean needReport) throws SMSException, ChannelException, InterruptedException {
    List<SMSMessageOutbound> smsMessageOutbounds = new ArrayList<>();
    List<String> pdus = SMSHelper.createStringPdus(null, number, text, smsSendReferenceNumber.getAndIncrement(), smsMessageClass, validityPeriod, needReport);
    unitLock.lock();
    try {
      checkStateAndExceptionOnFail(BaseUnit.State.TURNED_ON, "can't send SMS");

      int countOfSmsParts = 0;
      for (String pdu : pdus) {
        ATpsCMGSWrite atCMGSWrite = new ATpsCMGSWrite(SMSHelper.getPduSize(pdu), pdu);
        ATpsCMGSWriteResult atCMGSWriteResult = executeExtra(atCMGSWrite);

        SMSMessageOutbound smsMessageOutbound = SMSFactory.createSmsMessageOutbound(number, pdu, atCMGSWriteResult.getMessageReference(), ++countOfSmsParts, pdus.size(), needReport);
        smsMessageOutbounds.add(smsMessageOutbound);
      }
      return smsMessageOutbounds;
    } catch (PDUException | ChannelException e) {
      throw new SendSMSException("Can't send SMS", pdus.size(), smsMessageOutbounds.size(), e);
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  public IUSSDSession getUSSDSession() {
    return ussdSession;
  }

  @Override
  public IAudioStream getAudioStream() {
    return audioStream;
  }

  // turnOn/turnOff phases callback methods

  void preAtEngineStart() throws ChannelException, InterruptedException {
  }

  void waitForMobileStationStart() throws ChannelException, InterruptedException {
  }

  void init() throws ChannelException, InterruptedException {
  }

  void postInit() throws ChannelException, InterruptedException {
  }

  void deinit() throws ChannelException, InterruptedException {
  }

  void waitForMobileStationStop() throws ChannelException, InterruptedException {
  }

  void postAtEngineStop() throws ChannelException, InterruptedException {
  }

  // helpers methods

  <T extends IATResult> T execute(final IAT<T> command) throws ChannelException, InterruptedException {
    return atEngine.execute(command);
  }

  <T extends IATResult> T executeExtra(final IAT<T> command) throws ChannelException, InterruptedException {
    return execute(command);
  }

  void enslaveChannels(final IChannel... channels) throws ChannelException, InterruptedException {
    IChannel currentChannel = null;
    try {
      for (IChannel channel : channels) {
        currentChannel = channel;
        channel.enslave();
        enslavedChannels.add(channel);
      }
    } catch (ChannelException | InterruptedException e) {
      logger.error("[{}] - can't open channel [{}]", this, currentChannel, e);
      freeEnslavedChannels();
      throw e;
    }
  }

  void freeEnslavedChannels() throws ChannelException {
    while (!enslavedChannels.isEmpty()) {
      enslavedChannels.remove().free();
    }
  }

  // call objects atomic change methods
  private boolean compareAndSetCall(final Call expect, final Call update) {
    if (call == expect) {
      call = update;
      return true;
    } else {
      return false;
    }
  }

  boolean compareAndSetCallWithinUnitLock(final Call expect, final Call update) {
    unitLock.lock();
    try {
      return compareAndSetCall(expect, update);
    } finally {
      unitLock.unlock();
    }
  }

  private Call getCall() {
    return call;
  }

  Call getCallWithinUnitLock() {
    unitLock.lock();
    try {
      return getCall();
    } finally {
      unitLock.unlock();
    }
  }

  private void setCall(final Call call) {
    this.call = call;
  }

  void setCallWithinUnitLock(final Call newValue) {
    unitLock.lock();
    try {
      setCall(newValue);
    } finally {
      unitLock.unlock();
    }
  }

  // internal thread

  private class CallInfoThread extends Thread {

    private static final long CELL_MONITORING_PERIOD = 60000;

    private final IATInternalErrorHandler internalErrorHandler;

    private long previousCellMonitoringTimeMillis;
    private CellInfo[] previousCellInfos;

    public CallInfoThread(final IATInternalErrorHandler internalErrorHandler) {
      super("CallInfoThread (" + BaseMobileStationUnit.this + ")");
      this.internalErrorHandler = internalErrorHandler;
    }

    private void processCellMonitoring() {
      if (System.currentTimeMillis() - previousCellMonitoringTimeMillis >= CELL_MONITORING_PERIOD) {
        previousCellMonitoringTimeMillis = System.currentTimeMillis();
        try {
          CellInfo[] cellInfos = executeReadCellMonitoring();
          if (!Arrays.equals(cellInfos, previousCellInfos)) {
            serviceInfoHandler.handleCellMonitoring(cellInfos);
            previousCellInfos = cellInfos;
          }
        } catch (ChannelException ex) {
          logger.warn("[{}] - can't execute cell monitoring", this, ex);
          internalErrorHandler.handleATEngineExecutionError(ex);
        } catch (InterruptedException ex) {
          throw new AssertionError(ex);
        }
      }
    }

    private RegistrationReport processReadRegistrationReport() {
      try {
        return executeReadRegistrationReport();
      } catch (ChannelException ex) {
        logger.warn("[{}] - can't get registration report", this, ex);
        internalErrorHandler.handleATEngineExecutionError(ex);
      } catch (InterruptedException ex) {
        throw new AssertionError(ex);
      }
      return null;
    }

    private ATpsCREGReadResult processReadRegistrationStatus() {
      try {
        return executeReadRegistrationStatus();
      } catch (ChannelException ex) {
        logger.warn("[{}] - can't execute check registration", this, ex);
        internalErrorHandler.handleATEngineExecutionError(ex);
      } catch (InterruptedException ex) {
        throw new AssertionError(ex);
      }
      return null;
    }

    private String processReadOperator() {
      try {
        ATpsCOPSReadResult atpsCOPSReadResult = executeReadOperatorSelectionMode();
        return atpsCOPSReadResult.getOperator();
      } catch (ChannelException ex) {
        logger.warn("[{}] - can't get operator", this, ex);
        internalErrorHandler.handleATEngineExecutionError(ex);
      } catch (InterruptedException ex) {
        throw new AssertionError(ex);
      }
      return null;
    }

    private IExtendedCallErrorReport processReadExtendedCallErrorReport() {
      try {
        return executeReadExtendedCallErrorReport();
      } catch (ChannelException ex) {
        logger.warn("[{}] - can't get extended call error report", this, ex);
        internalErrorHandler.handleATEngineExecutionError(ex);
      } catch (InterruptedException ex) {
        throw new AssertionError(ex);
      }
      return new UnknownExtendedCallErrorReport();
    }

    @Override
    public void run() {
      Thread currentThread = Thread.currentThread();

      long registrationTimeout = options.getRegistrationTimeout();
      long ignoreRegistrationDeniedTimeout = options.getIgnoreRegistrationDeniedTimeout();
      long startRegistrationTimeMillis = System.currentTimeMillis();

      while (currentThread == callInfoThread) {
        ATpsCREGReadResult result = processReadRegistrationStatus();
        if (result == null) {
          continue;
        }

        NetworkRegistrationStatus stat = registrationStats.get(result.getStat());
        long registrationTime = System.currentTimeMillis() - startRegistrationTimeMillis;
        if (stat == NetworkRegistrationStatus.REGISTRATION_DENIED && registrationTime >= ignoreRegistrationDeniedTimeout || registrationTime >= registrationTimeout) {
          logger.error("[{}] - can't register in GSM network - registration timeout elapsed with status [{}]", this, stat);
          RegistrationReport registrationReport = processReadRegistrationReport();
          if (registrationReport == null) {
            continue;
          }

          serviceInfoHandler.handleNetworkRegistration(new RegistrationStatus(InternalRegistrationStatus.TIMEOUT_ERROR, stat), null, null, null, registrationReport.getErrorReport());
          return;
        }

        if (stat == NetworkRegistrationStatus.REGISTERED_TO_HOME_NETWORK || stat == NetworkRegistrationStatus.REGISTERED_ROAMING) {
          if (!registrationInfos.isEmpty()) {
            logger.warn("[{}] - clear odd network registration statuses", this);
            registrationInfos.clear(); // TODO tmp solution to clear all +CREG URCs before actual enable +CREG URC - on some modules &P profile is chosen with already enabled +CREG URC
          }
          registrationInfos.add(new RegistrationInfo(new RegistrationStatus(stat), result.getLocationAreaCode(), result.getCellID()));
          break;
        }
      }

      try {
        postInit();
      } catch (ChannelException ex) {
        logger.error("[{}] - can't execute post initialization for gsm mobile station unit", this, ex);
        internalErrorHandler.handleATEngineExecutionError(ex);
        return;
      } catch (InterruptedException ex) {
        throw new AssertionError(ex);
      }

      mobileIncomingSMSHandler.init(incomingSMSHandler); // this method call makes sense only after successful registration

      while (currentThread == callInfoThread) {
        processCellMonitoring();
        mobileIncomingSMSHandler.processSMSRoutine();

        RegistrationInfo registrationInfo = registrationInfos.peek();
        if (registrationInfo != null) { // have new registration info
          RegistrationReport registrationReport = processReadRegistrationReport();
          if (registrationReport != null) {
            String operator = processReadOperator();
            registrationInfos.poll(); // clear previously peek value

            serviceInfoHandler.handleNetworkRegistration(registrationInfo.getStatus(), operator, registrationInfo.getLocationAreaCode(), registrationInfo.getCellID(), registrationReport.getErrorReport());
          }
        }

        MobileStationEvent mobileStationEvent = pollMobileStationEvent();
        if (mobileStationEvent == null) {
          continue; // nothing to do
        }

        if (mobileStationEvent instanceof IncomingCallMobileStationEvent) {
          IncomingCallMobileStationEvent incomingCallEvent = (IncomingCallMobileStationEvent) mobileStationEvent;
          PhoneNumber terminatedNumber;
          if (incomingCallEvent.getNumber() == null) {
            terminatedNumber = new PhoneNumber(); // create "invalid" phone number - Private Number
          } else {
            terminatedNumber = new PhoneNumber(incomingCallEvent.getNumber());
          }

          TerminatedCall localTerminatedCall = new TerminatedCall(BaseMobileStationUnit.this);
          IMobileTerminatingCallHandler localTerminatedCallHandler = incomingCallHandler.handleNewIncomingMobileCall(terminatedNumber, localTerminatedCall);
          if (localTerminatedCallHandler != null) {
            localTerminatedCall.accept(localTerminatedCallHandler);
            setCallWithinUnitLock(localTerminatedCall);
          } else {
            logger.warn("[{}] - handleNewIncomingMobileCall return <null> handler, [{}] dropped", this, localTerminatedCall);
            try {
              execute(new ATHExec());
            } catch (ChannelException ex) {
              logger.warn("[{}] - can't disconnect existing call", this, ex);
            } catch (InterruptedException ex) {
              throw new AssertionError(ex);
            }
          }
        } else {
          Call localCall = getCallWithinUnitLock();
          if (localCall == null) {
            logger.debug("[{}] - unprocessed event [{}]", this, mobileStationEvent);
            continue;
          }

          if (mobileStationEvent instanceof DropMobileStationEvent) {
            DropMobileStationEvent dropEvent = (DropMobileStationEvent) mobileStationEvent;
            IExtendedCallErrorReport extendedCallErrorReport = processReadExtendedCallErrorReport();
            localCall.handleDropped(dropEvent.getMobileCallDropReason(), extendedCallErrorReport);
            compareAndSetCallWithinUnitLock(localCall, null);
          } else if (mobileStationEvent instanceof DtmfMobileStationEvent) {
            DtmfMobileStationEvent dtmfEvent = (DtmfMobileStationEvent) mobileStationEvent;
            localCall.handleDTMF(dtmfEvent.getDtmf());
          } else {
            if (localCall instanceof TerminatedCall) {
              TerminatedCall localTerminatedCall = (TerminatedCall) localCall;
              if (mobileStationEvent instanceof ActiveCallMobileStationEvent) {
                localTerminatedCall.handleActive();
              } else if (mobileStationEvent instanceof HeldCallMobileStationEvent) {
                localTerminatedCall.handleHeld();
              }
            } else if (localCall instanceof OriginatedCall) {
              OriginatedCall localOriginatedCall = (OriginatedCall) localCall;
              if (mobileStationEvent instanceof AlertingCallMobileStationEvent) {
                localOriginatedCall.handleAlerting();
              } else if (mobileStationEvent instanceof ActiveCallMobileStationEvent) {
                localOriginatedCall.handleActive();
              } else if (mobileStationEvent instanceof HeldCallMobileStationEvent) {
                localOriginatedCall.handleHeld();
              }
            } else {
              throw new AssertionError();
            }
          }
        }
      }
    }

    private final class UnknownExtendedCallErrorReport implements IExtendedCallErrorReport {

      private static final long serialVersionUID = 4874708436921020386L;

      @Override
      public int getCallControlConnectionManagementCause() {
        return -1;
      }

      @Override
      public String getCallControlConnectionManagementCauseDescription() {
        return "Unknown extended call error report";
      }

    }

  }

  private final class RegistrationReport {

    private final ATpsCREGReadResult atpsCREGReadResult;
    private final IExtendedNetworkErrorReport errorReport;

    public RegistrationReport(final ATpsCREGReadResult atpsCREGReadResult, final IExtendedNetworkErrorReport errorReport) {
      this.atpsCREGReadResult = atpsCREGReadResult;
      this.errorReport = errorReport;
    }

    public ATpsCREGReadResult getATpsCREGReadResult() {
      return atpsCREGReadResult;
    }

    public IExtendedNetworkErrorReport getErrorReport() {
      return errorReport;
    }

  }

  // internal handler's classes

  private static class RINGHandler implements IURCRINGHandler {

    @Override
    public void handleRing() {
    }

  }

  private class CREGHandler implements IURCpsCREGHandler {

    @Override
    public void handleNetworkRegistration(final PpsCREG.Status status, final String locationAreaCode, final String cellID) {
      registrationInfos.add(new RegistrationInfo(new RegistrationStatus(registrationStats.get(status)), locationAreaCode, cellID));
    }

  }

  private class CIEVHandler implements IURCpsCIEVHandler {

    @Override
    public void handleIndicatorChanges(final PpsCIEV.Indicator indicator, int value1, final int value2) {
      switch (indicator) {
        case SIGNAL:
          serviceInfoHandler.handleSignalBitErrorRate(value1);
          break;
        case RSSI:
          if (value1 != ServiceInfoConstants.SIGNAL_RECEIVED_STRENGTH_NOT_KNOWN_OR_NOT_DETECTABLE) {
            value1 = -112 + value1 * 15;
          }
          serviceInfoHandler.handleSignalReceivedStrength(value1);
          break;
      }
    }

  }

  private class NOCARRIERHandler implements IURCNOCARRIERHandler {

    @Override
    public void handleNoCarrier() {
      logger.trace("[{}] - handle NO CARRIER", BaseMobileStationUnit.this);
      if (!mobileStationEvents.offer(new DropMobileStationEvent(MobileCallDropReason.NO_CARRIER))) {
        throw new AssertionError();
      }
    }

  }

  private class NODIALTONEHandler implements IURCNODIALTONEHandler {

    @Override
    public void handleNoDialtone() {
      logger.trace("[{}] - handle NO DIALTONE", BaseMobileStationUnit.this);
      if (!mobileStationEvents.offer(new DropMobileStationEvent(MobileCallDropReason.NO_DIALTONE))) {
        throw new AssertionError();
      }
    }

  }

  private class BUSYHandler implements IURCBUSYHandler {

    @Override
    public void handleBusy() {
      logger.trace("[{}] - handle BUSY", BaseMobileStationUnit.this);
      if (!mobileStationEvents.offer(new DropMobileStationEvent(MobileCallDropReason.BUSY))) {
        throw new AssertionError();
      }
    }

  }

  private class CSSIHandler implements IURCpsCSSIHandler {

    @Override
    public void handleSupplementaryServiceNotification(final PpsCSSI.Code1 code1) {
      serviceInfoHandler.handleSupplementaryServiceNotification(supplementaryServiceNotificationCodes.get(code1));
    }

  }

  private class CMTIHandler implements IURCpsCMTIHandler {

    @Override
    public void handleSMS(final String memoryStorage, final int index) {
      logger.trace("[{}] - received new SMS - memory storage: [{}]; index: [{}]", BaseMobileStationUnit.this, memoryStorage, index);
      mobileIncomingSMSHandler.enqueueIncomingSMSIndex(index);
    }

  }

  private class CMTHandler implements IURCpsCMTHandler {

    @Override
    public void handleSMS(final int length, final String pdu) {
      logger.debug("[{}] - received new SMS - pdu: [{}] with length: [{}]", BaseMobileStationUnit.this, pdu, length);
      int pduSize = -1;
      try {
        pduSize = SMSHelper.getPduSize(pdu);
      } catch (PDUException e) {
        logger.error("[{}] - can't get size of pdu: [{}]", BaseMobileStationUnit.this, pdu, e);
      }
      if (length == pduSize) {
        mobileIncomingSMSHandler.processIncomingSMSPdu(pdu);
      } else {
        logger.warn("[{}] - cant't process received SMS, because length: [{}] not equals pduSize: [{}] for pdu: [{}]", BaseMobileStationUnit.this, length, pduSize, pdu);
      }
    }

  }

  private class CDSIHandler implements IURCpsCDSIHandler {

    @Override
    public void handleSMSStatusReport(final String memoryStorage, final int index) {
      logger.trace("[{}] - received sms status report - memory storage: [{}]; index: [{}]", BaseMobileStationUnit.this, memoryStorage, index);
      mobileIncomingSMSHandler.enqueueIncomingSMSIndex(index);
    }

  }

  private class CDSHandler implements IURCpsCDSHandler {

    @Override
    public void handleSMSStatusReport(final int length, final String pdu) {
      logger.debug("[{}] - received sms status report - pdu: [{}] with length: [{}]", BaseMobileStationUnit.this, pdu, length);
      int pduSize = -1;
      try {
        pduSize = SMSHelper.getPduSize(pdu);
      } catch (PDUException e) {
        logger.error("[{}] - can't get size of pdu: [{}]", BaseMobileStationUnit.this, pdu, e);
      }
      if (length == pduSize) {
        mobileIncomingSMSHandler.processIncomingSMSPdu(pdu);
      } else {
        logger.warn("[{}] - cant't process SMS status report, because length: [{}] not equals pduSize: [{}] for pdu: [{}]", BaseMobileStationUnit.this, length, pduSize, pdu);
      }
    }

  }

  abstract IChannel[] getAllChannels();


  abstract void executeDTMF(final String dtmfString) throws ChannelException, InterruptedException;

  abstract void executeDTMF(final char dtmf, int duration) throws ChannelException, InterruptedException;

  abstract CellInfo[] executeReadCellMonitoring() throws ChannelException, InterruptedException;

  abstract IExtendedCallErrorReport executeReadExtendedCallErrorReport() throws ChannelException, InterruptedException;

  abstract IExtendedNetworkErrorReport executeReadExtendedNetworkErrorReport() throws ChannelException, InterruptedException;

  private RegistrationReport executeReadRegistrationReport() throws ChannelException, InterruptedException {
    unitLock.lock();
    try {
      return new RegistrationReport(executeReadRegistrationStatus(), executeReadExtendedNetworkErrorReport());
    } finally {
      unitLock.unlock();
    }
  }

  private ATpsCREGReadResult executeReadRegistrationStatus() throws ChannelException, InterruptedException {
    return execute(new ATpsCREGRead());
  }

  private ATpsCOPSReadResult executeReadOperatorSelectionMode() throws ChannelException, InterruptedException {
    return execute(new ATpsCOPSRead());
  }

  protected abstract MobileStationEvent pollMobileStationEvent();

  // Object
  @Override
  public String toString() {
    return toS;
  }

  // static init
  static {
    registrationStats.put(PpsCREG.Status.NOT_REGISTERED_NOT_SEARCHING, NetworkRegistrationStatus.NOT_REGISTERED_NOT_SEARCHING);
    registrationStats.put(PpsCREG.Status.REGISTERED_TO_HOME_NETWORK, NetworkRegistrationStatus.REGISTERED_TO_HOME_NETWORK);
    registrationStats.put(PpsCREG.Status.NOT_REGISTERED_AND_SEARCHING, NetworkRegistrationStatus.NOT_REGISTERED_AND_SEARCHING);
    registrationStats.put(PpsCREG.Status.REGISTRATION_DENIED, NetworkRegistrationStatus.REGISTRATION_DENIED);
    registrationStats.put(PpsCREG.Status.UNKNOWN, NetworkRegistrationStatus.UNKNOWN);
    registrationStats.put(PpsCREG.Status.REGISTERED_ROAMING, NetworkRegistrationStatus.REGISTERED_ROAMING);

    supplementaryServiceNotificationCodes.put(PpsCSSI.Code1.CALL_HAS_BEEN_FORWARDED, SupplementaryServiceNotificationCode.CALL_HAS_BEEN_FORWARDED);
    supplementaryServiceNotificationCodes.put(PpsCSSI.Code1.CALL_IS_WAITING, SupplementaryServiceNotificationCode.CALL_IS_WAITING);
    supplementaryServiceNotificationCodes.put(PpsCSSI.Code1.INCOMING_CALLS_ARE_BARRED, SupplementaryServiceNotificationCode.INCOMING_CALLS_ARE_BARRED);
    supplementaryServiceNotificationCodes.put(PpsCSSI.Code1.OUTGOING_CALLS_ARE_BARRED, SupplementaryServiceNotificationCode.OUTGOING_CALLS_ARE_BARRED);
    supplementaryServiceNotificationCodes
        .put(PpsCSSI.Code1.SOME_OF_THE_CONDITIONAL_CALL_FORWARDINGS_ARE_ACTIVE, SupplementaryServiceNotificationCode.SOME_OF_THE_CONDITIONAL_CALL_FORWARDINGS_ARE_ACTIVE);
    supplementaryServiceNotificationCodes.put(PpsCSSI.Code1.UNCONDITIONAL_CALL_FORWARDING_IS_ACTIVE, SupplementaryServiceNotificationCode.UNCONDITIONAL_CALL_FORWARDING_IS_ACTIVE);
  }

}
