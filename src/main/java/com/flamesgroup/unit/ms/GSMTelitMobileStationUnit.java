/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.ms;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.ChannelUnknownCommandException;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.gsmb.IAudioChannel;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.RawATSubChannel2ATSubChannelStackedAdapter;
import com.flamesgroup.device.gsmb.Telit;
import com.flamesgroup.device.gsmb.atengine.ATEngine;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATEExec;
import com.flamesgroup.device.gsmb.atengine.at.ATVExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCCWAWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCINDWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMEEWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMERWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCMGFWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCOPSWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCREGWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCSSNWrite;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMIExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMIExecResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMMExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMMExecResult;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMRExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMRExecResult;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATadDExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATadKExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhBCCHLOCKWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCEERExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCEERExecResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCEERNETExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCEERNETExecResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCMUXMODEWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhCPUMODEWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhDTMFWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhDVIWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhECAMWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhIMCDENWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhJDRWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhMONIExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhMONIExecResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhMONIWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhNCIHWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhQSSWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhREGMODEWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhRSENWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhRTDEWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSHDNExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSLEDWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSRPWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSTIAWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhTSVOLWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsCLVLWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsCMUXWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsCNMIWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsCSCSWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsCSMSWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsVTSWrite;
import com.flamesgroup.device.gsmb.atengine.http.HTTPSession;
import com.flamesgroup.device.gsmb.atengine.http.IHTTPSession;
import com.flamesgroup.device.gsmb.atengine.param.PpsCCWA;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMEE;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMER;
import com.flamesgroup.device.gsmb.atengine.param.PpsCMGF;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG;
import com.flamesgroup.device.gsmb.atengine.param.telit.PadD;
import com.flamesgroup.device.gsmb.atengine.param.telit.PadK;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhCMUXMODE;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhCPUMODE;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhDTMF;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhDVI;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhIMCDEN;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhJDR;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhQSS;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhREGMODE;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSLED;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSRP;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhTSVOL;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCSCS;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCSMS;
import com.flamesgroup.device.gsmb.atengine.urc.IURC;
import com.flamesgroup.device.gsmb.atengine.urc.IURChhQSSHandler;
import com.flamesgroup.device.gsmb.atengine.urc.URCBUSY;
import com.flamesgroup.device.gsmb.atengine.urc.URCNOCARRIER;
import com.flamesgroup.device.gsmb.atengine.urc.URCNODIALTONE;
import com.flamesgroup.device.gsmb.atengine.urc.URCRING;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCDS;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCDSI;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCIEV;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCMT;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCMTI;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCREG;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCSSI;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhDTMFEVHandler;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhECAMHandler;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhHTTPRINGHandler;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhIMCDHandler;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhJDRHandler;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhRSENHandler;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhDTMFEV;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhECAM;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhHTTPRING;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhIMCD;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhJDR;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhQSS;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhRSEN;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URChhSMOV;
import com.flamesgroup.device.gsmb.atengine.urc.telit.URCpsCUSD;
import com.flamesgroup.device.gsmb.atengine.ussd.telit.USSDSession;
import com.flamesgroup.device.gsmb.atengine.ussd.telit.USSDSessionStarHashWrapper;
import com.flamesgroup.device.protocol.cmux.CMUXConnection;
import com.flamesgroup.device.protocol.remotesim.IRemoteSimConnection;
import com.flamesgroup.device.protocol.remotesim.RemoteSimConnection;
import com.flamesgroup.device.sc.SCSubChannelsJoining;
import com.flamesgroup.device.simb.ISCReaderChannel;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.ICellEqualizer;
import com.flamesgroup.unit.ICellLocker;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IExtendedNetworkErrorReport;
import com.flamesgroup.unit.IMobileStationUnitExternalErrorHandler;
import com.flamesgroup.unit.IMobileStationUnitInternalErrorHandler;
import com.flamesgroup.unit.MobileStationUnitSimInsertedTimeoutException;
import com.flamesgroup.unit.ms.event.ActiveCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.AlertingCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.DtmfMobileStationEvent;
import com.flamesgroup.unit.ms.event.HeldCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.IncomingCallMobileStationEvent;
import com.flamesgroup.unit.ms.event.MobileStationEvent;
import com.flamesgroup.unit.ms.event.WaitingCallMobileStationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public final class GSMTelitMobileStationUnit extends BaseMobileStationUnit {

  private static final Logger logger = LoggerFactory.getLogger(GSMTelitMobileStationUnit.class);

  private static final long SIM_INSERTED_STATUS_TIMEOUT = 60000;

  private final ISCReaderChannel scReaderChannel;

  private final IATEngine rawATEngine;

  private final IATEngine atEngineExtra;
  private final List<IURC> atEngineExtraURCs = new ArrayList<>();

  private final CMUXConnection cmuxConnection;

  private final IMobileStationUnitExternalErrorHandler externalErrorHandler;

  private final String traceMask;
  private final ITelitMobileStationTraceHandler traceHandler;
  private TelitMobileStationTraceSubChannelTraceCollector traceSubChannelTraceCollector; // trace subchannel available only on GSMBOX* devices
  private TelitMobileStationCMUXVirtualPortTraceCollector cmuxVirtualPortTraceCollector; // trace CMUX virtual port available on GSMBOX* and GSMB3 devices

  private final Semaphore simInsertedStatusSemaphore = new Semaphore(0);

  private final ICellEqualizer cellEqualizer;
  private final ICellLocker cellLocker;
  private final IHTTPSession httpSession;

  public GSMTelitMobileStationUnit(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCReaderChannel scReaderChannel,
      final IMobileStationUnitInternalErrorHandler internalErrorHandler, final IMobileStationUnitExternalErrorHandler externalErrorHandler, final String description,
      final String traceMask, final ITelitMobileStationTraceHandler traceHandler) {
    super(gsmChannel, audioChannel, internalErrorHandler, description);
    Objects.requireNonNull(scReaderChannel, "scReaderChannel mustn't be null");

    this.scReaderChannel = scReaderChannel;
    if (externalErrorHandler != null) {
      this.externalErrorHandler = externalErrorHandler;
    } else {
      this.externalErrorHandler = new IMobileStationUnitExternalErrorHandler() {
      };
    }

    if ((traceMask != null && traceHandler == null) || (traceMask == null && traceHandler != null)) {
      throw new IllegalArgumentException("traceMask & traceHandler must be both init or null");
    }
    this.traceMask = traceMask;
    this.traceHandler = traceHandler;

    RawATSubChannel2ATSubChannelStackedAdapter rawATSubChannel = new RawATSubChannel2ATSubChannelStackedAdapter(this.gsmChannel.getRawATSubChannel());
    rawATEngine = new ATEngine(rawATSubChannel, "RAW:" + description);

    cmuxConnection = new CMUXConnection(rawATSubChannel, 10000); // max timeout get from source code of Telit CMUX Implementation for Linux (CMuxProto.c:34-38)

    CMUXVirtualPort2ATSubChannelAdaper cmuxVirtualPort = new CMUXVirtualPort2ATSubChannelAdaper(cmuxConnection.getVirtualPorts().get(0));
    atEngine = new ATEngine(cmuxVirtualPort, description);

    CMUXVirtualPort2ATSubChannelAdaper extraVirtualPort = new CMUXVirtualPort2ATSubChannelAdaper(cmuxConnection.getVirtualPorts().get(1));
    atEngineExtra = new ATEngine(extraVirtualPort, description);

    USSDSession telitUSSDSession = new USSDSession(atEngine);
    ussdSession = new USSDSessionStarHashWrapper(telitUSSDSession);

    GSMTelitMobileIncomingSMSHandler gsmTelitMobileIncomingSMSHandler = new GSMTelitMobileIncomingSMSHandler(this);
    mobileIncomingSMSHandler = gsmTelitMobileIncomingSMSHandler;

    httpSession = new HTTPSession(atEngineExtra);

    atEngineURCs.add(new URChhRSEN(new RSENHandler()));
    atEngineURCs.add(new URChhQSS(new QSSHandler()));
    atEngineURCs.add(new URChhDTMFEV(new DTMFEVHandler()));
    atEngineURCs.add(new URChhSMOV(gsmTelitMobileIncomingSMSHandler));
    atEngineURCs.add(new URCpsCUSD(telitUSSDSession));
    atEngineURCs.add(new URChhIMCD(new IMCDHandler()));
    atEngineURCs.add(new URChhJDR(new JDRHandler()));
    atEngineURCs.add(new URChhECAM(new ECAMHandler()));

    atEngineExtraURCs.add(new URChhHTTPRING((IURChhHTTPRINGHandler) httpSession));

    // add dummy URC handlers
    atEngineURCs.add(new URChhHTTPRING((profId, httpStatusCode, contentType, dataSize) -> {}));
    //   from GSMTelitMobileStationUnit
    atEngineExtraURCs.add(new URChhRSEN(connected -> {}));
    atEngineExtraURCs.add(new URChhQSS(status -> {}));
    atEngineExtraURCs.add(new URChhDTMFEV(dtmf -> {}));
    atEngineExtraURCs.add(new URChhSMOV(memory -> {}));
    atEngineExtraURCs.add(new URCpsCUSD((status, message, dataCodingScheme) -> {}));
    atEngineExtraURCs.add(new URChhIMCD(status -> {}));
    atEngineExtraURCs.add(new URChhJDR(status -> {}));
    atEngineExtraURCs.add(new URChhECAM((callId, callStatus, callType, calledNumber, typeOfNumber) -> {}));
    //   from BaseMobileStationUnit
    atEngineExtraURCs.add(new URCpsCIEV((indicator, value1, value2) -> {}));
    atEngineExtraURCs.add(new URCpsCREG((status, locationAreaCode, cellID) -> {}));
    atEngineExtraURCs.add(new URCRING(() -> {}));
    atEngineExtraURCs.add(new URCNOCARRIER(() -> {}));
    atEngineExtraURCs.add(new URCNODIALTONE(() -> {}));
    atEngineExtraURCs.add(new URCBUSY(() -> {}));
    atEngineExtraURCs.add(new URCpsCSSI(code1 -> {}));
    atEngineExtraURCs.add(new URCpsCMTI((memoryStorage, index) -> {}));
    atEngineExtraURCs.add(new URCpsCMT((length, pdu) -> {}));
    atEngineExtraURCs.add(new URCpsCDSI((memoryStorage, index) -> {}));
    atEngineExtraURCs.add(new URCpsCDS((length, pdu) -> {}));

    cellEqualizer = new GSMTelitCachedCellEqualizer(this);
    cellLocker = new GSMTelitCellLocker(this);

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}]/[{}]/[{}]", toString(), this.gsmChannel, this.audioChannel, this.scReaderChannel);
    }
  }

  public GSMTelitMobileStationUnit(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCReaderChannel scReaderChannel,
      final IMobileStationUnitInternalErrorHandler internalErrorHandler, final IMobileStationUnitExternalErrorHandler externalErrorHandler, final String description) {
    this(gsmChannel, audioChannel, scReaderChannel, internalErrorHandler, externalErrorHandler, description, null, null);
  }

  public GSMTelitMobileStationUnit(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCReaderChannel scReaderChannel, final String description) {
    this(gsmChannel, audioChannel, scReaderChannel, null, null, description);
  }

  @Override
  public ICellEqualizer getCellEqualizer() {
    return cellEqualizer;
  }

  @Override
  public ICellLocker getCellLocker() {
    return cellLocker;
  }

  @Override
  public IHTTPSession getHTTPSession() {
    return httpSession;
  }

  @Override
  IChannel[] getAllChannels() {
    return new IChannel[] {gsmChannel, audioChannel, scReaderChannel};
  }

  @Override
  protected MobileStationEvent pollMobileStationEvent() {
    try {
      return mobileStationEvents.poll(200, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      throw new AssertionError(e);
    }
  }

  @Override
  void preAtEngineStart() throws ChannelException, InterruptedException {
    IRemoteSimConnection remoteSimConnection = new RemoteSimConnection(cmuxConnection.getVirtualPorts().get(2));
    RemoteSimConnection2SCEmulatorSubChannelAdapter scEmulatorSubChannel = new RemoteSimConnection2SCEmulatorSubChannelAdapter(remoteSimConnection);

    scSubChannelsJoining = new SCSubChannelsJoining(scReaderChannel.getSCReaderSubChannel(), scEmulatorSubChannel, internalErrorHandler, options.isDisablePhase2pSupport());

    try {
      if (traceMask != null) {
        traceSubChannelTraceCollector = new TelitMobileStationTraceSubChannelTraceCollector(gsmChannel.getTraceSubChannel());
        try {
          traceSubChannelTraceCollector.start(traceHandler);
        } catch (ChannelUnknownCommandException e) {
          traceSubChannelTraceCollector = null;
          logger.trace("[{}] - trace subchannel unsupported, so use trace CMUX virtual port", this);
          cmuxVirtualPortTraceCollector =
              new TelitMobileStationCMUXVirtualPortTraceCollector(cmuxConnection.getVirtualPorts().get(3)); // in CMUX configuration Trace Access point connect to VC4 (COM6)
        }
      }

      rawATEngine.start(atEngineURCs);
      try {
        Thread.sleep(Telit.GL865_TURN_ON_DELAY);

        // check if telit module has started by execution valuable command instead of simple AT echo
        rawATEngine.execute(new ATEExec(false));

        if (traceSubChannelTraceCollector != null) {
          rawATEngine.execute(new AThhRTDEWrite(traceMask));
        }

        if (options.getCpuMode() != null) {
          rawATEngine.execute(new AThhCPUMODEWrite(PhhCPUMODE.Mode.getModeByValue(options.getCpuMode())));
        }
        rawATEngine.execute(new ATpsCMEEWrite(PpsCMEE.ErrorMode.ENABLE_WITH_STRING_VALUES));
        rawATEngine.execute(new ATVExec(true));
        rawATEngine.execute(new ATadKExec(PadK.FlowControl.HARDWARE_BI_DIRECTIONAL_FLOW_CONTROL));
        rawATEngine.execute(new ATadDExec(PadD.DTRMode.HIGH_TO_LOW_TRANSITION_CURRENT_CONNECTION_CLOSED));
        rawATEngine.execute(new AThhSLEDWrite(PhhSLED.Mode.GPIO_HANDLED_BY_MODULE_SOFTWARE));
        rawATEngine.execute(new AThhCMUXMODEWrite(PhhCMUXMODE.Mode.NEW_BREAK_OCTECT_FORMAT_AND_IGNORE_DTR_FEATURE_IS_ENABLED));
        rawATEngine.execute(new ATpsCMUXWrite());

        cmuxConnection.connect();

        if (cmuxVirtualPortTraceCollector != null) {
          try {
            cmuxVirtualPortTraceCollector.start(traceHandler);
          } catch (ChannelException | InterruptedException e) {
            cmuxVirtualPortTraceCollector = null;
            try {
              cmuxConnection.disconnect();
            } catch (ChannelException | InterruptedException ce) {
              ce.addSuppressed(e);
              throw ce;
            }
          }
        }
      } catch (ChannelException | InterruptedException e) {
        try {
          rawATEngine.stop();
        } catch (ChannelException | InterruptedException ce) {
          ce.addSuppressed(e);
          throw ce;
        }
        throw e;
      }
    } catch (ChannelException | InterruptedException e) {
      if (traceSubChannelTraceCollector != null) {
        try {
          traceSubChannelTraceCollector.stop();
        } catch (ChannelException | InterruptedException ce) {
          ce.addSuppressed(e);
          throw ce;
        } finally {
          traceSubChannelTraceCollector = null;
        }
      }
      scSubChannelsJoining = null;
      throw e;
    }
  }

  @Override
  void init() throws ChannelException, InterruptedException {
    atEngineExtra.start(atEngineExtraURCs);
    atEngineExtra.execute(new ATpsCMEEWrite(PpsCMEE.ErrorMode.ENABLE_WITH_STRING_VALUES));

    if (cmuxVirtualPortTraceCollector != null) {
      execute(new AThhRTDEWrite(traceMask));
    }

    ATpsGMIExecResult atpsGMIExecResult = execute(new ATpsGMIExec());
    ATpsGMMExecResult atpsGMMExecResult = execute(new ATpsGMMExec());
    ATpsGMRExecResult atpsGMRExecResult = execute(new ATpsGMRExec());
    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - init [{}]/[{}]/[{}]", this, atpsGMIExecResult.getManufacturer(), atpsGMMExecResult.getModel(), atpsGMRExecResult.getRevision());
    }

    execute(new AThhREGMODEWrite(PhhREGMODE.Mode.BASIC));
    execute(new AThhQSSWrite(PhhQSS.Mode.ENABLE_BASIC_UNSOLICITED_INDICATION));
    execute(new AThhRSENWrite(true, 3));
    execute(new ATpsCLVLWrite(12));
    execute(new AThhDVIWrite(PhhDVI.DVIMode.ENABLE_DVI, 1, PhhDVI.ClockMode.DVI_SLAVE));
    execute(new AThhSRPWrite(PhhSRP.RingerPathNumber.CURRENT_SELECTED_AUDIO_PATH));
    execute(new AThhTSVOLWrite(EnumSet.of(PhhTSVOL.ClassOfTones.ALL_CLASSES), true, 14));
    if (options.isDisableVolumeOfIncomingSms()) {
      execute(new AThhTSVOLWrite(EnumSet.of(PhhTSVOL.ClassOfTones.RINGER_TONES, PhhTSVOL.ClassOfTones.ALARM_TONES), true, 0));
    }
    execute(new AThhDTMFWrite(PhhDTMF.DTMFDecoder.ENABLES_DTMF_DECODER));
    execute(new AThhMONIWrite());
    execute(new AThhJDRWrite(PhhJDR.Mode.ENABLE_URC_JDR));
    waitForSIMInsertedStatus();

    if (options.isDisableSat()) {
      execute(new AThhSTIAWrite(0));
    }

    execute(new AThhIMCDENWrite(PhhIMCDEN.Mode.ENABLE_IMSI_CATCHER_DETECTION));
    if (options.getOperatorCode() != null) {
      execute(new ATpsCOPSWrite(PpsCOPS.Mode.MANUAL_OPERATOR_SELECTION, PpsCOPS.Format.NUMERIC, options.getOperatorCode()));
      execute(new ATpsCOPSWrite(PpsCOPS.Mode.SET_ONLY_FORMAT, PpsCOPS.Format.LONG_ALPHANUMERIC));
    } else {
      execute(new ATpsCOPSWrite(PpsCOPS.Mode.AUTOMATIC));
    }

    execute(new ATpsCMGFWrite(PpsCMGF.SMSFormat.PDU));
    execute(new ATpsCSCSWrite(PpsCSCS.Charset.UCS2));
    execute(new ATpsCINDWrite(true, true));
    execute(new ATpsCMERWrite(PpsCMER.Mode.BUFFER_CIEV, Boolean.TRUE, Boolean.TRUE));
    execute(new AThhNCIHWrite(true));

    if (options.getCellLock() == null) {
      execute(new AThhBCCHLOCKWrite());
    } else {
      execute(new AThhBCCHLOCKWrite(options.getCellLock()));
    }
  }

  @Override
  void deinit() throws ChannelException, InterruptedException {
    execute(new ATpsCNMIWrite(PpsCNMI.UrcBufferingOption.BUFFER_URCS_IN_TA_AND_FLUSH_TO_TE,
        PpsCNMI.SMSDeliveringOption.NO_SMS_DELIVER_ROUTED_TO_TA,
        PpsCNMI.BroadcastReportingOption.NO_CBM_INDICATIONS_ROUTED_TO_TE,
        PpsCNMI.SMSStatusReportingOption.NO_SMS_STATUS_REPORT_ROUTED_TO_TE,
        PpsCNMI.BufferedURCHandlingMethod.CLEAR));
    execute(new ATpsCOPSWrite(PpsCOPS.Mode.MANUALLY_DEREGISTER_FROM_NETWORK));
    execute(new ATpsCSSNWrite(false));
    execute(new ATpsCREGWrite(PpsCREG.Mode.DISABLE_CREG_URC));
    execute(new ATpsCMERWrite(PpsCMER.Mode.DISCARD_CIEV, Boolean.FALSE, Boolean.TRUE));
    execute(new ATpsCINDWrite(false, false));
    execute(new AThhDTMFWrite(PhhDTMF.DTMFDecoder.DISABLE_DTMF_DECODER));
    execute(new AThhQSSWrite(PhhQSS.Mode.DISABLE_UNSOLICITED_INDICATION));

    atEngineExtra.stop();
  }

  @Override
  void postAtEngineStop() throws ChannelException, InterruptedException {
    Exception le = null;
    if (cmuxVirtualPortTraceCollector != null) {
      try {
        cmuxVirtualPortTraceCollector.stop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      } finally {
        cmuxVirtualPortTraceCollector = null;
      }
    }
    try {
      cmuxConnection.disconnect();

      rawATEngine.execute(new AThhSHDNExec());
    } catch (ChannelException | InterruptedException e) {
      if (le != null) {
        e.addSuppressed(le);
      }
      le = e;
    }
    try {
      rawATEngine.stop();
    } catch (ChannelException | InterruptedException e) {
      if (le != null) {
        e.addSuppressed(le);
      }
      le = e;
    }
    if (traceSubChannelTraceCollector != null) {
      try {
        traceSubChannelTraceCollector.stop();
      } catch (ChannelException | InterruptedException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      } finally {
        traceSubChannelTraceCollector = null;
      }
    }
    scSubChannelsJoining = null;
    if (le != null) {
      if (le instanceof ChannelException) {
        throw (ChannelException) le;
      } else {
        throw (InterruptedException) le;
      }
    }
  }

  @Override
  void postInit() throws ChannelException, InterruptedException {
    unitLock.lock();
    try {
      execute(new ATpsCREGWrite(PpsCREG.Mode.ENABLE_CREG_URC));
      execute(new ATpsCSSNWrite(true));
      execute(new ATpsCSMSWrite(PpsCSMS.MessagingService.COMPATIBLE_WITH_GSM_27_005));
      execute(new ATpsCNMIWrite(PpsCNMI.UrcBufferingOption.BUFFER_URCS_IN_TA_AND_FLUSH_TO_TE,
          PpsCNMI.SMSDeliveringOption.SMS_DELIVERS_ROUTED_DIRECTLY_TO_TE,
          PpsCNMI.BroadcastReportingOption.NO_CBM_INDICATIONS_ROUTED_TO_TE,
          PpsCNMI.SMSStatusReportingOption.SMS_STATUS_REPORT_ROUTED_TO_TE,
          PpsCNMI.BufferedURCHandlingMethod.CLEAR));
      execute(new AThhECAMWrite(PhhECAM.OnOff.ENABLE_WITH_CALLING_NUMBER));

      if (options.isSuppressIncomingCallSignal()) {
        try {
          execute(new ATpsCCWAWrite(null, PpsCCWA.Mode.DISABLE_CALL_WAITING, EnumSet.of(PpsCCWA.Class.VOICE)));
        } catch (ChannelException e) {
          logger.warn("[{}] - can't execute ATpsCCWAWrite command", this, e);
        }
      }
    } finally {
      unitLock.unlock();
    }
  }

  @Override
  IExtendedCallErrorReport executeReadExtendedCallErrorReport() throws ChannelException, InterruptedException {
    AThhCEERExecResult athhCEERExecResult = execute(new AThhCEERExec());
    return new TelitExtendedCallErrorReport(athhCEERExecResult.getCode());
  }

  @Override
  IExtendedNetworkErrorReport executeReadExtendedNetworkErrorReport() throws ChannelException, InterruptedException {
    AThhCEERNETExecResult athhCEERNETExecResult = execute(new AThhCEERNETExec());
    return new TelitExtendedNetworkErrorReport(athhCEERNETExecResult.getCode());
  }

  @Override
  void executeDTMF(final String dtmfString) throws ChannelException, InterruptedException {
    execute(new ATpsVTSWrite(dtmfString));
  }

  @Override
  void executeDTMF(final char dtmf, final int duration) throws ChannelException, InterruptedException {
    execute(new ATpsVTSWrite(dtmf, duration));
  }

  @Override
  CellInfo[] executeReadCellMonitoring() throws ChannelException, InterruptedException {
    AThhMONIExecResult athhMONIExecResult = execute(new AThhMONIExec());
    if (logger.isTraceEnabled()) {
      StringBuilder sb = new StringBuilder("#MONI: ").append(System.lineSeparator());
      sb.append("<<").append(System.lineSeparator());
      for (AThhMONIExecResult.CellInfo cellInfo : athhMONIExecResult.getCellList()) {
        sb.append(cellInfo.getCell()).append("/")
            .append(cellInfo.getBsic()).append("/")
            .append(cellInfo.getLac()).append("/")
            .append(cellInfo.getCellId()).append("/")
            .append(cellInfo.getArfcn()).append("/")
            .append(cellInfo.getPower()).append("/")
            .append(cellInfo.getC1()).append("/")
            .append(cellInfo.getC2()).append("/")
            .append(cellInfo.getTa()).append("/")
            .append(cellInfo.getRxQual()).append("/");
        if (cellInfo.getPlmn() != null) {
          sb.append(cellInfo.getPlmn());
        }
        sb.append(System.lineSeparator());
      }
      sb.append(">>").append(System.lineSeparator());
      logger.trace("[{}] - {}", this, sb.toString());
    }
    return convertToCellInfo(athhMONIExecResult.getCellList());
  }

  private CellInfo[] convertToCellInfo(final List<AThhMONIExecResult.CellInfo> telitCellInfos) {
    List<CellInfo> cellInfos = new ArrayList<>();
    for (AThhMONIExecResult.CellInfo cellInfo : telitCellInfos) {
      if (cellInfo.getBsic() == 0 && cellInfo.getLac() == 0 && cellInfo.getCellId() == 0 && cellInfo.getArfcn() == 1024 &&
          cellInfo.getPower() == -111 && cellInfo.getC1() == -1 && cellInfo.getC2() == -1) {
        logger.trace("[{}] - cellInfo: {} is empty - ignored", this, cellInfo);
      } else if ((cellInfo.getArfcn() < 0 || cellInfo.getArfcn() > 124) &&
          (cellInfo.getArfcn() < 512 || cellInfo.getArfcn() > 885) &&
          (cellInfo.getArfcn() < 975 || cellInfo.getArfcn() > 1023)) {
        logger.warn("[{}] - cellInfo: {} has arfcn: {} value not in the range of (0-124,975-1023,512-885) - ignored", this, cellInfo, cellInfo.getArfcn());
      } else {
        cellInfos.add(new CellInfo(cellInfo.getBsic(), cellInfo.getLac(), cellInfo.getCellId(), cellInfo.getArfcn(),
            cellInfo.getPower(), cellInfo.getC1(), cellInfo.getC2(),
            cellInfo.getTa(), cellInfo.getRxQual(), cellInfo.getPlmn()));
      }
    }
    return cellInfos.toArray(new CellInfo[cellInfos.size()]);
  }

  private void waitForSIMInsertedStatus() throws ChannelException, InterruptedException {
    if (!simInsertedStatusSemaphore.tryAcquire(SIM_INSERTED_STATUS_TIMEOUT, TimeUnit.MILLISECONDS)) {
      throw new MobileStationUnitSimInsertedTimeoutException("[" + this + "] - SIM inserted status waiting timeout elapsed");
    }
  }

  // helpers methods

  @Override
  <T extends IATResult> T executeExtra(final IAT<T> command) throws ChannelException, InterruptedException {
    return atEngineExtra.execute(command);
  }

  // internal handler's classes

  private class RSENHandler implements IURChhRSENHandler {

    @Override
    public void handleRemoteSIMEnable(final boolean connected) {
      logger.debug("[{}] - remote SIM connected: [{}]", GSMTelitMobileStationUnit.this, connected);
    }

  }

  private class QSSHandler implements IURChhQSSHandler {

    @Override
    public void handleStatus(final PhhQSS.Status status) {
      logger.trace("[{}] - SIM status: [{}]", GSMTelitMobileStationUnit.this, status);
      if (status != PhhQSS.Status.SIM_NOT_INSERTED) {
        simInsertedStatusSemaphore.release();
      }
    }

  }

  private class DTMFEVHandler implements IURChhDTMFEVHandler {

    @Override
    public void handleDTMF(final char dtmf) {
      logger.trace("[{}] - receive dtmf: [{}]", GSMTelitMobileStationUnit.this, dtmf);
      if (!mobileStationEvents.offer(new DtmfMobileStationEvent(dtmf))) {
        throw new AssertionError();
      }
    }

  }

  private class IMCDHandler implements IURChhIMCDHandler {

    @Override
    public void handleStatus(final PhhIMCDEN.Status status) {
      logger.info("[{}] - receive IMCD status: [{}]", GSMTelitMobileStationUnit.this, status);
      if (status == PhhIMCDEN.Status.IMSI_CATCHER_DETECTED) {
        externalErrorHandler.handleIMSICatcherDetected();
      }
    }

  }

  private class JDRHandler implements IURChhJDRHandler {

    @Override
    public void handleStatus(final PhhJDR.Status status) {
      logger.info("[{}] - receive JDR status: [{}]", GSMTelitMobileStationUnit.this, status);
      switch (status) {
        case JAMMED:
          externalErrorHandler.handleJammedDetected();
          break;
        case OPERATIVE:
          externalErrorHandler.handleNormalOperatingRestored();
          break;
      }
    }

  }

  private class ECAMHandler implements IURChhECAMHandler {

    @Override
    public void handleCallEvent(final int callId, final PhhECAM.CallStatus callStatus, final PhhECAM.CallType callType, final String calledNumber, final PhhECAM.TypeOfNumber typeOfNumber) {
      logger.debug("[{}] - receive ECAM callId: [{}], callStatus: [{}], callType: [{}], calledNumber: [{}], typeOfNumber: [{}]", GSMTelitMobileStationUnit.this, callId, callStatus, callType,
          calledNumber, typeOfNumber);

      MobileStationEvent mobileStationEvent;
      switch (callStatus) {
        case ACTIVE:
          mobileStationEvent = new ActiveCallMobileStationEvent(callId);
          break;
        case HOLD:
          mobileStationEvent = new HeldCallMobileStationEvent(callId);
          break;
        case CONNECTING:
          mobileStationEvent = new AlertingCallMobileStationEvent(callId);
          break;
        case ALERTING:
          mobileStationEvent = new IncomingCallMobileStationEvent(callId, calledNumber);
          break;
        case WAITING:
          mobileStationEvent = new WaitingCallMobileStationEvent(callId);
          break;
        default:
          // CALLING status ignored as not useful for now
          // BUSY status ignored because processed through Busy handler
          // IDLE status ignored because processed through NoCarrier and NoDialtone handlers
          mobileStationEvent = null;
      }
      if (mobileStationEvent != null) {
        if (!mobileStationEvents.offer(mobileStationEvent)) {
          throw new AssertionError();
        }
      }
    }

  }

}
