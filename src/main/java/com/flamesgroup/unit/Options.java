/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

public final class Options {

  private final String operatorCode;
  private final boolean suppressIncomingCallSignal;
  private final long registrationTimeout;
  private final long ignoreRegistrationDeniedTimeout;
  private final boolean disablePhase2pSupport;
  private final Integer cellLockArfcn;
  private final boolean disableSat;
  private final boolean disableVolumeOfIncomingSms;
  private final Integer cpuMode;

  private Options(final Builder builder) {
    this.operatorCode = builder.operatorCode;
    this.suppressIncomingCallSignal = builder.suppressIncomingCallSignal;
    this.registrationTimeout = builder.registrationTimeout;
    this.ignoreRegistrationDeniedTimeout = builder.ignoreRegistrationDeniedTimeout;
    this.disablePhase2pSupport = builder.disablePhase2pSupport;
    this.cellLockArfcn = builder.cellLockArfcn;
    this.disableSat = builder.disableSat;
    this.disableVolumeOfIncomingSms = builder.disableVolumeOfIncomingSms;
    this.cpuMode = builder.cpuMode;
  }

  public String getOperatorCode() {
    return operatorCode;
  }

  public boolean isSuppressIncomingCallSignal() {
    return suppressIncomingCallSignal;
  }

  public long getRegistrationTimeout() {
    return registrationTimeout;
  }

  public long getIgnoreRegistrationDeniedTimeout() {
    return ignoreRegistrationDeniedTimeout;
  }

  public boolean isDisablePhase2pSupport() {
    return disablePhase2pSupport;
  }

  public Integer getCellLock() {
    return cellLockArfcn;
  }

  public boolean isDisableSat() {
    return disableSat;
  }

  public boolean isDisableVolumeOfIncomingSms() {
    return disableVolumeOfIncomingSms;
  }

  public Integer getCpuMode() {
    return cpuMode;
  }

  public static class Builder {

    // Optional parameters - initialized to default values
    private String operatorCode;
    private boolean suppressIncomingCallSignal = false;
    private long registrationTimeout = 60 * 1000;
    private long ignoreRegistrationDeniedTimeout = 30 * 1000;
    private boolean disablePhase2pSupport = false;
    private Integer cellLockArfcn;
    private boolean disableSat = false;
    private boolean disableVolumeOfIncomingSms = false;
    private Integer cpuMode;

    public Builder operatorCode(final String code) {
      operatorCode = code;
      return this;
    }

    public Builder suppressIncomingCallSignal(final boolean suppress) {
      suppressIncomingCallSignal = suppress;
      return this;
    }

    public Builder registrationTimeout(final long timeout) {
      if (timeout <= 0) {
        throw new IllegalArgumentException("timeout must be greater 0");
      }
      registrationTimeout = timeout;
      return this;
    }

    public Builder igonreRegistrationDeniedTimeout(final long timeout) {
      if (timeout <= 0) {
        throw new IllegalArgumentException("timeout must be greater 0");
      }
      ignoreRegistrationDeniedTimeout = timeout;
      return this;
    }

    public Builder disablePhase2pSupport() {
      disablePhase2pSupport = true;
      return this;
    }

    public Builder cellLock(final Integer arfcn) {
      cellLockArfcn = arfcn;
      return this;
    }

    public Builder disableSat() {
      disableSat = true;
      return this;
    }

    public Builder disableVolumeOfIncomingSms(final boolean disable) {
      disableVolumeOfIncomingSms = disable;
      return this;
    }

    public Builder cpuMode(final Integer cpuMode) {
      this.cpuMode = cpuMode;
      return this;
    }

    public Options build() {
      return new Options(this);
    }

  }

}
