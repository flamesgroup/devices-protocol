/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.io.Serializable;

public final class CellInfo implements Serializable {

  private static final long serialVersionUID = 3179940929850282117L;

  private final int bsic;
  private final int lac;
  private final int cellId;
  private final int arfcn;
  private final int rxLev;
  private final int c1;
  private final int c2;

  private final int ta;
  private final int rxQual;
  private final String plmn;

  private transient String toS;

  /**
   * Cell monitoring parameters from Telit module
   *
   * @param bsic   base station identity code
   * @param lac    location area code
   * @param cellId cell identifier
   * @param arfcn  absolute frequency channel number
   * @param rxLev  received signal level in dBm
   * @param c1     cell selection criteria
   * @param c2     cell reselection criteria
   * @param ta     timing advance
   * @param rxQual quality of reception
   * @param plmn   public land mobile network
   * @see com.flamesgroup.device.gsmb.atengine.at.telit.AThhMONIExec
   * @see com.flamesgroup.device.gsmb.atengine.at.telit.AThhMONIExecResult
   */
  public CellInfo(final int bsic, final int lac, final int cellId, final int arfcn, final int rxLev, final int c1, final int c2, final int ta, final int rxQual, final String plmn) {
    this.bsic = bsic;
    this.lac = lac;
    this.cellId = cellId;
    this.arfcn = arfcn;
    this.rxLev = rxLev;
    this.c1 = c1;
    this.c2 = c2;

    this.ta = ta;
    this.rxQual = rxQual;
    this.plmn = plmn;
  }

  /**
   * Cell monitoring parameters from Siemens module
   *
   * @param bsic   base station identity code
   * @param lac    location area code
   * @param cellId cell identifier
   * @param arfcn  absolute frequency channel number
   * @param rxLev  received signal level in dBm
   * @param c1     cell selection criteria
   * @param c2     cell reselection criteria
   * @see com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMONCExec
   * @see com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMONCExecResult
   */
  public CellInfo(final int bsic, final int lac, final int cellId, final int arfcn, final int rxLev, final int c1, final int c2) {
    this(bsic, lac, cellId, arfcn, rxLev, c1, c2, -1, -1, null);
  }

  public int getBsic() {
    return bsic;
  }

  public int getLac() {
    return lac;
  }

  public int getCellId() {
    return cellId;
  }

  public int getArfcn() {
    return arfcn;
  }

  public int getRxLev() {
    return rxLev;
  }

  public int getC1() {
    return c1;
  }

  public int getC2() {
    return c2;
  }

  public int getTa() {
    return ta;
  }

  public int getRxQual() {
    return rxQual;
  }

  public String getPlmn() {
    return plmn;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof CellInfo)) {
      return false;
    }
    CellInfo cellInfo = (CellInfo) object;

    return bsic == cellInfo.bsic && lac == cellInfo.lac && cellId == cellInfo.cellId && arfcn == cellInfo.arfcn && rxLev == cellInfo.rxLev;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + bsic;
    result = prime * result + lac;
    result = prime * result + cellId;
    result = prime * result + arfcn;
    result = prime * result + rxLev;
    return result;
  }

  @Override
  public String toString() {
    if (toS == null) {
      toS = String.format("%s@%x:[%d,%d,%d,%d,%d]", getClass().getSimpleName(), hashCode(), bsic, lac, cellId, arfcn, rxLev);
    }
    return toS;
  }

}
