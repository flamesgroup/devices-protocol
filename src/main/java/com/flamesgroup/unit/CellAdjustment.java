/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.util.Objects;

public final class CellAdjustment {

  public final int MAX_VALUE = 60;
  public final int MIN_VALUE = -60;

  private final int arfcn;
  private final int rxLevAdjust;

  public CellAdjustment(final int arfcn, final int rxLevAdjust) {
    if (arfcn < 0) {
      throw new IllegalArgumentException("arfcn must be more or equal than 0, but it's " + arfcn);
    }
    if (rxLevAdjust < MIN_VALUE || MAX_VALUE < rxLevAdjust) {
      throw new IllegalArgumentException("rxLevAdjust must be more or equal than " + MIN_VALUE + " and less or equal than " + MAX_VALUE + ", but it's " + rxLevAdjust);
    }
    this.arfcn = arfcn;
    this.rxLevAdjust = rxLevAdjust;
  }

  public int getArfcn() {
    return arfcn;
  }

  public int getRxLevAdjust() {
    return rxLevAdjust;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CellAdjustment that = (CellAdjustment) o;
    return arfcn == that.arfcn &&
        rxLevAdjust == that.rxLevAdjust;
  }

  @Override
  public int hashCode() {
    return Objects.hash(arfcn, rxLevAdjust);
  }

}
