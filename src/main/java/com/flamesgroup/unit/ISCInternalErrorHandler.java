/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.SCEmulatorError;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderState;

public interface ISCInternalErrorHandler {

  default void handleSCReaderSubChannelErrorState(final SCReaderError error, final SCReaderState state) {
  }

  default void handleSCReaderSubChannelException(final ChannelException e) {
  }

  default void handleSCEmulatorSubChannelErrorState(final SCEmulatorError error, final SCEmulatorState state) {
  }

  default void handleSCEmulatorSubChannelException(final ChannelException e) {
  }

}
