/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.util.Objects;

public final class RegistrationStatus {

  private final InternalRegistrationStatus internalRegistrationStatus;
  private final NetworkRegistrationStatus networkRegistrationStatus;

  public RegistrationStatus(final InternalRegistrationStatus internalRegistrationStatus, final NetworkRegistrationStatus networkRegistrationStatus) {
    Objects.requireNonNull(internalRegistrationStatus, "internalRegistrationStatus mustn't be null");
    Objects.requireNonNull(networkRegistrationStatus, "networkRegistrationStatus mustn't be null");
    this.internalRegistrationStatus = internalRegistrationStatus;
    this.networkRegistrationStatus = networkRegistrationStatus;
  }

  public RegistrationStatus(final InternalRegistrationStatus internalRegistrationStatus) {
    this(internalRegistrationStatus, NetworkRegistrationStatus.INTERNAL_ERROR);
  }

  public RegistrationStatus(final NetworkRegistrationStatus networkRegistrationStatus) {
    this(InternalRegistrationStatus.NO_ERROR, networkRegistrationStatus);
  }

  public InternalRegistrationStatus getInternalRegistrationStatus() {
    return internalRegistrationStatus;
  }

  public NetworkRegistrationStatus getNetworkRegistrationStatus() {
    return networkRegistrationStatus;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof RegistrationStatus)) {
      return false;
    }
    RegistrationStatus registrationStatus = (RegistrationStatus) object;

    return internalRegistrationStatus == registrationStatus.internalRegistrationStatus &&
        networkRegistrationStatus == registrationStatus.networkRegistrationStatus;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + internalRegistrationStatus.hashCode();
    result = prime * result + networkRegistrationStatus.hashCode();
    return result;
  }

  public enum NetworkRegistrationStatus {

    NOT_REGISTERED_NOT_SEARCHING,
    REGISTERED_TO_HOME_NETWORK,
    NOT_REGISTERED_AND_SEARCHING,
    REGISTRATION_DENIED,
    UNKNOWN,
    REGISTERED_ROAMING,

    INTERNAL_ERROR, // dummy null status

  }

  public enum InternalRegistrationStatus {

    NO_ERROR,
    TIMEOUT_ERROR,
    POST_INIT_ERROR,

  }

}
