/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import com.flamesgroup.device.sc.APDUException;

public class IllegalCHVStateError extends APDUException {

  private static final long serialVersionUID = 5902363901691287087L;

  public IllegalCHVStateError() {
    super();
  }

  public IllegalCHVStateError(final String message) {
    super(message);
  }

  public IllegalCHVStateError(final String message, final Throwable cause) {
    super(message, cause);
  }

  public IllegalCHVStateError(final Throwable cause) {
    super(cause);
  }

}
