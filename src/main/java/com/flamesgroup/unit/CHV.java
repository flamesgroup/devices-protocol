/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import static java.lang.System.arraycopy;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

public class CHV implements Serializable {

  private static final long serialVersionUID = 3771076462151871767L;

  private final byte[] chv;

  private final String toS;

  public CHV(final String chv) {
    Objects.requireNonNull(chv, "chvValue must not be null");
    validateCHV(chv);

    this.chv = new byte[8];
    Arrays.fill(this.chv, (byte) 0xFF);
    byte[] chvBytes = chv.getBytes();
    arraycopy(chvBytes, 0, this.chv, 0, chvBytes.length);

    toS = String.format("%s@%x:[%s]", getClass().getSimpleName(), hashCode(), chv);
  }

  public byte[] getData() {
    return chv;
  }

  protected void validateLength(final int length) {
    if (length < 4 || length > 8) {
      throw new IllegalArgumentException("CHV size must be between 4 and 8 digits, but it's " + length);
    }
  }

  private void validateCHV(final String chv) {
    validateLength(chv.length());
    for (int i = 0; i < chv.length(); i++) {
      if (!Character.isDigit(chv.charAt(i))) {
        throw new IllegalArgumentException("CHV code must consist only form digits, but it's " + chv);
      }
    }
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof CHV)) {
      return false;
    }

    CHV that = (CHV) obj;
    return Arrays.equals(this.chv, that.chv);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(chv);
  }

  @Override
  public String toString() {
    return toS;
  }

  public static void encoding(final CHV chv, final ByteBuffer byteBuffer) {
    byteBuffer.put(chv.getData());
  }

}
