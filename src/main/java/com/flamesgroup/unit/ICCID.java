/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.io.Serializable;
import java.util.Objects;

public final class ICCID implements Serializable {

  private static final long serialVersionUID = 3596145819031140652L;

  private final String value;

  private transient String toS;

  public ICCID(final String value) {
    Objects.requireNonNull(value, "iccid value must not be null");
    if (value.length() < 2 || value.length() > 20) {
      throw new IllegalArgumentException("iccid value length mustn't be less than 2 and more than 20, but it's [" + value.length() + "]");
    }
    if (!value.matches("\\d+")) {
      throw new IllegalArgumentException("iccid value must contain only digits, but it's [" + value + "]");
    }

    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ICCID)) {
      return false;
    }

    ICCID that = (ICCID) object;
    return this.value.equals(that.value);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + value.hashCode();
    return result;
  }

  @Override
  public String toString() {
    if (toS == null) {
      toS = String.format("%s:[%s]", getClass().getSimpleName(), value);
    }
    return toS;
  }

}
