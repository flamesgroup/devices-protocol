/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.sc.APDUException;

public interface ISIMCardUnit {

  void turnOn() throws ChannelException, InterruptedException;

  void turnOff() throws ChannelException, InterruptedException;

  ICCID readICCID() throws ChannelException, APDUException, InterruptedException;

  IMSI readIMSI() throws ChannelException, APDUException, InterruptedException;

  String readOperatorName() throws ChannelException, APDUException, InterruptedException;

  PhoneNumber readPhoneNumber() throws ChannelException, APDUException, InterruptedException;

  boolean isCHV1Enabled() throws ChannelException, APDUException, InterruptedException;

  CHVStatus readCHV1status() throws ChannelException, APDUException, InterruptedException;

  CHVStatus readUnblockCHV1status() throws ChannelException, APDUException, InterruptedException;

  void verifyCHV1(CHV chv1) throws ChannelException, APDUException, InterruptedException;

  void changeCHV1(CHV oldCHV1, CHV newCHV1) throws ChannelException, APDUException, InterruptedException;

  void disableCHV1(CHV chv1) throws ChannelException, APDUException, InterruptedException;

  void enableCHV1(CHV chv1) throws ChannelException, APDUException, InterruptedException;

  void unblockCHV1(UnblockCHV unblockCHV1, CHV newCHV1) throws ChannelException, APDUException, InterruptedException; // PUK1

  CHVStatus readCHV2status() throws ChannelException, APDUException, InterruptedException;

  CHVStatus readUnblockCHV2status() throws ChannelException, APDUException, InterruptedException;

  void verifyCHV2(CHV chv2) throws ChannelException, APDUException, InterruptedException;

  void changeCHV2(CHV oldCHV2, CHV newCHV2) throws ChannelException, APDUException, InterruptedException;

  void unblockCHV2(UnblockCHV unblockCHV2, CHV newCHV2) throws ChannelException, APDUException, InterruptedException; // PUK2

}
