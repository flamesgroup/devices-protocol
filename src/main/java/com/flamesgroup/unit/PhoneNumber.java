/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;

public final class PhoneNumber implements Serializable {

  private static final long serialVersionUID = 1659866743555057401L;

  private static final String PRIVATE_NUMBER = "Private number";
  private static final String NUMBER_TEMPLATE = "[+*#\\d]+";
  private static final Pattern numberPattern = Pattern.compile(NUMBER_TEMPLATE);

  private final String value;
  private final boolean privateNumber;

  public PhoneNumber() {
    this.value = PRIVATE_NUMBER;
    this.privateNumber = true;
  }

  public PhoneNumber(final String phoneNumber) {
    if (!isValid(phoneNumber)) {
      throw new IllegalArgumentException(String.format("Invalid phoneNumber: %s", phoneNumber));
    }

    this.value = phoneNumber;
    this.privateNumber = false;
  }

  public String getValue() {
    return value;
  }

  public boolean isPrivate() {
    return privateNumber;
  }

  public static boolean isValid(final String phoneNumber) {
    Objects.requireNonNull(phoneNumber, "phoneNumber mustn't be null");
    return numberPattern.matcher(phoneNumber).matches();
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof PhoneNumber)) {
      return false;
    }

    PhoneNumber that = (PhoneNumber) object;
    return value.equals(that.value);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + value.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return String.format("%s:[%s]", getClass().getSimpleName(), value);
  }

}
