/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

public class UnblockCHV extends CHV {

  private static final long serialVersionUID = 1469083969491009895L;

  public UnblockCHV(final String chvString) {
    super(chvString);
  }

  @Override
  protected void validateLength(final int length) {
    if (length != 8) {
      throw new IllegalArgumentException("Unblock CHV size must be 8 digits, but it's " + length);
    }
  }

}
