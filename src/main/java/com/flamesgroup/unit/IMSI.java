/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.io.Serializable;
import java.util.Objects;

public final class IMSI implements Serializable {

  private static final long serialVersionUID = -2191584795970924761L;

  private final String imsiStr;

  private transient String toS;

  public IMSI(final String imsiStr) {
    Objects.requireNonNull(imsiStr, "imsiStr must not be null");
    if (imsiStr.length() > 15) { // this condition goes from SIM Card file structure
      throw new IllegalArgumentException("imsiStr length must <= 15, but it's [" + imsiStr.length() + "]");
    }
    if (!imsiStr.matches("\\d+")) {
      throw new IllegalArgumentException("imsiStr must contain only digits, but it's [" + imsiStr + "]");
    }

    this.imsiStr = imsiStr;
  }

  public String getValue() {
    return imsiStr;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof IMSI)) {
      return false;
    }
    IMSI imsi = (IMSI) object;

    return imsiStr.equals(imsi.getValue());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + imsiStr.hashCode();
    return result;
  }

  @Override
  public String toString() {
    if (toS == null) {
      toS = String.format("%s:[%s]", getClass().getSimpleName(), imsiStr);
    }
    return toS;
  }

}
