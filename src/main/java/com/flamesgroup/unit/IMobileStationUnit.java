/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.http.IHTTPSession;
import com.flamesgroup.device.gsmb.atengine.ussd.IUSSDSession;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.unit.sms.SMSMessageClass;
import com.flamesgroup.unit.sms.SMSMessageOutbound;

import java.util.List;

public interface IMobileStationUnit {

  byte[] getIMEI() throws ChannelException, InterruptedException;

  void setIMEI(byte[] imei) throws ChannelException, InterruptedException;

  void turnOn(Options options, IIncomingMobileCallHandler incomingCallHandler, IIncomingSMSHandler incomingSMSHandler, IServiceInfoHandler serviceInfoHandler)
      throws ChannelException, InterruptedException;

  void turnOff() throws ChannelException, InterruptedException;

  IMobileOriginatingCall dial(PhoneNumber number, IMobileOriginatingCallHandler originatedCallHandler) throws ChannelException, InterruptedException;

  List<SMSMessageOutbound> sendSMS(PhoneNumber number, String text, SMSMessageClass smsMessageClass, int validityPeriod, boolean needReport)
      throws SMSException, ChannelException, InterruptedException;

  IUSSDSession getUSSDSession();

  IAudioStream getAudioStream();

  ICellEqualizer getCellEqualizer();

  ICellLocker getCellLocker();
  
  IHTTPSession getHTTPSession();

}
