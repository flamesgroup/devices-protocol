/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import java.io.Serializable;

public class CHVStatus implements Serializable {

  private static final long serialVersionUID = 19370651004209747L;

  private final int mask;

  public CHVStatus(final int mask) {
    this.mask = mask;
  }

  public int getNumberOfFalsePresentations() {
    return mask & 0x0F;
  }

  public boolean isBlocked() {
    return (mask & 0x0F) <= 0;
  }

  public boolean isSecretCodeInitialized() {
    return (mask & 0x80) > 0;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof CHVStatus)) {
      return false;
    }

    CHVStatus that = (CHVStatus) obj;
    return this.mask == that.mask;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + mask;
    return result;
  }

}
