/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.channel;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.IMudpChannelHandler;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.mudp.MudpOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Channel implements IChannel {

  private static final Logger logger = LoggerFactory.getLogger(Channel.class);

  private static final byte ERRCODE_NO_ERR = (byte) 0x00;
  private static final byte ERRCODE_UNKNOWN_COMMAND = (byte) 0x10;
  private static final byte ERRCODE_INVALID_COMMAND = (byte) 0x20;
  private static final byte ERRCODE_INVALID_COMMAND_DATA = (byte) 0x30;
  private static final byte ERRCODE_BUSY = (byte) 0x40;
  private static final byte ERRCODE_INTERNAL_ERR = (byte) 0x70;

  private final IMudpChannel mudpChannel;
  private final MudpChannelAddress mudpChannelAddress;
  private final IChannelDataHandler[] attachedChannelDataHandlers = new IChannelDataHandler[16];
  private final Lock attachedChannelDataHandlersLock = new ReentrantLock();

  private final String toS;

  public Channel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int entryNumber, final DeviceUID deviceUid) {
    Objects.requireNonNull(mudpChannel, "MudpChannel must not be null");
    Objects.requireNonNull(mudpChannel, "MudpChannelAddress must not be null");
    this.mudpChannel = mudpChannel;
    this.mudpChannelAddress = mudpChannelAddress;

    {
      StringBuilder sb = new StringBuilder();
      sb.append(getClass().getSimpleName());
      sb.append("@");
      sb.append(hashCode());
      sb.append(":");
      if (entryNumber >= 0 || deviceUid != null) {
        sb.append("[");
        if (deviceUid != null) {
          sb.append(deviceUid.getCanonicalName());
          sb.append("/");
        }
        if (entryNumber >= 0) {
          sb.append(entryNumber);
        }
        sb.append("]");
      }
      toS = sb.toString();
    }

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}] and [{}]", toS, mudpChannel, mudpChannelAddress);
    }
  }

  public Channel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress) {
    this(mudpChannel, mudpChannelAddress, Integer.MIN_VALUE, null);
  }

  @Override
  public boolean isEnslaved() {
    return mudpChannel.isEnslaved();
  }

  @Override
  public void enslave() throws ChannelException, InterruptedException {
    try {
      mudpChannel.enslave(mudpChannelAddress, new MudpChannelHandler());
    } catch (MudpException e) {
      throw new ChannelException(e);
    }
  }

  @Override
  public void free() throws ChannelException {
    try {
      mudpChannel.free();
    } catch (MudpException e) {
      throw new ChannelException(e);
    }
  }

  @Override
  public IChannelData createChannelData() {
    return new ChannelData(this);
  }

  protected long getMaxAttemptTimeout() {
    MudpOptions mudpOptions = mudpChannel.getMudpOptions();
    return mudpOptions.getAttemptCount() * mudpOptions.getAttemptTimeout();
  }

  protected long getMaxRetransmitTimeout() {
    MudpOptions mudpOptions = mudpChannel.getMudpOptions();
    return mudpOptions.getRetransmitCount() * mudpOptions.getRetransmitTimeout();
  }


  void sendReliable(final byte type, final ByteBuffer data) throws MudpException, InterruptedException {
    mudpChannel.sendReliable(type, data);
  }

  void sendUnreliable(final byte type, final ByteBuffer data) throws MudpException {
    mudpChannel.sendUnreliable(type, data);
  }

  void addToAttachedChannelDataHandlers(final byte type, final IChannelDataHandler handler) throws ChannelException {
    attachedChannelDataHandlersLock.lock();
    try {
      if (attachedChannelDataHandlers[type] != null) {
        throw new ChannelException("[" + this + "] - Channel Data Handler with type [" + type + "] already attached");
      } else {
        attachedChannelDataHandlers[type] = handler;
      }
    } finally {
      attachedChannelDataHandlersLock.unlock();
    }
  }

  IChannelDataHandler removeFromAttachedChannelDataHandlers(final byte type) {
    attachedChannelDataHandlersLock.lock();
    try {
      IChannelDataHandler handler = attachedChannelDataHandlers[type];
      attachedChannelDataHandlers[type] = null;
      return handler;
    } finally {
      attachedChannelDataHandlersLock.unlock();
    }
  }

  IChannelDataHandler getAttachedChannelDataHandlerByType(final byte type) {
    attachedChannelDataHandlersLock.lock();
    try {
      return attachedChannelDataHandlers[type];
    } finally {
      attachedChannelDataHandlersLock.unlock();
    }
  }

  @Override
  public String toString() {
    return toS;
  }

  private final class MudpChannelHandler implements IMudpChannelHandler {

    @Override
    public void handleReceive(final byte type, final ByteBuffer data) {
      byte channelDataType = (byte) (type & 0x0F);
      IChannelDataHandler handler = getAttachedChannelDataHandlerByType(channelDataType);
      if (handler != null) {
        byte errCode = (byte) (type & 0xF0);
        if (errCode == ERRCODE_NO_ERR) {
          handler.handleReceive(data);
        } else {
          ChannelErrCodeException exception;
          switch (errCode) {
            case ERRCODE_UNKNOWN_COMMAND:
              exception = new ChannelUnknownCommandException(errCode);
              break;
            case ERRCODE_INVALID_COMMAND:
              exception = new ChannelInvalidCommandException(errCode);
              break;
            case ERRCODE_INVALID_COMMAND_DATA:
              exception = new ChannelInvalidCommandDataException(errCode);
              break;
            case ERRCODE_BUSY:
              exception = new ChannelBusyException(errCode);
              break;
            case ERRCODE_INTERNAL_ERR:
              exception = new ChannelInternalErrException(errCode);
              break;
            default:
              exception = new ChannelErrCodeException(errCode);
          }
          handler.handleErrCode(exception);
        }
      } else {
        logger.warn("[{}] - received data for unattached channel data type [{}] - data dropped", this, channelDataType);
      }
    }

  }

}
