/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.channel;

import com.flamesgroup.device.protocol.mudp.MudpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChannelData implements IChannelData {

  private final Logger logger = LoggerFactory.getLogger(ChannelData.class);

  private final Channel channel;
  private boolean attached;

  private byte type;

  private final Lock channelDataLock = new ReentrantLock();

  private final String toS;

  public ChannelData(final Channel channel) {
    Objects.requireNonNull(channel, "Channel must not be null");
    this.channel = channel;
    this.attached = false;

    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}]", toS, channel);
    }
  }

  // IChannelData
  @Override
  public boolean isAttached() {
    channelDataLock.lock();
    try {
      return attached;
    } finally {
      channelDataLock.unlock();
    }
  }

  @Override
  public void attach(final byte type, final IChannelDataHandler channelDataHandler) throws ChannelException {
    if (type < 0 || type > 0x0F) {
      throw new IllegalArgumentException("type must be >= 0 and <= 16");
    }
    if (channelDataHandler == null) {
      throw new IllegalArgumentException("ChannelDataHandler must not be null");
    }

    channelDataLock.lock();
    try {
      channel.addToAttachedChannelDataHandlers(type, channelDataHandler);
      attached = true;
      logger.trace("[{}] - attached", this);
      this.type = type;
    } finally {
      channelDataLock.unlock();
    }
  }

  @Override
  public void detach() {
    channelDataLock.lock();
    try {
      if (attached) {
        attached = false;
        logger.trace("[{}] - detached", this);
        channel.removeFromAttachedChannelDataHandlers(type);
      } else {
        logger.trace("[{}] - it's not attached", this);
      }
    } finally {
      channelDataLock.unlock();
    }
  }

  @Override
  public void sendReliable(final ByteBuffer data) throws ChannelException, InterruptedException {
    channelDataLock.lock();
    try {
      if (!attached) {
        throw new ChannelDataDetachedException();
      }

      try {
        channel.sendReliable(type, data);
      } catch (MudpException e) {
        throw new ChannelException(e);
      }

    } finally {
      channelDataLock.unlock();
    }
  }

  @Override
  public void sendUnreliable(final ByteBuffer data) throws ChannelException {
    channelDataLock.lock();
    try {
      if (!attached) {
        throw new ChannelDataDetachedException();
      }

      try {
        channel.sendUnreliable(type, data);
      } catch (MudpException e) {
        throw new ChannelException(e);
      }
    } finally {
      channelDataLock.unlock();
    }
  }

  // Object
  @Override
  public String toString() {
    return toS;
  }

}
