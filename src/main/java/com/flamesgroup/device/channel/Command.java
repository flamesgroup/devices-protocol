/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Command<CommandRequest extends ICommandRequest, CommandResponse extends ICommandResponse> {

  private enum State {
    WAITING, EXECUTING, EXECUTED
  }

  private final Logger logger = LoggerFactory.getLogger(Command.class);

  private final byte type;
  private final IChannelData channelData;
  private final long responseTimeout;

  private final Lock responseLock = new ReentrantLock();
  private final Condition responseCondition = responseLock.newCondition();

  private State state = State.WAITING;
  private CommandResponse receivedResponse;
  private ChannelException channelException;
  private final IChannelDataHandler channelDataHandler = new IChannelDataHandler() {

    @Override
    public void handleReceive(final ByteBuffer data) {
      boolean unexpectedResponse = true;
      responseLock.lock();
      try {
        if (state == State.EXECUTING) {
          unexpectedResponse = false;
        }
      } finally {
        responseLock.unlock();
      }

      if (unexpectedResponse) {
        CommandResponse unexpectedReceivedResponse = createResponse();
        try {
          unexpectedReceivedResponse.fromDataByteBuffer(data);
          logger.warn("[{}] - received unexpected Command Response [{}]", Command.this, unexpectedReceivedResponse);
        } catch (ChannelCommandDecodeException e) {
          logger.warn("[" + Command.this + "] - received unexpected undecoded Command Response", e);
        }
      } else {
        CommandResponse expectedReceivedResponse = createResponse();
        try {
          expectedReceivedResponse.fromDataByteBuffer(data);
          responseLock.lock();
          try {
            state = State.EXECUTED;
            receivedResponse = expectedReceivedResponse;
            responseCondition.signalAll();
          } finally {
            responseLock.unlock();
          }
        } catch (ChannelCommandDecodeException e) {
          responseLock.lock();
          try {
            state = State.EXECUTED;
            channelException = e;
            responseCondition.signalAll();
          } finally {
            responseLock.unlock();
          }
        }
      }
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      boolean unexpectedResponse = true;
      responseLock.lock();
      try {
        if (state == State.EXECUTING) {
          unexpectedResponse = false;
        }
      } finally {
        responseLock.unlock();
      }

      if (unexpectedResponse) {
        logger.warn("[{}] - received unexpected ErrCode [{}]", Command.this, e);
      } else {
        responseLock.lock();
        try {
          state = State.EXECUTED;
          channelException = specifyErrCode(e);
          responseCondition.signalAll();
        } finally {
          responseLock.unlock();
        }
      }
    }

  };

  public Command(final byte type, final IChannelData channelData, final long responseTimeout) {
    super();
    this.type = type;
    this.channelData = channelData;
    this.responseTimeout = responseTimeout;
  }

  public CommandResponse execute(final CommandRequest commandRequest) throws ChannelException, InterruptedException {
    responseLock.lock();
    try {
      if (state != State.WAITING) {
        throw new CommandNowExecutingException();
      }
      state = State.EXECUTING;
    } finally {
      responseLock.unlock();
    }

    try {
      channelData.attach(type, channelDataHandler);
      try {
        channelData.sendReliable(commandRequest.toDataByteBuffer());

        long executeBeginTimeMillis = System.currentTimeMillis();
        responseLock.lock();
        try {
          if (state == State.EXECUTING) {
            if (!responseCondition.await(responseTimeout, TimeUnit.MILLISECONDS)) {
              throw new ChannelTimeoutException("[" + this + "] - response timeout elapsed for request [" + commandRequest + "]");
            }
          }
          if (state == State.EXECUTED) {
            state = State.WAITING;
            if (receivedResponse != null) {
              return receivedResponse;
            }
            if (channelException != null) {
              throw channelException;
            }
          }
          throw new AssertionError(); // we must not be here
        } finally {
          responseLock.unlock();
          long executeEndTimeMillis = System.currentTimeMillis();
          if (logger.isTraceEnabled()) {
            logger.trace("[{}] - execution time millis [{}]", this, executeEndTimeMillis - executeBeginTimeMillis);
          }
        }
      } finally {
        channelData.detach();
      }
    } finally {
      responseLock.lock();
      try {
        state = State.WAITING;
        receivedResponse = null;
        channelException = null;
      } finally {
        responseLock.unlock();
      }
    }
  }

  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    return e;
  }

  public abstract CommandRequest createRequest();

  public abstract CommandResponse createResponse();

}
