/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.channel;

public class ChannelCommandEncodeException extends ChannelException {

  private static final long serialVersionUID = -8385063242499937618L;

  public ChannelCommandEncodeException() {
    super();
  }

  public ChannelCommandEncodeException(final String message) {
    super(message);
  }

  public ChannelCommandEncodeException(final Throwable throwable) {
    super(throwable);
  }

  public ChannelCommandEncodeException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
