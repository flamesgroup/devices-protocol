/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.command.SetIndicationModeCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IndicationChannel extends Channel implements IIndicationChannel {

  private static final Logger logger = LoggerFactory.getLogger(IndicationChannel.class);

  public static final byte COMMAND_SET_INDICATION_MODE = (byte) 0x00;

  private final Lock setIndicationModeLock = new ReentrantLock();
  private final SetIndicationModeCommand setIndicationModeCommand;

  public IndicationChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int commonEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, commonEntryNumber, deviceUid);

    setIndicationModeCommand = new SetIndicationModeCommand(COMMAND_SET_INDICATION_MODE, createChannelData(), 75 + getMaxAttemptTimeout());
  }

  public IndicationChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int commonEntryNumber) {
    this(mudpChannel, mudpChannelAddress, commonEntryNumber, null);
  }

  // IIndicationChannel
  @Override
  public void setIndicationMode(final IndicationMode indicationMode) throws ChannelException, InterruptedException {
    setIndicationModeLock.lock();
    try {
      setIndicationModeCommand.execute(setIndicationModeCommand.createRequest().setIndicationMode(indicationMode));
      logger.debug("[{}] - set indication mode [{}]", this, indicationMode);
    } finally {
      setIndicationModeLock.unlock();
    }
  }

}
