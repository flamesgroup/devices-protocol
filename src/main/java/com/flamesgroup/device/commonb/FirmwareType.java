/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import java.util.HashMap;
import java.util.Map;

public enum FirmwareType {

  UNKNOWN(0x00000000),
  FWUPDATER(0x00000001),
  GSMB_GSM(0x00000002),
  GSMB_SIM(0x00000004),
  SIMB_LAN(0x00000008),
  SIMB_SIM(0x00000010),
  GSMB2_LAN(0x00000020),
  GSMB2_GSM(0x00000040),
  FTB(0x00000080),
  GSMB3_LAN(0x00000100),
  SBOOT(0x80000000),
  GSMShC_MAIN_C(0x81000000),
  SIMShC_MAIN_L(0x82000000),
  SIMShC_MAIN_R(0x82000001);

  private final int n;

  FirmwareType(final int type) {
    this.n = type;
  }

  public int getN() {
    return n;
  }

  public static FirmwareType getFirmwareTypeByN(final int n) {
    FirmwareType firmwareType = mapN2Enum.get(n);
    if (firmwareType == null) {
      return UNKNOWN;
    } else {
      return firmwareType;
    }
  }

  private static final Map<Integer, FirmwareType> mapN2Enum = new HashMap<>();

  static {
    for (FirmwareType firmwareType : FirmwareType.values()) {
      mapN2Enum.put(firmwareType.getN(), firmwareType);
    }
  }

}
