/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;

import java.util.concurrent.atomic.AtomicBoolean;

public class IndicationChannelEmpty implements IIndicationChannel {

  private final AtomicBoolean enslaved = new AtomicBoolean(false);

  // IChannel
  @Override
  public boolean isEnslaved() {
    return enslaved.get();
  }

  @Override
  public void enslave() throws ChannelException, InterruptedException {
    enslaved.set(true);
  }

  @Override
  public void free() throws ChannelException {
    enslaved.set(false);
  }

  @Override
  public IChannelData createChannelData() {
    throw new UnsupportedOperationException();
  }

  // IIndicationChannel
  @Override
  public void setIndicationMode(final IndicationMode indicationMode) throws ChannelException, InterruptedException {
  }

}
