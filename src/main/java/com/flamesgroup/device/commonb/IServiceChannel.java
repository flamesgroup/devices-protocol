/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannel;

public interface IServiceChannel extends IChannel {

  ServiceInfo getInfo() throws ChannelException, InterruptedException;

  void reset(long timeout) throws ChannelException, InterruptedException;

  FWBlock readFWBlock(short blockNumber) throws ChannelException, InterruptedException;

  void writeFWBlock(short blockNumber, FWBlock fwBlock) throws ChannelException, InterruptedException;

  void writeCipheredFWBlock(short blockNumber, CipheredFWBlock cipheredFWBlock) throws ChannelException, InterruptedException;

  void applyFW(int crc) throws ChannelException, InterruptedException;

  void changeFWSerialNumber(byte[] cipheredData) throws ChannelException, InterruptedException;

  EEPROMBuffer readEEPROMBuffer(int address, int length) throws ChannelException, InterruptedException;

  void writeEEPROMBuffer(int address, EEPROMBuffer eepromBuffer) throws ChannelException, InterruptedException;

  byte[] readCpuUid() throws ChannelException, InterruptedException;

  void updateLicenseCurrentTime(long currentTimeMillis) throws ChannelException, InterruptedException;

  void updateLicenseExpireTime(byte[] key) throws ChannelException, InterruptedException;

  long readLicenseExpireTime() throws ChannelException, InterruptedException;

  LicenseState getLicenseState() throws ChannelException, InterruptedException;

}
