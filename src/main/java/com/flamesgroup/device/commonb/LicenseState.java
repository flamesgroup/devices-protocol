/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

public enum LicenseState {

  ACTIVE((byte) 0),
  LICENSE_STATE_FREEZE((byte) 1),
  LICENSE_STATE_EXPIRE((byte) 2),
  LICENSE_STATE_LOCK((byte) 3);

  private final byte n;

  LicenseState(final byte n) {
    this.n = n;
  }

  public byte getN() {
    return n;
  }

  public static LicenseState getLicenseStateByN(final byte n) {
    for (LicenseState licenseState : values()) {
      if (licenseState.getN() == n) {
        return licenseState;
      }
    }
    return null;
  }

}
