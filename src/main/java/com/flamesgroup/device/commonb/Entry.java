/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.DeviceUID;

import java.util.concurrent.atomic.AtomicReference;

public abstract class Entry implements IEntry {

  private final int number;
  private final DeviceUID deviceUid;

  private final AtomicReference<String> atomicToS = new AtomicReference<>();

  public Entry(final int number, final DeviceUID deviceUid) {
    this.number = number;
    this.deviceUid = deviceUid;
  }

  @Override
  public int getNumber() {
    return number;
  }

  @Override
  public DeviceUID getDeviceUID() {
    return deviceUid;
  }

  @Override
  public String toString() {
    String toS = atomicToS.get();
    if (toS == null) {
      if (deviceUid == null) {
        toS = String.format("%s@%x:[%d]", getClass().getSimpleName(), hashCode(), number);
      } else {
        toS = String.format("%s@%x:[%s/%d]", getClass().getSimpleName(), hashCode(), deviceUid.getCanonicalName(), number);
      }

      if (!atomicToS.compareAndSet(null, toS)) {
        return atomicToS.get();
      }
    }
    return toS;
  }

}
