/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.command.ChangeFWSerialNumberCommand.ChangeFWSerialNumberRequest;
import com.flamesgroup.device.commonb.command.ChangeFWSerialNumberCommand.ChangeFWSerialNumberResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ChangeFWSerialNumberCommand extends Command<ChangeFWSerialNumberRequest, ChangeFWSerialNumberResponse> {

  public ChangeFWSerialNumberCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public ChangeFWSerialNumberRequest createRequest() {
    return new ChangeFWSerialNumberRequest();
  }

  @Override
  public ChangeFWSerialNumberResponse createResponse() {
    return new ChangeFWSerialNumberResponse();
  }

  public static class ChangeFWSerialNumberRequest implements ICommandRequest {

    private static final int CIPHERED_DATA_LENGTH = 48;

    private final ByteBuffer data = ByteBuffer.allocate(48).order(ByteOrder.LITTLE_ENDIAN);
    private byte[] cipheredData;

    private ChangeFWSerialNumberRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.put(cipheredData);
      data.flip();
      return data;
    }

    public ChangeFWSerialNumberRequest setCipheredData(final byte[] cipheredData) {
      if (cipheredData.length != CIPHERED_DATA_LENGTH) {
        throw new IllegalArgumentException(String.format("Ciphered Data size must be %d, but it's %d", CIPHERED_DATA_LENGTH, cipheredData.length));
      }
      this.cipheredData = cipheredData;
      return this;
    }
  }

  public static class ChangeFWSerialNumberResponse implements ICommandResponse {

    private ChangeFWSerialNumberResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
