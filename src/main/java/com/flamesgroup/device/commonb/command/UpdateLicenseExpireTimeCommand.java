/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class UpdateLicenseExpireTimeCommand extends Command<UpdateLicenseExpireTimeCommand.UpdateLicenseExpireTimeRequest, UpdateLicenseExpireTimeCommand.UpdateLicenseExpireTimeResponse> {

  private static final byte ERRCODE_INCORRECT_DATA = (byte) (0x8 << 4);
  private static final byte ERRCODE_CORRUPTED_DATA = (byte) (0x9 << 4);

  public UpdateLicenseExpireTimeCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    switch (e.getErrCode()) {
      case ERRCODE_INCORRECT_DATA: {
        return new LicenseIncorrectDataErrException(e.getErrCode());
      }
      case ERRCODE_CORRUPTED_DATA: {
        return new LicenseCorruptedDataErrException(e.getErrCode());
      }
    }
    return super.specifyErrCode(e);
  }

  @Override
  public UpdateLicenseExpireTimeRequest createRequest() {
    return new UpdateLicenseExpireTimeRequest();
  }

  @Override
  public UpdateLicenseExpireTimeResponse createResponse() {
    return new UpdateLicenseExpireTimeResponse();
  }

  public static class UpdateLicenseExpireTimeRequest implements ICommandRequest {

    private static final int KEY_LENGTH = 48;

    private final ByteBuffer data = ByteBuffer.allocate(KEY_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    private byte[] key;

    private UpdateLicenseExpireTimeRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.put(key);
      data.flip();
      return data;
    }

    public UpdateLicenseExpireTimeRequest setKey(final byte[] key) {
      if (key.length != KEY_LENGTH) {
        throw new IllegalArgumentException("key length must be " + KEY_LENGTH + ", but it's length " + key.length);
      }
      this.key = key;
      return this;
    }

  }

  public static class UpdateLicenseExpireTimeResponse implements ICommandResponse {

    private UpdateLicenseExpireTimeResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
