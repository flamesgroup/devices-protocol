/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class UpdateLicenseCurrentTimeCommand extends Command<UpdateLicenseCurrentTimeCommand.UpdateLicenseCurrentTimeRequest, UpdateLicenseCurrentTimeCommand.UpdateLicenseCurrentTimeResponse> {

  private static final byte ERRCODE_INCORRECT_TIME = (byte) (0x8 << 4);
  private static final byte ERRCODE_LICENSE_FAILURE = (byte) (0x9 << 4);

  public UpdateLicenseCurrentTimeCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    switch (e.getErrCode()) {
      case ERRCODE_INCORRECT_TIME: {
        return new LicenseIncorrectTimeException(e.getErrCode());
      }
      case ERRCODE_LICENSE_FAILURE: {
        return new LicenseFailureErrException(e.getErrCode());
      }
    }
    return super.specifyErrCode(e);
  }

  @Override
  public UpdateLicenseCurrentTimeRequest createRequest() {
    return new UpdateLicenseCurrentTimeRequest();
  }

  @Override
  public UpdateLicenseCurrentTimeResponse createResponse() {
    return new UpdateLicenseCurrentTimeResponse();
  }

  public static class UpdateLicenseCurrentTimeRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
    private long currentTimeMillis;

    public UpdateLicenseCurrentTimeRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putLong(currentTimeMillis);
      data.flip();
      return data;
    }

    public UpdateLicenseCurrentTimeRequest setCurrentTimeMillis(final long currentTimeMillis) {
      this.currentTimeMillis = currentTimeMillis;
      return this;
    }

  }

  public static class UpdateLicenseCurrentTimeResponse implements ICommandResponse {

    public UpdateLicenseCurrentTimeResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
