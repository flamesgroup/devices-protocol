/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.FirmwareType;
import com.flamesgroup.device.commonb.ServiceInfo;
import com.flamesgroup.device.commonb.command.GetInfoCommand.GetInfoRequest;
import com.flamesgroup.device.commonb.command.GetInfoCommand.GetInfoResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class GetInfoCommand extends Command<GetInfoRequest, GetInfoResponse> {

  public GetInfoCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public GetInfoRequest createRequest() {
    return new GetInfoRequest();
  }

  @Override
  public GetInfoResponse createResponse() {
    return new GetInfoResponse();
  }

  public static class GetInfoRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private GetInfoRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      return data;
    }

  }

  public static class GetInfoResponse implements ICommandResponse {

    private ServiceInfo serviceInfo;

    private GetInfoResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 21) {
        throw new ChannelCommandDecodeException("Length of service info must be 21 byte, but it's " + data.remaining());
      }
      FirmwareType firmwareType = FirmwareType.getFirmwareTypeByN(data.getInt());
      int firmwareVersion = data.getInt();
      int firmwareUpdaterVersion = data.getInt();
      DeviceUID deviceUid = new DeviceUID(data.getInt());
      int osTime = data.getInt();
      byte cpuUsage = data.get();
      serviceInfo = new ServiceInfo(firmwareType, firmwareVersion, firmwareUpdaterVersion, deviceUid, osTime, cpuUsage);
    }

    public ServiceInfo getServiceInfo() {
      return serviceInfo;
    }

  }

}
