/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.command.ApplyFWCommand.ApplyFWRequest;
import com.flamesgroup.device.commonb.command.ApplyFWCommand.ApplyFWResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ApplyFWCommand extends Command<ApplyFWRequest, ApplyFWResponse> {

  private static final byte ERRCODE_APPLY_FW_CRC32_ERR = (byte) 0x80;
  private static final byte ERRCODE_FIRMWARE_TYPE_ERR = (byte) 0xA0;
  private static final byte ERRCODE_DEVICE_NUMBER_CHANGE_ERR = (byte) 0xB0;

  public ApplyFWCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_APPLY_FW_CRC32_ERR ||
        e.getErrCode() == ERRCODE_FIRMWARE_TYPE_ERR ||
        e.getErrCode() == ERRCODE_DEVICE_NUMBER_CHANGE_ERR) {
      return new ApplyFWErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public ApplyFWRequest createRequest() {
    return new ApplyFWRequest();
  }

  @Override
  public ApplyFWResponse createResponse() {
    return new ApplyFWResponse();
  }

  public static class ApplyFWRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
    private int crc;

    private ApplyFWRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putInt(crc);
      data.flip();
      return data;
    }

    public ApplyFWRequest setCRC(final int crc) {
      this.crc = crc;
      return this;
    }

  }

  public static class ApplyFWResponse implements ICommandResponse {

    private ApplyFWResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
