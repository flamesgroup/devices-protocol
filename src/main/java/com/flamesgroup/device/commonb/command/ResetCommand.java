/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.command.ResetCommand.ResetRequest;
import com.flamesgroup.device.commonb.command.ResetCommand.ResetResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ResetCommand extends Command<ResetRequest, ResetResponse> {

  public ResetCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public ResetRequest createRequest() {
    return new ResetRequest();
  }

  @Override
  public ResetResponse createResponse() {
    return new ResetResponse();
  }

  public static class ResetRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
    private int timeout;

    private ResetRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putInt(timeout);
      data.flip();
      return data;
    }

    public ResetRequest setTimeout(final long timeout) {
      if (timeout > 4294967295L) {
        throw new IllegalArgumentException("Reset timeout can't be greater than " + 4294967295L + ", but it's " + timeout);
      }
      this.timeout = (int) timeout;
      return this;
    }
  }

  public static class ResetResponse implements ICommandResponse {

    private ResetResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
