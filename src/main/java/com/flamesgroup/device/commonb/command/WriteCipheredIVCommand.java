/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.device.commonb.command.WriteCipheredIVCommand.WriteCipheredIVRequest;
import com.flamesgroup.device.commonb.command.WriteCipheredIVCommand.WriteCipheredIVResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class WriteCipheredIVCommand extends Command<WriteCipheredIVRequest, WriteCipheredIVResponse> {

  public WriteCipheredIVCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public WriteCipheredIVRequest createRequest() {
    return new WriteCipheredIVRequest();
  }

  @Override
  public WriteCipheredIVResponse createResponse() {
    return new WriteCipheredIVResponse();
  }

  public static class WriteCipheredIVRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(CipheredFWBlock.IV_LENGTH).order(ByteOrder.LITTLE_ENDIAN);

    private final byte[] iv = new byte[CipheredFWBlock.IV_LENGTH];

    private WriteCipheredIVRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.put(iv);
      data.flip();
      return data;
    }

    public WriteCipheredIVRequest setIV(final byte[] iv) {
      if (iv.length != CipheredFWBlock.IV_LENGTH) {
        throw new IllegalArgumentException(String.format("IV length must be %d bytes, but it's %d", CipheredFWBlock.IV_LENGTH, iv.length));
      }
      System.arraycopy(iv, 0, this.iv, 0, this.iv.length);
      return this;
    }

  }

  public static class WriteCipheredIVResponse implements ICommandResponse {

    private WriteCipheredIVResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
