/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ReadLicenseExpireTimeCommand extends Command<ReadLicenseExpireTimeCommand.ReadLicenseExpireTimeRequest, ReadLicenseExpireTimeCommand.ReadLicenseExpireTimeResponse> {

  public ReadLicenseExpireTimeCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public ReadLicenseExpireTimeRequest createRequest() {
    return new ReadLicenseExpireTimeRequest();
  }

  @Override
  public ReadLicenseExpireTimeResponse createResponse() {
    return new ReadLicenseExpireTimeResponse();
  }

  public static class ReadLicenseExpireTimeRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0);

    public ReadLicenseExpireTimeRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      return data;
    }

  }

  public static class ReadLicenseExpireTimeResponse implements ICommandResponse {

    private long expireTimeMillis;

    public ReadLicenseExpireTimeResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 8) {
        throw new ChannelCommandDecodeException("Response data length must be 8, but it's length " + data.remaining());
      }
      expireTimeMillis = data.order(ByteOrder.LITTLE_ENDIAN).getLong();
    }

    public long getExpireTimeMillis() {
      return expireTimeMillis;
    }

  }

}
