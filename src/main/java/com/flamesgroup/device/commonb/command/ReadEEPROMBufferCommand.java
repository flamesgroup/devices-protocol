/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.EEPROMBuffer;
import com.flamesgroup.device.commonb.command.ReadEEPROMBufferCommand.ReadEEPROMBufferRequest;
import com.flamesgroup.device.commonb.command.ReadEEPROMBufferCommand.ReadEEPROMBufferResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ReadEEPROMBufferCommand extends Command<ReadEEPROMBufferRequest, ReadEEPROMBufferResponse> {

  private static final byte ERRCODE_EEPROM_ERR = (byte) 0x90;

  public ReadEEPROMBufferCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_EEPROM_ERR) {
      return new EEPROMErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public ReadEEPROMBufferRequest createRequest() {
    return new ReadEEPROMBufferRequest();
  }

  @Override
  public ReadEEPROMBufferResponse createResponse() {
    return new ReadEEPROMBufferResponse();
  }

  public static class ReadEEPROMBufferRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN);
    private int address;
    private int length;

    private ReadEEPROMBufferRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putInt(address).put((byte) (length & 0xFF));
      data.flip();
      return data;
    }

    public ReadEEPROMBufferRequest setAddress(final int address) {
      this.address = address;
      return this;
    }

    public ReadEEPROMBufferRequest setLength(final int length) {
      this.length = length;
      return this;
    }

    public int getAddress() {
      return address;
    }

  }

  public static class ReadEEPROMBufferResponse implements ICommandResponse {

    private EEPROMBuffer eepromBuffer;

    private ReadEEPROMBufferResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() > EEPROMBuffer.MAX_EEPROM_BUFFER_LENGTH) {
        throw new ChannelCommandDecodeException("EEPROM buffer length can't be greater than " + EEPROMBuffer.MAX_EEPROM_BUFFER_LENGTH + " but it's " + data.remaining());
      }
      eepromBuffer = new EEPROMBuffer(data.remaining());
      data.get(eepromBuffer.getData());
    }

    public EEPROMBuffer getEEPROMBuffer() {
      return eepromBuffer;
    }

  }

}
