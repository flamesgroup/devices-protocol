/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.FWBlock;
import com.flamesgroup.device.commonb.command.WriteFWBlockCommand.WriteFWBlockRequest;
import com.flamesgroup.device.commonb.command.WriteFWBlockCommand.WriteFWBlockResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class WriteFWBlockCommand extends Command<WriteFWBlockRequest, WriteFWBlockResponse> {

  public WriteFWBlockCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public WriteFWBlockRequest createRequest() {
    return new WriteFWBlockRequest();
  }

  @Override
  public WriteFWBlockResponse createResponse() {
    return new WriteFWBlockResponse();
  }

  public static class WriteFWBlockRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(2 + FWBlock.LENGTH).order(ByteOrder.LITTLE_ENDIAN);

    private short blockNumber;
    private FWBlock fwBlock;

    private WriteFWBlockRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putShort(blockNumber);
      data.put(fwBlock.getData());
      data.flip();
      return data;
    }

    public WriteFWBlockRequest setBlockNumber(final short blockNumber) {
      this.blockNumber = blockNumber;
      return this;
    }

    public WriteFWBlockRequest setFWBlock(final FWBlock fwBlock) {
      this.fwBlock = fwBlock;
      return this;
    }

  }

  public static class WriteFWBlockResponse implements ICommandResponse {

    private WriteFWBlockResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
