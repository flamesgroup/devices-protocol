/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.LicenseState;

import java.nio.ByteBuffer;

public final class GetLicenseStateCommand extends Command<GetLicenseStateCommand.GetLicenseStateRequest, GetLicenseStateCommand.GetLicenseStateResponse> {

  public GetLicenseStateCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public GetLicenseStateRequest createRequest() {
    return new GetLicenseStateRequest();
  }

  @Override
  public GetLicenseStateResponse createResponse() {
    return new GetLicenseStateResponse();
  }

  public static class GetLicenseStateRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0);

    public GetLicenseStateRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      return data;
    }

  }

  public static class GetLicenseStateResponse implements ICommandResponse {

    private LicenseState licenseState;

    public GetLicenseStateResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 1) {
        throw new ChannelCommandDecodeException("Response data length must be 1, but it's length " + data.remaining());
      }
      byte n = data.get();
      licenseState = LicenseState.getLicenseStateByN(n);
      if (licenseState == null) {
        throw new ChannelCommandDecodeException("Response data " + n + " can't be decode as correct LicenseState");
      }
    }

    public LicenseState getLicenseState() {
      return licenseState;
    }

  }

}
