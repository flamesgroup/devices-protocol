/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.JournalRecord;
import com.flamesgroup.device.commonb.command.ReadJournalRecordCommand.ReadJournalRecordRequest;
import com.flamesgroup.device.commonb.command.ReadJournalRecordCommand.ReadJournalRecordResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public final class ReadJournalRecordCommand extends Command<ReadJournalRecordRequest, ReadJournalRecordResponse> {

  public ReadJournalRecordCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public ReadJournalRecordRequest createRequest() {
    return new ReadJournalRecordRequest();
  }

  @Override
  public ReadJournalRecordResponse createResponse() {
    return new ReadJournalRecordResponse();
  }

  public static class ReadJournalRecordRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
    private short recordNumber;

    private ReadJournalRecordRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putShort(recordNumber);
      data.flip();
      return data;
    }

    public ReadJournalRecordRequest setRecordNumber(final short recordNumber) {
      this.recordNumber = recordNumber;
      return this;
    }

  }

  public static class ReadJournalRecordResponse implements ICommandResponse {

    private JournalRecord journalRecord;

    private ReadJournalRecordResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 64) {
        throw new ChannelCommandDecodeException("Length of journal record must be 64 byte, but it's " + data.remaining());
      }
      int index = data.getInt();
      int timestamp = data.getInt();
      String comment;
      byte marker = data.get(data.position());
      if (marker == (byte) 255) {
        int markerAndReserved = data.getInt();
        int CFSR = data.getInt();
        int HFSR = data.getInt();
        int MMFAR = data.getInt();
        int BFAR = data.getInt();
        int PC = data.getInt();
        int LR = data.getInt();
        int PSR = data.getInt();
        String currentThreadName = getNullCharEndedString(data);

        comment = String.format("CFSR:0x%08X HFSR:0x%08X MMFAR:0x%08X BFAR:0x%08X PC:0x%08X LR:0x%08X PSR:0x%08X ctn:%s",
            CFSR, HFSR, MMFAR, BFAR, PC, LR, PSR, currentThreadName);
      } else {
        comment = getNullCharEndedString(data);
      }
      journalRecord = new JournalRecord(index, timestamp, comment);
    }

    public JournalRecord getJournalRecord() {
      return journalRecord;
    }

    private String getNullCharEndedString(final ByteBuffer data) {
      for (int i = data.position(); i < data.limit(); i++) {
        if (data.get(i) == (byte) 0) {
          data.limit(i);
        }
      }
      return Charset.forName("ISO-8859-1").decode(data).toString();
    }

  }

}
