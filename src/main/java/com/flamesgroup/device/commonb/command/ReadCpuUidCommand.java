/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ReadCpuUidCommand extends Command<ReadCpuUidCommand.ReadCpuUidRequest, ReadCpuUidCommand.ReadCpuUidResponse> {

  public ReadCpuUidCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public ReadCpuUidRequest createRequest() {
    return new ReadCpuUidRequest();
  }

  @Override
  public ReadCpuUidResponse createResponse() {
    return new ReadCpuUidResponse();
  }

  public static class ReadCpuUidRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private ReadCpuUidRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      return data;
    }

  }

  public static class ReadCpuUidResponse implements ICommandResponse {

    private static final int CPU_UID_LENGTH = 12;
    private byte[] cpuUid;

    private ReadCpuUidResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != CPU_UID_LENGTH) {
        throw new ChannelCommandDecodeException(String.format("Length of CPU UID must be %s byte, but it's %s", CPU_UID_LENGTH, data.remaining()));
      }
      cpuUid = new byte[CPU_UID_LENGTH];
      data.get(cpuUid);
    }

    public byte[] getCpuUid() {
      return cpuUid;
    }

  }

}
