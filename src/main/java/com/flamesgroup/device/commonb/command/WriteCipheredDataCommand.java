/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.device.commonb.command.WriteCipheredDataCommand.WriteCipheredDataRequest;
import com.flamesgroup.device.commonb.command.WriteCipheredDataCommand.WriteCipheredDataResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class WriteCipheredDataCommand extends Command<WriteCipheredDataRequest, WriteCipheredDataResponse> {

  public WriteCipheredDataCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  public WriteCipheredDataRequest createRequest() {
    return new WriteCipheredDataRequest();
  }

  @Override
  public WriteCipheredDataResponse createResponse() {
    return new WriteCipheredDataResponse();
  }

  public static class WriteCipheredDataRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(4 + CipheredFWBlock.DATA_LENGTH).order(ByteOrder.LITTLE_ENDIAN);

    private short blockNumber;
    private final byte[] cipheredData = new byte[CipheredFWBlock.DATA_LENGTH];

    private WriteCipheredDataRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putShort(blockNumber);
      // reserved alignment field
      data.putShort((short) 0xFFFF);
      data.put(cipheredData);
      data.flip();
      return data;
    }

    public WriteCipheredDataRequest setBlockNumber(final short blockNumber) {
      this.blockNumber = blockNumber;
      return this;
    }

    public WriteCipheredDataRequest setBlockCipheredData(final byte[] cipheredData) {
      if (cipheredData.length != CipheredFWBlock.DATA_LENGTH) {
        throw new IllegalArgumentException(String.format("Ciphered data length must be %d bytes, but it's %d", CipheredFWBlock.DATA_LENGTH, cipheredData.length));
      }
      System.arraycopy(cipheredData, 0, this.cipheredData, 0, this.cipheredData.length);
      return this;
    }

  }

  public static class WriteCipheredDataResponse implements ICommandResponse {

    private WriteCipheredDataResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
