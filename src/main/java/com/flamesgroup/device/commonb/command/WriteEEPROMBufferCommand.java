/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.commonb.EEPROMBuffer;
import com.flamesgroup.device.commonb.command.WriteEEPROMBufferCommand.WriteEEPROMBufferRequest;
import com.flamesgroup.device.commonb.command.WriteEEPROMBufferCommand.WriteEEPROMBufferResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class WriteEEPROMBufferCommand extends Command<WriteEEPROMBufferRequest, WriteEEPROMBufferResponse> {

  private static final byte ERRCODE_EEPROM_ERR = (byte) 0x90;

  public WriteEEPROMBufferCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_EEPROM_ERR) {
      return new EEPROMErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public WriteEEPROMBufferRequest createRequest() {
    return new WriteEEPROMBufferRequest();
  }

  @Override
  public WriteEEPROMBufferResponse createResponse() {
    return new WriteEEPROMBufferResponse();
  }

  public static class WriteEEPROMBufferRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(5 + EEPROMBuffer.MAX_EEPROM_BUFFER_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    private int address;
    private EEPROMBuffer eepromBuffer;

    private WriteEEPROMBufferRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putInt(address);
      data.put((byte) eepromBuffer.getData().length);
      data.put(eepromBuffer.getData());
      data.flip();
      return data;
    }

    public WriteEEPROMBufferRequest setAddress(final int address) {
      this.address = address;
      return this;
    }

    public WriteEEPROMBufferRequest setEEPROMBuffer(final EEPROMBuffer eepromBuffer) {
      this.eepromBuffer = eepromBuffer;
      return this;
    }

  }

  public static class WriteEEPROMBufferResponse implements ICommandResponse {

    private WriteEEPROMBufferResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
