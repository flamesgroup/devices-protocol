/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import java.io.Serializable;

public final class FWBlock implements Serializable {

  private static final long serialVersionUID = -6615110521669159410L;

  public final static int LENGTH = 256;

  private final byte[] data = new byte[LENGTH];

  private final String toS;

  public FWBlock() {
    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());
  }

  public byte[] getData() {
    return data;
  }

  @Override
  public String toString() {
    return toS;
  }

}
