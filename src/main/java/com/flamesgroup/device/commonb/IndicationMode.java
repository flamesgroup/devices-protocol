/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

public enum IndicationMode {

  NONE((byte) 0),
  INIT((byte) 1),
  READY((byte) 2),
  ACTIVE((byte) 3),
  DEINIT((byte) 4),
  BUSINESS_PROBLEM((byte) 5),
  SOFTWARE_PROBLEM((byte) 6),
  HARDWARE_PROBLEM((byte) 7),
  _STATIC_G((byte) 8),
  _STATIC_R((byte) 9),
  _SLOW_BLINK_G((byte) 10),
  _SLOW_BLINK_R((byte) 11),
  _SLOW_BLINK_GR((byte) 12),
  _FAST_BLINK_G((byte) 13),
  _FAST_BLINK_R((byte) 14),
  _FAST_BLINK_GR((byte) 15);

  private final byte n;

  IndicationMode(final byte n) {
    this.n = n;
  }

  public byte getN() {
    return n;
  }

}
