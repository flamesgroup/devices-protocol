/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.helper.ServiceInfoEncodingHelper;

import java.io.Serializable;

public class ServiceInfo implements Serializable {

  private static final long serialVersionUID = 4025549567741530843L;

  private final FirmwareType firmwareType;
  private final int firmwareVersion;
  private final int firmwareUpdaterVersion;
  private final DeviceUID deviceUid;
  private final int osTime;
  private final byte cpuUsage;

  private final String toS;

  public ServiceInfo(final FirmwareType firmwareType, final int firmwareVersion, final int firmwareUpdaterVersion, final DeviceUID deviceUid, final int osTime, final byte cpuUsage) {
    this.firmwareType = firmwareType;
    this.firmwareVersion = firmwareVersion;
    this.firmwareUpdaterVersion = firmwareUpdaterVersion;
    this.deviceUid = deviceUid;
    this.osTime = osTime;
    this.cpuUsage = cpuUsage;

    toS = String.format("%s@%x:[%s - %s/%s; Device UID - %s; OS time - %d; CPU usage - %d]", getClass().getSimpleName(), hashCode(),
        firmwareType,
        ServiceInfoEncodingHelper.firmwareVersionToString(firmwareVersion), ServiceInfoEncodingHelper.firmwareVersionToString(firmwareUpdaterVersion),
        deviceUid, osTime, cpuUsage);
  }

  public FirmwareType getFirmwareType() {
    return firmwareType;
  }

  public int getFirmwareVersion() {
    return firmwareVersion;
  }

  public int getFirmwareUpdaterVersion() {
    return firmwareUpdaterVersion;
  }

  public DeviceUID getDeviceUID() {
    return deviceUid;
  }

  public int getOsTime() {
    return osTime;
  }

  public byte getCpuUsage() {
    return cpuUsage;
  }

  @Override
  public String toString() {
    return toS;
  }

}
