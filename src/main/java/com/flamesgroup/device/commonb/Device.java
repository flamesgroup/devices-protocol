/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.mudp.MudpStreamSPIDev;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Device implements IDevice {

  private static final Logger logger = LoggerFactory.getLogger(Device.class);

  protected final IMudpConnection mudpConnections[];
  protected final DeviceUID deviceUid;

  private List<ICPUEntry> cpuEntries = Collections.emptyList();

  private final Lock deviceLock = new ReentrantLock();

  private boolean deviceAttached;

  public Device(final IMudpConnection[] mudpConnections, final DeviceUID deviceUid) {
    this.mudpConnections = mudpConnections;
    this.deviceUid = deviceUid;
    this.deviceAttached = false;
  }

  protected abstract ICPUEntry createCPUEntry(int cpuEntryNumber, DeviceUID deviceUid);

  protected void checkCPUEntryNumber(final int cpuEntryNumber) {
    if (cpuEntryNumber < 0 || cpuEntryNumber >= getCPUEntryCount()) {
      throw new IllegalArgumentException("cpuEntryNumber must be >= 0 and < " + getCPUEntryCount());
    }
  }

  protected MudpChannelAddress createMudpChannelAddressTypeAFor(final int cpuN, final int channelN) {
    if (cpuN < 0 || cpuN > 0x0F) {
      throw new IllegalArgumentException("cpuN must be >= 0 and <= 16");
    }
    if (channelN < 0 || channelN > 0x0F) {
      throw new IllegalArgumentException("channelN must be >= 0 and <= 16");
    }
    return new MudpChannelAddress((byte) (((cpuN << 4) & 0xF0) | (channelN & 0x0F)));
  }

  protected MudpChannelAddress createMudpChannelAddressTypeBFor(final int cpuN, final int channelN) {
    if (!(cpuN >= 0 && cpuN < MudpStreamSPIDev.CPU_N_MAX)) {
      throw new IllegalArgumentException("cpuN must be >= 0 and < " + MudpStreamSPIDev.CPU_N_MAX);
    }
    if (!(channelN >= 0 && channelN < MudpStreamSPIDev.CHANNEL_N_MAX)) {
      throw new IllegalArgumentException("channelN must be >= 0 and < " + MudpStreamSPIDev.CHANNEL_N_MAX);
    }
    return new MudpChannelAddress((byte) (cpuN * MudpStreamSPIDev.CHANNEL_N_MAX + channelN));
  }

  protected IMudpChannel createMudpChannelFor(final int connectionN) {
    return mudpConnections[connectionN].createChannel();
  }

  protected int getConnectionCount() {
    return mudpConnections.length;
  }

  @Override
  public boolean isAttach() {
    deviceLock.lock();
    try {
      return deviceAttached;
    } finally {
      deviceLock.unlock();
    }
  }

  @Override
  public void attach() throws MudpException, InterruptedException {
    deviceLock.lock();
    try {
      if (deviceAttached) {
        throw new MudpException("[" + this + "] - already attached");
      }
      int i = 0;
      try {
        while (i < mudpConnections.length) {
          mudpConnections[i].connect();
          i++;
        }
      } catch (MudpException e) {
        i--; // step to previous success connected MUDP connection
        while (i >= 0) {
          try {
            mudpConnections[i].disconnect();
          } catch (MudpException es) {
            e.addSuppressed(es);
          }
          i--;
        }
        throw e;
      }
      deviceAttached = true;
    } finally {
      deviceLock.unlock();
    }

    List<ICPUEntry> cpuEntries = new ArrayList<>(getCPUEntryCount());
    for (int i = 0; i < getCPUEntryCount(); i++) {
      cpuEntries.add(createCPUEntry(i, deviceUid));
    }
    this.cpuEntries = Collections.unmodifiableList(cpuEntries);
  }

  @Override
  public void detach() throws MudpException, InterruptedException {
    for (ICPUEntry cpuEntry : cpuEntries) {
      for (IChannel channel : cpuEntry.getSupportedChannels()) {
        if (channel.isEnslaved()) {
          logger.warn("[{}] - is enslaved, will be nulled", channel);
        }
      }
    }
    cpuEntries = Collections.emptyList();

    deviceLock.lock();
    try {
      if (!deviceAttached) {
        throw new MudpException("[" + this + "] - already detached");
      }
      MudpException mudpException = null;
      int i = 0;
      while (i < mudpConnections.length) {
        try {
          mudpConnections[i].disconnect();
        } catch (MudpException e) {
          if (mudpException == null) {
            mudpException = e;
          } else {
            mudpException.addSuppressed(e);
          }
        }
        i++;
      }
      deviceAttached = false;
      if (mudpException != null) {
        throw mudpException;
      }
    } finally {
      deviceLock.unlock();
    }
  }

  @Override
  public List<ICPUEntry> getCPUEntries() {
    return cpuEntries;
  }

  protected static final class CPUEntry extends Entry implements ICPUEntry {

    private final IServiceChannel serviceChannel;
    private final ILoggerChannel loggerChannel;

    public CPUEntry(final IServiceChannel serviceChannel, final ILoggerChannel loggerChannel, final int cpuEntryNumber, final DeviceUID deviceUid) {
      super(cpuEntryNumber, deviceUid);
      this.serviceChannel = serviceChannel;
      this.loggerChannel = loggerChannel;

      if (logger.isTraceEnabled()) {
        logger.trace("[{}] - created with [{}/{}]", toString(), serviceChannel, loggerChannel);
      }
    }

    @Override
    public List<IChannel> getSupportedChannels() {
      List<IChannel> supportedChannels = new ArrayList<>(3);
      if (serviceChannel != null) {
        supportedChannels.add(serviceChannel);
      }
      if (loggerChannel != null) {
        supportedChannels.add(loggerChannel);
      }
      return supportedChannels;
    }

    @Override
    public IServiceChannel getServiceChannel() {
      if (serviceChannel == null) {
        throw new UnsupportedOperationException();
      }
      return serviceChannel;
    }

    @Override
    public ILoggerChannel getLoggerChannel() {
      if (loggerChannel == null) {
        throw new UnsupportedOperationException();
      }
      return loggerChannel;
    }

  }

}
