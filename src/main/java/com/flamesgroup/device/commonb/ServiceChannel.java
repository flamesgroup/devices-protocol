/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.command.ApplyFWCommand;
import com.flamesgroup.device.commonb.command.ChangeFWSerialNumberCommand;
import com.flamesgroup.device.commonb.command.GetInfoCommand;
import com.flamesgroup.device.commonb.command.GetLicenseStateCommand;
import com.flamesgroup.device.commonb.command.ReadCpuUidCommand;
import com.flamesgroup.device.commonb.command.ReadEEPROMBufferCommand;
import com.flamesgroup.device.commonb.command.ReadFWBlockCommand;
import com.flamesgroup.device.commonb.command.ReadLicenseExpireTimeCommand;
import com.flamesgroup.device.commonb.command.ResetCommand;
import com.flamesgroup.device.commonb.command.UpdateLicenseCurrentTimeCommand;
import com.flamesgroup.device.commonb.command.UpdateLicenseExpireTimeCommand;
import com.flamesgroup.device.commonb.command.WriteCipheredDataCommand;
import com.flamesgroup.device.commonb.command.WriteCipheredIVCommand;
import com.flamesgroup.device.commonb.command.WriteEEPROMBufferCommand;
import com.flamesgroup.device.commonb.command.WriteFWBlockCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ServiceChannel extends Channel implements IServiceChannel {

  public static final byte COMMAND_GET_INFO = (byte) 0x01;
  public static final byte COMMAND_RESET = (byte) 0x02;
  public static final byte COMMAND_READ_FW_BLOCK = (byte) 0x03;
  public static final byte COMMAND_WRITE_FW_BLOCK = (byte) 0x04;
  public static final byte COMMAND_APPLY_FW = (byte) 0x05;
  public static final byte COMMAND_READ_EEPROM_BUFFER = (byte) 0x06;
  public static final byte COMMAND_WRITE_EEPROM_BUFFER = (byte) 0x07;
  public static final byte COMMAND_WRITE_CIPHERED_IV = (byte) 0x08;
  public static final byte COMMAND_WRITE_CIPHERED_DATA = (byte) 0x09;
  public static final byte COMMAND_CHANGE_FW_SERIAL_NUMBER = (byte) 0xA;
  public static final byte COMMAND_READ_CPU_UID = (byte) 0x0B;
  public static final byte COMMAND_UPDATE_LICENSE_CURRENT_TIME = (byte) 0x0C;
  public static final byte COMMAND_UPDATE_LICENSE_EXPIRE_TIME = (byte) 0xD;
  public static final byte COMMAND_READ_LICENSE_EXPIRE_TIME = (byte) 0x0E;
  public static final byte COMMAND_GET_LICENSE_STATE = (byte) 0x0F;

  private final Lock getInfoLock = new ReentrantLock();
  private final GetInfoCommand getInfoCommand;
  private final Lock resetLock = new ReentrantLock();
  private final ResetCommand resetCommand;
  private final Lock readFWBlockLock = new ReentrantLock();
  private final ReadFWBlockCommand readFWBlockCommand;
  private final Lock writeFWBlockLock = new ReentrantLock();
  private final WriteFWBlockCommand writeFWBlockCommand;
  private final Lock writeCipheredFWBlock = new ReentrantLock();
  private final WriteCipheredIVCommand writeCipheredIVCommand;
  private final WriteCipheredDataCommand writeCipheredDataCommand;
  private final Lock applyFWLock = new ReentrantLock();
  private final ApplyFWCommand applyFWCommand;
  private final Lock changeFWSerialNumberLock = new ReentrantLock();
  private final ChangeFWSerialNumberCommand changeFWSerialNumberCommand;
  private final Lock readEEPROMBufferLock = new ReentrantLock();
  private final ReadEEPROMBufferCommand readEEPROMBufferCommand;
  private final Lock writeEEPROMBufferLock = new ReentrantLock();
  private final WriteEEPROMBufferCommand writeEEPROMBufferCommand;
  private final Lock readCpuUidLock = new ReentrantLock();
  private final ReadCpuUidCommand readCpuUidCommand;
  private final Lock updateLicenseCurrentTimeLock = new ReentrantLock();
  private final UpdateLicenseCurrentTimeCommand updateLicenseCurrentTimeCommand;
  private final Lock updateLicenseExpireTimeLock = new ReentrantLock();
  private final UpdateLicenseExpireTimeCommand updateLicenseExpireTimeCommand;
  private final Lock readLicenseExpireTimeLock = new ReentrantLock();
  private final ReadLicenseExpireTimeCommand readLicenseExpireTimeCommand;
  private final Lock getLicenseStateLock = new ReentrantLock();
  private final GetLicenseStateCommand getLicenseStateCommand;

  public ServiceChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int cpuEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, cpuEntryNumber, deviceUid);

    getInfoCommand = new GetInfoCommand(COMMAND_GET_INFO, createChannelData(), 10 + getMaxAttemptTimeout());
    resetCommand = new ResetCommand(COMMAND_RESET, createChannelData(), 10 + getMaxAttemptTimeout());
    readFWBlockCommand = new ReadFWBlockCommand(COMMAND_READ_FW_BLOCK, createChannelData(), 10 + getMaxAttemptTimeout());
    writeFWBlockCommand = new WriteFWBlockCommand(COMMAND_WRITE_FW_BLOCK, createChannelData(), 50 + getMaxAttemptTimeout());
    writeCipheredIVCommand = new WriteCipheredIVCommand(COMMAND_WRITE_CIPHERED_IV, createChannelData(), 10 + getMaxAttemptTimeout()); // real process time 2 ms
    writeCipheredDataCommand = new WriteCipheredDataCommand(COMMAND_WRITE_CIPHERED_DATA, createChannelData(), 50 + getMaxAttemptTimeout()); // real process time 17 ms
    applyFWCommand = new ApplyFWCommand(COMMAND_APPLY_FW, createChannelData(), 200 + getMaxAttemptTimeout());
    changeFWSerialNumberCommand = new ChangeFWSerialNumberCommand(COMMAND_CHANGE_FW_SERIAL_NUMBER, createChannelData(), 20 + getMaxAttemptTimeout()); // real process time 9 ms
    readEEPROMBufferCommand = new ReadEEPROMBufferCommand(COMMAND_READ_EEPROM_BUFFER, createChannelData(), 100 + getMaxAttemptTimeout());
    writeEEPROMBufferCommand = new WriteEEPROMBufferCommand(COMMAND_WRITE_EEPROM_BUFFER, createChannelData(), 100 + getMaxAttemptTimeout());
    readCpuUidCommand = new ReadCpuUidCommand(COMMAND_READ_CPU_UID, createChannelData(), 50 + getMaxAttemptTimeout());
    updateLicenseCurrentTimeCommand = new UpdateLicenseCurrentTimeCommand(COMMAND_UPDATE_LICENSE_CURRENT_TIME, createChannelData(), 100 + getMaxAttemptTimeout());
    updateLicenseExpireTimeCommand = new UpdateLicenseExpireTimeCommand(COMMAND_UPDATE_LICENSE_EXPIRE_TIME, createChannelData(), 100 + getMaxAttemptTimeout());
    readLicenseExpireTimeCommand = new ReadLicenseExpireTimeCommand(COMMAND_READ_LICENSE_EXPIRE_TIME, createChannelData(), 50 + getMaxAttemptTimeout());
    getLicenseStateCommand = new GetLicenseStateCommand(COMMAND_GET_LICENSE_STATE, createChannelData(), 50 + getMaxAttemptTimeout());
  }

  public ServiceChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int cpuEntryNumber) {
    this(mudpChannel, mudpChannelAddress, cpuEntryNumber, null);
  }

  @Override
  public ServiceInfo getInfo() throws ChannelException, InterruptedException {
    getInfoLock.lock();
    try {
      return getInfoCommand.execute(getInfoCommand.createRequest()).getServiceInfo();
    } finally {
      getInfoLock.unlock();
    }
  }

  @Override
  public void reset(final long timeout) throws ChannelException, InterruptedException {
    resetLock.lock();
    try {
      resetCommand.execute(resetCommand.createRequest().setTimeout(timeout));
    } finally {
      resetLock.unlock();
    }
  }

  @Override
  public FWBlock readFWBlock(final short blockNumber) throws ChannelException, InterruptedException {
    readFWBlockLock.lock();
    try {
      return readFWBlockCommand.execute(readFWBlockCommand.createRequest().setBlockNumber(blockNumber)).getFWBlock();
    } finally {
      readFWBlockLock.unlock();
    }
  }

  @Override
  public void writeFWBlock(final short blockNumber, final FWBlock fwBlock) throws ChannelException, InterruptedException {
    writeFWBlockLock.lock();
    try {
      writeFWBlockCommand.execute(writeFWBlockCommand.createRequest().setBlockNumber(blockNumber).setFWBlock(fwBlock));
    } finally {
      writeFWBlockLock.unlock();
    }
  }

  @Override
  public void writeCipheredFWBlock(final short blockNumber, final CipheredFWBlock cipheredFWBlock) throws ChannelException, InterruptedException {
    writeCipheredFWBlock.lock();
    try {
      writeCipheredIVCommand.execute(writeCipheredIVCommand.createRequest().setIV(cipheredFWBlock.getIV()));
      writeCipheredDataCommand.execute(writeCipheredDataCommand.createRequest().setBlockNumber(blockNumber).setBlockCipheredData(cipheredFWBlock.getData()));
    } finally {
      writeCipheredFWBlock.unlock();
    }
  }

  @Override
  public void applyFW(final int crc) throws ChannelException, InterruptedException {
    applyFWLock.lock();
    try {
      applyFWCommand.execute(applyFWCommand.createRequest().setCRC(crc));
    } finally {
      applyFWLock.unlock();
    }
  }

  @Override
  public void changeFWSerialNumber(final byte[] cipheredData) throws ChannelException, InterruptedException {
    changeFWSerialNumberLock.lock();
    try {
      changeFWSerialNumberCommand.execute(changeFWSerialNumberCommand.createRequest().setCipheredData(cipheredData));
    } finally {
      changeFWSerialNumberLock.unlock();
    }
  }

  @Override
  public EEPROMBuffer readEEPROMBuffer(final int address, final int length) throws ChannelException, InterruptedException {
    readEEPROMBufferLock.lock();
    try {
      return readEEPROMBufferCommand.execute(readEEPROMBufferCommand.createRequest().setAddress(address).setLength(length)).getEEPROMBuffer();
    } finally {
      readEEPROMBufferLock.unlock();
    }
  }

  @Override
  public void writeEEPROMBuffer(final int address, final EEPROMBuffer eepromBuffer) throws ChannelException, InterruptedException {
    writeEEPROMBufferLock.lock();
    try {
      writeEEPROMBufferCommand.execute(writeEEPROMBufferCommand.createRequest().setAddress(address).setEEPROMBuffer(eepromBuffer));
    } finally {
      writeEEPROMBufferLock.unlock();
    }
  }

  @Override
  public byte[] readCpuUid() throws ChannelException, InterruptedException {
    readCpuUidLock.lock();
    try {
      return readCpuUidCommand.execute(readCpuUidCommand.createRequest()).getCpuUid();
    } finally {
      readCpuUidLock.unlock();
    }
  }

  @Override
  public void updateLicenseCurrentTime(final long currentTimeMillis) throws ChannelException, InterruptedException {
    updateLicenseCurrentTimeLock.lock();
    try {
      updateLicenseCurrentTimeCommand.execute(updateLicenseCurrentTimeCommand.createRequest().setCurrentTimeMillis(currentTimeMillis));
    } finally {
      updateLicenseCurrentTimeLock.unlock();
    }
  }

  @Override
  public void updateLicenseExpireTime(final byte[] key) throws ChannelException, InterruptedException {
    updateLicenseExpireTimeLock.lock();
    try {
      updateLicenseExpireTimeCommand.execute(updateLicenseExpireTimeCommand.createRequest().setKey(key));
    } finally {
      updateLicenseExpireTimeLock.unlock();
    }
  }

  @Override
  public long readLicenseExpireTime() throws ChannelException, InterruptedException {
    readLicenseExpireTimeLock.lock();
    try {
      return readLicenseExpireTimeCommand.execute(readLicenseExpireTimeCommand.createRequest()).getExpireTimeMillis();
    } finally {
      readLicenseExpireTimeLock.unlock();
    }
  }

  @Override
  public LicenseState getLicenseState() throws ChannelException, InterruptedException {
    getLicenseStateLock.lock();
    try {
      return getLicenseStateCommand.execute(getLicenseStateCommand.createRequest()).getLicenseState();
    } finally {
      getLicenseStateLock.unlock();
    }
  }

}
