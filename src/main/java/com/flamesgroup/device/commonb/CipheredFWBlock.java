/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import java.io.Serializable;

public final class CipheredFWBlock implements Serializable {

  private static final long serialVersionUID = -3709011468965994731L;

  public static final int IV_LENGTH = 16;
  public static final int DATA_LENGTH = 256;
  public static final int LENGTH = CipheredFWBlock.IV_LENGTH + CipheredFWBlock.DATA_LENGTH;

  private final byte[] iv = new byte[IV_LENGTH];
  private final byte[] data = new byte[DATA_LENGTH];

  public byte[] getIV() {
    return iv;
  }

  public byte[] getData() {
    return data;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

}
