/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.commonb.command.EraseJournalCommand;
import com.flamesgroup.device.commonb.command.LoggerAlreadyStartedErrException;
import com.flamesgroup.device.commonb.command.ReadJournalRecordCommand;
import com.flamesgroup.device.commonb.command.StartLoggerCommand;
import com.flamesgroup.device.commonb.command.StopLoggerCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LoggerChannel extends Channel implements ILoggerChannel, ILoggerSubChannel {

  private static final Logger logger = LoggerFactory.getLogger(LoggerChannel.class);

  public static final byte COMMAND_LOGGER_DATA = (byte) 0x00;
  public static final byte COMMAND_START_LOGGER = (byte) 0x01;
  public static final byte COMMAND_STOP_LOGGER = (byte) 0x02;
  public static final byte COMMAND_READ_JOURNAL_RECORD = (byte) 0x03;
  public static final byte COMMAND_ERASE_JOURNAL = (byte) 0x04;

  private final Lock readJournalRecordLock = new ReentrantLock();
  private final ReadJournalRecordCommand readJournalRecordCommand;
  private final Lock eraseJournalLock = new ReentrantLock();
  private final EraseJournalCommand eraseJournalCommand;

  private final Lock loggerSubChannelLock = new ReentrantLock();
  private final StartLoggerCommand startLoggerCommand;
  private final StopLoggerCommand stopLoggerCommand;

  private final IChannelData loggerSubChannelData;

  public LoggerChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int cpuEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, cpuEntryNumber, deviceUid);

    readJournalRecordCommand = new ReadJournalRecordCommand(COMMAND_READ_JOURNAL_RECORD, createChannelData(), 10 + getMaxAttemptTimeout());
    eraseJournalCommand = new EraseJournalCommand(COMMAND_ERASE_JOURNAL, createChannelData(), 100 + getMaxAttemptTimeout());

    startLoggerCommand = new StartLoggerCommand(COMMAND_START_LOGGER, createChannelData(), 10 + getMaxAttemptTimeout());
    stopLoggerCommand = new StopLoggerCommand(COMMAND_STOP_LOGGER, createChannelData(), 10 + getMaxAttemptTimeout());

    loggerSubChannelData = createChannelData();
  }

  public LoggerChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int cpuEntryNumber) {
    this(mudpChannel, mudpChannelAddress, cpuEntryNumber, null);
  }

  // ILoggerChannel
  @Override
  public ILoggerSubChannel getLoggerSubChannel() {
    return this;
  }

  @Override
  public JournalRecord readJournalRecord(final short recordNumber) throws ChannelException, InterruptedException {
    readJournalRecordLock.lock();
    try {
      return readJournalRecordCommand.execute(readJournalRecordCommand.createRequest().setRecordNumber(recordNumber)).getJournalRecord();
    } finally {
      readJournalRecordLock.unlock();
    }
  }

  @Override
  public void eraseJournal() throws ChannelException, InterruptedException {
    eraseJournalLock.lock();
    try {
      eraseJournalCommand.execute(eraseJournalCommand.createRequest());
    } finally {
      eraseJournalLock.unlock();
    }
  }

  // ILoggerSubChannel
  @Override
  public void start(final LoggerLevel loggerLevel, final ILoggerSubChannelHandler loggerSubChannelHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(loggerLevel, "loggerLevel mustn't be null");
    Objects.requireNonNull(loggerSubChannelHandler, "loggerSubChannelHandler mustn't be null");

    StartLoggerCommand.StartLoggerRequest startLoggerRequest = startLoggerCommand.createRequest().setLevel(loggerLevel);

    loggerSubChannelLock.lock();
    try {
      try {
        logger.debug("[{}] - starting Logger", this);
        startLoggerCommand.execute(startLoggerRequest);
      } catch (LoggerAlreadyStartedErrException e) {
        logger.info("[{}] - attempt to start already started Logger", this);
        stopLoggerCommand.execute(stopLoggerCommand.createRequest());
        startLoggerCommand.execute(startLoggerRequest);
      }
      loggerSubChannelData.attach(COMMAND_LOGGER_DATA, new LoggerSubChannelDataHandler(loggerSubChannelHandler));
      logger.debug("[{}] - started Logger", this);
    } finally {
      loggerSubChannelLock.unlock();
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    loggerSubChannelLock.lock();
    try {
      logger.debug("[{}] - stoping Logger", this);
      try {
        stopLoggerCommand.execute(stopLoggerCommand.createRequest());
      } finally {
        loggerSubChannelData.detach();
      }
      logger.debug("[{}] - stoped Logger", this);
    } finally {
      loggerSubChannelLock.unlock();
    }
  }

  // internal classes
  private final class LoggerSubChannelDataHandler implements IChannelDataHandler {

    private final ILoggerSubChannelHandler loggerSubChannelHandler;

    public LoggerSubChannelDataHandler(final ILoggerSubChannelHandler loggerSubChannelHandler) {
      this.loggerSubChannelHandler = loggerSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + LoggerChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      loggerSubChannelHandler.handleLoggerData(Charset.forName("ISO-8859-1").decode(data).toString());
    }

  }

}
