/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import java.io.Serializable;

public class JournalRecord implements Serializable {

  private static final long serialVersionUID = -3727317981070359745L;

  private final int index;
  private final int timestamp;
  private final String comment;

  public JournalRecord(final int index, final int timestamp, final String comment) {
    this.index = index;
    this.timestamp = timestamp;
    this.comment = comment;
  }

  public int getIndex() {
    return index;
  }

  public int getTimestamp() {
    return timestamp;
  }

  public String getComment() {
    return comment;
  }

  public boolean isEmpty() {
    return index == -1 && timestamp == -1; // also comment must be check for only 0xFF chars, but index & timestamp should be enough
  }

  @Override
  public String toString() {
    if (isEmpty()) {
      return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ":[empty]";
    } else {
      return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ":[" + index + ":" + timestamp + " - " + comment + "]";
    }
  }

}
