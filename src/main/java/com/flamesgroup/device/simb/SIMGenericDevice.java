/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.commonb.Device;
import com.flamesgroup.device.commonb.Entry;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class SIMGenericDevice extends Device implements ISIMDevice {

  private static final Logger logger = LoggerFactory.getLogger(SIMGenericDevice.class);

  private List<ISIMEntry> simEntries = Collections.emptyList();

  public SIMGenericDevice(final IMudpConnection[] mudpConnections, final DeviceUID deviceUid) {
    super(mudpConnections, deviceUid);
  }

  protected abstract ISIMEntry createSIMEntry(int simEntryNumber, DeviceUID deviceUid);

  protected void checkSIMEntryNumber(final int simEntryNumber) {
    if (simEntryNumber < 0 || simEntryNumber >= getSIMEntryCount()) {
      throw new IllegalArgumentException("simEntryNumber must be >= 0 and < " + getSIMEntryCount());
    }
  }

  @Override
  public void attach() throws MudpException, InterruptedException {
    super.attach();

    List<ISIMEntry> simEntries = new ArrayList<>(getSIMEntryCount());
    for (int i = 0; i < getSIMEntryCount(); i++) {
      simEntries.add(createSIMEntry(i, deviceUid));
    }
    this.simEntries = Collections.unmodifiableList(simEntries);
  }

  @Override
  public void detach() throws MudpException, InterruptedException {
    for (ISIMEntry simEntry : simEntries) {
      for (IChannel channel : simEntry.getSupportedChannels()) {
        if (channel.isEnslaved()) {
          logger.warn("[{}] - is enslaved, will be nulled", channel);
        }
      }
    }
    simEntries = Collections.emptyList();

    super.detach();
  }

  @Override
  public List<ISIMEntry> getSIMEntries() {
    return simEntries;
  }

  protected static class SIMBGenericEntry extends Entry implements ISIMEntry {

    private final ISCReaderChannel scReaderChannel;
    private final ISCMonitorChannel scMonitorChannel;
    private final IIndicationChannel indicationChannel;

    public SIMBGenericEntry(final ISCReaderChannel scReaderChannel, final ISCMonitorChannel scMonitorChannel, final IIndicationChannel indicationChannel,
        final int simEntryNumber, final DeviceUID deviceUid) {
      super(simEntryNumber, deviceUid);
      this.scReaderChannel = scReaderChannel;
      this.scMonitorChannel = scMonitorChannel;
      this.indicationChannel = indicationChannel;
    }

    @Override
    public List<IChannel> getSupportedChannels() {
      List<IChannel> supportedChannels = new ArrayList<>(1);
      if (scReaderChannel != null) {
        supportedChannels.add(scReaderChannel);
      }
      if (scMonitorChannel != null) {
        supportedChannels.add(scMonitorChannel);
      }
      if (indicationChannel != null) {
        supportedChannels.add(indicationChannel);
      }
      return supportedChannels;
    }

    @Override
    public ISCReaderChannel getSCReaderChannel() {
      if (scReaderChannel == null) {
        throw new UnsupportedOperationException();
      }
      return scReaderChannel;
    }

    @Override
    public ISCMonitorChannel getSCMonitorChannel() {
      if (scMonitorChannel == null) {
        throw new UnsupportedOperationException();
      }
      return scMonitorChannel;
    }

    @Override
    public IIndicationChannel getIndicationChannel() {
      if (indicationChannel == null) {
        throw new UnsupportedOperationException();
      }
      return indicationChannel;
    }

  }

}
