/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.IndicationChannel;
import com.flamesgroup.device.commonb.LoggerChannel;
import com.flamesgroup.device.commonb.ServiceChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.periphery.IPeripheryConnection;
import com.flamesgroup.jdev.DevException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class SIMBOXGenericDevice extends SIMGenericDevice {

  private static final Logger logger = LoggerFactory.getLogger(SIMBOXGenericDevice.class);

  private final IPeripheryConnection peripheryConnection;
  private final ScheduledExecutorService syncScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

  public SIMBOXGenericDevice(final IPeripheryConnection peripheryConnection, final IMudpConnection[] mudpConnections, final DeviceUID deviceUid) {
    super(mudpConnections, deviceUid);
    this.peripheryConnection = peripheryConnection;
  }

  @Override
  public void attach() throws MudpException, InterruptedException {
    try {
      peripheryConnection.resetAllCpus();
    } catch (DevException e) {
      throw new MudpException(e);
    }

    Thread.sleep(200); // give CPUs firmware time to init (necessary to prevent CRC errors after reattaching)

    super.attach();

    syncScheduledExecutorService.scheduleAtFixedRate(() -> {
      try {
        peripheryConnection.toggleSync();
      } catch (DevException e) {
        logger.warn("[{}] - can't toggle SYNC", SIMBOXGenericDevice.this, e);
      }
    }, 500, 500, TimeUnit.MILLISECONDS);
  }

  @Override
  public void detach() throws MudpException, InterruptedException {
    syncScheduledExecutorService.shutdown();

    super.detach();
  }

  protected int calculateRealConnectionNFor(final int entryNumber) {
    return entryNumber / 2 % getConnectionCount();
  }

  protected int calculateRealCpuNFor(final int entryNumber) {
    int cpuEntryPerConnection = getCPUEntryCount() / getConnectionCount();
    int cpuEntryNumberTmp = entryNumber / 2 / getConnectionCount();
    if (entryNumber % 2 == 0) {
      return cpuEntryPerConnection - 1 - cpuEntryNumberTmp;
    } else {
      return cpuEntryNumberTmp;
    }
  }

  @Override
  protected ICPUEntry createCPUEntry(final int cpuEntryNumber, final DeviceUID deviceUid) {
    checkCPUEntryNumber(cpuEntryNumber);

    int entryNumber = cpuEntryNumber / 1;
    int realConnectionN = calculateRealConnectionNFor(entryNumber);
    int realCpuN = calculateRealCpuNFor(entryNumber);
    int serviceRealChannelN = 0;
    int loggerRealChannelN = 1;

    final IServiceChannel serviceChannel = new ServiceChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, serviceRealChannelN), cpuEntryNumber, deviceUid);
    final ILoggerChannel loggerChannel = new LoggerChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, loggerRealChannelN), cpuEntryNumber, deviceUid);
    return new CPUEntry(serviceChannel, loggerChannel, cpuEntryNumber, deviceUid);
  }

  @Override
  protected ISIMEntry createSIMEntry(final int simEntryNumber, final DeviceUID deviceUid) {
    checkSIMEntryNumber(simEntryNumber);

    int entryNumber = simEntryNumber / 3;
    int realConnectionN = calculateRealConnectionNFor(entryNumber);
    int realCpuN = calculateRealCpuNFor(entryNumber);
    int scReaderRealChannelN = simEntryNumber % 3 + 2;
    int scMonitorRealChannelN = simEntryNumber % 3 + 5;
    int indicationRealChannelN = simEntryNumber % 3 + 8;

    final ISCReaderChannel scReaderChannel = new SCReaderChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, scReaderRealChannelN), simEntryNumber, deviceUid);
    final ISCMonitorChannel scMonitorChannel =
        new SCMonitorChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, scMonitorRealChannelN), simEntryNumber, deviceUid);
    final IIndicationChannel indicationChannel =
        new IndicationChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, indicationRealChannelN), simEntryNumber, deviceUid);
    return createSIMBOXEntry(scReaderChannel, scMonitorChannel, indicationChannel, simEntryNumber, deviceUid);
  }

  protected abstract ISIMEntry createSIMBOXEntry(ISCReaderChannel scReaderChannel, ISCMonitorChannel scStatusChannel, IIndicationChannel indicationChannel, int simEntryNumber, DeviceUID deviceUid);

}
