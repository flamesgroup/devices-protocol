/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.device.simb.command.GetSCReaderStateCommand.GetSCReaderStateRequest;
import com.flamesgroup.device.simb.command.GetSCReaderStateCommand.GetSCReaderStateResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class GetSCReaderStateCommand extends Command<GetSCReaderStateRequest, GetSCReaderStateResponse> {

  private static final byte ERRCODE_NOT_START_READER_ERR = (byte) 0x90;

  public GetSCReaderStateCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_NOT_START_READER_ERR) {
      return new SCReaderNonStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public GetSCReaderStateRequest createRequest() {
    return new GetSCReaderStateRequest();
  }

  @Override
  public GetSCReaderStateResponse createResponse() {
    return new GetSCReaderStateResponse();
  }

  public static class GetSCReaderStateRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private GetSCReaderStateRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear().flip();
      return data;
    }

  }

  public static class GetSCReaderStateResponse implements ICommandResponse {

    private SCReaderState state;

    private GetSCReaderStateResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 2) {
        throw new ChannelCommandDecodeException("State length must be 2, but it's " + data.remaining());
      }
      byte primaryState = data.get();
      byte secondaryState = data.get();
      state = new SCReaderState(primaryState, secondaryState);
    }

    public SCReaderState getState() {
      return state;
    }

  }

}
