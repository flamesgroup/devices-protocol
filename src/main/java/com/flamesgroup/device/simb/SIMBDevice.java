/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.IndicationChannelEmpty;
import com.flamesgroup.device.commonb.LoggerChannel;
import com.flamesgroup.device.commonb.ServiceChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SIMBDevice extends SIMGenericDevice {

  private static final Logger logger = LoggerFactory.getLogger(SIMBDevice.class);

  public static final int CPU_ENTRY_COUNT = 11;
  public static final int SIM_ENTRY_COUNT = 20;

  private final boolean restricted;

  public SIMBDevice(final IMudpConnection mudpConnection, final DeviceUID deviceUid, final boolean restricted) {
    super(new IMudpConnection[] {mudpConnection}, deviceUid);
    this.restricted = restricted;
  }

  public SIMBDevice(final IMudpConnection mudpConnection, final DeviceUID deviceUid) {
    this(mudpConnection, deviceUid, false);
  }

  public SIMBDevice(final IMudpConnection mudpConnection) {
    this(mudpConnection, null);
  }

  @Override
  public DeviceType getDeviceType() {
    return restricted ? DeviceType.SIMB_RESTRICTED : DeviceType.SIMB;
  }

  @Override
  public int getCPUEntryCount() {
    return CPU_ENTRY_COUNT;
  }

  @Override
  protected ICPUEntry createCPUEntry(final int cpuEntryNumber, final DeviceUID deviceUid) {
    checkCPUEntryNumber(cpuEntryNumber);

    int realCpuN = cpuEntryNumber;
    int serviceRealChannelN = 0;
    int loggerRealChannelN = 1;

    final IServiceChannel serviceChannel = new ServiceChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, serviceRealChannelN), cpuEntryNumber, deviceUid);
    final ILoggerChannel loggerChannel = new LoggerChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, loggerRealChannelN), cpuEntryNumber, deviceUid);
    return new CPUEntry(serviceChannel, loggerChannel, cpuEntryNumber, deviceUid);
  }

  @Override
  public int getSIMEntryCount() {
    return SIM_ENTRY_COUNT;
  }

  @Override
  protected ISIMEntry createSIMEntry(final int simEntryNumber, final DeviceUID deviceUid) {
    checkSIMEntryNumber(simEntryNumber);

    int realCpuN = simEntryNumber / 2 + 1;
    int scReaderRealChannelN = simEntryNumber % 2 == 0 ? 2 : 3;

    final ISCReaderChannel scReaderChannel = new SCReaderChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, scReaderRealChannelN), simEntryNumber, deviceUid);
    return new SIMBEntry(scReaderChannel, simEntryNumber, deviceUid);
  }

  private static final class SIMBEntry extends SIMBGenericEntry {

    public SIMBEntry(final ISCReaderChannel scReaderChannel, final int simEntryNumber, final DeviceUID deviceUid) {
      super(scReaderChannel, null, new IndicationChannelEmpty(), simEntryNumber, deviceUid); // ISCMonitorChannel supports only by SIMBOX*

      if (logger.isTraceEnabled()) {
        logger.trace("[{}] - created with [{}]", toString(), scReaderChannel);
      }
    }

  }

}
