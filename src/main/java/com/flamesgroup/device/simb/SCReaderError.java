/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

public enum SCReaderError {

  NO_ERR((byte) (0x00 & 0xFF)),
  TIMEOUT((byte) (0x01 & 0xFF)),
  NOT_SUPPORT((byte) (0x02 & 0xFF)),
  REJECTED((byte) (0x03 & 0xFF)),
  SERIAL_PROBLEM((byte) (0x04 & 0xFF)),
  HARDWARE_PROBLEM((byte) (0x05 & 0xFF)),
  CARD_EXTRACTION((byte) (0x06 & 0xFF));

  private final byte code;

  SCReaderError(final byte code) {
    this.code = code;
  }

  public byte getCode() {
    return code;
  }

  public static SCReaderError valueOf(final byte code) {
    short codeUnsigned = (short) (code & 0xFF);
    if (codeUnsigned >= 0 && codeUnsigned < scReaderErrors.length) {
      SCReaderError scReaderError = scReaderErrors[codeUnsigned];
      if (scReaderError != null) {
        return scReaderError;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported code for SCReaderError: 0x%02X", codeUnsigned));
  }

  private static final SCReaderError[] scReaderErrors;

  static {
    short maxCodeUnsigned = 0;
    for (SCReaderError scReaderError : values()) {
      short codeUnsigned = (short) (scReaderError.code & 0xFF);
      if (codeUnsigned > maxCodeUnsigned) {
        maxCodeUnsigned = codeUnsigned;
      }
    }
    scReaderErrors = new SCReaderError[maxCodeUnsigned + 1];
    for (SCReaderError scReaderError : values()) {
      short codeUnsigned = (short) (scReaderError.code & 0xFF);
      scReaderErrors[codeUnsigned] = scReaderError;
    }
  }

}
