/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.periphery.IPeripheryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SIMBOX60Device extends SIMBOXGenericDevice {

  private static final Logger logger = LoggerFactory.getLogger(SIMBOX60Device.class);

  public static final int CPU_ENTRY_COUNT = 20;
  public static final int SIM_ENTRY_COUNT = 60;

  public SIMBOX60Device(final IPeripheryConnection peripheryConnection, final IMudpConnection mudpConnection, final DeviceUID deviceUid) {
    super(peripheryConnection, new IMudpConnection[] {mudpConnection}, deviceUid);
  }

  public SIMBOX60Device(final IPeripheryConnection peripheryConnection, final IMudpConnection mudpConnection) {
    this(peripheryConnection, mudpConnection, null);
  }

  @Override
  public DeviceType getDeviceType() {
    return DeviceType.SIMBOX60;
  }

  @Override
  public int getCPUEntryCount() {
    return CPU_ENTRY_COUNT;
  }

  @Override
  public int getSIMEntryCount() {
    return SIM_ENTRY_COUNT;
  }

  @Override
  protected ISIMEntry createSIMBOXEntry(final ISCReaderChannel scReaderChannel, final ISCMonitorChannel scMonitorChannel, final IIndicationChannel indicationChannel,
      final int simEntryNumber, final DeviceUID deviceUid) {
    return new SIMBOX60Entry(scReaderChannel, scMonitorChannel, indicationChannel, simEntryNumber, deviceUid);
  }

  private static final class SIMBOX60Entry extends SIMBGenericEntry {

    public SIMBOX60Entry(final ISCReaderChannel scReaderChannel, final ISCMonitorChannel scMonitorChannel, final IIndicationChannel indicationChannel,
        final int simEntryNumber, final DeviceUID deviceUid) {
      super(scReaderChannel, scMonitorChannel, indicationChannel, simEntryNumber, deviceUid);

      if (logger.isTraceEnabled()) {
        logger.trace("[{}] - created with [{}], [{}]", toString(), scReaderChannel, scMonitorChannel);
      }
    }

  }

}
