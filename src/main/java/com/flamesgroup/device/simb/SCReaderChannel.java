/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpPacket;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.simb.command.GetSCReaderStateCommand;
import com.flamesgroup.device.simb.command.SCReaderAlreadyStartedErrException;
import com.flamesgroup.device.simb.command.StartSCReaderCommand;
import com.flamesgroup.device.simb.command.StopSCReaderCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SCReaderChannel extends Channel implements ISCReaderChannel, ISCReaderSubChannel {

  private final Logger logger = LoggerFactory.getLogger(SCReaderChannel.class);

  public static final byte COMMAND_GET_READER_AUTOMATE_STATE = (byte) 0x0;
  public static final byte COMMAND_START_READER = (byte) 0x1;
  public static final byte COMMAND_STOP_READER = (byte) 0x2;
  public static final byte APDU = (byte) 0x3; // first APDU is ATR
  public static final byte ERROR_AUTOMATE_STATE = (byte) 0xF;

  private final Lock getSCReaderStateLock = new ReentrantLock();
  private final GetSCReaderStateCommand getSCReaderStateCommand;
  private final Lock scReaderSubChannelLock = new ReentrantLock();
  private final StartSCReaderCommand startSCReaderCommand;
  private final StopSCReaderCommand stopSCReaderCommand;

  private final IChannelData scReaderSubChannelAPDU;
  private final IChannelData scReaderSubChannelError;

  private static final int ATR_TIMEOUT = 10000;
  private ATR atr;
  private SCReaderErrorStateException atrException;
  private final Lock atrLock = new ReentrantLock();
  private final Condition atrReadyCondition = atrLock.newCondition();

  public SCReaderChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int simEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, simEntryNumber, deviceUid);

    getSCReaderStateCommand = new GetSCReaderStateCommand(COMMAND_GET_READER_AUTOMATE_STATE, createChannelData(), 75 + getMaxAttemptTimeout());
    startSCReaderCommand = new StartSCReaderCommand(COMMAND_START_READER, createChannelData(), 100 + getMaxAttemptTimeout());
    stopSCReaderCommand = new StopSCReaderCommand(COMMAND_STOP_READER, createChannelData(), 100 + getMaxAttemptTimeout());

    scReaderSubChannelAPDU = createChannelData();
    scReaderSubChannelError = createChannelData();
  }

  public SCReaderChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int simEntryNumber) {
    this(mudpChannel, mudpChannelAddress, simEntryNumber, null);
  }

  // ISCReaderChannel
  @Override
  public ISCReaderSubChannel getSCReaderSubChannel() {
    return this;
  }

  // ISCReaderSubChannel
  @Override
  public ATR start(final ISCReaderSubChannelHandler scReaderSubChannelHandler) throws ChannelException, InterruptedException {
    if (scReaderSubChannelHandler == null) {
      throw new IllegalArgumentException("SCReaderSubChannelHandler must not be null");
    }

    scReaderSubChannelLock.lock();
    try {
      logger.debug("[{}] - starting SCReader", this);
      scReaderSubChannelAPDU.attach(SCReaderChannel.APDU, new SCReaderSubChannelAPDUHandler(scReaderSubChannelHandler));
      scReaderSubChannelError.attach(SCReaderChannel.ERROR_AUTOMATE_STATE, new SCReaderSubChannelErrorHandler(scReaderSubChannelHandler));
      try {
        try {
          startSCReaderCommand.execute(startSCReaderCommand.createRequest());
        } catch (SCReaderAlreadyStartedErrException e) {
          logger.info("[{}] - attempt to start already started SCReader", this);
          stopSCReaderCommand.execute(stopSCReaderCommand.createRequest());
          startSCReaderCommand.execute(startSCReaderCommand.createRequest());
        }

        try {
          atrLock.lock();
          try {
            if (atr == null) {
              if (!atrReadyCondition.await(ATR_TIMEOUT, TimeUnit.MILLISECONDS)) {
                throw new SCReaderErrorStateException(SCReaderError.TIMEOUT, null);
              } else if (atrException != null) {
                throw atrException;
              }
            }

            logger.debug("[{}] - started SCReader", this);
            return atr;
          } finally {
            atrException = null;
            atrLock.unlock();
          }
        } catch (SCReaderErrorStateException re) {
          try {
            stopSCReaderCommand.execute(stopSCReaderCommand.createRequest());
          } catch (ChannelException | InterruptedException e) {
            e.addSuppressed(re);
            throw e;
          }
          throw re;
        }
      } catch (ChannelException | InterruptedException e) {
        scReaderSubChannelError.detach();
        scReaderSubChannelAPDU.detach();
        throw e;
      }
    } finally {
      scReaderSubChannelLock.unlock();
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    scReaderSubChannelLock.lock();
    try {
      logger.debug("[{}] - stopping SCReader", this);
      try {
        stopSCReaderCommand.execute(stopSCReaderCommand.createRequest());
      } finally {
        atrLock.lock();
        try {
          atr = null;
        } finally {
          atrLock.unlock();
        }
        scReaderSubChannelError.detach();
        scReaderSubChannelAPDU.detach();
      }
      logger.debug("[{}] - stopped SCReader", this);
    } finally {
      scReaderSubChannelLock.unlock();
    }
  }

  @Override
  public void writeAPDUCommand(final APDUCommand command) throws ChannelException, InterruptedException {
    scReaderSubChannelLock.lock();
    try {
      final ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
      APDUCommand.encoding(command, bb);
      bb.flip();

      scReaderSubChannelAPDU.sendReliable(bb);
    } finally {
      scReaderSubChannelLock.unlock();
    }
  }

  @Override
  public SCReaderState getState() throws ChannelException, InterruptedException {
    getSCReaderStateLock.lock();
    try {
      SCReaderState state = getSCReaderStateCommand.execute(getSCReaderStateCommand.createRequest()).getState();
      logger.debug("[{}] - got SCReader state [{}]", this, state);
      return state;
    } finally {
      getSCReaderStateLock.unlock();
    }
  }

  // internal classes
  private final class SCReaderSubChannelAPDUHandler implements IChannelDataHandler {

    private final ISCReaderSubChannelHandler scReaderSubChannelHandler;

    public SCReaderSubChannelAPDUHandler(final ISCReaderSubChannelHandler scReaderSubChannelHandler) {
      this.scReaderSubChannelHandler = scReaderSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + SCReaderChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      atrLock.lock();
      try {
        if (atr == null) {
          atr = ATR.decoding(data);
          atrReadyCondition.signal();
          return;
        }
      } finally {
        atrLock.unlock();
      }
      scReaderSubChannelHandler.handleAPDUResponse(APDUResponse.decoding(data));
    }
  }

  private final class SCReaderSubChannelErrorHandler implements IChannelDataHandler {

    private final ISCReaderSubChannelHandler scReaderSubChannelHandler;

    public SCReaderSubChannelErrorHandler(final ISCReaderSubChannelHandler scReaderSubChannelHandler) {
      this.scReaderSubChannelHandler = scReaderSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + SCReaderChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      SCReaderError error = SCReaderError.valueOf(data.get());
      byte primaryState = data.get();
      byte secondaryState = data.get();
      SCReaderState state = new SCReaderState(primaryState, secondaryState);

      atrLock.lock();
      try {
        if (atr == null) {
          atrException = new SCReaderErrorStateException(error, state);
          atrReadyCondition.signal();
          return;
        }
      } finally {
        atrLock.unlock();
      }

      scReaderSubChannelHandler.handleErrorState(error, state);
    }
  }

}
