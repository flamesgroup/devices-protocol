/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.simb.command.SCMonitorAlreadyStartedErrException;
import com.flamesgroup.device.simb.command.StartSCMonitorCommand;
import com.flamesgroup.device.simb.command.StopSCMonitorCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SCMonitorChannel extends Channel implements ISCMonitorChannel, ISCMonitorSubChannel {

  private final Logger logger = LoggerFactory.getLogger(SCMonitorChannel.class);

  public static final byte COMMAND_START_MONITOR = (byte) 0x1;
  public static final byte COMMAND_STOP_MONITOR = (byte) 0x2;
  public static final byte PRESENT_STATE = (byte) 0xF;

  private final Lock scMonitorSubChannelLock = new ReentrantLock();
  private final StartSCMonitorCommand startSCMonitorCommand;
  private final StopSCMonitorCommand stopSCMonitorCommand;

  private final IChannelData scMonitorSubChannelPresentState;

  public SCMonitorChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int simEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, simEntryNumber, deviceUid);

    startSCMonitorCommand = new StartSCMonitorCommand(COMMAND_START_MONITOR, createChannelData(), 100 + getMaxAttemptTimeout());
    stopSCMonitorCommand = new StopSCMonitorCommand(COMMAND_STOP_MONITOR, createChannelData(), 100 + getMaxAttemptTimeout());

    scMonitorSubChannelPresentState = createChannelData();
  }

  public SCMonitorChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int simEntryNumber) {
    this(mudpChannel, mudpChannelAddress, simEntryNumber, null);
  }

  // ISCMonitorChannel
  @Override
  public ISCMonitorSubChannel getSCMonitorSubChannel() {
    return this;
  }

  // ISCMonitorSubChannel
  @Override
  public void start(final ISCMonitorSubChannelHandler scMonitorSubChannelHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(scMonitorSubChannelHandler, "scMonitorSubChannelHandler must not be null");

    scMonitorSubChannelLock.lock();
    try {
      logger.debug("[{}] - starting SCMonitor", this);
      scMonitorSubChannelPresentState.attach(SCMonitorChannel.PRESENT_STATE, new SCMonitorSubChannelSCStateHandler(scMonitorSubChannelHandler));
      try {
        try {
          startSCMonitorCommand.execute(startSCMonitorCommand.createRequest());
        } catch (SCMonitorAlreadyStartedErrException e) {
          logger.info("[{}] - attempt to start already started SCMonitor", this);
          stopSCMonitorCommand.execute(stopSCMonitorCommand.createRequest());
          startSCMonitorCommand.execute(startSCMonitorCommand.createRequest());
        }
      } catch (ChannelException | InterruptedException e) {
        scMonitorSubChannelPresentState.detach();
        throw e;
      }
      logger.debug("[{}] - started SCMonitor", this);
    } finally {
      scMonitorSubChannelLock.unlock();
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    scMonitorSubChannelLock.lock();
    try {
      logger.debug("[{}] - stopping SCMonitor", this);
      try {
        stopSCMonitorCommand.execute(stopSCMonitorCommand.createRequest());
      } finally {
        scMonitorSubChannelPresentState.detach();
      }
      logger.debug("[{}] - stopped SCMonitor", this);
    } finally {
      scMonitorSubChannelLock.unlock();
    }
  }

  // internal classes
  private final class SCMonitorSubChannelSCStateHandler implements IChannelDataHandler {

    private final ISCMonitorSubChannelHandler scMonitorSubChannelHandler;

    public SCMonitorSubChannelSCStateHandler(final ISCMonitorSubChannelHandler scMonitorSubChannelHandler) {
      this.scMonitorSubChannelHandler = scMonitorSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + SCMonitorChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      byte state = data.get();
      scMonitorSubChannelHandler.handlePresentState(state != 0);
    }

  }

}
