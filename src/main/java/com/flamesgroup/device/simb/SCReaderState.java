/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import java.io.Serializable;

public class SCReaderState implements Serializable {

  private static final long serialVersionUID = 1366396325770038384L;

  private final SCReaderStatePrimary primaryState;
  private final SCReaderStateSecondary secondaryState;

  private final transient String toS;

  public SCReaderState(final SCReaderStatePrimary primaryState, final SCReaderStateSecondary secondaryState) {
    this.primaryState = primaryState;
    this.secondaryState = secondaryState;

    toS = String.format("%s@%x:[%s:%s]", getClass().getSimpleName(), hashCode(), primaryState, secondaryState);
  }

  public SCReaderState(final byte primaryState, final byte secondaryState) {
    this(SCReaderStatePrimary.valueOf(primaryState), SCReaderStateSecondary.valueOf(secondaryState));
  }

  public SCReaderStatePrimary getPrimaryState() {
    return primaryState;
  }

  public SCReaderStateSecondary getSecondaryState() {
    return secondaryState;
  }

  public boolean isSecondaryStateError() {
    return (secondaryState.getCode() & 0xF0) != 0;
  }

  public enum SCReaderStatePrimary {

    NONE((byte) (0x00 & 0xFF)),
    IDLE((byte) (0x01 & 0xFF)),
    DEACTIVATION((byte) (0x02 & 0xFF)),
    COLD_RESET_INIT((byte) (0x03 & 0xFF)),
    COLD_RESET_SET_VCC((byte) (0x04 & 0xFF)),
    COLD_RESET_SET_CLK((byte) (0x05 & 0xFF)),
    COLD_RESET_SET_RST((byte) (0x06 & 0xFF)),
    ATR((byte) (0x07 & 0xFF)),
    PPS((byte) (0x08 & 0xFF)),
    APDU((byte) (0x09 & 0xFF));

    private final byte code;

    SCReaderStatePrimary(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static SCReaderStatePrimary valueOf(final byte code) {
      short codeUnsigned = (short) (code & 0xFF);
      if (codeUnsigned >= 0 && codeUnsigned < scReaderStatePrimaries.length) {
        SCReaderStatePrimary scReaderStatePrimary = scReaderStatePrimaries[codeUnsigned];
        if (scReaderStatePrimary != null) {
          return scReaderStatePrimary;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for SCReaderStatePrimary: 0x%02X", codeUnsigned));
    }

    private static final SCReaderStatePrimary[] scReaderStatePrimaries;

    static {
      short maxCodeUnsigned = 0;
      for (SCReaderStatePrimary scReaderStatePrimary : values()) {
        short codeUnsigned = (short) (scReaderStatePrimary.code & 0xFF);
        if (codeUnsigned > maxCodeUnsigned) {
          maxCodeUnsigned = codeUnsigned;
        }
      }
      scReaderStatePrimaries = new SCReaderStatePrimary[maxCodeUnsigned + 1];
      for (SCReaderStatePrimary scReaderStatePrimary : values()) {
        short codeUnsigned = (short) (scReaderStatePrimary.code & 0xFF);
        scReaderStatePrimaries[codeUnsigned] = scReaderStatePrimary;
      }
    }

  }

  public enum SCReaderStateSecondary {

    NONE((byte) (0x00 & 0xFF)),

    RECEIVE_ATR_TS((byte) (0x01 & 0xFF)),
    RECEIVE_ATR_T0((byte) (0x02 & 0xFF)),
    RECEIVE_ATR_TA1((byte) (0x03 & 0xFF)),
    RECEIVE_ATR_TB1((byte) (0x04 & 0xFF)),
    RECEIVE_ATR_TC1((byte) (0x05 & 0xFF)),
    RECEIVE_ATR_TD1((byte) (0x06 & 0xFF)),
    RECEIVE_ATR_TA2((byte) (0x07 & 0xFF)),
    RECEIVE_ATR_TB2((byte) (0x08 & 0xFF)),
    RECEIVE_ATR_TC2((byte) (0x09 & 0xFF)),
    RECEIVE_ATR_TD2((byte) (0x0A & 0xFF)),
    RECEIVE_ATR_TAi((byte) (0x0B & 0xFF)),
    RECEIVE_ATR_TBi((byte) (0x0C & 0xFF)),
    RECEIVE_ATR_TCi((byte) (0x0D & 0xFF)),
    RECEIVE_ATR_TDi((byte) (0x0E & 0xFF)),
    RECEIVE_ATR_HISTORICAL_Ti((byte) (0x0F & 0xFF)),
    RECEIVE_ATR_TCK((byte) (0x10 & 0xFF)),

    SEND_PPS((byte) (0x20 & 0xFF)),
    RECEIVE_PPS_PPSS((byte) (0x21 & 0xFF)),
    RECEIVE_PPS_PPS0((byte) (0x22 & 0xFF)),
    RECEIVE_PPS_PPS1((byte) (0x23 & 0xFF)),
    RECEIVE_PPS_PPS2((byte) (0x24 & 0xFF)),
    RECEIVE_PPS_PPS3((byte) (0x25 & 0xFF)),
    RECEIVE_PPS_PCK((byte) (0x26 & 0xFF)),

    PEND_AND_PARSE_APDU_COMMAND((byte) (0x40 & 0xFF)),
    SEND_APDU_COMMAND_HEADER((byte) (0x41 & 0xFF)),
    RECEIVE_AND_PROCESS_PROCEDURE_BYTE((byte) (0x42 & 0xFF)),
    SEND_APDU_COMMAND_DATA((byte) (0x43 & 0xFF)),
    RECEIVE_APDU_RESPONSE_DATA((byte) (0x44 & 0xFF)),
    RECEIVE_APDU_RESPONSE_SW2((byte) (0x45 & 0xFF)),
    RENDER_AND_POST_APDU_RESPONSE((byte) (0x46 & 0xFF));

    private final byte code;

    SCReaderStateSecondary(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static SCReaderStateSecondary valueOf(final byte code) {
      short codeUnsigned = (short) (code & 0xFF);
      if (codeUnsigned >= 0 && codeUnsigned < scReaderStateSecondaries.length) {
        SCReaderStateSecondary scReaderStateSecondary = scReaderStateSecondaries[codeUnsigned];
        if (scReaderStateSecondary != null) {
          return scReaderStateSecondary;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for SCReaderStateSecondary: 0x%02X", codeUnsigned));
    }

    private static final SCReaderStateSecondary[] scReaderStateSecondaries;

    static {
      short maxCodeUnsigned = 0;
      for (SCReaderStateSecondary scReaderStateSecondary : values()) {
        short codeUnsigned = (short) (scReaderStateSecondary.code & 0xFF);
        if (codeUnsigned > maxCodeUnsigned) {
          maxCodeUnsigned = codeUnsigned;
        }
      }
      scReaderStateSecondaries = new SCReaderStateSecondary[maxCodeUnsigned + 1];
      for (SCReaderStateSecondary scReaderStateSecondary : values()) {
        short codeUnsigned = (short) (scReaderStateSecondary.code & 0xFF);
        scReaderStateSecondaries[codeUnsigned] = scReaderStateSecondary;
      }
    }

  }

  @Override
  public String toString() {
    return toS;
  }

}
