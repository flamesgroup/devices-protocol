/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.ftb;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.commonb.Device;
import com.flamesgroup.device.commonb.Entry;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.LoggerChannel;
import com.flamesgroup.device.commonb.ServiceChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FtbDevice extends Device implements IFtbDevice {

  private static final Logger logger = LoggerFactory.getLogger(FtbDevice.class);

  public static final int CPU_ENTRY_COUNT = 1;
  public static final int FTB_ENTRY_COUNT = 1;

  private List<IFtbEntry> ftbEntries = Collections.emptyList();

  public FtbDevice(final IMudpConnection mudpConnection, final DeviceUID deviceUid) {
    super(new IMudpConnection[] {mudpConnection}, deviceUid);
  }

  public FtbDevice(final IMudpConnection mudpConnection) {
    this(mudpConnection, null);
  }

  @Override
  public DeviceType getDeviceType() {
    return DeviceType.FTB;
  }

  @Override
  public int getCPUEntryCount() {
    return CPU_ENTRY_COUNT;
  }

  @Override
  protected ICPUEntry createCPUEntry(final int cpuEntryNumber, final DeviceUID deviceUid) {
    checkCPUEntryNumber(cpuEntryNumber);

    int realCpuN = cpuEntryNumber;
    int serviceRealChannelN = 0;
    int loggerRealChannelN = 1;

    final IServiceChannel serviceChannel = new ServiceChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, serviceRealChannelN), cpuEntryNumber, deviceUid);
    final ILoggerChannel loggerChannel = new LoggerChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, loggerRealChannelN), cpuEntryNumber, deviceUid);
    return new CPUEntry(serviceChannel, loggerChannel, cpuEntryNumber, deviceUid);
  }

  @Override
  public int getFTBEntryCount() {
    return FTB_ENTRY_COUNT;
  }

  protected IFtbEntry createFtbEntry(final int ftbEntryNumber, final DeviceUID deviceUid) {
    checkFTBEntryNumber(ftbEntryNumber);

    int realCpuN = ftbEntryNumber;
    int loaderRealChannelN = 2;

    final ILoaderChannel loaderChannel = new LoaderChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, loaderRealChannelN), ftbEntryNumber, deviceUid);
    return new FtbEntry(loaderChannel, ftbEntryNumber, deviceUid);
  }

  protected void checkFTBEntryNumber(final int ftbEntryNumber) {
    if (ftbEntryNumber < 0 || ftbEntryNumber >= getFTBEntryCount()) {
      throw new IllegalArgumentException("ftbEntryNumber must be >= 0 and < " + getFTBEntryCount());
    }
  }

  @Override
  public void attach() throws MudpException, InterruptedException {
    super.attach();

    List<IFtbEntry> ftbEntries = new ArrayList<>(getFTBEntryCount());
    for (int i = 0; i < getFTBEntryCount(); i++) {
      ftbEntries.add(createFtbEntry(i, deviceUid));
    }
    this.ftbEntries = Collections.unmodifiableList(ftbEntries);
  }

  @Override
  public void detach() throws MudpException, InterruptedException {
    for (IFtbEntry ftbEntry : ftbEntries) {
      for (IChannel channel : ftbEntry.getSupportedChannels()) {
        if (channel.isEnslaved()) {
          logger.warn("[{}] - is enslaved, will be nulled", channel);
        }
      }
    }
    ftbEntries = Collections.emptyList();

    super.detach();
  }

  @Override
  public List<IFtbEntry> getFtbEntries() {
    return ftbEntries;
  }

  private static final class FtbEntry extends Entry implements IFtbEntry {

    private final ILoaderChannel loaderChannel;

    public FtbEntry(final ILoaderChannel loaderChannel, final int ftbEntryNumber, final DeviceUID deviceUid) {
      super(ftbEntryNumber, deviceUid);
      this.loaderChannel = loaderChannel;

      if (logger.isTraceEnabled()) {
        logger.trace("[{}] - created with [{}]", toString(), loaderChannel);
      }
    }

    @Override
    public List<IChannel> getSupportedChannels() {
      List<IChannel> supportedChannels = new ArrayList<>(1);
      if (loaderChannel != null) {
        supportedChannels.add(loaderChannel);
      }
      return supportedChannels;
    }

    @Override
    public ILoaderChannel getLoaderChannel() {
      if (loaderChannel == null) {
        throw new UnsupportedOperationException();
      }
      return loaderChannel;
    }

  }

}
