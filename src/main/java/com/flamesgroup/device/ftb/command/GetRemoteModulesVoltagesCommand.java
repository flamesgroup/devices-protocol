/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.ftb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.ftb.RemoteModulesVoltagesFlag;
import com.flamesgroup.device.ftb.command.GetRemoteModulesVoltagesCommand.GetRemoteModulesVoltagesRequest;
import com.flamesgroup.device.ftb.command.GetRemoteModulesVoltagesCommand.GetRemoteModulesVoltagesResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.EnumSet;

public class GetRemoteModulesVoltagesCommand extends Command<GetRemoteModulesVoltagesRequest, GetRemoteModulesVoltagesResponse> {

  private static final byte WORKER_LOADER_ERR_CODE_INVALID_STATE = (byte) (0x8 << 4);
  private static final byte WORKER_LOADER_ERR_CODE_IOCP_ERR = (byte) (0xB << 4);

  public GetRemoteModulesVoltagesCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == WORKER_LOADER_ERR_CODE_INVALID_STATE) {
      return new InvalidStateErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_IOCP_ERR) {
      return new IocpErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public GetRemoteModulesVoltagesRequest createRequest() {
    return new GetRemoteModulesVoltagesRequest();
  }

  @Override
  public GetRemoteModulesVoltagesResponse createResponse() {
    return new GetRemoteModulesVoltagesResponse();
  }

  public static class GetRemoteModulesVoltagesRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private GetRemoteModulesVoltagesRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear().flip();
      return data;
    }
  }

  public static class GetRemoteModulesVoltagesResponse implements ICommandResponse {

    private final EnumSet<RemoteModulesVoltagesFlag> attachedRemoteModulesVoltagesFlags = EnumSet.noneOf(RemoteModulesVoltagesFlag.class);

    private GetRemoteModulesVoltagesResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 1) {
        throw new ChannelCommandDecodeException("Response data must be 1 byte, but it's length " + data.remaining());
      }
      byte flags = data.get();
      if ((flags & 0x01) != 0) {
        attachedRemoteModulesVoltagesFlags.add(RemoteModulesVoltagesFlag.Vm1);
      }
      if ((flags & 0x02) != 0) {
        attachedRemoteModulesVoltagesFlags.add(RemoteModulesVoltagesFlag.Vm2);
      }
    }

    public EnumSet<RemoteModulesVoltagesFlag> getRemoteModulesVoltagesFlags() {
      return attachedRemoteModulesVoltagesFlags;
    }

  }

}
