/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.ftb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.ftb.PageData;
import com.flamesgroup.device.ftb.command.ReadPageCommand.ReadPageRequest;
import com.flamesgroup.device.ftb.command.ReadPageCommand.ReadPageResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ReadPageCommand extends Command<ReadPageRequest, ReadPageResponse> {

  private static final byte WORKER_LOADER_ERR_CODE_INVALID_STATE = (byte) (0x8 << 4);
  private static final byte WORKER_LOADER_ERR_CODE_ARMP_ERR = (byte) (0xD << 4);

  public ReadPageCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == WORKER_LOADER_ERR_CODE_INVALID_STATE) {
      return new InvalidStateErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_ARMP_ERR) {
      return new ArmpErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public ReadPageRequest createRequest() {
    return new ReadPageRequest();
  }

  @Override
  public ReadPageResponse createResponse() {
    return new ReadPageResponse();
  }

  public static class ReadPageRequest implements ICommandRequest {

    private short pageNumber;
    private final ByteBuffer data = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN); // pageNumber length

    private ReadPageRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putShort(pageNumber);
      data.flip();
      return data;
    }

    public ReadPageRequest setPageNumber(final short pageNumber) {
      this.pageNumber = pageNumber;
      return this;
    }
  }

  public static class ReadPageResponse implements ICommandResponse {

    private final PageData pageData = new PageData();

    private ReadPageResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 256) {
        throw new ChannelCommandDecodeException("Page length must be 256 digits, but it's " + data.remaining());
      }
      data.get(pageData.getData());
    }

    public PageData getPageData() {
      return pageData;
    }
  }

}
