/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.ftb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.ftb.command.AttachBoardCommand.AttachBoardRequest;
import com.flamesgroup.device.ftb.command.AttachBoardCommand.AttachBoardResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class AttachBoardCommand extends Command<AttachBoardRequest, AttachBoardResponse> {

  private static final byte WORKER_LOADER_ERR_CODE_INVALID_STATE = (byte) (0x8 << 4);
  private static final byte WORKER_LOADER_ERR_CODE_INCORRECT_BOARD_VOLTAGE = (byte) (0x9 << 4);
  private static final byte WORKER_LOADER_ERR_CODE_AVRP_FIRMWARE_ERR = (byte) (0xA << 4);
  private static final byte WORKER_LOADER_ERR_CODE_IOCP_ERR = (byte) (0xB << 4);
  private static final byte WORKER_LOADER_ERR_CODE_REMOTE_BOARD_NO_V12_ERR = (byte) (0xE << 4);
  private static final byte WORKER_LOADER_ERR_CODE_REMOTE_BOARD_NO_V3_3_ERR = (byte) (0xF << 4);

  public AttachBoardCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == WORKER_LOADER_ERR_CODE_INVALID_STATE) {
      return new InvalidStateErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_INCORRECT_BOARD_VOLTAGE) {
      return new IncorrectBoardVoltageErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_AVRP_FIRMWARE_ERR) {
      return new AvrpFirmwareErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_IOCP_ERR) {
      return new IocpErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_REMOTE_BOARD_NO_V12_ERR) {
      return new RemoteBoardNoV12ErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_REMOTE_BOARD_NO_V3_3_ERR) {
      return new RemoteBoardNoV3_3ErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public AttachBoardRequest createRequest() {
    return new AttachBoardRequest();
  }

  @Override
  public AttachBoardResponse createResponse() {
    return new AttachBoardResponse();
  }

  public static class AttachBoardRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private AttachBoardRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear().flip();
      return data;
    }
  }

  public static class AttachBoardResponse implements ICommandResponse {

    private AttachBoardResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
