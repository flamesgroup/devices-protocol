/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.ftb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.ftb.PageData;
import com.flamesgroup.device.ftb.command.WritePageCommand.WritePageRequest;
import com.flamesgroup.device.ftb.command.WritePageCommand.WritePageResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class WritePageCommand extends Command<WritePageRequest, WritePageResponse> {

  private static final byte WORKER_LOADER_ERR_CODE_INVALID_STATE = (byte) (0x8 << 4);
  private static final byte WORKER_LOADER_ERR_CODE_ARMP_ERR = (byte) (0xD << 4);

  public WritePageCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == WORKER_LOADER_ERR_CODE_INVALID_STATE) {
      return new InvalidStateErrException(e.getErrCode());
    } else if (e.getErrCode() == WORKER_LOADER_ERR_CODE_ARMP_ERR) {
      return new ArmpErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public WritePageRequest createRequest() {
    return new WritePageRequest();
  }

  @Override
  public WritePageResponse createResponse() {
    return new WritePageResponse();
  }

  public static class WritePageRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(3 + PageData.LENGTH).order(ByteOrder.LITTLE_ENDIAN);// pageNumber length + pageData length

    private short pageNumber;
    private PageData pageData;

    private WritePageRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putShort(pageNumber);
      data.put(pageData.getData());
      data.flip();
      return data;
    }

    public WritePageRequest setPageNumber(final short pageNumber) {
      this.pageNumber = pageNumber;
      return this;
    }

    public WritePageRequest setPageData(final PageData pageData) {
      this.pageData = pageData;
      return this;
    }
  }

  public static class WritePageResponse implements ICommandResponse {

    private WritePageResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length is " + data.remaining());
      }
    }
  }

}
