/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.ftb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.ftb.command.AttachBoardCommand;
import com.flamesgroup.device.ftb.command.DetachBoardCommand;
import com.flamesgroup.device.ftb.command.FirmwareIPCCBCommand;
import com.flamesgroup.device.ftb.command.GetRemoteBoardVoltagesCommand;
import com.flamesgroup.device.ftb.command.GetRemoteModulesVoltagesCommand;
import com.flamesgroup.device.ftb.command.ReadPageCommand;
import com.flamesgroup.device.ftb.command.StartLoaderCommand;
import com.flamesgroup.device.ftb.command.StopLoaderCommand;
import com.flamesgroup.device.ftb.command.TurnOffCommand;
import com.flamesgroup.device.ftb.command.TurnOnCommand;
import com.flamesgroup.device.ftb.command.WritePageCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LoaderChannel extends Channel implements ILoaderChannel, ILoaderSubChannel {

  private final Logger logger = LoggerFactory.getLogger(LoaderChannel.class);

  public static final byte COMMAND_ATTACH_BOARD = (byte) 0x01;
  public static final byte COMMAND_DETACH_BOARD = (byte) 0x02;
  public static final byte COMMAND_START_LOADER = (byte) 0x03;
  public static final byte COMMAND_STOP_LOADER = (byte) 0x04;
  public static final byte COMMAND_READ_PAGE = (byte) 0x05;
  public static final byte COMMAND_WRITE_PAGE = (byte) 0x06;
  public static final byte COMMAND_GET_REMOTE_BOARD_VOLTAGES = (byte) 0x07;
  public static final byte COMMAND_TURN_ON = (byte) 0x08;
  public static final byte COMMAND_TURN_OFF = (byte) 0x09;
  public static final byte COMMAND_GET_REMOTE_MODULES_VOLTAGES = (byte) 0x0A;
  public static final byte COMMAND_FIRMWARE_IPCCB = (byte) 0x0B;

  private final Lock attachBoardLock = new ReentrantLock();
  private final AttachBoardCommand attachBoardCommand;
  private final Lock detachBoardLock = new ReentrantLock();
  private final DetachBoardCommand detachBoardCommand;
  private final Lock getRemoteBoardVoltagesLock = new ReentrantLock();
  private final GetRemoteBoardVoltagesCommand getRemoteBoardVoltagesCommand;
  private final Lock turnOnLock = new ReentrantLock();
  private final TurnOnCommand turnOnCommand;
  private final Lock turnOffLock = new ReentrantLock();
  private final TurnOffCommand turnOffCommand;
  private final Lock getRemoteModulesVoltagesLock = new ReentrantLock();
  private final GetRemoteModulesVoltagesCommand getRemoteModulesVoltagesCommand;
  private final Lock firmwareIPCCBLock = new ReentrantLock();
  private final FirmwareIPCCBCommand firmwareIPCCBCommand;

  private final Lock startLoaderLock = new ReentrantLock();
  private final StartLoaderCommand startLoaderCommand;
  private final Lock stopLoaderLock = new ReentrantLock();
  private final StopLoaderCommand stopLoaderCommand;
  private final Lock writeLock = new ReentrantLock();
  private final WritePageCommand writePageCommand;
  private final Lock readLock = new ReentrantLock();
  private final ReadPageCommand readPageCommand;

  public LoaderChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int ftbEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, ftbEntryNumber, deviceUid);

    attachBoardCommand = new AttachBoardCommand(COMMAND_ATTACH_BOARD, createChannelData(), 3600 + getMaxAttemptTimeout());
    detachBoardCommand = new DetachBoardCommand(COMMAND_DETACH_BOARD, createChannelData(), 10 + getMaxAttemptTimeout());
    getRemoteBoardVoltagesCommand = new GetRemoteBoardVoltagesCommand(COMMAND_GET_REMOTE_BOARD_VOLTAGES, createChannelData(), 1870 + getMaxAttemptTimeout());
    turnOnCommand = new TurnOnCommand(COMMAND_TURN_ON, createChannelData(), 10 + getMaxAttemptTimeout());
    turnOffCommand = new TurnOffCommand(COMMAND_TURN_OFF, createChannelData(), 10 + getMaxAttemptTimeout());
    getRemoteModulesVoltagesCommand = new GetRemoteModulesVoltagesCommand(COMMAND_GET_REMOTE_MODULES_VOLTAGES, createChannelData(), 5150 + getMaxAttemptTimeout());
    firmwareIPCCBCommand = new FirmwareIPCCBCommand(COMMAND_FIRMWARE_IPCCB, createChannelData(), 2800 + getMaxAttemptTimeout());

    startLoaderCommand = new StartLoaderCommand(COMMAND_START_LOADER, createChannelData(), 43000 + getMaxAttemptTimeout());
    stopLoaderCommand = new StopLoaderCommand(COMMAND_STOP_LOADER, createChannelData(), 250 + getMaxAttemptTimeout());
    writePageCommand = new WritePageCommand(COMMAND_WRITE_PAGE, createChannelData(), 75 + getMaxAttemptTimeout());
    readPageCommand = new ReadPageCommand(COMMAND_READ_PAGE, createChannelData(), 75 + getMaxAttemptTimeout());
  }

  public LoaderChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int ftbEntryNumber) {
    this(mudpChannel, mudpChannelAddress, ftbEntryNumber, null);
  }

  // ILoaderChannel
  @Override
  public void attachBoard() throws ChannelException, InterruptedException {
    attachBoardLock.lock();
    try {
      logger.debug("[{}] - board attaching", this);
      attachBoardCommand.execute(attachBoardCommand.createRequest());
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - board was attached", this);
      }
    } finally {
      attachBoardLock.unlock();
    }
  }

  @Override
  public void detachBoard() throws ChannelException, InterruptedException {
    detachBoardLock.lock();
    try {
      logger.debug("[{}] - board detaching", this);
      detachBoardCommand.execute(detachBoardCommand.createRequest());
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - board was detached", this);
      }
    } finally {
      detachBoardLock.unlock();
    }
  }

  @Override
  public EnumSet<RemoteBoardVoltagesFlag> getRemoteBoardVoltages() throws ChannelException, InterruptedException {
    getRemoteBoardVoltagesLock.lock();
    try {
      logger.debug("[{}] - getting remote board voltages", this);
      EnumSet<RemoteBoardVoltagesFlag> attachedRemoteBoardVoltagesFlags = getRemoteBoardVoltagesCommand.execute(getRemoteBoardVoltagesCommand.createRequest()).getRemoteBoardVoltagesFlags();
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - got remote board voltages", this);
      }
      return attachedRemoteBoardVoltagesFlags;
    } finally {
      getRemoteBoardVoltagesLock.unlock();
    }
  }

  @Override
  public void turnOn() throws ChannelException, InterruptedException {
    turnOnLock.lock();
    try {
      logger.debug("[{}] - turning on", this);
      turnOnCommand.execute(turnOnCommand.createRequest());
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - turned on", this);
      }
    } finally {
      turnOnLock.unlock();
    }
  }

  @Override
  public void turnOff() throws ChannelException, InterruptedException {
    turnOffLock.lock();
    try {
      logger.debug("[{}] - turning off", this);
      turnOffCommand.execute(turnOffCommand.createRequest());
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - turned off", this);
      }
    } finally {
      turnOffLock.unlock();
    }
  }

  @Override
  public EnumSet<RemoteModulesVoltagesFlag> getRemoteModulesVoltages() throws ChannelException, InterruptedException {
    getRemoteModulesVoltagesLock.lock();
    try {
      logger.debug("[{}] - getting remote modules voltages", this);
      EnumSet<RemoteModulesVoltagesFlag> attachedRemoteModulesVoltagesFlags = getRemoteModulesVoltagesCommand.execute(getRemoteModulesVoltagesCommand.createRequest()).getRemoteModulesVoltagesFlags();
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - got remote modules voltages", this);
      }
      return attachedRemoteModulesVoltagesFlags;
    } finally {
      getRemoteModulesVoltagesLock.unlock();
    }
  }

  @Override
  public void firmwareIPCCB() throws ChannelException, InterruptedException {
    firmwareIPCCBLock.lock();
    try {
      logger.debug("[{}] - IPCCB firmware updating", this);
      firmwareIPCCBCommand.execute(firmwareIPCCBCommand.createRequest());
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - IPCCB firmware updated", this);
      }
    } finally {
      firmwareIPCCBLock.unlock();
    }
  }

  @Override
  public ILoaderSubChannel getLoaderSubChannel() {
    return this;
  }

  // ILoaderSubChannel
  @Override
  public void start(final byte cpuNumber) throws ChannelException, InterruptedException {
    startLoaderLock.lock();
    try {
      logger.debug("[{}] - starting loader", this);
      startLoaderCommand.execute(startLoaderCommand.createRequest().setCpuNumber(cpuNumber));
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - loader started", this);
      }
    } finally {
      startLoaderLock.unlock();
    }
  }

  @Override
  public void stop(final int remoteCRC32) throws ChannelException, InterruptedException {
    stopLoaderLock.lock();
    try {
      logger.debug("[{}] - stopping loader", this);
      stopLoaderCommand.execute(stopLoaderCommand.createRequest().setRemoteCRC32(remoteCRC32));
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - loader stopped", this);
      }
    } finally {
      stopLoaderLock.unlock();
    }

  }

  @Override
  public void writePage(final short pageNumber, final PageData pageData) throws ChannelException, InterruptedException {
    writeLock.lock();
    try {
      logger.debug("[{}] - page writing", this);
      writePageCommand.execute(writePageCommand.createRequest().setPageNumber(pageNumber).setPageData(pageData));
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - page was write", this);
      }
    } finally {
      writeLock.unlock();
    }
  }

  @Override
  public PageData readPage(final short pageNumber) throws ChannelException, InterruptedException {
    readLock.lock();
    try {
      logger.debug("[{}] - page reading", this);
      PageData pageData = readPageCommand.execute(readPageCommand.createRequest().setPageNumber(pageNumber)).getPageData();
      if (logger.isDebugEnabled()) {
        logger.debug("[{}] - page was read", this);
      }
      return pageData;
    } finally {
      readLock.unlock();
    }
  }

}
