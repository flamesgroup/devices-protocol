/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class DeviceUID implements Comparable<DeviceUID>, Serializable {

  private static final long serialVersionUID = 6011637198903403752L;

  public static final int MAX_DEVICE_NUMBER = 0x00FFFFFF;

  private static final String CANONICAL_NAME_FORMAT_PATTERN = "%s-%d";
  private static final Pattern CANONICAL_NAME_PARSE_PATTERN = Pattern.compile("^(?<type>\\w+)-(?<number>\\d+)$");

  private final int uid;

  private final String canonicalName;
  private final String toS;

  public DeviceUID(final int uid) {
    this.uid = uid;

    canonicalName = String.format(CANONICAL_NAME_FORMAT_PATTERN, getDeviceType(), getDeviceNumber());
    toS = String.format("%s(0x%08X)", canonicalName, getUID());
  }

  public DeviceUID(final DeviceType deviceType, final int deviceNumber) {
    this(deviceType.getN() << 24 | deviceNumber);
    if (deviceNumber > MAX_DEVICE_NUMBER) {
      throw new IllegalArgumentException("Device number can't be greater than " + MAX_DEVICE_NUMBER + ", but it's " + deviceNumber);
    }
  }

  public int getUID() {
    return uid;
  }

  public DeviceType getDeviceType() {
    return DeviceType.getDeviceTypeByN((uid >> 24) & 0xFF);
  }

  public int getDeviceNumber() {
    return uid & MAX_DEVICE_NUMBER;
  }

  public String getCanonicalName() {
    return canonicalName;
  }

  @Override
  public int compareTo(final DeviceUID deviceUID) {
    return Integer.compare(this.getUID(), deviceUID.getUID());
  }

  @Override
  public String toString() {
    return toS;
  }

  @Override
  public int hashCode() {
    return uid;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof DeviceUID) {
      DeviceUID deviceUID = (DeviceUID) obj;
      return deviceUID.uid == this.uid;
    }
    return false;
  }

  public static DeviceUID valueFromCanonicalName(final String deviceUIDCanonicalName) {
    Objects.requireNonNull(deviceUIDCanonicalName, "deviceUIDCanonicalName mustn't be null");

    Matcher deviceUIDCanonicalNameMatcher = CANONICAL_NAME_PARSE_PATTERN.matcher(deviceUIDCanonicalName);
    if (deviceUIDCanonicalNameMatcher.matches()) {
      return new DeviceUID(DeviceType.valueOf(deviceUIDCanonicalNameMatcher.group("type")), Integer.parseInt(deviceUIDCanonicalNameMatcher.group("number")));
    } else {
      throw new IllegalArgumentException(String.format("[%s] isn't correspond to pattern '<device type>-<device number>'", deviceUIDCanonicalName));
    }
  }

}
