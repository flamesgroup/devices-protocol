/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.box.IBOXDevAddresses;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.Device;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.LoggerChannel;
import com.flamesgroup.device.commonb.ServiceChannel;
import com.flamesgroup.device.ftb.FtbDevice;
import com.flamesgroup.device.gsmb.GSMB2Device;
import com.flamesgroup.device.gsmb.GSMB3Device;
import com.flamesgroup.device.gsmb.GSMBDevice;
import com.flamesgroup.device.gsmb.GSMBOX4Device;
import com.flamesgroup.device.gsmb.GSMBOX8Device;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.IMudpConnectionErrorHandler;
import com.flamesgroup.device.protocol.mudp.IMudpStream;
import com.flamesgroup.device.protocol.mudp.MudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.mudp.MudpOptions;
import com.flamesgroup.device.protocol.mudp.MudpStreamSPIDev;
import com.flamesgroup.device.protocol.mudp.MudpStreamSocket;
import com.flamesgroup.device.protocol.periphery.IPeripheryConnection;
import com.flamesgroup.device.simb.ISIMDevice;
import com.flamesgroup.device.simb.SIMBDevice;
import com.flamesgroup.device.simb.SIMBOX120Device;
import com.flamesgroup.device.simb.SIMBOX60Device;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.gpio.ISysfsGPIOOut;
import com.flamesgroup.jdev.gpio.SysfsGPIO;
import com.flamesgroup.jdev.spi.SPIDevAddress;
import org.scijava.nativelib.NativeLibraryUtil;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.List;
import java.util.stream.Collectors;

public final class DeviceUtil {

  // MUDP options constants have mainly empirically values
  private static final MudpOptions ETH_MUDP_OPTIONS = new MudpOptions(3, 500, 5, 50);
  private static final MudpOptions SPI_MUDP_OPTIONS = new MudpOptions(10, 500, 5, 50);

  private static final int GSMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT = 1_200_000; // ~ 1 hour => 1 * 60 * 60 * 1000 / 3 (where 3 ms - theoretical exchange iteration duration for GSMBOX)
  private static final int SIMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT = 300_000; // ~ 1 hour => 1 * 60 * 60 * 1000 / 12 (where 12 ms - theoretical exchange iteration duration for SIMBOX)

  private DeviceUtil() {
  }

  public static DeviceClass getDeviceClass() {
    switch (NativeLibraryUtil.getArchitecture()) {
      case LINUX_32:
      case LINUX_64:
      case WINDOWS_32:
      case WINDOWS_64:
      case OSX_32:
      case OSX_64:
        return DeviceClass.ETH;
      case LINUX_ARM:
        return DeviceClass.SPI;
      default:
        return DeviceClass.UNDEFINED;
    }
  }

  public static IDevice createEthDevice(final SocketAddress socketAddress, final IMudpConnectionErrorHandler errorHandler) throws ChannelException, MudpException, InterruptedException {
    IDevice device = new StubDevice(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS));

    DeviceUID deviceUID;

    device.attach();
    try {
      ICPUEntry cpuEntry = device.getCPUEntries().get(0);
      IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
      serviceChannel.enslave();
      try {
        deviceUID = serviceChannel.getInfo().getDeviceUID();
      } finally {
        serviceChannel.free();
      }
    } finally {
      device.detach();
    }

    switch (deviceUID.getDeviceType()) {
      case FTB:
        return new FtbDevice(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID);
      case GSMB:
        return new GSMBDevice(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID);
      case GSMB_RESTRICTED:
        return new GSMBDevice(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID, true);
      case GSMB2:
        return new GSMB2Device(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID);
      case GSMB2_RESTRICTED:
        return new GSMB2Device(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID, true);
      case GSMB3:
        return new GSMB3Device(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID);
      case GSMB3_RESTRICTED:
        return new GSMB3Device(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID, true);
      case SIMB:
        return new SIMBDevice(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID);
      case SIMB_RESTRICTED:
        return new SIMBDevice(new MudpConnection(new MudpStreamSocket(socketAddress), ETH_MUDP_OPTIONS, errorHandler), deviceUID, true);
      default: {
        throw new IllegalStateException("Unsupported device type [" + deviceUID.getDeviceType() + "] for creating " + DeviceClass.ETH + " class device");
      }
    }
  }

  public static IDevice createEthDevice(final SocketAddress socketAddress) throws ChannelException, MudpException, InterruptedException {
    return createEthDevice(socketAddress, null);
  }

  public static IGSMDevice createEthGSMDevice(final SocketAddress socketAddress, final IMudpConnectionErrorHandler errorHandler) throws ChannelException, MudpException, InterruptedException {
    IDevice device = createEthDevice(socketAddress, errorHandler);
    if (device instanceof IGSMDevice) {
      return (IGSMDevice) device;
    } else {
      throw new IllegalStateException("Can't cast [" + device.getClass().getSimpleName() + "] to " + IGSMDevice.class.getSimpleName());
    }
  }

  public static IGSMDevice createEthGSMDevice(final SocketAddress socketAddress) throws ChannelException, MudpException, InterruptedException {
    return createEthGSMDevice(socketAddress, null);
  }

  public static ISIMDevice createEthSIMDevice(final SocketAddress socketAddress, final IMudpConnectionErrorHandler errorHandler) throws ChannelException, MudpException, InterruptedException {
    IDevice device = createEthDevice(socketAddress, errorHandler);
    if (device instanceof ISIMDevice) {
      return (ISIMDevice) device;
    } else {
      throw new IllegalStateException("Can't cast [" + device.getClass().getSimpleName() + "] to " + ISIMDevice.class.getSimpleName());
    }
  }

  public static ISIMDevice createEthSIMDevice(final SocketAddress socketAddress) throws ChannelException, MudpException, InterruptedException {
    return createEthSIMDevice(socketAddress, null);
  }

  public static IDevice createSpiDevice(final IMudpConnectionErrorHandler errorHandler) throws IOException, DevException {
    if (getDeviceClass() != DeviceClass.SPI) {
      throw new IllegalStateException("Can't create " + DeviceClass.SPI + " class device within " + getDeviceClass() + " class device");
    }

    final DeviceUID deviceUID = BOXUtil.readDeviceUID();
    final IBOXDevAddresses boxDevAddresses = BOXUtil.getBOXDevAddresses();
    final List<SPIDevAddress> spiDevAddresses = boxDevAddresses.getSPIDevAddresses();

    final ISysfsGPIOOut boot0 = new SysfsGPIO(boxDevAddresses.getBOOT0SysfsGPIOAddress());
    final ISysfsGPIOOut pboot = new SysfsGPIO(boxDevAddresses.getPBOOTSysfsGPIOAddress());
    final List<ISysfsGPIOOut> rsts = boxDevAddresses.getRSTSysfsGPIOAddresses().stream().map(SysfsGPIO::new).collect(Collectors.toList());
    final IPeripheryConnection peripheryConnection = new IPeripheryConnection() {
      private int toggleSyncCounter = 0;

      @Override
      public void resetAllCpus() throws DevException {
        boot0.setValue(false);
        pboot.setValue(false);
        for (ISysfsGPIOOut rst : rsts) {
          rst.setValue(false);
        }
        for (ISysfsGPIOOut rst : rsts) {
          rst.setValue(true);
        }
      }

      @Override
      public void toggleSync() throws DevException {
        pboot.setValue(toggleSyncCounter++ % 2 == 1);
      }
    };

    switch (deviceUID.getDeviceType()) {
      case GSMBOX4: {
        final int spiChainSlavesCount = GSMBOX4Device.CPU_ENTRY_COUNT;
        IMudpStream mudpStream = new MudpStreamSPIDev(spiDevAddresses.get(0), spiChainSlavesCount, 3, 3, GSMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT);
        IMudpConnection mudpConnection = new MudpConnection(mudpStream, SPI_MUDP_OPTIONS, errorHandler);
        return new GSMBOX4Device(peripheryConnection, mudpConnection, deviceUID);
      }
      case GSMBOX8: {
        final int spiChainSlavesCount = GSMBOX8Device.CPU_ENTRY_COUNT / 2;
        IMudpStream mudpStream0 = new MudpStreamSPIDev(spiDevAddresses.get(0), spiChainSlavesCount, 3, 3, GSMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT);
        IMudpStream mudpStream1 = new MudpStreamSPIDev(spiDevAddresses.get(1), spiChainSlavesCount, 3, 3, GSMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT);
        IMudpConnection mudpConnection0 = new MudpConnection(mudpStream0, SPI_MUDP_OPTIONS, errorHandler);
        IMudpConnection mudpConnection1 = new MudpConnection(mudpStream1, SPI_MUDP_OPTIONS, errorHandler);
        return new GSMBOX8Device(peripheryConnection, mudpConnection0, mudpConnection1, deviceUID);
      }
      case SIMBOX60: {
        final int spiChainSlavesCount = SIMBOX60Device.CPU_ENTRY_COUNT;
        IMudpStream mudpStream = new MudpStreamSPIDev(spiDevAddresses.get(0), spiChainSlavesCount, 3, 3, SIMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT);
        IMudpConnection mudpConnection = new MudpConnection(mudpStream, SPI_MUDP_OPTIONS, errorHandler);
        return new SIMBOX60Device(peripheryConnection, mudpConnection, deviceUID);
      }
      case SIMBOX120: {
        final int spiChainSlavesCount = SIMBOX120Device.CPU_ENTRY_COUNT / 2;
        IMudpStream mudpStream0 = new MudpStreamSPIDev(spiDevAddresses.get(0), spiChainSlavesCount, 3, 3, SIMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT);
        IMudpStream mudpStream1 = new MudpStreamSPIDev(spiDevAddresses.get(1), spiChainSlavesCount, 3, 3, SIMBOX_STATISTICS_GATHERING_ITERATION_AMOUNT);
        IMudpConnection mudpConnection0 = new MudpConnection(mudpStream0, SPI_MUDP_OPTIONS, errorHandler);
        IMudpConnection mudpConnection1 = new MudpConnection(mudpStream1, SPI_MUDP_OPTIONS, errorHandler);
        return new SIMBOX120Device(peripheryConnection, mudpConnection0, mudpConnection1, deviceUID);
      }
      default: {
        throw new IllegalStateException("Unsupported device type [" + deviceUID.getDeviceType() + "] for creating " + DeviceClass.SPI + " class device");
      }
    }
  }
  public static IDevice createSpiDevice() throws IOException, DevException {
    return createSpiDevice(null);
  }

  public static IGSMDevice createSpiGSMDevice(final IMudpConnectionErrorHandler errorHandler) throws IOException, DevException {
    IDevice device = createSpiDevice(errorHandler);
    if (device instanceof IGSMDevice) {
      return (IGSMDevice) device;
    } else {
      throw new IllegalStateException("Can't cast [" + device.getClass().getSimpleName() + "] to " + IGSMDevice.class.getSimpleName());
    }
  }

  public static IGSMDevice createSpiGSMDevice() throws IOException, DevException {
    return createSpiGSMDevice(null);
  }

  public static ISIMDevice createSpiSIMDevice(final IMudpConnectionErrorHandler errorHandler) throws IOException, DevException {
    IDevice device = createSpiDevice(errorHandler);
    if (device instanceof ISIMDevice) {
      return (ISIMDevice) device;
    } else {
      throw new IllegalStateException("Can't cast [" + device.getClass().getSimpleName() + "] to " + ISIMDevice.class.getSimpleName());
    }
  }

  public static ISIMDevice createSpiSIMDevice() throws IOException, DevException {
    return createSpiSIMDevice(null);
  }

  private static final class StubDevice extends Device {

    public StubDevice(final IMudpConnection mudpConnection, final DeviceUID deviceUid) {
      super(new IMudpConnection[] {mudpConnection}, deviceUid);
    }

    public StubDevice(final IMudpConnection mudpConnection) {
      this(mudpConnection, null);
    }

    @Override
    public DeviceType getDeviceType() {
      return DeviceType.NONE;
    }

    @Override
    public int getCPUEntryCount() {
      return 1;
    }

    @Override
    protected ICPUEntry createCPUEntry(final int cpuEntryNumber, final DeviceUID deviceUid) {
      checkCPUEntryNumber(cpuEntryNumber);

      int realCpuN = 0;
      int serviceRealChannelN = 0;
      int loggerRealChannelN = 1;

      final IServiceChannel serviceChannel = new ServiceChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, serviceRealChannelN), cpuEntryNumber, deviceUid);
      final ILoggerChannel loggerChannel = new LoggerChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, loggerRealChannelN), cpuEntryNumber, deviceUid);
      return new CPUEntry(serviceChannel, loggerChannel, cpuEntryNumber, deviceUid);
    }

  }

}
