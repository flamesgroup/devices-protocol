/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import java.util.HashMap;
import java.util.Map;

public enum DeviceType {

  NONE(0b00000000, DeviceClass.UNDEFINED),
  SERVER(0b00010000, DeviceClass.UNDEFINED),
  GSMB(0b00100000, DeviceClass.ETH),
  SIMB(0b00110000, DeviceClass.ETH),
  GSMB2(0b01000000, DeviceClass.ETH),
  FTB(0b01010000, DeviceClass.ETH),
  GSMB3(0b01100000, DeviceClass.ETH),
  GSMB_RESTRICTED(0b10000000 + GSMB.getN(), DeviceClass.ETH),
  SIMB_RESTRICTED(0b10000000 + SIMB.getN(), DeviceClass.ETH),
  GSMB2_RESTRICTED(0b10000000 + GSMB2.getN(), DeviceClass.ETH),
  GSMB3_RESTRICTED(0b10000000 + GSMB3.getN(), DeviceClass.ETH),
  GSMBOX4(0b01110000 + 0b0001, DeviceClass.SPI),
  GSMBOX8(0b01110000 + 0b0000, DeviceClass.SPI),
  SIMBOX60(0b01110000 + 0b1001, DeviceClass.SPI),
  SIMBOX120(0b01110000 + 0b1000, DeviceClass.SPI);

  private final int n;
  private final DeviceClass deviceClass;

  DeviceType(final int type, final DeviceClass deviceClass) {
    this.n = type;
    this.deviceClass = deviceClass;
  }

  public int getN() {
    return n;
  }

  public DeviceClass getDeviceClass() {
    return deviceClass;
  }

  public static DeviceType getDeviceTypeByN(final int n) {
    return mapN2Enum.get(n);
  }

  private static final Map<Integer, DeviceType> mapN2Enum = new HashMap<>();

  static {
    for (DeviceType deviceType : DeviceType.values()) {
      mapN2Enum.put(deviceType.getN(), deviceType);
    }
  }

}
