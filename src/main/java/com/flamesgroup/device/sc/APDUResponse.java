/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.device.helper.DataRepresentationHelper;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class APDUResponse implements Serializable {

  private static final long serialVersionUID = 529218200316647798L;

  private static final byte HEADER_SIZE = 2;

  private final byte[] data;
  private final byte sw1;
  private final byte sw2;
  private final Status status;

  public APDUResponse(final byte[] data, final byte sw1, final byte sw2) {
    this.data = data;
    this.sw1 = sw1;
    this.sw2 = sw2;

    status = Status.valueOf(sw1, sw2);
  }

  public byte[] getData() {
    return data;
  }

  public byte getSW1() {
    return sw1;
  }

  public byte getSW2() {
    return sw2;
  }

  public Status getStatus() {
    return status;
  }

  public int getLength() {
    return data.length + 2;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof APDUResponse)) {
      return false;
    }
    APDUResponse that = (APDUResponse) obj;
    return Arrays.equals(this.data, that.data) && this.sw1 == that.sw1 && this.sw2 == that.sw2;
  }

  public int hashCode() {
    int result = 17;
    result = 31 * result + Arrays.hashCode(data);
    result = 31 * result + sw1;
    result = 31 * result + sw2;
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[D:[");
    DataRepresentationHelper.appendHexArrayString(sb, data);
    sb.append("]");
    sb.append(" S:").append(status);
    sb.append("(sw1:");
    DataRepresentationHelper.appendHexString(sb, sw1);
    sb.append(" sw2:");
    DataRepresentationHelper.appendHexString(sb, sw2);
    sb.append(")]");
    return sb.toString();
  }

  public static APDUResponse decoding(final ByteBuffer byteBuffer) {
    int dataLength = byteBuffer.limit() - HEADER_SIZE;
    byte[] data = new byte[dataLength];
    byteBuffer.get(data);

    byte sw1 = byteBuffer.get();
    byte sw2 = byteBuffer.get();
    return new APDUResponse(data, sw1, sw2);
  }

  public static void encoding(final APDUResponse apduResponse, final ByteBuffer byteBuffer) {
    byteBuffer.put(apduResponse.getData());
    byteBuffer.put(apduResponse.getSW1());
    byteBuffer.put(apduResponse.getSW2());
  }

  public enum Status {

    UNKNOWN_RESPONSE,

    // Responses to commands which are correctly executed
    NORMAL_ENDING,
    NORMAL_ENDING_WITH_SIM_COMMAND,
    NORMAL_ENDING_WITH_ADDITIONAL_SIM_DATA,
    NORMAL_ENDING_WITH_ADDITIONAL_DATA,

    // Responses to commands which are postponed
    APPLICATION_TOOLKIT_BUSY,

    // Memory management
    NEED_INTERNAL_UPDATE_ROUTINE,
    MEMORY_PROBLEM,

    // Referencing management
    NO_EF_SELECTED,
    INVALID_ADDRESS,
    FILE_ID_NOT_FOUND,
    FILE_INCONSISTENT_WITH_COMMAND,

    // Security management
    NO_CHV_INITIALIZED,
    ACCESS_CONDITION_NOT_FULFILLED,
    CONTRADICTION_WITH_CHV_STATUS,
    CONTRADICTION_WITH_INVALIDATION_STATUS,
    CHV_BLOCKED,
    INCREASE_CANNOT_BE_PERFORMED,

    // Application independent errors
    INCORRECT_PARAMETER_P3,
    INCORRECT_PARAMETER_P1_OR_P2,
    UNKNOWN_INSTRUCTION_CODE,
    WRONG_INSTRUCTION_CLASS,
    TECHNICAL_PROBLEM_WITH_NO_DIAGNOSTIC;

    public static Status valueOf(final byte sw1, final byte sw2) {
      switch (sw1) {
        case (byte) 0x90:
          return NORMAL_ENDING;
        case (byte) 0x91:
          return NORMAL_ENDING_WITH_SIM_COMMAND;
        case (byte) 0x9E:
          return NORMAL_ENDING_WITH_ADDITIONAL_SIM_DATA;
        case (byte) 0x9F:
          return NORMAL_ENDING_WITH_ADDITIONAL_DATA;

        case (byte) 0x93:
          switch (sw2) {
            case (byte) 0x00:
              return APPLICATION_TOOLKIT_BUSY;
          }

        case (byte) 0x92:
          switch (sw2) {
            case (byte) 0x40:
              return MEMORY_PROBLEM;
            default:
              return NEED_INTERNAL_UPDATE_ROUTINE;
          }

        case (byte) 0x94:
          switch (sw2) {
            case (byte) 0x00:
              return NO_EF_SELECTED;
            case (byte) 0x02:
              return INVALID_ADDRESS;
            case (byte) 0x04:
              return FILE_ID_NOT_FOUND;
            case (byte) 0x08:
              return FILE_INCONSISTENT_WITH_COMMAND;
          }

        case (byte) 0x98:
          switch (sw2) {
            case (byte) 0x02:
              return NO_CHV_INITIALIZED;
            case (byte) 0x04:
              return ACCESS_CONDITION_NOT_FULFILLED;
            case (byte) 0x08:
              return CONTRADICTION_WITH_CHV_STATUS;
            case (byte) 0x10:
              return CONTRADICTION_WITH_INVALIDATION_STATUS;
            case (byte) 0x40:
              return CHV_BLOCKED;
            case (byte) 0x50:
              return INCREASE_CANNOT_BE_PERFORMED;
          }

        case (byte) 0x67:
          return INCORRECT_PARAMETER_P3;
        case (byte) 0x6B:
          return INCORRECT_PARAMETER_P1_OR_P2;
        case (byte) 0x6D:
          return UNKNOWN_INSTRUCTION_CODE;
        case (byte) 0x6E:
          return WRONG_INSTRUCTION_CLASS;
        case (byte) 0x6F:
          return TECHNICAL_PROBLEM_WITH_NO_DIAGNOSTIC;

        default:
          return UNKNOWN_RESPONSE;
      }
    }

  }

}
