/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.device.helper.DataRepresentationHelper;

import java.nio.ByteBuffer;
import java.util.Objects;

public class RecordOperation {

  public enum RecordOperationMode {

    NEXT_RECORD_MODE((byte) 0x02),
    PREV_RECORD_MODE((byte) 0x03),
    ABSOLUTE_MODE((byte) 0x04);

    private final byte code;

    RecordOperationMode(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static RecordOperationMode valueOf(final byte code) {
      for (RecordOperationMode recordOperationMode : values()) {
        if (recordOperationMode.getCode() == code) {
          return recordOperationMode;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for RecordOperationMode: 0x%02X", code));
    }

  }

  private final RecordOperationMode mode;
  private final int number;

  public RecordOperation(final RecordOperationMode mode, final int number) {
    Objects.requireNonNull(mode, "mode mustn't be null");
    if (number < 0 || number > 0xFF) {
      throw new IllegalArgumentException("number can't be less 0 or above 255 but it's " + number);
    }
    this.mode = mode;
    this.number = (mode == RecordOperationMode.ABSOLUTE_MODE) ? number : 0;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[M:").append(mode);
    sb.append(" N:");
    DataRepresentationHelper.appendHexString(sb, (byte) number);
    sb.append("]");
    return sb.toString();
  }

  public RecordOperationMode getMode() {
    return mode;
  }

  public int getNumber() {
    return number;
  }

  public static void encoding(final RecordOperation recordOperation, final ByteBuffer byteBuffer) {
    byteBuffer.put((byte) recordOperation.getNumber());
    byteBuffer.put(recordOperation.getMode().getCode());
  }

}
