/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import java.nio.ByteBuffer;

public class FileAccessConditions {

  private AccessCondition update;
  private AccessCondition read;
  private AccessCondition increase;
  private AccessCondition invalidate;
  private AccessCondition rehabilitate;

  public FileAccessConditions(int mask) {
    invalidate = AccessCondition.valueOf(trim(mask));
    mask >>= 4;
    rehabilitate = AccessCondition.valueOf(trim(mask));
    mask >>= 8; // Skip RFU
    increase = AccessCondition.valueOf(trim(mask));
    mask >>= 4;
    update = AccessCondition.valueOf(trim(mask));
    mask >>= 4;
    read = AccessCondition.valueOf(trim(mask));
  }

  public FileAccessConditions() {
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof FileAccessConditions)) {
      return false;
    }
    FileAccessConditions that = (FileAccessConditions) obj;
    if (!this.update.equals(that.update)) {
      return false;
    }
    if (!this.read.equals(that.read)) {
      return false;
    }
    if (!this.increase.equals(that.increase)) {
      return false;
    }
    if (!this.invalidate.equals(that.invalidate)) {
      return false;
    }
    if (!this.rehabilitate.equals(that.rehabilitate)) {
      return false;
    }
    return true;
  }

  public void setUpdate(final AccessCondition update) {
    this.update = update;
  }

  public void setRead(final AccessCondition read) {
    this.read = read;
  }

  public void setIncrease(final AccessCondition increase) {
    this.increase = increase;
  }

  public void setInvalidate(final AccessCondition invalidate) {
    this.invalidate = invalidate;
  }

  public void setRehabilitate(final AccessCondition rehabilitate) {
    this.rehabilitate = rehabilitate;
  }

  public AccessCondition getIncrease() {
    return increase;
  }

  public AccessCondition getInvalidate() {
    return invalidate;
  }

  public AccessCondition getRead() {
    return read;
  }

  public AccessCondition getRehabilitate() {
    return rehabilitate;
  }

  public AccessCondition getUpdate() {
    return update;
  }

  private static int trim(final int mask) {
    return mask & 0xf;
  }

  public static void encoding(final FileAccessConditions fileAccessConditions, final ByteBuffer byteBuffer) {
    byteBuffer.put((byte) (fileAccessConditions.getUpdate().getCode() | (fileAccessConditions.getRead().getCode() << 4)));
    byteBuffer.put((byte) (0 | (fileAccessConditions.getIncrease().getCode() << 4)));
    byteBuffer.put((byte) (fileAccessConditions.getInvalidate().getCode() | (fileAccessConditions.getRehabilitate().getCode() << 4)));
  }

}
