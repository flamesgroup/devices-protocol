/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SelectEFResponse {

  private static final int BASE_LENGTH = 14;

  private int fileSize;
  private FileID fileID;
  private FileType fileType;
  /**
   * For FileType.EF only
   */
  private boolean increaseAllowed;
  private FileAccessConditions accessConditions;
  private FileStatus fileStatus;
  private int length;
  private FileStructure structure;
  /**
   * For FileStructure.LINEAR_FIXED and FileStructure.CYCLIC
   */
  private int recordLength;
  private byte[] rfu;

  private int responseLength;

  public SelectEFResponse(final APDUResponse response) {
    ByteBuffer bb = ByteBuffer.wrap(response.getData()).order(ByteOrder.BIG_ENDIAN);
    bb.getShort(); // RFU
    fileSize = bb.getShort();
    fileID = FileID.valueOf(bb.getShort());
    fileType = FileType.valueOf(bb.get());
    increaseAllowed = (bb.get() & 0x40 /* bit 7 */) > 0;
    readAccessConditions(bb);
    fileStatus = new FileStatus(bb.get());
    length = bb.get();
    structure = FileStructure.valueOf(bb.get());
    responseLength = BASE_LENGTH;
    if (bb.remaining() > 0) {
      recordLength = bb.get();
      responseLength++;
    }
    if (bb.remaining() > 0) {
      rfu = new byte[bb.remaining()];
      bb.get(rfu);
      responseLength += rfu.length;
    }
  }

  private void readAccessConditions(final ByteBuffer bb) {
    byte[] accessConditionsData = new byte[4];
    bb.get(accessConditionsData, 1, 3);
    ByteBuffer accessConditionsBuffer = ByteBuffer.allocate(accessConditionsData.length).order(ByteOrder.BIG_ENDIAN);
    accessConditionsBuffer.clear();
    accessConditionsBuffer.put(accessConditionsData);
    accessConditionsBuffer.flip();
    accessConditions = new FileAccessConditions(accessConditionsBuffer.getInt());
  }

  public int getResponseLength() {
    return responseLength;
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof SelectEFResponse)) {
      return false;
    }
    SelectEFResponse that = (SelectEFResponse) obj;
    if (this.fileSize != that.fileSize) {
      return false;
    }
    if (!this.fileID.equals(that.fileID)) {
      return false;
    }

    if (!this.fileType.equals(that.fileType)) {
      return false;
    }

    if (this.increaseAllowed != that.increaseAllowed) {
      return false;
    }

    if (!this.accessConditions.equals(that.accessConditions)) {
      return false;
    }

    if (!this.fileStatus.equals(that.fileStatus)) {
      return false;
    }

    if (this.length != that.length) {
      return false;
    }

    if (!this.structure.equals(that.structure)) {
      return false;
    }

    if (this.recordLength != that.recordLength) {
      return false;
    }

    return true;
  }

  public void setFileSize(final int fileSize) {
    this.fileSize = fileSize;
  }

  public void setFileID(final FileID fileID) {
    this.fileID = fileID;
  }

  public void setFileType(final FileType fileType) {
    this.fileType = fileType;
  }

  public void setIncreaseAllowed(final boolean increaseAllowed) {
    this.increaseAllowed = increaseAllowed;
  }

  public void setAccessConditions(final FileAccessConditions accessConditions) {
    this.accessConditions = accessConditions;
  }

  public void setFileStatus(final FileStatus fileStatus) {
    this.fileStatus = fileStatus;
  }

  public void setLength(final int length) {
    this.length = length;
  }

  public void setStructure(final FileStructure structure) {
    this.structure = structure;
  }

  public int getFileSize() {
    return fileSize;
  }

  public FileID getFileID() {
    return fileID;
  }

  public FileType getFileType() {
    return fileType;
  }

  public boolean isIncreaseAllowed() {
    return increaseAllowed;
  }

  public FileAccessConditions getAccessConditions() {
    return accessConditions;
  }

  public FileStatus getFileStatus() {
    return fileStatus;
  }

  public int getLength() {
    return length;
  }

  public FileStructure getStructure() {
    return structure;
  }

  public int getRecordLength() {
    return recordLength;
  }

}
