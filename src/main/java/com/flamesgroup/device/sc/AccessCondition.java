/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import java.util.HashMap;
import java.util.Map;

public enum AccessCondition {

  ALWAYS(0x00),
  CHV1(0x01),
  CHV2(0x02),
  RFU(0x03),
  ADM1(0x04),
  ADM2(0x05),
  ADM3(0x06),
  ADM4(0x07),
  ADM5(0x08),
  ADM6(0x09),
  ADM7(0x0A),
  ADM8(0x0B),
  ADM9(0x0C),
  ADM10(0x0D),
  ADM11(0x0E),
  NEVER(0x0F);

  private final int code;

  AccessCondition(final int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  private static final Map<Integer, AccessCondition> conditions = new HashMap<>();

  public static AccessCondition valueOf(final int mask) {
    return conditions.get(mask);
  }

  static {
    for (AccessCondition c : values()) {
      conditions.put(c.getCode(), c);
    }
  }

}
