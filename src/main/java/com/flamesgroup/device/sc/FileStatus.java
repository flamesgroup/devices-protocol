/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

public class FileStatus {

  private int mask;

  public FileStatus(final int mask) {
    this.mask = mask;
  }

  public void setInvalidated(final boolean invalidated) {
    if (invalidated) {
      mask |= 0x1;
    } else {
      mask &= 0xfe;
    }
  }

  public void setReadableUpdatable(final boolean readable) {
    if (readable) {
      mask |= 0x4;
    } else {
      mask &= 0xfb;
    }
  }

  public boolean isInvalidated() {
    return (mask & 0x1) > 0;
  }

  public boolean isReadableUpdatable() {
    return (mask & 0x4) > 0;
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof FileStatus)) {
      return false;
    }
    FileStatus that = (FileStatus) obj;
    return this.mask == that.mask;
  }

}
