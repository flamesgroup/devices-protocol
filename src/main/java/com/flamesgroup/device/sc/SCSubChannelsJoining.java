/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannel;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannelHandler;
import com.flamesgroup.device.gsmb.SCEmulatorError;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.unit.ISCInternalErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SCSubChannelsJoining implements ISCReaderSubChannelHandler, ISCEmulatorSubChannelHandler {

  static final Logger logger = LoggerFactory.getLogger(SCSubChannelsJoining.class);

  private final ISCReaderSubChannel scReaderSubChannel;
  private final ISCEmulatorSubChannel scEmulatorSubChannel;
  private final ISCInternalErrorHandler internalErrorHandler;
  private final boolean disablePhase2pSupport;

  private volatile SCSubChannelsJoiningThread scSubChannelsJoiningThread;

  @SuppressWarnings("unchecked")
  private final BlockingQueue<Object> queue = new LinkedBlockingQueue();

  private final String toS;

  public SCSubChannelsJoining(final ISCReaderSubChannel scReaderSubChannel, final ISCEmulatorSubChannel scEmulatorSubChannel, final ISCInternalErrorHandler internalErrorHandler,
      final boolean disablePhase2pSupport) {
    Objects.requireNonNull(scReaderSubChannel, "scReaderSubChannel mustn't be null");
    Objects.requireNonNull(scEmulatorSubChannel, "scEmulatorSubChannel mustn't be null");
    this.scReaderSubChannel = scReaderSubChannel;
    this.scEmulatorSubChannel = scEmulatorSubChannel;
    if (internalErrorHandler != null) {
      this.internalErrorHandler = internalErrorHandler;
    } else {
      this.internalErrorHandler = new ISCInternalErrorHandler() {
      };
    }
    this.disablePhase2pSupport = disablePhase2pSupport;

    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}] and [{}]", toS, scReaderSubChannel, scEmulatorSubChannel);
    }
  }

  public SCSubChannelsJoining(final ISCReaderSubChannel scReaderSubChannel, final ISCEmulatorSubChannel scEmulatorSubChannel, final ISCInternalErrorHandler internalErrorHandler) {
    this(scReaderSubChannel, scEmulatorSubChannel, internalErrorHandler, false);
  }

  public SCSubChannelsJoining(final ISCReaderSubChannel scReaderSubChannel, final ISCEmulatorSubChannel scEmulatorSubChannel) {
    this(scReaderSubChannel, scEmulatorSubChannel, null);
  }

  public synchronized void join() throws SCReaderSubChannelsJoiningException, SCEmulatorSubChannelsJoiningException, InterruptedException {
    ATR atr;

    logger.debug("[{}] - joining", this);
    try {
      atr = scReaderSubChannel.start(this);
    } catch (ChannelException e) {
      logger.warn("[" + this + "] - can't start SC Reader Sub channel", e);
      throw new SCReaderSubChannelsJoiningException(e);
    } catch (InterruptedException e) {
      logger.warn("[" + this + "] - starting SC Reader Sub channel interrupted", e);
      throw e;
    }

    try {
      try {
        scEmulatorSubChannel.start(atr, this);
      } catch (ChannelException e) {
        logger.warn("[" + this + "] - can't start SC Emulator Sub channel, must stop started SC Reader Sub channel", e);
        throw new SCEmulatorSubChannelsJoiningException(e);
      } catch (InterruptedException e) {
        logger.warn("[" + this + "] - starting SC Emulator Sub channel interrupted, must stop started SC Reader Sub channel", e);
        throw e;
      }
    } catch (SCEmulatorSubChannelsJoiningException | InterruptedException e) {
      stopSCReaderSubChannelWithExceptionSuppress(e);
      throw e;
    }
    logger.debug("[{}] - joined", this);

    scSubChannelsJoiningThread = new SCSubChannelsJoiningThread();
    scSubChannelsJoiningThread.start();
  }

  public synchronized void disjoin() throws SCReaderSubChannelsJoiningException, SCEmulatorSubChannelsJoiningException, InterruptedException {
    SCSubChannelsJoiningThread scSubChannelsJoiningThreadLocal = scSubChannelsJoiningThread;
    scSubChannelsJoiningThread = null;
    queue.offer(new Object()); // wake up BlockingQueue
    scSubChannelsJoiningThreadLocal.join();

    logger.debug("[{}] - disjoining", this);
    try {
      try {
        scEmulatorSubChannel.stop();
      } catch (ChannelException e) {
        logger.warn("[" + this + "] - can't stop SC Emulator Sub channel, must stop SC Reader Sub channel", e);
        throw new SCEmulatorSubChannelsJoiningException(e);
      } catch (InterruptedException e) {
        logger.warn("[" + this + "] - stopping SC Emulator Sub channel interrupted, must stop SC Reader Sub channel", e);
        throw e;
      }
    } catch (SCEmulatorSubChannelsJoiningException | InterruptedException e) {
      stopSCReaderSubChannelWithExceptionSuppress(e);
      throw e;
    }

    try {
      scReaderSubChannel.stop();
    } catch (ChannelException e) {
      logger.warn("[" + this + "] - can't stop SC Reader Sub channel", e);
      throw new SCReaderSubChannelsJoiningException(e);
    } catch (InterruptedException e) {
      logger.warn("[" + this + "] - stopping SC Reader Sub channel interrupted", e);
      throw e;
    }
    logger.debug("[{}] - disjoined", this);
  }

  private void stopSCReaderSubChannelWithExceptionSuppress(final Exception be) {
    try {
      scReaderSubChannel.stop();
    } catch (ChannelException e) {
      be.addSuppressed(e);
      logger.warn("[" + this + "] - can't stop started SC Reader Sub channel (current exception will be suppress by previous)", e);
    } catch (InterruptedException e) {
      be.addSuppressed(e);
      logger.warn("[" + this + "] - stopping started SC Reader Sub channel interrupted (current exception will be suppress by previous)", e);
    }
  }

  @Override
  public void handleAPDUResponse(final APDUResponse response) {
    if (!queue.offer(response)) {
      logger.warn("[{}] - APDU response queue overflow", this);
    }
  }

  @Override
  public void handleErrorState(final SCReaderError error, final SCReaderState state) {
    logger.warn("[{}] - SC Reader Sub channel error state: {}({})", this, error, state);
    internalErrorHandler.handleSCReaderSubChannelErrorState(error, state);
  }

  @Override
  public void handleAPDUCommand(final APDUCommand command) {
    if (disablePhase2pSupport && command.getInstruction() == APDUCommand.Instruction.TERMINAL_PROFILE) {
      logger.debug("[{}] - disable Phase 2+ mode by intercept APDU command [{}] and send empty APDU response to it", this, command);
      handleAPDUResponse(new APDUResponse(new byte[0], (byte) 0x90, (byte) 0x00));
      return;
    }
    if (!queue.offer(command)) {
      logger.warn("[{}] - APDU command queue overflow", this);
    }
  }

  @Override
  public void handleErrorState(final SCEmulatorError error, final SCEmulatorState state) {
    logger.warn("[{}] - SC Emulator Sub channel error state: {}({})", this, error, state);
    internalErrorHandler.handleSCEmulatorSubChannelErrorState(error, state);
  }

  @Override
  public String toString() {
    return toS;
  }

  private class SCSubChannelsJoiningThread extends Thread {

    public SCSubChannelsJoiningThread() {
      super("JoiningThread (" + SCSubChannelsJoining.this + ")");
    }

    @Override
    public void run() {
      Thread thisThread = Thread.currentThread();

      logger.debug("[{}] - started", SCSubChannelsJoining.this);
      while (thisThread == scSubChannelsJoiningThread) {
        try {
          Object apdu = queue.take();

          if (apdu instanceof APDUCommand) {
            APDUCommand command = (APDUCommand) apdu;
            logger.debug("[{}] - send {}", SCSubChannelsJoining.this, command);
            try {
              scReaderSubChannel.writeAPDUCommand(command);
            } catch (ChannelException e) {
              logger.warn("[" + SCSubChannelsJoining.this + "] - while writing [" + command + "]", e);
              internalErrorHandler.handleSCReaderSubChannelException(e);
            }
          } else if (apdu instanceof APDUResponse) {
            APDUResponse response = (APDUResponse) apdu;
            logger.debug("[{}] - received {}", SCSubChannelsJoining.this, response);
            try {
              scEmulatorSubChannel.writeAPDUResponse(response);
            } catch (ChannelException e) {
              logger.warn("[" + SCSubChannelsJoining.this + "] - while writing [" + response + "]", e);
              internalErrorHandler.handleSCEmulatorSubChannelException(e);
            }
          }
        } catch (InterruptedException e) {
          logger.warn("[" + SCSubChannelsJoining.this + "] - was interrupted", e);
          return;
        }
      }
      logger.debug("[{}] - stopped", SCSubChannelsJoining.this);
    }

  }

}
