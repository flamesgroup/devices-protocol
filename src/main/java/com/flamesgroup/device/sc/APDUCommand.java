/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.device.helper.DataRepresentationHelper;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

public class APDUCommand implements Serializable {

  private static final long serialVersionUID = 2515562555887846016L;

  private final InstructionClass instructionClass;
  private final Instruction instruction;
  private final byte p1;
  private final byte p2;
  private final byte p3;
  private final byte[] data;

  public APDUCommand(final InstructionClass instructionClass, final Instruction instruction, final byte p1, final byte p2, final byte p3, final byte[] data) {
    Objects.requireNonNull(instruction, "instruction must not be null");
    Objects.requireNonNull(data, "data must not be null");
    this.instructionClass = instructionClass;
    this.instruction = instruction;
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    this.data = data;
  }

  public APDUCommand(final Instruction instruction, final byte p1, final byte p2, final byte p3, final byte[] data) {
    this(APDUCommand.InstructionClass.GSM_APPLICATION, instruction, p1, p2, p3, data);
  }

  public APDUCommand(final Instruction instruction, final byte p1, final byte p2, final byte p3) {
    this(instruction, p1, p2, p3, new byte[0]);
  }

  public InstructionClass getInstructionClass() {
    return instructionClass;
  }

  public Instruction getInstruction() {
    return instruction;
  }

  public byte getP1() {
    return p1;
  }

  public byte getP2() {
    return p2;
  }

  public byte getP3() {
    return p3;
  }

  public byte[] getData() {
    return data;
  }

  public int getLength() {
    return 5 + data.length;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof APDUCommand)) {
      return false;
    }
    APDUCommand apduCommand = (APDUCommand) obj;
    return instructionClass.equals(apduCommand.instructionClass) && instruction.equals(apduCommand.instruction)
        && p1 == apduCommand.p1 && p2 == apduCommand.p2 && p3 == apduCommand.p3
        && Arrays.equals(data, apduCommand.data);
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + instructionClass.hashCode();
    result = 31 * result + instruction.hashCode();
    result = 31 * result + p1;
    result = 31 * result + p2;
    result = 31 * result + p3;
    result = 31 * result + Arrays.hashCode(data);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[I:").append(instruction);
    sb.append(" p1:");
    DataRepresentationHelper.appendHexString(sb, p1);
    sb.append(" p2:");
    DataRepresentationHelper.appendHexString(sb, p2);
    sb.append(" p3:");
    DataRepresentationHelper.appendHexString(sb, p3);
    sb.append(" D:[");
    DataRepresentationHelper.appendHexArrayString(sb, data);
    sb.append("]]");
    return sb.toString();
  }

  public static APDUCommand decoding(final ByteBuffer byteBuffer) {
    InstructionClass instructionClass = InstructionClass.valueOf(byteBuffer.get());
    Instruction instruction = Instruction.valueOf(byteBuffer.get());
    byte param1 = byteBuffer.get();
    byte param2 = byteBuffer.get();
    byte param3 = byteBuffer.get();
    byte[] data = new byte[byteBuffer.remaining()];
    byteBuffer.get(data);
    return new APDUCommand(instructionClass, instruction, param1, param2, param3, data);
  }

  public static void encoding(final APDUCommand apduCommand, final ByteBuffer byteBuffer) {
    byteBuffer.put(apduCommand.getInstructionClass().getCode());
    byteBuffer.put(apduCommand.getInstruction().getCode());
    byteBuffer.put(apduCommand.getP1());
    byteBuffer.put(apduCommand.getP2());
    byteBuffer.put(apduCommand.getP3());
    byteBuffer.put(apduCommand.getData());
  }

  public enum InstructionClass {

    GSM_APPLICATION((byte) 0xA0);

    private final byte code;

    InstructionClass(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static InstructionClass valueOf(final byte code) {
      for (InstructionClass instructionClass : values()) {
        if (instructionClass.getCode() == code) {
          return instructionClass;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for InstructionClass: 0x%02X", code));
    }

  }

  public enum Instruction {

    SELECT((byte) 0xA4),
    STATUS((byte) 0xF2),
    READ_BINARY((byte) 0xB0),
    UPDATE_BINARY((byte) 0xD6),
    READ_RECORD((byte) 0xB2),
    UPDATE_RECORD((byte) 0xDC),
    SEEK((byte) 0xA2),
    INCREASE((byte) 0x32),
    VERIFY_CHV((byte) 0x20),
    CHANGE_CHV((byte) 0x24),
    DISABLE_CHV((byte) 0x26),
    ENABLE_CHV((byte) 0x28),
    UNBLOCK_CHV((byte) 0x2C),
    INVALIDATE((byte) 0x04),
    REHABILITATE((byte) 0x44),
    RUN_GSM((byte) 0x88),
    SLEEP((byte) 0xFA),
    GET_RESPONSE((byte) 0xC0),
    TERMINAL_PROFILE((byte) 0x10),
    ENVELOPE((byte) 0xC2),
    FETCH((byte) 0x12),
    TERMINAL_RESPONSE((byte) 0x14);

    private final byte code;

    Instruction(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static Instruction valueOf(final byte code) {
      int codeUnsigned = Byte.toUnsignedInt(code);
      if (codeUnsigned < instructions.length) {
        Instruction instruction = instructions[codeUnsigned];
        if (instruction != null) {
          return instruction;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for Instruction: 0x%02X", code));
    }

    private static final Instruction[] instructions;

    static {
      int maxCodeUnsigned = 0;
      for (Instruction instruction : values()) {
        int codeUnsigned = Byte.toUnsignedInt(instruction.code);
        if (codeUnsigned > maxCodeUnsigned) {
          maxCodeUnsigned = codeUnsigned;
        }
      }
      instructions = new Instruction[maxCodeUnsigned + 1];
      for (Instruction instruction : values()) {
        int codeUnsigned = Byte.toUnsignedInt(instruction.code);
        instructions[codeUnsigned] = instruction;
      }
    }

  }

}
