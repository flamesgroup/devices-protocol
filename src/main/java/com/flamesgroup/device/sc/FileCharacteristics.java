/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

public class FileCharacteristics {

  public enum Level {
    LOW,
    HIGH,
    NO
  }

  private final int mask;

  private static final Level[] levels = {Level.NO, Level.LOW, Level.HIGH};

  public FileCharacteristics(final int mask) {
    this.mask = mask;
  }

  public boolean isClockStopAllowed() {
    return (mask & 0x04) > 0;
  }

  public Level getLevel() {
    return levels[mask & 0x03];
  }

  public boolean isCHV1Enabled() {
    return (mask & 0x80) == 0;
  }

}
