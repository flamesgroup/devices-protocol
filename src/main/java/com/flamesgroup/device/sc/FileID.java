/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public enum FileID {

  MF(0x3F00, null, FileType.MF),
  DF_GSM(0x7F20, MF, FileType.DF),
  DF_TELECOM(0x7F10, MF, FileType.DF),
  DF_IS_41(0x7F22, MF, FileType.DF),
  EF_ICCID(0x2FE2, MF, FileType.EF),
  EF_ELP(0x2F05, MF, FileType.EF),
  EF_ADN(0x6F3A, DF_TELECOM, FileType.EF),
  EF_FDN(0x6F3B, DF_TELECOM, FileType.EF),
  EF_SMS(0x6F3C, DF_TELECOM, FileType.EF),
  EF_CCP(0x6F3D, DF_TELECOM, FileType.EF),
  EF_MSISDN(0x6F40, DF_TELECOM, FileType.EF),
  EF_SMSP(0x6F42, DF_TELECOM, FileType.EF),
  EF_SMSS(0x6F43, DF_TELECOM, FileType.EF),
  EF_LND(0x6F44, DF_TELECOM, FileType.EF),
  EF_SMSR(0x6F47, DF_TELECOM, FileType.EF),
  EF_SDN(0x6F49, DF_TELECOM, FileType.EF),
  EF_EXT1(0x6F4A, DF_TELECOM, FileType.EF),
  EF_EXT2(0x6F4B, DF_TELECOM, FileType.EF),
  EF_EXT3(0x6F4C, DF_TELECOM, FileType.EF),
  EF_BDN(0x6F4D, DF_TELECOM, FileType.EF),
  EF_EXT4(0x6F4E, DF_TELECOM, FileType.EF),
  DF_IRIDIUM(0x5F30, DF_GSM, FileType.DF),
  DF_GLOBST(0x5F31, DF_GSM, FileType.DF),
  DF_ICO(0x5F32, DF_GSM, FileType.DF),
  DF_ACES(0x5F33, DF_GSM, FileType.DF),
  DF_PCS1900(0x5F40, DF_GSM, FileType.DF),
  EF_LP(0x6F05, DF_GSM, FileType.EF),
  EF_IMSI(0x6F07, DF_GSM, FileType.EF),
  EF_KC(0x6F20, DF_GSM, FileType.EF),
  EF_PLMNSEL(0x6F30, DF_GSM, FileType.EF),
  EF_HPLMN(0x6F31, DF_GSM, FileType.EF),
  EF_ACMMAX(0x6F37, DF_GSM, FileType.EF),
  EF_SST(0x6F38, DF_GSM, FileType.EF),
  EF_ACM(0x6F39, DF_GSM, FileType.EF),
  EF_GID1(0x6F3E, DF_GSM, FileType.EF),
  EF_GID2(0x6F3F, DF_GSM, FileType.EF),
  EF_PUCT(0x6F41, DF_GSM, FileType.EF),
  EF_CBMI(0x6F45, DF_GSM, FileType.EF),
  EF_SPN(0x6F46, DF_GSM, FileType.EF),
  EF_CBMID(0x6F48, DF_GSM, FileType.EF),
  EF_BCCH(0x6F74, DF_GSM, FileType.EF),
  EF_ACC(0x6F78, DF_GSM, FileType.EF),
  EF_FPLMN(0x6F7B, DF_GSM, FileType.EF),
  EF_LOCI(0x6F7E, DF_GSM, FileType.EF),
  EF_AD(0x6FAD, DF_GSM, FileType.EF),
  EF_PHASE(0x6FAE, DF_GSM, FileType.EF),
  EF_VGCS(0x6FB1, DF_GSM, FileType.EF),
  EF_VGCSS(0x6FB2, DF_GSM, FileType.EF),
  EF_VBS(0x6FB3, DF_GSM, FileType.EF),
  EF_VBSS(0x6FB4, DF_GSM, FileType.EF),
  EF_EMLPP(0x6FB5, DF_GSM, FileType.EF),
  EF_AAEM(0x6FB6, DF_GSM, FileType.EF),
  EF_ECC(0x6FB7, DF_GSM, FileType.EF),
  EF_CBMIR(0x6F50, DF_GSM, FileType.EF),
  EF_NIA(0x6F51, DF_GSM, FileType.EF),
  EF_KCGPRS(0x6F52, DF_GSM, FileType.EF),
  EF_LOCIGPRS(0x6F53, DF_GSM, FileType.EF);

  private final int code;
  private final FileID parent;
  private final FileType fileType;

  private final LinkedList<FileID> children = new LinkedList<>();

  FileID(final int code, final FileID parent, final FileType fileType) {
    this.code = code;
    this.parent = parent;
    this.fileType = fileType;
    if (parent != null) {
      parent.children.add(this);
    }
  }

  public int getCode() {
    return code;
  }

  public FileType getFileType() {
    return fileType;
  }

  public FileID getParent() {
    return parent;
  }

  private static final Map<Integer, FileID> ids;

  public static FileID valueOf(final int code) {
    if (ids.containsKey(code)) {
      return ids.get(code);
    }
    throw new IllegalArgumentException(String.format("Unsupported code for FileID: 0x%04X", code));
  }

  static {
    ids = new HashMap<>();
    for (FileID e : values()) {
      ids.put(e.getCode(), e);
    }
  }

  public static void encoding(final FileID fileID, final ByteBuffer byteBuffer) {
    byteBuffer.putShort((short) fileID.getCode());
  }
}
