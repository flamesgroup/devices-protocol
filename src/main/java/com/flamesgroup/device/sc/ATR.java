/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.device.helper.DataRepresentationHelper;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Objects;

public final class ATR implements Serializable {

  private static final long serialVersionUID = 450338654654231458L;

  public static final int MAX_SIZE = 33;

  private final byte[] data;

  public ATR(final byte[] data) {
    Objects.requireNonNull(data, "ATR data must not be null");
    if (data.length > MAX_SIZE) {
      throw new IllegalArgumentException("ATR data must not be greater than " + MAX_SIZE + ", but it's " + data.length);
    }
    this.data = data;
  }

  public byte[] getData() {
    return data;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[D:[");
    DataRepresentationHelper.appendHexArrayString(sb, data);
    sb.append("]]");
    return sb.toString();
  }

  public static ATR decoding(final ByteBuffer byteBuffer) {
    byte[] data = new byte[byteBuffer.remaining()];
    byteBuffer.get(data);
    return new ATR(data);
  }

  public static void encoding(final ATR atr, final ByteBuffer byteBuffer) {
    byteBuffer.put(atr.getData());
  }

}
