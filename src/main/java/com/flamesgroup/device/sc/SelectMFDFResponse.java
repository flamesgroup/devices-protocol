/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.unit.CHVStatus;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SelectMFDFResponse {

  private static final int BASE_LENGTH = 22;

  private final int memAmount;
  private final FileID fileID;
  private final FileType fileType;

  private final FileCharacteristics fileCharacteristics;
  private final int dfNumber;
  private final int efNumber;
  private final int chvNumber;
  private final CHVStatus chv1Status;
  private final CHVStatus unblockChv1Status;
  private final CHVStatus chv2Status;
  private final CHVStatus unblockChv2Status;
  private final byte[] rfu;
  private final byte[] adminManagementData;
  private final int adminManagementDataLength;

  private int responseLength;

  public SelectMFDFResponse(final APDUResponse response) {
    ByteBuffer bb = ByteBuffer.wrap(response.getData()).order(ByteOrder.BIG_ENDIAN);
    bb.getShort(); // RFU
    this.memAmount = bb.getShort();
    this.fileID = FileID.valueOf(bb.getShort());
    this.fileType = FileType.valueOf(bb.get());
    bb.getInt(); // RFU
    bb.get(); // RFU
    bb.get(); // AdminDataLength
    fileCharacteristics = new FileCharacteristics(bb.get());
    dfNumber = bb.get();
    efNumber = bb.get();
    chvNumber = bb.get();
    bb.get(); // RFU
    chv1Status = new CHVStatus(bb.get());
    unblockChv1Status = new CHVStatus(bb.get());
    chv2Status = new CHVStatus(bb.get());
    unblockChv2Status = new CHVStatus(bb.get());
    responseLength = BASE_LENGTH;
    if (bb.remaining() > 0) {
      rfu = new byte[1];
      bb.get(rfu); // RFU
      responseLength += rfu.length;
    } else {
      rfu = null;
    }
    if (bb.remaining() > 0) {
      adminManagementData = new byte[bb.remaining()];
      bb.get(adminManagementData);
      adminManagementDataLength = adminManagementData.length;
      responseLength += adminManagementDataLength;
    } else {
      adminManagementData = null;
      adminManagementDataLength = 0;
    }
  }

  public int getResponseLength() {
    return responseLength;
  }

  public int getMemAmount() {
    return memAmount;
  }

  public FileID getFileID() {
    return fileID;
  }

  public FileType getFileType() {
    return fileType;
  }

  public FileCharacteristics getFileCharacteristics() {
    return fileCharacteristics;
  }

  public int getDfNumber() {
    return dfNumber;
  }

  public int getEfNumber() {
    return efNumber;
  }

  public int getChvNumber() {
    return chvNumber;
  }

  public CHVStatus getChv1Status() {
    return chv1Status;
  }

  public CHVStatus getUnblockChv1Status() {
    return unblockChv1Status;
  }

  public CHVStatus getChv2Status() {
    return chv2Status;
  }

  public CHVStatus getUnblockChv2Status() {
    return unblockChv2Status;
  }

  public byte[] getAdminManagementData() {
    return adminManagementData;
  }

}
