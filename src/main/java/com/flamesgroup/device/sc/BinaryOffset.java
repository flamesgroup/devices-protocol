/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

import com.flamesgroup.device.helper.DataRepresentationHelper;

public class BinaryOffset {

  private final int offset;

  public BinaryOffset(final int offset) {
    if (offset < 0 || offset > 0xFFFF) {
      throw new IllegalArgumentException("offset can't be less 0 or above 255 but it's " + offset);
    }

    this.offset = offset;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[O:");
    DataRepresentationHelper.appendHexString(sb, (short) offset);
    sb.append("]");
    return sb.toString();
  }

  public byte getLow() {
    return (byte) (offset & 0xFF);
  }

  public byte getHigh() {
    return (byte) ((offset >> 8) & 0xFF);
  }

}
