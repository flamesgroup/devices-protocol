/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

public enum FileStructure {

  TRANSPARENT(0x00),
  LINEAR_FIXED(0x01),
  CYCLIC(0x03);

  private final int code;

  FileStructure(final int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  public static FileStructure valueOf(final int code) {
    for (FileStructure fileStructure : values()) {
      if (fileStructure.getCode() == code) {
        return fileStructure;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported code for FileStructure: 0x%02X", code));
  }

}
