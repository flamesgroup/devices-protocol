/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc;

public enum FileType {

  RFU(0x00),
  MF(0x01),
  DF(0x02),
  EF(0x04);

  private final int code;

  FileType(final int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  public static FileType valueOf(final int code) {
    for (FileType fileType : values()) {
      if (fileType.getCode() == code) {
        return fileType;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported code for FileType: 0x%02X", code));
  }
}
