/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.channel.ChannelInvalidCommandDataException;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.ILoggerSubChannelHandler;
import com.flamesgroup.device.commonb.JournalRecord;
import com.flamesgroup.device.commonb.LoggerLevel;
import com.flamesgroup.device.protocol.mudp.MudpConnection;
import com.flamesgroup.device.tool.parameter.BoardIPMainParameter;
import com.flamesgroup.device.tool.parameter.CPUEntryParameter;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.util.Scanner;

public class BoardLogReader {

  private static final String READ_LOG_COMMAND = "readlog";
  private static final String[] READ_LOG_COMMAND_ALIASES = {"rl"};
  private static final String READ_LOG_ALL_COMMAND = "readlogall";
  private static final String[] READ_LOG_ALL_COMMAND_ALIASES = {"rla"};
  private static final String READ_JOURNAL_COMMAND = "readjournal";
  private static final String[] READ_JOURNAL_COMMAND_ALIASES = {"rj"};
  private static final String READ_JOURNAL_ALL_COMMAND = "readjournalall";
  private static final String[] READ_JOURNAL_ALL_COMMAND_ALIASES = {"rja"};
  private static final String ERASE_JOURNAL_COMMAND = "erasejournal";
  private static final String[] ERASE_JOURNAL_COMMAND_ALIASES = {"ej"};
  private static final String ERASE_JOURNAL_ALL_COMMAND = "erasejournalall";
  private static final String[] ERASE_JOURNAL_ALL_COMMAND_ALIASES = {"eja"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final BoardIPMainParameter mainParameter = new BoardIPMainParameter();
    final CPUEntryParameter cpuEntryParameter = new CPUEntryParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final ReadLogCommandParameters readLogCommandParameters = new ReadLogCommandParameters(mainParameter, cpuEntryParameter);
    final ReadLogAllCommandParameters readLogAllCommandParameters = new ReadLogAllCommandParameters(mainParameter);
    final ReadJournalCommandParameters readJournalCommandParameters = new ReadJournalCommandParameters(mainParameter, cpuEntryParameter);
    final ReadJournalAllCommandParameters readJournalAllCommandParameters = new ReadJournalAllCommandParameters(mainParameter);
    final EraseJournalCommandParameters eraseJournalCommandParameters = new EraseJournalCommandParameters(mainParameter, cpuEntryParameter);
    final EraseJournalAllCommandParameters eraseJournalAllCommandParameters = new EraseJournalAllCommandParameters(mainParameter);

    clp.addObject(commonParameters);
    clp.addCommand(READ_LOG_COMMAND, readLogCommandParameters, READ_LOG_COMMAND_ALIASES);
    clp.addCommand(READ_LOG_ALL_COMMAND, readLogAllCommandParameters, READ_LOG_ALL_COMMAND_ALIASES);
    clp.addCommand(READ_JOURNAL_COMMAND, readJournalCommandParameters, READ_JOURNAL_COMMAND_ALIASES);
    clp.addCommand(READ_JOURNAL_ALL_COMMAND, readJournalAllCommandParameters, READ_JOURNAL_ALL_COMMAND_ALIASES);
    clp.addCommand(ERASE_JOURNAL_COMMAND, eraseJournalCommandParameters, ERASE_JOURNAL_COMMAND_ALIASES);
    clp.addCommand(ERASE_JOURNAL_ALL_COMMAND, eraseJournalAllCommandParameters, ERASE_JOURNAL_ALL_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else {
        IDevice device = DeviceUtil.createEthDevice(new InetSocketAddress(mainParameter.getBoardIP(), MudpConnection.MUDP_DEFAULT_PORT));
        device.attach();
        try {
          if (READ_LOG_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSelectedCpuEntryNumbers(cpuEntryParameter.getNumber());
            readLog(device, cpuEntryNumbers);
          } else if (READ_LOG_ALL_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createAllCpuEntryNumbers(device.getCPUEntryCount());
            readLog(device, cpuEntryNumbers);
          } else if (READ_JOURNAL_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSelectedCpuEntryNumbers(cpuEntryParameter.getNumber());
            readJournal(device, cpuEntryNumbers);
          } else if (READ_JOURNAL_ALL_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createAllCpuEntryNumbers(device.getCPUEntryCount());
            readJournal(device, cpuEntryNumbers);
          } else if (ERASE_JOURNAL_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSelectedCpuEntryNumbers(cpuEntryParameter.getNumber());
            eraseJournal(device, cpuEntryNumbers);
          } else if (ERASE_JOURNAL_ALL_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createAllCpuEntryNumbers(device.getCPUEntryCount());
            eraseJournal(device, cpuEntryNumbers);
          } else {
            throw new AssertionError();
          }
        } finally {
          device.detach();
        }
      }
    }
  }

  private static void readLog(final IDevice device, final int[] cpuEntryNumbers) throws Exception {
    for (int cpuEntryNumber : cpuEntryNumbers) {
      ILoggerChannel loggerChannel = device.getCPUEntries().get(cpuEntryNumber).getLoggerChannel();
      loggerChannel.enslave();
      loggerChannel.getLoggerSubChannel().start(LoggerLevel.LOGGER_DEBUG, new LoggerSubChannelHandler(cpuEntryNumber));
    }

    System.out.println("Press enter to stop logging and exit...");
    Scanner keyboard = new Scanner(System.in);
    keyboard.nextLine();

    for (int cpuEntryNumber : cpuEntryNumbers) {
      ILoggerChannel loggerChannel = device.getCPUEntries().get(cpuEntryNumber).getLoggerChannel();
      loggerChannel.getLoggerSubChannel().stop();
      loggerChannel.free();
    }
  }

  private static void readJournal(final IDevice device, final int[] cpuEntryNumbers) throws Exception {
    for (int cpuEntryNumber : cpuEntryNumbers) {
      ICPUEntry cpuEntry = device.getCPUEntries().get(cpuEntryNumber);
      System.out.println(cpuEntry);
      ILoggerChannel loggerChannel = cpuEntry.getLoggerChannel();
      loggerChannel.enslave();
      short recordNumber = 0;
      try {
        while (true) {
          JournalRecord record = loggerChannel.readJournalRecord(recordNumber++);
          System.out.println(record);
        }
      } catch (ChannelInvalidCommandDataException e) {
      }
      loggerChannel.free();
    }
  }

  private static void eraseJournal(final IDevice device, final int[] cpuEntryNumbers) throws Exception {
    for (int cpuEntryNumber : cpuEntryNumbers) {
      ILoggerChannel loggerChannel = device.getCPUEntries().get(cpuEntryNumber).getLoggerChannel();
      loggerChannel.enslave();
      loggerChannel.eraseJournal();
      loggerChannel.free();
    }
  }

  private static final class LoggerSubChannelHandler implements ILoggerSubChannelHandler {

    private final int cpuN;

    public LoggerSubChannelHandler(final int cpuN) {
      this.cpuN = cpuN;
    }

    @Override
    public void handleLoggerData(final String loggerData) {
      System.out.print("CPU ");
      System.out.print(cpuN);
      System.out.print(" >>");
      System.out.print(loggerData);
      System.out.flush();
    }
  }

  private static int[] createSelectedCpuEntryNumbers(final int cpuEntryNumber) {
    return new int[] {cpuEntryNumber};
  }

  private static int[] createAllCpuEntryNumbers(final int cpuEntryCount) {
    int[] cpuEntryNumbers = new int[cpuEntryCount];
    for (int i = 0; i < cpuEntryNumbers.length; i++) {
      cpuEntryNumbers[i] = i;
    }
    return cpuEntryNumbers;
  }

  @Parameters(commandDescription = "Read log on selected CPU entry")
  private static class ReadLogCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CPUEntryParameter cpuEntryParameter;

    public ReadLogCommandParameters(final BoardIPMainParameter mainParameter, final CPUEntryParameter cpuEntryParameter) {
      this.mainParameter = mainParameter;
      this.cpuEntryParameter = cpuEntryParameter;
    }
  }

  @Parameters(commandDescription = "Read log on all CPU entries")
  private static class ReadLogAllCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    public ReadLogAllCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Read journal on selected CPU entry")
  private static class ReadJournalCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CPUEntryParameter cpuEntryParameter;

    public ReadJournalCommandParameters(final BoardIPMainParameter mainParameter, final CPUEntryParameter cpuEntryParameter) {
      this.mainParameter = mainParameter;
      this.cpuEntryParameter = cpuEntryParameter;
    }
  }

  @Parameters(commandDescription = "Read journal on all CPU entries")
  private static class ReadJournalAllCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    public ReadJournalAllCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Erase journal on selected CPU entry")
  private static class EraseJournalCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CPUEntryParameter cpuEntryParameter;

    public EraseJournalCommandParameters(final BoardIPMainParameter mainParameter, final CPUEntryParameter cpuEntryParameter) {
      this.mainParameter = mainParameter;
      this.cpuEntryParameter = cpuEntryParameter;
    }
  }

  @Parameters(commandDescription = "Erase journal on all CPU entries")
  private static class EraseJournalAllCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    public EraseJournalAllCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

}
