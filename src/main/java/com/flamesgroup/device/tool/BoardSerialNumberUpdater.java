/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import static com.flamesgroup.device.helper.DataRepresentationHelper.toByteArray;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.protocol.mudp.MudpConnection;
import com.flamesgroup.device.tool.parameter.BoardIPMainParameter;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;

public class BoardSerialNumberUpdater {

  private static final String UPDATE_NUMBER_COMMAND = "update";
  private static final String[] UPDATE_NUMBER_COMMAND_ALIASES = {"u"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final BoardIPMainParameter mainParameter = new BoardIPMainParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final UpdateCommandParameters updateCommandParameters = new UpdateCommandParameters(mainParameter);

    clp.addObject(commonParameters);
    clp.addCommand(UPDATE_NUMBER_COMMAND, updateCommandParameters, UPDATE_NUMBER_COMMAND_ALIASES);

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else {
        IDevice device = DeviceUtil.createEthDevice(new InetSocketAddress(mainParameter.getBoardIP(), MudpConnection.MUDP_DEFAULT_PORT));
        device.attach();
        try {
          if (UPDATE_NUMBER_COMMAND.equals(clp.getParsedCommand())) {
            byte[] masterKey = updateCommandParameters.getMasterKey();
            byte[] slaveKey = updateCommandParameters.getSlaveKey();

            int cpuCount = device.getCPUEntries().size();
            if (cpuCount > 1) {
              if (slaveKey == null) {
                throw new ParameterException("Must be two ciphered keys for master and slave cpu");
              } else {
                for (int i = 1; i < cpuCount; i++) {
                  try {
                    updateSerialNumber(device.getCPUEntries().get(i).getServiceChannel(), slaveKey);
                    System.out.println(String.format("Successful change serial number for slave CPU %d", i));
                  } catch (ChannelException e) {
                    throw new ParameterException(String.format("Can't change FW serial number for slave CPU %d", i), e);
                  }
                }
              }
            } else if (slaveKey != null) {
              throw new ParameterException("Must be only master ciphered key without slave ciphered key");
            }

            try {
              updateSerialNumber(device.getCPUEntries().get(0).getServiceChannel(), masterKey);
              System.out.println("Successful change serial number for master CPU");
            } catch (ChannelException e) {
              throw new ParameterException("Can't change FW serial number for master CPU", e);
            }
          } else {
            throw new AssertionError();
          }
        } finally {
          device.detach();
        }
      }
    }
  }

  private static void updateSerialNumber(final IServiceChannel serviceChannel, final byte[] cipheredKey) throws ChannelException, InterruptedException {
    serviceChannel.enslave();
    try {
      serviceChannel.changeFWSerialNumber(cipheredKey);
      serviceChannel.reset(200);
    } finally {
      serviceChannel.free();
    }
  }

  @Parameters(commandDescription = "Update firmware serial number")
  private static class UpdateCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @Parameter(names = "--masterKey", description = "Firmware serial number key string for LAN cpu (number 0)", required = true)
    private String masterKey;

    @Parameter(names = "--slaveKey", description = "Firmware serial number key string for all other GSM or SIM cpu numbers (if device contains only master cpu, this key can be missed)")
    private String slaveKey;

    public UpdateCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }

    public byte[] getMasterKey() {
      if (masterKey == null) {
        return null;
      } else {
        return toByteArray(masterKey);
      }
    }

    public byte[] getSlaveKey() {
      if (slaveKey == null) {
        return null;
      } else {
        return toByteArray(slaveKey);
      }
    }
  }

}
