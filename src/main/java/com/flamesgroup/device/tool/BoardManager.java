/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpStream;
import com.flamesgroup.device.protocol.cudp.ICudpConnection;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.tool.parameter.CudpCommonParameters;
import com.flamesgroup.device.tool.parameter.CudpDefault;
import com.flamesgroup.device.tool.parameter.DeviceUIDMainParameter;
import com.flamesgroup.device.tool.parameter.InetAddressConverter;

import java.lang.invoke.MethodHandles;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.UUID;

public class BoardManager {

  private static final String REBOOT_COMMAND = "reboot";
  private static final String[] REBOOT_COMMAND_ALIASES = {"r"};
  private static final String GET_IP_CONFIG_COMMAND = "getipconfig";
  private static final String[] GET_IP_CONFIG_COMMAND_ALIASES = {"g"};
  private static final String SET_IP_CONFIG_COMMAND = "setipconfig";
  private static final String[] SET_IP_CONFIG_COMMAND_ALIASES = {"s"};
  private static final String LIST_COMMAND = "list";
  private static final String[] LIST_COMMAND_ALIASES = {"l"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final DeviceUIDMainParameter mainParameter = new DeviceUIDMainParameter();

    final CudpCommonParameters commonParameters = new CudpCommonParameters();
    final RebootCommandParameters rebootCommandParameters = new RebootCommandParameters(mainParameter);
    final GetIPConfigCommandParameters getIPConfigCommandParameters = new GetIPConfigCommandParameters(mainParameter);
    final SetIPConfigCommandParameters setIPConfigCommandParameters = new SetIPConfigCommandParameters(mainParameter);
    final ListCommandParameters listCommandParameters = new ListCommandParameters();

    clp.addObject(commonParameters);
    clp.addCommand(REBOOT_COMMAND, rebootCommandParameters, REBOOT_COMMAND_ALIASES);
    clp.addCommand(GET_IP_CONFIG_COMMAND, getIPConfigCommandParameters, GET_IP_CONFIG_COMMAND_ALIASES);
    clp.addCommand(SET_IP_CONFIG_COMMAND, setIPConfigCommandParameters, SET_IP_CONFIG_COMMAND_ALIASES);
    clp.addCommand(LIST_COMMAND, listCommandParameters, LIST_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else {
        ICudpConnection cudpConnection = new CudpConnection(new CudpStream(), commonParameters.getResponseTimeout());
        cudpConnection.connect(new InetSocketAddress(commonParameters.getRemoteAddress(), CudpConnection.CUDP_DEFAULT_PORT));
        try {
          if (REBOOT_COMMAND.equals(clp.getParsedCommand())) {
            rebootDevice(cudpConnection, mainParameter.getDeviceUID(), rebootCommandParameters.getTimeout());
          } else if (GET_IP_CONFIG_COMMAND.equals(clp.getParsedCommand())) {
            getDeviceIPInfo(cudpConnection, mainParameter.getDeviceUID());
          } else if (SET_IP_CONFIG_COMMAND.equals(clp.getParsedCommand())) {
            setDeviceIPInfo(cudpConnection, mainParameter.getDeviceUID(), setIPConfigCommandParameters.getIp(), setIPConfigCommandParameters.getMask(),
                setIPConfigCommandParameters.getGateway(), setIPConfigCommandParameters.getMaster());
          } else if (LIST_COMMAND.equals(clp.getParsedCommand())) {
            listDevices(cudpConnection, commonParameters.getResponseTimeout());
          } else {
            throw new AssertionError();
          }
        } finally {
          cudpConnection.disconnect();
        }
      }
    }
  }

  private static void listDevices(final ICudpConnection cudpStack, final long timeout) throws CudpException, InterruptedException {
    DeviceUID[] deviceUIDs = cudpStack.getDeviceUIDs(timeout).toArray(new DeviceUID[0]);
    Arrays.sort(deviceUIDs);
    System.out.println("Found " + deviceUIDs.length + " devices:");
    for (DeviceUID deviceUID : deviceUIDs) {
      getDeviceIPInfo(cudpStack, deviceUID);
    }
  }

  private static void rebootDevice(final ICudpConnection cudpStack, final DeviceUID deviceUID, final int timeout) throws CudpException, InterruptedException {
    cudpStack.reset(deviceUID, timeout);
    System.out.println(deviceUID + " rebooted");
  }

  private static void getDeviceIPInfo(final ICudpConnection cudpStack, final DeviceUID deviceUID) throws CudpException, InterruptedException {
    IPConfig config = cudpStack.getIPConfig(deviceUID);
    System.out.println(deviceUID + ": [IP - " + config.getIP() + "] [mask - " + config.getMask() + "] [gateway - " + config.getGateway()
        + "] [master IP - " + config.getMasterIP() + "]");
  }

  private static void setDeviceIPInfo(final ICudpConnection cudpStack, final DeviceUID deviceUID, final InetAddress ip, final InetAddress mask, final InetAddress gateway, final InetAddress master)
      throws CudpException, InterruptedException {

    IPConfig config = new IPConfig((Inet4Address) ip, (Inet4Address) mask, (Inet4Address) gateway, (Inet4Address) master, UUID.randomUUID());

    cudpStack.setIPConfig(deviceUID, config);
    System.out.println(deviceUID + " got [IP - " + config.getIP() + "] [mask - " + config.getMask() + "] [gateway - " + config.getGateway()
        + "] [master IP - " + config.getMasterIP() + "]");
  }

  @Parameters(commandDescription = "Reboot device")
  private static class RebootCommandParameters {
    @ParametersDelegate
    private final DeviceUIDMainParameter mainParameter;

    @Parameter(names = "--timeout", description = "Timeout before reboot")
    private int timeout = 100;

    public RebootCommandParameters(final DeviceUIDMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }

    public int getTimeout() {
      return timeout;
    }
  }

  @Parameters(commandDescription = "Get device IP config")
  private static class GetIPConfigCommandParameters {
    @ParametersDelegate
    private final DeviceUIDMainParameter mainParameter;

    public GetIPConfigCommandParameters(final DeviceUIDMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Set device IP config")
  private static class SetIPConfigCommandParameters {
    @ParametersDelegate
    private final DeviceUIDMainParameter mainParameter;

    @Parameter(names = "--ip", description = "Devices IP", converter = InetAddressConverter.class)
    private InetAddress ip = CudpDefault.ADDRESS;

    @Parameter(names = "--mask", description = "Devices network mask", converter = InetAddressConverter.class)
    private InetAddress mask = CudpDefault.MASK;

    @Parameter(names = "--gateway", description = "Default gateway", converter = InetAddressConverter.class)
    private InetAddress gateway = CudpDefault.ADDRESS;

    @Parameter(names = "--master", description = "Devices master IP", converter = InetAddressConverter.class)
    private InetAddress master = CudpDefault.ADDRESS;

    public SetIPConfigCommandParameters(final DeviceUIDMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }

    public InetAddress getIp() {
      return ip;
    }

    public InetAddress getMask() {
      return mask;
    }

    public InetAddress getGateway() {
      return gateway;
    }

    public InetAddress getMaster() {
      return master;
    }
  }

  @Parameters(commandDescription = "List all devices in network segment")
  private static class ListCommandParameters {
  }

}
