/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.gsmb.IRawATSubChannel;
import com.flamesgroup.device.gsmb.IRawATSubChannelHandler;
import com.flamesgroup.device.gsmb.Telit;
import com.flamesgroup.device.gsmb.atengine.ATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMRExec;
import com.flamesgroup.device.gsmb.atengine.at.ATpsGMRExecResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATadWExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsIPRWrite;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsIPR;
import com.flamesgroup.device.tool.parameter.GSMEntryParameter;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BaseTelitFirmwareUpdater {

  protected static final String READ_COMMAND = "read";
  protected static final String[] READ_COMMAND_ALIASES = {"r"};
  protected static final String READ_ALL_COMMAND = "readall";
  protected static final String[] READ_ALL_COMMAND_ALIASES = {"ra"};
  protected static final String UPDATE_COMMAND = "update";
  protected static final String[] UPDATE_COMMAND_ALIASES = {"u"};
  protected static final String UPDATE_ALL_COMMAND = "updateall";
  protected static final String[] UPDATE_ALL_COMMAND_ALIASES = {"ua"};
  protected static final String SHOW_COMMAND = "show";
  protected static final String[] SHOW_COMMAND_ALIASES = {"s"};

  protected static int[] createSelectedGsmEntryNumbers(final int gsmEntryNumber) {
    return new int[] {gsmEntryNumber};
  }

  protected static int[] createAllGsmEntryNumbers(final int gsmEntryCount) {
    int[] gsmEntryNumbers = new int[gsmEntryCount];
    for (int i = 0; i < gsmEntryNumbers.length; i++) {
      gsmEntryNumbers[i] = i;
    }
    return gsmEntryNumbers;
  }

  protected static void readTelitModuleFirmwareRevisionIdentification(final IGSMDevice gsmDevice, final int[] gsmEntryNumbers, final boolean verbose) throws Exception {
    ExecutorService executorService = Executors.newCachedThreadPool();
    List<Tuple<Integer, Future<String>>> tupleFutures = new LinkedList<>();
    for (int gsmEntryNumber : gsmEntryNumbers) {
      Future<String> future = executorService.submit(new TelitModuleFirmwareRevisionIdentificationReader(gsmDevice.getGSMEntries().get(gsmEntryNumber).getGSMChannel()));
      tupleFutures.add(Tuple.of(gsmEntryNumber, future));
    }
    executorService.shutdown();

    final ProgressStringFormatter progressStringFormatter = new ProgressStringFormatter();
    while (!executorService.awaitTermination(100, TimeUnit.MILLISECONDS)) {
      System.out.print("Reading " + progressStringFormatter.progress() + "\r");
    }
    System.out.println("Reading finish");

    for (Tuple<Integer, Future<String>> tupleFuture : tupleFutures) {
      try {
        String revisionIdentification = tupleFuture.getSecond().get();
        System.out.println("Firmware revision identification on channel #" + tupleFuture.getFirst() + " is: " + revisionIdentification);
      } catch (ExecutionException e) {
        System.out.println("Firmware revision identification on channel #" + tupleFuture.getFirst() + " read ERROR: " + e.getCause().getMessage());
        if (verbose) {
          e.getCause().printStackTrace(System.err);
        }
      }
    }
  }

  protected static void updateTelitModuleFirmware(final IGSMDevice gsmDevice, final int[] gsmEntryNumbers, final Path telitFirmwarePath, final boolean skipInit, final boolean verbose)
      throws Exception {
    final TelitFirmware telitFirmware = TelitFirmware.read(telitFirmwarePath);
    ExecutorService executorService = Executors.newCachedThreadPool();
    List<Tuple<Integer, Future<String>>> tupleFutures = new LinkedList<>();
    for (int gsmEntryNumber : gsmEntryNumbers) {
      Future<String> future = executorService.submit(new TelitModuleFirmwareUpdater(gsmDevice.getGSMEntries().get(gsmEntryNumber).getGSMChannel(), telitFirmware.getBlocks(), skipInit));
      tupleFutures.add(Tuple.of(gsmEntryNumber, future));
    }
    executorService.shutdown();

    System.out.println("Note: Updating takes 10-15 minutes");
    final ProgressStringFormatter progressStringFormatter = new ProgressStringFormatter();
    while (!executorService.awaitTermination(100, TimeUnit.MILLISECONDS)) {
      System.out.print("Updating " + progressStringFormatter.progress() + "\r");
    }
    System.out.println("Updating finish");

    for (Tuple<Integer, Future<String>> tupleFuture : tupleFutures) {
      try {
        String updaterResult = tupleFuture.getSecond().get();
        System.out.println("Update firmware on channel #" + tupleFuture.getFirst() + " complete: " + updaterResult);
      } catch (ExecutionException e) {
        System.out.println("Update firmware on channel #" + tupleFuture.getFirst() + " complete: ERROR: " + e.getCause().getMessage());
        if (verbose) {
          e.getCause().printStackTrace(System.err);
        }
      }
    }
  }

  protected static void showTelitFirmware(final Path telitFirmwarePath) throws Exception {
    final TelitFirmware telitFirmware = TelitFirmware.read(telitFirmwarePath);
    System.out.println("Software Version: " + telitFirmware.getSoftwareVersion());
    System.out.println("Product Name: " + telitFirmware.getProductName());
    System.out.println("Memory Type: " + telitFirmware.getMemoryType());
    System.out.println("Stream Version: " + telitFirmware.getStreamVersion());
    System.out.println("Blocks count: " + telitFirmware.getBlocks().size());
  }

  protected static class TelitModuleFirmwareRevisionIdentificationReader implements Callable<String> {

    private final IGSMChannel gsmChannel;

    public TelitModuleFirmwareRevisionIdentificationReader(final IGSMChannel gsmChannel) {
      this.gsmChannel = gsmChannel;
    }

    @Override
    public String call() throws Exception {
      try {
        gsmChannel.enslave();
        IATSubChannel atSubChannel = gsmChannel.getATSubChannel();

        ATEngine atEngine = new ATEngine(atSubChannel);
        atEngine.start(Collections.emptyList());

        TimeUnit.MILLISECONDS.sleep(Telit.GL865_TURN_ON_DELAY);

        ATpsGMRExecResult result = atEngine.execute(new ATpsGMRExec());
        String fwRevision = result.getRevision();
        atEngine.stop();

        return fwRevision;
      } finally {
        gsmChannel.free();
      }
    }

  }

  protected static class TelitModuleFirmwareUpdater implements Callable<String> {

    private final IGSMChannel gsmChannel;
    private final List<byte[]> firmwareBlocks;
    private final boolean skipInit;

    private static final byte BOOT_CHAR = 0x55;

    private static final int MAX_RETRIES_COUNT = 10; // magic empirical constant

    private static final int DELAY_FIRST_BOOT_CHAR_MS = 100; // magic empirical constant
    private static final int TIMEOUT_BOOT_MS = 100;
    private static final int TIMEOUT_FIRMWARE_MS = 120000;

    private final ReentrantLock executeLock = new ReentrantLock();
    private final Condition executeCondition = executeLock.newCondition();

    private final ByteBuffer byteBufferToReceive = ByteBuffer.allocate(260).order(ByteOrder.LITTLE_ENDIAN);

    private enum Response {
      ACK_BOOT_STRING, ACK, NACK, TIMEOUT
    }

    private Response response;
    private TelitFirmwareUpdaterException responseException;

    public TelitModuleFirmwareUpdater(final IGSMChannel gsmChannel, final List<byte[]> firmwareBlocks, final boolean skipInit) {
      Objects.requireNonNull(gsmChannel, "gsmChannel mustn't be null");
      Objects.requireNonNull(firmwareBlocks, "firmwareBlocks mustn't be null");
      this.gsmChannel = gsmChannel;
      this.firmwareBlocks = firmwareBlocks;
      this.skipInit = skipInit;
    }

    @Override
    public String call() throws Exception {
      gsmChannel.enslave();
      try {
        IRawATSubChannel rawATSubChannel = gsmChannel.getRawATSubChannel();

        rawATSubChannel.start(new IRawATSubChannelHandler() {
          @Override
          public void handleRawATData(final ByteBuffer atDataBuffer) {
            byteBufferToReceive.put(atDataBuffer);
            byteBufferToReceive.flip();
            do {
              byteBufferToReceive.mark();
              try {
                byte data = byteBufferToReceive.get();
                if ((data >= (byte) 0xA0 && data <= (byte) 0xA9) || data == (byte) 0x1) { // AckChar
                  handleResponse(Response.ACK);
                } else if (data == (byte) 0xB5) { // NackChar
                  handleResponse(Response.NACK);
                } else if (data == (byte) 0x92) { // AckBootString
                  if (byteBufferToReceive.get() != (byte) 0x00) {
                    break;
                  }
                  if (byteBufferToReceive.get() != (byte) 0x00) {
                    break;
                  }

                  handleResponse(Response.ACK_BOOT_STRING);
                }
              } catch (BufferUnderflowException e) {
                byteBufferToReceive.reset();
                break;
              }
            } while (true);
            byteBufferToReceive.compact();
          }
        });
        try {
          // Delay before send first Boot Char to allow module correctly turn on
          Thread.sleep(DELAY_FIRST_BOOT_CHAR_MS);
          // Boot Mode Phase
          int retries_counter = MAX_RETRIES_COUNT;
          while (retries_counter-- > 0) {
            try {
              if (execute(rawATSubChannel, new byte[] {BOOT_CHAR}, TIMEOUT_BOOT_MS) == Response.ACK_BOOT_STRING) {
                break;
              }
            } catch (TelitFirmwareUpdaterException e) {
              // At once after turned on module some rubbish can be released by one throw AT channel,
              // it's a protection for TelitFirmwareUpdaterException (response without request)
            }
          }
          if (retries_counter <= 0) {
            throw new TelitModuleFirmwareUpdaterException("Boot Mode Phase error: passed [" + MAX_RETRIES_COUNT + "] iterations without success");
          }

          // Downloading Phase
          for (int i = 0; i < firmwareBlocks.size(); i++) {
            Response r = execute(rawATSubChannel, firmwareBlocks.get(i), TIMEOUT_FIRMWARE_MS);
            if (r != Response.ACK) {
              throw new TelitModuleFirmwareUpdaterException("Downloading Phase error: iteration [" + i + "] got response [" + r + "]");
            }
          }
        } finally {
          rawATSubChannel.stop();
        }

        if (!skipInit) {
          try {
            IATSubChannel atSubChannel = gsmChannel.getATSubChannel();

            ATEngine atEngine = new ATEngine(atSubChannel);
            atEngine.start(Collections.emptyList());

            TimeUnit.MILLISECONDS.sleep(Telit.GL865_TURN_ON_DELAY);

            atEngine.execute(new ATpsIPRWrite(PpsIPR.Rate.RATE_115200));
            atEngine.execute(new ATadWExec());

            atEngine.stop();
          } catch (InterruptedException | ChannelException e) {
            throw new TelitModuleFirmwareUpdaterException("Can't perform follow-up initialization on GSM channel", e);
          }
        }
      } finally {
        gsmChannel.free();
      }

      return "SUCCESSFUL";
    }

    private Response execute(final IRawATSubChannel rawATSubChannel, final byte[] firmwareBlock, final int timeout) throws Exception {
      ByteBuffer firmwareBlockBuffer = ByteBuffer.wrap(firmwareBlock);
      executeLock.lock();
      try {
        if (responseException != null) {
          throw responseException;
        }

        rawATSubChannel.writeRawATData(firmwareBlockBuffer);
        if (executeCondition.await(timeout, TimeUnit.MILLISECONDS)) {
          return response;
        } else {
          return Response.TIMEOUT;
        }
      } finally {
        executeLock.unlock();
      }
    }

    private void handleResponse(final Response response) {
      executeLock.lock();
      try {
        if (executeLock.hasWaiters(executeCondition)) {
          this.response = response;
          executeCondition.signal();
        } else {
          responseException = new TelitModuleFirmwareUpdaterException("Receive [" + response + "] response without request");
        }
      } finally {
        executeLock.unlock();
      }
    }

  }

  protected abstract static class TelitFirmwareUpdaterException extends Exception {
    private static final long serialVersionUID = 50279483546751222L;

    public TelitFirmwareUpdaterException(final String message) {
      super(message);
    }

    public TelitFirmwareUpdaterException(final String message, final Throwable cause) {
      super(message, cause);
    }
  }

  protected static class TelitModuleFirmwareUpdaterException extends TelitFirmwareUpdaterException {
    private static final long serialVersionUID = 4377936706033511670L;

    public TelitModuleFirmwareUpdaterException(final String message) {
      super(message);
    }

    public TelitModuleFirmwareUpdaterException(final String message, final Throwable cause) {
      super(message, cause);
    }
  }

  protected final static class Tuple<A, B> {
    public static <A, B> Tuple<A, B> of(final A a, final B b) {
      return new Tuple<>(a, b);
    }

    private final A a;
    private final B b;

    private Tuple(final A a, final B b) {
      this.a = a;
      this.b = b;
    }

    public A getFirst() {
      return a;
    }

    public B getSecond() {
      return b;
    }

  }

  @Parameters(commandDescription = "Read Telit module firmware revision identification on selected GSM entry")
  protected static class BaseReadCommandParameters {
    @ParametersDelegate
    private final GSMEntryParameter gsmEntryParameter;

    public BaseReadCommandParameters(final GSMEntryParameter gsmEntryParameter) {
      this.gsmEntryParameter = gsmEntryParameter;
    }
  }

  @Parameters(commandDescription = "Read Telit module firmware revision identification on all GSM entries")
  protected static class BaseReadAllCommandParameters {
    public BaseReadAllCommandParameters() {
    }
  }

  @Parameters(commandDescription = "Update Telit module firmware on selected GSM entry")
  protected static class BaseUpdateCommandParameters {
    @ParametersDelegate
    private final GSMEntryParameter gsmEntryParameter;

    @ParametersDelegate
    private final TelitFirmwarePathParameter pathParameter;

    @ParametersDelegate
    private final SkipInitParameter skipInitParameter;

    public BaseUpdateCommandParameters(final GSMEntryParameter gsmEntryParameter, final TelitFirmwarePathParameter pathParameter,
        final SkipInitParameter skipInitParameter) {
      this.gsmEntryParameter = gsmEntryParameter;
      this.pathParameter = pathParameter;
      this.skipInitParameter = skipInitParameter;
    }

  }

  @Parameters(commandDescription = "Update Telit module firmware on all GSM entries")
  protected static class BaseUpdateAllCommandParameters {
    @ParametersDelegate
    private final TelitFirmwarePathParameter pathParameter;

    @ParametersDelegate
    private final SkipInitParameter skipInitParameter;

    public BaseUpdateAllCommandParameters(final TelitFirmwarePathParameter pathParameter, final SkipInitParameter skipInitParameter) {
      this.pathParameter = pathParameter;
      this.skipInitParameter = skipInitParameter;
    }
  }

  @Parameters(commandDescription = "Show Telit firmware information")
  protected static class BaseShowCommandParameters {
    @ParametersDelegate
    private final TelitFirmwarePathParameter pathParameter;

    public BaseShowCommandParameters(final TelitFirmwarePathParameter pathParameter) {
      this.pathParameter = pathParameter;
    }
  }

  protected final static class TelitFirmwarePathParameter {
    @Parameter(names = "--path", description = "Path to Telit firmware", required = true)
    private Path path;

    public Path getPath() {
      return path;
    }
  }

  protected final static class SkipInitParameter {
    @Parameter(names = "--skipInit", description = "Skip GSM module follow-up initialization")
    private boolean skipInit = false;

    public boolean isSkipInit() {
      return skipInit;
    }
  }

}
