/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.ServiceInfo;
import com.flamesgroup.device.helper.ServiceInfoEncodingHelper;
import com.flamesgroup.device.protocol.mudp.MudpConnection;
import com.flamesgroup.device.tool.parameter.BoardIPMainParameter;
import com.flamesgroup.device.tool.parameter.CPUEntryParameter;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class BoardFirmwareUpdater {

  private static final String READ_COMMAND = "read";
  private static final String[] READ_COMMAND_ALIASES = {"r"};
  private static final String READ_MASTER_COMMAND = "readmaster";
  private static final String[] READ_MASTER_COMMAND_ALIASES = {"rm"};
  private static final String READ_SLAVE_COMMAND = "readslave";
  private static final String[] READ_SLAVE_COMMAND_ALIASES = {"rs"};
  private static final String READ_ALL_COMMAND = "readall";
  private static final String[] READ_ALL_COMMAND_ALIASES = {"ra"};
  private static final String UPDATE_COMMAND = "update";
  private static final String[] UPDATE_COMMAND_ALIASES = {"u"};
  private static final String UPDATE_MASTER_COMMAND = "updatemaster";
  private static final String[] UPDATE_MASTER_COMMAND_ALIASES = {"um"};
  private static final String UPDATE_SLAVE_COMMAND = "updateslave";
  private static final String[] UPDATE_SLAVE_COMMAND_ALIASES = {"us"};
  private static final String SHOW_COMMAND = "show";
  private static final String[] SHOW_COMMAND_ALIASES = {"s"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final BoardIPMainParameter mainParameter = new BoardIPMainParameter();
    final CPUEntryParameter cpuEntryParameter = new CPUEntryParameter();
    final CipheredFirmwarePathParameter pathParameter = new CipheredFirmwarePathParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final ReadCommandParameters readCommandParameters = new ReadCommandParameters(mainParameter, cpuEntryParameter);
    final ReadMasterCommandParameters readMasterCommandParameters = new ReadMasterCommandParameters(mainParameter);
    final ReadSlaveCommandParameters readSlaveCommandParameters = new ReadSlaveCommandParameters(mainParameter);
    final ReadAllCommandParameters readAllCommandParameters = new ReadAllCommandParameters(mainParameter);
    final UpdateCommandParameters updateCommandParameters = new UpdateCommandParameters(mainParameter, cpuEntryParameter, pathParameter);
    final UpdateMasterCommandParameters updateMasterCommandParameters = new UpdateMasterCommandParameters(mainParameter, pathParameter);
    final UpdateSlaveCommandParameters updateSlaveCommandParameters = new UpdateSlaveCommandParameters(mainParameter, pathParameter);
    final ShowCommandParameters showCommandParameters = new ShowCommandParameters(pathParameter);

    clp.addObject(commonParameters);
    clp.addCommand(READ_COMMAND, readCommandParameters, READ_COMMAND_ALIASES);
    clp.addCommand(READ_MASTER_COMMAND, readMasterCommandParameters, READ_MASTER_COMMAND_ALIASES);
    clp.addCommand(READ_SLAVE_COMMAND, readSlaveCommandParameters, READ_SLAVE_COMMAND_ALIASES);
    clp.addCommand(READ_ALL_COMMAND, readAllCommandParameters, READ_ALL_COMMAND_ALIASES);
    clp.addCommand(UPDATE_COMMAND, updateCommandParameters, UPDATE_COMMAND_ALIASES);
    clp.addCommand(UPDATE_MASTER_COMMAND, updateMasterCommandParameters, UPDATE_MASTER_COMMAND_ALIASES);
    clp.addCommand(UPDATE_SLAVE_COMMAND, updateSlaveCommandParameters, UPDATE_SLAVE_COMMAND_ALIASES);
    clp.addCommand(SHOW_COMMAND, showCommandParameters, SHOW_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (SHOW_COMMAND.equals(clp.getParsedCommand())) {
        showCipheredFirmwareInformation(pathParameter.getPath());
      } else {
        IDevice device = DeviceUtil.createEthDevice(new InetSocketAddress(mainParameter.getBoardIP(), MudpConnection.MUDP_DEFAULT_PORT));
        device.attach();
        try {
          if (READ_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSelectedCpuEntryNumbers(cpuEntryParameter.getNumber());
            readServiceInfo(device, cpuEntryNumbers);
          } else if (READ_MASTER_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createMasterCpuEntryNumbers();
            readServiceInfo(device, cpuEntryNumbers);
          } else if (READ_SLAVE_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSlaveCpuEntryNumbers(device.getCPUEntryCount());
            readServiceInfo(device, cpuEntryNumbers);
          } else if (READ_ALL_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createAllCpuEntryNumbers(device.getCPUEntryCount());
            readServiceInfo(device, cpuEntryNumbers);
          } else if (UPDATE_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSelectedCpuEntryNumbers(cpuEntryParameter.getNumber());
            updateCipheredFirmware(device, cpuEntryNumbers, pathParameter.getPath());
          } else if (UPDATE_MASTER_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createMasterCpuEntryNumbers();
            updateCipheredFirmware(device, cpuEntryNumbers, pathParameter.getPath());
          } else if (UPDATE_SLAVE_COMMAND.equals(clp.getParsedCommand())) {
            int[] cpuEntryNumbers = createSlaveCpuEntryNumbers(device.getCPUEntryCount());
            updateCipheredFirmware(device, cpuEntryNumbers, pathParameter.getPath());
          } else {
            throw new AssertionError();
          }
        } finally {
          device.detach();
        }
      }
    }
  }

  private static int[] createSelectedCpuEntryNumbers(final int cpuEntryNumber) {
    return new int[] {cpuEntryNumber};
  }

  private static int[] createMasterCpuEntryNumbers() {
    return new int[] {0};
  }

  private static int[] createSlaveCpuEntryNumbers(final int cpuEntryCount) {
    int[] cpuEntryNumbers = new int[cpuEntryCount - 1];
    for (int i = 0; i < cpuEntryNumbers.length; i++) {
      cpuEntryNumbers[i] = i + 1;
    }
    return cpuEntryNumbers;
  }

  private static int[] createAllCpuEntryNumbers(final int cpuEntryCount) {
    int[] cpuEntryNumbers = new int[cpuEntryCount];
    for (int i = 0; i < cpuEntryNumbers.length; i++) {
      cpuEntryNumbers[i] = i;
    }
    return cpuEntryNumbers;
  }

  private static ServiceInfo getServiceInfo(final IDevice device, final int cpuEntryNumber) throws Exception {
    ICPUEntry cpuEntry = device.getCPUEntries().get(cpuEntryNumber);
    IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
    serviceChannel.enslave();
    ServiceInfo info = serviceChannel.getInfo();
    serviceChannel.free();
    return info;
  }

  private static void readServiceInfo(final IDevice device, final int[] cpuEntryNumbers) throws Exception {
    for (int cpuEntryNumber : cpuEntryNumbers) {
      ServiceInfo info = getServiceInfo(device, cpuEntryNumber);

      System.out.println("Service info for CPU " + cpuEntryNumber + ":");
      System.out.println("  Firmware type: " + info.getFirmwareType());
      System.out.println("  Firmware version: " + ServiceInfoEncodingHelper.firmwareVersionToString(info.getFirmwareVersion()));
      System.out.println("  Firmware updater version: " + ServiceInfoEncodingHelper.firmwareVersionToString(info.getFirmwareUpdaterVersion()));
      System.out.println("  Device UID: " + info.getDeviceUID());
      System.out.println("  OS time: " + info.getOsTime());
      System.out.println("  CPU usage: " + info.getCpuUsage() + "%");
    }
  }

  private static void updateCipheredFirmware(final IDevice device, final int[] cpuEntryNumbers, final Path cipheredFirmwareZipPath) throws Exception {
    URI cipheredFirmwareZipUri = URI.create("jar:file:" + cipheredFirmwareZipPath.toRealPath());
    FileSystem cipheredFirmwareZipFs = FileSystems.newFileSystem(cipheredFirmwareZipUri, Collections.emptyMap());

    for (int cpuEntryNumber : cpuEntryNumbers) {
      ServiceInfo serviceInfo = getServiceInfo(device, cpuEntryNumber);
      if (serviceInfo.getFirmwareUpdaterVersion() < 0) {
        System.out.println("Firmware updater is not detected on CPU " + cpuEntryNumber + ", firmware can not updated without it");
        continue;
      }

      BoardCipheredFirmware boardCipheredFirmware = BoardCipheredFirmware.read(Files.list(cipheredFirmwareZipFs.getPath("/"))
          .filter(p -> !Files.isDirectory(p) && p.getFileName().toString().equals(serviceInfo.getDeviceUID().getCanonicalName() + "_" + serviceInfo.getFirmwareType().name()))
          .findFirst()
          .orElse(null));

      ICPUEntry cpuEntry = device.getCPUEntries().get(cpuEntryNumber);
      IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
      serviceChannel.enslave();

      Iterator<CipheredFWBlock> iterator = boardCipheredFirmware.getCipheredFWBlocks().iterator();
      for (short blockNumber = 0; iterator.hasNext(); blockNumber++) {
        CipheredFWBlock cipheredFWBlock = iterator.next();
        serviceChannel.writeCipheredFWBlock(blockNumber, cipheredFWBlock);
      }
      serviceChannel.applyFW(boardCipheredFirmware.getFirmwareCrc32());
      serviceChannel.reset(200);
      serviceChannel.free();
      System.out.println("Ciphered firmware for CPU " + cpuEntryNumber + " was successfully updated");
    }
  }

  private static void showCipheredFirmwareInformation(final Path cipheredFirmwareZipPath) throws Exception {
    URI cipheredFirmwareZipUri = URI.create("jar:file:" + cipheredFirmwareZipPath.toRealPath());
    FileSystem cipheredFirmwareZipFs = FileSystems.newFileSystem(cipheredFirmwareZipUri, Collections.emptyMap());

    List<Path> cipheredFirmwarePaths = Files.list(cipheredFirmwareZipFs.getPath("/"))
        .filter(p -> !Files.isDirectory(p))
        .filter(p -> {
          final String fileName = p.getFileName().toString();
          String[] fileNameSplit = fileName.split("_");
          try {
            DeviceUID deviceUID = DeviceUID.valueFromCanonicalName(fileNameSplit[0]);
            return DeviceClass.ETH == deviceUID.getDeviceType().getDeviceClass();
          } catch (Exception ignore) {
            return false;
          }
        })
        .sorted().collect(Collectors.toList());
    for (Path cipheredFirmwarePath : cipheredFirmwarePaths) {
      System.out.println("File name: " + cipheredFirmwarePath.getFileName());
      BoardCipheredFirmware boardCipheredFirmware = BoardCipheredFirmware.read(cipheredFirmwarePath);
      System.out.println("  Firmware type: " + boardCipheredFirmware.getFirmwareType());
      System.out.println("  Firmware serial number: " + boardCipheredFirmware.getFirmwareSerialNumber());
      System.out.println("  Firmware version: " + boardCipheredFirmware.getFirmwareVersion());
      System.out.println("  Git SHA: " + boardCipheredFirmware.getGitSha());
      System.out.println("  Firmware date: " + boardCipheredFirmware.getDate());
      System.out.println("  Firmware time: " + boardCipheredFirmware.getTime());
    }
  }

  @Parameters(commandDescription = "Read service info on selected CPU entry")
  private static class ReadCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CPUEntryParameter cpuEntryParameter;

    public ReadCommandParameters(final BoardIPMainParameter mainParameter, final CPUEntryParameter cpuEntryParameter) {
      this.mainParameter = mainParameter;
      this.cpuEntryParameter = cpuEntryParameter;
    }
  }

  @Parameters(commandDescription = "Read service info on LAN master CPU entry")
  private static class ReadMasterCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    public ReadMasterCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Read service info on GSM or SIM slave CPU entries")
  private static class ReadSlaveCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    public ReadSlaveCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Read service info on all CPU entries")
  private static class ReadAllCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    public ReadAllCommandParameters(final BoardIPMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Update CPU firmware on selected CPU entry")
  private static class UpdateCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CPUEntryParameter cpuEntryParameter;

    @ParametersDelegate
    private final CipheredFirmwarePathParameter pathParameter;

    public UpdateCommandParameters(final BoardIPMainParameter mainParameter, final CPUEntryParameter cpuEntryParameter, final CipheredFirmwarePathParameter pathParameter) {
      this.mainParameter = mainParameter;
      this.cpuEntryParameter = cpuEntryParameter;
      this.pathParameter = pathParameter;
    }
  }

  @Parameters(commandDescription = "Update CPU firmware on LAN master CPU entry")
  private static class UpdateMasterCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CipheredFirmwarePathParameter pathParameter;

    public UpdateMasterCommandParameters(final BoardIPMainParameter mainParameter, final CipheredFirmwarePathParameter pathParameter) {
      this.mainParameter = mainParameter;
      this.pathParameter = pathParameter;
    }
  }

  @Parameters(commandDescription = "Update CPU firmware on GSM or SIM slave CPU entries")
  private static class UpdateSlaveCommandParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final CipheredFirmwarePathParameter pathParameter;

    public UpdateSlaveCommandParameters(final BoardIPMainParameter mainParameter, final CipheredFirmwarePathParameter pathParameter) {
      this.mainParameter = mainParameter;
      this.pathParameter = pathParameter;
    }
  }

  @Parameters(commandDescription = "Show ciphered firmware information")
  private static class ShowCommandParameters {
    @ParametersDelegate
    private final CipheredFirmwarePathParameter pathParameter;

    public ShowCommandParameters(final CipheredFirmwarePathParameter pathParameter) {
      this.pathParameter = pathParameter;
    }
  }

  private final static class CipheredFirmwarePathParameter {
    @Parameter(names = "--path", description = "Path to zip file with ciphered firmware", required = true)
    private Path path;

    public Path getPath() {
      return path;
    }
  }

}
