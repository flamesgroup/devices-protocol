/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpStream;
import com.flamesgroup.device.protocol.cudp.ICudpConnection;
import com.flamesgroup.device.tool.parameter.CudpCommonParameters;
import com.flamesgroup.device.tool.parameter.DeviceUIDMainParameter;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Set;

public class BoardExpireTimeUpdater {

  private static final String READ_COMMAND = "read";
  private static final String[] READ_COMMAND_ALIASES = {"r"};
  private static final String UPDATE_COMMAND = "update";
  private static final String[] UPDATE_COMMAND_ALIASES = {"u"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final DeviceUIDMainParameter mainParameter = new DeviceUIDMainParameter();

    final CudpCommonParameters commonParameters = new CudpCommonParameters();
    final ReadCommandParameters readCommandParameters = new ReadCommandParameters(mainParameter);
    final UpdateCommandParameters updateCommandParameters = new UpdateCommandParameters();

    clp.addObject(commonParameters);
    clp.addCommand(READ_COMMAND, readCommandParameters, READ_COMMAND_ALIASES);
    clp.addCommand(UPDATE_COMMAND, updateCommandParameters, UPDATE_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else {
        ICudpConnection cudpConnection = new CudpConnection(new CudpStream(), commonParameters.getResponseTimeout());
        cudpConnection.connect(new InetSocketAddress(commonParameters.getRemoteAddress(), CudpConnection.CUDP_DEFAULT_PORT));
        try {
          if (READ_COMMAND.equals(clp.getParsedCommand())) {
            readDeviceExpireTime(cudpConnection, mainParameter.getDeviceUID());
          } else if (UPDATE_COMMAND.equals(clp.getParsedCommand())) {
            final Set<DeviceUID> observableDeviceUIDs = cudpConnection.getDeviceUIDs(commonParameters.getResponseTimeout());
            updateDevicesExpireTime(cudpConnection, observableDeviceUIDs, updateCommandParameters.getPath(), commonParameters.isVerbose());
          } else {
            throw new AssertionError();
          }
        } finally {
          cudpConnection.disconnect();
        }
      }
    }
  }

  private static void readDeviceExpireTime(final ICudpConnection cudpConnection, final DeviceUID deviceUID) throws CudpException, InterruptedException {
    final SimpleDateFormat iso8601SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    long expireTime = cudpConnection.getExpireTime(deviceUID).getValue();
    System.out.println("device.uid=" + deviceUID.getCanonicalName());
    System.out.println("cpu0.license.expiretime.timestamp=" + expireTime);
    System.out.println("cpu0.license.expiretime.iso8601=" + iso8601SimpleDateFormat.format(expireTime));
  }

  private static void updateDevicesExpireTime(final ICudpConnection cudpConnection, final Set<DeviceUID> observableDeviceUIDs, final Path keysZipPath, final boolean verbose)
      throws IOException, CudpException, InterruptedException {
    int okCount = 0;
    int errorCount = 0;
    int notFoundCount = 0;

    URI keysZipUri = URI.create("jar:file:" + keysZipPath.toRealPath());
    FileSystem keysZipFs = FileSystems.newFileSystem(keysZipUri, Collections.emptyMap());

    for (DeviceUID deviceUID : observableDeviceUIDs) {
      byte key[];
      Path keyPath = keysZipFs.getPath(deviceUID.getCanonicalName());
      try {
        System.out.print(String.format("  Read key file for %s", deviceUID.getCanonicalName()));
        key = Files.readAllBytes(keyPath);
        cudpConnection.updateExpireTime(deviceUID, key);
        System.out.println(" - OK");
        okCount++;
      } catch (NoSuchFileException ignored) {
        System.out.println(" - WARN: missing file");
        notFoundCount++;
      } catch (CudpException | IOException e) {
        printError(" - ERROR: ", e, verbose);
        errorCount++;
      }
    }

    System.out.println("");
    System.out.println(String.format("OK - %d", okCount));
    System.out.println(String.format("ERROR - %d", errorCount));
    System.out.println(String.format("NOT FOUND - %d", notFoundCount));
  }

  @Parameters(commandDescription = "Read device license expire time")
  private static class ReadCommandParameters {
    @ParametersDelegate
    private final DeviceUIDMainParameter mainParameter;

    private ReadCommandParameters(final DeviceUIDMainParameter mainParameter) {
      this.mainParameter = mainParameter;
    }
  }

  @Parameters(commandDescription = "Update devices license expire time")
  private static class UpdateCommandParameters {
    @Parameter(names = "--path", description = "Path to zip file with keys", required = true)
    private Path path;

    public Path getPath() {
      return path;
    }
  }

  private static void printError(final String prefixMessage, final Exception e, final boolean verbose) {
    System.out.println(prefixMessage + e.getMessage());
    if (verbose) {
      System.err.println("EXCEPTION in Thread: " + Thread.currentThread().getName() + " (" + Thread.currentThread().toString() + ")");
      e.printStackTrace(System.err);
    }
  }

}
