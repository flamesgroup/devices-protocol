/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.bootloader.stm32.Stm32BootloaderException;
import com.flamesgroup.device.bootloader.stm32.Stm32BootloaderUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.box.IBOXDevAddresses;
import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.device.tool.parameter.CommonParameters;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.IDev;
import com.flamesgroup.jdev.gpio.ISysfsGPIOOut;
import com.flamesgroup.jdev.gpio.SysfsGPIO;
import com.flamesgroup.jdev.i2c.I2COptions;
import com.flamesgroup.jdev.i2c.jni.I2CDev;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BOXFirmwareBurn {

  private static final String LIST_COMMAND = "list";
  private static final String[] LIST_COMMAND_ALIASES = {"l"};
  private static final String BURN_COMMAND = "burn";
  private static final String[] BURN_COMMAND_ALIASES = {"b"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final FirmwareZipPathParameter pathParameter = new FirmwareZipPathParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final ListCommandParameters listCommandParameters = new ListCommandParameters();
    final BurnCommandParameters burnCommandParameters = new BurnCommandParameters(pathParameter);

    clp.addObject(commonParameters);
    clp.addCommand(LIST_COMMAND, listCommandParameters, LIST_COMMAND_ALIASES);
    clp.addCommand(BURN_COMMAND, burnCommandParameters, BURN_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        if (LIST_COMMAND.equals(clp.getParsedCommand())) {
          listCpuUid();
        } else if (BURN_COMMAND.equals(clp.getParsedCommand())) {
          burnFirmware(pathParameter.getPath(), burnCommandParameters.isReadoutUnprotect(), burnCommandParameters.isReadoutProtect(), commonParameters.isVerbose());
        } else {
          throw new AssertionError();
        }
      }
    }
  }

  private static void listCpuUid() throws Exception {
    DeviceUID deviceUID = BOXUtil.readDeviceUID();
    System.out.println("device.uid=" + deviceUID.getCanonicalName());

    IBOXDevAddresses boxDevAddresses = BOXUtil.getBOXDevAddresses();
    ISysfsGPIOOut boot0 = new SysfsGPIO(boxDevAddresses.getBOOT0SysfsGPIOAddress());
    ISysfsGPIOOut pboot = new SysfsGPIO(boxDevAddresses.getPBOOTSysfsGPIOAddress());
    List<ISysfsGPIOOut> rsts = boxDevAddresses.getRSTSysfsGPIOAddresses().stream().map(SysfsGPIO::new).collect(Collectors.toList());
    IDev stm32BootloaderDev = new I2CDev(boxDevAddresses.getI2CDevAddress(), new I2COptions(0x39));

    boot0.setValue(false);
    pboot.setValue(false);
    for (ISysfsGPIOOut rst : rsts) {
      rst.setValue(false);
    }
    try {
      boot0.setValue(true);
      for (int i = 0; i < rsts.size(); i++) {
        ISysfsGPIOOut rst = rsts.get(i);
        rst.setValue(true);
        try {
          byte[] cpuUid = Stm32BootloaderUtil.readCpuUid(stm32BootloaderDev);
          System.out.println("cpu" + i + ".uid=" + DataRepresentationHelper.toHexArrayString(cpuUid, "", ""));
        } catch (Stm32BootloaderException | DevException e) {
          System.out.println("cpu" + i + ".error=" + e.getMessage());
        }
        rst.setValue(false);
      }
    } finally {
      boot0.setValue(false);
      pboot.setValue(false);
      for (ISysfsGPIOOut rst : rsts) {
        rst.setValue(true);
      }
    }
  }

  private static void burnFirmware(final Path firmwareZipPath, final boolean readoutUnprotect, final boolean readoutProtect, final boolean verbose) throws Exception {

    Stm32BootloaderUtil.IUploadFirmwareHandler h = new Stm32BootloaderUtil.IUploadFirmwareHandler() {

      private final PercentProgressStringFormatter percentProgressStringFormatter = new PercentProgressStringFormatter();

      @Override
      public void handleEraseBegin() {
        System.out.print("  Progress - erasing...\r");
      }

      @Override
      public void handleEraseEnd() {
        System.out.print("  Progress - erased    \r");
      }

      @Override
      public void handleWriteBegin() {
        System.out.print("                       \r"); // clear previous text
        System.out.print("  Progress " + percentProgressStringFormatter.begin() + "\r");
      }

      @Override
      public void handleWriteProgress(final int written, final int length) {
        System.out.print("  Progress " + percentProgressStringFormatter.progress(written, length) + "\r");
      }

      @Override
      public void handleWriteEnd() {
        System.out.print("  Progress " + percentProgressStringFormatter.end() + "\r");
      }
    };

    int successBurn = 0;
    int errorBurn = 0;

    URI firmwareZipUri = URI.create("jar:file:" + firmwareZipPath.toRealPath());
    FileSystem firmwareZipFs = FileSystems.newFileSystem(firmwareZipUri, Collections.emptyMap());

    IBOXDevAddresses boxDevAddresses = BOXUtil.getBOXDevAddresses();
    ISysfsGPIOOut boot0 = new SysfsGPIO(boxDevAddresses.getBOOT0SysfsGPIOAddress());
    ISysfsGPIOOut pboot = new SysfsGPIO(boxDevAddresses.getPBOOTSysfsGPIOAddress());
    List<ISysfsGPIOOut> rsts = boxDevAddresses.getRSTSysfsGPIOAddresses().stream().map(SysfsGPIO::new).collect(Collectors.toList());
    IDev stm32BootloaderDev = new I2CDev(boxDevAddresses.getI2CDevAddress(), new I2COptions(0x39));

    boot0.setValue(false);
    pboot.setValue(false);
    for (ISysfsGPIOOut rst : rsts) {
      rst.setValue(false);
    }
    boot0.setValue(true);
    try {
      for (int i = 0; i < rsts.size(); i++) {
        System.out.println("Burn CPU " + i);

        ISysfsGPIOOut rst = rsts.get(i);

        if (readoutUnprotect) {
          rst.setValue(true);
          try {
            System.out.print("  Readout Unprotect");
            Stm32BootloaderUtil.readoutUnprotect(stm32BootloaderDev);
            System.out.println(" - Ok");
          } catch (Stm32BootloaderException | DevException e) {
            printError(" - ERROR: ", e, verbose);
            errorBurn++;
            continue;
          } finally {
            rst.setValue(false);
          }
        }

        String cpuUidName;
        rst.setValue(true);
        try {
          byte[] cpuUid = Stm32BootloaderUtil.readCpuUid(stm32BootloaderDev);
          cpuUidName = DataRepresentationHelper.toHexArrayString(cpuUid, "", "");
          System.out.println("  CPU UID - " + cpuUidName);
        } catch (Stm32BootloaderException | DevException e) {
          printError("  CPU UID - ERROR: ", e, verbose);
          errorBurn++;
          continue;
        } finally {
          rst.setValue(false);
        }

        byte[] firmware;
        Path firmwarePath = firmwareZipFs.getPath(cpuUidName);
        try {
          System.out.print("  Read firmware file");
          firmware = Stm32BootloaderUtil.readAndAlignFirmware(firmwarePath);
          System.out.println(" - Ok");
        } catch (NoSuchFileException e) {
          System.out.println(" - ERROR: no proper file found");
          errorBurn++;
          continue;
        } catch (IOException e) {
          printError(" - ERROR: ", e, verbose);
          errorBurn++;
          continue;
        }

        rst.setValue(true);
        try {
          Stm32BootloaderUtil.uploadFirmware(stm32BootloaderDev, firmware, h);
          System.out.println();
        } catch (Stm32BootloaderException | DevException e) {
          printError("    ERROR: ", e, verbose);
          errorBurn++;
          continue;
        } finally {
          rst.setValue(false);
        }

        if (readoutProtect) {
          rst.setValue(true);
          try {
            System.out.print("  Readout Protect");
            Stm32BootloaderUtil.readoutProtect(stm32BootloaderDev);
            System.out.println(" - Ok");
          } catch (Stm32BootloaderException | DevException e) {
            printError(" - ERROR: ", e, verbose);
            errorBurn++;
            continue;
          } finally {
            rst.setValue(false);
          }
        }

        successBurn++;
      }
    } finally {
      boot0.setValue(false);
      for (ISysfsGPIOOut rst : rsts) {
        rst.setValue(true);
      }
    }
    System.out.println("Burn result");
    System.out.println("  success: " + successBurn);
    System.out.println("  error: " + errorBurn);
  }

  @Parameters(commandDescription = "List all CPU UID through STM32 bootloader in properties format")
  private static class ListCommandParameters {
    public ListCommandParameters() {
    }
  }

  @Parameters(commandDescription = "Burn firmware on all CPU entries through STM32 bootloader")
  private static class BurnCommandParameters {
    @ParametersDelegate
    private final FirmwareZipPathParameter pathParameter;

    @Parameter(names = "--readoutUnprotect", description = "Readout unprotect before burn", hidden = true)
    private boolean readoutUnprotect = false;

    @Parameter(names = "--readoutProtect", description = "Readout protect after burn", hidden = true)
    private boolean readoutProtect = false;

    public BurnCommandParameters(final FirmwareZipPathParameter pathParameter) {
      this.pathParameter = pathParameter;
    }

    public boolean isReadoutUnprotect() {
      return readoutUnprotect;
    }

    public boolean isReadoutProtect() {
      return readoutProtect;
    }
  }

  private final static class FirmwareZipPathParameter {
    @Parameter(names = "--path", description = "Path to zip file with firmware", required = true)
    private Path path;

    public Path getPath() {
      return path;
    }
  }

  private static void printError(final String prefixMessage, final Exception e, final boolean verbose) {
    System.out.println(prefixMessage + e.getMessage());
    if (verbose) {
      System.err.println("EXCEPTION in Thread: " + Thread.currentThread().getName() + " (" + Thread.currentThread().toString() + ")");
      e.printStackTrace(System.err);
    }
  }

}
