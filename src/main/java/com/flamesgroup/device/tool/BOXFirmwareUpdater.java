/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.bootloader.secure.SecureBootloaderException;
import com.flamesgroup.device.bootloader.secure.SecureBootloaderUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.box.IBOXDevAddresses;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.ServiceInfo;
import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.device.helper.ServiceInfoEncodingHelper;
import com.flamesgroup.device.tool.parameter.CommonParameters;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.gpio.ISysfsGPIOOut;
import com.flamesgroup.jdev.gpio.SysfsGPIO;
import com.flamesgroup.jdev.i2c.I2COptions;
import com.flamesgroup.jdev.i2c.jni.I2CDev;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class BOXFirmwareUpdater {

  private static final String READ_COMMAND = "read";
  private static final String[] READ_COMMAND_ALIASES = {"r"};
  private static final String LIST_COMMAND = "list";
  private static final String[] LIST_COMMAND_ALIASES = {"l"};
  private static final String UPDATE_COMMAND = "update";
  private static final String[] UPDATE_COMMAND_ALIASES = {"u"};
  private static final String SHOW_COMMAND = "show";
  private static final String[] SHOW_COMMAND_ALIASES = {"s"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final CipheredFirmwareZipPathParameter pathParameter = new CipheredFirmwareZipPathParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final ReadCommandParameters readCommandParameters = new ReadCommandParameters();
    final ListCommandParameters listCommandParameters = new ListCommandParameters();
    final UpdateCommandParameters updateCommandParameters = new UpdateCommandParameters(pathParameter);
    final ShowCommandParameters showCommandParameters = new ShowCommandParameters(pathParameter);

    clp.addObject(commonParameters);
    clp.addCommand(READ_COMMAND, readCommandParameters, READ_COMMAND_ALIASES);
    clp.addCommand(LIST_COMMAND, listCommandParameters, LIST_COMMAND_ALIASES);
    clp.addCommand(UPDATE_COMMAND, updateCommandParameters, UPDATE_COMMAND_ALIASES);
    clp.addCommand(SHOW_COMMAND, showCommandParameters, SHOW_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (SHOW_COMMAND.equals(clp.getParsedCommand())) {
        showCipheredFirmwareInformation(pathParameter.getPath());
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        if (READ_COMMAND.equals(clp.getParsedCommand())) {
          readServiceInfos();
        } else if (LIST_COMMAND.equals(clp.getParsedCommand())) {
          listCpuUid(commonParameters.isVerbose());
        } else if (UPDATE_COMMAND.equals(clp.getParsedCommand())) {
          updateCipheredFirmware(pathParameter.getPath(), commonParameters.isVerbose());
        } else {
          throw new AssertionError();
        }
      }
    }
  }

  private static void readServiceInfos() throws Exception {
    List<ServiceInfo> serviceInfos = new LinkedList<>();

    IDevice device = DeviceUtil.createSpiDevice();
    device.attach();
    try {
      for (ICPUEntry cpuEntry : device.getCPUEntries()) {
        IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
        serviceChannel.enslave();
        try {
          ServiceInfo serviceInfo = serviceChannel.getInfo();
          serviceInfos.add(serviceInfo);
        } finally {
          serviceChannel.free();
        }
      }
    } finally {
      device.detach();
    }

    System.out.println("BOX Device UID: " + BOXUtil.readDeviceUID());

    for (int i = 0; i < serviceInfos.size(); i++) {
      ServiceInfo serviceInfo = serviceInfos.get(i);
      System.out.println("Service info for CPU " + i + ":");
      System.out.println("  Firmware type: " + serviceInfo.getFirmwareType());
      System.out.println("  Firmware version: " + ServiceInfoEncodingHelper.firmwareVersionToString(serviceInfo.getFirmwareVersion()));
      System.out.println("  Firmware updater version: " + ServiceInfoEncodingHelper.firmwareVersionToString(serviceInfo.getFirmwareUpdaterVersion()));
      System.out.println("  Device UID: " + serviceInfo.getDeviceUID());
      System.out.println("  OS time: " + serviceInfo.getOsTime());
      System.out.println("  CPU usage: " + serviceInfo.getCpuUsage() + "%");
    }
  }

  private static void listCpuUid(final boolean verbose) throws Exception {
    DeviceUID deviceUID = BOXUtil.readDeviceUID();
    System.out.println("device.uid=" + deviceUID.getCanonicalName());

    IBOXDevAddresses boxDevAddresses = BOXUtil.getBOXDevAddresses();
    ISysfsGPIOOut boot0 = new SysfsGPIO(boxDevAddresses.getBOOT0SysfsGPIOAddress());
    ISysfsGPIOOut pboot = new SysfsGPIO(boxDevAddresses.getPBOOTSysfsGPIOAddress());
    List<ISysfsGPIOOut> rsts = boxDevAddresses.getRSTSysfsGPIOAddresses().stream().map(SysfsGPIO::new).collect(Collectors.toList());

    boot0.setValue(false);
    pboot.setValue(false);
    for (ISysfsGPIOOut rst : rsts) {
      rst.setValue(false);
    }
    pboot.setValue(true);
    for (ISysfsGPIOOut rst : rsts) {
      rst.setValue(true);
    }
    try {
      for (int i = 0; i < rsts.size(); i++) {
        try {
          byte[] cpuUid = SecureBootloaderUtil.readCpuUid(new I2CDev(boxDevAddresses.getI2CDevAddress(), new I2COptions(i + 1)));
          System.out.println("cpu" + i + ".uid=" + DataRepresentationHelper.toHexArrayString(cpuUid, "", ""));
        } catch (SecureBootloaderException | DevException e) {
          printError("cpu" + i + ".error=", e, verbose);
        }
      }
    } finally {
      for (ISysfsGPIOOut rst : rsts) {
        rst.setValue(false);
      }
      boot0.setValue(false);
      pboot.setValue(false);
      for (ISysfsGPIOOut rst : rsts) {
        rst.setValue(true);
      }
    }
  }

  private static void updateCipheredFirmware(final Path cipheredFirmwareZipPath, final boolean verbose) throws Exception {

    SecureBootloaderUtil.IUploadFirmwareHandler h = new SecureBootloaderUtil.IUploadFirmwareHandler() {

      private final PercentProgressStringFormatter percentProgressStringFormatter = new PercentProgressStringFormatter();

      @Override
      public void handleStartBegin() {
        System.out.print("  Progress - starting...\r");
      }

      @Override
      public void handleStartEnd() {
        System.out.print("  Progress - started    \r");
      }

      @Override
      public void handleWriteBegin() {
        System.out.print("                        \r"); // clear previous text
        System.out.print("  Progress " + percentProgressStringFormatter.begin() + "\r");
      }

      @Override
      public void handleWriteProgress(final int firmwareBlocksWritten, final int firmwareBlocksCount) {
        System.out.print("  Progress " + percentProgressStringFormatter.progress(firmwareBlocksWritten, firmwareBlocksCount) + "\r");
      }

      @Override
      public void handleWriteEnd() {
        System.out.print("  Progress " + percentProgressStringFormatter.end() + "\r");
      }

      @Override
      public void handleFinalizeBegin() {
        System.out.print("  Progress - finalizing...\r");
      }

      @Override
      public void handleFinalizeEnd() {
        System.out.print("  Progress - finalized    \r");
      }

    };

    int successUpdate = 0;
    int errorUpdate = 0;

    URI cipheredFirmwareZipUri = URI.create("jar:file:" + cipheredFirmwareZipPath.toRealPath());
    FileSystem cipheredFirmwareZipFs = FileSystems.newFileSystem(cipheredFirmwareZipUri, Collections.emptyMap());

    IBOXDevAddresses boxDevAddresses = BOXUtil.getBOXDevAddresses();
    ISysfsGPIOOut boot0 = new SysfsGPIO(boxDevAddresses.getBOOT0SysfsGPIOAddress());
    ISysfsGPIOOut pboot = new SysfsGPIO(boxDevAddresses.getPBOOTSysfsGPIOAddress());
    List<ISysfsGPIOOut> rsts = boxDevAddresses.getRSTSysfsGPIOAddresses().stream().map(SysfsGPIO::new).collect(Collectors.toList());

    boot0.setValue(false);
    pboot.setValue(false);
    for (ISysfsGPIOOut rst : rsts) {
      rst.setValue(false);
    }
    pboot.setValue(true);
    for (ISysfsGPIOOut rst : rsts) {
      rst.setValue(true);
    }

    DeviceUID deviceUID = BOXUtil.readDeviceUID();
    try {
      for (int i = 0; i < rsts.size(); i++) {
        I2CDev secureBootloaderDev = new I2CDev(boxDevAddresses.getI2CDevAddress(), new I2COptions(i + 1));

        System.out.println("Update CPU " + i);

        String cpuUidName;
        try {
          byte[] cpuUid = SecureBootloaderUtil.readCpuUid(secureBootloaderDev);
          cpuUidName = DataRepresentationHelper.toHexArrayString(cpuUid, "", "");
          System.out.println("  CPU UID - " + cpuUidName);
        } catch (SecureBootloaderException | DevException e) {
          printError("  CPU UID - ERROR: ", e, verbose);
          errorUpdate++;
          continue;
        }

        BOXCipheredFirmware boxCipheredFirmware;
        Path cipheredFirmwarePath = cipheredFirmwareZipFs.getPath(deviceUID.getCanonicalName() + "_" + cpuUidName);
        try {
          System.out.print("  Read ciphered firmware file");
          boxCipheredFirmware = BOXCipheredFirmware.read(cipheredFirmwarePath);
          System.out.println(" - Ok");
        } catch (NoSuchFileException e) {
          System.out.println(" - ERROR: no proper file found");
          errorUpdate++;
          continue;
        } catch (IOException e) {
          printError(" - ERROR: ", e, verbose);
          errorUpdate++;
          continue;
        }

        try {
          SecureBootloaderUtil.uploadFirmware(secureBootloaderDev, boxCipheredFirmware.getCipheredFWBlocks(), boxCipheredFirmware.getFirmwareCrc32(), h);
          System.out.println();
        } catch (SecureBootloaderException | DevException e) {
          printError("    ERROR: ", e, verbose);
          errorUpdate++;
          continue;
        }

        successUpdate++;
      }
    } finally {
      for (ISysfsGPIOOut rst : rsts) {
        rst.setValue(false);
      }
      pboot.setValue(false);
      for (ISysfsGPIOOut rst : rsts) {
        rst.setValue(true);
      }
    }

    System.out.println("Update result");
    System.out.println("  success: " + successUpdate);
    System.out.println("  error: " + errorUpdate);
  }

  private static void showCipheredFirmwareInformation(final Path cipheredFirmwareZipPath) throws Exception {
    URI cipheredFirmwareZipUri = URI.create("jar:file:" + cipheredFirmwareZipPath.toRealPath());
    FileSystem cipheredFirmwareZipFs = FileSystems.newFileSystem(cipheredFirmwareZipUri, Collections.emptyMap());

    List<Path> cipheredFirmwarePaths = Files.list(cipheredFirmwareZipFs.getPath("/"))
        .filter(p -> !Files.isDirectory(p))
        .filter(p -> {
          final String fileName = p.getFileName().toString();
          String[] fileNameSplit = fileName.split("_");
          try {
            DeviceUID deviceUID = DeviceUID.valueFromCanonicalName(fileNameSplit[0]);
            return DeviceClass.SPI == deviceUID.getDeviceType().getDeviceClass();
          } catch (Exception ignore) {
            return false;
          }
        })
        .sorted().collect(Collectors.toList());
    for (Path cipheredFirmwarePath : cipheredFirmwarePaths) {
      System.out.println("File name: " + cipheredFirmwarePath.getFileName());
      BOXCipheredFirmware boxCipheredFirmware = BOXCipheredFirmware.read(cipheredFirmwarePath);
      System.out.println("  Device UID: " + boxCipheredFirmware.getDeviceUID().getCanonicalName());
      System.out.println("  CPU N: " + boxCipheredFirmware.getCpuN());
      System.out.println("  Firmware type: " + boxCipheredFirmware.getFirmwareType());
      System.out.println("  Firmware version: " + boxCipheredFirmware.getFirmwareVersion());
      System.out.println("  Git SHA: " + boxCipheredFirmware.getGitSha());
      System.out.println("  Firmware date: " + boxCipheredFirmware.getDate());
      System.out.println("  Firmware time: " + boxCipheredFirmware.getTime());
    }
  }

  @Parameters(commandDescription = "Read service info on all CPU entries")
  private static class ReadCommandParameters {
    public ReadCommandParameters() {
    }
  }

  @Parameters(commandDescription = "List all CPU UID through secure bootloader in properties format")
  private static class ListCommandParameters {
    public ListCommandParameters() {
    }
  }

  @Parameters(commandDescription = "Update firmware on all CPU entries through secure bootloader")
  private static class UpdateCommandParameters {
    @ParametersDelegate
    private final CipheredFirmwareZipPathParameter pathParameter;

    public UpdateCommandParameters(final CipheredFirmwareZipPathParameter pathParameter) {
      this.pathParameter = pathParameter;
    }
  }

  @Parameters(commandDescription = "Show ciphered firmware information")
  private static class ShowCommandParameters {
    @ParametersDelegate
    private final CipheredFirmwareZipPathParameter pathParameter;

    public ShowCommandParameters(final CipheredFirmwareZipPathParameter pathParameter) {
      this.pathParameter = pathParameter;
    }
  }

  private final static class CipheredFirmwareZipPathParameter {
    @Parameter(names = "--path", description = "Path to zip file with ciphered firmware", required = true)
    private Path path;

    public Path getPath() {
      return path;
    }
  }

  private static void printError(final String prefixMessage, final Exception e, final boolean verbose) {
    System.out.println(prefixMessage + e.getMessage());
    if (verbose) {
      System.err.println("EXCEPTION in Thread: " + Thread.currentThread().getName() + " (" + Thread.currentThread().toString() + ")");
      e.printStackTrace(System.err);
    }
  }

}
