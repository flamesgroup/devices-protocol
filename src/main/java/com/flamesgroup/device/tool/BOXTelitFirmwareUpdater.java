/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.tool.parameter.CommonParameters;
import com.flamesgroup.device.tool.parameter.GSMEntryParameter;

import java.lang.invoke.MethodHandles;

public final class BOXTelitFirmwareUpdater extends BaseTelitFirmwareUpdater {

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final GSMEntryParameter gsmEntryParameter = new GSMEntryParameter();
    final TelitFirmwarePathParameter pathParameter = new TelitFirmwarePathParameter();
    final SkipInitParameter skipInitParameter = new SkipInitParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final ReadCommandParameters readCommandParameters = new ReadCommandParameters(gsmEntryParameter);
    final ReadAllCommandParameters readAllCommandParameters = new ReadAllCommandParameters();
    final UpdateCommandParameters updateCommandParameters = new UpdateCommandParameters(gsmEntryParameter, pathParameter, skipInitParameter);
    final UpdateAllCommandParameters updateAllCommandParameters = new UpdateAllCommandParameters(pathParameter, skipInitParameter);
    final ShowCommandParameters showCommandParameters = new ShowCommandParameters(pathParameter);

    clp.addObject(commonParameters);
    clp.addCommand(READ_COMMAND, readCommandParameters, READ_COMMAND_ALIASES);
    clp.addCommand(READ_ALL_COMMAND, readAllCommandParameters, READ_ALL_COMMAND_ALIASES);
    clp.addCommand(UPDATE_COMMAND, updateCommandParameters, UPDATE_COMMAND_ALIASES);
    clp.addCommand(UPDATE_ALL_COMMAND, updateAllCommandParameters, UPDATE_ALL_COMMAND_ALIASES);
    clp.addCommand(SHOW_COMMAND, showCommandParameters, SHOW_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (SHOW_COMMAND.equals(clp.getParsedCommand())) {
        showTelitFirmware(pathParameter.getPath());
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        IGSMDevice gsmDevice = DeviceUtil.createSpiGSMDevice();
        gsmDevice.attach();
        try {
          if (READ_COMMAND.equals(clp.getParsedCommand())) {
            final int[] gsmEntryNumbers = createSelectedGsmEntryNumbers(gsmEntryParameter.getNumber());
            readTelitModuleFirmwareRevisionIdentification(gsmDevice, gsmEntryNumbers, commonParameters.isVerbose());
          } else if (READ_ALL_COMMAND.equals(clp.getParsedCommand())) {
            final int[] gsmEntryNumbers = createAllGsmEntryNumbers(gsmDevice.getGSMEntryCount());
            readTelitModuleFirmwareRevisionIdentification(gsmDevice, gsmEntryNumbers, commonParameters.isVerbose());
          } else if (UPDATE_COMMAND.equals(clp.getParsedCommand())) {
            final int[] gsmEntryNumbers = createSelectedGsmEntryNumbers(gsmEntryParameter.getNumber());
            updateTelitModuleFirmware(gsmDevice, gsmEntryNumbers, pathParameter.getPath(), skipInitParameter.isSkipInit(), commonParameters.isVerbose());
          } else if (UPDATE_ALL_COMMAND.equals(clp.getParsedCommand())) {
            final int[] gsmEntryNumbers = createAllGsmEntryNumbers(gsmDevice.getGSMEntryCount());
            updateTelitModuleFirmware(gsmDevice, gsmEntryNumbers, pathParameter.getPath(), skipInitParameter.isSkipInit(), commonParameters.isVerbose());
          } else {
            throw new AssertionError();
          }
        } finally {
          gsmDevice.detach();
        }
      }
    }
  }

  private static class ReadCommandParameters extends BaseReadCommandParameters {
    public ReadCommandParameters(final GSMEntryParameter gsmEntryParameter) {
      super(gsmEntryParameter);
    }
  }

  private static class ReadAllCommandParameters extends BaseReadAllCommandParameters {
    public ReadAllCommandParameters() {
    }
  }

  private static class UpdateCommandParameters extends BaseUpdateCommandParameters {
    public UpdateCommandParameters(final GSMEntryParameter gsmEntryParameter, final TelitFirmwarePathParameter pathParameter,
        final SkipInitParameter skipInitParameter) {
      super(gsmEntryParameter, pathParameter, skipInitParameter);
    }
  }

  private static class UpdateAllCommandParameters extends BaseUpdateAllCommandParameters {
    public UpdateAllCommandParameters(final TelitFirmwarePathParameter pathParameter, final SkipInitParameter skipInitParameter) {
      super(pathParameter, skipInitParameter);
    }
  }

  private static class ShowCommandParameters extends BaseShowCommandParameters {
    public ShowCommandParameters(final TelitFirmwarePathParameter pathParameter) {
      super(pathParameter);
    }
  }

}
