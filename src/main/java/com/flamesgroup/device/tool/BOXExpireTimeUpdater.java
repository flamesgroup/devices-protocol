/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.LicenseState;
import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Collections;

public class BOXExpireTimeUpdater {

  private static final String READ_COMMAND = "read";
  private static final String[] READ_COMMAND_ALIASES = {"r"};
  private static final String UPDATE_COMMAND = "update";
  private static final String[] UPDATE_COMMAND_ALIASES = {"u"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final KeysZipPathParameter pathParameter = new KeysZipPathParameter();

    final CommonParameters commonParameters = new CommonParameters();
    final ReadCommandParameters readCommandParameters = new ReadCommandParameters();
    final UpdateCommandParameters updateCommandParameters = new UpdateCommandParameters(pathParameter);

    clp.addObject(commonParameters);
    clp.addCommand(READ_COMMAND, readCommandParameters, READ_COMMAND_ALIASES);
    clp.addCommand(UPDATE_COMMAND, updateCommandParameters, UPDATE_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        if (READ_COMMAND.equals(clp.getParsedCommand())) {
          readLicenseStateAndExpireTime(commonParameters.isVerbose());
        } else if (UPDATE_COMMAND.equals(clp.getParsedCommand())) {
          updateLicenseExpireTime(pathParameter.getPath(), commonParameters.isVerbose());
        } else {
          throw new AssertionError();
        }
      }
    }
  }

  private static void readLicenseStateAndExpireTime(final boolean verbose) throws Exception {
    final SimpleDateFormat iso8601SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    DeviceUID deviceUID = BOXUtil.readDeviceUID();
    System.out.println("device.uid=" + deviceUID.getCanonicalName());

    IDevice device = DeviceUtil.createSpiDevice();
    device.attach();
    try {
      for (ICPUEntry cpuEntry : device.getCPUEntries()) {
        int cpuEntryNumber = cpuEntry.getNumber();
        IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
        serviceChannel.enslave();
        try {
          try {
            LicenseState licenseState = serviceChannel.getLicenseState();
            System.out.println("cpu" + cpuEntryNumber + ".license.state=" + licenseState);
          } catch (ChannelException e) {
            printError("cpu" + cpuEntryNumber + ".license.state.error=", e, verbose);
          }

          try {
            long expireTime = serviceChannel.readLicenseExpireTime();
            System.out.println("cpu" + cpuEntryNumber + ".license.expiretime.timestamp=" + expireTime);
            System.out.println("cpu" + cpuEntryNumber + ".license.expiretime.iso8601=" + iso8601SimpleDateFormat.format(expireTime));
          } catch (ChannelException e) {
            printError("cpu" + cpuEntryNumber + ".license.expiretime.error=", e, verbose);
          }
        } finally {
          serviceChannel.free();
        }
      }
    } finally {
      device.detach();
    }
  }

  private static void updateLicenseExpireTime(final Path keysZipPath, final boolean verbose) throws Exception {
    int successUpdate = 0;
    int errorUpdate = 0;

    URI keysZipUri = URI.create("jar:file:" + keysZipPath.toRealPath());
    FileSystem keysZipFs = FileSystems.newFileSystem(keysZipUri, Collections.emptyMap());

    IDevice device = DeviceUtil.createSpiDevice();
    device.attach();
    try {
      for (ICPUEntry cpuEntry : device.getCPUEntries()) {
        System.out.println("Update CPU " + cpuEntry.getNumber());

        IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
        serviceChannel.enslave();
        try {
          System.out.print("  Read CPU UID");
          byte cpuUid[] = serviceChannel.readCpuUid();
          String cpuUidName = DataRepresentationHelper.toHexArrayString(cpuUid, "", "");
          System.out.println(" - Ok");

          byte key[];
          Path keyPath = keysZipFs.getPath(cpuEntry.getDeviceUID().getCanonicalName() + "_" + cpuUidName);
          try {
            System.out.print("  Read key file");
            key = Files.readAllBytes(keyPath);
            System.out.println(" - Ok");
          } catch (NoSuchFileException e) {
            System.out.println(" - ERROR: no proper file found");
            errorUpdate++;
            continue;
          } catch (IOException e) {
            printError(" - ERROR: ", e, verbose);
            errorUpdate++;
            continue;
          }

          System.out.print("  Update license expire time");
          serviceChannel.updateLicenseExpireTime(key);
          System.out.println(" - Ok");

          System.out.print("  Update license current time");
          serviceChannel.updateLicenseCurrentTime(System.currentTimeMillis());
          System.out.println(" - Ok");
        } catch (ChannelException e) {
          printError(" - ERROR: ", e, verbose);
          errorUpdate++;
          continue;
        } finally {
          serviceChannel.free();
        }

        successUpdate++;
      }
    } finally {
      device.detach();
    }

    System.out.println("Update result:");
    System.out.println("  success: " + successUpdate);
    System.out.println("  error: " + errorUpdate);
  }

  @Parameters(commandDescription = "Read device CPUs license state and expire time")
  private static class ReadCommandParameters {
  }

  @Parameters(commandDescription = "Update device CPUs license expire time")
  private static class UpdateCommandParameters {
    @ParametersDelegate
    private final KeysZipPathParameter pathParameter;

    public UpdateCommandParameters(final KeysZipPathParameter pathParameter) {
      this.pathParameter = pathParameter;
    }
  }

  private final static class KeysZipPathParameter {
    @Parameter(names = "--path", description = "Path to zip file with keys", required = true)
    private Path path;

    public Path getPath() {
      return path;
    }
  }

  private static void printError(final String prefixMessage, final Exception e, final boolean verbose) {
    System.out.println(prefixMessage + e.getMessage());
    if (verbose) {
      System.err.println("EXCEPTION in Thread: " + Thread.currentThread().getName() + " (" + Thread.currentThread().toString() + ")");
      e.printStackTrace(System.err);
    }
  }

}
