/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

public final class PercentProgressStringFormatter {

  private final ProgressStringFormatter progressStringFormatter = new ProgressStringFormatter(3);

  public String begin() {
    return "-   0%";
  }

  public String progress(final int value, final int max) {
    int p = value * 100 / max;
    return progressStringFormatter.progress() + " " + String.format("%3d%%\r", p);
  }

  public String end() {
    return "- 100%";
  }

}
