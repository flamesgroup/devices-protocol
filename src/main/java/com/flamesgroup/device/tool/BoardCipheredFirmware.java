/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.device.commonb.FirmwareType;
import com.flamesgroup.device.helper.ServiceInfoEncodingHelper;
import com.flamesgroup.device.protocol.util.CRC16;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

public final class BoardCipheredFirmware extends BaseCipheredFirmware {

  private final FirmwareType firmwareType;
  private final int firmwareSerialNumber;
  private final String firmwareVersion;
  private final String gitSha;
  private final String date;
  private final String time;
  private final int firmwareCrc32;

  private BoardCipheredFirmware(final FirmwareType firmwareType, final int firmwareSerialNumber, final String firmwareVersion,
      final String gitSha, final String date, final String time, final int firmwareCrc32, final List<CipheredFWBlock> cipheredFWBlocks) {
    super(cipheredFWBlocks);
    this.firmwareType = firmwareType;
    this.firmwareSerialNumber = firmwareSerialNumber;
    this.firmwareVersion = firmwareVersion;
    this.gitSha = gitSha;
    this.date = date;
    this.time = time;
    this.firmwareCrc32 = firmwareCrc32;
  }

  public FirmwareType getFirmwareType() {
    return firmwareType;
  }

  public int getFirmwareSerialNumber() {
    return firmwareSerialNumber;
  }

  public String getFirmwareVersion() {
    return firmwareVersion;
  }

  public String getGitSha() {
    return gitSha;
  }

  public String getDate() {
    return date;
  }

  public String getTime() {
    return time;
  }

  public int getFirmwareCrc32() {
    return firmwareCrc32;
  }

  public static BoardCipheredFirmware read(final Path cipheredFirmwarePath) throws IOException {
    Objects.requireNonNull(cipheredFirmwarePath, "cipheredFirmwarePath mustn't be null");

    ByteBuffer cipheredFirmwareBb = ByteBuffer.wrap(Files.readAllBytes(cipheredFirmwarePath)).order(ByteOrder.LITTLE_ENDIAN);
    return BoardCipheredFirmware.read(cipheredFirmwareBb);
  }

  public static BoardCipheredFirmware read(final ByteBuffer cipheredFirmwareBb) throws IOException {
    Objects.requireNonNull(cipheredFirmwareBb, "cipheredFirmwareBb mustn't be null");

    int headerStartPosition = cipheredFirmwareBb.position();
    final FirmwareType firmwareType = FirmwareType.getFirmwareTypeByN(cipheredFirmwareBb.getInt());
    final int firmwareSerialNumber = cipheredFirmwareBb.getInt();
    final String firmwareVersion = ServiceInfoEncodingHelper.firmwareVersionToString(cipheredFirmwareBb.getInt());
    byte[] gitShaBa = new byte[40];
    cipheredFirmwareBb.get(gitShaBa);
    final String gitSha = new String(gitShaBa).trim();
    final byte[] dateBa = new byte[16];
    cipheredFirmwareBb.get(dateBa);
    final String date = new String(dateBa).trim();
    final byte[] timeBa = new byte[16];
    cipheredFirmwareBb.get(timeBa);
    final String time = new String(timeBa).trim();
    final int firmwareCrc32 = cipheredFirmwareBb.getInt();
    cipheredFirmwareBb.position(cipheredFirmwareBb.position() + 38); // reserve
    int headerEndPosition = cipheredFirmwareBb.position();

    cipheredFirmwareBb.position(headerStartPosition);

    byte[] firmwareHeaderBa = new byte[headerEndPosition - headerStartPosition];
    cipheredFirmwareBb.get(firmwareHeaderBa);

    final CRC16 crc16 = new CRC16();
    crc16.update(firmwareHeaderBa, 0, firmwareHeaderBa.length);
    short calculatedFirmwareHeaderCrc16 = (short) crc16.getValue();

    short firmwareHeaderCrc16 = cipheredFirmwareBb.getShort();

    if (calculatedFirmwareHeaderCrc16 != firmwareHeaderCrc16) {
      throw new IllegalArgumentException("Ciphered firmware header has wrong CRC16");
    }

    final List<CipheredFWBlock> cipheredFWBlocks = readCipheredFWBlocks(cipheredFirmwareBb);

    return new BoardCipheredFirmware(firmwareType, firmwareSerialNumber, firmwareVersion, gitSha, date, time, firmwareCrc32, cipheredFWBlocks);
  }

}
