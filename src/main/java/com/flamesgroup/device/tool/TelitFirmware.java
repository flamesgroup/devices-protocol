/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public final class TelitFirmware {

  private final String softwareVersion;
  private final String productName;
  private final int memoryType;
  private final int streamVersion;
  private final long speedOffset;

  private final List<byte[]> blocks;

  private TelitFirmware(final String softwareVersion, final String productName, final int memoryType, final int streamVersion, final long speedOffset, final List<byte[]> blocks) {
    this.softwareVersion = softwareVersion;
    this.productName = productName;
    this.memoryType = memoryType;
    this.streamVersion = streamVersion;
    this.speedOffset = speedOffset;
    this.blocks = Collections.unmodifiableList(blocks);
  }

  public String getSoftwareVersion() {
    return softwareVersion;
  }

  public String getProductName() {
    return productName;
  }

  public int getMemoryType() {
    return memoryType;
  }

  public int getStreamVersion() {
    return streamVersion;
  }

  public long getSpeedOffset() {
    return speedOffset;
  }

  public List<byte[]> getBlocks() {
    return blocks;
  }

  public static TelitFirmware read(final Path path) throws Exception {
    Objects.requireNonNull(path, "path mustn't be null");

    ByteBuffer firmwareBb = ByteBuffer.wrap(Files.readAllBytes(path)).order(ByteOrder.LITTLE_ENDIAN);

    int headerSize = Short.toUnsignedInt(firmwareBb.getShort());
    byte[] softwareVersionBa = new byte[32];
    firmwareBb.get(softwareVersionBa);
    String softwareVersion = new String(softwareVersionBa, "ASCII").trim();
    byte[] productNameBa = new byte[32];
    firmwareBb.get(productNameBa);
    String productName = new String(productNameBa, "ASCII").trim();
    int memoryType = Short.toUnsignedInt(firmwareBb.getShort());
    int streamVersion = Short.toUnsignedInt(firmwareBb.getShort());
    long speedOffset = Integer.toUnsignedLong(firmwareBb.getInt());

    final List<byte[]> blocks = new LinkedList<>();

    firmwareBb.position(headerSize);
    while (firmwareBb.hasRemaining()) {
      int blockSize = Short.toUnsignedInt(firmwareBb.getShort()) + 1;
      byte[] block = new byte[blockSize];
      firmwareBb.get(block);
      blocks.add(block);
    }

    return new TelitFirmware(softwareVersion, productName, memoryType, streamVersion, speedOffset, blocks);
  }

}
