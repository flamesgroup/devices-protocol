/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool.parameter;

import com.beust.jcommander.Parameter;

import java.net.InetAddress;

public class CudpCommonParameters extends CommonParameters {

  @Parameter(names = "--remoteAddress", description = "Broadcast address for interaction with devices", converter = InetAddressConverter.class)
  private InetAddress remoteAddress = CudpDefault.REMOTE_ADDRESS;

  @Parameter(names = "--responseTimeout", description = "Waiting response timeout")
  private int responseTimeout = 1000;

  public InetAddress getRemoteAddress() {
    return remoteAddress;
  }

  public int getResponseTimeout() {
    return responseTimeout;
  }

}
