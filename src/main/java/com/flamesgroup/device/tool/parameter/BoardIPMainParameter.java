/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool.parameter;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public final class BoardIPMainParameter {

  @Parameter(description = "<BoardIP>", converter = InetAddressConverter.class)
  private List<InetAddress> boardsIPs = new ArrayList<>();

  public InetAddress getBoardIP() {
    if (boardsIPs.size() == 1) {
      return boardsIPs.get(0);
    } else {
      throw new ParameterException("<BoardIP> parameter specify incorrectly");
    }
  }

}
