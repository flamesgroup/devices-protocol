/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool.parameter;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class CudpDefault {

  public static final InetAddress ADDRESS;
  public static final InetAddress MASK;
  public static final InetAddress REMOTE_ADDRESS;

  static {
    try {
      ADDRESS = InetAddress.getByName("0.0.0.0");
      MASK = InetAddress.getByName("255.255.255.0");
      REMOTE_ADDRESS = InetAddress.getByName("255.255.255.255");
    } catch (UnknownHostException e) {
      throw new AssertionError(e);
    }
  }

}
