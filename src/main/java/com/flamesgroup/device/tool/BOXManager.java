/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.box.IBOXDevAddresses;
import com.flamesgroup.device.tool.parameter.CommonParameters;
import com.flamesgroup.jdev.gpio.ISysfsGPIOOut;
import com.flamesgroup.jdev.gpio.SysfsGPIO;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.stream.Collectors;

public class BOXManager {

  private static final String REBOOT_COMMAND = "reboot";
  private static final String[] REBOOT_COMMAND_ALIASES = {"r"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final CommonParameters commonParameters = new CommonParameters();
    final RebootCommandParameters rebootCommandParameters = new RebootCommandParameters();

    clp.addObject(commonParameters);
    clp.addCommand(REBOOT_COMMAND, rebootCommandParameters, REBOOT_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        IBOXDevAddresses boxDevAddresses = BOXUtil.getBOXDevAddresses();
        ISysfsGPIOOut boot0 = new SysfsGPIO(boxDevAddresses.getBOOT0SysfsGPIOAddress());
        ISysfsGPIOOut pboot = new SysfsGPIO(boxDevAddresses.getPBOOTSysfsGPIOAddress());
        List<ISysfsGPIOOut> rsts = boxDevAddresses.getRSTSysfsGPIOAddresses().stream().map(SysfsGPIO::new).collect(Collectors.toList());

        if (REBOOT_COMMAND.equals(clp.getParsedCommand())) {
          boot0.setValue(false);
          pboot.setValue(false);
          for (ISysfsGPIOOut rst : rsts) {
            rst.setValue(false);
          }
          Thread.sleep(100);
          for (ISysfsGPIOOut rst : rsts) {
            rst.setValue(true);
          }
        } else {
          throw new AssertionError();
        }
      }
    }
  }

  @Parameters(commandDescription = "Reboot all microcontrollers on device and set default value for BOOT0 & PBOOT")
  private static class RebootCommandParameters {
  }

}
