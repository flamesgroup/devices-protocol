package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.simb.ISIMDevice;
import com.flamesgroup.device.simb.ISIMEntry;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class BOXIndicationsTester {

  private static final String SET_INDICATION_MODE_ALL_COMMAND = "setindicationmodeall";
  private static final String[] SET_INDICATION_MODE_ALL_COMMAND_ALIASES = {"sima"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final CommonParameters commonParameters = new CommonParameters();
    final SetIndicationModeAllCommandParameters setIndicationModeAllCommandParameters = new SetIndicationModeAllCommandParameters();

    clp.addObject(commonParameters);
    clp.addCommand(SET_INDICATION_MODE_ALL_COMMAND, setIndicationModeAllCommandParameters, SET_INDICATION_MODE_ALL_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        System.out.println("BOX Device UID: " + BOXUtil.readDeviceUID());

        IDevice device = DeviceUtil.createSpiDevice();
        device.attach();
        try {
          if (SET_INDICATION_MODE_ALL_COMMAND.equals(clp.getParsedCommand())) {
            setIndicationModeAll(device, setIndicationModeAllCommandParameters.getIndicationMode(), commonParameters.isVerbose());
          } else {
            throw new AssertionError();
          }

          // Need to wait here for user interaction,
          // because after detach indication will be stopped (because of SYNC signal toggle stop) and
          // set to none (because of firmware reaction for SYNC signal toggle stop).
          System.out.println("Press enter to exit...");
          Scanner keyboard = new Scanner(System.in);
          keyboard.nextLine();

        } finally {
          device.detach();
        }
      }
    }
  }

  private static void setIndicationModeAll(final IDevice device, final IndicationMode indicationMode, final boolean verbose) throws Exception {
    final List<IIndicationChannel> indicationChannels;
    if (device instanceof IGSMDevice) {
      IGSMDevice gsmDevice = (IGSMDevice) device;
      indicationChannels = gsmDevice.getGSMEntries().stream().map(IGSMEntry::getIndicationChannel).collect(Collectors.toList());
    } else if (device instanceof ISIMDevice) {
      ISIMDevice simDevice = (ISIMDevice) device;
      indicationChannels = simDevice.getSIMEntries().stream().map(ISIMEntry::getIndicationChannel).collect(Collectors.toList());
    } else {
      throw new AssertionError();
    }

    final AtomicInteger successSetAtomicInteger = new AtomicInteger();
    final AtomicInteger errorSetAtomicInteger = new AtomicInteger();

    ExecutorService executor = Executors.newCachedThreadPool();
    indicationChannels.forEach(indicationChannel -> executor.submit(() -> {
      final String actionPrefixMessage = "Set indication for " + indicationChannel;
      try {
        indicationChannel.enslave();
        try {
          indicationChannel.setIndicationMode(indicationMode);
          successSetAtomicInteger.incrementAndGet();
          if (verbose) {
            System.out.println(actionPrefixMessage + " - SUCCESS");
          }
        } finally {
          indicationChannel.free();
        }
      } catch (ChannelException | InterruptedException e) {
        errorSetAtomicInteger.incrementAndGet();
        printError(actionPrefixMessage + " - ERROR: ", Thread.currentThread(), e, verbose);
      }
    }));
    executor.shutdown();
    executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);

    if (verbose) {
      System.out.println("Set indication result");
      System.out.println("  success: " + successSetAtomicInteger.get());
      System.out.println("  error: " + errorSetAtomicInteger.get());
    }
  }

  @Parameters(commandDescription = "Set indication mode for all entries")
  private static class SetIndicationModeAllCommandParameters {

    @Parameter(names = "--mode", description = "Indication mode", required = true)
    private IndicationMode indicationMode;

    public IndicationMode getIndicationMode() {
      return indicationMode;
    }
  }

  private static void printError(final String prefixMessage, final Thread thread, final Exception e, final boolean verbose) {
    System.out.println(prefixMessage + e.getMessage());
    if (verbose) {
      System.err.println("EXCEPTION in Thread: " + thread.getName() + " (" + thread.toString() + ")");
      e.printStackTrace(System.err);
    }
  }

}
