/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.flamesgroup.device.DeviceClass;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.channel.ChannelInvalidCommandDataException;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.lang.invoke.MethodHandles;

public class BOXLogReader {

  private static final String READ_JOURNAL_ALL_COMMAND = "readjournalall";
  private static final String[] READ_JOURNAL_ALL_COMMAND_ALIASES = {"rja"};
  private static final String ERASE_JOURNAL_ALL_COMMAND = "erasejournalall";
  private static final String[] ERASE_JOURNAL_ALL_COMMAND_ALIASES = {"eja"};

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final CommonParameters commonParameters = new CommonParameters();
    final ReadJournalAllCommandParameters readJournalAllCommandParameters = new ReadJournalAllCommandParameters();
    final EraseJournalAllCommandParameters eraseJournalAllCommandParameters = new EraseJournalAllCommandParameters();

    clp.addObject(commonParameters);
    clp.addCommand(READ_JOURNAL_ALL_COMMAND, readJournalAllCommandParameters, READ_JOURNAL_ALL_COMMAND_ALIASES);
    clp.addCommand(ERASE_JOURNAL_ALL_COMMAND, eraseJournalAllCommandParameters, ERASE_JOURNAL_ALL_COMMAND_ALIASES);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else if (clp.getParsedCommand() == null) {
        throw new ParameterException("Command undefined");
      } else if (DeviceUtil.getDeviceClass() != DeviceClass.SPI) {
        throw new ParameterException("Can execute only on device class " + DeviceClass.SPI);
      } else {
        DeviceUID deviceUID = BOXUtil.readDeviceUID();
        System.out.println("device.uid=" + deviceUID.getCanonicalName());
        IDevice device = DeviceUtil.createSpiDevice();
        device.attach();
        try {
          if (READ_JOURNAL_ALL_COMMAND.equals(clp.getParsedCommand())) {
            readJournalAll(device);
          } else if (ERASE_JOURNAL_ALL_COMMAND.equals(clp.getParsedCommand())) {
            eraseJournalAll(device);
          } else {
            throw new AssertionError();
          }
        } finally {
          device.detach();
        }
      }
    }
  }

  private static void readJournalAll(final IDevice device) throws Exception {
    for (final ICPUEntry cpuEntry : device.getCPUEntries()) {
      ILoggerChannel loggerChannel = cpuEntry.getLoggerChannel();
      loggerChannel.enslave();
      System.out.println("Start read journal on CPU " + cpuEntry.getNumber());
      short recordNumber = 0;
      try {
        while (true) {
          System.out.println(loggerChannel.readJournalRecord(recordNumber++));
        }
      } catch (ChannelInvalidCommandDataException ignored) {
        System.out.println("End read journal on CPU " + cpuEntry.getNumber());
      }
      loggerChannel.free();
    }
  }

  private static void eraseJournalAll(final IDevice device) throws Exception {
    for (final ICPUEntry cpuEntry : device.getCPUEntries()) {
      ILoggerChannel loggerChannel = cpuEntry.getLoggerChannel();
      loggerChannel.enslave();
      System.out.println("Erase journal on CPU " + cpuEntry.getNumber());
      loggerChannel.eraseJournal();
      loggerChannel.free();
    }
  }

  @Parameters(commandDescription = "Read all journal records of all CPUs")
  private static class ReadJournalAllCommandParameters {
  }

  @Parameters(commandDescription = "Erase all journal records of all CPUs")
  private static class EraseJournalAllCommandParameters {
  }

}
