/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParametersDelegate;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.protocol.mudp.MudpConnection;
import com.flamesgroup.device.tool.parameter.BoardIPMainParameter;
import com.flamesgroup.device.tool.parameter.CommonParameters;
import com.flamesgroup.device.tool.parameter.GSMEntryParameter;

import java.lang.invoke.MethodHandles;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public final class BoardATTerminal extends BaseATTerminal {

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    final BoardIPMainParameter mainParameter = new BoardIPMainParameter();
    final GSMEntryParameter gsmEntryParameter = new GSMEntryParameter();

    final ATTerminalCommonParameters commonParameters = new ATTerminalCommonParameters(mainParameter, gsmEntryParameter);

    clp.addObject(commonParameters);

    Thread.setDefaultUncaughtExceptionHandler(new ToolUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      clp.usage();
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        clp.usage();
      } else {
        InetAddress boardIP = mainParameter.getBoardIP();
        IGSMDevice gsmDevice = DeviceUtil.createEthGSMDevice(new InetSocketAddress(boardIP, MudpConnection.MUDP_DEFAULT_PORT));

        main(gsmDevice, gsmEntryParameter.getNumber());
      }
    }
  }

  public static class ATTerminalCommonParameters extends CommonParameters {
    @ParametersDelegate
    private final BoardIPMainParameter mainParameter;

    @ParametersDelegate
    private final GSMEntryParameter gsmEntryParameter;

    public ATTerminalCommonParameters(final BoardIPMainParameter mainParameter, final GSMEntryParameter gsmEntryParameter) {
      this.mainParameter = mainParameter;
      this.gsmEntryParameter = gsmEntryParameter;
    }
  }

}
