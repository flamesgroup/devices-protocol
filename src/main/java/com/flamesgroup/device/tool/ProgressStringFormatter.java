/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

public final class ProgressStringFormatter {

  private static final String[] progressStrings = new String[] {"-", "\\", "|", "/"};

  private final int progressSpeedDivider;

  private int progressCounter = 0;

  public ProgressStringFormatter(final int progressSpeedDivider) {
    if (progressSpeedDivider <= 0) {
      throw new IllegalArgumentException("progressSpeedDivider can't be less or equal 0");
    }
    this.progressSpeedDivider = progressSpeedDivider;
  }

  public ProgressStringFormatter() {
    this(1);
  }

  public String progress() {
    int progressCharsIndex = (progressCounter++ / progressSpeedDivider) % progressStrings.length;
    return progressStrings[progressCharsIndex];
  }

}
