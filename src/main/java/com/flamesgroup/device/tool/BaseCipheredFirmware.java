/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.flamesgroup.device.commonb.CipheredFWBlock;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

public abstract class BaseCipheredFirmware {

  private final List<CipheredFWBlock> cipheredFWBlocks;

  public BaseCipheredFirmware(final List<CipheredFWBlock> cipheredFWBlocks) {
    this.cipheredFWBlocks = cipheredFWBlocks;
  }

  public List<CipheredFWBlock> getCipheredFWBlocks() {
    return cipheredFWBlocks;
  }

  protected static List<CipheredFWBlock> readCipheredFWBlocks(final ByteBuffer firmwareBb) {
    if (firmwareBb.remaining() % CipheredFWBlock.LENGTH != 0) {
      throw new IllegalArgumentException("Ciphered firmware data malformed - incorrect size");
    }

    List<CipheredFWBlock> cipheredFWBlocks = new LinkedList<>();

    while (firmwareBb.hasRemaining()) {
      CipheredFWBlock cipheredFWBlock = new CipheredFWBlock();
      firmwareBb.get(cipheredFWBlock.getIV());
      firmwareBb.get(cipheredFWBlock.getData());
      cipheredFWBlocks.add(cipheredFWBlock);
    }

    return cipheredFWBlocks;
  }

}
