/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.tool;

import com.beust.jcommander.ParameterException;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.gsmb.IRawATSubChannel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public abstract class BaseATTerminal {

  private static final char CR = 0x0D;
  private static final char CTRLZ = 0x1A;
  private static final char ESC = 0x1B;

  private static final String EXIT_STRING = "EXIT";
  private static final String CTRLZ_STRING = "CTRLZ";
  private static final String ESC_STRING = "ESC";

  private static final Charset atCharset = Charset.forName("ISO-8859-1");

  protected static void main(final IGSMDevice gsmDevice, final int gsmEntryNumber) throws Exception {
    gsmDevice.attach();
    try {
      if (gsmEntryNumber < 0) {
        throw new ParameterException("Entry number can't be negative");
      } else if (gsmEntryNumber >= gsmDevice.getGSMEntryCount()) {
        throw new ParameterException("Entry number can't be greater or equals than " + gsmDevice.getGSMEntryCount());
      }

      IGSMEntry gsmEntry = gsmDevice.getGSMEntries().get(gsmEntryNumber);
      IGSMChannel gsmChannel = gsmEntry.getGSMChannel();
      gsmChannel.enslave();
      try {
        IRawATSubChannel rawATSubChannel = gsmChannel.getRawATSubChannel();
        rawATSubChannel.start(b -> System.out.print(atCharset.decode(b)));
        try {
          System.out.println("AT Terminal on " + gsmEntry);
          System.out.println("(special strings: \"EXIT\" to exit; \"CTRLZ\" and \"ESC\" at the end replaced with proper char)");

          BufferedReader atReader = new BufferedReader(new InputStreamReader(System.in));
          while (true) {
            String command = atReader.readLine();

            // After pressing 'Ctrl + C + C', readLine() method returns null
            if (command == null) {
              break;
            }

            if (command.isEmpty()) {
              continue;
            } else if (command.toUpperCase().equals(EXIT_STRING)) {
              break;
            } else if (command.toUpperCase().endsWith(CTRLZ_STRING)) {
              command = command.substring(0, command.length() - CTRLZ_STRING.length()) + CTRLZ;
            } else if (command.toUpperCase().endsWith(ESC_STRING)) {
              command = command.substring(0, command.length() - ESC_STRING.length()) + ESC;
            }

            rawATSubChannel.writeRawATData(atCharset.encode(command + CR));
          }

        } finally {
          rawATSubChannel.stop();
        }
      } finally {
        gsmChannel.free();
      }
    } finally {
      gsmDevice.detach();
    }
  }

}
