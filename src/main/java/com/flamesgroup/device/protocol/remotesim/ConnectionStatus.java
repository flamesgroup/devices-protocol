/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

public enum ConnectionStatus {

  OK_CAN_FULFILL_REQUIREMENTS((byte) 0),
  ERROR_UNABLE_TO_ESTABLISH_CONNECTION((byte) 1),
  ERROR_DOES_NOT_SUPPORT_MAX_MESSAGE_SIZE((byte) 2),
  ERROR_MAX_MESSAGE_SIZE_TOO_SMALL((byte) 3),
  OK_ONGOING_CALL((byte) 4);

  private final byte status;

  ConnectionStatus(final byte status) {
    this.status = status;
  }

  public byte getStatus() {
    return status;
  }

  public static ConnectionStatus valueOf(final byte status) {
    short statusUnsigned = (short) (status & 0xFF);
    if (statusUnsigned >= 0 && statusUnsigned < connectionStatuses.length) {
      ConnectionStatus connectionStatus = connectionStatuses[statusUnsigned];
      if (connectionStatus != null) {
        return connectionStatus;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported %s type: 0x%02X", ConnectionStatus.class.getSimpleName(), statusUnsigned));
  }

  private static final ConnectionStatus[] connectionStatuses;

  static {
    short maxStatus = 0;
    for (ConnectionStatus connectionStatus : values()) {
      short statusUnsigned = (short) (connectionStatus.status & 0xFF);
      if (statusUnsigned > maxStatus) {
        maxStatus = statusUnsigned;
      }
    }
    connectionStatuses = new ConnectionStatus[maxStatus + 1];
    for (ConnectionStatus connectionStatus : values()) {
      short statusUnsigned = (short) (connectionStatus.status & 0xFF);
      connectionStatuses[statusUnsigned] = connectionStatus;
    }
  }

}
