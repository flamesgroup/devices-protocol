/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

public enum DisconnectionType {

  GRACEFUL((byte) 0),
  IMMEDIATE((byte) 1);

  private final byte type;

  DisconnectionType(final byte type) {
    this.type = type;
  }

  public byte getType() {
    return type;
  }

  public static DisconnectionType valueOf(final byte type) {
    short typeUnsigned = (short) (type & 0xFF);
    if (typeUnsigned >= 0 && typeUnsigned < disconnectionTypes.length) {
      DisconnectionType disconnectionType = disconnectionTypes[typeUnsigned];
      if (disconnectionType != null) {
        return disconnectionType;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported %s type: 0x%02X", DisconnectionType.class.getSimpleName(), typeUnsigned));
  }

  private static final DisconnectionType[] disconnectionTypes;

  static {
    short maxType = 0;
    for (DisconnectionType disconnectionType : values()) {
      short typeUnsigned = (short) (disconnectionType.type & 0xFF);
      if (typeUnsigned > maxType) {
        maxType = typeUnsigned;
      }
    }
    disconnectionTypes = new DisconnectionType[maxType + 1];
    for (DisconnectionType disconnectionType : values()) {
      short typeUnsigned = (short) (disconnectionType.type & 0xFF);
      disconnectionTypes[typeUnsigned] = disconnectionType;
    }
  }

}
