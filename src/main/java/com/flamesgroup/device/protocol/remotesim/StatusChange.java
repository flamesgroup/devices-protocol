/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

public enum StatusChange {

  UNKNOWN_ERROR((byte) 0),
  CARD_RESET((byte) 1),
  CARD_NOT_ACCESSIBLE((byte) 2),
  CARD_REMOVED((byte) 3),
  CARD_INSERTED((byte) 4),
  CARD_RECOVERED((byte) 5);

  private final byte status;

  StatusChange(final byte status) {
    this.status = status;
  }

  public byte getStatus() {
    return status;
  }

  public static StatusChange valueOf(final byte status) {
    short statusUnsigned = (short) (status & 0xFF);
    if (statusUnsigned >= 0 && statusUnsigned < statusChanges.length) {
      StatusChange statusChange = statusChanges[statusUnsigned];
      if (statusChange != null) {
        return statusChange;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported %s type: 0x%02X", StatusChange.class.getSimpleName(), statusUnsigned));
  }

  private static final StatusChange[] statusChanges;

  static {
    short maxStatus = 0;
    for (StatusChange statusChange : values()) {
      short statusUnsigned = (short) (statusChange.status & 0xFF);
      if (statusUnsigned > maxStatus) {
        maxStatus = statusUnsigned;
      }
    }
    statusChanges = new StatusChange[maxStatus + 1];
    for (StatusChange statusChange : values()) {
      short statusUnsigned = (short) (statusChange.status & 0xFF);
      statusChanges[statusUnsigned] = statusChange;
    }
  }

}
