/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

public enum ResultCode {

  OK((byte) 0),
  ERROR_NO_REASON_DEFINED((byte) 1),
  ERROR_CARD_NO_ACCESSIBLE((byte) 2),
  ERROR_CARD_ALREADY_POWERED_OFF((byte) 3),
  ERROR_CARD_REMOVED((byte) 4),
  ERROR_CARD_ALREADY_POWERED_ON((byte) 5),
  ERROR_DATA_NOT_AVAILABLE((byte) 6),
  ERROR_NOT_SUPPORTED((byte) 7);

  private final byte code;

  ResultCode(final byte code) {
    this.code = code;
  }

  public byte getCode() {
    return code;
  }

  public static ResultCode valueOf(final byte code) {
    short codeUnsigned = (short) (code & 0xFF);
    if (codeUnsigned >= 0 && codeUnsigned < resultCodes.length) {
      ResultCode resultCode = resultCodes[codeUnsigned];
      if (resultCode != null) {
        return resultCode;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported %s type: 0x%02X", ResultCode.class.getSimpleName(), codeUnsigned));
  }

  private static final ResultCode[] resultCodes;

  static {
    short maxCode = 0;
    for (ResultCode resultCode : values()) {
      short codeUnsigned = (short) (resultCode.code & 0xFF);
      if (codeUnsigned > maxCode) {
        maxCode = codeUnsigned;
      }
    }
    resultCodes = new ResultCode[maxCode + 1];
    for (ResultCode resultCode : values()) {
      short codeUnsigned = (short) (resultCode.code & 0xFF);
      resultCodes[codeUnsigned] = resultCode;
    }
  }

}
