/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

import java.util.Arrays;

public class MessageParameter {

  private final ID id;
  private final byte[] value;

  public MessageParameter(final ID id, final byte[] value) {
    this.id = id;
    this.value = value;
  }

  public ID getId() {
    return id;
  }

  public byte[] getValue() {
    return value;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof MessageParameter) {
      MessageParameter messageParameter = (MessageParameter) obj;
      return id == messageParameter.id && Arrays.equals(value, messageParameter.value);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return id.hashCode() << 24 + Arrays.hashCode(value);
  }

  public enum ID {

    MAX_MSG_SIZE((byte) 0x00),
    CONNECTION_STATUS((byte) 0x01),
    RESULT_CODE((byte) 0x02),
    DISCONNECTION_TYPE((byte) 0x03),
    COMMAND_APDU((byte) 0x04),
    COMMAND_APDU_7816((byte) 0x10),
    RESPONSE_APDU((byte) 0x05),
    ATR((byte) 0x06),
    CARD_READER_STATUS((byte) 0x07),
    STATUS_CHANGE((byte) 0x08),
    TRANSPORT_PROTOCOL((byte) 0x09);

    private final byte id;

    ID(final byte id) {
      this.id = id;
    }

    public byte getID() {
      return id;
    }

    public static ID valueOf(final byte id) {
      short idUnsigned = (short) (id & 0xFF);
      if (idUnsigned >= 0 && idUnsigned < parameterIDs.length) {
        ID parameterID = parameterIDs[idUnsigned];
        if (parameterID != null) {
          return parameterID;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported %s.%s type: 0x%02X", MessageParameter.class.getSimpleName(), ID.class.getSimpleName(), idUnsigned));
    }

    private static final ID[] parameterIDs;

    static {
      short maxId = 0;
      for (ID parameterID : values()) {
        short idUnsigned = (short) (parameterID.id & 0xFF);
        if (idUnsigned > maxId) {
          maxId = idUnsigned;
        }
      }
      parameterIDs = new ID[maxId + 1];
      for (ID parameterID : values()) {
        short idUnsigned = (short) (parameterID.id & 0xFF);
        parameterIDs[idUnsigned] = parameterID;
      }
    }

  }

}
