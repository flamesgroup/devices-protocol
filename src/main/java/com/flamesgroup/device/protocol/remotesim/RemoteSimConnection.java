/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPort;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPortHandler;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.Thread.UncaughtExceptionHandler;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RemoteSimConnection implements IRemoteSimConnection, ICMUXVirtualPortHandler {

  private final Logger logger = LoggerFactory.getLogger(RemoteSimConnection.class);

  private final ICMUXVirtualPort virtualPort;

  private final Lock connectionLock = new ReentrantLock();

  private volatile RemoteSimConnectionThread connectionThread;

  private final BlockingQueue<Message> receivedSAPMessages = new LinkedBlockingQueue<>();

  private ATR connectionATR;
  private IRemoteSimConnectionHandler connectionHandler;

  private static final char CR = 0x0D;
  private static final char LF = 0x0A;

  /*
   * Header size - 4
   * Maximum number of parameters - 2
   * Maximum length of a parameter - 2^16(65535)
   * (Maximum number of parameters) * ((Header size) + (4 + Maximum length + 1(padding)))
   * 2 * (4 + 4 + (65535 + 1)) = 131088
   * To be sure multiply 2 = 2 * 131088 = 262176
   */
  private final ByteBuffer byteBufferToReceive = ByteBuffer.allocate(131088 * 2).order(ByteOrder.BIG_ENDIAN);
  private final Lock byteBufferToReceiveLock = new ReentrantLock();

  private final ByteBuffer byteBufferToTransmit = ByteBuffer.allocate(131088 * 2).order(ByteOrder.BIG_ENDIAN);
  private final Lock byteBufferToTransmitLock = new ReentrantLock();

  private final String toS;

  public RemoteSimConnection(final ICMUXVirtualPort virtualPort) {
    Objects.requireNonNull(virtualPort, "virtualPort must not be null");
    this.virtualPort = virtualPort;

    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}]", toS, virtualPort);
    }
  }

  // IRemoteSimConnection

  @Override
  public boolean isConnected() {
    return connectionThread != null;
  }

  @Override
  public void connect(final ATR atr, final IRemoteSimConnectionHandler remoteSimConnectionHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(atr, "atr must not be null");
    Objects.requireNonNull(remoteSimConnectionHandler, "remoteSimHandler must not be null");

    logger.debug("[{}] - connecting", this);
    connectionLock.lock();
    try {
      if (isConnected()) {
        throw new IllegalStateException("[" + this + "] already enabled");
      }

      connectionThread = new RemoteSimConnectionThread();
      connectionThread.setDaemon(true);
      connectionThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(final Thread t, final Throwable e) {
          logger.error(String.format("[%s] - get uncaught Exception", t.getName()), e);
        }
      });
      connectionThread.start();

      connectionATR = atr;
      connectionHandler = remoteSimConnectionHandler;

      virtualPort.open(this);
    } finally {
      connectionLock.unlock();
    }
    logger.debug("[{}] - connected", this);
  }

  @Override
  public void disconnect() throws ChannelException, InterruptedException {
    logger.debug("[{}] - disconnecting", this);
    connectionLock.lock();
    try {
      virtualPort.close();

      connectionHandler = null;

      Thread localConnectionThread = connectionThread;
      connectionThread = null;
      receivedSAPMessages.put(new Message(null)); // put empty Message to wake up connection thread
      localConnectionThread.join();
    } finally {
      connectionLock.unlock();
    }
    logger.debug("[{}] - disconnected", this);
  }

  @Override
  public void transferAPDUResponse(final APDUResponse apduResponse) throws ChannelException, InterruptedException {
    sendTransferAPDUResponse(ResultCode.OK, apduResponse);
  }

  // ICMUXVirtualPortHandler

  @Override
  public void handleData(final ByteBuffer data) {
    byteBufferToReceiveLock.lock();
    try {
      byteBufferToReceive.put(data);
      byteBufferToReceive.flip();
      do {
        try {
          byteBufferToReceive.mark();

          Message sapMessageRequest = Message.decoding(byteBufferToReceive);
          receivedSAPMessages.offer(sapMessageRequest);
        } catch (BufferUnderflowException e) {
          byteBufferToReceive.reset();
          break;
        } catch (MessageCodingException e) {
          logger.warn("Can't encode SAPClient request", e);
          break;
        }
      } while (true);
      byteBufferToReceive.compact();
    } finally {
      byteBufferToReceiveLock.unlock();
    }
  }

  // Object

  @Override
  public String toString() {
    return toS;
  }

  // internal functionality

  private void sendSAPMessage(final Message message) throws ChannelException, InterruptedException {
    byteBufferToTransmitLock.lock();
    byteBufferToTransmit.clear();
    try {
      Message.encoding(message, byteBufferToTransmit);
      byteBufferToTransmit.flip();

      virtualPort.sendData(byteBufferToTransmit);
    } finally {
      byteBufferToTransmitLock.unlock();
    }
  }

  private void sendConnectionResponse(final ConnectionStatus status, final short maxSizeMessageSize) throws ChannelException, InterruptedException {
    connectionLock.lock();
    try {
      Message message;
      MessageParameter messageParameter;

      message = new Message(Message.ID.CONNECTION_RESP);
      messageParameter = new MessageParameter(MessageParameter.ID.CONNECTION_STATUS, new byte[] {status.getStatus()});
      message.getParameters().add(messageParameter);
      if (status != ConnectionStatus.OK_CAN_FULFILL_REQUIREMENTS && status != ConnectionStatus.OK_ONGOING_CALL) {
        messageParameter = new MessageParameter(MessageParameter.ID.MAX_MSG_SIZE, new byte[] {(byte) (maxSizeMessageSize >> 8), (byte) (maxSizeMessageSize)});
        message.getParameters().add(messageParameter);
      }

      sendSAPMessage(message);
    } finally {
      connectionLock.unlock();
    }
  }

  private void sendDisconnectionResponse() throws ChannelException, InterruptedException {
    connectionLock.lock();
    try {
      Message message = new Message(Message.ID.DISCONNECTION_RESP);

      sendSAPMessage(message);
    } finally {
      connectionLock.unlock();
    }
  }

  public void sendTransferAPDUResponse(final ResultCode resultCode, final APDUResponse apduResponse) throws ChannelException, InterruptedException {
    Objects.requireNonNull(resultCode, "resultCode must not be null");
    Objects.requireNonNull(apduResponse, "apduResponse must not be null");

    connectionLock.lock();
    try {
      Message message;
      MessageParameter messageParameter;

      message = new Message(Message.ID.TRANSFER_APDU_RESP);
      messageParameter = new MessageParameter(MessageParameter.ID.RESULT_CODE, new byte[] {resultCode.getCode()});
      message.getParameters().add(messageParameter);
      if (resultCode == ResultCode.OK) {
        byte[] messageParameterValue = new byte[apduResponse.getLength()];
        {
          final ByteBuffer bb = ByteBuffer.wrap(messageParameterValue);
          APDUResponse.encoding(apduResponse, bb);
        }
        messageParameter = new MessageParameter(MessageParameter.ID.RESPONSE_APDU, messageParameterValue);
        message.getParameters().add(messageParameter);
      }

      sendSAPMessage(message);
    } finally {
      connectionLock.unlock();
    }
  }

  public void sendTransferATRResponse(final ResultCode resultCode, final ATR atr) throws ChannelException, InterruptedException {
    Objects.requireNonNull(resultCode, "ResultCode must not be null");
    Objects.requireNonNull(atr, "Atr must not be null");

    connectionLock.lock();
    try {
      Message message;
      MessageParameter messageParameter;

      message = new Message(Message.ID.TRANSFER_ATR_RESP);
      messageParameter = new MessageParameter(MessageParameter.ID.RESULT_CODE, new byte[] {resultCode.getCode()});
      message.getParameters().add(messageParameter);
      if (resultCode == ResultCode.OK) {
        messageParameter = new MessageParameter(MessageParameter.ID.ATR, Arrays.copyOf(atr.getData(), atr.getData().length));
        message.getParameters().add(messageParameter);
      }

      sendSAPMessage(message);
    } finally {
      connectionLock.unlock();
    }
  }

  private void sendPowerSimOnResponse(final ResultCode resultCode) throws ChannelException, InterruptedException {
    connectionLock.lock();
    try {
      Message message;
      MessageParameter messageParameter;

      message = new Message(Message.ID.POWER_SIM_ON_RESP);
      messageParameter = new MessageParameter(MessageParameter.ID.RESULT_CODE, new byte[] {resultCode.getCode()});
      message.getParameters().add(messageParameter);

      sendSAPMessage(message);
    } finally {
      connectionLock.unlock();
    }
  }

  private void sendStatusIndication(final StatusChange status) throws ChannelException, InterruptedException {
    connectionLock.lock();
    try {
      Message message;
      MessageParameter messageParameter;

      message = new Message(Message.ID.STATUS_IND);
      messageParameter = new MessageParameter(MessageParameter.ID.STATUS_CHANGE, new byte[] {status.getStatus()});
      message.getParameters().add(messageParameter);

      sendSAPMessage(message);
    } finally {
      connectionLock.unlock();
    }
  }

  public void handleConnectionRequest(final Message sapMessageRequestConnection) throws ChannelException, InterruptedException {
    if (sapMessageRequestConnection.getParameters().size() != 1) {
      throw new RemoteSimException(String.format("Wrong parameters number %d, must be %d",
          sapMessageRequestConnection.getParameters().size(), 1));
    }
    if (sapMessageRequestConnection.getParameters().get(0).getId() != MessageParameter.ID.MAX_MSG_SIZE) {
      throw new RemoteSimException(String.format("Wrong parameter ID %s, must be %s",
          sapMessageRequestConnection.getParameters().get(0).getId(), MessageParameter.ID.MAX_MSG_SIZE));
    }
    sendConnectionResponse(ConnectionStatus.OK_ONGOING_CALL, (short) 300);
    sendStatusIndication(StatusChange.CARD_INSERTED);
  }

  public void handleDisconnectionRequest(final Message sapMessageRequestDisconnection) throws ChannelException, InterruptedException {
    if (sapMessageRequestDisconnection.getParameters().size() != 0) {
      throw new RemoteSimException(String.format("Wrong parameters number %d, must be %d",
          sapMessageRequestDisconnection.getParameters().size(), 0));
    }
    sendDisconnectionResponse();
  }

  public void handleTransferAPDURequest(final Message sapMessageRequestTransferAPDU) throws ChannelException, InterruptedException {
    if (sapMessageRequestTransferAPDU.getParameters().size() != 1) {
      throw new RemoteSimException(String.format("Wrong parameters number %d, must be %d",
          sapMessageRequestTransferAPDU.getParameters().size(), 1));
    }
    if (sapMessageRequestTransferAPDU.getParameters().get(0).getId() != MessageParameter.ID.COMMAND_APDU) {
      throw new RemoteSimException(String.format("Wrong parameter ID %s, must be %s",
          sapMessageRequestTransferAPDU.getParameters().get(0).getId(), MessageParameter.ID.COMMAND_APDU));
    }
    final ByteBuffer bb = ByteBuffer.wrap(sapMessageRequestTransferAPDU.getParameters().get(0).getValue());
    APDUCommand apduCommand = APDUCommand.decoding(bb);
    
    IRemoteSimConnectionHandler connectionHandlerLocal;
    connectionLock.lock();
    try {
        connectionHandlerLocal = connectionHandler;
    } finally {
      connectionLock.unlock();
    }
    
    if (connectionHandlerLocal != null) {
        connectionHandlerLocal.handleAPDUCommand(apduCommand);
    }
  }

  public void handleTransferATRRequest(final Message sapMessageRequestTransferATR) throws ChannelException, InterruptedException {
    if (sapMessageRequestTransferATR.getParameters().size() != 0) {
      throw new RemoteSimException(String.format("Wrong parameters number %d, must be %d",
          sapMessageRequestTransferATR.getParameters().size(), 0));
    }
    sendTransferATRResponse(ResultCode.OK, connectionATR);
  }

  public void handlePowerSimOnRequest(final Message sapMessageRequestPowerSimOn) throws ChannelException, InterruptedException {
    if (sapMessageRequestPowerSimOn.getParameters().size() != 0) {
      throw new RemoteSimException(String.format("Wrong parameters number %d, must be %d",
          sapMessageRequestPowerSimOn.getParameters().size(), 0));
    }
    sendPowerSimOnResponse(ResultCode.OK);
  }

  // internal thread

  private class RemoteSimConnectionThread extends Thread {

    public RemoteSimConnectionThread() {
      super("RemoteSimConnectionThread (" + virtualPort + ")");
    }

    @Override
    public void run() {
      Thread thisThread = Thread.currentThread();
      while (true) {
        Message message;
        try {
          message = receivedSAPMessages.take();
        } catch (InterruptedException e) {
          logger.warn("[{}] - current thread has been interrupted", this);
          break;
        }

        if (thisThread != connectionThread) { // polite thread stop
          break;
        }

        try {
          switch (message.getId()) {
            case CONNECTION_REQ: {
              handleConnectionRequest(message);
              break;
            }
            case DISCONNECTION_REQ: {
              handleDisconnectionRequest(message);
              break;
            }
            case TRANSFER_APDU_REQ: {
              handleTransferAPDURequest(message);
              break;
            }
            case TRANSFER_ATR_REQ: {
              handleTransferATRRequest(message);
              break;
            }
            case POWER_SIM_ON_REQ: {
              handlePowerSimOnRequest(message);
              break;
            }
            default: {
              logger.warn("[{}] - unknown msgID [{}]", this, message.getId());
            }
          }
        } catch (InterruptedException e) {
          logger.warn("[" + this + "] - current thread has been interrupted", e);
          break;
        } catch (ChannelException e) {
          logger.warn("[" + this + "] - unexpected ChannelException", e);
        }
      }
    }

  }

}
