/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Message {

  private final ID id;
  private final List<MessageParameter> parameters = new ArrayList<>(2); // where 2 is maximum parameters number form doc

  public Message(final ID id) {
    this.id = id;
  }

  public ID getId() {
    return id;
  }

  public List<MessageParameter> getParameters() {
    return parameters;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof Message) {
      Message message = (Message) obj;
      return id == message.id && parameters.equals(message.parameters);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return id.hashCode() << 24 + parameters.hashCode();
  }

  public enum ID {

    CONNECTION_REQ((byte) 0x00, true),
    CONNECTION_RESP((byte) 0x01, false),
    DISCONNECTION_REQ((byte) 0x02, true),
    DISCONNECTION_RESP((byte) 0x03, false),
    DISCONNECTION_IND((byte) 0x04, false),
    TRANSFER_APDU_REQ((byte) 0x05, true),
    TRANSFER_APDU_RESP((byte) 0x06, false),
    TRANSFER_ATR_REQ((byte) 0x07, true),
    TRANSFER_ATR_RESP((byte) 0x08, false),
    POWER_SIM_OFF_REQ((byte) 0x09, true),
    POWER_SIM_OFF_RESP((byte) 0x0A, false),
    POWER_SIM_ON_REQ((byte) 0x0B, true),
    POWER_SIM_ON_RESP((byte) 0x0C, false),
    RESET_SIM_REQ((byte) 0x0D, true),
    RESET_SIM_RESP((byte) 0x0E, false),
    TRANSFER_CARD_READER_STATUS_REQ((byte) 0x0F, true),
    TRANSFER_CARD_READER_STATUS_RESP((byte) 0x10, false),
    STATUS_IND((byte) 0x11, false),
    ERROR_RESP((byte) 0x12, false),
    SET_TRANSPORT_PROTOCOL_REQ((byte) 0x13, true),
    SET_TRANSPORT_PROTOCOL_RESP((byte) 0x14, false);

    private final byte id;
    private final boolean request;

    ID(final byte id, final boolean request) {
      this.id = id;
      this.request = request;
    }

    public byte getID() {
      return id;
    }

    public boolean isRequest() {
      return request;
    }

    public boolean isResponse() {
      return !request;
    }

    public static ID valueOf(final byte id) {
      short idUnsigned = (short) (id & 0xFF);
      if (idUnsigned >= 0 && idUnsigned < messageIDs.length) {
        ID messageID = messageIDs[idUnsigned];
        if (messageID != null) {
          return messageID;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported %s.%s type: 0x%02X", Message.class.getSimpleName(), ID.class.getSimpleName(), idUnsigned));
    }

    private static final ID[] messageIDs;

    static {
      short maxId = 0;
      for (ID messageID : values()) {
        short idUnsigned = (short) (messageID.id & 0xFF);
        if (idUnsigned > maxId) {
          maxId = idUnsigned;
        }
      }
      messageIDs = new ID[maxId + 1];
      for (ID messageID : values()) {
        short idUnsigned = (short) (messageID.id & 0xFF);
        messageIDs[idUnsigned] = messageID;
      }
    }

  }

  public static Message decoding(final ByteBuffer buffer) throws MessageCodingException {
    try {
      Message message = new Message(ID.valueOf(buffer.get()));

      int numberOfParameters = 0xFF & buffer.get();

      buffer.getShort(); // Reserved

      while (numberOfParameters-- > 0) {
        MessageParameter.ID parameterID = MessageParameter.ID.valueOf(buffer.get());

        buffer.get(); // Reserved

        int parameterLength = buffer.getShort() & 0xFFFF;
        byte[] parameterValue = new byte[parameterLength];
        buffer.get(parameterValue);
        MessageParameter messageParameter = new MessageParameter(parameterID, parameterValue);

        // Padding Bytes
        int paddingBytesLength = calculatePaddingBytesLength(parameterLength);
        while (paddingBytesLength-- > 0) {
          buffer.get();
        }

        message.getParameters().add(messageParameter);
      }

      return message;
    } catch (IllegalArgumentException e) {
      throw new MessageCodingException(e);
    }
  }

  public static void encoding(final Message message, final ByteBuffer buffer) throws MessageCodingException {
    try {
      buffer.put(message.getId().getID());

      buffer.put((byte) message.getParameters().size());

      buffer.putShort((short) 0); // Reserved

      for (MessageParameter messageParameter : message.getParameters()) {
        buffer.put(messageParameter.getId().getID());

        buffer.put((byte) 0); // Reserved

        int parameterLength = messageParameter.getValue().length;
        buffer.putShort((short) parameterLength);

        buffer.put(messageParameter.getValue());

        // Padding Bytes
        int paddingBytesLength = calculatePaddingBytesLength(parameterLength);
        while (paddingBytesLength-- > 0) {
          buffer.put((byte) 0);
        }
      }
    } finally {
    }
  }

  private static int calculatePaddingBytesLength(final int parameterLength) {
    int remainder = parameterLength % 4;
    if (remainder == 0) {
      return 0;
    } else {
      return 4 - remainder;
    }
  }

}
