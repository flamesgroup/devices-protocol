/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

public enum FrameType {

  SAMB((byte) 0b00101111),
  UA((byte) 0b01100011),
  DM((byte) 0b00001111),
  DISC((byte) 0b01000011),
  UIH((byte) 0b11101111);

  private final byte type;

  FrameType(final byte type) {
    this.type = type;
  }

  public byte getType() {
    return type;
  }

  public static FrameType valueOf(final byte type) {
    short typeUnsigned = (short) (type & 0xFF);
    if (typeUnsigned >= 0 && typeUnsigned < frameTypes.length) {
      FrameType frameType = frameTypes[typeUnsigned];
      if (frameType != null) {
        return frameType;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported FrameType type: 0x%02X", typeUnsigned));
  }

  private static final FrameType[] frameTypes;

  static {
    short maxType = 0;
    for (FrameType frameType : values()) {
      short typeUnsigned = (short) (frameType.type & 0xFF);
      if (typeUnsigned > maxType) {
        maxType = typeUnsigned;
      }
    }
    frameTypes = new FrameType[maxType + 1];
    for (FrameType frameType : values()) {
      short typeUnsigned = (short) (frameType.type & 0xFF);
      frameTypes[typeUnsigned] = frameType;
    }
  }

}
