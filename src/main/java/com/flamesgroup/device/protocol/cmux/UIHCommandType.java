/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

public enum UIHCommandType {

  NSC((byte) 0x04),
  TEST((byte) 0x08),
  PSC((byte) 0x10),
  CLD((byte) 0x30),
  MSC((byte) 0x38);

  private final byte type;

  UIHCommandType(final byte type) {
    this.type = type;
  }

  public byte getType() {
    return type;
  }

  public static UIHCommandType valueOf(final byte type) {
    short typeUnsigned = (short) (type & 0xFF);
    if (typeUnsigned >= 0 && typeUnsigned < uihControlCommandTypes.length) {
      UIHCommandType uihControlCommandType = uihControlCommandTypes[typeUnsigned];
      if (uihControlCommandType != null) {
        return uihControlCommandType;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported UIHCommandType type: 0x%02X", typeUnsigned));
  }

  private static final UIHCommandType[] uihControlCommandTypes;

  static {
    short maxType = 0;
    for (UIHCommandType uihControlCommandType : values()) {
      short typeUnsigned = (short) (uihControlCommandType.type & 0xFF);
      if (typeUnsigned > maxType) {
        maxType = typeUnsigned;
      }
    }
    uihControlCommandTypes = new UIHCommandType[maxType + 1];
    for (UIHCommandType uihControlCommandType : values()) {
      short typeUnsigned = (short) (uihControlCommandType.type & 0xFF);
      uihControlCommandTypes[typeUnsigned] = uihControlCommandType;
    }
  }

}
