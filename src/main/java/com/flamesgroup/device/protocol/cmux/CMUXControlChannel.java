/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import com.flamesgroup.device.channel.ChannelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public final class CMUXControlChannel extends CMUXChannel {

  private final static Logger logger = LoggerFactory.getLogger(CMUXControlChannel.class);

  public static final byte ADDRESS_DLCI = (byte) 0;

  private UIHCommandMSC responseUIHCommandMSC;

  public CMUXControlChannel(final CMUXConnection cmuxConnection, final long timeout) {
    super(cmuxConnection, ADDRESS_DLCI, timeout);
  }

  public void open() throws ChannelException, InterruptedException {
    cmuxChannelLock.lock();
    try {
      executeSAMB();
    } finally {
      cmuxChannelLock.unlock();
    }
  }

  public void close() throws ChannelException, InterruptedException {
    cmuxChannelLock.lock();
    try {
      executeDISC();
    } finally {
      cmuxChannelLock.unlock();
    }
  }

  private void sendUIH(final UIHCommand uihCommand) throws ChannelException, InterruptedException {
    CMUXFrame cmuxFrame = new CMUXFrame();
    cmuxFrame.setAddressEA(true).setAddressCR(true).setAddressDLCI(addressDLCI);
    cmuxFrame.setControlPF(false).setControlFrameType(FrameType.UIH);
    cmuxFrame.getInformationField().clear();
    UIHCommand.encoding(uihCommand, cmuxFrame.getInformationField());
    cmuxFrame.getInformationField().flip();

    cmuxConnection.sendCMUXFrame(cmuxFrame);
  }

  void sendModemStatus(final byte addressDLCI, final MSCValue mscValue) throws ChannelException, InterruptedException {
    cmuxChannelLock.lock();
    try {
      executeMSCRequest(addressDLCI, mscValue);
    } finally {
      cmuxChannelLock.unlock();
    }
  }

  private void executeMSCRequest(final byte addressDLCI, final MSCValue mscValue) throws ChannelException, InterruptedException {
    UIHCommandMSC requestUIHCommandMSC = new UIHCommandMSC().setAddressEA(true).setAddressDLCI(addressDLCI).setModemStatus(mscValue);

    UIHCommand requestUIHCommand = new UIHCommand();
    requestUIHCommand.setTypeEA(true).setTypeCR(true).setCommandType(UIHCommandType.MSC);
    requestUIHCommand.getValue().clear();
    UIHCommandMSC.encoding(requestUIHCommandMSC, requestUIHCommand.getValue());
    requestUIHCommand.getValue().flip();

    executeControlFrameLock.lock();
    try {
      sendUIH(requestUIHCommand);

      if (!executeControlFrameCondition.await(timeout, TimeUnit.MILLISECONDS)) {
        throw new CMUXConnectionTimeoutException(String.format("[%s] - [%s] request timeout", this, requestUIHCommand.getCommandType()));
      } else {
        try {
          if (requestUIHCommandMSC.equals(responseUIHCommandMSC)) {
            throw new CMUXException(String.format("[%s] - response [%s] not equal to request [%s]", this, responseUIHCommandMSC, requestUIHCommandMSC));
          }
        } finally {
          responseUIHCommandMSC = null;
        }
      }
    } finally {
      executeControlFrameLock.unlock();
    }
  }

  private void handleMSCResponse(final UIHCommandMSC responseUIHCommandMSC) throws CMUXException, InterruptedException {
    executeControlFrameLock.lock();
    try {
      if (executeControlFrameLock.hasWaiters(executeControlFrameCondition)) {
        if (this.responseUIHCommandMSC != null) {
          logger.warn("[{}] - receive new [{}] when previous [{}] have not precessed yet, previous one rewritten", this, responseUIHCommandMSC, this.responseUIHCommandMSC);
        }
        this.responseUIHCommandMSC = responseUIHCommandMSC;
        executeControlFrameCondition.signal();
      } else {
        logger.warn("[{}] - receive [{}] without request, dropped", this, responseUIHCommandMSC);
      }
    } finally {
      executeControlFrameLock.unlock();
    }
  }

  private void handleMSCRequest(final UIHCommandMSC requestUIHCommandMSC) throws ChannelException, InterruptedException {
    cmuxConnection.handleModemStatus(requestUIHCommandMSC.getAddressDLCI(), requestUIHCommandMSC.getModemStatus());

    UIHCommand responseUIHCommand = new UIHCommand();
    responseUIHCommand.setTypeEA(true).setTypeCR(false).setCommandType(UIHCommandType.MSC);
    responseUIHCommand.getValue().clear();
    UIHCommandMSC.encoding(requestUIHCommandMSC, responseUIHCommand.getValue());
    responseUIHCommand.getValue().flip();

    sendUIH(responseUIHCommand);
  }

  @Override
  void handleUIHFrame(final CMUXFrame cmuxFrame) throws ChannelException, InterruptedException {
    UIHCommand uihCommand = UIHCommand.decoding(cmuxFrame.getInformationField());

    if (!uihCommand.isTypeEA()) {
      throw new CMUXFramePatternException(String.format("[%s] - wrong type EA bit state, must be 'true' in control UIH frame, [%s] dropped", this, cmuxFrame));
    }

    switch (uihCommand.getCommandType()) {
      case MSC: {
        UIHCommandMSC uihCommandMSC = UIHCommandMSC.decoding(uihCommand.getValue());

        if (!uihCommandMSC.isAddressEA()) {
          throw new CMUXFramePatternException(String.format("[%s] - wrong address EA bit state, must be 'true' in MSC of control UIH frame, [%s] dropped", this, cmuxFrame));
        }

        if (uihCommand.isTypeCR()) {
          handleMSCRequest(uihCommandMSC);
        } else {
          handleMSCResponse(uihCommandMSC);
        }
        break;
      }
      default: {
        logger.warn("[{}] - unsupported UIH Command frame command type, [{}] dropped", this, uihCommand);
      }
    }
  }

  @Override
  void handleModemStatus(final MSCValue mscValue) {
    // TODO
  }
}
