/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

public final class MSCValue {

  private final static byte EA_BIT_N = (byte) 0; // Extension Bit

  private final static byte FC_BIT_N = (byte) 1;

  // DCE2DTE
  private final static byte DSR_BIT_N = (byte) 2;
  private final static byte CTS_BIT_N = (byte) 3;
  private final static byte RING_BIT_N = (byte) 6;
  private final static byte DCD_BIT_N = (byte) 7;

  // DTE2DCE
  private final static byte DTR_BIT_N = (byte) 2;
  private final static byte RTS_BIT_N = (byte) 3;

  private byte lineStatus;
  private BreakStatus breakStatus;

  public MSCValue() {
    this(true); // Extension Bit is always set to 1, so set it by default
  }

  public MSCValue(final boolean lineStatusEA) {
    set(EA_BIT_N, lineStatusEA);
  }

  public byte getLineStatus() {
    return lineStatus;
  }

  public MSCValue setLineStatus(final byte lineStatus) {
    this.lineStatus = lineStatus;
    return this;
  }

  public BreakStatus getBreakStatus() {
    return breakStatus;
  }

  public MSCValue setBreakStatus(final BreakStatus breakStatus) {
    this.breakStatus = breakStatus;
    return this;
  }

  public boolean isLineStatusEA() {
    return get(EA_BIT_N);
  }

  public MSCValue setLineStatusEA(final boolean value) {
    set(EA_BIT_N, value);
    return this;
  }

  public boolean isFC() {
    return get(FC_BIT_N);
  }

  public MSCValue setFC(final boolean value) {
    set(FC_BIT_N, value);
    return this;
  }

  // DCE2DTE
  public boolean isDSR() {
    return get(DSR_BIT_N);
  }

  public MSCValue setDSR(final boolean value) {
    set(DSR_BIT_N, value);
    return this;
  }

  public boolean isCTS() {
    return get(CTS_BIT_N);
  }

  public MSCValue setCTS(final boolean value) {
    set(CTS_BIT_N, value);
    return this;
  }

  public boolean isRING() {
    return get(RING_BIT_N);
  }

  public MSCValue setRING(final boolean value) {
    set(RING_BIT_N, value);
    return this;
  }

  public boolean isDCD() {
    return get(DCD_BIT_N);
  }

  public MSCValue setDCD(final boolean value) {
    set(DCD_BIT_N, value);
    return this;
  }

  // DTE2DCE
  public boolean isDTR() {
    return get(DTR_BIT_N);
  }

  public MSCValue setDTR(final boolean value) {
    set(DTR_BIT_N, value);
    return this;
  }

  public boolean isRTS() {
    return get(RTS_BIT_N);
  }

  public MSCValue setRTS(final boolean value) {
    set(RTS_BIT_N, value);
    return this;
  }

  private boolean get(final int bitIndex) {
    return (lineStatus & (1 << bitIndex)) != 0;
  }

  private void set(final int bitIndex, final boolean value) {
    if (value) {
      lineStatus |= (1 << bitIndex);
    } else {
      lineStatus &= ~(1 << bitIndex);
    }
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof MSCValue))
      return false;

    MSCValue mscValue = (MSCValue) o;

    if (lineStatus != mscValue.lineStatus)
      return false;
    if (breakStatus != mscValue.breakStatus)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) lineStatus;
    result = 31 * result + (breakStatus != null ? breakStatus.hashCode() : 0);
    return result;
  }

  public enum BreakStatus {
    NO_BREAK((byte) 0x01),
    BREAK((byte) 0x03);

    private final byte status;

    BreakStatus(final byte status) {
      this.status = status;
    }

    public byte getStatus() {
      return status;
    }

    public static BreakStatus valueOf(final byte status) {
      short statusUnsigned = (short) (status & 0xFF);
      if (statusUnsigned >= 0 && statusUnsigned < breakStatuses.length) {
        BreakStatus breakStatus = breakStatuses[statusUnsigned];
        if (breakStatus != null) {
          return breakStatus;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported BreakStatus status: 0x%02X", statusUnsigned));
    }

    private static final BreakStatus[] breakStatuses;

    static {
      short maxStatus = 0;
      for (BreakStatus breakStatus : values()) {
        short statusUnsigned = (short) (breakStatus.status & 0xFF);
        if (statusUnsigned > maxStatus) {
          maxStatus = statusUnsigned;
        }
      }
      breakStatuses = new BreakStatus[maxStatus + 1];
      for (BreakStatus breakStatus : values()) {
        short statusUnsigned = (short) (breakStatus.status & 0xFF);
        breakStatuses[statusUnsigned] = breakStatus;
      }
    }
  }
}
