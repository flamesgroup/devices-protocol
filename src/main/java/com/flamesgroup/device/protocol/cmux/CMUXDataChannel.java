/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import com.flamesgroup.device.channel.ChannelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public final class CMUXDataChannel extends CMUXChannel implements ICMUXVirtualPort {

  private final Logger logger = LoggerFactory.getLogger(CMUXDataChannel.class);

  private final AtomicReference<ICMUXVirtualPortHandler> cmuxVirtualPortHandlerAtomicReference = new AtomicReference<>();

  public CMUXDataChannel(final CMUXConnection cmuxConnection, final byte addressDLCI, final long timeout) {
    super(cmuxConnection, addressDLCI, timeout);

    if (addressDLCI == CMUXControlChannel.ADDRESS_DLCI) {
      throw new IllegalArgumentException("addressDLCI of CMUXDataChannel can't be 0");
    }
  }

  @Override
  public boolean isOpen() {
    return cmuxVirtualPortHandlerAtomicReference.get() != null;
  }

  @Override
  public void open(final ICMUXVirtualPortHandler cmuxVirtualPortHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(cmuxVirtualPortHandler, "cmuxVirtualPortHandler must not be null");
    cmuxChannelLock.lock();
    try {
      cmuxVirtualPortHandlerAtomicReference.set(cmuxVirtualPortHandler);
      try {
        executeSAMB();
        sendModemStatus(new MSCValue().setFC(false).setRTS(true).setDTR(true));
      } catch (Exception e) {
        cmuxVirtualPortHandlerAtomicReference.set(null);
        throw e;
      }
    } finally {
      cmuxChannelLock.unlock();
    }
  }

  @Override
  public void close() throws ChannelException, InterruptedException {
    cmuxChannelLock.lock();
    try {
      executeDISC();
    } finally {
      cmuxVirtualPortHandlerAtomicReference.set(null);
      cmuxChannelLock.unlock();
    }
  }

  @Override
  public void sendData(final ByteBuffer data) throws ChannelException, InterruptedException {
    cmuxChannelLock.lock();
    try {
      int lengthToTransmit;
      while (data.remaining() > 0) {
        if (data.remaining() > CMUXFrame.MAX_INFORMATION_FIELD_LENGTH) {
          lengthToTransmit = CMUXFrame.MAX_INFORMATION_FIELD_LENGTH;
        } else {
          lengthToTransmit = data.remaining();
        }

        CMUXFrame cmuxFrame = new CMUXFrame();
        cmuxFrame.setAddressEA(true).setAddressCR(true).setAddressDLCI(addressDLCI);
        cmuxFrame.setControlPF(false).setControlFrameType(FrameType.UIH);
        cmuxFrame.getInformationField().clear();
        while (lengthToTransmit-- > 0) {
          cmuxFrame.getInformationField().put(data.get());
        }
        cmuxFrame.getInformationField().flip();

        cmuxConnection.sendCMUXFrame(cmuxFrame);
      }
    } finally {
      cmuxChannelLock.unlock();
    }
  }

  @Override
  void handleUIHFrame(final CMUXFrame cmuxFrame) throws CMUXException, InterruptedException {
    ICMUXVirtualPortHandler cmuxVirtualPortHandlerLocal = cmuxVirtualPortHandlerAtomicReference.get();
    if (cmuxVirtualPortHandlerLocal != null) {
      cmuxVirtualPortHandlerLocal.handleData(cmuxFrame.getInformationField());
    } else {
      logger.warn("[{}] - handle unexpected UIH frame, [{}] dropped", this, cmuxFrame);
    }
  }

  @Override
  void handleModemStatus(final MSCValue mscValue) {
    // TODO
  }
}
