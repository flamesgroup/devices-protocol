/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import java.nio.ByteBuffer;

public final class UIHCommandMSC {

  private static final byte EA_BIT = (byte) 0x01; // Extension Bit
  private static final byte CR_BIT = (byte) 0x02; // Command/Response

  private boolean addressEA;
  private byte addressDLCI;
  private MSCValue mscValue;

  public byte getAddress() {
    byte address = (byte) (addressDLCI << 2);
    address |= CR_BIT;
    if (addressEA)
      address |= EA_BIT;
    return address;
  }

  public UIHCommandMSC setAddress(final byte address) {
    addressDLCI = (byte) (address >> 2);
    addressEA = (address & EA_BIT) != 0;
    return this;
  }

  public boolean isAddressEA() {
    return addressEA;
  }

  public UIHCommandMSC setAddressEA(final boolean addressEA) {
    this.addressEA = addressEA;
    return this;
  }

  public byte getAddressDLCI() {
    return addressDLCI;
  }

  public UIHCommandMSC setAddressDLCI(final byte addressDLCI) {
    this.addressDLCI = addressDLCI;
    return this;
  }

  public MSCValue getModemStatus() {
    return mscValue;
  }

  public UIHCommandMSC setModemStatus(final MSCValue mscValue) {
    this.mscValue = mscValue;
    return this;
  }

  public static UIHCommandMSC decoding(final ByteBuffer buffer) throws CMUXFrameCodingException {
    UIHCommandMSC msc = new UIHCommandMSC();

    msc.setAddress(buffer.get());

    MSCValue mscValue = new MSCValue();
    mscValue.setLineStatus(buffer.get());
    if (buffer.remaining() > 0) {
      mscValue.setBreakStatus(MSCValue.BreakStatus.valueOf(buffer.get()));
    } else {
      mscValue.setBreakStatus(null);
    }

    msc.setModemStatus(mscValue);

    return msc;
  }

  public static void encoding(final UIHCommandMSC msc, final ByteBuffer buffer) throws CMUXFrameCodingException {
    buffer.put(msc.getAddress());

    MSCValue mscValue = msc.getModemStatus();
    buffer.put(mscValue.getLineStatus());
    if (mscValue.getBreakStatus() != null) {
      buffer.put(mscValue.getBreakStatus().getStatus());
    }
  }

}
