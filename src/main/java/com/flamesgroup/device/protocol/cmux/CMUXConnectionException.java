/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

public class CMUXConnectionException extends CMUXException {

  private static final long serialVersionUID = -7531861778496032534L;

  public CMUXConnectionException() {
  }

  public CMUXConnectionException(final String message) {
    super(message);
  }

  public CMUXConnectionException(final Throwable throwable) {
    super(throwable);
  }

  public CMUXConnectionException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
