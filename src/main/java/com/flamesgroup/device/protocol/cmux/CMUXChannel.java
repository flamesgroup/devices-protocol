/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import com.flamesgroup.device.channel.ChannelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class CMUXChannel {

  private final Logger logger = LoggerFactory.getLogger(CMUXChannel.class);

  protected final CMUXConnection cmuxConnection;
  protected final byte addressDLCI;
  protected final long timeout;

  protected final Lock cmuxChannelLock = new ReentrantLock();

  protected final ReentrantLock executeControlFrameLock = new ReentrantLock();
  protected final Condition executeControlFrameCondition = executeControlFrameLock.newCondition();

  private CMUXFrame responseCMUXFrame;

  private final String toS;

  public CMUXChannel(final CMUXConnection cmuxConnection, final byte addressDLCI, final long timeout) {
    Objects.requireNonNull(cmuxConnection, "cmuxConnection must not be null");
    if (addressDLCI < 0 || addressDLCI >= CMUXConnection.ADDRESS_DLCI_COUNT) {
      throw new IllegalArgumentException("addressDLCI must be >= 0 and < " + CMUXConnection.ADDRESS_DLCI_COUNT);
    }
    if (timeout < 0) {
      throw new IllegalArgumentException("timeout must be >= 0");
    }
    this.cmuxConnection = cmuxConnection;
    this.addressDLCI = addressDLCI;
    this.timeout = timeout;

    toS = String.format("%s@%x:[%d]", getClass().getSimpleName(), hashCode(), addressDLCI);

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}] and timeout [{}]", toS, cmuxConnection, timeout);
    }
  }

  private void execute(final CMUXFrame commandCMUXFrame) throws ChannelException, InterruptedException {
    executeControlFrameLock.lock();
    try {
      cmuxConnection.sendCMUXFrame(commandCMUXFrame);

      if (!executeControlFrameCondition.await(timeout, TimeUnit.MILLISECONDS)) {
        throw new CMUXConnectionTimeoutException(String.format("[%s] - [%s] command timeout", this, commandCMUXFrame.getControlFrameType()));
      } else {
        try {
          switch (responseCMUXFrame.getControlFrameType()) {
            case UA:
              break;
            case DM:
              throw new CMUXRejectException(String.format("[%s] - [%s] command rejected", this, commandCMUXFrame.getControlFrameType()));
            default:
              throw new CMUXException(String.format("[%s] - wrong frame type [%s] response for [%s] command", this, responseCMUXFrame.getControlFrameType(), commandCMUXFrame.getControlFrameType()));
          }
        } finally {
          responseCMUXFrame = null;
        }
      }
    } finally {
      executeControlFrameLock.unlock();
    }
  }

  protected void executeSAMB() throws ChannelException, InterruptedException {
    CMUXFrame commandCMUXFrame = new CMUXFrame();
    commandCMUXFrame.setAddressEA(true).setAddressCR(true).setAddressDLCI(addressDLCI);
    commandCMUXFrame.setControlPF(true).setControlFrameType(FrameType.SAMB);
    commandCMUXFrame.getInformationField().clear().flip();

    execute(commandCMUXFrame);
  }

  protected void executeDISC() throws ChannelException, InterruptedException {
    CMUXFrame commandCMUXFrame = new CMUXFrame();
    commandCMUXFrame.setAddressEA(true).setAddressCR(true).setAddressDLCI(addressDLCI);
    commandCMUXFrame.setControlPF(true).setControlFrameType(FrameType.DISC);
    commandCMUXFrame.getInformationField().clear().flip();

    execute(commandCMUXFrame);
  }

  void handleCMUXFrame(final CMUXFrame cmuxFrame) throws ChannelException, InterruptedException {
    switch (cmuxFrame.getControlFrameType()) {
      case UIH:
        // 3GPP TS 27.010: 5.4.3.1 Information Data
        // The frames sent by the initiating station have the C/R bit set to 1 and those sent by the responding station have the C/R bit set to 0. Both stations set the P-bit to 0.
        /* TODO analyze how this bit can be checked
        if (cmuxFrame.isAddressCR()) {
          throw new CMUXFramePatternException(String.format("[%s] - wrong address CR bit state, must be 'false' in UIH frame from module to application, [%s] dropped", this, cmuxFrame));
        }
        */
        if (cmuxFrame.isControlPF()) {
          throw new CMUXFramePatternException(String.format("[%s] - wrong control PF bit state, must be 'false' in UIH frame, [%s] dropped", this, cmuxFrame));
        }

        handleUIHFrame(cmuxFrame);
        break;
      case UA:
      case DM:
        if (!cmuxFrame.isAddressCR()) {
          throw new CMUXFramePatternException(String.format("[%s] - wrong address CR bit state, always must be 'true', [%s] dropped", this, cmuxFrame));
        }
        if (!cmuxFrame.isControlPF()) {
          throw new CMUXFramePatternException(String.format("[%s] - wrong control PF bit state, always must be 'true', [%s] dropped", this, cmuxFrame));
        }

        executeControlFrameLock.lock();
        try {
          if (executeControlFrameLock.hasWaiters(executeControlFrameCondition)) {
            if (this.responseCMUXFrame != null) {
              logger.warn("[{}] - receive new [{}] when previous [{}] have not precessed yet, previous one rewritten", this, cmuxFrame, this.responseCMUXFrame);
            }
            this.responseCMUXFrame = cmuxFrame;
            executeControlFrameCondition.signal();
          } else {
            logger.warn("[{}] - receive [{}] without request, dropped", this, cmuxFrame);
          }
        } finally {
          executeControlFrameLock.unlock();
        }
        break;
      default:
        logger.warn("[{}] - unsupported frame [{}], dropped", this, cmuxFrame);
    }
  }

  abstract void handleUIHFrame(CMUXFrame cmuxFrame) throws ChannelException, InterruptedException;

  void sendModemStatus(final MSCValue mscValue) throws ChannelException, InterruptedException {
    cmuxConnection.sendModemStatus(addressDLCI, mscValue);
  }

  abstract void handleModemStatus(MSCValue mscValue);

  @Override
  public String toString() {
    return toS;
  }
}
