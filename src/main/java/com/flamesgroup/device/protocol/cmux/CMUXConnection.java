/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IRawATSubChannel;
import com.flamesgroup.device.gsmb.IRawATSubChannelHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CMUXConnection implements ICMUXConnection, IRawATSubChannelHandler {

  public static final byte VIRTUAL_PORT_COUNT = (byte) 4;
  public static final byte ADDRESS_DLCI_COUNT = (byte) (VIRTUAL_PORT_COUNT + 1); // <virtual port count> + <control channel>

  private static final CMUXFrame CLOSE_CMUX_FRAME = new CMUXFrame();

  private final Logger logger = LoggerFactory.getLogger(CMUXConnection.class);

  private final Lock connectionLock = new ReentrantLock();

  private final CMUXChannel[] cmuxChannels = new CMUXChannel[ADDRESS_DLCI_COUNT]; // base type array for handling incoming CMUX Frames
  private final CMUXControlChannel cmuxControlChannel;
  private final CMUXDataChannel[] cmuxDataChannels = new CMUXDataChannel[VIRTUAL_PORT_COUNT];
  private final List<ICMUXVirtualPort> cmuxVirtualPorts;

  private final IRawATSubChannel rawATSubChannel;
  private final long timeout;

  private volatile CMUXConnectionThread connectionThread;

  // 260 bytes - size of mudp buffer
  private final ByteBuffer byteBufferToReceive = ByteBuffer.allocate(260 * 3).order(ByteOrder.LITTLE_ENDIAN);

  /*
   *  133 bytes - max CMUX frame length, when max length field is 7 bits - 127
   *  maximum 10 CMUX packets can be received at a time
   */
  private final ByteBuffer byteBufferToTransmit = ByteBuffer.allocate(133 * 10).order(ByteOrder.LITTLE_ENDIAN);

  private final String toS;

  public CMUXConnection(final IRawATSubChannel rawATSubChannel, final long timeout) {
    Objects.requireNonNull(rawATSubChannel, "rawATSubChannel must not be null");
    if (timeout < 0) {
      throw new IllegalArgumentException("timeout must be >= 0");
    }
    this.rawATSubChannel = rawATSubChannel;
    this.timeout = timeout;

    cmuxControlChannel = new CMUXControlChannel(this, timeout);
    cmuxChannels[CMUXControlChannel.ADDRESS_DLCI] = cmuxControlChannel;

    for (byte i = 0; i < VIRTUAL_PORT_COUNT; i++) {
      byte addressDLCI = (byte) (i + 1);
      cmuxDataChannels[i] = new CMUXDataChannel(this, addressDLCI, timeout);
      cmuxChannels[addressDLCI] = cmuxDataChannels[i];
    }

    cmuxVirtualPorts = Collections.unmodifiableList(Arrays.asList((ICMUXVirtualPort[]) cmuxDataChannels));

    toS = String.format("%s@%x:[%d]", getClass().getSimpleName(), hashCode(), timeout);

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}] and timeout [{}]", toS, rawATSubChannel, timeout);
    }
  }

  // ICMUXConnection

  @Override
  public boolean isConnected() {
    return connectionThread != null;
  }

  @Override
  public void connect() throws ChannelException, InterruptedException {
    logger.debug("[{}] - connecting", this);
    connectionLock.lock();
    try {
      if (isConnected()) {
        throw new IllegalStateException("[" + this + "] already connected");
      }

      connectionThread = new CMUXConnectionThread();
      connectionThread.setDaemon(true);
      connectionThread.setUncaughtExceptionHandler((t, e) -> logger.error("[{}] - get uncaught Exception", t.getName(), e));
      connectionThread.start();

      try {
        rawATSubChannel.start(this);

        // after ATInitSequence, during 5 seconds, we have to execute a command - open a port
        try {
          cmuxControlChannel.open();
        } catch (CMUXException | InterruptedException e) {
          logger.warn("[{}] - can't open CMUX Control Channel during connect", this, e);
          try {
            rawATSubChannel.stop();
          } catch (ChannelException | InterruptedException ce) {
            ce.addSuppressed(e);
            throw ce;
          }
          throw e;
        }
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't start Raw AT SubChannel during connect", this, e);
        try {
          connectionThread.politeStop();
        } catch (InterruptedException ce) {
          ce.addSuppressed(e);
          throw ce;
        } finally {
          connectionThread = null;
        }
      }
    } catch (Exception e) {
      logger.warn("[{}] - can't connect to CMUX Connection", this, e);

      throw e;
    } finally {
      connectionLock.unlock();
    }
    logger.debug("[{}] - connected", this);
  }

  @Override
  public void disconnect() throws ChannelException, InterruptedException {
    logger.debug("[{}] - disconnecting", this);
    connectionLock.lock();
    try {
      if (!isConnected()) {
        throw new IllegalStateException("[" + this + "] already disconnected");
      }

      // DISC command sent at DLCI 0 have the same meaning as the Multiplexer Close Down command (see clause 5.4.6.3.3). See also clause 5.8.2 for more information about the Close-down procedure.
      for (byte i = 0; i < VIRTUAL_PORT_COUNT; i++) {
        CMUXDataChannel cmuxDataChannel = cmuxDataChannels[i];
        if (cmuxDataChannel.isOpen()) {
          try {
            cmuxDataChannel.close();
          } catch (ChannelException | InterruptedException e) {
            logger.warn("[{}] - can't close CMUX Data Channel on virtual port {}", this, i, e);
          }
        }
      }

      Exception le = null;
      try {
        cmuxControlChannel.close();
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't close CMUX Control Channel during disconnect", this, e);
        le = e;
      }
      try {
        rawATSubChannel.stop();
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't stop Raw AT SubChannel during disconnect", this, e);
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      try {
        connectionThread.politeStop();
      } catch (InterruptedException e) {
        logger.warn("[{}] - can't polite stop Connection Thread during disconnect", this, e);
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      } finally {
        connectionThread = null;
      }
      if (le != null) {
        if (le instanceof ChannelException) {
          throw (ChannelException) le;
        } else {
          throw (InterruptedException) le;
        }
      }
    } catch (Exception e) {
      logger.warn("[{}] - can't correctly disconnect from CMUX Connection", this, e);

      throw e;
    } finally {
      connectionLock.unlock();
    }
    logger.debug("[{}] - disconnected", this);
  }

  @Override
  public List<ICMUXVirtualPort> getVirtualPorts() {
    return cmuxVirtualPorts;
  }

  // IRawATSubChannelHandler

  @Override
  public void handleRawATData(final ByteBuffer data) {
    try {
      byteBufferToReceive.put(data);
    } catch (BufferOverflowException e) {
      logger.error("[{}] - CMUX buffer to receive overflow - data dropped", this, e);
      return;
    }
    byteBufferToReceive.flip();
    do {
      byteBufferToReceive.mark();
      try {
        CMUXFrame cmuxFrame = CMUXFrame.decoding(byteBufferToReceive);
        CMUXConnectionThread connectionThreadLocal = connectionThread;
        if (connectionThreadLocal == null) {
          logger.warn("[{}] - CMUX connection thread is null - CMUX frame dropped", this);
          break;
        }
        try {
          connectionThreadLocal.handleCMUXFrame(cmuxFrame);
        } catch (InterruptedException e) {
          logger.error("[{}] - can't put CMUX frame", this, e);
          break;
        }
      } catch (BufferUnderflowException e) {
        byteBufferToReceive.reset();
        break;
      } catch (CMUXFrameCodingException e) {
        logger.error("[{}] - can't decode CMUX frame", this, e);
      }
    } while (true);
    byteBufferToReceive.compact();
  }

  // Object

  @Override
  public String toString() {
    return toS;
  }

  // namespace functionality

  void sendCMUXFrame(final CMUXFrame cmuxFrame) throws ChannelException, InterruptedException {
    connectionLock.lock();
    try {
      byteBufferToTransmit.clear();
      CMUXFrame.encoding(cmuxFrame, byteBufferToTransmit);
      byteBufferToTransmit.flip();

      rawATSubChannel.writeRawATData(byteBufferToTransmit);
    } finally {
      connectionLock.unlock();
    }
  }

  void sendModemStatus(final byte addressDLCI, final MSCValue mscValue) throws ChannelException, InterruptedException {
    cmuxControlChannel.sendModemStatus(addressDLCI, mscValue);
  }

  void handleModemStatus(final byte addressDLCI, final MSCValue mscValue) {
    cmuxChannels[addressDLCI].handleModemStatus(mscValue);
  }

  // internal thread

  private class CMUXConnectionThread extends Thread {

    private final BlockingQueue<CMUXFrame> receivedCMUXFrames = new LinkedBlockingQueue<>();

    public CMUXConnectionThread() {
      super("CMUXConnectionThread (" + rawATSubChannel + ")");
    }

    public void politeStop() throws InterruptedException {
      if (!receivedCMUXFrames.offer(CLOSE_CMUX_FRAME)) {
        this.interrupt();
      }
      this.join();
    }

    public void handleCMUXFrame(final CMUXFrame cmuxFrame) throws InterruptedException {
      receivedCMUXFrames.put(cmuxFrame);
    }

    @Override
    public void run() {
      while (true) {
        CMUXFrame cmuxFrame;

        try {
          cmuxFrame = receivedCMUXFrames.take();
        } catch (InterruptedException e) {
          logger.warn("[{}] - current thread has been interrupted", this, e);
          break;
        }

        if (cmuxFrame == CLOSE_CMUX_FRAME) { // polite thread stop
          break;
        }

        if (!cmuxFrame.isAddressEA()) {
          logger.warn("[{}] - wrong address EA bit state, always must be 'true', [{}] dropped", this, cmuxFrame);
          continue;
        }

        try {
          cmuxChannels[cmuxFrame.getAddressDLCI()].handleCMUXFrame(cmuxFrame);
        } catch (InterruptedException e) {
          logger.warn("[{}] - current thread has been interrupted", this, e);
          break;
        } catch (CMUXFramePatternException e) {
          logger.warn("[{}] - receive incorrect CMUX frame pattern", this, e);
        } catch (ChannelException e) {
          logger.warn("[{}] - unexpected exception", this, e);
        }
      }
    }

  }

}
