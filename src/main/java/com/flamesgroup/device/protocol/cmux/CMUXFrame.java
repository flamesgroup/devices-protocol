/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.device.protocol.cmux.util.CRC8;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class CMUXFrame {

  public static final int MAX_INFORMATION_FIELD_LENGTH = 127; // TODO make configurable with proper CMUX enable AT command execution (currently use default max CMUX frame size)

  private static final byte FLAG = (byte) 0xF9;

  private static final byte EA_BIT = (byte) 0x01; // Extension Bit
  private static final byte CR_BIT = (byte) 0x02; // Command/Response
  private static final byte PF_BIT = (byte) 0x10; // Poll/Final Bit

  private boolean addressEA;
  private boolean addressCR;
  private byte addressDLCI;
  private boolean controlPF;
  private FrameType controlFrameType;
  private final ByteBuffer informationField = ByteBuffer.allocate(MAX_INFORMATION_FIELD_LENGTH).order(ByteOrder.LITTLE_ENDIAN);

  public CMUXFrame() {
  }

  private byte getAddress() {
    byte address = (byte) (addressDLCI << 2);
    if (addressCR)
      address |= CR_BIT;
    if (addressEA)
      address |= EA_BIT;
    return address;
  }

  private CMUXFrame setAddress(final byte address) {
    addressDLCI = (byte) (address >> 2);
    addressCR = (address & CR_BIT) != 0;
    addressEA = (address & EA_BIT) != 0;
    return this;
  }

  public boolean isAddressEA() {
    return addressEA;
  }

  public CMUXFrame setAddressEA(final boolean addressEA) {
    this.addressEA = addressEA;
    return this;
  }

  public boolean isAddressCR() {
    return addressCR;
  }

  public CMUXFrame setAddressCR(final boolean addressCR) {
    this.addressCR = addressCR;
    return this;
  }

  public byte getAddressDLCI() {
    return addressDLCI;
  }

  public CMUXFrame setAddressDLCI(final byte addressDLCI) {
    this.addressDLCI = addressDLCI;
    return this;
  }

  private byte getControl() {
    byte control = controlFrameType.getType();
    if (controlPF)
      control |= PF_BIT;
    return control;
  }

  private CMUXFrame setControl(final byte control) {
    this.controlFrameType = FrameType.valueOf((byte) (control & ~PF_BIT));
    this.controlPF = (control & PF_BIT) != 0;
    return this;
  }

  public boolean isControlPF() {
    return controlPF;
  }

  public CMUXFrame setControlPF(final boolean controlPF) {
    this.controlPF = controlPF;
    return this;
  }

  public FrameType getControlFrameType() {
    return controlFrameType;
  }

  public CMUXFrame setControlFrameType(final FrameType controlFrameType) {
    this.controlFrameType = controlFrameType;
    return this;
  }

  public ByteBuffer getInformationField() {
    return informationField;
  }

  public static CMUXFrame decoding(final ByteBuffer buffer) throws CMUXFrameCodingException {
    int positionOrig = buffer.position();
    int limitOrig = buffer.limit();
    try {
      if (buffer.get() != FLAG) {
        throw new CMUXFrameCodingException("Can't decode - wrong start flag");
      }
      CMUXFrame cmuxFrame = new CMUXFrame();

      cmuxFrame.setAddress(buffer.get());

      cmuxFrame.setControl(buffer.get());

      short lengthIndicator = (short) (0xFF & buffer.get());
      if ((lengthIndicator & EA_BIT) == 0) {
        lengthIndicator |= (buffer.get() << 8);
      }
      short length = (short) (0x7FFF & (lengthIndicator >> 1));

      cmuxFrame.getInformationField().clear();
      if (cmuxFrame.getInformationField().remaining() < length) {
        throw new CMUXFrameCodingException("Can't decode CMUX frame - information field overflow");
      }
      for (int i = 0; i < length; i++) {
        cmuxFrame.getInformationField().put(buffer.get());
      }
      cmuxFrame.getInformationField().flip();

      if (!CRC8.checkReceiverCode(buffer.get(), cmuxFrame.getAddress(), cmuxFrame.getControl(), lengthIndicator)) {
        throw new CMUXFrameCodingException("Can't decode - wrong FCS");
      }

      if (buffer.get() != FLAG) {
        throw new CMUXFrameCodingException("Can't decode - wrong stop flag");
      }

      return cmuxFrame;
    } catch (CMUXFrameCodingException | IllegalArgumentException e) {
      buffer.position(positionOrig);

      while (true) {
        if (buffer.get() == FLAG)
          break;
      }

      int positionDrop = buffer.position();

      buffer.position(positionOrig);
      buffer.limit(positionDrop);

      String droppedData = DataRepresentationHelper.toHexArrayString(buffer);

      buffer.position(positionDrop);
      buffer.limit(limitOrig);

      throw new CMUXFrameCodingException("Drop malformed data [" + droppedData + "]", e);
    }
  }

  public static void encoding(final CMUXFrame cmuxFrame, final ByteBuffer buffer) throws CMUXFrameCodingException {
    buffer.put(FLAG);

    buffer.put(cmuxFrame.getAddress());

    buffer.put(cmuxFrame.getControl());

    int length = cmuxFrame.getInformationField().remaining();
    short lengthIndicator = (short) (length << 1);
    if (length <= 127) {
      lengthIndicator |= EA_BIT;
      buffer.put((byte) lengthIndicator);
    } else {
      buffer.putShort(lengthIndicator);
    }

    buffer.put(cmuxFrame.getInformationField());

    buffer.put(CRC8.checkTransmitterCode(cmuxFrame.getAddress(), cmuxFrame.getControl(), lengthIndicator));

    buffer.put(FLAG);
  }

}
