/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class UIHCommand {

  public static final int MAX_VALUE_LENGTH = CMUXFrame.MAX_INFORMATION_FIELD_LENGTH;

  private static final byte EA_BIT = (byte) 0x01; // Extension Bit
  private static final byte CR_BIT = (byte) 0x02; // Command/Response

  private boolean typeEA;
  private boolean typeCR;
  private UIHCommandType commandType;
  private final ByteBuffer value = ByteBuffer.allocate(MAX_VALUE_LENGTH).order(ByteOrder.LITTLE_ENDIAN);

  public UIHCommand() {
  }

  private byte getType() {
    byte type = (byte) (commandType.getType() << 2);
    if (typeCR)
      type |= CR_BIT;
    if (typeEA)
      type |= EA_BIT;
    return type;
  }

  private UIHCommand setType(final byte type) {
    this.commandType = UIHCommandType.valueOf((byte) ((type >> 2) & 0x3F));
    this.typeCR = (type & CR_BIT) != 0;
    this.typeEA = (type & EA_BIT) != 0;
    return this;
  }

  public boolean isTypeEA() {
    return typeEA;
  }

  public UIHCommand setTypeEA(final boolean typeEA) {
    this.typeEA = typeEA;
    return this;
  }

  public boolean isTypeCR() {
    return typeCR;
  }

  public UIHCommand setTypeCR(final boolean typeCR) {
    this.typeCR = typeCR;
    return this;
  }

  public UIHCommandType getCommandType() {
    return commandType;
  }

  public UIHCommand setCommandType(final UIHCommandType commandType) {
    this.commandType = commandType;
    return this;
  }

  public ByteBuffer getValue() {
    return value;
  }

  public static UIHCommand decoding(final ByteBuffer buffer) throws CMUXFrameCodingException {
    UIHCommand uihCommand = new UIHCommand();

    uihCommand.setType(buffer.get());

    short lengthIndicator = (short) (0xFF & buffer.get());
    if ((lengthIndicator & EA_BIT) == 0) {
      lengthIndicator |= (buffer.get() << 8);
    }
    short length = (short) (0x7FFF & (lengthIndicator >> 1));

    uihCommand.getValue().clear();
    if (uihCommand.getValue().remaining() < length) {
      throw new CMUXFrameCodingException("Can't decode UID Command frame - information field overflow");
    }
    for (int i = 0; i < length; i++) {
      uihCommand.getValue().put(buffer.get());
    }
    uihCommand.getValue().flip();

    return uihCommand;
  }

  public static void encoding(final UIHCommand uihCommand, final ByteBuffer buffer) throws CMUXFrameCodingException {
    buffer.put(uihCommand.getType());

    int length = uihCommand.getValue().remaining();
    short lengthIndicator = (short) (length << 1);
    if (length <= 127) {
      lengthIndicator |= EA_BIT;
      buffer.put((byte) lengthIndicator);
    } else {
      buffer.putShort(lengthIndicator);
    }

    buffer.put(uihCommand.getValue());
  }

}
