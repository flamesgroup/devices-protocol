/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.device.protocol.util.CRC16;
import com.flamesgroup.jdev.DevClosedException;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.spi.ISPIDev;
import com.flamesgroup.jdev.spi.SPIDevAddress;
import com.flamesgroup.jdev.spi.SPIOptions;
import com.flamesgroup.jdev.spi.jni.SPIDev;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class MudpStreamSPIDev implements IMudpStream {

  private final Logger logger = LoggerFactory.getLogger(MudpStreamSPIDev.class);

  public static final int SPI_DEV_CLK = 12_000_000;

  private static final byte EMPTY_PACKET_SPECIAL_MUDP_PROTOCOL_VERSION = (byte) 0xFF;

  private static final int SPI_CHAIN_SLAVES_COUNT_MIN = 1;
  private static final int SPI_CHAIN_SLAVES_COUNT_MAX = 20;
  private static final int MUDP_SERVICE_PACKET_FRAME_LENGTH = MudpPacket.SERVICE_PACKET_MAX_LENGTH + CRC16.LENGTH;
  private static final int MUDP_DATA_PACKET_FRAME_LENGTH = MudpPacket.DATA_PACKET_MAX_LENGTH + CRC16.LENGTH;

  private final static MudpPacket CLOSE_MUDP_PACKET = new MudpPacket(0);
  private final static MudpPacket HANG_MUDP_PACKET = new MudpPacket(0);

  public static final int CPU_N_MAX = SPI_CHAIN_SLAVES_COUNT_MAX;
  public static final int CHANNEL_N_MAX = 12; // = 256 / 20 (where 256 - max byte count, 20 - max cpu in SPI chain)

  private final Lock streamLock = new ReentrantLock();

  private final SPIDevAddress spiDevAddress;
  private final int spiChainSlavesCount;
  private final int mudpServicePacketsCountPerSlave;
  private final int mudpDataPacketsCountPerSlave;

  private final int exchangeBufferCapacity;
  private final ByteBuffer exchangeBuffer;

  private final IStatistics statistics;

  private ISPIDev spiDev;
  private MudpStreamSPIDevThread streamThread;

  private BlockingQueue<MudpPacket> incomeMudpPacketQueue;
  private List<BlockingQueue<MudpPacket>> outcomeMudpServicePacketQueues;
  private List<BlockingQueue<MudpPacket>> outcomeMudpDataPacketQueues;

  private MudpIOException streamMudpIOException;

  private final AtomicReference<String> atomicToS = new AtomicReference<>();

  public MudpStreamSPIDev(final SPIDevAddress spiDevAddress, final int spiChainSlavesCount, final int mudpServicePacketsCountPerSlave, final int mudpDataPacketsCountPerSlave,
      final int statisticsGatheringIterationAmount) {
    Objects.requireNonNull(spiDevAddress, "spiDevAddress mustn't be null");
    if (spiChainSlavesCount < SPI_CHAIN_SLAVES_COUNT_MIN || spiChainSlavesCount > SPI_CHAIN_SLAVES_COUNT_MAX) {
      throw new IllegalArgumentException("spiChainSlavesCount must be >= " + SPI_CHAIN_SLAVES_COUNT_MIN + " and <= " + SPI_CHAIN_SLAVES_COUNT_MAX + ", but it [" + spiChainSlavesCount + "]");
    }
    if (mudpServicePacketsCountPerSlave <= 0) {
      throw new IllegalArgumentException("mudpServicePacketsCountPerSlave must be > 0");
    }
    if (mudpDataPacketsCountPerSlave <= 0) {
      throw new IllegalArgumentException("mudpDataPacketsCountPerSlave must be > 0");
    }
    if (statisticsGatheringIterationAmount < 0) {
      throw new IllegalArgumentException("statisticsGatheringIterationAmount must be >= 0");
    }

    this.spiDevAddress = spiDevAddress;
    this.spiChainSlavesCount = spiChainSlavesCount;
    this.mudpServicePacketsCountPerSlave = mudpServicePacketsCountPerSlave;
    this.mudpDataPacketsCountPerSlave = mudpDataPacketsCountPerSlave;

    exchangeBufferCapacity = (MUDP_SERVICE_PACKET_FRAME_LENGTH * mudpServicePacketsCountPerSlave + MUDP_DATA_PACKET_FRAME_LENGTH * mudpDataPacketsCountPerSlave) * spiChainSlavesCount;
    exchangeBuffer = ByteBuffer.allocateDirect(exchangeBufferCapacity).order(ByteOrder.LITTLE_ENDIAN);

    // statistics instance must be construct only after all current instance variables init, because they used during construction
    if (statisticsGatheringIterationAmount == 0) {
      statistics = new MudpStreamSPIDevEmptyStatistics();
    } else {
      statistics = new MudpStreamSPIDevStatistics(this, statisticsGatheringIterationAmount);
    }

    updateToS();
  }

  public MudpStreamSPIDev(final SPIDevAddress spiDevAddress, final int spiChainSlavesCount, final int mudpServicePacketsCountPerSlave, final int mudpDataPacketsCountPerSlave) {
    this(spiDevAddress, spiChainSlavesCount, mudpServicePacketsCountPerSlave, mudpDataPacketsCountPerSlave, 0);
  }

  public SPIDevAddress getSPIDevAddress() {
    return spiDevAddress;
  }

  public int getSPIChainSlavesCount() {
    return spiChainSlavesCount;
  }

  public int getMudpServicePacketsCountPerSlave() {
    return mudpServicePacketsCountPerSlave;
  }

  public int getMudpDataPacketsCountPerSlave() {
    return mudpDataPacketsCountPerSlave;
  }

  public int getTheoreticalExchangeIterationDuration() {
    return (exchangeBufferCapacity * 8) / (MudpStreamSPIDev.SPI_DEV_CLK / 1000) + 1;
  }

  private boolean isOpenLocal(final ISPIDev spiDevLocal) {
    return spiDevLocal != null && spiDevLocal.isOpen();
  }

  private ISPIDev getSPIDevLocal() {
    streamLock.lock();
    try {
      return spiDev;
    } finally {
      streamLock.unlock();
    }
  }

  private MudpIOException closeByInterrupt(final InterruptedException ie) {
    try {
      close();
    } catch (MudpIOException ce) {
      logger.warn("[{}] - can't close by interrupt", this, ce);
      ce.addSuppressed(ie);
      return ce;
    }
    return new MudpClosedStreamException("[" + this + "] - closed by interrupt", ie);
  }

  @Override
  public boolean isOpen() {
    return isOpenLocal(getSPIDevLocal());
  }

  @Override
  public void open() throws MudpIOException {
    streamLock.lock();
    try {
      incomeMudpPacketQueue = new LinkedBlockingQueue<>();

      {
        List<BlockingQueue<MudpPacket>> localOutcomeQueues = new ArrayList<>(spiChainSlavesCount);
        for (int i = 0; i < spiChainSlavesCount; i++) {
          localOutcomeQueues.add(new LinkedBlockingQueue<>());
        }
        outcomeMudpServicePacketQueues = Collections.unmodifiableList(localOutcomeQueues);
      }
      {
        List<BlockingQueue<MudpPacket>> localOutcomeQueues = new ArrayList<>(spiChainSlavesCount);
        for (int i = 0; i < spiChainSlavesCount; i++) {
          localOutcomeQueues.add(new LinkedBlockingQueue<>());
        }
        outcomeMudpDataPacketQueues = Collections.unmodifiableList(localOutcomeQueues);
      }

      spiDev = new SPIDev(spiDevAddress, new SPIOptions(SPIOptions.Mode.SPI_MODE_1, SPIOptions.Lsb.MSB_FIRST, 0, SPI_DEV_CLK));
      spiDev.open();

      streamThread = new MudpStreamSPIDevThread(spiDev, incomeMudpPacketQueue, outcomeMudpServicePacketQueues, outcomeMudpDataPacketQueues);
      streamThread.setDaemon(true);
      streamThread.setUncaughtExceptionHandler((thread, e) -> logger.error("[{}] - get uncaught Exception", thread, e));
      streamThread.start();
    } catch (DevException e) {
      throw new MudpIOException(e);
    } finally {
      streamLock.unlock();
      updateToS();
    }
  }

  @Override
  public void close() throws MudpIOException {
    streamLock.lock();
    try {
      if (!isOpenLocal(spiDev)) {
        return;
      }

      incomeMudpPacketQueue.add(CLOSE_MUDP_PACKET);
      incomeMudpPacketQueue = null;
      outcomeMudpServicePacketQueues = null;
      outcomeMudpDataPacketQueues = null;

      spiDev.close();
      spiDev = null;

      streamThread.interrupt();
      streamThread.join();
      streamThread = null;
    } catch (DevException | InterruptedException e) {
      throw new MudpIOException(e);
    } finally {
      streamLock.unlock();
      updateToS();
    }
  }

  @Override
  public MudpPacket readMudpPacket() throws MudpIOException, MudpPacketReadException {
    BlockingQueue<MudpPacket> incomeMudpPacketQueueLocal;
    MudpIOException streamMudpIOExceptionLocal;
    streamLock.lock();
    try {
      incomeMudpPacketQueueLocal = incomeMudpPacketQueue;
      streamMudpIOExceptionLocal = streamMudpIOException;
    } finally {
      streamLock.unlock();
    }
    if (incomeMudpPacketQueueLocal == null) {
      throw new MudpClosedStreamException(String.format("[%s] - isn't opened", this));
    }
    if (streamMudpIOExceptionLocal != null) {
      throw streamMudpIOExceptionLocal;
    }
    try {
      MudpPacket mudpPacket = incomeMudpPacketQueueLocal.take();
      if (mudpPacket == CLOSE_MUDP_PACKET) {
        incomeMudpPacketQueueLocal.put(CLOSE_MUDP_PACKET);
        throw new MudpClosedStreamException(String.format("[%s] - closed", this));
      } else if (mudpPacket == HANG_MUDP_PACKET) {
        incomeMudpPacketQueueLocal.put(HANG_MUDP_PACKET);
        streamLock.lock();
        try {
          streamMudpIOExceptionLocal = streamMudpIOException;
        } finally {
          streamLock.unlock();
        }
        if (streamMudpIOExceptionLocal != null) {
          throw streamMudpIOExceptionLocal;
        } else {
          throw new AssertionError();
        }
      } else {
        return mudpPacket;
      }
    } catch (InterruptedException e) {
      throw closeByInterrupt(e);
    }
  }

  @Override
  public void writeMudpPacket(final MudpPacket mudpPacket) throws MudpIOException, MudpPacketWriteException {
    Objects.requireNonNull(mudpPacket, "mudpPacket mustn't be null");
    int spiChainSlaveN = channelToSpiChainSlaveN(mudpPacket.getChannel());
    if (spiChainSlaveN >= spiChainSlavesCount) {
      throw new MudpIOException(String.format("[%s] - SPI chain slave [%d] can't be reach in this stream", this, spiChainSlaveN));
    }
    List<BlockingQueue<MudpPacket>> outcomeMudpPacketQueuesLocal;
    MudpIOException streamMudpIOExceptionLocal;
    streamLock.lock();
    try {
      if (mudpPacket.isDataPacket()) {
        outcomeMudpPacketQueuesLocal = outcomeMudpDataPacketQueues;
      } else {
        outcomeMudpPacketQueuesLocal = outcomeMudpServicePacketQueues;
      }
      streamMudpIOExceptionLocal = streamMudpIOException;
    } finally {
      streamLock.unlock();
    }
    if (outcomeMudpPacketQueuesLocal == null) {
      throw new MudpClosedStreamException(String.format("[%s] - isn't opened", this));
    }
    if (streamMudpIOExceptionLocal != null) {
      throw streamMudpIOExceptionLocal;
    }
    BlockingQueue<MudpPacket> outcomeMudpPacketQueueLocal = outcomeMudpPacketQueuesLocal.get(spiChainSlaveN);
    try {
      outcomeMudpPacketQueueLocal.put(mudpPacket);
    } catch (InterruptedException e) {
      throw closeByInterrupt(e);
    }
  }

  private static int channelToSpiChainSlaveN(final byte channel) {
    return Byte.toUnsignedInt(channel) / CHANNEL_N_MAX;
  }

  private static byte channelToChannelN(final byte channel) {
    return (byte) (Byte.toUnsignedInt(channel) % CHANNEL_N_MAX);
  }

  private static byte spiChainSlaveNAndChannelNToChannel(final int spiChainSlaveN, final byte channelN) {
    return (byte) (spiChainSlaveN * CHANNEL_N_MAX + channelN);
  }

  private class MudpStreamSPIDevThread extends Thread {

    private final ISPIDev spiDev;
    private final BlockingQueue<MudpPacket> incomeMudpPacketQueue;
    private final List<BlockingQueue<MudpPacket>> outcomeMudpServicePacketQueues;
    private final List<BlockingQueue<MudpPacket>> outcomeMudpDataPacketQueues;

    private final CRC16 crc16 = new CRC16();

    public MudpStreamSPIDevThread(final ISPIDev spiDev, final BlockingQueue<MudpPacket> incomeMudpPacketQueue, final List<BlockingQueue<MudpPacket>> outcomeMudpServicePacketQueues,
        final List<BlockingQueue<MudpPacket>> outcomeMudpDataPacketQueues) {
      super("MudpStreamSPIDevThread (" + spiDevAddress.toDeviceName() + ")");
      this.spiDev = spiDev;
      this.incomeMudpPacketQueue = incomeMudpPacketQueue;
      this.outcomeMudpServicePacketQueues = outcomeMudpServicePacketQueues;
      this.outcomeMudpDataPacketQueues = outcomeMudpDataPacketQueues;

      setPriority(MAX_PRIORITY - 1);
    }

    private void writeMudpPacketFrame(final MudpPacket mudpPacket, final ByteBuffer exchangeBuffer, final MudpPacket.ChannelMasquerading channelMasquerading, final int maxLength)
        throws MudpPacketWriteException {
      int startMudpPacketFramePosition = exchangeBuffer.position();
      int endMudpPacketInMudpPacketFramePosition = startMudpPacketFramePosition + maxLength;

      if (mudpPacket != null) {
        try {
          MudpPacket.encoding(mudpPacket, exchangeBuffer, channelMasquerading);
        } catch (MudpPacketWriteException e) {
          exchangeBuffer.position(startMudpPacketFramePosition);
          throw e;
        }
      } else {
        exchangeBuffer.put(EMPTY_PACKET_SPECIAL_MUDP_PROTOCOL_VERSION);
      }

      while (exchangeBuffer.position() < endMudpPacketInMudpPacketFramePosition) {
        exchangeBuffer.put((byte) 0);
      }

      crc16.reset();
      for (int i = startMudpPacketFramePosition; i < endMudpPacketInMudpPacketFramePosition; i++) {
        crc16.update(exchangeBuffer.get(i));
      }
      short actualCrc16 = (short) crc16.getValue();
      exchangeBuffer.putShort(actualCrc16);
    }

    private void writeMudpServicePacketFrame(final MudpPacket mudpPacket, final ByteBuffer exchangeBuffer, final MudpPacket.ChannelMasquerading channelMasquerading) throws MudpPacketWriteException {
      writeMudpPacketFrame(mudpPacket, exchangeBuffer, channelMasquerading, MudpPacket.SERVICE_PACKET_MAX_LENGTH);
    }

    private void writeMudpDataPacketFrame(final MudpPacket mudpPacket, final ByteBuffer exchangeBuffer, final MudpPacket.ChannelMasquerading channelMasquerading) throws MudpPacketWriteException {
      writeMudpPacketFrame(mudpPacket, exchangeBuffer, channelMasquerading, MudpPacket.DATA_PACKET_MAX_LENGTH);
    }

    private MudpPacket readMudpPacketFrame(final ByteBuffer exchangeBuffer, final MudpPacket.ChannelMasquerading channelMasquerading, final int mudpPacketLength) throws MudpPacketReadException {
      int startMudpPacketFramePosition = exchangeBuffer.position();
      int endMudpPacketInMudpPacketFramePosition = startMudpPacketFramePosition + mudpPacketLength;
      int endMudpPacketFramePosition = endMudpPacketInMudpPacketFramePosition + CRC16.LENGTH;

      crc16.reset();
      for (int i = startMudpPacketFramePosition; i < endMudpPacketInMudpPacketFramePosition; i++) {
        crc16.update(exchangeBuffer.get(i));
      }
      short expectedCrc16 = (short) crc16.getValue();
      short receivedCrc16 = exchangeBuffer.getShort(endMudpPacketInMudpPacketFramePosition);
      if (expectedCrc16 != receivedCrc16) {
        exchangeBuffer.position(endMudpPacketFramePosition);
        throw new MudpPacketReadWrongCRCException(String.format("expected CRC16 0x%04X - received CRC16 0x%04X%n%s%n", expectedCrc16, receivedCrc16,
            createDumpOfBufferRegion(exchangeBuffer, startMudpPacketFramePosition, endMudpPacketInMudpPacketFramePosition)));
      }
      // check version field of mudp packet for special value of empty packet
      if (exchangeBuffer.get(startMudpPacketFramePosition) == EMPTY_PACKET_SPECIAL_MUDP_PROTOCOL_VERSION) {
        exchangeBuffer.position(endMudpPacketFramePosition);
        return null;
      }

      try {
        return MudpPacket.decoding(exchangeBuffer, channelMasquerading);
      } finally {
        exchangeBuffer.position(endMudpPacketFramePosition);
      }
    }

    private MudpPacket readMudpServicePacketFrame(final ByteBuffer exchangeBuffer, final MudpPacket.ChannelMasquerading channelMasquerading) throws MudpPacketReadException {
      return readMudpPacketFrame(exchangeBuffer, channelMasquerading, MudpPacket.SERVICE_PACKET_MAX_LENGTH);
    }

    private MudpPacket readMudpDataPacketFrame(final ByteBuffer exchangeBuffer, final MudpPacket.ChannelMasquerading channelMasquerading) throws MudpPacketReadException {
      return readMudpPacketFrame(exchangeBuffer, channelMasquerading, MudpPacket.DATA_PACKET_MAX_LENGTH);
    }

    @Override
    public void run() {
      long exchangeIterationCounter = 0;

      int hardHangSpiChainSlaveIndex = -1;
      int hardHangSpiChainSlaveGuardCounter = 0;

      MudpPacket.ChannelMasquerading writeChannelMasquerading[] = new MudpPacket.ChannelMasquerading[spiChainSlavesCount];
      MudpPacket.ChannelMasquerading readChannelMasquerading[] = new MudpPacket.ChannelMasquerading[spiChainSlavesCount];
      for (int i = 0; i < spiChainSlavesCount; i++) {
        final int spiChainSlaveN = i;
        writeChannelMasquerading[i] = channel -> channelToChannelN(channel);
        readChannelMasquerading[i] = channel -> spiChainSlaveNAndChannelNToChannel(spiChainSlaveN, channel);
      }

      statistics.init();

      while (!Thread.interrupted()) {
        if (hardHangSpiChainSlaveGuardCounter >= 3) {
          streamLock.lock();
          try {
            streamMudpIOException = new MudpHangStreamException("Hard hang of SPI chain slave [" + hardHangSpiChainSlaveIndex + "]");
          } finally {
            streamLock.unlock();
          }
          incomeMudpPacketQueue.add(HANG_MUDP_PACKET);
          break;
        }

        int hangSpiChainSlaveIndex = Integer.MIN_VALUE;

        for (int i = spiChainSlavesCount - 1; i >= 0; i--) { // data for last slave sends first because of SPI chain
          Queue<MudpPacket> outcomeMudpServicePacketQueue = outcomeMudpServicePacketQueues.get(i);
          for (int j = 0; j < mudpServicePacketsCountPerSlave; j++) {
            MudpPacket mudpServicePacket = outcomeMudpServicePacketQueue.poll();
            try {
              writeMudpServicePacketFrame(mudpServicePacket, exchangeBuffer, writeChannelMasquerading[i]);
            } catch (MudpPacketWriteException e) {
              logger.warn("[{}] iteration [{}] - can't write Service Packet [{}] to exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, mudpServicePacket, j, i, e);
            }
            if (mudpServicePacket != null) {
              logger.trace("[{}] iteration [{}] - write Service Packet [{}] to exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, mudpServicePacket, j, i);
              statistics.handleWriteMudpServicePacketFrame(i);
            }
          }
          Queue<MudpPacket> outcomeMudpDataPacketQueue = outcomeMudpDataPacketQueues.get(i);
          for (int j = 0; j < mudpDataPacketsCountPerSlave; j++) {
            MudpPacket mudpDataPacket = outcomeMudpDataPacketQueue.poll();
            try {
              writeMudpDataPacketFrame(mudpDataPacket, exchangeBuffer, writeChannelMasquerading[i]);
            } catch (MudpPacketWriteException e) {
              logger.warn("[{}] iteration [{}] - can't write Data Packet [{}] to exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, mudpDataPacket, j, i, e);
            }
            if (mudpDataPacket != null) {
              logger.trace("[{}] iteration [{}] - write Data Packet [{}] to exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, mudpDataPacket, j, i);
              statistics.handleWriteMudpDataPacketFrame(i);
            }
          }
        }
        exchangeBuffer.flip();

        try {
          spiDev.exchange(exchangeBuffer);
        } catch (DevClosedException e) {
          break;
        } catch (DevException e) {
          logger.warn("[{}] iteration [{}] - error during data exchanging", this, exchangeIterationCounter, e);
          continue;
        }

        for (int i = spiChainSlavesCount - 1; i >= 0; i--) { // data from last slave receives first because of SPI chain
          int anyReadExceptionInARowCounter = 0;

          for (int j = 0; j < mudpServicePacketsCountPerSlave; j++) {
            MudpPacket mudpServicePacket = null;
            try {
              mudpServicePacket = readMudpServicePacketFrame(exchangeBuffer, readChannelMasquerading[i]);
            } catch (MudpPacketReadException e) {
              logger.warn("[{}] iteration [{}] - can't read Service Packet from exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, j, i, e);
              anyReadExceptionInARowCounter++;
              statistics.handleReadMudpServicePacketException(i);
            }
            if (mudpServicePacket != null) {
              logger.trace("[{}] iteration [{}] - read Service Packet [{}] from exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, mudpServicePacket, j, i);
              statistics.handleReadMudpServicePacketFrame(i);
              if (!incomeMudpPacketQueue.offer(mudpServicePacket)) {
                logger.warn("[{}] iteration [{}] - can't add Service Packet [{}] to income queue - packet dropped", this, exchangeIterationCounter, mudpServicePacket);
              }
            }
          }
          for (int j = 0; j < mudpDataPacketsCountPerSlave; j++) {
            MudpPacket mudpDataPacket = null;
            try {
              mudpDataPacket = readMudpDataPacketFrame(exchangeBuffer, readChannelMasquerading[i]);
            } catch (MudpPacketReadException e) {
              logger.warn("[{}] iteration [{}] - can't read Data Packet from exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, j, i, e);
              anyReadExceptionInARowCounter++;
              statistics.handleReadMudpDataPacketException(i);
            }
            if (mudpDataPacket != null) {
              logger.trace("[{}] iteration [{}] - read Data Packet [{}] from exchange buffer slot [{}] of SPI chain slave [{}]", this, exchangeIterationCounter, mudpDataPacket, j, i);
              statistics.handleReadMudpDataPacketFrame(i);
              if (!incomeMudpPacketQueue.offer(mudpDataPacket)) {
                logger.warn("[{}] iteration [{}] - can't add Data Packet [{}] to income queue - packet dropped", this, exchangeIterationCounter, mudpDataPacket);
              }
            }
          }

          if (hangSpiChainSlaveIndex < i && anyReadExceptionInARowCounter == (mudpServicePacketsCountPerSlave + mudpDataPacketsCountPerSlave)) {
            hangSpiChainSlaveIndex = i;
          }
        }
        exchangeBuffer.clear();

        if (hangSpiChainSlaveIndex >= 0) {
          if (hardHangSpiChainSlaveIndex == hangSpiChainSlaveIndex) {
            hardHangSpiChainSlaveGuardCounter++;
          } else {
            hardHangSpiChainSlaveIndex = hangSpiChainSlaveIndex;
            hardHangSpiChainSlaveGuardCounter = 1;
          }
        } else {
          hardHangSpiChainSlaveIndex = Integer.MIN_VALUE;
          hardHangSpiChainSlaveGuardCounter = 0;
        }

        statistics.iterate();

        exchangeIterationCounter++;
      }
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode());
    }

    private String createDumpOfBufferRegion(final ByteBuffer bb, final int beginRegionPosition, final int endRegionPosition) {
      int originPosition = bb.position();
      int originLimit = bb.limit();
      bb.position(0).limit(bb.capacity()); // prevent possible Exception in case of creating HexMatrixString from completely different range compare to origin
      bb.position(beginRegionPosition).limit(endRegionPosition);
      String hexMatrixString = DataRepresentationHelper.toHexMatrixString(bb);
      bb.position(0).limit(bb.capacity());
      bb.position(originPosition).limit(originLimit);
      return hexMatrixString;
    }
  }

  private void updateToS() {
    String spiDevAddressStatus;
    if (isOpen()) {
      spiDevAddressStatus = spiDevAddress.toDeviceName();
    } else {
      spiDevAddressStatus = "not opened";
    }
    atomicToS.set(String.format("%s@%x:[%s]", getClass().getSimpleName(), hashCode(), spiDevAddressStatus));
  }

  @Override
  public String toString() {
    return atomicToS.get();
  }

  public interface IStatistics {
    void init();

    void handleWriteMudpServicePacketFrame(int spiChainSlaveN);

    void handleWriteMudpDataPacketFrame(int spiChainSlaveN);

    void handleReadMudpServicePacketFrame(int spiChainSlaveN);

    void handleReadMudpDataPacketFrame(int spiChainSlaveN);

    void handleReadMudpServicePacketException(int spiChainSlaveN);

    void handleReadMudpDataPacketException(int spiChainSlaveN);

    void iterate();
  }

}
