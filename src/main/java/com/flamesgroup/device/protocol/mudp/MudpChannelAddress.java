/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

public final class MudpChannelAddress {

  private final byte channel;

  public MudpChannelAddress(final byte channel) {
    this.channel = channel;
  }

  public MudpChannelAddress() {
    this((byte) 0);
  }

  public byte getChannel() {
    return channel;
  }

  @Override
  public int hashCode() {
    return Byte.toUnsignedInt(channel);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o instanceof MudpChannelAddress) {
      MudpChannelAddress that = (MudpChannelAddress) o;
      return channel == that.channel;
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) + ":[C=" + Byte.toUnsignedInt(channel) + "]";
  }

}
