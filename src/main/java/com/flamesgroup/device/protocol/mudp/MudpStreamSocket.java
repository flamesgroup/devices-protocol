/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import com.flamesgroup.device.helper.DataRepresentationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.NotYetConnectedException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class MudpStreamSocket implements IMudpStream {

  private final Logger logger = LoggerFactory.getLogger(MudpStreamSocket.class);

  private final ByteBuffer readBuffer = ByteBuffer.allocateDirect(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
  private final ByteBuffer writeBuffer = ByteBuffer.allocateDirect(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);

  private final Lock streamLock = new ReentrantLock();
  private final AtomicReference<String> atomicToS = new AtomicReference<>();

  private final SocketAddress socketAddress;

  private DatagramChannel mudpDatagramChannel;

  public MudpStreamSocket(final SocketAddress socketAddress) {
    Objects.requireNonNull(socketAddress, "socketAddress mustn't be null");
    this.socketAddress = socketAddress;

    updateToS();
  }

  private boolean isOpenLocal(final DatagramChannel mudpDatagramChannelLocal) {
    return mudpDatagramChannelLocal != null && mudpDatagramChannelLocal.isConnected();
  }

  private DatagramChannel getMudpDatagramChannelLocal() {
    streamLock.lock();
    try {
      return mudpDatagramChannel;
    } finally {
      streamLock.unlock();
    }
  }

  @Override
  public boolean isOpen() {
    return isOpenLocal(getMudpDatagramChannelLocal());
  }

  @Override
  public void open() throws MudpIOException {
    streamLock.lock();
    try {
      mudpDatagramChannel = DatagramChannel.open();
      mudpDatagramChannel.configureBlocking(true);
      mudpDatagramChannel.connect(socketAddress);
    } catch (ClosedChannelException e) {
      throw new MudpClosedStreamException(e);
    } catch (IOException e) {
      throw new MudpIOException(e);
    } finally {
      streamLock.unlock();
      updateToS();
    }
  }

  @Override
  public void close() throws MudpIOException {
    streamLock.lock();
    try {
      mudpDatagramChannel.close();
      mudpDatagramChannel = null;
    } catch (IOException e) {
      throw new MudpIOException(e);
    } finally {
      streamLock.unlock();
      updateToS();
    }
  }

  @Override
  public MudpPacket readMudpPacket() throws MudpIOException, MudpPacketReadException {
    DatagramChannel mudpDatagramChannelLocal = getMudpDatagramChannelLocal();
    if (!isOpenLocal(mudpDatagramChannelLocal)) {
      throw new MudpClosedStreamException();
    }

    synchronized (readBuffer) {
      readBuffer.clear();
      try {
        mudpDatagramChannelLocal.read(readBuffer);
      } catch (NotYetConnectedException | ClosedChannelException e) {
        throw new MudpClosedStreamException(e);
      } catch (IOException e) {
        throw new MudpIOException(e);
      }
      readBuffer.flip();
      try {
        readBuffer.mark();
        return MudpPacket.decoding(readBuffer);
      } catch (MudpPacketReadException e) {
        readBuffer.reset();
        logger.warn("[" + this + "] - can't decode read buffer [" + DataRepresentationHelper.toHexArrayString(readBuffer) + "]", e);
        throw e;
      }
    }
  }

  @Override
  public void writeMudpPacket(final MudpPacket mudpPacket) throws MudpIOException, MudpPacketWriteException {
    DatagramChannel mudpDatagramChannelLocal = getMudpDatagramChannelLocal();
    if (!isOpenLocal(mudpDatagramChannelLocal)) {
      throw new MudpClosedStreamException();
    }

    synchronized (writeBuffer) {
      writeBuffer.clear();
      try {
        MudpPacket.encoding(mudpPacket, writeBuffer);
      } catch (MudpPacketWriteException e) {
        logger.warn("[" + this + "] - can't encode write packet [" + mudpPacket + "]", e);
        throw e;
      }
      writeBuffer.flip();
      try {
        mudpDatagramChannelLocal.write(writeBuffer);
      } catch (NotYetConnectedException | ClosedChannelException e) {
        throw new MudpClosedStreamException(e);
      } catch (IOException e) {
        throw new MudpIOException(e);
      }
    }
  }

  private void updateToS() {
    String remoteAddressStatus;
    final DatagramChannel mudpDatagramChannelLocal = getMudpDatagramChannelLocal();
    if (mudpDatagramChannelLocal != null) {
      try {
        remoteAddressStatus = mudpDatagramChannelLocal.getRemoteAddress().toString();
      } catch (IOException e) {
        remoteAddressStatus = e.getMessage();
      }
    } else {
      remoteAddressStatus = "not opened";
    }
    atomicToS.set(String.format("%s@%x:[%s]", getClass().getSimpleName(), hashCode(), remoteAddressStatus));
  }

  @Override
  public String toString() {
    return atomicToS.get();
  }

}
