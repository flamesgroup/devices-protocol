/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

public final class MudpOptions {

  private final int attemptCount;
  private final long attemptTimeout;
  private final int retransmitCount;
  private final long retransmitTimeout;

  public MudpOptions(final int attemptCount, final long attemptTimeout, final int retransmitCount, final long retransmitTimeout) {
    if (attemptCount > 255) {
      throw new IllegalArgumentException("Attempt Count can't be greater than 255 but it's " + attemptCount);
    }
    if (attemptTimeout > 65535) {
      throw new IllegalArgumentException("Attempt Timeout can't be greater than 65535 but it's " + attemptTimeout);
    }
    if (retransmitCount > 255) {
      throw new IllegalArgumentException("Retransmit Count can't be greater than 255 but it's " + attemptCount);
    }
    if (retransmitTimeout > 65535) {
      throw new IllegalArgumentException("Retransmit Timeout can't be greater than 65535 but it's " + retransmitTimeout);
    }
    this.attemptCount = attemptCount;
    this.attemptTimeout = attemptTimeout;
    this.retransmitCount = retransmitCount;
    this.retransmitTimeout = retransmitTimeout;
  }

  public int getAttemptCount() {
    return attemptCount;
  }

  public long getAttemptTimeout() {
    return attemptTimeout;
  }

  public int getRetransmitCount() {
    return retransmitCount;
  }

  public long getRetransmitTimeout() {
    return retransmitTimeout;
  }

  @Override
  public String toString() {
    return String.format("%s@%x:[%d/%d/%d/%d]", getClass().getSimpleName(), hashCode(), attemptCount, attemptTimeout, retransmitCount, retransmitTimeout);
  }

}
