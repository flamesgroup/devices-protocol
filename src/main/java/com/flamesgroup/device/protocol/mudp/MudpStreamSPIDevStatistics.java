/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public final class MudpStreamSPIDevStatistics implements MudpStreamSPIDev.IStatistics {

  private final Logger logger = LoggerFactory.getLogger(MudpStreamSPIDevStatistics.class);

  private final int spiChainSlavesCount;
  private final int mudpServicePacketsCountPerSlave;
  private final int mudpDataPacketsCountPerSlave;

  private final int statisticsGatheringIterationAmount;
  private int statisticsIterationCounter;

  private int exchangeIterationDurationMin;
  private int exchangeIterationDurationMax;
  private int exchangeIterationDurationSum;

  private final int qualityAMultiplier = 2;
  private final int qualityBMultiplier = 3;
  private final int qualityCMultiplier = 5;
  private final int qualityDMultiplier = 10;
  private final int exchangeIterationDurationQualityALimit;
  private final int exchangeIterationDurationQualityBLimit;
  private final int exchangeIterationDurationQualityCLimit;
  private final int exchangeIterationDurationQualityDLimit;
  private int exchangeIterationDurationQualityACounter;
  private int exchangeIterationDurationQualityBCounter;
  private int exchangeIterationDurationQualityCCounter;
  private int exchangeIterationDurationQualityDCounter;
  private int exchangeIterationDurationQualityFCounter;

  private final int mudpServicePacketFrameCapacityMaxPerGathering;
  private final int mudpDataPacketFrameCapacityMaxPerGathering;
  private final int writeMudpServicePacketFrameCounterPerSlave[];
  private final int writeMudpDataPacketFrameCounterPerSlave[];
  private final int readMudpServicePacketFrameCounterPerSlave[];
  private final int readMudpDataPacketFrameCounterPerSlave[];
  private final int errorMudpServicePacketFrameCounterPerSlave[];
  private final int errorMudpDataPacketFrameCounterPerSlave[];

  private long prevIterationTimeMillis;
  private long prevStatisticsGatheringTimeMillis;

  public MudpStreamSPIDevStatistics(final MudpStreamSPIDev mudpStreamSPIDev, final int statisticsGatheringIterationAmount) {
    spiChainSlavesCount = mudpStreamSPIDev.getSPIChainSlavesCount();
    mudpServicePacketsCountPerSlave = mudpStreamSPIDev.getMudpServicePacketsCountPerSlave();
    mudpDataPacketsCountPerSlave = mudpStreamSPIDev.getMudpDataPacketsCountPerSlave();
    this.statisticsGatheringIterationAmount = statisticsGatheringIterationAmount;

    int theoreticalExchangeIterationDuration = mudpStreamSPIDev.getTheoreticalExchangeIterationDuration();
    exchangeIterationDurationQualityALimit = theoreticalExchangeIterationDuration * qualityAMultiplier;
    exchangeIterationDurationQualityBLimit = theoreticalExchangeIterationDuration * qualityBMultiplier;
    exchangeIterationDurationQualityCLimit = theoreticalExchangeIterationDuration * qualityCMultiplier;
    exchangeIterationDurationQualityDLimit = theoreticalExchangeIterationDuration * qualityDMultiplier;

    mudpServicePacketFrameCapacityMaxPerGathering = mudpServicePacketsCountPerSlave * statisticsGatheringIterationAmount;
    mudpDataPacketFrameCapacityMaxPerGathering = mudpDataPacketsCountPerSlave * statisticsGatheringIterationAmount;
    writeMudpServicePacketFrameCounterPerSlave = new int[spiChainSlavesCount];
    writeMudpDataPacketFrameCounterPerSlave = new int[spiChainSlavesCount];
    readMudpServicePacketFrameCounterPerSlave = new int[spiChainSlavesCount];
    readMudpDataPacketFrameCounterPerSlave = new int[spiChainSlavesCount];
    errorMudpServicePacketFrameCounterPerSlave = new int[spiChainSlavesCount];
    errorMudpDataPacketFrameCounterPerSlave = new int[spiChainSlavesCount];
  }

  private void reset() {
    statisticsIterationCounter = 0;

    exchangeIterationDurationMin = Integer.MAX_VALUE;
    exchangeIterationDurationMax = Integer.MIN_VALUE;
    exchangeIterationDurationSum = 0;

    exchangeIterationDurationQualityACounter = 0;
    exchangeIterationDurationQualityBCounter = 0;
    exchangeIterationDurationQualityCCounter = 0;
    exchangeIterationDurationQualityDCounter = 0;
    exchangeIterationDurationQualityFCounter = 0;

    Arrays.fill(writeMudpServicePacketFrameCounterPerSlave, 0);
    Arrays.fill(writeMudpDataPacketFrameCounterPerSlave, 0);
    Arrays.fill(readMudpServicePacketFrameCounterPerSlave, 0);
    Arrays.fill(readMudpDataPacketFrameCounterPerSlave, 0);
    Arrays.fill(errorMudpServicePacketFrameCounterPerSlave, 0);
    Arrays.fill(errorMudpDataPacketFrameCounterPerSlave, 0);
  }

  @Override
  public void init() {
    reset();
    prevIterationTimeMillis = prevStatisticsGatheringTimeMillis = System.currentTimeMillis();

    StringBuilder sb = new StringBuilder();
    appendDescription(sb);
    logger.info("[{}]{}{}", this, System.lineSeparator(), sb.toString());
  }

  @Override
  public void handleWriteMudpServicePacketFrame(final int spiChainSlaveN) {
    writeMudpServicePacketFrameCounterPerSlave[spiChainSlaveN]++;
  }

  @Override
  public void handleWriteMudpDataPacketFrame(final int spiChainSlaveN) {
    writeMudpDataPacketFrameCounterPerSlave[spiChainSlaveN]++;
  }

  @Override
  public void handleReadMudpServicePacketFrame(final int spiChainSlaveN) {
    readMudpServicePacketFrameCounterPerSlave[spiChainSlaveN]++;
  }

  @Override
  public void handleReadMudpDataPacketFrame(final int spiChainSlaveN) {
    readMudpDataPacketFrameCounterPerSlave[spiChainSlaveN]++;
  }

  @Override
  public void handleReadMudpServicePacketException(final int spiChainSlaveN) {
    errorMudpServicePacketFrameCounterPerSlave[spiChainSlaveN]++;
  }

  @Override
  public void handleReadMudpDataPacketException(final int spiChainSlaveN) {
    errorMudpDataPacketFrameCounterPerSlave[spiChainSlaveN]++;
  }

  private boolean isStatisticsReady() {
    return statisticsIterationCounter++ > statisticsGatheringIterationAmount;
  }

  private int calculateStatisticsGatheringDuration(final long currentTimeMillis) {
    long statisticsGatheringDuration = currentTimeMillis - prevStatisticsGatheringTimeMillis;
    prevStatisticsGatheringTimeMillis = currentTimeMillis;
    if (statisticsGatheringDuration > Integer.MAX_VALUE) {
      logger.warn("[{}] - actual statistics gathering duration [{}] is greater than maximum integer number", this, statisticsGatheringDuration);
      return Integer.MAX_VALUE;
    } else {
      return (int) statisticsGatheringDuration;
    }
  }

  private int calculateExchangeIterationDuration(final long currentTimeMillis) {
    long exchangeIterationDuration = currentTimeMillis - prevIterationTimeMillis;
    prevIterationTimeMillis = currentTimeMillis;
    if (exchangeIterationDuration > Integer.MAX_VALUE) {
      logger.warn("[{}] - actual exchange iteration duration [{}] is greater than maximum integer number", this, exchangeIterationDuration);
      return Integer.MAX_VALUE;
    } else {
      return (int) exchangeIterationDuration;
    }
  }

  private int percent(final int n, final int v) {
    return (n * 100) / v;
  }

  @Override
  public void iterate() {
    long currentTimeMillis = System.currentTimeMillis();

    if (isStatisticsReady()) {
      StringBuilder sb = new StringBuilder();

      long statisticsGatheringDuration = calculateStatisticsGatheringDuration(currentTimeMillis);

      int exchangeIterationDurationAvg = exchangeIterationDurationSum / statisticsGatheringIterationAmount;

      sb.append("SGI                  = ").append(statisticsGatheringIterationAmount).append(System.lineSeparator());
      sb.append("SGD  ms              = ").append(statisticsGatheringDuration).append(System.lineSeparator());
      sb.append("EID  ms  min/avg/max = ").append(exchangeIterationDurationMin).append('/').append(exchangeIterationDurationAvg).append('/').append(exchangeIterationDurationMax)
          .append(System.lineSeparator());
      sb.append("EIDQ cnt A/B/C/D/F   = ").append(exchangeIterationDurationQualityACounter).append('/').append(exchangeIterationDurationQualityBCounter).append('/')
          .append(exchangeIterationDurationQualityCCounter).append('/').append(exchangeIterationDurationQualityDCounter).append('/').append(exchangeIterationDurationQualityFCounter)
          .append(System.lineSeparator());
      sb.append("BU").append(System.lineSeparator());
      sb.append(" W   %   ");
      appendCapacityUsage(sb, writeMudpServicePacketFrameCounterPerSlave, writeMudpDataPacketFrameCounterPerSlave);
      sb.append(" R   %   ");
      appendCapacityUsage(sb, readMudpServicePacketFrameCounterPerSlave, readMudpDataPacketFrameCounterPerSlave);
      sb.append("ERR  cnt ");
      appendErrorCounter(sb, errorMudpServicePacketFrameCounterPerSlave, errorMudpDataPacketFrameCounterPerSlave);

      logger.info("[{}]{}{}", this, System.lineSeparator(), sb.toString());

      reset();
    }

    int exchangeIterationDuration = calculateExchangeIterationDuration(currentTimeMillis);

    if (exchangeIterationDurationMin > exchangeIterationDuration) {
      exchangeIterationDurationMin = exchangeIterationDuration;
    }
    if (exchangeIterationDurationMax < exchangeIterationDuration) {
      exchangeIterationDurationMax = exchangeIterationDuration;
    }
    exchangeIterationDurationSum += exchangeIterationDuration;

    if (exchangeIterationDuration < exchangeIterationDurationQualityALimit) {
      exchangeIterationDurationQualityACounter++;
    } else if (exchangeIterationDuration < exchangeIterationDurationQualityBLimit) {
      exchangeIterationDurationQualityBCounter++;
    } else if (exchangeIterationDuration < exchangeIterationDurationQualityCLimit) {
      exchangeIterationDurationQualityCCounter++;
    } else if (exchangeIterationDuration < exchangeIterationDurationQualityDLimit) {
      exchangeIterationDurationQualityDCounter++;
    } else {
      exchangeIterationDurationQualityFCounter++;
    }

  }

  private void appendCapacityUsage(final StringBuilder sb, final int[] mudpServicePacketFrameCounterPerSlave, final int[] mudpDataPacketFrameCounterPerSlave) {
    sb.append('|');
    for (int i = 0; i < spiChainSlavesCount; i++) {
      int writeMudpServicePacketFrameCapacityUsagePercent = percent(mudpServicePacketFrameCounterPerSlave[i], mudpServicePacketFrameCapacityMaxPerGathering);
      int writeMudpDataPacketFrameCapacityUsagePercent = percent(mudpDataPacketFrameCounterPerSlave[i], mudpDataPacketFrameCapacityMaxPerGathering);
      {
        appendIntegerWithMinLength(sb, writeMudpServicePacketFrameCapacityUsagePercent, 3);
      }
      sb.append('/');
      {
        appendIntegerWithMinLength(sb, writeMudpDataPacketFrameCapacityUsagePercent, 3);
      }
      sb.append('|');
    }
    sb.append(System.lineSeparator());
  }

  private void appendErrorCounter(final StringBuilder sb, final int[] errorMudpServicePacketFrameCounterPerSlave, final int[] errorMudpDataPacketFrameCounterPerSlave) {
    sb.append('|');
    for (int i = 0; i < spiChainSlavesCount; i++) {
      {
        if (errorMudpServicePacketFrameCounterPerSlave[i] >= 100) {
          sb.append(">99");
        } else {
          appendIntegerWithMinLength(sb, errorMudpServicePacketFrameCounterPerSlave[i], 3);
        }
      }
      sb.append('/');
      {
        if (errorMudpDataPacketFrameCounterPerSlave[i] >= 100) {
          sb.append(">99");
        } else {
          appendIntegerWithMinLength(sb, errorMudpDataPacketFrameCounterPerSlave[i], 3);
        }
      }
      sb.append('|');
    }
    sb.append(System.lineSeparator());
  }

  private void appendIntegerWithMinLength(final StringBuilder sb, final int v, final int minLength) {
    String vs = Integer.toString(v);
    for (int i = vs.length(); i < minLength; i++) {
      sb.append(' ');
    }
    sb.append(vs);
  }

  private void appendDescription(final StringBuilder sb) {
    sb.append("SGI                  - number of Statistics Gathering Iteration").append(System.lineSeparator());
    sb.append("SGD  ms              - Statistics Gathering Duration in ms").append(System.lineSeparator());
    sb.append("EID  ms  min/avg/max - Exchange Iteration Duration min/avg/max in ms").append(System.lineSeparator());
    sb.append("EIDQ cnt A/B/C/D/F   - number of Exchange Iteration Duration by Quality").append(System.lineSeparator());
    sb.append("                       where").append(System.lineSeparator());
    sb.append("                         A - less than theoretical exchange iteration duration multiply by ").append(qualityAMultiplier).append(System.lineSeparator());
    sb.append("                         B - less than theoretical exchange iteration duration multiply by ").append(qualityBMultiplier).append(System.lineSeparator());
    sb.append("                         C - less than theoretical exchange iteration duration multiply by ").append(qualityCMultiplier).append(System.lineSeparator());
    sb.append("                         D - less than theoretical exchange iteration duration multiply by ").append(qualityDMultiplier).append(System.lineSeparator());
    sb.append("                         F - everything else").append(System.lineSeparator());
    sb.append("BU                   - Bandwidth Usage").append(System.lineSeparator());
    sb.append(" W   %               - percent of Write bandwidth usage for service/data packet frames on each SPI chain slave").append(System.lineSeparator());
    sb.append(" R   %               - percent of Read bandwidth usage for service/data packet frames on each SPI chain slave").append(System.lineSeparator());
    sb.append("ERR  cnt             - number of read Error for service/data packet frames on each SPI chain slave").append(System.lineSeparator());
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

}
