/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class MudpChannel implements IMudpChannel {

  static final byte ERRCODE_NO_ERR = (byte) 0x00;
  static final byte ERRCODE_INTERNAL_ERROR = (byte) 0x80;
  static final byte ERRCODE_INCOMPATIBLE_VERSION = (byte) 0x01;
  static final byte ERRCODE_INVALID_DATA = (byte) 0x02;
  static final byte ERRCODE_CHANNEL_NOT_EXIST = (byte) 0x10;
  static final byte ERRCODE_CHANNEL_UNENSLAVED = (byte) 0x11;
  static final byte ERRCODE_CHANNEL_BUSY = (byte) 0x12;
  static final byte ERRCODE_LICENCE_FAILURE = (byte) 0x20;

  private enum State {
    FREE, ENSLAVING, ENSLAVED
  }

  private enum ReliableState {
    IDLE, ACK_WAITING, ACK_RECEIVED
  }

  private final Logger logger = LoggerFactory.getLogger(MudpChannel.class);

  private final MudpConnection mudpConnection;
  private final MudpOptions mudpOptions;

  private MudpChannelAddress mudpChannelAddress;
  private IMudpChannelHandler mudpChannelHandler;

  private State state = State.FREE;
  private MudpErrCodeException stateErrCodeException;
  private MudpErrCodeException dataErrCodeException;
  private byte sendSequence;
  private byte receiveSequence;

  private final Lock simultaneousAccessLock = new ReentrantLock();
  private final Lock channelLock = new ReentrantLock();
  private final Condition enslaveAckCondition = channelLock.newCondition();
  private final Condition reliableAckCondition = channelLock.newCondition();

  private ReliableState reliableState = ReliableState.IDLE;

  private final String toS;

  public MudpChannel(final MudpConnection mudpConnection, final MudpOptions mudpOptions) {
    if (mudpConnection == null) {
      throw new IllegalArgumentException("Mudp Connection must not be null");
    }
    if (mudpOptions == null) {
      throw new IllegalArgumentException("Mudp Options must not be null");
    }
    this.mudpConnection = mudpConnection;
    this.mudpOptions = mudpOptions;

    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}]", toS, mudpConnection);
    }
  }

  @Override
  public boolean isEnslaved() {
    channelLock.lock();
    try {
      return state == State.ENSLAVED;
    } finally {
      channelLock.unlock();
    }
  }

  @Override
  public void enslave(final MudpChannelAddress mudpChannelAddress, final IMudpChannelHandler mudpChannelHandler) throws MudpException, InterruptedException {
    if (mudpChannelAddress == null) {
      throw new IllegalArgumentException("MudpChannelAddress must not be null");
    }
    if (mudpChannelHandler == null) {
      throw new IllegalArgumentException("MudpChannelHandler must not be null");
    }

    final ByteBuffer mudpEnslavePacketDataBuffer = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN);
    mudpEnslavePacketDataBuffer.clear();
    mudpEnslavePacketDataBuffer.put((byte) mudpOptions.getAttemptCount());
    mudpEnslavePacketDataBuffer.putShort((short) mudpOptions.getAttemptTimeout());
    mudpEnslavePacketDataBuffer.putShort((short) mudpOptions.getRetransmitTimeout());
    mudpEnslavePacketDataBuffer.flip();

    simultaneousAccessLock.lock();
    try {
      channelLock.lock();
      try {
        if (state != State.FREE) {
          throw new MudpChannelAlreadyEnslavedException();
        }
        this.mudpChannelHandler = mudpChannelHandler;
        this.mudpChannelAddress = mudpChannelAddress;
        mudpConnection.addToEnslaved(mudpChannelAddress, this);
        state = State.ENSLAVING;
      } finally {
        channelLock.unlock();
      }

      int attemptCount = mudpOptions.getAttemptCount();
      int attemptDecIteration = attemptCount;
      while (true) {
        MudpPacket mudpEnslavePacket =
            new MudpPacket(mudpEnslavePacketDataBuffer).setVersion(MudpConnection.MUDP_PROTOCOL_VERSION).setChannel(mudpChannelAddress.getChannel()).setMSFlag(true).setEEFlag(true)
                .setErrCode((byte) 0).setType((byte) 0);

        try {
          mudpConnection.writeMudpPacket(mudpEnslavePacket);
        } catch (MudpIOException e) {
          if (--attemptDecIteration > 0) {
            logger.warn("[{}] - writing Enslave Packet [{}] in [{}] on attempt [{} of {}] problem",
                this, mudpEnslavePacket, mudpConnection, (attemptCount - attemptDecIteration), attemptCount, e);
            continue;
          } else {
            throw new MudpIOException("[" + this + "] - failed to send Enslave Packet [" + mudpEnslavePacket + "]", e);
          }
        }

        channelLock.lock();
        try {
          if (state == State.ENSLAVING) {
            if (!enslaveAckCondition.await(mudpOptions.getAttemptTimeout(), TimeUnit.MILLISECONDS)) {
              if (--attemptDecIteration > 0) {
                logger.warn("[{}] - waiting Enslave Ack Packet for Enslave Packet [{}] on attempt [{} of {}] timeout",
                    this, mudpEnslavePacket, (attemptCount - attemptDecIteration), attemptCount);
                continue;
              } else {
                throw new MudpChannelTimeoutException("Enslave timeout");
              }
            }
          }
          if (state == State.FREE) { // channel was closed while wait for Enslave Ack Packet
            throw new MudpChannelAsynchronousFreeChannelException();
          } else if (state == State.ENSLAVED) {
            if (stateErrCodeException != null) {
              throw stateErrCodeException;
            } else {
              sendSequence = 0;
              receiveSequence = 0;
              return;
            }
          } else {
            throw new AssertionError(); // must not be here
          }
        } finally {
          channelLock.unlock();
        }
      }
    } catch (MudpIOException | MudpChannelTimeoutException | MudpErrCodeException | InterruptedException e) {
      state = State.FREE;
      mudpConnection.removeFromEnslaved(mudpChannelAddress);
      this.mudpChannelHandler = null;
      this.mudpChannelAddress = null;
      throw e;
    } finally {
      simultaneousAccessLock.unlock();
    }
  }

  @Override
  public void free() {
    channelLock.lock();
    try {
      if (state != State.FREE) {
        enslaveAckCondition.signalAll();
        reliableAckCondition.signalAll();
        state = State.FREE;
        mudpConnection.removeFromEnslaved(mudpChannelAddress);
        mudpChannelHandler = null;
        mudpChannelAddress = null;
      }
    } finally {
      channelLock.unlock();
    }
  }

  @Override
  public void sendReliable(final byte type, final ByteBuffer data) throws MudpException, InterruptedException {
    if (data == null) {
      throw new IllegalArgumentException("Data must not be null");
    }

    simultaneousAccessLock.lock();
    try {
      MudpChannelAddress currentMudpChannelAddress;
      byte currentSendSequence;

      channelLock.lock();
      try {
        if (state != State.ENSLAVED) {
          throw new MudpChannelFreedChannelException();
        }
        currentMudpChannelAddress = mudpChannelAddress;
        currentSendSequence = getSendSequence();
      } finally {
        channelLock.unlock();
      }

      int attemptCount = mudpOptions.getAttemptCount();
      int attemptDecIteration = attemptCount;
      int retransmitAttemptCount = mudpOptions.getRetransmitCount();
      int retransmitDecIteration = retransmitAttemptCount;
      while (true) {
        if (attemptDecIteration == -1) {
          attemptDecIteration = attemptCount;
          Thread.sleep(mudpOptions.getRetransmitTimeout());
        }

        channelLock.lock();
        try {
          if (state != State.ENSLAVED) {
            throw new MudpChannelFreedChannelException();
          }
          reliableState = ReliableState.ACK_WAITING;
        } finally {
          channelLock.unlock();
        }

        MudpPacket mudpDataPacket =
            new MudpPacket(data).setVersion(MudpConnection.MUDP_PROTOCOL_VERSION).setChannel(currentMudpChannelAddress.getChannel()).setMSFlag(true).setREFlag(true).setSequence(currentSendSequence)
                .setErrCode(ERRCODE_NO_ERR).setType(type);

        try {
          mudpConnection.writeMudpPacket(mudpDataPacket);
        } catch (MudpIOException e) {
          if (--attemptDecIteration > 0) {
            logger.warn("[{}] - writing Reliable Packet [{}] in [{}] on attempt [{} of {}] problem",
                this, mudpDataPacket, mudpConnection, (attemptCount - attemptDecIteration), attemptCount, e);
            continue;
          } else {
            throw new MudpIOException("[" + this + "] - failed to send Reliable Data Packet [" + mudpDataPacket + "]", e);
          }
        }

        channelLock.lock();
        try {
          if (state != State.ENSLAVED) {
            throw new MudpChannelFreedChannelException();
          }
          if (reliableState == ReliableState.ACK_WAITING) {
            if (!reliableAckCondition.await(mudpOptions.getAttemptTimeout(), TimeUnit.MILLISECONDS)) {
              if (--attemptDecIteration > 0) {
                logger.warn("[{}] - waiting Reliable Data Ack Packet for Reliable Data Packet [{}] on attempt [{} of {}] timeout",
                    this, mudpDataPacket, (attemptCount - attemptDecIteration), attemptCount);
                continue;
              } else {
                throw new MudpChannelTimeoutException("[" + this + "] - sending Reliable Data Packet [" + mudpDataPacket + "] timeout");
              }
            }
          }
          if (state == State.FREE) {
            throw new MudpChannelAsynchronousFreeChannelException();
          } else if (reliableState == ReliableState.ACK_RECEIVED) {
            if (dataErrCodeException == null) {
              return;
            } else if (dataErrCodeException instanceof MudpChannelBusyException && (--retransmitDecIteration > 0)) {
              logger.info("[{}] - channel busy on attempt [{} of {}] - try send Reliable Data Packet [{}] after retransmit timeout",
                  this, (retransmitAttemptCount - retransmitDecIteration), retransmitAttemptCount, mudpDataPacket);
              attemptDecIteration = -1;
            } else {
              throw dataErrCodeException;
            }
          } else {
            throw new AssertionError(); // must not be here
          }
        } finally {
          reliableState = ReliableState.IDLE;
          channelLock.unlock();
        }
      }
    } finally {
      simultaneousAccessLock.unlock();
    }
  }

  @Override
  public void sendUnreliable(final byte type, final ByteBuffer data) throws MudpException {
    if (data == null) {
      throw new IllegalArgumentException("Data must not be null");
    }

    MudpChannelAddress currentMudpChannelAddress;

    channelLock.lock();
    try {
      if (state != State.ENSLAVED) {
        throw new MudpChannelFreedChannelException();
      }
      currentMudpChannelAddress = mudpChannelAddress;
    } finally {
      channelLock.unlock();
    }

    MudpPacket mudpDataPacket =
        new MudpPacket(data).setVersion(MudpConnection.MUDP_PROTOCOL_VERSION).setChannel(currentMudpChannelAddress.getChannel()).setMSFlag(true).setSequence((byte) 0).setErrCode(ERRCODE_NO_ERR)
            .setType(type);

    mudpConnection.writeMudpPacket(mudpDataPacket);
  }

  @Override
  public MudpOptions getMudpOptions() {
    return mudpOptions;
  }

  void processMudpPacket(final MudpPacket mudpPacket) throws MudpIOException {
    if (mudpPacket.isEEFlag()) {
      if (!mudpPacket.isACKFlag()) {
        logger.warn("[{}] - received Enslave Packet [{}] - packet dropped (current implementation can't process enslave command)", mudpConnection, mudpPacket);
      } else {
        processEnslaveAckPacket(mudpPacket);
      }
    } else {
      if (!mudpPacket.isREFlag()) {
        processUnreliableDataPacket(mudpPacket);
      } else {
        if (mudpPacket.isACKFlag()) {
          processAckPacket(mudpPacket);
        } else {
          processReliableDataPacket(mudpPacket);
        }
      }
    }
  }

  private void processEnslaveAckPacket(final MudpPacket mudpEnslavAckPacket) {
    channelLock.lock();
    try {
      if (state != State.ENSLAVING) {
        logger.warn("[{}] - received Enslave Ack Packet [{}] for non ENSLAVING channel - packet dropped", mudpConnection, mudpEnslavAckPacket);
      } else {
        if (mudpEnslavAckPacket.isERRFlag()) {
          stateErrCodeException = decodeErrCode(mudpEnslavAckPacket.getErrCode());
        } else {
          stateErrCodeException = null;
        }
        state = State.ENSLAVED;
        enslaveAckCondition.signal();
      }
    } finally {
      channelLock.unlock();
    }
  }

  private void processUnreliableDataPacket(final MudpPacket mudpDataPacket) {
    channelLock.lock();
    try {
      if (state != State.ENSLAVED) {
        logger.warn("[{}] - received Unreliable Data Packet [{}] for non ENSLAVING channel - packet dropped", mudpConnection, mudpDataPacket);
        return;
      }
    } finally {
      channelLock.unlock();
    }

    mudpChannelHandler.handleReceive(mudpDataPacket.getType(), mudpDataPacket.getData());
  }

  private void processAckPacket(final MudpPacket mudpAckPacket) {
    channelLock.lock();
    try {
      if (reliableState != ReliableState.ACK_WAITING) {
        logger.debug("[{}] - received Reliable Data Ack Packet [{}] without waiting for it - maybe delayed, packet dropped", mudpConnection, mudpAckPacket);
      } else if (mudpAckPacket.getSequence() != getSendSequence()) {
        logger.debug("[{}] - received Reliable Data Ack Packet [{}] with incorrect sequence - maybe duplicate, packet dropped", mudpConnection, mudpAckPacket);
      } else {
        reliableState = ReliableState.ACK_RECEIVED;
        if (mudpAckPacket.isERRFlag()) {
          dataErrCodeException = decodeErrCode(mudpAckPacket.getErrCode());
        } else {
          dataErrCodeException = null;
          incSendSequence();
        }
        reliableAckCondition.signal();
      }
    } finally {
      channelLock.unlock();
    }
  }

  private void processReliableDataPacket(final MudpPacket mudpDataPacket) throws MudpIOException {
    channelLock.lock();
    try {
      if (state != State.ENSLAVED) {
        logger.warn("[{}] - received Reliable Data Packet [{}] for non ENSLAVING channel - packet dropped", mudpConnection, mudpDataPacket);
        return;
      }
    } finally {
      channelLock.unlock();
    }

    MudpPacket mudpAckPacket =
        new MudpPacket(0).setVersion(mudpDataPacket.getVersion()).setChannel(mudpDataPacket.getChannel()).setMSFlag(true).setREFlag(true).setACKFlag(true).setSequence(mudpDataPacket.getSequence())
            .setErrCode(ERRCODE_NO_ERR);

    try {
      mudpConnection.writeMudpPacket(mudpAckPacket);
    } catch (MudpPacketWriteException e) {
      logger.warn(String.format("[%s] - sending Reliable Data Ack Packet [%s] problem", mudpConnection, mudpAckPacket), e);
      return;
    }

    channelLock.lock();
    try {
      if (mudpDataPacket.getSequence() != getReceiveSequence()) {
        return;
      }
      incReceiveSequence();
    } finally {
      channelLock.unlock();
    }

    mudpChannelHandler.handleReceive(mudpDataPacket.getType(), mudpDataPacket.getData());
  }

  private byte getSendSequence() {
    return (byte) (sendSequence & MudpPacket.FLAG_SEQUENCE_MASK);
  }

  private void incSendSequence() {
    sendSequence++;
  }

  private byte getReceiveSequence() {
    return (byte) (receiveSequence & MudpPacket.FLAG_SEQUENCE_MASK);
  }

  private void incReceiveSequence() {
    receiveSequence++;
  }

  private MudpErrCodeException decodeErrCode(final byte errCode) {
    switch (errCode) {
      case ERRCODE_INTERNAL_ERROR:
        return new MudpInternalErrorException(errCode);
      case ERRCODE_INVALID_DATA:
        return new MudpInvalidDataException(errCode);
      case ERRCODE_CHANNEL_NOT_EXIST:
        return new MudpChannelNotExistException(errCode);
      case ERRCODE_CHANNEL_UNENSLAVED:
        return new MudpChannelUnenslavedException(errCode);
      case ERRCODE_CHANNEL_BUSY:
        return new MudpChannelBusyException(errCode);
      case ERRCODE_LICENCE_FAILURE:
        return new MudpLicenceFailureException(errCode);
      default:
        return new MudpErrCodeException(errCode);
    }
  }

  @Override
  public String toString() {
    return toS;
  }

}
