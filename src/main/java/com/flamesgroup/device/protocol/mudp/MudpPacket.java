/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import com.flamesgroup.device.helper.DataRepresentationHelper;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class MudpPacket {

  public static final int HEADER_LENGTH = 1 + 1 + 1 + 1; // version + channel + flags + err_code
  public static final int DATA_HEADER_LENGTH = 1 + 1 + 2; // type + reserved + length
  public static final int DATA_MAX_LENGTH = 260;
  public static final int DATA_PACKET_MAX_LENGTH = HEADER_LENGTH + DATA_HEADER_LENGTH + DATA_MAX_LENGTH;
  public static final int SERVICE_PACKET_MAX_LENGTH = HEADER_LENGTH;

  protected static final byte FLAG_MS = (byte) 0x80;
  protected static final byte FLAG_EE = (byte) 0x40;
  protected static final byte FLAG_ACK = (byte) 0x20;
  protected static final byte FLAG_ERR = (byte) 0x10;
  protected static final byte FLAG_RE = (byte) 0x02;
  protected static final byte FLAG_SEQUENCE_MASK = (byte) 0x01;

  private byte version;
  private byte channel;
  private byte flags;
  private byte errCode;
  private byte type;
  private final ByteBuffer data;

  public MudpPacket(final ByteBuffer data) {
    if (data == null) {
      throw new IllegalArgumentException("Data must not be null");
    }
    this.data = data;
  }

  public MudpPacket(final int capacity) {
    this(ByteBuffer.allocate(capacity).order(ByteOrder.LITTLE_ENDIAN));
  }

  public MudpPacket() {
    this(DATA_MAX_LENGTH);
  }

  public byte getVersion() {
    return version;
  }

  public MudpPacket setVersion(final byte version) {
    this.version = version;
    return this;
  }

  public byte getChannel() {
    return channel;
  }

  public MudpPacket setChannel(final byte channel) {
    this.channel = channel;
    return this;
  }

  private byte getFlags() {
    return flags;
  }

  private MudpPacket setFlags(final byte flags) {
    this.flags = flags;
    return this;
  }

  public MudpPacket clearFlags() {
    this.flags = (byte) (this.flags & FLAG_SEQUENCE_MASK);
    return this;
  }

  public boolean isMSFlag() {
    return (flags & FLAG_MS) != 0;
  }

  public MudpPacket setMSFlag(final boolean msFlag) {
    if (msFlag) {
      flags |= FLAG_MS;
    } else {
      flags &= ~FLAG_MS;
    }
    return this;
  }

  public boolean isEEFlag() {
    return (flags & FLAG_EE) != 0;
  }

  public MudpPacket setEEFlag(final boolean eeFlag) {
    if (eeFlag) {
      flags |= FLAG_EE;
    } else {
      flags &= ~FLAG_EE;
    }
    return this;
  }

  public boolean isACKFlag() {
    return (flags & FLAG_ACK) != 0;
  }

  public MudpPacket setACKFlag(final boolean ackFlag) {
    if (ackFlag) {
      flags |= FLAG_ACK;
    } else {
      flags &= ~FLAG_ACK;
    }
    return this;
  }

  public boolean isERRFlag() {
    return (flags & FLAG_ERR) != 0;
  }

  public MudpPacket setERRFlag(final boolean errFlag) {
    if (errFlag) {
      flags |= FLAG_ERR;
    } else {
      flags &= ~FLAG_ERR;
    }
    return this;
  }

  public boolean isREFlag() {
    return (flags & FLAG_RE) != 0;
  }

  public MudpPacket setREFlag(final boolean reFlag) {
    if (reFlag) {
      flags |= FLAG_RE;
    } else {
      flags &= ~FLAG_RE;
    }
    return this;
  }

  public byte getSequence() {
    return (byte) (flags & FLAG_SEQUENCE_MASK);
  }

  public MudpPacket setSequence(final byte sequence) {
    flags = (byte) (flags & ~FLAG_SEQUENCE_MASK);
    flags |= (byte) (sequence & FLAG_SEQUENCE_MASK);
    return this;
  }

  public byte getErrCode() {
    return errCode;
  }

  public MudpPacket setErrCode(final byte errCode) {
    this.errCode = errCode;
    return this;
  }

  public byte getType() {
    return type;
  }

  public MudpPacket setType(final byte type) {
    this.type = type;
    return this;
  }

  boolean isDataPacket() {
    return isDataPacket(flags);
  }

  public ByteBuffer getData() {
    return data;
  }

  static boolean isDataPacket(final byte flags) {
    // Note: service Enslave Packet from master to slave have slave configuration data
    return ((flags & FLAG_EE) != 0 && (flags & FLAG_MS) != 0) || (flags & FLAG_ACK) == 0;
  }

  public static MudpPacket decoding(final ByteBuffer buffer, final ChannelMasquerading channelMasquerading) throws MudpPacketReadException {
    try {
      byte version = buffer.get();
      byte channel = channelMasquerading.coding(buffer.get());
      byte flags = buffer.get();
      byte errCode = buffer.get();
      if (isDataPacket(flags)) {
        byte type = buffer.get();
        buffer.get(); // get reserved
        int length = buffer.getShort() & 0xFFFF;
        ByteBuffer data = ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN);
        for (int i = 0; i < length; i++) {
          data.put(buffer.get());
        }
        data.flip();
        return new MudpPacket(data).setVersion(version).setChannel(channel).setFlags(flags).setErrCode(errCode).setType(type);
      } else {
        ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);
        return new MudpPacket(data).setVersion(version).setChannel(channel).setFlags(flags).setErrCode(errCode);
      }
    } catch (BufferUnderflowException e) {
      throw new MudpPacketReadException("Not enough data in read buffer", e);
    } catch (BufferOverflowException e) {
      throw new AssertionError(e); // must not be here, because destination data buffer created with given length
    }
  }

  public static MudpPacket decoding(final ByteBuffer buffer) throws MudpPacketReadException {
    return decoding(buffer, channel -> channel);
  }

  public static void encoding(final MudpPacket mudpPacket, final ByteBuffer buffer, final ChannelMasquerading channelMasquerading) throws MudpPacketWriteException {
    try {
      buffer.put(mudpPacket.getVersion());
      buffer.put(channelMasquerading.coding(mudpPacket.getChannel()));
      buffer.put(mudpPacket.getFlags());
      buffer.put(mudpPacket.getErrCode());
      if (isDataPacket(mudpPacket.getFlags())) {
        buffer.put(mudpPacket.getType());
        buffer.put((byte) 0); // put reserved
        ByteBuffer data = mudpPacket.getData();
        int length = data.remaining();
        if (length > DATA_MAX_LENGTH) {
          throw new MudpPacketWriteException("Data Packet data buffer length [" + length + "] greater than max allowed data buffer length [" + DATA_MAX_LENGTH + "]");
        }
        buffer.putShort((short) (length & 0xFFFF));
        int position = data.position();
        buffer.put(data);
        data.position(position);
      }
    } catch (BufferOverflowException e) {
      throw new MudpPacketWriteException("Not enough space in write buffer", e);
    }
  }

  public static void encoding(final MudpPacket mudpPacket, final ByteBuffer buffer) throws MudpPacketWriteException {
    encoding(mudpPacket, buffer, channel -> channel);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o instanceof MudpPacket) {
      MudpPacket that = (MudpPacket) o;
      if (version == that.version && channel == that.channel && flags == that.flags && errCode == that.errCode) {
        if (isDataPacket(flags)) {
          return type == that.type && data.equals(that.data);
        } else {
          return true;
        }
      }
      return false;
    }
    return false;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (int) version;
    result = 31 * result + (int) channel;
    result = 31 * result + (int) flags;
    result = 31 * result + (int) errCode;
    result = 31 * result + (int) type;
    if (isDataPacket(flags)) {
      result = 31 * result + data.hashCode();
    }
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode())).append(':');
    sb.append("[V:").append(version);
    sb.append(" C:");
    DataRepresentationHelper.appendHexString(sb, channel);
    sb.append(" F:");
    DataRepresentationHelper.appendHexString(sb, flags);
    sb.append(" E:");
    DataRepresentationHelper.appendHexString(sb, errCode);
    if (isDataPacket()) {
      sb.append(" T:");
      DataRepresentationHelper.appendHexString(sb, type);
      sb.append(" D:");
      DataRepresentationHelper.appendHexArrayString(sb, data);
    }
    sb.append("]");
    return sb.toString();
  }

  @FunctionalInterface
  public interface ChannelMasquerading {
    byte coding(byte channel);
  }

}
