/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class MudpConnection implements IMudpConnection {

  static final byte MUDP_PROTOCOL_VERSION = 0x01;
  public static final int MUDP_DEFAULT_PORT = 9020;

  private final Logger logger = LoggerFactory.getLogger(MudpConnection.class);

  private final IMudpStream mudpStream;
  private final MudpOptions mudpOptions;
  private final IMudpConnectionErrorHandler errorHandler;

  private final Lock connectionLock = new ReentrantLock();
  private final ConcurrentMap<MudpChannelAddress, MudpChannel> enslavedMudpChannelsMap = new ConcurrentHashMap<>();

  private final String toS;

  private MudpConnectionThread connectionThread;

  public MudpConnection(final IMudpStream mudpStream, final MudpOptions mudpOptions, final IMudpConnectionErrorHandler errorHandler) {
    Objects.requireNonNull(mudpStream, "Mudp Stream must not be null");
    Objects.requireNonNull(mudpOptions, "Mudp Options must not be null");
    this.mudpStream = mudpStream;
    this.mudpOptions = mudpOptions;
    if (errorHandler != null) {
      this.errorHandler = errorHandler;
    } else {
      this.errorHandler = new IMudpConnectionErrorHandler() {
      };
    }

    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}] and [{}]", toS, mudpStream, mudpOptions);
    }
  }

  public MudpConnection(final IMudpStream mudpStream, final MudpOptions mudpOptions) {
    this(mudpStream, mudpOptions, null);
  }

  void addToEnslaved(final MudpChannelAddress channelAddress, final MudpChannel channel) throws MudpConnectionException {
    if (enslavedMudpChannelsMap.putIfAbsent(channelAddress, channel) != null) {
      throw new MudpConnectionException("[" + this + "] - channel with address [" + channelAddress + "] already enslaved");
    }
  }

  MudpChannel removeFromEnslaved(final MudpChannelAddress channelAddress) {
    return enslavedMudpChannelsMap.remove(channelAddress);
  }

  private MudpChannel getEnslavedByNumber(final byte number) {
    return enslavedMudpChannelsMap.get(new MudpChannelAddress(number));
  }

  @Override
  public boolean isConnect() {
    return mudpStream.isOpen();
  }

  @Override
  public void connect() throws MudpException {
    connectionLock.lock();
    try {
      mudpStream.open();
      logger.debug("[{}] - connected", this);

      connectionThread = new MudpConnectionThread();
      connectionThread.setDaemon(true);
      connectionThread.start();
    } finally {
      connectionLock.unlock();
    }
  }

  @Override
  public void disconnect() throws MudpException, InterruptedException {
    connectionLock.lock();
    try {
      if (!isConnect()) {
        logger.debug("[{}] - it's not connected", this);
        return;
      }

      mudpStream.close();
      logger.debug("[{}] - disconnected", this);

      connectionThread.interrupt();
      connectionThread.join();
      connectionThread = null;

      for (MudpChannel mudpChannel : enslavedMudpChannelsMap.values()) {
        if (mudpChannel != null) {
          logger.warn("[{}] - [{}] is enslaved, will be nulled", this, mudpChannel);
        }
      }
      enslavedMudpChannelsMap.clear();
    } finally {
      connectionLock.unlock();
    }
  }

  @Override
  public IMudpChannel createChannel() {
    return new MudpChannel(this, mudpOptions);
  }

  @Override
  public MudpOptions getMudpOptions() {
    return mudpOptions;
  }

  void writeMudpPacket(final MudpPacket mudpPacket) throws MudpIOException, MudpPacketWriteException {
    mudpStream.writeMudpPacket(mudpPacket);
  }

  private class MudpConnectionThread extends Thread {

    public MudpConnectionThread() {
      super("MudpConnectionThread (" + mudpStream + ")");
    }

    @Override
    public void run() {
      MudpChannel mudpChannel;
      while (!Thread.interrupted()) {
        try {
          MudpPacket mudpPacket;
          try {
            mudpPacket = mudpStream.readMudpPacket();
          } catch (MudpPacketReadException e) {
            logger.warn("[" + this + "] - reading problem", e);
            continue;
          }
          if (mudpPacket.getVersion() != MUDP_PROTOCOL_VERSION) {
            logger.warn("[{}] - unsupported protocol version [{}], support protocol version [{}] or lower - packet dropped",
                this, mudpPacket.getVersion(), MUDP_PROTOCOL_VERSION);
            continue;
          }
          if (mudpPacket.isMSFlag()) {
            logger.warn("[{}] - received master packet - packet dropped", this);
            continue;
          }
          // packet from slave, so look at our masters
          mudpChannel = getEnslavedByNumber(mudpPacket.getChannel());
          if (mudpChannel == null) {
            logger.warn("[{}] - received packet from unknown channel [{}] - packet dropped", this, Byte.toUnsignedInt(mudpPacket.getChannel()));
            continue;
          }
          long beforeHandlerExecutionTime = System.currentTimeMillis();
          // during processing of MudpPacket *data* field ByteBuffer internal values of position, limit and mark could be (and are likely to be) changed
          mudpChannel.processMudpPacket(mudpPacket);
          long afterHandlerExecutionTime = System.currentTimeMillis();
          long handlerExecutionDuration = afterHandlerExecutionTime - beforeHandlerExecutionTime;
          if (handlerExecutionDuration >= mudpOptions.getAttemptTimeout()) {
            logger.warn("[{}] - packet from channel [{}] process duration [{}] longer than attempt timeout [{}]", this, Byte.toUnsignedInt(mudpPacket.getChannel()),
                handlerExecutionDuration, mudpOptions.getAttemptTimeout());
          }
        } catch (MudpClosedStreamException e) {
          Thread.currentThread().interrupt();
        } catch (MudpIOException e) {
          logger.error("[{}] - unexpected IOException - call error handler and shutdown current thread", this, e);

          errorHandler.handleMudpStreamException(e);

          try {
            mudpStream.close(); // something really bad happened with stream - it's better to close it and stop thread
          } catch (MudpIOException ec) {
            logger.error("[{}] - unexpected IOException while disconnect during shutdown current thread", this, ec);
          }
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  @Override
  public String toString() {
    return toS;
  }

}
