package com.flamesgroup.device.protocol.mudp;

public interface IMudpConnectionErrorHandler {

  default void handleMudpStreamException(final MudpException e) {
  }

}
