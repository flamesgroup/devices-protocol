/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

public final class MudpStreamSPIDevEmptyStatistics implements MudpStreamSPIDev.IStatistics {

  @Override
  public void init() {
  }

  @Override
  public void handleWriteMudpServicePacketFrame(final int spiChainSlaveN) {
  }

  @Override
  public void handleWriteMudpDataPacketFrame(final int spiChainSlaveN) {
  }

  @Override
  public void handleReadMudpServicePacketFrame(final int spiChainSlaveN) {
  }

  @Override
  public void handleReadMudpDataPacketFrame(final int spiChainSlaveN) {
  }

  @Override
  public void handleReadMudpServicePacketException(final int spiChainSlaveN) {
  }

  @Override
  public void handleReadMudpDataPacketException(final int spiChainSlaveN) {
  }

  @Override
  public void iterate() {
  }

}
