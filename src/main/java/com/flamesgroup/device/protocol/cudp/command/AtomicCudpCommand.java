/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnectionTimeoutException;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.ICudpLinkHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class AtomicCudpCommand<CudpCommandRequest extends ICudpCommandRequest, CudpCommandResponse extends ICudpCommandResponse> implements
    ICudpLinkHandler {

  private enum State {
    WAITING, EXECUTING, EXECUTED
  }

  private final Logger logger = LoggerFactory.getLogger(AtomicCudpCommand.class);

  private final ICudpLink cudpLink;
  private final long responseTimeout;

  private final Lock responseLock = new ReentrantLock();
  private final Condition responseCondition = responseLock.newCondition();

  private State state = State.WAITING;
  private CudpCommandResponse response;
  private CudpException exception;

  public AtomicCudpCommand(final ICudpLink cudpLink, final long responseTimeout) {
    this.cudpLink = cudpLink;
    this.responseTimeout = responseTimeout;
  }

  public CudpCommandResponse execute(final SocketAddress remoteAddress, final CudpCommandRequest commandRequest) throws CudpException, InterruptedException {
    responseLock.lock();
    try {
      if (state != State.WAITING) {
        throw new CudpCommandNowExecutingException();
      }
      state = State.EXECUTING;
    } finally {
      responseLock.unlock();
    }

    try {
      cudpLink.link(commandRequest.getSDUID(), this);
      try {
        cudpLink.send(remoteAddress, commandRequest.toCudpPacket());

        responseLock.lock();
        try {
          if (state == State.EXECUTING) {
            if (!responseCondition.await(responseTimeout, TimeUnit.MILLISECONDS)) {
              throw new CudpConnectionTimeoutException("[" + this + "] - response timeout elapsed for request [" + commandRequest + "]");
            }
          }
          if (state == State.EXECUTED) {
            state = State.WAITING;
            if (response != null) {
              return response;
            }
            if (exception != null) {
              throw exception;
            }
          }
          throw new AssertionError(); // we must not be here
        } finally {
          responseLock.unlock();
        }
      } finally {
        cudpLink.unlink();
      }
    } finally {
      responseLock.lock();
      try {
        state = State.WAITING;
        response = null;
        exception = null;
      } finally {
        responseLock.unlock();
      }
    }
  }

  @Override
  public void handleReceive(final SocketAddress remoteAddress, final CudpPacket packet) {
    if (!packet.isERRFlag()) {
      handleResponse(packet);
    } else {
      handleErrCode(new CudpErrCodeException(packet.getErrCode()));
    }
  }

  private void handleResponse(final CudpPacket cudpPacket) {
    boolean unexpectedResponse = true;
    responseLock.lock();
    try {
      if (state == State.EXECUTING) {
        unexpectedResponse = false;
      }
    } finally {
      responseLock.unlock();
    }

    CudpCommandResponse receivedResponse = createResponse();
    if (unexpectedResponse) {
      try {
        receivedResponse.fromCudpPacket(cudpPacket);
        logger.warn("[{}] - received unexpected Command Response [{}]", this, receivedResponse);
      } catch (CudpCommandException e) {
        logger.warn("[" + this + "] - received unexpected undecoded Command Response", e);
      }
    } else {
      try {
        receivedResponse.fromCudpPacket(cudpPacket);
        responseLock.lock();
        try {
          state = State.EXECUTED;
          response = receivedResponse;
          responseCondition.signalAll();
        } finally {
          responseLock.unlock();
        }
      } catch (CudpCommandException e) {
        responseLock.lock();
        try {
          state = State.EXECUTED;
          exception = e;
          responseCondition.signalAll();
        } finally {
          responseLock.unlock();
        }
      }
    }
  }

  private void handleErrCode(final CudpErrCodeException e) {
    boolean unexpectedResponse = true;
    responseLock.lock();
    try {
      if (state == State.EXECUTING) {
        unexpectedResponse = false;
      }
    } finally {
      responseLock.unlock();
    }

    if (unexpectedResponse) {
      logger.warn("[{}] - received unexpected ErrCode [{}]", this, e);
    } else {
      responseLock.lock();
      try {
        state = State.EXECUTED;
        exception = specifyErrCode(e);
        responseCondition.signalAll();
      } finally {
        responseLock.unlock();
      }
    }
  }

  protected abstract CudpErrCodeException specifyErrCode(CudpErrCodeException e);

  public abstract CudpCommandRequest createRequest(DeviceUID sduid, DeviceUID dduid);

  public abstract CudpCommandResponse createResponse();

}
