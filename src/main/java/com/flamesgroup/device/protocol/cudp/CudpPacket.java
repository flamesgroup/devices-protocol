/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.DeviceUID;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

public final class CudpPacket {

  public static final byte TYPE_PING = 1;
  public static final byte TYPE_SET_IP_CONFIG = 2;
  public static final byte TYPE_GET_IP_CONFIG = 3;
  public static final byte TYPE_RESET = 4;
  public static final byte TYPE_UPDATE_EXPIRE_TIME = 6;
  public static final byte TYPE_GET_EXPIRE_TIME = 7;

  private static final byte FLAG_BT = (byte) 0x80;
  private static final byte FLAG_ACK = (byte) 0x40;
  private static final byte FLAG_ERR = (byte) 0x20;

  private byte version;
  private byte type;
  private byte flags;
  private byte errCode;
  private int suid;
  private int duid;
  private long timestamp;
  private Inet4Address IP;
  private Inet4Address mask;
  private Inet4Address gateway;
  private Inet4Address masterIP;
  private UUID uuid;
  private long currentTime;
  private int delay;
  private byte[] key;
  private long expireTime;

  public byte getVersion() {
    return version;
  }

  public CudpPacket setVersion(final byte version) {
    this.version = version;
    return this;
  }

  public byte getType() {
    return type;
  }

  public CudpPacket setType(final byte type) {
    this.type = type;
    return this;
  }

  public CudpPacket clearFlags() {
    this.flags = (byte) 0;
    return this;
  }

  public boolean isBTFlag() {
    return (flags & FLAG_BT) != 0;
  }

  public CudpPacket setBTFlag(final boolean btFlag) {
    if (btFlag) {
      flags |= FLAG_BT;
    } else {
      flags &= ~FLAG_BT;
    }
    return this;
  }

  public boolean isACKFlag() {
    return (flags & FLAG_ACK) != 0;
  }

  public CudpPacket setACKFlag(final boolean ackFlag) {
    if (ackFlag) {
      flags |= FLAG_ACK;
    } else {
      flags &= ~FLAG_ACK;
    }
    return this;
  }

  public boolean isERRFlag() {
    return (flags & FLAG_ERR) != 0;
  }

  public CudpPacket setERRFlag(final boolean errFlag) {
    if (errFlag) {
      flags |= FLAG_ERR;
    } else {
      flags &= ~FLAG_ERR;
    }
    return this;
  }

  public byte getErrCode() {
    return errCode;
  }

  public CudpPacket setErrCode(final byte errCode) {
    this.errCode = errCode;
    return this;
  }

  public int getSUID() {
    return suid;
  }

  public CudpPacket setSUID(final int suid) {
    this.suid = suid;
    return this;
  }

  public int getDUID() {
    return duid;
  }

  public CudpPacket setDUID(final int duid) {
    this.duid = duid;
    return this;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public CudpPacket setTimestamp(final long timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  public Inet4Address getIP() {
    return IP;
  }

  public CudpPacket setIP(final Inet4Address IP) {
    this.IP = IP;
    return this;
  }

  public Inet4Address getMask() {
    return mask;
  }

  public CudpPacket setMask(final Inet4Address mask) {
    this.mask = mask;
    return this;
  }

  public Inet4Address getGateway() {
    return gateway;
  }

  public CudpPacket setGateway(final Inet4Address gateway) {
    this.gateway = gateway;
    return this;
  }

  public Inet4Address getMasterIP() {
    return masterIP;
  }

  public CudpPacket setMasterIP(final Inet4Address masterIP) {
    this.masterIP = masterIP;
    return this;
  }

  public UUID getUUID() {
    return uuid;
  }

  public CudpPacket setUUID(final UUID uuid) {
    this.uuid = uuid;
    return this;
  }

  public long getCurrentTime() {
    return currentTime;
  }

  public CudpPacket setCurrentTime(final long currentTime) {
    this.currentTime = currentTime;
    return this;
  }

  public int getDelay() {
    return delay;
  }

  public CudpPacket setDelay(final int delay) {
    this.delay = delay;
    return this;
  }

  public byte[] getKey() {
    return key;
  }

  public void setKey(final byte[] key) {
    this.key = key;
  }

  public long getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(final long expireTime) {
    this.expireTime = expireTime;
  }

  public void readFromByteBuffer(final ByteBuffer buffer) throws CudpPacketReadException {
    version = buffer.get();
    type = buffer.get();
    flags = buffer.get();
    errCode = buffer.get();
    suid = buffer.getInt();
    duid = buffer.getInt();

    switch (type) {
      case TYPE_PING: {
        timestamp = buffer.getLong();
        break;
      }
      case TYPE_SET_IP_CONFIG: {
        if (!isACKFlag()) {
          byte[] data = new byte[4];
          buffer.get(data);
          IP = convertByte4Array2Inet4Address(data);
          buffer.get(data);
          mask = convertByte4Array2Inet4Address(data);
          buffer.get(data);
          gateway = convertByte4Array2Inet4Address(data);
          buffer.get(data);
          masterIP = convertByte4Array2Inet4Address(data);
          uuid = new UUID(buffer.getLong(), buffer.getLong());
          currentTime = buffer.getLong();
        }
        break;
      }
      case TYPE_GET_IP_CONFIG: {
        if (isACKFlag()) {
          byte[] data = new byte[4];
          buffer.get(data);
          IP = convertByte4Array2Inet4Address(data);
          buffer.get(data);
          mask = convertByte4Array2Inet4Address(data);
          buffer.get(data);
          gateway = convertByte4Array2Inet4Address(data);
          buffer.get(data);
          masterIP = convertByte4Array2Inet4Address(data);
          uuid = new UUID(buffer.getLong(), buffer.getLong());
        }
        break;
      }
      case TYPE_RESET: {
        if (!isACKFlag()) {
          delay = buffer.getInt();
        }
        break;
      }
      case TYPE_UPDATE_EXPIRE_TIME: {
        if (!isACKFlag()) {
          key = new byte[48];
          buffer.get(key);
        }
        break;
      }
      case TYPE_GET_EXPIRE_TIME: {
        if (isACKFlag()) {
          expireTime = buffer.getLong();
        }
        break;
      }
      default: {
        throw new CudpPacketReadException("Can't decode - unknown CUDP packet type: [" + type + "]");
      }
    }
  }

  public void writeToByteBuffer(final ByteBuffer buffer) throws CudpPacketWriteException {
    buffer.put(version);
    buffer.put(type);
    buffer.put(flags);
    buffer.put(errCode);
    buffer.putInt(suid);
    buffer.putInt(duid);

    switch (type) {
      case TYPE_PING: {
        buffer.putLong(timestamp);
        break;
      }
      case TYPE_SET_IP_CONFIG: {
        if (!isACKFlag()) {
          buffer.put(convertInet4Address2Byte4Array(IP));
          buffer.put(convertInet4Address2Byte4Array(mask));
          buffer.put(convertInet4Address2Byte4Array(gateway));
          buffer.put(convertInet4Address2Byte4Array(masterIP));
          buffer.putLong(uuid.getMostSignificantBits());
          buffer.putLong(uuid.getLeastSignificantBits());
          buffer.putLong(currentTime);
        }
        break;
      }
      case TYPE_GET_IP_CONFIG: {
        if (isACKFlag()) {
          buffer.put(convertInet4Address2Byte4Array(IP));
          buffer.put(convertInet4Address2Byte4Array(mask));
          buffer.put(convertInet4Address2Byte4Array(gateway));
          buffer.put(convertInet4Address2Byte4Array(masterIP));
          buffer.putLong(uuid.getMostSignificantBits());
          buffer.putLong(uuid.getLeastSignificantBits());
        }
        break;
      }
      case TYPE_RESET: {
        if (!isACKFlag()) {
          buffer.putInt(delay);
        }
        break;
      }
      case TYPE_UPDATE_EXPIRE_TIME: {
        if (!isACKFlag()) {
          buffer.put(key);
        }
        break;
      }
      case TYPE_GET_EXPIRE_TIME: {
        if (isACKFlag()) {
          buffer.putLong(expireTime);
        }
        break;
      }
      default: {
        throw new CudpPacketWriteException("Can't encode - unknown CUDP packet type: [" + type + "]");
      }
    }
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof CudpPacket) {
      CudpPacket cudpPacket = (CudpPacket) obj;
      if (this.version == cudpPacket.version && this.type == cudpPacket.type && this.flags == cudpPacket.flags
          && this.errCode == cudpPacket.errCode && this.suid == cudpPacket.suid && this.duid == cudpPacket.duid) {
        if (isERRFlag()) {
          return true;
        } else {
          switch (type) {
            case TYPE_PING: {
              return this.timestamp == cudpPacket.timestamp;
            }
            case TYPE_SET_IP_CONFIG: {
              return isACKFlag() || (this.IP != null ? this.IP.equals(cudpPacket.IP) : cudpPacket.IP == null)
                  && (this.mask != null ? this.mask.equals(cudpPacket.mask) : cudpPacket.mask == null)
                  && (this.gateway != null ? this.gateway.equals(cudpPacket.gateway) : cudpPacket.gateway == null)
                  && (this.masterIP != null ? this.masterIP.equals(cudpPacket.masterIP) : cudpPacket.masterIP == null)
                  && (this.uuid != null ? this.uuid.equals(cudpPacket.uuid) : cudpPacket.uuid == null)
                  && (this.currentTime == cudpPacket.currentTime);
            }
            case TYPE_GET_IP_CONFIG: {
              return !isACKFlag() || (this.IP != null ? this.IP.equals(cudpPacket.IP) : cudpPacket.IP == null)
                  && (this.mask != null ? this.mask.equals(cudpPacket.mask) : cudpPacket.mask == null)
                  && (this.gateway != null ? this.gateway.equals(cudpPacket.gateway) : cudpPacket.gateway == null)
                  && (this.masterIP != null ? this.masterIP.equals(cudpPacket.masterIP) : cudpPacket.masterIP == null)
                  && (this.uuid != null ? this.uuid.equals(cudpPacket.uuid) : cudpPacket.uuid == null);
            }
            case TYPE_RESET: {
              return isACKFlag() || this.delay == cudpPacket.delay;
            }
            case TYPE_UPDATE_EXPIRE_TIME: {
              return isACKFlag() || Arrays.equals(this.key, cudpPacket.key);
            }
            case TYPE_GET_EXPIRE_TIME: {
              return !isACKFlag() || this.expireTime == cudpPacket.expireTime;
            }
            default: {
              return true;
            }
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return this.version << 24 + this.type << 16 + (this.duid + this.suid) & 0xFFFF;
  }

  @Override
  public String toString() {
    return String.format("%s@%x:[V:%d T:0x%X %s DUID:%s SUID:%s]", getClass().getSimpleName(), hashCode(), version, type, isACKFlag() ? "ACK" : "", new DeviceUID(duid), new DeviceUID(suid));
  }

  private Inet4Address convertByte4Array2Inet4Address(final byte[] data) throws CudpPacketReadException {
    InetAddress inetAddress;
    byte reverseData[] = new byte[data.length];
    reverseByteArray(data, reverseData);
    try {
      inetAddress = InetAddress.getByAddress(reverseData);
    } catch (UnknownHostException ex) {
      throw new CudpPacketReadException(ex);
    }
    if (inetAddress instanceof Inet4Address) {
      return (Inet4Address) inetAddress;
    } else {
      throw new CudpPacketReadException("Read data is not IPv4 instance");
    }
  }

  private byte[] convertInet4Address2Byte4Array(final Inet4Address inet4Address) throws CudpPacketWriteException {
    byte[] data = inet4Address.getAddress();
    byte reverseData[] = new byte[data.length];
    reverseByteArray(data, reverseData);
    return reverseData;
  }

  private void reverseByteArray(final byte[] src, final byte[] dst) {
    for (int i = 0; i < src.length; i++) {
      dst[dst.length - i - 1] = src[i];
    }
  }

}
