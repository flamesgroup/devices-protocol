/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.protocol.cudp.command.GetIPConfigCommand.GetIPConfigRequest;
import com.flamesgroup.device.protocol.cudp.command.GetIPConfigCommand.GetIPConfigResponse;

public final class GetIPConfigCommand extends AtomicCudpCommand<GetIPConfigRequest, GetIPConfigResponse> {

  public GetIPConfigCommand(final ICudpLink cudpLink, final long responseTimeout) {
    super(cudpLink, responseTimeout);
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return e;
  }

  @Override
  public GetIPConfigRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new GetIPConfigRequest(sduid, dduid);
  }

  @Override
  public GetIPConfigResponse createResponse() {
    return new GetIPConfigResponse();
  }

  public static class GetIPConfigRequest implements ICudpCommandRequest {

    private final DeviceUID sduid;
    private final DeviceUID dduid;
    private boolean broadcast;

    private GetIPConfigRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_GET_IP_CONFIG);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast);
      return cudpPacket;
    }

    public GetIPConfigRequest setBroadcast(final boolean broadcast) {
      this.broadcast = broadcast;
      return this;
    }

  }

  public static class GetIPConfigResponse implements ICudpCommandResponse {

    private IPConfig ipConfig;

    private GetIPConfigResponse() {
      super();
    }

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_GET_IP_CONFIG) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_GET_IP_CONFIG));
      }

      ipConfig = new IPConfig(cudpPacket.getIP(), cudpPacket.getMask(), cudpPacket.getGateway(), cudpPacket.getMasterIP(), cudpPacket.getUUID());
    }

    public IPConfig getIPConfig() {
      return ipConfig;
    }

  }

}
