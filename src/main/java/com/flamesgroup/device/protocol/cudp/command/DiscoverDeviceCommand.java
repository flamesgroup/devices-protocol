/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.ICudpLinkHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class DiscoverDeviceCommand implements ICudpLinkHandler {

  private enum State {
    WAITING, EXECUTING
  }

  private final Logger logger = LoggerFactory.getLogger(AtomicCudpCommand.class);

  private final ICudpLink cudpLink;
  private final long discoveryTimeout;

  private final Lock discoverLock = new ReentrantLock();

  private State state = State.WAITING;
  private DiscoverResponse response;

  public DiscoverDeviceCommand(final ICudpLink cudpLink, final long discoveryTimeout) {
    this.cudpLink = cudpLink;
    this.discoveryTimeout = discoveryTimeout;
  }

  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return e;
  }

  public DiscoverRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new DiscoverRequest(sduid, dduid);
  }

  public DiscoverResponse createResponse() {
    return new DiscoverResponse();
  }

  public DiscoverResponse execute(final SocketAddress remoteAddress, final DiscoverRequest commandRequest) throws CudpException, InterruptedException {
    discoverLock.lock();
    try {
      if (state != State.WAITING) {
        throw new CudpCommandNowExecutingException();
      }
      state = State.EXECUTING;
      response = createResponse();
    } finally {
      discoverLock.unlock();
    }

    try {
      cudpLink.link(commandRequest.getSDUID(), this);
      try {
        cudpLink.send(remoteAddress, commandRequest.toCudpPacket());
        Thread.sleep(discoveryTimeout);
      } finally {
        cudpLink.unlink();
      }
      return response;
    } finally {
      discoverLock.lock();
      try {
        state = State.WAITING;
        response = null;
      } finally {
        discoverLock.unlock();
      }
    }
  }

  @Override
  public void handleReceive(final SocketAddress sourceAddress, final CudpPacket cudpPacket) {
    State localState;
    discoverLock.lock();
    try {
      localState = state;
    } finally {
      discoverLock.unlock();
    }

    if (cudpPacket.isERRFlag()) {
      logger.debug("[{}] - received error DiscoverDeviceCommand Response CudpPacket [{}]", this, cudpPacket);
    } else if (localState == State.EXECUTING) {
      try {
        response.fromCudpPacket(cudpPacket);
      } catch (CudpCommandException e) {
        logger.debug("[" + this + "] - received unexpected undecoded DiscoverDeviceCommand Response", e);
      }
    } else {
      logger.debug("[{}] - received unexpected DiscoverDeviceCommand Response CudpPacket [{}]", this, cudpPacket);
    }
  }

  public static class DiscoverRequest implements ICudpCommandRequest {

    private final DeviceUID sduid;
    private final DeviceUID dduid;
    private final boolean broadcast = true;
    private long timestamp;

    private DiscoverRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_PING);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast).setTimestamp(timestamp);
      return cudpPacket;
    }

    public DiscoverRequest setTimestamp(final long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

  }

  public static class DiscoverResponse implements ICudpCommandResponse {

    private final Set<DeviceUID> devices = new HashSet<>();

    private DiscoverResponse() {
      super();
    }

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_PING) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_PING));
      }
      devices.add(new DeviceUID(cudpPacket.getSUID()));
    }

    public Set<DeviceUID> getDevices() {
      return devices;
    }

  }

}
