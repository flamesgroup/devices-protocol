/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.DeviceUID;

import java.net.SocketAddress;
import java.util.Set;

public interface ICudpConnection {

  boolean isConnected();

  void connect(SocketAddress remoteBroadcastAddress) throws CudpException;

  void disconnect() throws CudpException, InterruptedException;

  Set<DeviceUID> getDeviceUIDs(long waitResponseInterval) throws CudpException, InterruptedException;

  void ping(DeviceUID uid) throws CudpException, InterruptedException;

  long ping(SocketAddress remoteAddress) throws CudpException, InterruptedException;

  void setIPConfig(DeviceUID uid, IPConfig deviceIPConfig) throws CudpException, InterruptedException;

  IPConfig getIPConfig(DeviceUID uid) throws CudpException, InterruptedException;

  void reset(DeviceUID uid, long delayMillisecond) throws CudpException, InterruptedException;

  void updateExpireTime(DeviceUID uid, byte[] key) throws CudpException, InterruptedException;

  ExpireTime getExpireTime(DeviceUID uid) throws CudpException, InterruptedException;

}
