/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.command.ResetCommand.ResetRequest;
import com.flamesgroup.device.protocol.cudp.command.ResetCommand.ResetResponse;

public final class ResetCommand extends AtomicCudpCommand<ResetRequest, ResetResponse> {

  public ResetCommand(final ICudpLink cudpLink, final long responseTimeout) {
    super(cudpLink, responseTimeout);
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return e;
  }

  @Override
  public ResetRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new ResetRequest(sduid, dduid);
  }

  @Override
  public ResetResponse createResponse() {
    return new ResetResponse();
  }

  public static class ResetRequest implements ICudpCommandRequest {

    private final DeviceUID sduid;
    private final DeviceUID dduid;
    private boolean broadcast;
    private long delay;

    private ResetRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_RESET);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast).setDelay((int) delay);
      return cudpPacket;
    }

    public ResetRequest setBroadcast(final boolean broadcast) {
      this.broadcast = broadcast;
      return this;
    }

    public ResetRequest setDelay(final long delay) {
      if (delay > 4294967295L) {
        throw new IllegalArgumentException("Reset delay can't be greater than " + 4294967295L + ", but it's " + delay);
      }
      this.delay = delay;
      return this;
    }

  }

  public static class ResetResponse implements ICudpCommandResponse {

    private ResetResponse() {
      super();
    }

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_RESET) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_RESET));
      }
    }

  }

}
