/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.command.UpdateExpireTimeCommand.UpdateExpireTimeRequest;
import com.flamesgroup.device.protocol.cudp.command.UpdateExpireTimeCommand.UpdateExpireTimeResponse;

public final class UpdateExpireTimeCommand extends AtomicCudpCommand<UpdateExpireTimeRequest, UpdateExpireTimeResponse> {

  public UpdateExpireTimeCommand(final ICudpLink cudpLink, final long responseTimeout) {
    super(cudpLink, responseTimeout);
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return e;
  }

  @Override
  public UpdateExpireTimeRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new UpdateExpireTimeRequest(sduid, dduid);
  }

  @Override
  public UpdateExpireTimeResponse createResponse() {
    return new UpdateExpireTimeResponse();
  }

  public static class UpdateExpireTimeRequest implements ICudpCommandRequest {

    private static final int KEY_LENGTH = 48;

    private final DeviceUID sduid;
    private final DeviceUID dduid;

    private boolean broadcast;
    private byte[] key;

    public UpdateExpireTimeRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() throws CudpCommandException {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_UPDATE_EXPIRE_TIME);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast).setKey(key);
      return cudpPacket;
    }

    public UpdateExpireTimeRequest setBroadcast(final boolean broadcast) {
      this.broadcast = broadcast;
      return this;
    }

    public UpdateExpireTimeRequest setKey(final byte[] key) {
      if (key.length != KEY_LENGTH) {
        throw new IllegalArgumentException(String.format("key size must be %d, but it's %d", KEY_LENGTH, key.length));
      }
      this.key = key;
      return this;
    }
  }

  public static class UpdateExpireTimeResponse implements ICudpCommandResponse {

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_UPDATE_EXPIRE_TIME) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_UPDATE_EXPIRE_TIME));
      }
    }
  }

}
