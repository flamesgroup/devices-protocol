/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Objects;
import java.util.UUID;

public final class IPConfig implements Serializable {

  private static final long serialVersionUID = -1966566172935999357L;

  public static final UUID UNINITIALIZED_UUID = new UUID(-1, -1);

  private final Inet4Address ip;
  private final Inet4Address mask;
  private final Inet4Address gateway;
  private final Inet4Address masterIP;
  private final UUID uuid;

  private final String toS;

  public IPConfig(final Inet4Address ip, final Inet4Address mask, final Inet4Address gateway, final Inet4Address masterIp, final UUID uuid) {
    Objects.requireNonNull(ip, "IP address must not be null");
    Objects.requireNonNull(mask, "Subnet mask must not be null");
    Objects.requireNonNull(gateway, "Gateway address must not be null");
    Objects.requireNonNull(masterIp, "Master IP address must not be null");
    Objects.requireNonNull(uuid, "uuid must not be null");

    this.ip = ip;
    this.mask = mask;
    this.gateway = gateway;
    this.masterIP = masterIp;
    this.uuid = uuid;

    toS = String.format("%s@%x:[ip:[%s], mask:[%s], gateway:[%s], masterip:[%s]], uuid:[%s]", getClass().getSimpleName(), hashCode(),
        ip.getHostAddress(), mask.getHostAddress(), gateway.getHostAddress(), masterIp.getHostAddress(), uuid);
  }

  public IPConfig(final InetAddress ip, final InetAddress mask, final InetAddress gateway, final InetAddress masterIp, final UUID uuid) {
    this((Inet4Address) ip, (Inet4Address) mask, (Inet4Address) gateway, (Inet4Address) masterIp, uuid);
  }

  public Inet4Address getIP() {
    return ip;
  }

  public Inet4Address getMask() {
    return mask;
  }

  public Inet4Address getGateway() {
    return gateway;
  }

  public Inet4Address getMasterIP() {
    return masterIP;
  }

  public UUID getUUID() {
    return uuid;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof IPConfig)) {
      return false;
    }

    IPConfig that = (IPConfig) object;
    return this.ip.equals(that.ip) && this.mask.equals(that.mask) && this.gateway.equals(that.gateway)
        && this.masterIP.equals(that.masterIP) && this.uuid.equals(that.uuid);
  }

  @Override
  public int hashCode() {
    return ip.hashCode() + mask.hashCode() + gateway.hashCode() + masterIP.hashCode() + uuid.hashCode();
  }

  @Override
  public String toString() {
    return toS;
  }

}
