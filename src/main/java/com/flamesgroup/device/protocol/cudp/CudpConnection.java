/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.command.DiscoverDeviceCommand;
import com.flamesgroup.device.protocol.cudp.command.GetExpireTimeCommand;
import com.flamesgroup.device.protocol.cudp.command.GetIPConfigCommand;
import com.flamesgroup.device.protocol.cudp.command.PingCommand;
import com.flamesgroup.device.protocol.cudp.command.ResetCommand;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigCommand;
import com.flamesgroup.device.protocol.cudp.command.UpdateExpireTimeCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.Thread.UncaughtExceptionHandler;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedChannelException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class CudpConnection implements ICudpConnection {

  private final Logger logger = LoggerFactory.getLogger(CudpConnection.class);

  public static final byte CUDP_PROTOCOL_VERSION = 1;
  public static final int CUDP_DEFAULT_PORT = 9010;

  private final DeviceUID UID_SPECIAL_COMMON = new DeviceUID(0x00000000);
  private final DeviceUID UID_BROADCAST_PING = new DeviceUID(DeviceType.SERVER, 0x00FFFFFF);

  private final ICudpStream cudpStream;
  private final long timeout;
  private CudpConnectionThread connectionThread;

  private SocketAddress remoteBroadcastAddress;

  private final ConcurrentMap<DeviceUID, ICudpLinkHandler> cudpLinkMap = new ConcurrentHashMap<>();

  private final AtomicInteger uidDeviceNumberCounterAtomic = new AtomicInteger();

  private final Lock discoverDeviceLock = new ReentrantLock();

  private final String toS;

  public CudpConnection(final ICudpStream cudpStream, final long timeout) {
    if (cudpStream == null) {
      throw new IllegalArgumentException("CudpStream must not be null");
    }
    if (!(timeout >= 0 && timeout <= 1000000)) { // timeout must prevent UID repetition
      throw new IllegalArgumentException("Timeout must be in range from 0 millisecont to 1000000 millisecont");
    }

    this.cudpStream = cudpStream;
    this.timeout = timeout;

    toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());

    if (logger.isTraceEnabled()) {
      logger.trace("[{}] - created with [{}] and timeout [{}]", toS, cudpStream, timeout);
    }
  }

  @Override
  public boolean isConnected() {
    return cudpStream.isOpen();
  }

  @Override
  public void connect(final SocketAddress remoteBroadcastAddress) throws CudpException {
    if (remoteBroadcastAddress == null) {
      throw new IllegalArgumentException("RemoteBroadcastAddress must not be null");
    }
    this.remoteBroadcastAddress = remoteBroadcastAddress;
    cudpStream.open(null);
    logger.debug("[{}] - connected", this);

    connectionThread = new CudpConnectionThread();
    connectionThread.setDaemon(true);
    connectionThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(final Thread t, final Throwable e) {
        logger.error("[" + t.getName() + "] - get uncaught Exception", e);
      }
    });
    connectionThread.start();
  }

  @Override
  public void disconnect() throws CudpException, InterruptedException {
    if (isConnected()) {
      cudpStream.close();
      logger.debug("[{}] - disconnected", this);
      connectionThread.interrupt();
      connectionThread.join();
    } else {
      logger.debug("[{}] - it's not connected", this);
    }
  }

  private void checkAndThrowIfUnallowableUID(final DeviceUID uid) {
    if (uid == null) {
      throw new IllegalArgumentException("DeviceUID must not be null");
    }
    if (uid.equals(UID_SPECIAL_COMMON)) {
      throw new IllegalArgumentException("Special Common DeviceUID [" + UID_SPECIAL_COMMON + "] can't used as DeviceUID parameter");
    }
  }

  @Override
  public Set<DeviceUID> getDeviceUIDs(final long waitResponseInterval) throws CudpException, InterruptedException {
    discoverDeviceLock.lock();
    try {
      DiscoverDeviceCommand discoverDeviceCommand = new DiscoverDeviceCommand(createCudpLink(), waitResponseInterval);
      return discoverDeviceCommand.execute(remoteBroadcastAddress, discoverDeviceCommand.createRequest(UID_BROADCAST_PING, UID_SPECIAL_COMMON)).getDevices();
    } finally {
      discoverDeviceLock.unlock();
    }
  }

  @Override
  public void ping(final DeviceUID uid) throws CudpException, InterruptedException {
    checkAndThrowIfUnallowableUID(uid);

    PingCommand pingCommand = new PingCommand(createCudpLink(), timeout);
    pingCommand.execute(remoteBroadcastAddress, pingCommand.createRequest(getNextUID(), uid).setBroadcast(true).setTimestamp(System.currentTimeMillis()));
  }

  @Override
  public long ping(final SocketAddress remoteAddress) throws CudpException, InterruptedException {
    PingCommand pingCommand = new PingCommand(createCudpLink(), timeout);
    long pingTimeStamp = pingCommand.execute(remoteAddress,
        pingCommand.createRequest(getNextUID(), UID_SPECIAL_COMMON).setBroadcast(false).setTimestamp(System.currentTimeMillis())).getTimestamp();
    return System.currentTimeMillis() - pingTimeStamp;
  }

  @Override
  public void setIPConfig(final DeviceUID uid, final IPConfig deviceIPConfig) throws CudpException, InterruptedException {
    checkAndThrowIfUnallowableUID(uid);
    if (deviceIPConfig == null) {
      throw new IllegalArgumentException("IPConfig must not be null");
    }

    SetIPConfigCommand setIPCommand = new SetIPConfigCommand(createCudpLink(), timeout);
    setIPCommand.execute(remoteBroadcastAddress, setIPCommand.createRequest(getNextUID(), uid).setBroadcast(true).setIPConfig(deviceIPConfig).setCurrentTime(System.currentTimeMillis()));

    Thread.sleep(10); // give some time to apply new IP config - necessary to prevent unreachable port exception in MUDP if connect too fast
  }

  @Override
  public IPConfig getIPConfig(final DeviceUID uid) throws CudpException, InterruptedException {
    checkAndThrowIfUnallowableUID(uid);

    GetIPConfigCommand getIPCommand = new GetIPConfigCommand(createCudpLink(), timeout);
    return getIPCommand.execute(remoteBroadcastAddress, getIPCommand.createRequest(getNextUID(), uid).setBroadcast(true)).getIPConfig();
  }

  @Override
  public void reset(final DeviceUID uid, final long delayMillisecond) throws CudpException, InterruptedException {
    checkAndThrowIfUnallowableUID(uid);

    ResetCommand resetCommand = new ResetCommand(createCudpLink(), timeout);
    resetCommand.execute(remoteBroadcastAddress, resetCommand.createRequest(getNextUID(), uid).setBroadcast(true).setDelay(delayMillisecond));
  }

  @Override
  public void updateExpireTime(final DeviceUID uid, final byte[] key) throws CudpException, InterruptedException {
    checkAndThrowIfUnallowableUID(uid);

    UpdateExpireTimeCommand updateExpireTimeCommand = new UpdateExpireTimeCommand(createCudpLink(), timeout);
    updateExpireTimeCommand.execute(remoteBroadcastAddress, updateExpireTimeCommand.createRequest(getNextUID(), uid).setBroadcast(true).setKey(key));
  }

  @Override
  public ExpireTime getExpireTime(final DeviceUID uid) throws CudpException, InterruptedException {
    checkAndThrowIfUnallowableUID(uid);

    GetExpireTimeCommand getExpireTimeCommand = new GetExpireTimeCommand(createCudpLink(), timeout);
    long expireTime = getExpireTimeCommand.execute(remoteBroadcastAddress, getExpireTimeCommand.createRequest(getNextUID(), uid).setBroadcast(true)).getExpireTime();
    return new ExpireTime(expireTime);
  }

  private ICudpLink createCudpLink() {
    return new CudpLink(this);
  }

  void addToLinked(final DeviceUID uid, final ICudpLinkHandler cudpLinkHandler) throws CudpConnectionException {
    if (cudpLinkMap.putIfAbsent(uid, cudpLinkHandler) != null) {
      throw new CudpConnectionException("[" + this + "] - DeviceUID [" + uid + "] already linked");
    }
  }

  void removeFromLinked(final DeviceUID uid) {
    cudpLinkMap.remove(uid);
  }

  void sendCudpPacket(final SocketAddress remoteAddress, final CudpPacket cudpPacket) throws CudpException {
    cudpStream.sendCudpPacket(remoteAddress, cudpPacket);
  }

  private DeviceUID getNextUID() {
    int newUIDDeviceNumber = uidDeviceNumberCounterAtomic.getAndUpdate(n -> n >= DeviceUID.MAX_DEVICE_NUMBER ? 0 : n + 1);
    return new DeviceUID(DeviceType.SERVER, newUIDDeviceNumber);
  }

  private class CudpConnectionThread extends Thread {

    public CudpConnectionThread() {
      super("CudpConnectionThread (" + cudpStream + ")");
    }

    @Override
    public void run() {
      final CudpPacket cudpPacketAck = new CudpPacket();
      SocketAddress sourceAddress;
      while (!Thread.interrupted()) {
        try {
          try {
            sourceAddress = cudpStream.receiveCudpPacket(cudpPacketAck);
          } catch (CudpPacketReadException e) {
            logger.warn("[" + this + "] - receiving problem", e);
            continue;
          }
          if (cudpPacketAck.getVersion() != CUDP_PROTOCOL_VERSION) {
            logger.warn("[{}] - unsupported protocol version [{}], support protocol version [{}] or lower - packet dropped",
                this, cudpPacketAck.getVersion(), CUDP_PROTOCOL_VERSION);
            continue;
          }
          if (!cudpPacketAck.isACKFlag()) {
            logger.warn("[{}] - received request packet - packet dropped", this);
            continue;
          }

          ICudpLinkHandler cudpLinkHandler = cudpLinkMap.get(new DeviceUID(cudpPacketAck.getDUID()));
          if (cudpLinkHandler == null) {
            logger.warn("[{}] - received packet without request or too late [{}] - packet dropped", this, cudpPacketAck);
            continue;
          }
          cudpLinkHandler.handleReceive(sourceAddress, cudpPacketAck);
        } catch (CudpIOException e) {
          if (e.getCause() instanceof ClosedChannelException || e.getCause() instanceof AsynchronousCloseException) {
            Thread.currentThread().interrupt();
          } else if (e.getCause() instanceof SocketException) {
            logger.warn("[" + this + "] - socket problem - ignored", e);
          } else {
            logger.error("[" + this + "] - unexpected IOException - shutdown current thread", e);
            try {
              cudpStream.close();
            } catch (CudpIOException e1) {
              logger.error("[" + this + "] - unexpected IOException while disconnect during shutdown current thread ", e);
            }
            Thread.currentThread().interrupt();
          }
        }
      }
    }
  }

  @Override
  public String toString() {
    return toS;
  }

}
