/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.command.GetExpireTimeCommand.GetExpireTimeRequest;
import com.flamesgroup.device.protocol.cudp.command.GetExpireTimeCommand.GetExpireTimeResponse;

public class GetExpireTimeCommand extends AtomicCudpCommand<GetExpireTimeRequest, GetExpireTimeResponse> {

  public GetExpireTimeCommand(final ICudpLink cudpLink, final long responseTimeout) {
    super(cudpLink, responseTimeout);
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return e;
  }

  @Override
  public GetExpireTimeRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new GetExpireTimeRequest(sduid, dduid);
  }

  @Override
  public GetExpireTimeResponse createResponse() {
    return new GetExpireTimeResponse();
  }

  public static class GetExpireTimeRequest implements ICudpCommandRequest {

    private final DeviceUID sduid;
    private final DeviceUID dduid;

    private boolean broadcast;

    public GetExpireTimeRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() throws CudpCommandException {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_GET_EXPIRE_TIME);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast);
      return cudpPacket;
    }

    public GetExpireTimeRequest setBroadcast(final boolean broadcast) {
      this.broadcast = broadcast;
      return this;
    }

  }

  public static class GetExpireTimeResponse implements ICudpCommandResponse {

    private long expireTime;

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_GET_EXPIRE_TIME) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_GET_EXPIRE_TIME));
      }

      expireTime = cudpPacket.getExpireTime();
    }

    public long getExpireTime() {
      return expireTime;
    }

  }

}
