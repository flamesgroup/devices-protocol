/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import java.io.Serializable;
import java.util.Date;

public final class ExpireTime implements Serializable {

  private static final long serialVersionUID = 6246570916089167853L;
  private static final long UNLIMITED = 0xFFFFFFFFFFFFFFFFL;

  private final long value;

  public ExpireTime(final long value) {
    this.value = value;
  }

  public long getValue() {
    return value;
  }

  public Date getValueInDate() {
    return new Date(value);
  }

  public boolean isUnlimited() {
    return value == UNLIMITED;
  }

}
