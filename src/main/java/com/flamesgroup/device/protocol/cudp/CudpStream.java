/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.helper.DataRepresentationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class CudpStream implements ICudpStream {

  private final Logger logger = LoggerFactory.getLogger(CudpStream.class);

  private final Lock cudpStreamLock = new ReentrantLock();
  private final AtomicReference<String> atomicToS = new AtomicReference<>();

  private DatagramChannel cudpDatagramChannel;

  private boolean isOpenLocal(final DatagramChannel cudpDatagramChannelLocal) {
    return cudpDatagramChannelLocal != null && cudpDatagramChannelLocal.isOpen();
  }

  private DatagramChannel getCudpDatagramChannelLocal() {
    cudpStreamLock.lock();
    try {
      return cudpDatagramChannel;
    } finally {
      cudpStreamLock.unlock();
    }
  }

  @Override
  public boolean isOpen() {
    return isOpenLocal(getCudpDatagramChannelLocal());
  }

  @Override
  public void open(final SocketAddress localSocketAddress) throws CudpIOException {
    cudpStreamLock.lock();
    try {
      cudpDatagramChannel = DatagramChannel.open();
      cudpDatagramChannel.configureBlocking(true);
      cudpDatagramChannel.setOption(StandardSocketOptions.SO_BROADCAST, true);
      cudpDatagramChannel.bind(localSocketAddress);
    } catch (IOException e) {
      throw new CudpIOException(e);
    } finally {
      cudpStreamLock.unlock();
      updateToS();
    }
  }

  @Override
  public void close() throws CudpIOException {
    cudpStreamLock.lock();
    try {
      if (cudpDatagramChannel == null) {
        return;
      }

      cudpDatagramChannel.close();
      cudpDatagramChannel = null;
    } catch (IOException e) {
      throw new CudpIOException(e);
    } finally {
      cudpStreamLock.unlock();
      updateToS();
    }
  }

  @Override
  public SocketAddress receiveCudpPacket(final CudpPacket cudpPacket) throws CudpIOException, CudpPacketReadException {
    DatagramChannel cudpDatagramChannelLocal = getCudpDatagramChannelLocal();
    if (!isOpenLocal(cudpDatagramChannelLocal)) {
      throw new CudpIOException(new ClosedChannelException());
    }

    SocketAddress socketAddress;
    ByteBuffer readBuffer = ByteBuffer.allocate(76).order(ByteOrder.LITTLE_ENDIAN);
    readBuffer.clear();
    try {
      socketAddress = cudpDatagramChannelLocal.receive(readBuffer);
    } catch (IOException e) {
      throw new CudpIOException(e);
    }
    readBuffer.flip();
    try {
      readBuffer.mark();
      cudpPacket.readFromByteBuffer(readBuffer);
      logger.trace("[{}] - received [{}]", this, cudpPacket);
    } catch (CudpPacketReadException e) {
      readBuffer.reset();
      logger.warn("[{}] - can't decode receive buffer [{}]", this, DataRepresentationHelper.toHexArrayString(readBuffer), e);
      throw e;
    }
    return socketAddress;
  }

  @Override
  public void sendCudpPacket(final SocketAddress socketAddress, final CudpPacket cudpPacket) throws CudpIOException, CudpPacketWriteException {
    DatagramChannel cudpDatagramChannelLocal = getCudpDatagramChannelLocal();
    if (!isOpenLocal(cudpDatagramChannelLocal)) {
      throw new CudpIOException(new ClosedChannelException());
    }

    ByteBuffer writeBuffer = ByteBuffer.allocate(76).order(ByteOrder.LITTLE_ENDIAN);
    writeBuffer.clear();
    try {
      cudpPacket.writeToByteBuffer(writeBuffer);
    } catch (CudpPacketWriteException e) {
      logger.warn("[{}] - can't encode send packet [{}]", this, cudpPacket, e);
      throw e;
    }
    writeBuffer.flip();
    try {
      cudpDatagramChannelLocal.send(writeBuffer, socketAddress);
      logger.trace("[{}] - sent [{}]", this, cudpPacket);
    } catch (IOException e) {
      throw new CudpIOException(e);
    }
  }

  private void updateToS() {
    String localAddressStatus;
    final DatagramChannel cudpDatagramChannelLocal = getCudpDatagramChannelLocal();
    if (cudpDatagramChannelLocal != null) {
      try {
        localAddressStatus = cudpDatagramChannelLocal.getLocalAddress().toString();
      } catch (IOException e) {
        localAddressStatus = e.getMessage();
      }
    } else {
      localAddressStatus = "not opened";
    }
    atomicToS.set(String.format("%s@%x:[%s]", getClass().getSimpleName(), hashCode(), localAddressStatus));
  }

  @Override
  public String toString() {
    return atomicToS.get();
  }

}
