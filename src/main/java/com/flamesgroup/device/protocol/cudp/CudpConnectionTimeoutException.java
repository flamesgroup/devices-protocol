/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

public class CudpConnectionTimeoutException extends CudpConnectionException {

  private static final long serialVersionUID = -1136506450810771907L;

  public CudpConnectionTimeoutException() {
  }

  public CudpConnectionTimeoutException(final String message) {
    super(message);
  }

  public CudpConnectionTimeoutException(final Throwable throwable) {
    super(throwable);
  }

  public CudpConnectionTimeoutException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
