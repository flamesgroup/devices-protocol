/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.DeviceUID;

import java.net.SocketAddress;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CudpLink implements ICudpLink {

  private final CudpConnection cudpConnection;

  private final Lock linkLock = new ReentrantLock();

  private DeviceUID linkedDUID;

  public CudpLink(final CudpConnection cudpConnection) {
    this.cudpConnection = cudpConnection;
  }

  @Override
  public boolean isLinked() {
    linkLock.lock();
    try {
      return linkedDUID != null;
    } finally {
      linkLock.unlock();
    }
  }

  @Override
  public void link(final DeviceUID duid, final ICudpLinkHandler cudpLinkHandler) throws CudpException {
    linkLock.lock();
    try {
      cudpConnection.addToLinked(duid, cudpLinkHandler);
      linkedDUID = duid;
    } finally {
      linkLock.unlock();
    }
  }

  @Override
  public void unlink() {
    linkLock.lock();
    try {
      if (linkedDUID != null) {
        cudpConnection.removeFromLinked(linkedDUID);
        linkedDUID = null;
      }
    } finally {
      linkLock.unlock();
    }
  }

  @Override
  public void send(final SocketAddress remoteAddress, final CudpPacket packet) throws CudpException {
    cudpConnection.sendCudpPacket(remoteAddress, packet);
  }

}
