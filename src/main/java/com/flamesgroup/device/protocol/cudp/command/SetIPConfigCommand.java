/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigCommand.SetIPConfigRequest;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigCommand.SetIPConfigResponse;

public final class SetIPConfigCommand extends AtomicCudpCommand<SetIPConfigRequest, SetIPConfigResponse> {

  private static final byte ERRCODE_SET_IP_CONFIG_PARAMETERS_ERR = 0x10;
  private static final byte ERRCODE_SET_IP_CONFIG_LICENSE_INVALID = 0x11;
  private static final byte ERRCODE_SET_IP_CONFIG_LICENSE_EXPIRE = 0x12;

  public SetIPConfigCommand(final ICudpLink cudpLink, final long responseTimeout) {
    super(cudpLink, responseTimeout);
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    switch (e.getErrCode()) {
      case ERRCODE_SET_IP_CONFIG_PARAMETERS_ERR:
        return new SetIPConfigParametersErrException(e.getErrCode());
      case ERRCODE_SET_IP_CONFIG_LICENSE_INVALID:
        return new SetIPConfigLicenseInvalidException(e.getErrCode());
      case ERRCODE_SET_IP_CONFIG_LICENSE_EXPIRE:
        return new SetIPConfigLicenseExpireException(e.getErrCode());
      default:
        return e;
    }
  }

  @Override
  public SetIPConfigRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new SetIPConfigRequest(sduid, dduid);
  }

  @Override
  public SetIPConfigResponse createResponse() {
    return new SetIPConfigResponse();
  }

  public static class SetIPConfigRequest implements ICudpCommandRequest {

    private final DeviceUID sduid;
    private final DeviceUID dduid;
    private boolean broadcast;
    private IPConfig ipConfig;
    private long currentTime;

    private SetIPConfigRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_SET_IP_CONFIG);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast);
      cudpPacket.setIP(ipConfig.getIP());
      cudpPacket.setMask(ipConfig.getMask());
      cudpPacket.setGateway(ipConfig.getGateway());
      cudpPacket.setMasterIP(ipConfig.getMasterIP());
      cudpPacket.setUUID(ipConfig.getUUID());
      cudpPacket.setCurrentTime(currentTime);
      return cudpPacket;
    }

    public SetIPConfigRequest setBroadcast(final boolean broadcast) {
      this.broadcast = broadcast;
      return this;
    }

    public SetIPConfigRequest setIPConfig(final IPConfig ipConfig) {
      this.ipConfig = ipConfig;
      return this;
    }

    public SetIPConfigRequest setCurrentTime(final long currentTime) {
      this.currentTime = currentTime;
      return this;
    }

  }

  public static class SetIPConfigResponse implements ICudpCommandResponse {

    private SetIPConfigResponse() {
      super();
    }

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_SET_IP_CONFIG) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_SET_IP_CONFIG));
      }
    }

  }

}
