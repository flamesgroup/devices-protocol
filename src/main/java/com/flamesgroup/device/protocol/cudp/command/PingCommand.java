/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.command.PingCommand.PingRequest;
import com.flamesgroup.device.protocol.cudp.command.PingCommand.PingResponse;

public final class PingCommand extends AtomicCudpCommand<PingRequest, PingResponse> {

  public PingCommand(final ICudpLink cudpLink, final long responseTimeout) {
    super(cudpLink, responseTimeout);
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return e;
  }

  @Override
  public PingRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new PingRequest(sduid, dduid);
  }

  @Override
  public PingResponse createResponse() {
    return new PingResponse();
  }

  public static class PingRequest implements ICudpCommandRequest {

    private final DeviceUID sduid;
    private final DeviceUID dduid;
    private boolean broadcast;
    private long timestamp;

    private PingRequest(final DeviceUID sduid, final DeviceUID dduid) {
      this.sduid = sduid;
      this.dduid = dduid;
    }

    @Override
    public DeviceUID getSDUID() {
      return sduid;
    }

    @Override
    public DeviceUID getDDUID() {
      return dduid;
    }

    @Override
    public CudpPacket toCudpPacket() {
      CudpPacket cudpPacket = new CudpPacket();
      cudpPacket.setVersion(CudpConnection.CUDP_PROTOCOL_VERSION);
      cudpPacket.setType(CudpPacket.TYPE_PING);
      cudpPacket.setSUID(sduid.getUID()).setDUID(dduid.getUID());
      cudpPacket.setBTFlag(broadcast).setTimestamp(timestamp);
      return cudpPacket;
    }

    public PingRequest setBroadcast(final boolean broadcast) {
      this.broadcast = broadcast;
      return this;
    }

    public PingRequest setTimestamp(final long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

  }

  public static class PingResponse implements ICudpCommandResponse {

    private long timestamp;

    private PingResponse() {
      super();
    }

    @Override
    public void fromCudpPacket(final CudpPacket cudpPacket) throws CudpCommandException {
      if (cudpPacket.getType() != CudpPacket.TYPE_PING) {
        throw new CudpCommandException(String.format("Wrong ack packet type of received packet [%s] must be [%s]", cudpPacket.getType(), CudpPacket.TYPE_PING));
      }
      timestamp = cudpPacket.getTimestamp();
    }

    public long getTimestamp() {
      return timestamp;
    }

  }

}
