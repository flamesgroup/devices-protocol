/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ByteArrayHelper {

  private ByteArrayHelper() {
  }

  public static void putIntBigEndian(final byte[] ba, final int position, final int v) {
    ByteBuffer bb = ByteBuffer.wrap(ba).order(ByteOrder.BIG_ENDIAN);
    bb.position(position);
    bb.putInt(v);
  }

  public static void putIntLittleEndian(final byte[] ba, final int position, final int v) {
    ByteBuffer bb = ByteBuffer.wrap(ba).order(ByteOrder.LITTLE_ENDIAN);
    bb.position(position);
    bb.putInt(v);
  }

  public static short getShortBigEndian(final byte[] ba, final int position) {
    ByteBuffer bb = ByteBuffer.wrap(ba).order(ByteOrder.BIG_ENDIAN);
    bb.position(position);
    return bb.getShort();
  }

  public static short getShortLittleEndian(final byte[] ba, final int position) {
    ByteBuffer bb = ByteBuffer.wrap(ba).order(ByteOrder.LITTLE_ENDIAN);
    bb.position(position);
    return bb.getShort();
  }

  public static int getIntBigEndian(final byte[] ba, final int position) {
    ByteBuffer bb = ByteBuffer.wrap(ba).order(ByteOrder.BIG_ENDIAN);
    bb.position(position);
    return bb.getInt();
  }

  public static int getIntLittleEndian(final byte[] ba, final int position) {
    ByteBuffer bb = ByteBuffer.wrap(ba).order(ByteOrder.LITTLE_ENDIAN);
    bb.position(position);
    return bb.getInt();
  }

}
