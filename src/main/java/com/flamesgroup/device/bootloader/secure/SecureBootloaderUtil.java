/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.secure;

import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.i2c.jni.I2CDev;

import java.util.List;

public class SecureBootloaderUtil {

  public static byte[] readCpuUid(final I2CDev i2cDev) throws SecureBootloaderException, DevException, InterruptedException {
    SecureBootloader secureBootloader = new SecureBootloader(i2cDev);
    secureBootloader.open();
    try {
      return secureBootloader.getCpuUid();
    } finally {
      secureBootloader.close();
    }
  }

  public static void uploadFirmware(final I2CDev i2cDev, final List<CipheredFWBlock> blocks, final int firmwareCrc32,
      final IUploadFirmwareHandler handler) throws SecureBootloaderException, DevException, InterruptedException {
    SecureBootloader secureBootloader = new SecureBootloader(i2cDev);
    secureBootloader.open();
    try {
      handler.handleStartBegin();
      secureBootloader.startFirmwareDownload();
      handler.handleStartEnd();
      handler.handleWriteBegin();
      for (int i = 0; i < blocks.size(); i++) {
        CipheredFWBlock firmwareBlock = blocks.get(i);
        secureBootloader.writeCipheredFirmwareBlock(i, firmwareBlock);

        handler.handleWriteProgress(i, blocks.size());
      }
      handler.handleWriteEnd();
      handler.handleFinalizeBegin();
      secureBootloader.finalizeFirmwareDownload(firmwareCrc32);
      handler.handleFinalizeEnd();
    } finally {
      secureBootloader.close();
    }
  }

  public interface IUploadFirmwareHandler {

    void handleStartBegin();

    void handleStartEnd();

    void handleWriteBegin();

    void handleWriteProgress(int firmwareBlocksWritten, int firmwareBlocksCount);

    void handleWriteEnd();

    void handleFinalizeBegin();

    void handleFinalizeEnd();

  }

}
