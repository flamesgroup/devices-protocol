/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.secure;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.bootloader.ByteArrayHelper;
import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.device.commonb.FirmwareType;
import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.i2c.II2CDev;

import java.util.Arrays;
import java.util.Objects;

public class SecureBootloader implements ISecureBootloader {

  private final II2CDev i2CDev;

  public SecureBootloader(final II2CDev i2CDev) {
    Objects.requireNonNull(i2CDev, "i2CDev mustn't be null");
    this.i2CDev = i2CDev;
  }

  @Override
  public boolean isOpen() {
    return i2CDev.isOpen();
  }

  @Override
  public void open() throws DevException {
    i2CDev.open();
  }

  @Override
  public void close() throws DevException {
    i2CDev.close();
  }

  @Override
  public SecureBootloaderVersions getVersion() throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.GET_VERSION);
    waitAck();

    byte[] ba = new byte[9];
    readData(ba);
    checkLastByteAsChecksum(ba);

    int secureBootloaderVersion = ByteArrayHelper.getIntLittleEndian(ba, 0);
    int firmwareVersion = ByteArrayHelper.getIntLittleEndian(ba, 4);

    return new SecureBootloaderVersions(secureBootloaderVersion, firmwareVersion);
  }

  @Override
  public byte[] getCpuUid() throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.GET_CPU_UID);
    waitAck();

    byte[] ba = new byte[13];
    readData(ba);
    checkLastByteAsChecksum(ba);

    return Arrays.copyOf(ba, 12);
  }

  @Override
  public DeviceUID getDeviceUid() throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.GET_DEVICE_UID);
    waitAck();

    byte[] ba = new byte[5];
    readData(ba);
    checkLastByteAsChecksum(ba);

    int uid = ByteArrayHelper.getIntLittleEndian(ba, 0);

    return new DeviceUID(uid);
  }

  @Override
  public void startFirmwareDownload() throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.START_FW);
    waitAck();
  }

  @Override
  public void finalizeFirmwareDownload(final int crc32) throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.FINALIZE_FW);
    waitAck();

    byte[] ba = new byte[5];
    ByteArrayHelper.putIntLittleEndian(ba, 0, crc32);
    initLastByteAsChecksum(ba);
    writeData(ba);
    waitAck();
  }

  @Override
  public void writeCipheredFirmwareBlock(final int number, final CipheredFWBlock block) throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.WRITE_BLOCK);
    waitAck();

    byte[] ba = new byte[5];
    ByteArrayHelper.putIntLittleEndian(ba, 0, number);
    initLastByteAsChecksum(ba);
    writeData(ba);
    waitAck();

    ba = new byte[CipheredFWBlock.IV_LENGTH + CipheredFWBlock.DATA_LENGTH + 1];
    System.arraycopy(block.getIV(), 0, ba, 0, block.getIV().length);
    System.arraycopy(block.getData(), 0, ba, block.getIV().length, block.getData().length);
    initLastByteAsChecksum(ba);
    writeData(ba);
    waitAck();
  }

  @Override
  public FirmwareType getFirmwareType() throws SecureBootloaderException, DevException, InterruptedException {
    sendCommand(Command.GET_FW_TYPE);
    waitAck();

    byte[] ba = new byte[5];
    readData(ba);
    checkLastByteAsChecksum(ba);

    int n = ByteArrayHelper.getIntLittleEndian(ba, 0);

    return FirmwareType.getFirmwareTypeByN(n);
  }

  private void sendCommand(final Command command) throws SecureBootloaderException, DevException, InterruptedException {
    byte[] ba = new byte[2];
    ba[0] = command.getValue();
    byte checksum = crcxor(ba[0], (byte) 0xFF);
    ba[ba.length - 1] = checksum;

    writeData(ba);
  }

  private void waitAck() throws SecureBootloaderException, DevException, InterruptedException {
    byte[] ba = new byte[1];

    int r = i2CDev.read(ba, 0, ba.length);
    if (r == 0) {
      throw new SecureBootloaderException("Acknowledgement response timeout");
    } else {
      if (ba[0] == Answer.NO_ERR.getValue()) {
        // return;
      } else if (ba[0] == Answer.EXEC_ERR.getValue()) {
        throw new SecureBootloaderException("Get " + Answer.EXEC_ERR);
      } else if (ba[0] == Answer.CRC_ERR.getValue()) {
        throw new SecureBootloaderException("Get " + Answer.CRC_ERR);
      } else {
        throw new IllegalStateException("Get undefined response: [" + DataRepresentationHelper.toHexString(ba[0]) + "]");
      }
    }
  }

  private void readData(final byte[] ba, final int off, final int len) throws SecureBootloaderException, DevException, InterruptedException {
    int r = i2CDev.read(ba, off, len);
    if (r != len) {
      throw new SecureBootloaderException("Can't read " + len + " bytes, read only " + r);
    }
  }

  private void readData(final byte[] ba) throws SecureBootloaderException, DevException, InterruptedException {
    readData(ba, 0, ba.length);
  }

  private void writeData(final byte[] ba, final int off, final int len) throws SecureBootloaderException, DevException, InterruptedException {
    int w = i2CDev.write(ba, off, len);
    if (w != len) {
      throw new SecureBootloaderException("Can't write " + len + " bytes, write only " + w);
    }
  }

  private void writeData(final byte[] ba) throws SecureBootloaderException, DevException, InterruptedException {
    writeData(ba, 0, ba.length);
  }

  private void checkLastByteAsChecksum(final byte[] ba) throws SecureBootloaderException {
    byte receivedChecksum = ba[ba.length - 1];
    byte calculatedChecksum = crcxor(ba, 0, ba.length - 1);
    if (calculatedChecksum != receivedChecksum) {
      throw new SecureBootloaderException("Invalid checksum: " +
          "received [" + DataRepresentationHelper.toHexString(receivedChecksum) + "] - " +
          "calculated [" + DataRepresentationHelper.toHexString(calculatedChecksum) + "]");
    }
  }

  private void initLastByteAsChecksum(final byte[] ba) throws SecureBootloaderException {
    ba[ba.length - 1] = crcxor(ba, 0, ba.length - 1);
  }

  private byte crcxor(final byte[] ba, final int off, final int len) {
    byte checksum = 0;
    for (int i = off; i < len; i++) {
      checksum ^= ba[i];
    }
    return checksum;
  }

  private byte crcxor(final byte... ba) {
    return crcxor(ba, 0, ba.length);
  }

  public enum Command {
    GET_VERSION((byte) 0x01),
    GET_CPU_UID((byte) 0x02),
    GET_DEVICE_UID((byte) 0x03),
    START_FW((byte) 0x04),
    FINALIZE_FW((byte) 0x05),
    WRITE_BLOCK((byte) 0x06),
    GET_FW_TYPE((byte) 0x07);

    private final byte value;

    Command(final byte value) {
      this.value = value;
    }

    public byte getValue() {
      return value;
    }

    public static Command valueOf(final byte value) {
      for (final Command command : Command.values()) {
        if (command.getValue() == value) {
          return command;
        }
      }
      return null;
    }
  }

  public enum Answer {

    NO_ERR((byte) 0x79),
    EXEC_ERR((byte) 0x1F),
    CRC_ERR((byte) 0x1E);

    private final byte value;

    Answer(final byte value) {
      this.value = value;
    }

    public byte getValue() {
      return value;
    }

  }

}
