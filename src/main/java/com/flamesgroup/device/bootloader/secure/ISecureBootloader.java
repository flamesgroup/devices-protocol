/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.secure;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.CipheredFWBlock;
import com.flamesgroup.device.commonb.FirmwareType;
import com.flamesgroup.jdev.DevException;

public interface ISecureBootloader {

  boolean isOpen();

  void open() throws DevException;

  void close() throws DevException;

  SecureBootloaderVersions getVersion() throws SecureBootloaderException, DevException, InterruptedException;

  byte[] getCpuUid() throws SecureBootloaderException, DevException, InterruptedException;

  DeviceUID getDeviceUid() throws SecureBootloaderException, DevException, InterruptedException;

  void startFirmwareDownload() throws SecureBootloaderException, DevException, InterruptedException;

  void finalizeFirmwareDownload(int crc32) throws SecureBootloaderException, DevException, InterruptedException;

  void writeCipheredFirmwareBlock(int number, CipheredFWBlock block) throws SecureBootloaderException, DevException, InterruptedException;

  FirmwareType getFirmwareType() throws SecureBootloaderException, DevException, InterruptedException;

}
