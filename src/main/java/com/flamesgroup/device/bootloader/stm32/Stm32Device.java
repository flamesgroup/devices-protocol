/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.stm32;

import com.flamesgroup.device.bootloader.ByteArrayHelper;

public final class Stm32Device {

  private final ConnectionType connectionType;
  private final byte bootloaderVersion;
  private SupportedCommands supportedCommands;
  private short pid;
  private Stm32BootloaderDeviceDependentParameters parameters;

  private Stm32Device(final ConnectionType connectionType, final byte bootloaderVersion) {
    this.connectionType = connectionType;
    this.bootloaderVersion = bootloaderVersion;
  }

  public static Stm32Device createWithConnectionTypeAndGetVersionCommandResponse(final ConnectionType connectionType, final byte[] getVersionCommandResponse) {
    return new Stm32Device(connectionType, getVersionCommandResponse[0]);
  }

  public void initWithGetCommandResponse(final byte[] getCommandResponse) {
    if (getCommandResponse[0] != getGetCommandN()) {
      throw new IllegalStateException("Illegal number of bytes: [" + String.format("0x%02X", getCommandResponse[0]) + "] in Get command response");
    }
    if (getCommandResponse[1] != bootloaderVersion) {
      throw new IllegalStateException("Illegal bootloader version: [" + String.format("0x%02X", getCommandResponse[1]) + "] in Get command response");
    }

    supportedCommands = new SupportedCommands();
    supportedCommands.init(getCommandResponse, 2, getGetCommandN());
  }

  public void initWithGetId(final byte[] getIdCommandResponse) {
    if (getIdCommandResponse[0] != getGetIdCommandN()) {
      throw new IllegalStateException("Illegal number of bytes: [" + String.format("0x%02X", getIdCommandResponse[0]) + "] in Get ID command response");
    }
    pid = ByteArrayHelper.getShortBigEndian(getIdCommandResponse, 1);
    parameters = Stm32BootloaderDeviceDependentParameters.findByPid(pid);
  }

  public int getGetCommandN() {
    switch (connectionType) {
      case UART:
        switch (bootloaderVersion) {
          case 0x31:
            return 11;
          default:
            throw new IllegalStateException("Unsupported bootloader version: [" + String.format("0x%02X", bootloaderVersion) + "]");
        }
      case I2C:
        switch (bootloaderVersion) {
          case 0x10:
            return 11;
          case 0x11:
            return 17;
          default:
            throw new IllegalStateException("Unsupported bootloader version: [" + String.format("0x%02X", bootloaderVersion) + "]");
        }
      default:
        throw new IllegalStateException("Unsupported connection type: [" + connectionType + "]");
    }
  }

  public int getGetIdCommandN() {
    return 1;
  }

  public int getBootloaderVersion() {
    return this.bootloaderVersion;
  }

  public SupportedCommands getSupportedCommands() {
    return this.supportedCommands;
  }

  public int getPid() {
    return this.pid;
  }

  public Stm32BootloaderDeviceDependentParameters getParameters() {
    return parameters;
  }

  public enum ConnectionType {
    UART, I2C
  }

  public class SupportedCommands {

    private Stm32Bootloader.Command get;
    private Stm32Bootloader.Command getVersion;
    private Stm32Bootloader.Command getId;
    private Stm32Bootloader.Command readMemory;
    private Stm32Bootloader.Command go;
    private Stm32Bootloader.Command writeMemory;
    private Stm32Bootloader.Command eraseMemory;
    private Stm32Bootloader.Command writeProtect;
    private Stm32Bootloader.Command writeUnprotect;
    private Stm32Bootloader.Command readoutProtect;
    private Stm32Bootloader.Command readoutUnprotect;
    private Stm32Bootloader.Command computeCRC;

    public void init(final byte[] buffer, final int offset, final int length) {
      for (int i = 0; i < length; i++) {
        byte commandCode = buffer[i + offset];
        Stm32Bootloader.Command command = Stm32Bootloader.Command.valueOf(commandCode);
        if (command == null) {
          throw new IllegalStateException("Can't decode command with value: [" + String.format("0x%02X", commandCode) + "]");
        }
        switch (command) {
          case GET:
            get = command;
            break;
          case GET_VERSION:
            getVersion = command;
            break;
          case GET_ID:
            getId = command;
            break;
          case READ_MEMORY:
            readMemory = command;
            break;
          case GO:
            go = command;
            break;
          case WRITE_MEMORY:
          case NS_WRITE_MEMORY:
            writeMemory = command;
            break;
          case ERASE_MEMORY:
          case EXTENDED_ERASE_MEMORY:
          case NS_EXTENDED_ERASE_MEMORY:
            eraseMemory = command;
            break;
          case WRITE_PROTECT:
          case NS_WRITE_PROTECT:
            writeProtect = command;
            break;
          case WRITE_UNPROTECT:
          case NS_WRITE_UNPROTECT:
            writeUnprotect = command;
            break;
          case READOUT_PROTECT:
          case NS_READOUT_PROTECT:
            readoutProtect = command;
            break;
          case READOUT_UNPROTECT:
          case NS_READOUT_UNPROTECT:
            readoutUnprotect = command;
            break;
          case STM32_CMD_CRC:
            this.computeCRC = command;
            break;
        }
      }

    }

    public Stm32Bootloader.Command getGet() {
      return get;
    }

    public Stm32Bootloader.Command getGetVersion() {
      return getVersion;
    }

    public Stm32Bootloader.Command getGetId() {
      return getId;
    }

    public Stm32Bootloader.Command getReadMemory() {
      return readMemory;
    }

    public Stm32Bootloader.Command getGo() {
      return go;
    }

    public Stm32Bootloader.Command getWriteMemory() {
      return writeMemory;
    }

    public Stm32Bootloader.Command getEraseMemory() {
      return eraseMemory;
    }

    public Stm32Bootloader.Command getWriteProtect() {
      return writeProtect;
    }

    public Stm32Bootloader.Command getWriteUnprotect() {
      return writeUnprotect;
    }

    public Stm32Bootloader.Command getReadoutProtect() {
      return readoutProtect;
    }

    public Stm32Bootloader.Command getReadoutUnprotect() {
      return readoutUnprotect;
    }

    public Stm32Bootloader.Command getComputeCRC() {
      return computeCRC;
    }

  }

}
