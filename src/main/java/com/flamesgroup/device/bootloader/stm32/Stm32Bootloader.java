/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.stm32;

import com.flamesgroup.device.bootloader.ByteArrayHelper;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.IDev;
import com.flamesgroup.jdev.i2c.II2CDev;
import com.flamesgroup.jdev.uart.IUARTDev;

import java.util.Objects;

public class Stm32Bootloader implements IStm32Bootloader {

  private static final int GENERAL_TIMEOUT = 0;
  private static final int WRITE_TIMEOUT = 500;
  private static final int PROTECT_TIMEOUT = 1000;
  private static final int ERASE_TIMEOUT = 35000;

  private final IDev dev;
  private final Stm32Device.ConnectionType connectionType;
  private Stm32Device device;
  private boolean initBootloader = false;

  public Stm32Bootloader(final IDev dev) {
    Objects.requireNonNull(dev, "dev mustn't be null");
    if (dev instanceof IUARTDev) {
      connectionType = Stm32Device.ConnectionType.UART;
    } else if (dev instanceof II2CDev) {
      connectionType = Stm32Device.ConnectionType.I2C;
    } else {
      throw new IllegalArgumentException("Unsupported dev class: [" + dev.getClass() + "]");
    }

    this.dev = dev;
  }

  @Override
  public boolean isOpen() {
    return dev.isOpen();
  }

  @Override
  public void open() throws Stm32BootloaderException, DevException, InterruptedException {
    dev.open();
    try {
      int getVersionResponseLength;
      switch (connectionType) {
        case UART:
          getVersionResponseLength = 3;
          break;
        case I2C:
          getVersionResponseLength = 1;
          break;
        default:
          throw new IllegalStateException("mustn't be here");
      }

      sendCommand(Command.GET_VERSION);
      waitAck(GENERAL_TIMEOUT);

      byte[] getVersionCommandResponse = new byte[getVersionResponseLength];
      readData(getVersionCommandResponse);
      waitAck(GENERAL_TIMEOUT);

      device = Stm32Device.createWithConnectionTypeAndGetVersionCommandResponse(connectionType, getVersionCommandResponse);

      sendCommand(Command.GET);
      waitAck(GENERAL_TIMEOUT);

      byte[] getCommandResponse = new byte[device.getGetCommandN() + 1 + 1];
      readData(getCommandResponse);
      waitAck(GENERAL_TIMEOUT);

      device.initWithGetCommandResponse(getCommandResponse);

      sendCommand(Command.GET_ID);
      waitAck(GENERAL_TIMEOUT);

      byte[] getIdCommandResponse = new byte[device.getGetIdCommandN() + 1 + 1];
      readData(getIdCommandResponse);
      waitAck(GENERAL_TIMEOUT);

      device.initWithGetId(getIdCommandResponse);
    } catch (Exception e) {
      dev.close();
      throw e;
    }
  }

  @Override
  public void close() throws DevException {
    dev.close();
  }

  @Override
  public void read(final int address, final byte[] data, final int dataOffset, final int dataLength) throws Stm32BootloaderException, DevException, InterruptedException {
    if (dataLength < 0 && dataLength > 256) {
      throw new Stm32BootloaderException("dataLength must be from 0 till 256");
    }

    Command readMemoryCommand = device.getSupportedCommands().getReadMemory();

    sendCommand(readMemoryCommand);
    waitAck(GENERAL_TIMEOUT);

    sendAddress(address);
    waitAck(GENERAL_TIMEOUT);

    byte[] ba = new byte[2];
    ba[0] = (byte) (dataLength - 1);
    byte checksum = (byte) (ba[0] ^ 0xFF);
    ba[ba.length - 1] = checksum;

    writeData(ba);
    waitAck(GENERAL_TIMEOUT);

    readData(data, dataOffset, dataLength);
  }

  @Override
  public void write(final int address, final byte[] data, final int dataOffset, final int dataLength) throws Stm32BootloaderException, DevException, InterruptedException {
    if (dataLength < 0 && dataLength > 256) {
      throw new Stm32BootloaderException("dataLength must be from 0 till 256");
    }
    if ((address & 0x3) != 0 || (dataLength & 0x3) != 0) {
      throw new Stm32BootloaderException("WRITE address and length must be 4 byte aligned");
    }

    Command writeMemoryCommand = device.getSupportedCommands().getWriteMemory();

    sendCommand(writeMemoryCommand);
    waitAck(GENERAL_TIMEOUT);

    sendAddress(address);
    waitAck(GENERAL_TIMEOUT);

    byte[] ba = new byte[dataLength + 2];
    ba[0] = (byte) (dataLength - 1);
    System.arraycopy(data, dataOffset, ba, 1, dataLength);
    byte checksum = 0;
    for (int i = 0; i < ba.length - 1; i++) {
      checksum ^= ba[i];
    }
    ba[ba.length - 1] = checksum;

    writeData(ba);
    waitAck(WRITE_TIMEOUT);
  }

  @Override
  public void globalErase() throws Stm32BootloaderException, DevException, InterruptedException {
    Command eraseMemoryCommand = device.getSupportedCommands().getEraseMemory();

    sendCommand(eraseMemoryCommand);
    waitAck(GENERAL_TIMEOUT);

    if (eraseMemoryCommand == Command.ERASE_MEMORY) {
      byte[] ba = new byte[2];
      ba[0] = (byte) 0xFF;
      ba[1] = (byte) 0x00;

      writeData(ba);
      waitAck(ERASE_TIMEOUT);
    } else {
      byte[] ba = new byte[3];
      ba[0] = (byte) 0xFF;
      ba[1] = (byte) 0xFF;
      ba[2] = (byte) 0x00;

      writeData(ba);
      waitAck(ERASE_TIMEOUT);
    }
  }

  @Override
  public void readoutProtect() throws Stm32BootloaderException, DevException, InterruptedException {
    Command readoutProtectCommand = device.getSupportedCommands().getReadoutProtect();

    sendCommand(readoutProtectCommand);
    waitAck(GENERAL_TIMEOUT);

    waitAck(PROTECT_TIMEOUT);

    // after success command execution bootloader generates a system Reset
    // to take into account the new configuration of the option byte
    initBootloader = false;
  }

  @Override
  public void readoutUnprotect() throws Stm32BootloaderException, DevException, InterruptedException {
    Command readoutUnprotectCommand = device.getSupportedCommands().getReadoutUnprotect();

    sendCommand(readoutUnprotectCommand);
    waitAck(GENERAL_TIMEOUT);

    waitAck(ERASE_TIMEOUT);

    // after success command execution bootloader generates a system Reset
    // to take into account the new configuration of the option byte
    initBootloader = false;
  }

  @Override
  public void go(final int address) throws Stm32BootloaderException, DevException, InterruptedException {
    Command goCommand = device.getSupportedCommands().getGo();

    sendCommand(goCommand);
    waitAck(GENERAL_TIMEOUT);

    sendAddress(address);
    waitAck(GENERAL_TIMEOUT);
  }

  @Override
  public void runRawCode(final int targetAddress, final byte[] code) throws Stm32BootloaderException, DevException, InterruptedException {
    if ((targetAddress & 0x3) != 0) {
      throw new Stm32BootloaderException("code address must be 4 byte aligned");
    }

    int stackAddress = device.getParameters().getValidRamMemoryEnd();
    int codeAddress = targetAddress + 8 + 1; // +1 to switch in THUMB mode

    final byte[] goCode = new byte[(code.length / 4 + 1) * 4 + 4 + 4]; // make goCode length 4 byte aligned and add 2 addresses length
    ByteArrayHelper.putIntLittleEndian(goCode, 0, stackAddress);
    ByteArrayHelper.putIntLittleEndian(goCode, 4, codeAddress);
    System.arraycopy(code, 0, goCode, 8, code.length);

    int writeLeft = goCode.length;
    int offset = 0;
    int address = targetAddress;

    while (writeLeft > 0) {
      int writeLength = writeLeft > 256 ? 256 : writeLeft;
      write(address, goCode, offset, writeLength);
      offset += writeLength;
      address += writeLength;
      writeLeft -= writeLength;
    }

    go(targetAddress);
  }

  public Stm32Device getDevice() {
    return device;
  }

  private void initBootloader() throws Stm32BootloaderException, DevException {
    byte[] ba = new byte[] {Command.STM32_CMD_INIT.getValue()};
    dev.write(ba, 0, ba.length);
    byte[] bufferACK = new byte[1];

    int r = dev.read(bufferACK, 0, bufferACK.length);

    if (r == 0 || (bufferACK[0] != Answer.ACK.getValue() && bufferACK[0] != Answer.NACK.getValue())) {
      dev.write(ba, 0, ba.length);
      r = dev.read(bufferACK, 0, bufferACK.length);
      if (r == 0) {
        throw new Stm32BootloaderException("Device is not available");
      }
      if (bufferACK[0] != Answer.NACK.getValue()) {
        throw new Stm32BootloaderException("Failed to init device.");
      }
    }
  }

  private void sendCommand(final Command command) throws Stm32BootloaderException, DevException, InterruptedException {
    if (dev instanceof IUARTDev && !initBootloader) {
      initBootloader();
      initBootloader = true;
    }

    byte[] ba = new byte[2];
    ba[0] = command.getValue();
    byte checksum = (byte) (ba[0] ^ 0xFF);
    ba[ba.length - 1] = checksum;

    writeData(ba);
  }

  private void sendAddress(final int address) throws Stm32BootloaderException, DevException, InterruptedException {
    byte[] ba = new byte[5];
    ByteArrayHelper.putIntBigEndian(ba, 0, address);
    byte checksum = 0;
    for (int i = 0; i < 4; i++) {
      checksum ^= ba[i];
    }
    ba[ba.length - 1] = checksum;

    writeData(ba);
  }

  private void waitAck(final int timeout) throws Stm32BootloaderException, DevException, InterruptedException {
    long startWaitingTimeMillis = System.currentTimeMillis();
    byte[] ba = new byte[1];
    while (true) {
      int r = dev.read(ba, 0, ba.length);
      if (r == 0) {
        if (System.currentTimeMillis() - startWaitingTimeMillis >= timeout) {
          throw new Stm32BootloaderException("Get ACK timeout");
        }
        Thread.sleep(100);
      } else {
        if (ba[0] == Answer.ACK.getValue()) {
          return;
        } else if (ba[0] == Answer.NACK.getValue()) {
          throw new Stm32BootloaderException("Get NACK");
        } else if (ba[0] == Answer.BUSY.getValue()) {
          if (System.currentTimeMillis() - startWaitingTimeMillis >= timeout) {
            throw new Stm32BootloaderException("Get BUSY");
          }
          Thread.sleep(100);
        }
      }
    }
  }

  private void readData(final byte[] ba, final int off, final int len) throws Stm32BootloaderException, DevException, InterruptedException {
    int r = dev.read(ba, off, len);
    if (r != len) {
      throw new Stm32BootloaderException("Can't read " + len + " bytes, read only " + r);
    }
  }

  private void readData(final byte[] ba) throws Stm32BootloaderException, DevException, InterruptedException {
    readData(ba, 0, ba.length);
  }

  private void writeData(final byte[] ba, final int off, final int len) throws Stm32BootloaderException, DevException, InterruptedException {
    int w = dev.write(ba, off, len);
    if (w != len) {
      throw new Stm32BootloaderException("Can't write " + len + " bytes, write only " + w);
    }
  }

  private void writeData(final byte[] ba) throws Stm32BootloaderException, DevException, InterruptedException {
    writeData(ba, 0, ba.length);
  }

  public enum Command {

    STM32_CMD_INIT((byte) 0x7F),
    GET((byte) 0x00),
    GET_VERSION((byte) 0x01),
    GET_ID((byte) 0x02),
    READ_MEMORY((byte) 0x11),
    GO((byte) 0x21),
    WRITE_MEMORY((byte) 0x31),
    NS_WRITE_MEMORY((byte) 0x32),
    ERASE_MEMORY((byte) 0x43),
    EXTENDED_ERASE_MEMORY((byte) 0x44),
    NS_EXTENDED_ERASE_MEMORY((byte) 0x45),
    WRITE_PROTECT((byte) 0x63),
    NS_WRITE_PROTECT((byte) 0x64),
    WRITE_UNPROTECT((byte) 0x73),
    NS_WRITE_UNPROTECT((byte) 0x74),
    READOUT_PROTECT((byte) 0x82),
    NS_READOUT_PROTECT((byte) 0x83),
    READOUT_UNPROTECT((byte) 0x92),
    NS_READOUT_UNPROTECT((byte) 0x93),
    STM32_CMD_CRC((byte) 0xA1);

    private final byte value;

    Command(final byte value) {
      this.value = value;
    }

    public byte getValue() {
      return value;
    }

    public static Command valueOf(final byte value) {
      for (Command command : Command.values()) {
        if (command.getValue() == value) {
          return command;
        }
      }
      return null;
    }

  }

  public enum Answer {

    ACK((byte) 0x79),
    NACK((byte) 0x1F),
    BUSY((byte) 0x76);

    private final byte value;

    Answer(final byte value) {
      this.value = value;
    }

    public byte getValue() {
      return value;
    }

  }

}
