/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.stm32;

import com.flamesgroup.jdev.DevException;

public interface IStm32Bootloader {

  boolean isOpen();

  void open() throws Stm32BootloaderException, DevException, InterruptedException;

  void close() throws DevException;

  void read(int address, byte[] data, int dataOffset, int dataLength) throws Stm32BootloaderException, DevException, InterruptedException;

  void write(int address, byte[] data, int dataOffset, int dataLength) throws Stm32BootloaderException, DevException, InterruptedException;

  void globalErase() throws Stm32BootloaderException, DevException, InterruptedException;

  void readoutProtect() throws Stm32BootloaderException, DevException, InterruptedException;

  void readoutUnprotect() throws Stm32BootloaderException, DevException, InterruptedException;

  void go(int address) throws Stm32BootloaderException, DevException, InterruptedException;

  void runRawCode(int targetAddress, byte[] code) throws Stm32BootloaderException, DevException, InterruptedException;

}
