/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.stm32;

import java.util.LinkedList;
import java.util.List;

public final class Stm32BootloaderDeviceDependentParameters {

  public enum Series {
    F0, F1, F2, F4, L0, L1
  }

  private static final List<Stm32BootloaderDeviceDependentParameters> table = new LinkedList<>();

  private final Series series;
  private final String name;
  private final short pid;
  private final byte blId;
  private final int validRamMemoryBegin;
  private final int validRamMemoryEnd;
  private final int systemMemoryBegin;
  private final int systemMemoryEnd;

  public Stm32BootloaderDeviceDependentParameters(final Series series, final String name, final short pid, final byte blId, final int validRamMemoryBegin, final int validRamMemoryEnd,
      final int systemMemoryBegin, final int systemMemoryEnd) {
    this.series = series;
    this.name = name;
    this.pid = pid;
    this.blId = blId;
    this.validRamMemoryBegin = validRamMemoryBegin;
    this.validRamMemoryEnd = validRamMemoryEnd;
    this.systemMemoryBegin = systemMemoryBegin;
    this.systemMemoryEnd = systemMemoryEnd;
  }

  public Series getSeries() {
    return series;
  }

  public String getName() {
    return name;
  }

  public short getPid() {
    return pid;
  }

  public byte getBlId() {
    return blId;
  }

  public int getValidRamMemoryBegin() {
    return validRamMemoryBegin;
  }

  public int getValidRamMemoryEnd() {
    return validRamMemoryEnd;
  }

  public int getSystemMemoryBegin() {
    return systemMemoryBegin;
  }

  public int getSystemMemoryEnd() {
    return systemMemoryEnd;
  }

  static {
    table.add(new Stm32BootloaderDeviceDependentParameters(Series.F0, "STM32F03xxx", (short) 0x444, (byte) 0x10, 0x20000800, 0x20000FFF, 0x1FFFEC00, 0x1FFFF7FF));
    table.add(new Stm32BootloaderDeviceDependentParameters(Series.F4, "STM32F401xB(C)", (short) 0x423, (byte) 0xD1, 0x20003000, 0x2000FFFF, 0x1FFF0000, 0x1FFF77FF));
    table.add(new Stm32BootloaderDeviceDependentParameters(Series.F4, "STM32F401xD(E)", (short) 0x433, (byte) 0xD1, 0x20003000, 0x20017FFF, 0x1FFF0000, 0x1FFF77FF));
  }

  public static Stm32BootloaderDeviceDependentParameters findByPid(final short pid) {
    for (Stm32BootloaderDeviceDependentParameters r : table) {
      if (r.getPid() == pid) {
        return r;
      }
    }
    return null;
  }

}
