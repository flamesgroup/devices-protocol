/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.bootloader.stm32;

import com.flamesgroup.device.bootloader.ByteArrayHelper;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.IDev;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class Stm32BootloaderUtil {

  private static final byte[] stmGetIdCode = {
      (byte) 0x07, (byte) 0x4B,                           // ldr r3, [pc, #28]
      (byte) 0x08, (byte) 0x4A,                           // ldr r2, [pc, #32]
      (byte) 0x12, (byte) 0x68,                           // ldr r2, [r2]
      (byte) 0x1A, (byte) 0x60,                           // str r2, [r3]
      (byte) 0x07, (byte) 0x4B,                           // ldr r3, [pc, #28]
      (byte) 0x08, (byte) 0x4A,                           // ldr r2, [pc, #32]
      (byte) 0x12, (byte) 0x68,                           // ldr r2, [r2]
      (byte) 0x1A, (byte) 0x60,                           // str r2, [r3]
      (byte) 0x07, (byte) 0x4B,                           // ldr r3, [pc, #28]
      (byte) 0x08, (byte) 0x4A,                           // ldr r2, [pc, #32]
      (byte) 0x12, (byte) 0x68,                           // ldr r2, [r2]
      (byte) 0x1A, (byte) 0x60,                           // str r2, [r3]
      (byte) 0x07, (byte) 0x49,                           // ldr r1, [pc, #28] ; (<AIRCR_OFFSET>)
      (byte) 0x08, (byte) 0x4A,                           // ldr r2, [pc, #32] ; (<AIRCR_RESET_VALUE>)
      (byte) 0x0A, (byte) 0x60,                           // str r2, [r1, #0]
      (byte) 0xfe, (byte) 0xe7,                           // endless: b endless
      (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, // .word 0x20XXXXXX
      (byte) 0x10, (byte) 0x7A, (byte) 0xFF, (byte) 0x1F, // .word 0x1FFF7A10
      (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, // .word 0x20XXXXXX
      (byte) 0x14, (byte) 0x7A, (byte) 0xFF, (byte) 0x1F, // .word 0x1FFF7A14
      (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, // .word 0x20XXXXXX
      (byte) 0x18, (byte) 0x7A, (byte) 0xFF, (byte) 0x1F, // .word 0x1FFF7A18
      (byte) 0x0c, (byte) 0xed, (byte) 0x00, (byte) 0xe0, // .word 0xe000ed0c <AIRCR_OFFSET> = NVIC AIRCR register address
      (byte) 0x04, (byte) 0x00, (byte) 0xfa, (byte) 0x05  // .word 0x05fa0004 <AIRCR_RESET_VALUE> = VECTKEY | SYSRESETREQ
  };

  public static void readoutProtect(final IDev dev) throws Stm32BootloaderException, DevException, InterruptedException {
    Thread.sleep(5);

    IStm32Bootloader stm32Bootloader = new Stm32Bootloader(dev);
    stm32Bootloader.open();
    try {
      stm32Bootloader.readoutProtect();
    } finally {
      stm32Bootloader.close();
    }

    Thread.sleep(500);
  }

  public static void readoutUnprotect(final IDev dev) throws Stm32BootloaderException, DevException, InterruptedException {
    Thread.sleep(5);

    IStm32Bootloader stm32Bootloader = new Stm32Bootloader(dev);
    stm32Bootloader.open();
    try {
      stm32Bootloader.readoutUnprotect();
    } finally {
      stm32Bootloader.close();
    }

    Thread.sleep(500);
  }

  public static byte[] readCpuUid(final IDev dev) throws Stm32BootloaderException, DevException, InterruptedException {
    Thread.sleep(5);

    Stm32Bootloader stm32Bootloader = new Stm32Bootloader(dev);
    stm32Bootloader.open();
    try {
      int validRamMemoryBegin = stm32Bootloader.getDevice().getParameters().getValidRamMemoryBegin();

      byte[] stmGetIdCode = Stm32BootloaderUtil.stmGetIdCode.clone();
      ByteArrayHelper.putIntLittleEndian(stmGetIdCode, 32, validRamMemoryBegin);
      validRamMemoryBegin += 4;
      ByteArrayHelper.putIntLittleEndian(stmGetIdCode, 40, validRamMemoryBegin);
      validRamMemoryBegin += 4;
      ByteArrayHelper.putIntLittleEndian(stmGetIdCode, 48, validRamMemoryBegin);
      validRamMemoryBegin += 4;

      stm32Bootloader.runRawCode(validRamMemoryBegin, stmGetIdCode);
    } finally {
      stm32Bootloader.close();
    }

    Thread.sleep(5);

    byte[] data;
    stm32Bootloader = new Stm32Bootloader(dev);
    stm32Bootloader.open();
    try {
      data = new byte[12];
      stm32Bootloader.read(stm32Bootloader.getDevice().getParameters().getValidRamMemoryBegin(), data, 0, data.length);
    } finally {
      stm32Bootloader.close();
    }

    Thread.sleep(5);

    return data;
  }

  public static byte[] readAndAlignFirmware(final Path path) throws IOException {
    try (SeekableByteChannel sbc = Files.newByteChannel(path)) {
      long fwSizeAligned = (sbc.size() / 4 + 1) * 4;
      if (fwSizeAligned > Integer.MAX_VALUE) {
        throw new IllegalArgumentException("Firmware file size too large");
      }
      byte[] fwAlignedData = new byte[(int) fwSizeAligned];
      sbc.read(ByteBuffer.wrap(fwAlignedData));
      return fwAlignedData;
    }
  }

  public static void uploadFirmware(final IDev dev, final byte[] fwAlignedData, IUploadFirmwareHandler handler) throws Stm32BootloaderException, DevException, InterruptedException {
    Objects.requireNonNull(dev);
    Objects.requireNonNull(fwAlignedData);
    if (fwAlignedData.length % 4 != 0) {
      throw new IllegalArgumentException("Firmware data array must be align by 4");
    }
    if (handler == null) {
      handler = new EmptyUploadFirmwareHandler();
    }

    Thread.sleep(5);

    Stm32Bootloader stm32Bootloader = new Stm32Bootloader(dev);
    stm32Bootloader.open();
    try {
      handler.handleEraseBegin();
      stm32Bootloader.globalErase();
      handler.handleEraseEnd();

      handler.handleWriteBegin();
      int flashAddressToWrite = 0x08000000;
      int fwAlignedDataWritten = 0;
      int fwAlignedDataLeftToWrite = fwAlignedData.length;
      while (fwAlignedDataLeftToWrite > 0) {
        int fwAlignedDataChunkWriteLength = fwAlignedDataLeftToWrite > 256 ? 256 : fwAlignedDataLeftToWrite;
        stm32Bootloader.write(flashAddressToWrite, fwAlignedData, fwAlignedDataWritten, fwAlignedDataChunkWriteLength);
        flashAddressToWrite += fwAlignedDataChunkWriteLength;
        fwAlignedDataWritten += fwAlignedDataChunkWriteLength;
        fwAlignedDataLeftToWrite -= fwAlignedDataChunkWriteLength;

        handler.handleWriteProgress(fwAlignedDataWritten, fwAlignedData.length);
      }
      handler.handleWriteEnd();
    } finally {
      stm32Bootloader.close();
    }

    Thread.sleep(5);
  }

  public static void uploadFirmware(final IDev dev, final byte[] fwAlignedData) throws Stm32BootloaderException, DevException, InterruptedException {
    uploadFirmware(dev, fwAlignedData, null);
  }

  public static void uploadFirmware(final IDev dev, final Path path, final IUploadFirmwareHandler handler) throws Stm32BootloaderException, DevException, IOException, InterruptedException {
    uploadFirmware(dev, readAndAlignFirmware(path), handler);
  }

  public static void uploadFirmware(final IDev dev, final Path path) throws Stm32BootloaderException, DevException, IOException, InterruptedException {
    uploadFirmware(dev, path, null);
  }

  public interface IUploadFirmwareHandler {

    void handleEraseBegin();

    void handleEraseEnd();

    void handleWriteBegin();

    void handleWriteProgress(int written, int length);

    void handleWriteEnd();

  }

  private static class EmptyUploadFirmwareHandler implements IUploadFirmwareHandler {
    @Override
    public void handleEraseBegin() {
    }

    @Override
    public void handleEraseEnd() {
    }

    @Override
    public void handleWriteBegin() {
    }

    @Override
    public void handleWriteProgress(final int written, final int length) {
    }

    @Override
    public void handleWriteEnd() {
    }
  }

}
