/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.box;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.gpio.ISysfsGPIOIn;
import com.flamesgroup.jdev.gpio.SysfsGPIO;
import com.flamesgroup.jdev.gpio.SysfsGPIOAddress;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Properties;

public final class BOXUtil {

  private static final Path EEPROM_PATH = Paths.get("/sys/devices/platform/sunxi-i2c.2/i2c-2/2-0050/eeprom");

  private static final String deviceUIDPropertyName = "box.device.uid";

  private static final ISysfsGPIOIn boardRev1SysfsGPIOIn;
  private static final ISysfsGPIOIn boardRev2SysfsGPIOIn;
  private static final ISysfsGPIOIn boardRev3SysfsGPIOIn;
  private static final ISysfsGPIOIn boardRev4SysfsGPIOIn;

  static {
    boardRev1SysfsGPIOIn = new SysfsGPIO(new SysfsGPIOAddress(4, "BOARD_REV1"));
    boardRev2SysfsGPIOIn = new SysfsGPIO(new SysfsGPIOAddress(5, "BOARD_REV2"));
    boardRev3SysfsGPIOIn = new SysfsGPIO(new SysfsGPIOAddress(6, "BOARD_REV3"));
    boardRev4SysfsGPIOIn = new SysfsGPIO(new SysfsGPIOAddress(7, "BOARD_REV4"));
  }

  private BOXUtil() {
  }

  protected static DeviceType getDeviceType() throws DevException {
    int boardRev1 = boardRev1SysfsGPIOIn.getValue() ? 1 : 0;
    int boardRev2 = boardRev2SysfsGPIOIn.getValue() ? 1 : 0;
    int boardRev3 = boardRev3SysfsGPIOIn.getValue() ? 1 : 0;
    int boardRev4 = boardRev4SysfsGPIOIn.getValue() ? 1 : 0;
    int boardRev = (boardRev4 << 3) | (boardRev3 << 2) | (boardRev2 << 1) | (boardRev1 << 0);

    switch (boardRev) {
      case 0b0000: {
        return DeviceType.GSMBOX4;
      }
      case 0b0001: {
        return DeviceType.GSMBOX8;
      }
      case 0b1000: {
        return DeviceType.SIMBOX60;
      }
      case 0b1001: {
        return DeviceType.SIMBOX120;
      }
      default: {
        throw new IllegalStateException("Can't map board rev [0b" + boardRev4 + boardRev3 + boardRev2 + boardRev1 + "]");
      }
    }
  }

  public static IBOXDevAddresses getBOXDevAddresses() throws DevException {
    DeviceType deviceType = getDeviceType();
    switch (deviceType) {
      case GSMBOX4: {
        return new GSMBOX4DevAddresses();
      }
      case GSMBOX8: {
        return new GSMBOX8DevAddresses();
      }
      case SIMBOX60: {
        return new SIMBOX60DevAddresses();
      }
      case SIMBOX120: {
        return new SIMBOX120DevAddresses();
      }
      default: {
        throw new IllegalStateException("Can't map device type [" + deviceType + "]");
      }
    }
  }

  public static DeviceUID readDeviceUID() throws IOException, DevException {
    try (InputStream in = Files.newInputStream(EEPROM_PATH, StandardOpenOption.READ)) {
      Properties boxProperties = new Properties();
      boxProperties.load(in);
      String deviceUIDCanonicalName = boxProperties.getProperty(deviceUIDPropertyName);
      if (deviceUIDCanonicalName == null) {
        throw new IllegalStateException("DeviceUID is undefined");
      } else {
        DeviceUID deviceUID;
        try {
          deviceUID = DeviceUID.valueFromCanonicalName(deviceUIDCanonicalName);
        } catch (IllegalArgumentException e) {
          throw new IllegalStateException("Read DeviceUID [" + deviceUIDCanonicalName + "] is incorrect", e);
        }
        DeviceType actualDeviceType = getDeviceType();
        if (deviceUID.getDeviceType() != actualDeviceType) {
          throw new IllegalStateException("Read DeviceUID [" + deviceUIDCanonicalName + "] inconsistent to actual DeviceType [" + actualDeviceType + "]");
        }
        return deviceUID;
      }
    }
  }

  public static void writeDeviceUID(final DeviceUID deviceUID) throws IOException, DevException {
    Objects.requireNonNull(deviceUID, "deviceUID mustn't be null");
    DeviceType actualDeviceType = getDeviceType();
    if (deviceUID.getDeviceType() != actualDeviceType) {
      throw new IllegalStateException("Written DeviceUID [" + deviceUID.getCanonicalName() + "] inconsistent to actual DeviceType [" + actualDeviceType + "]");
    }
    try (OutputStream out = Files.newOutputStream(EEPROM_PATH, StandardOpenOption.WRITE)) {
      Properties boxProperties = new Properties();
      boxProperties.setProperty(deviceUIDPropertyName, deviceUID.getCanonicalName());
      boxProperties.store(out, "THIS PROPERTIES MUST NOT BE CHANGE MANUALLY");
    }
    // check if DeviceUID was actually written
    try (InputStream in = Files.newInputStream(EEPROM_PATH, StandardOpenOption.READ)) {
      Properties boxProperties = new Properties();
      boxProperties.load(in);
      String deviceUIDCanonicalName = boxProperties.getProperty(deviceUIDPropertyName);
      if (!deviceUID.getCanonicalName().equals(deviceUIDCanonicalName)) {
        throw new IllegalStateException("Written DeviceUID [" + deviceUID.getCanonicalName() + "] fail, most likely due to the hardware write-protect");
      }
    }
  }

}
