/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.box;

import com.flamesgroup.device.simb.SIMBOX60Device;
import com.flamesgroup.jdev.gpio.SysfsGPIOAddress;
import com.flamesgroup.jdev.i2c.I2CDevAddress;
import com.flamesgroup.jdev.spi.SPIDevAddress;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SIMBOX60DevAddresses implements IBOXDevAddresses {

  private static final List<SPIDevAddress> spiDevAddresses;
  private static final I2CDevAddress i2CDevAddress;
  private static final SysfsGPIOAddress boot0SysfsGPIOAddress;
  private static final SysfsGPIOAddress pbootSysfsGPIOAddress;
  private static final List<SysfsGPIOAddress> rstSysfsGPIOAddresses;

  static {
    {
      List<SPIDevAddress> spiDevAddressesLocal = new ArrayList<>(1);
      spiDevAddressesLocal.add(new SPIDevAddress(2, 0));
      spiDevAddresses = Collections.unmodifiableList(spiDevAddressesLocal);
    }
    {
      i2CDevAddress = new I2CDevAddress(1);
    }
    {
      boot0SysfsGPIOAddress = new SysfsGPIOAddress(2, "BOOT0");
    }
    {
      pbootSysfsGPIOAddress = new SysfsGPIOAddress(3, "PBOOT");
    }
    {
      List<SysfsGPIOAddress> arrayList = new ArrayList<>(SIMBOX60Device.CPU_ENTRY_COUNT);
      arrayList.add(new SysfsGPIOAddress(113, "MCU20_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(110, "MCU1_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(114, "MCU19_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(109, "MCU2_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(101, "MCU18_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(106, "MCU3_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(103, "MCU17_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(104, "MCU4_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(129, "MCU16_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(126, "MCU5_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(131, "MCU15_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(124, "MCU6_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(117, "MCU14_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(122, "MCU7_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(119, "MCU13_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(120, "MCU8_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(145, "MCU12_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(142, "MCU9_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(134, "MCU11_ODD_RST"));
      arrayList.add(new SysfsGPIOAddress(137, "MCU10_ODD_RST"));
      rstSysfsGPIOAddresses = Collections.unmodifiableList(arrayList);
    }
  }

  @Override
  public List<SPIDevAddress> getSPIDevAddresses() {
    return spiDevAddresses;
  }

  @Override
  public I2CDevAddress getI2CDevAddress() {
    return i2CDevAddress;
  }

  @Override
  public SysfsGPIOAddress getBOOT0SysfsGPIOAddress() {
    return boot0SysfsGPIOAddress;
  }

  @Override
  public SysfsGPIOAddress getPBOOTSysfsGPIOAddress() {
    return pbootSysfsGPIOAddress;
  }

  @Override
  public List<SysfsGPIOAddress> getRSTSysfsGPIOAddresses() {
    return rstSysfsGPIOAddresses;
  }

}
