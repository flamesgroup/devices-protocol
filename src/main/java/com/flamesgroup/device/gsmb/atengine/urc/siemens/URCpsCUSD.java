/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.siemens;

import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.Status;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCUSDHandler;

import java.util.Objects;

public final class URCpsCUSD extends com.flamesgroup.device.gsmb.atengine.urc.URCpsCUSD {

  public URCpsCUSD(final IURCpsCUSDHandler handler) {
    super(new URCpsCUSDHandlerWrapper(handler));
  }

  @Override
  public boolean process(final String atLexeme) {
    if ("> ".equals(atLexeme)) {
      ((URCpsCUSDHandlerWrapper) handler).handleUssdOnUserActionRequest();
      return true;
    }
    return super.process(atLexeme);
  }

  private static class URCpsCUSDHandlerWrapper implements IURCpsCUSDHandler {

    private final IURCpsCUSDHandler handler;

    private Status status;
    private String message;
    private int dataCodingScheme = -1;

    public URCpsCUSDHandlerWrapper(final IURCpsCUSDHandler handler) {
      Objects.requireNonNull(handler, "handler mustn't be null");
      this.handler = handler;
    }

    @Override
    public void handleUssd(final Status status, final String message, final int dataCodingScheme) {
      if (status == Status.FURTHER_USER_ACTION_REQUIRED) {
        this.status = status;
        this.message = message;
        this.dataCodingScheme = dataCodingScheme;
      } else {
        handler.handleUssd(status, message, dataCodingScheme);
      }
    }

    public void handleUssdOnUserActionRequest() {
      handler.handleUssd(status, message, dataCodingScheme);
      status = null;
      message = null;
      dataCodingScheme = -1;
    }

  }

}
