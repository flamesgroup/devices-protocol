/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCMUX.PortSpeed;

import java.util.Objects;

/**
 * Multiplexing Mode <br>
 * Set command is used to enable/disable the 3GPP TS 27.010 multiplexing protocol control channel.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=91">Paragraph 3.5.4.1.7.</a>
 */
public final class ATpsCMUXWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final PortSpeed portSpeed, final int maxFrameSize) {
    final int maxFrameSizeMin = 1; // point 5.7.2 of 3GPP TS 07.10
    final int maxFrameSizeMax = 32768; // point 5.7.2 of 3GPP TS 07.10
    Objects.requireNonNull(portSpeed, "portSpeed mustn't be null");
    if (maxFrameSize < maxFrameSizeMin || maxFrameSize > maxFrameSizeMax) {
      throw new IllegalArgumentException(String.format("Incorrect maxFrameSize value [%s] - must be in range from %d to %d", maxFrameSize, maxFrameSizeMin, maxFrameSizeMax));
    }
    return "AT+CMUX=0,0," + portSpeed.getValue() + ',' + maxFrameSize + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final PortSpeed portSpeed) {
    Objects.requireNonNull(portSpeed, "portSpeed mustn't be null");
    return "AT+CMUX=0,0," + portSpeed.getValue() + "\r";
  }

  public ATpsCMUXWrite(final PortSpeed portSpeed, final int maxFrameSize) {
    super(buildCommandATDataWithParamCheck(portSpeed, maxFrameSize));
  }

  public ATpsCMUXWrite(final PortSpeed portSpeed) {
    super(buildCommandATDataWithParamCheck(portSpeed));
  }

  public ATpsCMUXWrite() {
    super("AT+CMUX=0\r");
  }

}
