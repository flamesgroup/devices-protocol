/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLSECCFG;

import java.util.Objects;

/**
 * Configure security parameters of a SSL socket<br>
 * This command allows configuring SSL connection parameters.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=694">Paragraph 3.5.7.24.9.</a>
 */
public final class AThhSSLSECCFGExec extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final PhhSSLSECCFG.CipherSuite cipherSuite, final PhhSSLSECCFG.AuthMMode authMMode) {
    Objects.requireNonNull(cipherSuite, "cipherSuite mustn't be null");
    Objects.requireNonNull(authMMode, "authMMode mustn't be null");
    return "AT#SSLSECCFG=1," + cipherSuite.getValue() + "," + authMMode.getValue() + "\r";
  }

  public AThhSSLSECCFGExec(final PhhSSLSECCFG.CipherSuite cipherSuite, final PhhSSLSECCFG.AuthMMode authMMode) {
    super(buildCommandATDataWithParamCheck(cipherSuite, authMMode));
  }

}
