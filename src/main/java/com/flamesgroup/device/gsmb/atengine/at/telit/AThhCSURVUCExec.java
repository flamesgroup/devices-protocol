/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import java.util.List;
import java.util.Objects;

/**
 * Network Survey <br>
 * Execution command allows to perform a quick survey through the given channels.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=546">Paragraph 3.5.7.11.4.</a>
 */
public final class AThhCSURVUCExec extends AThhCSURVBaseExec {

  private static String buildCommandATDataWithParamCheck(final List<Integer> channels) {
    Objects.requireNonNull(channels, "channels mustn't be null");
    if (channels.isEmpty()) {
      throw new IllegalArgumentException("channels mustn't be empty");
    }
    if (channels.size() > 20) {
      throw new IllegalArgumentException("channels size must be less or equal 20");
    }
    StringBuilder sb = new StringBuilder();
    sb.append("AT#CSURVUC=");
    for (int i = 0; i < channels.size(); i++) {
      Integer channel = channels.get(i);
      if (i != 0) {
        sb.append(",");
      }
      sb.append(channel);
    }
    sb.append("\r");
    return sb.toString();
  }

  public AThhCSURVUCExec(final List<Integer> channels) {
    super(buildCommandATDataWithParamCheck(channels));
  }

}
