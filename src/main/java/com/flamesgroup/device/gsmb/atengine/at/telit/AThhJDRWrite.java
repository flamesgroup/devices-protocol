/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhJDR.Mode;

import java.util.Objects;

/**
 * Jammed Detect & Report <br>
 * Set command allows to control the Jammed Detect & Report feature.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=570">Paragraph 3.5.7.13.1.</a>
 */
public final class AThhJDRWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final Mode mode) {
    Objects.requireNonNull(mode, "mode mustn't be null");
    return "AT#JDR=" + mode.getValue() + '\r';
  }

  private static String buildCommandATDataWithParamCheck(final Mode mode, final int mnpl, final int dcmn) {
    Objects.requireNonNull(mode, "mode mustn't be null");
    if (mnpl < 0 || mnpl > 127) {
      throw new IllegalArgumentException("Incorrect MNLP value: " + mnpl + ", must be within 0..127 range (factory default is 70)");
    }
    if (dcmn < 0 || dcmn > 254) {
      throw new IllegalArgumentException("Incorrect DCMN value: " + mnpl + ", must be within 0..254 range (factory default is 5)");
    }

    return "AT#JDR=" + mode.getValue() + ',' + mnpl + ',' + dcmn + '\r';
  }

  /**
   * @param mode behaviour mode of the Jammed Detect & Report
   */
  public AThhJDRWrite(final Mode mode) {
    super(buildCommandATDataWithParamCheck(mode));
  }

  /**
   * @param mode behaviour mode of the Jammed Detect & Report
   * @param mnpl Maximum Noise Power Level: 0..127 (factory default is 70)
   * @param dcmn Disturbed Channel Minimum Number: 0..254 (factory default is 5)
   */
  public AThhJDRWrite(final Mode mode, final int mnpl, final int dcmn) {
    super(buildCommandATDataWithParamCheck(mode, mnpl, dcmn));
  }

}
