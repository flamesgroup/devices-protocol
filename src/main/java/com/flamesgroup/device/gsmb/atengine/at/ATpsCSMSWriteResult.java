/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.BroadcastTypeMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileOriginatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileTerminatedMessages;

import java.util.Objects;

public final class ATpsCSMSWriteResult implements IATResult {

  private final MobileTerminatedMessages mobileTerminatedMessages;
  private final MobileOriginatedMessages mobileOriginatedMessages;
  private final BroadcastTypeMessages broadcastTypeMessages;

  public ATpsCSMSWriteResult(final MobileTerminatedMessages mobileTerminatedMessages,
      final MobileOriginatedMessages mobileOriginatedMessages, final BroadcastTypeMessages broadcastTypeMessages) {
    this.mobileTerminatedMessages = mobileTerminatedMessages;
    this.mobileOriginatedMessages = mobileOriginatedMessages;
    this.broadcastTypeMessages = broadcastTypeMessages;
  }

  public MobileTerminatedMessages getMobileTerminatedMessages() {
    return mobileTerminatedMessages;
  }

  public MobileOriginatedMessages getMobileOriginatedMessages() {
    return mobileOriginatedMessages;
  }

  public BroadcastTypeMessages getBroadcastTypeMessages() {
    return broadcastTypeMessages;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCSMSWriteResult)) {
      return false;
    }
    ATpsCSMSWriteResult that = (ATpsCSMSWriteResult) object;

    return Objects.equals(mobileTerminatedMessages, that.getMobileTerminatedMessages())
        && Objects.equals(mobileOriginatedMessages, that.getMobileOriginatedMessages())
        && Objects.equals(broadcastTypeMessages, that.getBroadcastTypeMessages());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(mobileTerminatedMessages);
    result = prime * result + Objects.hashCode(mobileOriginatedMessages);
    result = prime * result + Objects.hashCode(broadcastTypeMessages);
    return result;
  }

}
