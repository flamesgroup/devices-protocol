/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCLCC.CallState;
import com.flamesgroup.device.gsmb.atengine.param.PpsCLCC.Mode;
import com.flamesgroup.device.gsmb.atengine.param.PpsCLCC.TypeOfAddressOctet;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class ATpsCLCCExecResult implements IATResult {

  private final List<ATpsCLCCExecResult.CallInfo> callList;

  public ATpsCLCCExecResult(final List<ATpsCLCCExecResult.CallInfo> callList) {
    this.callList = Collections.unmodifiableList(callList);
  }

  public List<ATpsCLCCExecResult.CallInfo> getCallList() {
    return callList;
  }

  public static class CallInfo {

    private final int idx;
    private final boolean originated;
    private final CallState state;
    private final Mode mode;
    private final boolean conference;
    private final String number;
    private final TypeOfAddressOctet typeOfAddressOctet;

    public CallInfo(final int idx, final boolean originated, final CallState state, final Mode mode, final boolean conference,
        final String number, final TypeOfAddressOctet typeOfAddressOctet) {
      this.idx = idx;
      this.originated = originated;
      this.state = state;
      this.mode = mode;
      this.conference = conference;
      this.number = number;
      this.typeOfAddressOctet = typeOfAddressOctet;
    }

    public int getIdx() {
      return idx;
    }

    public boolean isOriginated() {
      return originated;
    }

    public CallState getState() {
      return state;
    }

    public Mode getMode() {
      return mode;
    }

    public boolean isConference() {
      return conference;
    }

    public String getNumber() {
      return number;
    }

    public TypeOfAddressOctet getTypeOfAddressOctet() {
      return typeOfAddressOctet;
    }

    @Override
    public boolean equals(final Object object) {
      if (object == this) {
        return true;
      }
      if (!(object instanceof CallInfo)) {
        return false;
      }
      CallInfo that = (CallInfo) object;

      return idx == that.getIdx()
          && originated == that.isOriginated()
          && Objects.equals(state, that.getState())
          && Objects.equals(mode, that.getMode())
          && conference == that.isConference()
          && Objects.equals(number, that.getNumber())
          && Objects.equals(typeOfAddressOctet, that.getTypeOfAddressOctet());
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 3;
      result = prime * result + idx;
      result = prime * result + Boolean.hashCode(originated);
      result = prime * result + Objects.hashCode(state);
      result = prime * result + Objects.hashCode(mode);
      result = prime * result + Boolean.hashCode(conference);
      result = prime * result + Objects.hashCode(number);
      result = prime * result + Objects.hashCode(typeOfAddressOctet);
      return result;
    }

    @Override
    public String toString() {
      return String.format("%s@%x:[%d/%s/%s/%s/%s/%s/%s]", getClass().getSimpleName(), hashCode(),
          idx, originated, state, mode, conference, number, typeOfAddressOctet);
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCLCCExecResult)) {
      return false;
    }
    ATpsCLCCExecResult that = (ATpsCLCCExecResult) object;

    return Objects.equals(callList, that.getCallList());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(callList);
    return result;
  }

}
