/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;
import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.ATEmptyResult;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLSECDATA;

import java.util.Objects;

/**
 * Write the security data<br>
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=695">Paragraph 3.5.7.24.10.</a>
 */
public final class AThhSSLSECDATAActionStoreWrite implements IAT<ATEmptyResult> {

  private final AThhSSLSECDATAActionStoreWrite.AThhSSLSECDATAWrite1 part1;
  private final AThhSSLSECDATAActionStoreWrite.AThhSSLSECDATAWrite2 part2;

  public AThhSSLSECDATAActionStoreWrite(final PhhSSLSECDATA.DataType dataType, final int size, final String data) {
    if (data.getBytes().length != size) {
      throw new IllegalArgumentException(String.format("Incorrect size data [%d] - must be equal to the size [%d]", data.getBytes().length, size));
    }
    part1 = new AThhSSLSECDATAWrite1(dataType, size);
    part2 = new AThhSSLSECDATAWrite2(data);
  }

  @Override
  public ATEmptyResult execute(final IATSubChannel atSubChannel) throws ChannelException, InterruptedException {
    part1.execute(atSubChannel);
    return part2.execute(atSubChannel);
  }

  @Override
  public boolean isWaitBeforeNextAT() {
    return true;
  }

  @Override
  public boolean process(final String atLexeme) {
    return part1.process(atLexeme) || part2.process(atLexeme);
  }

  private static final class AThhSSLSECDATAWrite1 extends EmptyResponseSolidAT {

    private static String buildCommandWithParamCheck(final PhhSSLSECDATA.DataType dataType, final int size) {
      Objects.requireNonNull(dataType, "dataType mustn't be null");
      if (!ATLexemeProcessHelper.isValueInRange(size, 1, 2047)) {
        throw new IllegalArgumentException(String.format("Incorrect size value [%d] - must be in range of 1-2047", size));
      }
      return "AT#SSLSECDATA=1,1," + dataType.getValue() + "," + size + "\r";
    }

    private AThhSSLSECDATAWrite1(final PhhSSLSECDATA.DataType dataType, final int size) {
      super(buildCommandWithParamCheck(dataType, size), 10000);
    }

    @Override
    protected boolean internalProcessATOKStatus(final String lexeme) {
      return "> ".equals(lexeme) || super.internalProcessATOKStatus(lexeme);
    }

  }

  private static final class AThhSSLSECDATAWrite2 extends EmptyResponseSolidAT {

    private static String buildCommandATDataWithParamCheck(final String data) {
      Objects.requireNonNull(data, "data mustn't be null");
      return data + (char) 0x1A;
    }

    private AThhSSLSECDATAWrite2(final String data) {
      super(buildCommandATDataWithParamCheck(data), 10000);
    }

    @Override
    public boolean isWaitBeforeNextAT() {
      return false;
    }

  }

}
