/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

/**
 * Indicator Control <br>
 * Set command is used to control the registration state of ME indicators, in order to automatically send the +CIEV URC, whenever the value of the associated indicator changes.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=144">Paragraph 3.5.4.4.5.</a>
 */
public final class ATpsCINDWrite extends EmptyResponseSolidAT {

  public ATpsCINDWrite(final boolean registerSignalIndicator, final boolean registerRssiIndicator) {
    super("AT+CIND=0," + (registerSignalIndicator ? 1 : 0) + ",0,0,0,0,0,0," + (registerRssiIndicator ? 1 : 0) + "\r");
  }

}
