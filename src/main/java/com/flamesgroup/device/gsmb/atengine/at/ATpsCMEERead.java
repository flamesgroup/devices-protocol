/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCMEE.ErrorMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Report Mobile Equipment Error <br>
 * Read command returns the current value of subparameter
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=180">Paragraph 3.5.4.5.1.</a>
 */
public final class ATpsCMEERead extends SolidAT<ATpsCMEEReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CMEE: (?<mode>[0-2])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private ErrorMode errorMode;

  public ATpsCMEERead() {
    super("AT+CMEE?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      errorMode = ErrorMode.getErrorModeByValue(Integer.parseInt(matcher.group("mode")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCMEEReadResult getCommandATResult() {
    return new ATpsCMEEReadResult(errorMode);
  }

}
