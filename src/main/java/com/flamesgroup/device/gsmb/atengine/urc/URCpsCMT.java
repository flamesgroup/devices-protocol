/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URCpsCMT implements IURC {

  // actually first part template slightly different for Telit & Siemens
  // Telit provide some <alpha> as first parameter (which actually  isn't used)
  // Siemens provide nothing except comma before second parameter
  // to simplify implementation more general template is used
  private static final String URC_FIRST_PART_TEMPLATE = "\\+CMT: .*,(?<length>\\d+)";
  private static final String URC_SECOND_PART_TEMPLATE = "(?<pdu>[a-fA-F_0-9]+)";

  private static final Pattern urcFirstPartPattern = Pattern.compile(URC_FIRST_PART_TEMPLATE);
  private static final Pattern urcSecondPartPattern = Pattern.compile(URC_SECOND_PART_TEMPLATE);

  private final IURCpsCMTHandler handler;

  private boolean processFirstPart = true;
  private int pduLength = -1;

  public URCpsCMT(final IURCpsCMTHandler handler) {
    Objects.requireNonNull(handler, "handler mustn't be null");
    this.handler = handler;
  }

  @Override
  public boolean process(final String atLexeme) {
    if (processFirstPart) {
      Matcher matcher = urcFirstPartPattern.matcher(atLexeme);
      if (matcher.matches()) {
        processFirstPart = false;
        pduLength = Integer.parseInt(matcher.group("length"));
        return true;
      }
    } else {
      Matcher matcher = urcSecondPartPattern.matcher(atLexeme);
      if (matcher.matches()) {
        String pdu = matcher.group("pdu");
        handler.handleSMS(pduLength, pdu);
        processFirstPart = true;
        pduLength = -1;
        return true;
      }
    }
    return false;
  }

}
