/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCMER.Mode;

/**
 * Mobile Equipment Event Reporting <br>
 * Set command enables/disables sending of unsolicited result codes from TA to TE in the case of indicator state changes
 * (n.b.: sending of URCs in the case of key pressings or display changes are currently not implemented).
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=146">Paragraph 3.5.4.4.6.</a>
 */
public final class ATpsCMERWrite extends EmptyResponseSolidAT {

  private static String buildCommandATData(final Mode mode, final Boolean enableIndicatorEventReporting, final Boolean clearTABuffer) {
    StringBuilder commandATDataBuilder = new StringBuilder("AT+CMER=");
    if (mode != null) {
      commandATDataBuilder.append(mode.getValue());
      if (enableIndicatorEventReporting != null) {
        commandATDataBuilder.append(",0,0,").append(enableIndicatorEventReporting ? 2 : 0);
        if (clearTABuffer != null && clearTABuffer) {
          commandATDataBuilder.append(",0");
        }
      }
    }
    return commandATDataBuilder.append("\r").toString();
  }

  public ATpsCMERWrite(final Mode mode, final Boolean enableIndicatorEventReporting, final Boolean clearTABuffer) {
    super(buildCommandATData(mode, enableIndicatorEventReporting, clearTABuffer));
  }

}
