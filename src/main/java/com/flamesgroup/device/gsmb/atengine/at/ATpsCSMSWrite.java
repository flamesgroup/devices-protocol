/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.BroadcastTypeMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileOriginatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileTerminatedMessages;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Select Message Service <br>
 * Set command selects messaging service
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=202">Paragraph 3.5.5.1.1.</a>
 */
public abstract class ATpsCSMSWrite extends SolidAT<ATpsCSMSWriteResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CSMS: (?<mt>[01]),(?<mo>[01]),(?<bm>[01])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private int mt;
  private int mo;
  private int bm;

  public ATpsCSMSWrite(final String commandATData) {
    super(commandATData);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      mt = Integer.parseInt(matcher.group("mt"));
      mo = Integer.parseInt(matcher.group("mo"));
      bm = Integer.parseInt(matcher.group("bm"));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCSMSWriteResult getCommandATResult() {
    return new ATpsCSMSWriteResult(MobileTerminatedMessages.getMobileTerminatedMessagesByValue(mt),
        MobileOriginatedMessages.getMobileOriginatedMessagesByValue(mo),
        BroadcastTypeMessages.getBroadcastTypeMessagesByValue(bm));
  }

}
