/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd.siemens;

import static com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.Status.NETWORK_TIME_OUT;
import static com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.Status.OPERATION_NOT_SUPPORTED;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATDExec;
import com.flamesgroup.device.gsmb.atengine.at.siemens.USSDResponse;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.IURCpsCMEERRORPhoneBusyHandler;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.IURCpsCMEERRORSSNotExecutedHandler;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.IURCpsCMEERRORServiceOptionNotSupportedHandler;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.IURCpsCMEERRORUnknownHandler;
import com.flamesgroup.device.gsmb.atengine.ussd.BaseUSSDSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class USSDSession extends BaseUSSDSession implements IURCpsCMEERRORUnknownHandler, IURCpsCMEERRORSSNotExecutedHandler,
    IURCpsCMEERRORServiceOptionNotSupportedHandler, IURCpsCMEERRORPhoneBusyHandler {

  private static final Logger logger = LoggerFactory.getLogger(USSDSession.class);

  private int sessionTimeoutDetector;

  public USSDSession(final IATEngine atEngine) {
    super(atEngine);
  }

  @Override
  protected void executeOpenAction(final String ussdString) throws ChannelException, InterruptedException {
    atEngine.execute(new ATDExec(ussdString));
  }

  @Override
  protected void executeResponseAction(final String response) throws ChannelException, InterruptedException {
    atEngine.execute(new USSDResponse(response, false));
  }

  @Override
  protected void executeCloseAction() throws ChannelException, InterruptedException {
    atEngine.execute(new USSDResponse("", true));
  }

  @Override
  public void handleUnknownError() {
    sessionTimeoutDetector++;
  }

  @Override
  public void handleSSNotExecutedError() {
    sessionTimeoutDetector++;
    if (sessionTimeoutDetector == 3) {
      sessionTimeoutDetector = 0;

      logger.debug(String.format("[%s] - [%s]", this, NETWORK_TIME_OUT.getDescription()));
      setUssdRequestAndNotifyWaiter(NETWORK_TIME_OUT.getDescription(), true);
    }
  }

  @Override
  public void handleServiceOptionNotSupportedError() {
    logger.debug(String.format("[%s] - [%s]", this, OPERATION_NOT_SUPPORTED.getDescription()));
    setUssdRequestAndNotifyWaiter(OPERATION_NOT_SUPPORTED.getDescription(), true);
  }

  @Override
  public void handlePhoneBusyError() {
    logger.debug(String.format("[%s] - [phone busy]", this));
    setUssdRequestAndNotifyWaiter("phone busy", true);
  }

}
