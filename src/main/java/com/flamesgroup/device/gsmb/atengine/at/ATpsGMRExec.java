/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Revision Identification <br>
 * Execution command returns the software revision identification.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=58">Paragraph 3.5.3.1.11.</a>
 */
public final class ATpsGMRExec extends SolidAT<ATpsGMRExecResult> {

  private static final String REVISION_TEMPLATE = "(?:REVISION )?(?<revision>\\d+\\.\\d+(?:\\.\\d+(?:-B\\d+)?)?)";
  private static final Pattern revisionPattern = Pattern.compile(REVISION_TEMPLATE);

  private String revision;

  public ATpsGMRExec() {
    super("AT+GMR\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = revisionPattern.matcher(lexeme);
    if (matcher.matches()) {
      revision = matcher.group("revision");
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsGMRExecResult getCommandATResult() {
    return new ATpsGMRExecResult(revision);
  }

}
