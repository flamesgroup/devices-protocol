/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

/**
 * PLMN Network Survey (Numeric Format) <br>
 * Execution command performs a quick network survey through channels. The survey stops as soon as a BCCH carriers belonging to the selected PLMN is found.
 * The result is given in numeric format and is like command #CSURVC.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=552">Paragraph 3.5.7.11.11.</a>
 */
public final class AThhCSURVPCExec extends AThhCSURVBaseExec {

  private static String buildCommandATDataWithParamCheck(final int plmn) {
    if (plmn <= 0) {
      throw new IllegalArgumentException("plmn must be more then 0");
    }
    return "AT#CSURVPC=" + Integer.toHexString(plmn) + "\r";
  }

  public AThhCSURVPCExec(final int plmn) {
    super(buildCommandATDataWithParamCheck(plmn));
  }

}
