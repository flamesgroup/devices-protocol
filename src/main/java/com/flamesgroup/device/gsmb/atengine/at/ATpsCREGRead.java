/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCREG.Status;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG.URCPresentMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Network Registration Report <br>
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=102">Paragraph 3.5.4.3.3.</a>
 */
public final class ATpsCREGRead extends SolidAT<ATpsCREGReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CREG: (?<n>[0-2]),(?<stat>[0-5])(,\"(?<lac>.+)\",\"(?<ci>.+)\")?";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private URCPresentMode urcPresentationMode;
  private Status stat;
  private String locationAreaCode;
  private String cellID;

  public ATpsCREGRead() {
    super("AT+CREG?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      urcPresentationMode = URCPresentMode.getModeByValue(Integer.parseInt(matcher.group("n")));
      stat = Status.getStatusByValue(Integer.parseInt(matcher.group("stat")));
      locationAreaCode = matcher.group("lac");
      cellID = matcher.group("ci");
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCREGReadResult getCommandATResult() {
    return new ATpsCREGReadResult(urcPresentationMode, stat, locationAreaCode, cellID);
  }

}
