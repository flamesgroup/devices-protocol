/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURBaseExecResult.BcchCarrierInfo;
import static com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURBaseExecResult.CellStatus;
import static com.flamesgroup.device.gsmb.atengine.at.telit.AThhCSURBaseExecResult.NonBcchCarrierInfo;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Network Survey data processor based on default format (AT#CSURVF = 0, AT#CSURVEXT = 0) with enabled <CR><LF> removing (AT#CSURVNLF = 1).
 * Also Network Survey data processor support changing Extended Network Survey mode (for now only AT#CSURVEXT = 1).
 */
public abstract class AThhCSURVBaseExec extends SolidAT<AThhCSURBaseExecResult> {

  private static final String AT_RESPONSE_START_TEMPLATE = "Network survey started \\.\\.\\.";
  private static final String AT_RESPONSE_BCCH_TEMPLATE = "(?<arfcn>\\d+),(?<bsic>\\d+),(?<rxLev>-?\\d+),(?<ber>[._0-9]+),(?<mcc>\\p{XDigit}{3}),(?<mnc>\\p{XDigit}{2,3}),(?<lac>\\d+)," +
      "(?<cellId>\\d+),(?<cellStatus>[0-5]),(?:(?:0)|((?<numArfcn>\\d+),(?<arfcnn>(\\d+\\s?)+)))," +
      "?(?:(?<numChannels>\\d+))?,?(?:(?<array>(\\d+\\s?)+))?";
  private static final String AT_RESPONSE_NON_BCCH_TEMPLATE = "(?<arfcn>\\d+),(?<rxLev>-?\\d+)";
  private static final String AT_RESPONSE_END_TEMPLATE = "Network survey ended";

  private static final Pattern atResponseStartPattern = Pattern.compile(AT_RESPONSE_START_TEMPLATE);
  private static final Pattern atBcchResponsePattern = Pattern.compile(AT_RESPONSE_BCCH_TEMPLATE);
  private static final Pattern atNonBcchResponsePattern = Pattern.compile(AT_RESPONSE_NON_BCCH_TEMPLATE);
  private static final Pattern atResponseEndPattern = Pattern.compile(AT_RESPONSE_END_TEMPLATE);

  private final List<BcchCarrierInfo> bcchCarrierInfos = new ArrayList<>();
  private final List<NonBcchCarrierInfo> nonBcchCarrierInfos = new ArrayList<>();
  private boolean networkSurveyStarted;

  public AThhCSURVBaseExec(final String commandATData) {
    // From reference guide estimated maximum time to get response: 10 to start data output; 120 seconds to complete.
    // Real maximum time to get response is greater then estimated value from reference guide, so use extra 200 seconds for now.
    super(commandATData, TimeUnit.SECONDS.toMillis(10 + 120 + 200));
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    if (!networkSurveyStarted) {
      Matcher matcher = atResponseStartPattern.matcher(lexeme);
      if (matcher.matches()) {
        networkSurveyStarted = true;
        return true;
      }
    } else {
      Matcher matcher = atResponseEndPattern.matcher(lexeme);
      if (matcher.matches()) {
        networkSurveyStarted = false;
        return true;
      }
      matcher = atNonBcchResponsePattern.matcher(lexeme);
      if (matcher.matches()) {
        int arfcn = Integer.parseInt(matcher.group("arfcn"));
        int rxLev = Integer.parseInt(matcher.group("rxLev"));
        nonBcchCarrierInfos.add(new NonBcchCarrierInfo(arfcn, rxLev));
        return true;
      }
      matcher = atBcchResponsePattern.matcher(lexeme);
      if (matcher.matches()) {
        int arfcn = Integer.parseInt(matcher.group("arfcn"));
        int bsic = Integer.parseInt(matcher.group("bsic"));
        int rxLev = Integer.parseInt(matcher.group("rxLev"));
        float ber = Float.parseFloat(matcher.group("ber"));
        int mcc = mcc2int(matcher.group("mcc"));
        int mnc = mnc2int(matcher.group("mnc"));
        int lac = lac2int(matcher.group("lac"));
        int cellId = Integer.parseInt(matcher.group("cellId"));
        CellStatus cellStatus = CellStatus.getStatusByValue(Integer.parseInt(matcher.group("cellStatus")));
        String numArfcn = matcher.group("numArfcn");
        List<Integer> arfcnns = null;
        if (numArfcn != null && Integer.parseInt(numArfcn) > 0) {
          String arfcnn = matcher.group("arfcnn");
          arfcnns = Arrays.stream(arfcnn.trim().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        }
        String numChannels = matcher.group("numChannels");
        List<Integer> channels = null;
        if (numChannels != null && Integer.parseInt(numChannels) > 0) {
          String array = matcher.group("array");
          channels = Arrays.stream(array.trim().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        }
        bcchCarrierInfos.add(new BcchCarrierInfo(arfcn, bsic, rxLev, ber, mcc, mnc, lac, cellId, cellStatus, arfcnns, channels));
        return true;
      }
    }
    return false;
  }

  @Override
  protected AThhCSURBaseExecResult getCommandATResult() {
    return new AThhCSURBaseExecResult(bcchCarrierInfos, nonBcchCarrierInfos);
  }

  private int mcc2int(final String mccStr) {
    if (mccStr.equals("FFF")) {
      return 0;
    } else {
      return Integer.parseInt(mccStr, 16);
    }
  }

  private int mnc2int(final String mncStr) {
    if (mncStr.equals("FF")) {
      return 0;
    } else {
      return Integer.parseInt(mncStr, 16);
    }
  }

  private int lac2int(final String lacStr) {
    if (lacStr.equals("65535")) {
      return 0;
    } else {
      return Integer.parseInt(lacStr);
    }
  }

}
