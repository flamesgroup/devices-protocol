/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PadY.UserProfile;

import java.util.Objects;

/**
 * Default Reset Basic Profile Designation <br>
 * Execution command defines the basic profiles which will be loaded on startup. If parameter is omitted, the command has the same behaviour as AT&Y0
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=56">Paragraph 3.5.3.1.4.</a>
 */
public final class ATadYExec extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final UserProfile userProfile) {
    Objects.requireNonNull(userProfile, "userProfile mustn't be null");
    return "AT&Y" + userProfile.getValue() + "\r";
  }

  public ATadYExec(final UserProfile userProfile) {
    super(buildCommandATDataWithParamCheck(userProfile));
  }

}
