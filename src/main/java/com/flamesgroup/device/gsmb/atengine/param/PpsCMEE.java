/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCMEE {

  private PpsCMEE() {
  }

  public enum ErrorMode {

    DISABLE_RESULT_CODE(0),
    ENABLE_WITH_NUMERIC_VALUES(1),
    ENABLE_WITH_STRING_VALUES(2);

    private final int value;

    ErrorMode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static ErrorMode getErrorModeByValue(final int value) {
      for (ErrorMode errorMode : ErrorMode.values()) {
        if (errorMode.getValue() == value) {
          return errorMode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
