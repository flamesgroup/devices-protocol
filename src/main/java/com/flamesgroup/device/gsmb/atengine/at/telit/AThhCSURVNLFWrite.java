/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

/**
 * &lt;CR&gt;&lt;LF&gt; Removing On Easy Scan® Commands Family <br>
 * Set command enables/disables the automatic &lt;CR&gt;&lt;LF&gt; removing from each
 * information text line.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=549">Paragraph 3.5.7.11.8.</a>
 */
public final class AThhCSURVNLFWrite extends EmptyResponseSolidAT {

  /**
   * @param enable {@code true} to remove &lt;CR&gt;&lt;LF&gt; from information text,
   *               otherwise -  disables &lt;CR&gt;&lt;LF&gt; removing; they’ll be present in the information text on Easy Scan® Commands Family
   */
  public AThhCSURVNLFWrite(final boolean enable) {
    super("AT#CSURVNLF=" + (enable ? 1 : 0) + "\r");
  }

}
