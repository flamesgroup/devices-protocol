/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Pattern;

public abstract class ATshCallForwarding<TATResult extends IATResult> extends SolidAT<TATResult> {

  private static final String COMMAND_TEMPLATE = ""
      + "(?:[*#]{1,2})"
      + "(?:(?:(?:21|6[27])(?:\\*(\\+?[0-9]+)?(?:\\*(?:[0-9]{2})?)?)?)"
      + "|"
      + "(?:(?:61|00[24])(?:\\*(\\+?[0-9]+)?(?:\\*(?:[0-9]{2})?(?:\\*[0-9]*)?)?)?))#";

  private static final Pattern commandPattern = Pattern.compile(COMMAND_TEMPLATE);

  protected ATshCallForwarding(final String commandATData) {
    super(commandATData, 5000);
  }

  public static boolean isCallForwardingCode(final String code) {
    return commandPattern.matcher(code).matches();
  }

}
