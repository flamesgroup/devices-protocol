/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCSSI {

  private PpsCSSI() {
  }

  public enum Code1 {

    UNCONDITIONAL_CALL_FORWARDING_IS_ACTIVE(0),
    SOME_OF_THE_CONDITIONAL_CALL_FORWARDINGS_ARE_ACTIVE(1),
    CALL_HAS_BEEN_FORWARDED(2),
    CALL_IS_WAITING(3),
    OUTGOING_CALLS_ARE_BARRED(5),
    INCOMING_CALLS_ARE_BARRED(6);

    private final int value;

    Code1(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Code1 getCode1ByValue(final int value) {
      for (Code1 code1 : Code1.values()) {
        if (code1.getValue() == value) {
          return code1;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
