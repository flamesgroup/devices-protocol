/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhCPUMODE {

  private PhhCPUMODE() {
  }

  public enum Mode {

    NORMAL_CPU_CLOCK_26_MHZ(0),
    CPU_CLOCK_52_MHZ(1),
    CPU_CLOCK_52_MHZ_DURING_GPRS_TX_RX_ONLY(2),
    CPU_CLOCK_104_MHZ(3),
    CPU_CLOCK_104_MHZ_DURING_GPRS_TX_RX_ONLY(4),
    CPU_CLOCK_52_MHZ_DURING_GPRS_TX_RX_AND_VOICE_CALL(5),
    CPU_CLOCK_104_MHZ_DURING_GPRS_TX_RX_AND_VOICE_CALL(6),
    CPU_CLOCK_MAX_SUPPORTED_DURING_RSA_AT_COMMAND(7);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
