/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

/**
 * Loudspeaker Volume Level <br>
 * Set command is used to select the volume of the internal loudspeaker audio output of the device.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=167">Paragraph 3.5.4.4.21.</a>
 */
public final class ATpsCLVLWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final int volumeLevel) {
    if (volumeLevel < 0 || volumeLevel > 14) {
      throw new IllegalArgumentException(String.format("Incorrect level value [%d] - must be in range of 0-14", volumeLevel));
    }
    return "AT+CLVL=" + volumeLevel + "\r";
  }

  public ATpsCLVLWrite(final int volumeLevel) {
    super(buildCommandATDataWithParamCheck(volumeLevel));
  }

}
