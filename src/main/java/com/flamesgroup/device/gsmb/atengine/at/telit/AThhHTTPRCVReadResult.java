/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Objects;

public final class AThhHTTPRCVReadResult implements IATResult {

  private final String htmlSource;

  public AThhHTTPRCVReadResult(final String htmlSource) {
    this.htmlSource = htmlSource;
  }

  public String getHtmlSource() {
    return htmlSource;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof AThhHTTPRCVReadResult))
      return false;
    AThhHTTPRCVReadResult that = (AThhHTTPRCVReadResult) o;
    return Objects.equals(htmlSource, that.htmlSource);
  }

  @Override
  public int hashCode() {
    return Objects.hash(htmlSource);
  }

}
