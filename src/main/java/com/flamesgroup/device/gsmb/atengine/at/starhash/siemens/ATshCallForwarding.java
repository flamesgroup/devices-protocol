/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.siemens;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import com.flamesgroup.device.gsmb.atengine.at.starhash.siemens.ATshCallForwardingResult.CallForwardingInfo;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCallForwarding.Reason;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCallForwarding.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ATshCallForwarding extends com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCallForwarding<ATshCallForwardingResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\^SCCFC: (?<reason>[0-5]+),(?<status>[01]),(?<class>[0-9]{1,3})(,\"(?<number>.+)\",(?<type>129|145)(?<time>[0-9]+))?";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private final List<CallForwardingInfo> callForwardingInfoList = new ArrayList<>();

  public ATshCallForwarding(final String code) {
    super("ATD" + code + ";\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      Reason reason = Reason.getReasonByValue(Integer.parseInt(matcher.group("reason")));
      boolean status = ATLexemeProcessHelper.intStringToBoolean(matcher.group("status"));
      Set<ServiceClass> serviceClasses = ServiceClass.getServiceClassesByValue(Integer.parseInt(matcher.group("class")));
      String number = matcher.group("number");
      if (number == null) {
        callForwardingInfoList.add(new CallForwardingInfo(reason, status, serviceClasses));
      } else {
        Type type = Type.getTypeByValue(Integer.parseInt(matcher.group("type")));
        long time = Long.parseLong(matcher.group("time"));
        callForwardingInfoList.add(new CallForwardingInfo(reason, status, serviceClasses, number, type, time));
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATshCallForwardingResult getCommandATResult() {
    return new ATshCallForwardingResult(callForwardingInfoList);
  }

}
