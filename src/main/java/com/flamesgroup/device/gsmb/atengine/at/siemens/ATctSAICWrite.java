/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSAIC.EarpieceAmplifier;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSAIC.IO;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSAIC.Microphone;

import java.util.Objects;

public final class ATctSAICWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final IO io) {
    Objects.requireNonNull(io, "io mustn't be null");
    return "AT^SAIC=" + io.getValue() + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final IO io, final Microphone microphone) {
    Objects.requireNonNull(io, "io mustn't be null");
    Objects.requireNonNull(microphone, "microphone mustn't be null");
    return "AT^SAIC=" + io.getValue() + ',' + microphone.getValue() + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final IO io, final Microphone microphone, final EarpieceAmplifier amplifier) {
    Objects.requireNonNull(io, "io mustn't be null");
    Objects.requireNonNull(microphone, "microphone mustn't be null");
    Objects.requireNonNull(amplifier, "amplifier mustn't be null");
    return "AT^SAIC=" + io.getValue() + ',' + microphone.getValue() + ',' + amplifier.getValue() + "\r";
  }

  public ATctSAICWrite(final IO io, final Microphone microphone, final EarpieceAmplifier amplifier) {
    super(buildCommandATDataWithParamCheck(io, microphone, amplifier));
  }

  public ATctSAICWrite(final IO io, final Microphone microphone) {
    super(buildCommandATDataWithParamCheck(io, microphone));
  }

  public ATctSAICWrite(final IO io) {
    super(buildCommandATDataWithParamCheck(io));
  }

}
