/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ATpsVTSWrite extends EmptyResponseSolidAT {

  private static final String DTMF_TEMPLATE = "[0-9#*ABCDabcd]+";
  private static final Pattern dtmfPattern = Pattern.compile(DTMF_TEMPLATE);

  protected static String buildCommandATDataWithParamCheck(final String dtmfString) {
    Objects.requireNonNull(dtmfString, "dtmfString mustn't be null");
    Matcher matcher = dtmfPattern.matcher(dtmfString);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("dtmfString doesn't match DTMF template");
    }
    return "AT+VTS=\"" + dtmfString + "\"\r";
  }

  protected static void checkParam(final char dtmf) {
    Matcher matcher = dtmfPattern.matcher(String.valueOf(dtmf));
    if (!matcher.matches()) {
      throw new IllegalArgumentException("dtmf doesn't match DTMF template");
    }
  }

  public ATpsVTSWrite(final String commandATData, final long timeout) {
    super(commandATData, timeout);
  }

}
