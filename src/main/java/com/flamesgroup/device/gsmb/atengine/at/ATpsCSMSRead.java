/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Select Message Service <br>
 * Read command reports current service setting along with supported message types
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=202">Paragraph 3.5.5.1.1.</a>
 */
public abstract class ATpsCSMSRead<TATResult extends ATpsCSMSReadResult> extends SolidAT<TATResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CSMS: (?<service>[01]),(?<mt>[01]),(?<mo>[01]),(?<bm>[01])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  protected int service;
  protected int mt;
  protected int mo;
  protected int bm;

  public ATpsCSMSRead() {
    super("AT+CSMS?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      service = Integer.parseInt(matcher.group("service"));
      mt = Integer.parseInt(matcher.group("mt"));
      mo = Integer.parseInt(matcher.group("mo"));
      bm = Integer.parseInt(matcher.group("bm"));
      return true;
    } else {
      return false;
    }
  }

}
