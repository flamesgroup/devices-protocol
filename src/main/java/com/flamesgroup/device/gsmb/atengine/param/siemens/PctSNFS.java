/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.siemens;

public final class PctSNFS {

  private PctSNFS() {
  }

  public enum AudioMode {

    /**
     * Test mode with activated audio loop.
     */
    AUDIO_MODE0(0),
    /**
     * Standard mode optimized for the reference handset, that can
     * be connected to the analog interface 1 (see "MC55i Hardware
     * Interface Description" for information on this handset).
     */
    AUDIO_MODE1(1),
    /**
     * Customer specific mode for a basic handsfree (speakerphone)
     * device (Siemens Car Kit Portable).
     */
    AUDIO_MODE2(2),
    /**
     * Customer specific mode for a mono-headset.
     */
    AUDIO_MODE3(3),
    /**
     * Customer specific mode for a user handset.
     */
    AUDIO_MODE4(4),
    /**
     * Customer specific mode.
     */
    AUDIO_MODE5(5),
    /**
     * Customer specific mode.
     */
    AUDIO_MODE6(6);

    private final int value;

    AudioMode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static AudioMode getAudioModeByValue(final int value) {
      for (AudioMode mode : AudioMode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
