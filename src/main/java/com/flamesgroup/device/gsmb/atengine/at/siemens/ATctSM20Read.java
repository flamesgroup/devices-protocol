/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20.CallMode;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20.CmgwMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ATctSM20Read extends SolidAT<ATctSM20ReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\^SM20: (?<callmode>[01]), (?<cmgwmode>[01])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private CallMode callMode;
  private CmgwMode cmgwMode;

  public ATctSM20Read() {
    super("AT^SM20?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      callMode = CallMode.getModeByValue(Integer.parseInt(matcher.group("callmode")));
      cmgwMode = CmgwMode.getModeByValue(Integer.parseInt(matcher.group("cmgwmode")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATctSM20ReadResult getCommandATResult() {
    return new ATctSM20ReadResult(callMode, cmgwMode);
  }

}
