/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhCBC.ChargerState;

import java.util.Objects;

public final class AThhCBCExecResult implements IATResult {

  private final ChargerState chargerState;
  private final int batteryVoltage;

  public AThhCBCExecResult(final ChargerState chargerState, final int batteryVoltage) {
    this.chargerState = chargerState;
    this.batteryVoltage = batteryVoltage;
  }

  public ChargerState getChargerState() {
    return chargerState;
  }

  public int getBatteryVoltage() {
    return batteryVoltage;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof AThhCBCExecResult)) {
      return false;
    }
    AThhCBCExecResult that = (AThhCBCExecResult) object;

    return Objects.equals(chargerState, that.getChargerState())
        && batteryVoltage == that.getBatteryVoltage();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(chargerState);
    result = prime * result + batteryVoltage;
    return result;
  }

}
