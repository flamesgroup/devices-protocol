/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSLED.Mode;

import java.util.Objects;

/**
 * STAT_LED GPIO Setting <br>
 * Set command sets the behaviour of the STAT_LED GPIO
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=301">Paragraph 3.5.7.1.29.</a>
 */
public final class AThhSLEDWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final Mode mode, final Integer onDuration, final Integer offDuration) {
    Objects.requireNonNull(mode, "mode mustn't be null");
    StringBuilder at = new StringBuilder("AT#SLED=");
    at.append(mode.getValue());
    if (onDuration != null) {
      if (onDuration < 1 || onDuration > 100) {
        throw new IllegalArgumentException(String.format("Incorrect onDuration value [%d] - must be in range of 1-100", onDuration));
      }
      at.append(',').append(onDuration);
      if (offDuration != null) {
        if (offDuration < 1 || offDuration > 100) {
          throw new IllegalArgumentException(String.format("Incorrect offDuration value [%d] - must be in range of 1-100", offDuration));
        }
        at.append(',').append(offDuration);
      }
    }

    return at.append("\r").toString();
  }

  public AThhSLEDWrite(final Mode mode, final int onDuration, final int offDuration) {
    super(buildCommandATDataWithParamCheck(mode, onDuration, offDuration));
  }

  public AThhSLEDWrite(final Mode mode, final int onDuration) {
    super(buildCommandATDataWithParamCheck(mode, onDuration, null));
  }

  public AThhSLEDWrite(final Mode mode) {
    super(buildCommandATDataWithParamCheck(mode, null, null));
  }

}
