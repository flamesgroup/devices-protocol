/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20.CallMode;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20.CmgwMode;

import java.util.Objects;

public final class ATctSM20ReadResult implements IATResult {

  private final CallMode callMode;
  private final CmgwMode cmgwMode;

  public ATctSM20ReadResult(final CallMode callMode, final CmgwMode cmgwMode) {
    this.callMode = callMode;
    this.cmgwMode = cmgwMode;
  }

  public CallMode getCallMode() {
    return callMode;
  }

  public CmgwMode getCmgwMode() {
    return cmgwMode;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATctSM20ReadResult)) {
      return false;
    }
    ATctSM20ReadResult that = (ATctSM20ReadResult) object;

    return Objects.equals(callMode, that.getCallMode())
        && Objects.equals(cmgwMode, that.getCmgwMode());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(callMode);
    result = prime * result + Objects.hashCode(cmgwMode);
    return result;
  }

}
