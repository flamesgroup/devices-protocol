/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extended Numeric Error report <br>
 * Execution command causes the TA to return a numeric code which should offer the user of the TA a report of the reason for:
 * <ul>
 * <li>the failure in the last unsuccessful call setup (originating or answering);</li>
 * <li>the last call release;</li>
 * <li>the last unsuccessful GPRS attach or unsuccessful PDP context activation;</li>
 * <li>the last GPRS detach or PDP context deactivation.</li>
 * </ul>
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=277">Paragraph 3.5.7.1.9.</a>
 */
public final class AThhCEERExec extends SolidAT<AThhCEERExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "#CEER: (?<code>\\d+)";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private int code;

  public AThhCEERExec() {
    super("AT#CEER\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      code = Integer.parseInt(matcher.group("code"));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected AThhCEERExecResult getCommandATResult() {
    return new AThhCEERExecResult(code);
  }

}
