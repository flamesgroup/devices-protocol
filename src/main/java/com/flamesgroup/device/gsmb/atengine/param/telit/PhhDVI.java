/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhDVI {

  private PhhDVI() {
  }

  public enum DVIMode {

    DISABLE_DVI(0),
    ENABLE_DVI(1),
    ENABLE_DVI_AND_ANALOG_LINES(2);

    private final int value;

    DVIMode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum ClockMode {

    DVI_SLAVE(0),
    DVI_MASTER(1); //default

    private final int value;

    ClockMode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
