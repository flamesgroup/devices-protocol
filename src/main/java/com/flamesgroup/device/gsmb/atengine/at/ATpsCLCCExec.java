/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.param.PpsCLCC;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * List Current Calls <br>
 * Execution command returns the list of current calls and their characteristics
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=128">Paragraph 3.5.4.3.15.</a>
 */
public final class ATpsCLCCExec extends SolidAT<ATpsCLCCExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CLCC: (?<idx>[0-9]+),(?<dir>[01]),(?<stat>[0-5]),(?<mode>[0-9]),(?<mpty>[01])(,\"(?<number>.*?)\",(?<type>(\\d{3}))(,(?<alpha>.*))?)?";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private final List<ATpsCLCCExecResult.CallInfo> callList = new ArrayList<>();

  public ATpsCLCCExec() {
    super("AT+CLCC\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      int idx = Integer.parseInt(matcher.group("idx"));
      boolean originated = !ATLexemeProcessHelper.intStringToBoolean(matcher.group("dir"));
      PpsCLCC.CallState state = PpsCLCC.CallState.getStateByValue(Integer.parseInt(matcher.group("stat")));
      PpsCLCC.Mode mode = PpsCLCC.Mode.getModeByValue(Integer.parseInt(matcher.group("mode")));
      boolean conference = ATLexemeProcessHelper.intStringToBoolean(matcher.group("mpty"));
      String number = matcher.group("number");
      String type = matcher.group("type");
      PpsCLCC.TypeOfAddressOctet typeOfAddressOctet = null;
      if (type != null) {
        typeOfAddressOctet = PpsCLCC.TypeOfAddressOctet.getTypeOfAddressOctetByValue(Integer.parseInt(type));
      }
      callList.add(new ATpsCLCCExecResult.CallInfo(idx, originated, state, mode, conference, number, typeOfAddressOctet));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCLCCExecResult getCommandATResult() {
    return new ATpsCLCCExecResult(callList);
  }

}
