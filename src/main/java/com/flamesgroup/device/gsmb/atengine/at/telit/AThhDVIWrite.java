/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhDVI.ClockMode;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhDVI.DVIMode;

import java.util.Objects;

/**
 * Digital Voiceband Interface <br>
 * Set command enables/disables the Digital Voiceband Interface
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=667">Paragraph 3.5.7.21.7.1.</a>
 */
public final class AThhDVIWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final DVIMode dviMode, final int dviPort, final ClockMode clockMode) {
    Objects.requireNonNull(dviMode, "dviMode mustn't be null");
    if (dviPort < 1 || dviPort > 2) {
      throw new IllegalArgumentException(String.format("Incorrect dviPort value [%s] -  dviPort must be in range of 1-2", dviPort));
    }
    Objects.requireNonNull(clockMode, "clockMode mustn't be null");
    if (dviPort == 0) {
      if (clockMode == ClockMode.DVI_SLAVE) {
        throw new IllegalArgumentException(String.format("clockMode [%s] is available only on port 1", ClockMode.DVI_SLAVE));
      }
    }
    return "AT#DVI=" + dviMode.getValue() + ',' + dviPort + ',' + clockMode.getValue() + "\r";
  }

  /**
   * @param dviMode   enable/disable the DVI
   * @param dviPort   DVI port (1-2)
   * @param clockMode DVI slave/master. DVI slave is available only on port 1
   */
  public AThhDVIWrite(final DVIMode dviMode, final int dviPort, final ClockMode clockMode) {
    super(buildCommandATDataWithParamCheck(dviMode, dviPort, clockMode));
  }

}
