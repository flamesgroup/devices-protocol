/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd.telit;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCLIPResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCLIRResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCallWaitingResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshIMEIResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCLIP;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCLIR;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCallBarring;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCallBarringResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCallForwarding;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCallForwardingResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshCallWaiting;
import com.flamesgroup.device.gsmb.atengine.at.starhash.telit.ATshIMEI;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIP;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIR;
import com.flamesgroup.device.gsmb.atengine.ussd.BaseUSSDSessionStartHashWrapper;

import java.util.HashMap;
import java.util.Map;

public class USSDSessionStarHashWrapper extends BaseUSSDSessionStartHashWrapper {

  public USSDSessionStarHashWrapper(final USSDSession ussdSession) {
    super(ussdSession);
  }

  @Override
  protected String executeIMEICommand() throws ChannelException, InterruptedException {
    ATshIMEIResult result = executeWithinWrappedUSSDSession(new ATshIMEI());
    return result.getIMEI();
  }

  @Override
  protected PshCLIP.Status executeCLIPCommand() throws ChannelException, InterruptedException {
    ATshCLIPResult result = executeWithinWrappedUSSDSession(new ATshCLIP());
    return result.getStatus();
  }

  @Override
  protected PshCLIR.StatusOnNetwork executeCLIRCommand() throws ChannelException, InterruptedException {
    ATshCLIRResult result = executeWithinWrappedUSSDSession(new ATshCLIR());
    return result.getStatusOnNetwork();
  }

  @Override
  protected Map<ServiceClass, Boolean> executeCallForwardingCommand(final String code) throws ChannelException, InterruptedException {
    ATshCallForwardingResult result = executeWithinWrappedUSSDSession(new ATshCallForwarding(code));
    Map<ServiceClass, Boolean> serviceClasses = new HashMap<>();
    for (ATshCallForwardingResult.CallForwardingInfo callForwardingInfo : result.getCallForwardingInfoList()) {
      for (ServiceClass serviceClass : callForwardingInfo.getServiceClasses()) {
        serviceClasses.put(serviceClass, callForwardingInfo.isActive());
      }
    }
    return serviceClasses;
  }

  @Override
  protected Map<ServiceClass, Boolean> executeCallWaitingCommand(final String code) throws ChannelException, InterruptedException {
    ATshCallWaitingResult result = executeWithinWrappedUSSDSession(new ATshCallWaiting(code));
    Map<ServiceClass, Boolean> serviceClasses = new HashMap<>();
    for (ATshCallWaitingResult.CallWaitingInfo callWaitingInfo : result.getCallWaitingInfoList()) {
      for (ServiceClass serviceClass : callWaitingInfo.getServiceClasses()) {
        serviceClasses.put(serviceClass, callWaitingInfo.isActive());
      }
    }
    return serviceClasses;
  }

  @Override
  protected Map<ServiceClass, Boolean> executeCallBarringCommand(final String code) throws ChannelException, InterruptedException {
    ATshCallBarringResult result = executeWithinWrappedUSSDSession(new ATshCallBarring(code));
    Map<ServiceClass, Boolean> serviceClasses = new HashMap<>();
    for (ATshCallBarringResult.ATCallBarringInfo callBarringInfo : result.getCallBarringInfoList()) {
      for (ServiceClass serviceClass : callBarringInfo.getServiceClasses()) {
        serviceClasses.put(serviceClass, callBarringInfo.isActive());
      }
    }
    return serviceClasses;
  }

}
