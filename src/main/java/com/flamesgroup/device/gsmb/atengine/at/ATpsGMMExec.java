/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsGMM.Model;

/**
 * Model Identification <br>
 * Execution command returns the model identification.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=57">Paragraph 3.5.3.1.10.</a>
 */
public final class ATpsGMMExec extends SolidAT<ATpsGMMExecResult> {

  private Model model;

  public ATpsGMMExec() {
    super("AT+GMM\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    try {
      model = Model.getModelByName(lexeme);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  @Override
  protected ATpsGMMExecResult getCommandATResult() {
    return new ATpsGMMExecResult(model);
  }

}
