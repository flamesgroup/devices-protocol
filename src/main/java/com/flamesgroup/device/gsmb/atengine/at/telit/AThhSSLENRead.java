/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Read currently enabling a SSL socket <br>
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=690">Paragraph 3.5.7.24.3.</a>
 */
public final class AThhSSLENRead extends SolidAT<AThhSSLENReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "#SSLEN: 1,(?<enable>[1,0])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private boolean enable;

  public AThhSSLENRead() {
    super("AT#SSLEN?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      enable = ATLexemeProcessHelper.intStringToBoolean(matcher.group("enable"));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected AThhSSLENReadResult getCommandATResult() {
    return new AThhSSLENReadResult(enable);
  }

}
