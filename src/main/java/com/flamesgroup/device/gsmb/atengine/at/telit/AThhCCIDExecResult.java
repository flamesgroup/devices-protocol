/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Objects;

public final class AThhCCIDExecResult implements IATResult {

  private final String iccid;

  public AThhCCIDExecResult(final String iccid) {
    this.iccid = iccid;
  }

  public String getIccid() {
    return iccid;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof AThhCCIDExecResult)) {
      return false;
    }
    AThhCCIDExecResult that = (AThhCCIDExecResult) object;

    return Objects.equals(iccid, that.getIccid());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(iccid);
    return result;
  }

}
