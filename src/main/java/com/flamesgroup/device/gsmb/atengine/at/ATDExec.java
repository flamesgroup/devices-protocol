/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PATD;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public final class ATDExec extends EmptyResponseSolidAT {

  private static final long COMMAND_EXECUTE_TIMEOUT = TimeUnit.MINUTES.toMillis(3);

  private static String buildCommandATDataWithParamCheck(final String number) {
    Objects.requireNonNull(number, "number mustn't be null");
    return "ATD" + number + ";\r";
  }

  private static String buildCommandATDataWithParamCheck(final String number, final PATD.GSMModifier gsmModifier) {
    Objects.requireNonNull(number, "number mustn't be null");
    Objects.requireNonNull(gsmModifier, "gsmModifier mustn't be null");
    return "ATD" + number + gsmModifier.getValue() + ";\r";
  }

  public ATDExec(final String number) {
    super(buildCommandATDataWithParamCheck(number), COMMAND_EXECUTE_TIMEOUT);
  }

  public ATDExec(final String number, final PATD.GSMModifier gsmModifier) {
    super(buildCommandATDataWithParamCheck(number, gsmModifier), COMMAND_EXECUTE_TIMEOUT);
  }

  @Override
  protected boolean internalProcessATErrorStatus(final String lexeme) {
    switch (lexeme) {
      case "NO CARRIER": {
        setCommandATErrorStatusException(new ATNoCarrierStatusException(getCommandATData()));
        return true;
      }
      case "BUSY": {
        setCommandATErrorStatusException(new ATBusyStatusException(getCommandATData()));
        return true;
      }
      case "NO DIALTONE": {
        setCommandATErrorStatusException(new ATNoDialtoneStatusException(getCommandATData()));
        return true;
      }
    }
    return super.internalProcessATErrorStatus(lexeme);
  }

}
