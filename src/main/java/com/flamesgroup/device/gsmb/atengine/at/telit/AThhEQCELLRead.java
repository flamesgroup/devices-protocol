/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AThhEQCELLRead extends SolidAT<AThhEQCELLReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "#EQCELL: (?<mode>[01])(?:,(?<arfcn>\\d+),(?<val>-?\\d+))?";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private final List<AThhEQCELLReadResult.EqCellInfo> eqCellList = new ArrayList<>();

  public AThhEQCELLRead() {
    super("AT#EQCELL?\r", 7000); // double value of real execution
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      String mode = matcher.group("mode");
      if (mode.equals("1")) {
        int arfcn = Integer.parseInt(matcher.group("arfcn"));
        int val = Integer.parseInt(matcher.group("val"));

        eqCellList.add(new AThhEQCELLReadResult.EqCellInfo(arfcn, val));
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected AThhEQCELLReadResult getCommandATResult() {
    return new AThhEQCELLReadResult(eqCellList);
  }

}
