/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLEN;

import java.util.Objects;

/**
 * Enable a SSL socket <br>
 * This command enables a socket secured by SSL.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=689">Paragraph 3.5.7.24.3.</a>
 */
public final class AThhSSLENExec extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final PhhSSLEN.OnOff onOff) {
    Objects.requireNonNull(onOff, "flowControl mustn't be null");
    return "AT#SSLEN=1," + onOff.getValue() + "\r";
  }

  public AThhSSLENExec(final PhhSSLEN.OnOff onOff) {
    super(buildCommandATDataWithParamCheck(onOff));
  }

}
