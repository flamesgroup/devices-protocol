/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.siemens;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import com.flamesgroup.device.gsmb.atengine.param.starhash.siemens.PshCallBarring;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ATshCallBarringResult implements IATResult {

  private final List<ATCallBarringInfo> callBarringInfoList;

  public ATshCallBarringResult(final List<ATCallBarringInfo> callBarringInfoList) {
    this.callBarringInfoList = Collections.unmodifiableList(callBarringInfoList);
  }

  public List<ATCallBarringInfo> getCallBarringInfoList() {
    return callBarringInfoList;
  }

  public static class ATCallBarringInfo {

    private final PshCallBarring.Facility facility;
    private final boolean active;
    private final Set<ServiceClass> serviceClasses;

    public ATCallBarringInfo(final PshCallBarring.Facility facility, final boolean active, final Set<ServiceClass> serviceClasses) {
      this.facility = facility;
      this.active = active;
      this.serviceClasses = serviceClasses;
    }

    public PshCallBarring.Facility getFacility() {
      return facility;
    }

    public boolean isActive() {
      return active;
    }

    public Set<ServiceClass> getServiceClasses() {
      return serviceClasses;
    }

    @Override
    public boolean equals(final Object object) {
      if (this == object) {
        return true;
      }
      if (!(object instanceof ATCallBarringInfo)) {
        return false;
      }
      ATCallBarringInfo that = (ATCallBarringInfo) object;

      return Objects.equals(facility, that.getFacility())
          && active == that.isActive()
          && Objects.equals(serviceClasses, that.getServiceClasses());
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 3;
      result = prime * result + Objects.hashCode(facility);
      result = prime * result + Boolean.hashCode(active);
      result = prime * result + Objects.hashCode(serviceClasses);
      return result;
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ATshCallBarringResult)) {
      return false;
    }
    ATshCallBarringResult that = (ATshCallBarringResult) object;

    return Objects.equals(callBarringInfoList, that.getCallBarringInfoList());

  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(callBarringInfoList);
    return result;
  }

}
