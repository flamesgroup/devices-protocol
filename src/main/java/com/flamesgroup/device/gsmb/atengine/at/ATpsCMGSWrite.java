/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Send Message <br>
 * Execution command sends to the network a message.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=250">Paragraph 3.5.5.4.1.</a>
 */
public final class ATpsCMGSWrite implements IAT<ATpsCMGSWriteResult> {

  private final ATpsCMGSWritePart1 part1;
  private final ATpsCMGSWritePart2 part2;

  public ATpsCMGSWrite(final String destinationAddress, final int typeOfDestinationAddress, final String text) {
    part1 = new ATpsCMGSWritePart1(destinationAddress, typeOfDestinationAddress);
    part2 = new ATpsCMGSWritePart2(text, true);
  }

  public ATpsCMGSWrite(final String destinationAddress, final String text) {
    part1 = new ATpsCMGSWritePart1(destinationAddress);
    part2 = new ATpsCMGSWritePart2(text, true);
  }

  public ATpsCMGSWrite(final int length, final String pdu) {
    part1 = new ATpsCMGSWritePart1(length);
    part2 = new ATpsCMGSWritePart2(pdu, false);
  }

  @Override
  public ATpsCMGSWriteResult execute(final IATSubChannel atSubChannel) throws ChannelException, InterruptedException {
    part1.execute(atSubChannel);
    return part2.execute(atSubChannel);
  }

  @Override
  public boolean isWaitBeforeNextAT() {
    return true;
  }

  @Override
  public boolean process(final String atLexeme) {
    return part1.process(atLexeme) || part2.process(atLexeme);
  }

  private static final class ATpsCMGSWritePart1 extends EmptyResponseSolidAT {

    private static String buildCommandATDataWithParamCheck(final String destinationAddress, final int typeOfDestinationAddress) {
      Objects.requireNonNull(destinationAddress, "destinationAddress address mustn't be null");
      return "AT+CMGS=\"" + destinationAddress + "\"," + typeOfDestinationAddress + "\r";
    }

    private static String buildCommandATDataWithParamCheck(final String destinationAddress) {
      Objects.requireNonNull(destinationAddress, "destinationAddress address mustn't be null");
      return "AT+CMGS=\"" + destinationAddress + "\"\r";
    }

    private static String buildCommandATDataWithParamCheck(final int length) {
      if (length <= 0) {
        throw new IllegalArgumentException(String.format("length value [%d] isn't correct - length must be greater than 0", length));
      }
      return "AT+CMGS=" + length + "\r";
    }

    public ATpsCMGSWritePart1(final String destinationAddress, final int typeOfDestinationAddress) {
      super(buildCommandATDataWithParamCheck(destinationAddress, typeOfDestinationAddress), 10000);
    }

    public ATpsCMGSWritePart1(final String destinationAddress) {
      super(buildCommandATDataWithParamCheck(destinationAddress), 10000);
    }

    public ATpsCMGSWritePart1(final int length) {
      super(buildCommandATDataWithParamCheck(length), 10000);
    }

    @Override
    protected boolean internalProcessATOKStatus(final String lexeme) {
      return "> ".equals(lexeme) || super.internalProcessATOKStatus(lexeme);
    }

  }

  private static final class ATpsCMGSWritePart2 extends SolidAT<ATpsCMGSWriteResult> {

    private static final String AT_RESPONSE_TEMPLATE = "\\+CMGS: (?<mr>[0-9]+)(,(?<sctsackpdu>.+))?";
    private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

    private final boolean isTextMode;

    private int mr;
    private String sctsOrAckpdu;

    private static String buildCommandATDataWithParamCheck(final String data) {
      Objects.requireNonNull(data, "data mustn't be null");
      return data + (char) 0x1A;
    }

    public ATpsCMGSWritePart2(final String data, final boolean isTextMode) {
      super(buildCommandATDataWithParamCheck(data), 60000);
      this.isTextMode = isTextMode;
    }

    @Override
    public boolean isWaitBeforeNextAT() {
      return false;
    }

    @Override
    protected boolean internalProcessATResponse(final String lexeme) {
      Matcher matcher = atResponsePattern.matcher(lexeme);
      if (matcher.matches()) {
        mr = Integer.parseInt(matcher.group("mr"));
        sctsOrAckpdu = matcher.group("sctsackpdu");
        return true;
      } else {
        return false;
      }
    }

    @Override
    protected ATpsCMGSWriteResult getCommandATResult() {
      return new ATpsCMGSWriteResult(mr, sctsOrAckpdu, isTextMode);
    }

  }

}
