/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhCBC {

  private PhhCBC() {
  }

  public enum ChargerState {

    CHARGER_NOT_CONNECTED(0),
    CHARGER_CONNECTED_AND_CHARGING(1),
    CHARGER_CONNECTED_AND_CHARGE_COMPLETED(2);

    private final int value;

    ChargerState(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static ChargerState getChargerStateByValue(final int value) {
      for (ChargerState state : ChargerState.values()) {
        if (state.getValue() == value) {
          return state;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }
  }

}
