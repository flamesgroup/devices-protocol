/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCBC.BatteryChargeLevel;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCBC.BatteryStatus;

import java.util.Objects;

public final class ATpsCBCExecResult implements IATResult {

  private final BatteryStatus batteryStatus;
  private final BatteryChargeLevel batteryChargeLevel;

  public ATpsCBCExecResult(final BatteryStatus batteryStatus, final BatteryChargeLevel batteryChargeLevel) {
    this.batteryStatus = batteryStatus;
    this.batteryChargeLevel = batteryChargeLevel;
  }

  public BatteryStatus getBatteryStatus() {
    return batteryStatus;
  }

  public BatteryChargeLevel getBatteryChargeLevel() {
    return batteryChargeLevel;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCBCExecResult)) {
      return false;
    }
    ATpsCBCExecResult that = (ATpsCBCExecResult) object;

    return Objects.equals(batteryStatus, that.getBatteryStatus())
        && Objects.equals(batteryChargeLevel, that.getBatteryChargeLevel());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(batteryStatus);
    result = prime * result + Objects.hashCode(batteryChargeLevel);
    return result;
  }

}
