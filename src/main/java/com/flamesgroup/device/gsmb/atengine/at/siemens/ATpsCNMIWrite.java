/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BroadcastReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BufferedURCHandlingMethod;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSDeliveringOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSStatusReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.UrcBufferingOption;

public final class ATpsCNMIWrite extends com.flamesgroup.device.gsmb.atengine.at.ATpsCNMIWrite {

  public ATpsCNMIWrite(final UrcBufferingOption urcBufferingOption, final SMSDeliveringOption smsDeliveringOption,
      final BroadcastReportingOption broadcastReportingOption, final SMSStatusReportingOption smsStatusReportingOption,
      final BufferedURCHandlingMethod bufferedURCHandlingMethod) {
    super(urcBufferingOption, smsDeliveringOption, broadcastReportingOption, smsStatusReportingOption, bufferedURCHandlingMethod);
    if (bufferedURCHandlingMethod == BufferedURCHandlingMethod.FLUSH_TO_TE) {
      throw new IllegalArgumentException("bufferedURCHandlingMethod value " + bufferedURCHandlingMethod + " is not supported");
    }
  }

}
