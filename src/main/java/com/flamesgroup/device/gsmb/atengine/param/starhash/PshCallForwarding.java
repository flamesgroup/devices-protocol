/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.starhash;

public final class PshCallForwarding {

  public enum Reason {

    UNCONDITIONAL(0),
    MOBILE_BUSY(1),
    NO_REPLY(2),
    NOT_REACHABLE(3),
    ALL_CALL_FORWARDING(4), // includes reasons 0, 1, 2 and 3
    ALL_CONDITIONAL_CALL_FORWARDING(5); // includes reasons 1, 2 and 3

    private final int value;

    Reason(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Reason getReasonByValue(final int value) {
      for (Reason reason : Reason.values()) {
        if (reason.getValue() == value) {
          return reason;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Type {

    NATIONAL_NUMBERING_SCHEME(129),
    INTERNATIONAL_NUMBERING_SCHEME(145);

    private final int value;

    Type(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Type getTypeByValue(final int value) {
      for (Type type : Type.values()) {
        if (type.getValue() == value) {
          return type;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
