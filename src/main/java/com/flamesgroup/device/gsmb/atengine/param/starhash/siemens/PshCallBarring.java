/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.starhash.siemens;

public final class PshCallBarring {

  private PshCallBarring() {
  }

  public enum Facility {

    AO, // Barring All Outgoing Calls (BAOC)
    OI, // Barring Outgoing International Calls (BOIC)
    OX, // Barring Outgoing International Calls except to Home Country (BOIC ex Home)
    AI, // Barring All Incoming Calls (BAIC)
    IR, // Barring Incoming Calls when Roaming outside the home country (BAIC Roaming)
    AB, // All Barring Services
    AG, // All Outgoing Barring Services
    AC; // All Incoming Barring Services

    Facility() {
    }

  }

}
