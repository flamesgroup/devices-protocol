/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Format;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Mode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Operator Selection <br>
 * Read command returns current value of &lt;mode&gt;,&lt;format&gt; and &lt;oper&gt;
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=105">Paragraph 3.5.4.3.4.</a>
 */
public final class ATpsCOPSRead extends SolidAT<ATpsCOPSReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+COPS: (?<mode>[0-4])(,(?<format>[02])(,\"(?<oper>.*)\")?)?";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private Mode mode;
  private Format format;
  private String operator;

  public ATpsCOPSRead() {
    super("AT+COPS?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      mode = Mode.getModeByValue(Integer.parseInt(matcher.group("mode")));
      String formatStr = matcher.group("format");
      if (formatStr != null) {
        format = Format.getFormatByValue(Integer.parseInt(formatStr));
      }
      operator = matcher.group("oper");
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCOPSReadResult getCommandATResult() {
    return new ATpsCOPSReadResult(mode, format, operator);
  }

}
