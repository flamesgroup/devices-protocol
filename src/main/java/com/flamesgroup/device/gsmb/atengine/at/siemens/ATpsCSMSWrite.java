/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.param.siemens.PpsCSMS;

import java.util.Objects;

public final class ATpsCSMSWrite extends com.flamesgroup.device.gsmb.atengine.at.ATpsCSMSWrite {

  private static String buildCommandATDataWithParamCheck(final PpsCSMS.MessagingService messagingService) {
    Objects.requireNonNull(messagingService, "messagingService mustn't be null");
    return "AT+CSMS=" + messagingService.getValue() + "\r";
  }

  public ATpsCSMSWrite(final PpsCSMS.MessagingService messagingService) {
    super(buildCommandATDataWithParamCheck(messagingService));
  }

}
