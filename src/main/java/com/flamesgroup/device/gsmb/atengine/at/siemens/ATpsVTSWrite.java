/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

public final class ATpsVTSWrite extends com.flamesgroup.device.gsmb.atengine.at.ATpsVTSWrite {

  private static final int MAX_TIMEOUT = 25750; //(254 * 100 + 300) + 50

  private static String buildCommandATDataWithParamCheck(final char dtmf, final int duration) {
    checkParam(dtmf);
    int durationConverted = duration / 100 - 2;
    if (durationConverted < 1 || durationConverted > 255) {
      throw new IllegalArgumentException(String.format("Incorrect duration value [%d] - duration must be in range of 300-25700", duration));
    }
    return "AT+VTS=\"" + dtmf + "\"," + durationConverted + "\r";
  }

  /**
   * @param dtmfString String of ASCII characters in the set 0-9,#,*,A, B, C, D. Maximal
   *                   length of the string is 29
   */
  public ATpsVTSWrite(final String dtmfString) {
    super(buildCommandATDataWithParamCheck(dtmfString), MAX_TIMEOUT * dtmfString.length());
  }

  /**
   * @param dtmf     DTMF single character
   * @param duration tone duration in milliseconds. The minimum duration is 300ms
   *                 (equivalent to 1 for AT command),
   *                 maximum - 25700ms (equivalent to 255 for AT command)
   */
  public ATpsVTSWrite(final char dtmf, final int duration) {
    super(buildCommandATDataWithParamCheck(dtmf, duration), MAX_TIMEOUT);
  }

}
