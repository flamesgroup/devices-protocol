/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Format;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Mode;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Operator Selection <br>
 * Set command forces an attempt to select and register the GSM network operator.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=105">Paragraph 3.5.4.3.4.</a>
 */
public final class ATpsCOPSWrite extends EmptyResponseSolidAT {

  private static final long COMMAND_EXECUTE_TIMEOUT = TimeUnit.SECONDS.toMillis(5);

  private static String buildCommandATDataWithParamCheck(final Mode mode, final Format format, final String operator) {
    Objects.requireNonNull(mode, "mode mustn't be null");

    StringBuilder commandATDataBuilder = new StringBuilder("AT+COPS=").append(mode.getValue());
    if (format != null) {
      commandATDataBuilder.append(',').append(format.getValue());
      if (operator != null) {
        commandATDataBuilder.append(',').append('\"').append(operator).append('\"');
      }
    }
    return commandATDataBuilder.append("\r").toString();
  }

  public ATpsCOPSWrite(final Mode mode, final Format format, final String operator) {
    super(buildCommandATDataWithParamCheck(mode, format, operator), COMMAND_EXECUTE_TIMEOUT);
  }

  public ATpsCOPSWrite(final Mode mode, final Format format) {
    this(mode, format, null);
  }

  public ATpsCOPSWrite(final Mode mode) {
    this(mode, null);
  }

}
