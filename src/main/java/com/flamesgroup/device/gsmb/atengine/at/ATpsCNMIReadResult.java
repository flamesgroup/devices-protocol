/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BroadcastReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BufferedURCHandlingMethod;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSDeliveringOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSStatusReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.UrcBufferingOption;

import java.util.Objects;

public final class ATpsCNMIReadResult implements IATResult {

  private final UrcBufferingOption urcBufferingOption;
  private final SMSDeliveringOption smsDeliveringOption;
  private final BroadcastReportingOption broadcastReportingOption;
  private final SMSStatusReportingOption smsStatusReportingOption;
  private final BufferedURCHandlingMethod bufferedURCHandlingMethod;

  public ATpsCNMIReadResult(final UrcBufferingOption urcBufferingOption, final SMSDeliveringOption smsDeliveringOption,
      final BroadcastReportingOption broadcastReportingOption, final SMSStatusReportingOption smsStatusReportingOption,
      final BufferedURCHandlingMethod bufferedURCHandlingMethod) {
    this.urcBufferingOption = urcBufferingOption;
    this.smsDeliveringOption = smsDeliveringOption;
    this.broadcastReportingOption = broadcastReportingOption;
    this.smsStatusReportingOption = smsStatusReportingOption;
    this.bufferedURCHandlingMethod = bufferedURCHandlingMethod;
  }

  public UrcBufferingOption getURCBufferingOption() {
    return urcBufferingOption;
  }

  public SMSDeliveringOption getSMSDeliveringOption() {
    return smsDeliveringOption;
  }

  public BroadcastReportingOption getBroadcastReportingOption() {
    return broadcastReportingOption;
  }

  public SMSStatusReportingOption getSMSStatusReportingOption() {
    return smsStatusReportingOption;
  }

  public BufferedURCHandlingMethod getBufferedURCHandlingMethod() {
    return bufferedURCHandlingMethod;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCNMIReadResult)) {
      return false;
    }
    ATpsCNMIReadResult that = (ATpsCNMIReadResult) object;

    return Objects.equals(urcBufferingOption, that.getURCBufferingOption())
        && Objects.equals(smsDeliveringOption, that.getSMSDeliveringOption())
        && Objects.equals(broadcastReportingOption, that.getBroadcastReportingOption())
        && Objects.equals(smsStatusReportingOption, that.getSMSStatusReportingOption())
        && Objects.equals(bufferedURCHandlingMethod, that.getBufferedURCHandlingMethod());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(urcBufferingOption);
    result = prime * result + Objects.hashCode(smsDeliveringOption);
    result = prime * result + Objects.hashCode(broadcastReportingOption);
    result = prime * result + Objects.hashCode(smsStatusReportingOption);
    result = prime * result + Objects.hashCode(bufferedURCHandlingMethod);
    return result;
  }

}
