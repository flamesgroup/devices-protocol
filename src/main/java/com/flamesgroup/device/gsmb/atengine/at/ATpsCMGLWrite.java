/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation only for PDU mode. <br>
 * List Messages <br>
 * Execution command reports the list of all the messages
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=229">Paragraph 3.5.5.3.2.</a>
 */
public final class ATpsCMGLWrite extends SolidAT<ATpsCMGLWriteResult> {

  private static final String AT_RESPONSE_TEMPLATE1 = "\\+CMGL: (?<index>\\d+),(?<stat>[0-4]),(?<alpha>.+)?,(?<length>\\d+)";
  private static final String AT_RESPONSE_TEMPLATE2 = "(?<pdu>[a-fA-F_0-9]+)";

  private static final Pattern atResponsePattern1 = Pattern.compile(AT_RESPONSE_TEMPLATE1);
  private static final Pattern atResponsePattern2 = Pattern.compile(AT_RESPONSE_TEMPLATE2);

  private int index;
  private int stat;
  private String alpha;
  private int length;

  private final List<SMSInfo> smsList = new ArrayList<>();

  private boolean processFirstPart = true;

  private static String buildCommandATDataWithParamCheck(final SMSStatus smsStatus) {
    Objects.requireNonNull(smsStatus, "smsStatus mustn't be null");
    return "AT+CMGL=" + smsStatus.getValue() + "\r";
  }

  public ATpsCMGLWrite(final SMSStatus smsStatus) {
    super(buildCommandATDataWithParamCheck(smsStatus), 20000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    if (processFirstPart) {
      Matcher matcher = atResponsePattern1.matcher(lexeme);
      if (matcher.matches()) {
        index = Integer.parseInt(matcher.group("index"));
        stat = Integer.parseInt(matcher.group("stat"));
        alpha = matcher.group("alpha");
        length = Integer.parseInt(matcher.group("length"));
        processFirstPart = false;
        return true;
      }
    } else {
      Matcher matcher = atResponsePattern2.matcher(lexeme);
      if (matcher.matches()) {
        String pdu = matcher.group("pdu");
        smsList.add(new SMSInfo(index, SMSStatus.getSMSStatusByValue(stat), alpha, length, pdu));
        processFirstPart = true;
        return true;
      }
    }
    return false;
  }

  @Override
  protected ATpsCMGLWriteResult getCommandATResult() {
    return new ATpsCMGLWriteResult(smsList);
  }

}
