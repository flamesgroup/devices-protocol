/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSMGO.SMSOverflowStatus;

import java.util.Objects;

public final class ATctSMGOReadResult implements IATResult {

  private final boolean smsOverflowPresentationModeEnabled;
  private final SMSOverflowStatus smsOverflowStatus;

  public ATctSMGOReadResult(final boolean smsOverflowPresentationModeEnabled, final SMSOverflowStatus smsOverflowStatus) {
    this.smsOverflowPresentationModeEnabled = smsOverflowPresentationModeEnabled;
    this.smsOverflowStatus = smsOverflowStatus;
  }

  public boolean isSmsOverflowPresentationModeEnabled() {
    return smsOverflowPresentationModeEnabled;
  }

  public SMSOverflowStatus getSmsOverflowStatus() {
    return smsOverflowStatus;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATctSMGOReadResult)) {
      return false;
    }
    ATctSMGOReadResult that = (ATctSMGOReadResult) object;

    return smsOverflowPresentationModeEnabled == that.isSmsOverflowPresentationModeEnabled()
        && Objects.equals(smsOverflowStatus, that.getSmsOverflowStatus());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Boolean.hashCode(smsOverflowPresentationModeEnabled);
    result = prime * result + Objects.hashCode(smsOverflowStatus);
    return result;
  }

}
