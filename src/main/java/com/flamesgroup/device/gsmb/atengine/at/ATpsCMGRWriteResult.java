/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import java.util.Objects;

/**
 * Result only for PDU mode.
 */
public final class ATpsCMGRWriteResult implements IATResult {

  private final SMSStatus status;
  private final String alpha;
  private final int length;
  private final String pdu;

  public ATpsCMGRWriteResult(final SMSStatus status, final String alpha, final int length, final String pdu) {
    this.status = status;
    this.alpha = alpha;
    this.length = length;
    this.pdu = pdu;
  }

  public SMSStatus getStatus() {
    return status;
  }

  public String getAlpha() {
    return alpha;
  }

  public int getLength() {
    return length;
  }

  public String getPDU() {
    return pdu;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCMGRWriteResult)) {
      return false;
    }
    ATpsCMGRWriteResult that = (ATpsCMGRWriteResult) object;

    return Objects.equals(status, that.getStatus())
        && Objects.equals(alpha, that.getAlpha())
        && length == that.getLength()
        && Objects.equals(pdu, that.getPDU());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(status);
    result = prime * result + Objects.hashCode(alpha);
    result = prime * result + length;
    result = prime * result + Objects.hashCode(pdu);
    return result;
  }

}
