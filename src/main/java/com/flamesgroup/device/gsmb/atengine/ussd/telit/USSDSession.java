/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd.telit;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCUSDWrite;
import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.URCPresentation;
import com.flamesgroup.device.gsmb.atengine.ussd.BaseUSSDSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class USSDSession extends BaseUSSDSession {

  private static final Logger logger = LoggerFactory.getLogger(USSDSession.class);

  public USSDSession(final IATEngine atEngine) {
    super(atEngine);
  }

  @Override
  protected void executeOpenAction(final String ussdString) throws ChannelException, InterruptedException {
    atEngine.execute(new ATpsCUSDWrite(URCPresentation.ENABLE, ussdString));
  }

  @Override
  protected void executeResponseAction(final String response) throws ChannelException, InterruptedException {
    atEngine.execute(new ATpsCUSDWrite(URCPresentation.ENABLE, response));
  }

  @Override
  protected void executeCloseAction() throws ChannelException, InterruptedException {
    atEngine.execute(new ATpsCUSDWrite(URCPresentation.CANCEL_SESSION));
  }

}
