/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Read ICCID (Integrated Circuit Card Identification) <br>
 * Execution command reads on SIM the ICCID (card identification number that AT#CCID provides a unique identification number for the SIM)
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=276">Paragraph 3.5.7.1.7.</a>
 */
public final class AThhCCIDExec extends SolidAT<AThhCCIDExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "#CCID: (?<cid>.+)?";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private String iccid;

  public AThhCCIDExec() {
    super("AT#CCID\r", 90000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      iccid = matcher.group("cid");
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected AThhCCIDExecResult getCommandATResult() {
    return new AThhCCIDExecResult(iccid);
  }

}
