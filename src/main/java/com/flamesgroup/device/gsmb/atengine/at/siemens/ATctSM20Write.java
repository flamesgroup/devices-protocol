/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20.CallMode;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSM20.CmgwMode;

import java.util.Objects;

public final class ATctSM20Write extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final CallMode callMode, final CmgwMode cmgwMode) {
    Objects.requireNonNull(callMode, "callMode mustn't be null");
    Objects.requireNonNull(cmgwMode, "cmgwMode mustn't be null");
    return "AT^SM20=" + callMode.getValue() + ',' + cmgwMode.getValue() + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final CallMode callMode) {
    Objects.requireNonNull(callMode, "callMode mustn't be null");
    return "AT^SM20=" + callMode.getValue() + "\r";
  }

  public ATctSM20Write(final CallMode callMode, final CmgwMode cmgwMode) {
    super(buildCommandATDataWithParamCheck(callMode, cmgwMode));
  }

  public ATctSM20Write(final CallMode callMode) {
    super(buildCommandATDataWithParamCheck(callMode));
  }

}
