/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhRSEN.SAPFormat;

import java.util.Objects;

/**
 * Remote SIM Enable <br>
 * Set command is used to enable/disable the Remote SIM feature.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=635">Paragraph 3.5.7.19.1.</a>
 */
public final class AThhRSENWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final boolean enableRemoteSIM, final SAPFormat sapFormat, final int muxChannelNumber, final int beacon,
      final boolean enableScriptMode) {
    Objects.requireNonNull(sapFormat, "sapFormat mustn't be null");
    Objects.requireNonNull(muxChannelNumber, "muxChannelNumber mustn't be null");
    if (!enableScriptMode) {
      if (muxChannelNumber < 1 || muxChannelNumber > 3) {
        throw new IllegalArgumentException(String.format("Incorrect muxChannelNumber value [%d] - must be in range of 1-3", muxChannelNumber));
      }
    } else {
      if (muxChannelNumber < 1 || muxChannelNumber > 2) {
        throw new IllegalArgumentException(String.format("Incorrect muxChannelNumber value [%d] - must be in range of 1-2 for enabled scriptmode", muxChannelNumber));
      }
    }
    if (beacon < 0 || beacon > 100) {
      throw new IllegalArgumentException(String.format("Incorrect beacon value [%d] - must be in range of 0-100", beacon));
    }

    return "AT#RSEN=" + (enableRemoteSIM ? 1 : 0) + ','
        + sapFormat.getValue() + ",0,"
        + muxChannelNumber + ','
        + beacon + ','
        + (enableScriptMode ? 1 : 0) + "\r";
  }

  /**
   * @param enableRemoteSIM  enable/disable remote SIM
   * @param sapFormat        SAP format
   * @param muxChannelNumber MUX channel number (1-3) or MDM interface number (1-2) in scripts
   *                         (if enableScriptMode is {@code true})
   * @param beacon           retransmition timer of SAP Connection Request
   * @param enableScriptMode enable/disable script mode
   */
  public AThhRSENWrite(final boolean enableRemoteSIM, final SAPFormat sapFormat, final int muxChannelNumber, final int beacon,
      final boolean enableScriptMode) {
    super(buildCommandATDataWithParamCheck(enableRemoteSIM, sapFormat, muxChannelNumber, beacon, enableScriptMode));
  }

  /**
   * @param enableRemoteSIM  enable/disable remote SIM
   * @param muxChannelNumber MUX channel number (1-3)
   */
  public AThhRSENWrite(final boolean enableRemoteSIM, final int muxChannelNumber) {
    this(enableRemoteSIM, SAPFormat.BINARY_SAP, muxChannelNumber, 0, false);
  }

}
