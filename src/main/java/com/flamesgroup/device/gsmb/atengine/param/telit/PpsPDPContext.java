/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public class PpsPDPContext {

  public enum PDPType {

    IP("IP"),
    IPV6("IPV6");

    private final String value;

    PDPType(final String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

  }

  public enum Dcomp {

    OFF(0),// default if value is omitted
    ON(1);

    private final int value;

    Dcomp(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum Hcomp {

    OFF(0),// default if value is omitted
    ON(1);

    private final int value;

    Hcomp(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
