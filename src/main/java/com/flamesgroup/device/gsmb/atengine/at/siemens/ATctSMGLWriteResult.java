/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.SMSInfo;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Result only for PDU mode.
 */
public final class ATctSMGLWriteResult implements IATResult {

  private final List<SMSInfo> smsList;

  public ATctSMGLWriteResult(final List<SMSInfo> smsList) {
    this.smsList = Collections.unmodifiableList(smsList);
  }

  public List<SMSInfo> getSmsList() {
    return smsList;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATctSMGLWriteResult)) {
      return false;
    }
    ATctSMGLWriteResult that = (ATctSMGLWriteResult) object;

    return Objects.equals(smsList, that.getSmsList());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(smsList);
    return result;
  }

}
