/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCREG.Status;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG.URCPresentMode;

import java.util.Objects;

public final class ATpsCREGReadResult implements IATResult {

  private final URCPresentMode urcPresentationMode;
  private final Status stat;
  private final String locationAreaCode;
  private final String cellID;

  public ATpsCREGReadResult(final URCPresentMode urcPresentationMode, final Status stat, final String locationAreaCode, final String cellID) {
    this.urcPresentationMode = urcPresentationMode;
    this.stat = stat;
    this.locationAreaCode = locationAreaCode;
    this.cellID = cellID;
  }

  public URCPresentMode getURCPresentationMode() {
    return urcPresentationMode;
  }

  public Status getStat() {
    return stat;
  }

  public String getLocationAreaCode() {
    return locationAreaCode;
  }

  public String getCellID() {
    return cellID;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCREGReadResult)) {
      return false;
    }
    ATpsCREGReadResult that = (ATpsCREGReadResult) object;

    return Objects.equals(urcPresentationMode, that.getURCPresentationMode())
        && Objects.equals(stat, that.getStat())
        && Objects.equals(locationAreaCode, that.getLocationAreaCode())
        && Objects.equals(cellID, that.getCellID());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(urcPresentationMode);
    result = prime * result + Objects.hashCode(stat);
    result = prime * result + Objects.hashCode(locationAreaCode);
    result = prime * result + Objects.hashCode(cellID);
    return result;
  }

}
