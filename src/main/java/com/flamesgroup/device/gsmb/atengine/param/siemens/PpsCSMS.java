/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.siemens;

public final class PpsCSMS {

  private PpsCSMS() {
  }

  public enum MessagingService {

    COMPATIBLE_WITH_GSM_07_05_PHASE_2_VERSION_4_7_0(0),
    COMPATIBLE_WITH_GSM_07_05_PHASE_2_PLUS_VERSION(1);

    private final int value;

    MessagingService(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static MessagingService getMessagingServiceByValue(final int value) {
      for (MessagingService service : MessagingService.values()) {
        if (service.getValue() == value) {
          return service;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
