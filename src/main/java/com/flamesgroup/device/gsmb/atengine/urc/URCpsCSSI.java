/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import com.flamesgroup.device.gsmb.atengine.param.PpsCSSI;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URCpsCSSI implements IURC {

  private static final String URC_TEMPLATE = "\\+CSSI: (?<code1>[0-356])";
  private static final Pattern urcPattern = Pattern.compile(URC_TEMPLATE);

  private final IURCpsCSSIHandler handler;

  public URCpsCSSI(final IURCpsCSSIHandler handler) {
    Objects.requireNonNull(handler, "handler mustn't be null");
    this.handler = handler;
  }

  @Override
  public boolean process(final String atLexeme) {
    Matcher matcher = urcPattern.matcher(atLexeme);
    if (matcher.matches()) {
      int code1 = Integer.parseInt(matcher.group("code1"));

      handler.handleSupplementaryServiceNotification(PpsCSSI.Code1.getCode1ByValue(code1));
      return true;
    } else {
      return false;
    }
  }

}
