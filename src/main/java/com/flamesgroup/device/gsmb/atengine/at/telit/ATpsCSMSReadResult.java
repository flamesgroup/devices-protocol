/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.BroadcastTypeMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileOriginatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileTerminatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCSMS.MessagingService;

import java.util.Objects;

public final class ATpsCSMSReadResult extends com.flamesgroup.device.gsmb.atengine.at.ATpsCSMSReadResult<MessagingService> {

  public ATpsCSMSReadResult(final MessagingService messagingService, final MobileTerminatedMessages mobileTerminatedMessages,
      final MobileOriginatedMessages mobileOriginatedMessages, final BroadcastTypeMessages broadcastTypeMessages) {
    super(messagingService, mobileTerminatedMessages, mobileOriginatedMessages, broadcastTypeMessages);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(messagingService);
    result = prime * result + Objects.hashCode(mobileTerminatedMessages);
    result = prime * result + Objects.hashCode(mobileOriginatedMessages);
    result = prime * result + Objects.hashCode(broadcastTypeMessages);
    return result;
  }

}
