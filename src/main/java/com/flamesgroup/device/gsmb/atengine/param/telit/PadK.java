/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PadK {

  private PadK() {
  }

  public enum FlowControl {

    NO_FLOW_CONTROL(0),
    HARDWARE_MONO_DIRECTIONAL_FLOW_CONTROL(1),
    SOFTWARE_MONO_DIRECTIONAL_FLOW_CONTROL(2),
    HARDWARE_BI_DIRECTIONAL_FLOW_CONTROL(3),
    SOFTWARE_BI_DIRECTIONAL_WITH_FILTERING(4),
    SOFTWARE_BI_DIRECTIONAL_WITHOUT_FILTERING(5),
    HARDWARE_AND_SOFTWARE_BI_DIRECTIONAL_FLOW_CONTROL(6);

    private final int value;

    FlowControl(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
