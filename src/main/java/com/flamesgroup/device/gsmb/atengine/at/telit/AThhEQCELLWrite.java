/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

public final class AThhEQCELLWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final int arfcn, final int rxLevAdjust) {
    if (!(ATLexemeProcessHelper.isValueInRange(arfcn, 0, 124) || ATLexemeProcessHelper.isValueInRange(arfcn, 975, 1023) || ATLexemeProcessHelper.isValueInRange(arfcn, 512, 885))) {
      throw new IllegalArgumentException("arfcn must be in ranges of (0-124,975-1023,512-885), but it's " + arfcn);
    }
    if (!ATLexemeProcessHelper.isValueInRange(rxLevAdjust, -60, 60)) {
      throw new IllegalArgumentException("rxLevAdjust must be in range of (-60-+60), but it's " + rxLevAdjust);
    }
    return "AT#EQCELL=1," + arfcn + "," + rxLevAdjust + "\r";
  }

  /**
   * Reset all.
   */
  public AThhEQCELLWrite() {
    super("AT#EQCELL=0\r");
  }

  /**
   * Increase/decrease the real value of the cell power.
   *
   * @param arfcn       assigned absolute radio-frequency channel number (0-124,975-1023,512-885)
   * @param rxLevAdjust value on which the original cell power level will be adjusted (-60-+60dBm)
   */
  public AThhEQCELLWrite(final int arfcn, final int rxLevAdjust) {
    super(buildCommandATDataWithParamCheck(arfcn, rxLevAdjust));
  }

}
