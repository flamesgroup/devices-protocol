/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCNMI {

  private PpsCNMI() {
  }

  public enum UrcBufferingOption {

    BUFFER_URCS_IN_TA(0),
    DISCARD_AND_REJECT_NEW_URCS(1),
    BUFFER_URCS_IN_TA_AND_FLUSH_TO_TE(2),
    FORWARD_URCS_DIRECTLY_TO_TE(3);

    private final int value;

    UrcBufferingOption(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static UrcBufferingOption getOptionByValue(final int value) {
      for (UrcBufferingOption option : UrcBufferingOption.values()) {
        if (option.getValue() == value) {
          return option;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum SMSDeliveringOption {

    NO_SMS_DELIVER_ROUTED_TO_TA(0),
    IF_SMS_DELIVER_STORED_IN_META_IOML_ROUTED_TO_TE(1),
    SMS_DELIVERS_ROUTED_DIRECTLY_TO_TE(2),
    CLASS3_SMS_DELIEVERS_ROUTED_DIRECTLY_TO_TE(3);

    private final int value;

    SMSDeliveringOption(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static SMSDeliveringOption getOptionByValue(final int value) {
      for (SMSDeliveringOption option : SMSDeliveringOption.values()) {
        if (option.getValue() == value) {
          return option;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum BroadcastReportingOption {

    NO_CBM_INDICATIONS_ROUTED_TO_TE(0),
    NEW_CBMS_ROUTED_DIRECTLY_TO_TE(2),
    /**
     * only for siemens
     */
    CLASS3_CBMS_ROUTED_DIRECTLY_TO_TE(3);

    private final int value;

    BroadcastReportingOption(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static BroadcastReportingOption getOptionByValue(final int value) {
      for (BroadcastReportingOption option : BroadcastReportingOption.values()) {
        if (option.getValue() == value) {
          return option;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum SMSStatusReportingOption {

    NO_SMS_STATUS_REPORT_ROUTED_TO_TE(0),
    SMS_STATUS_REPORT_ROUTED_TO_TE(1),
    IF_SMS_STATUS_REPORT_ROUTED_INTO_META_IOML_ROUTED_TO_TE(2);

    private final int value;

    SMSStatusReportingOption(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static SMSStatusReportingOption getOptionByValue(final int value) {
      for (SMSStatusReportingOption option : SMSStatusReportingOption.values()) {
        if (option.getValue() == value) {
          return option;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum BufferedURCHandlingMethod {

    FLUSH_TO_TE(0),
    CLEAR(1);

    private final int value;

    BufferedURCHandlingMethod(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static BufferedURCHandlingMethod getOptionByValue(final int value) {
      for (BufferedURCHandlingMethod method : BufferedURCHandlingMethod.values()) {
        if (method.getValue() == value) {
          return method;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
