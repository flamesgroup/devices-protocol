/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;
import com.flamesgroup.device.gsmb.atengine.ATEngineTimeoutException;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AT<TATResult extends IATResult> implements IAT<TATResult> {

  public static final char CTRL_Z = 0x1A;
  public static final char ESC = 0x1B;

  private static final String AT_OK_STATUS_TEMPLATE = "OK";
  private static final Pattern atOkStatusPattern = Pattern.compile(AT_OK_STATUS_TEMPLATE);

  private static final String AT_ERROR_STATUS_TEMPLATE = "ERROR|\\+CM[ES] ERROR:.++";
  private static final Pattern atErrorStatusPattern = Pattern.compile(AT_ERROR_STATUS_TEMPLATE);

  protected final Lock commandLock = new ReentrantLock();
  protected final Condition commandCondition = commandLock.newCondition();

  private final long timeout;

  private boolean executed;
  private ATErrorStatusException exception;

  public AT(final long timeout) {
    this.timeout = timeout;
  }

  public AT() {
    this(2000);
  }

  @Override
  public boolean process(final String atLexeme) {
    commandLock.lock();
    try {
      if (executed) {
        return false;
      }
      if (internalProcessATEcho(atLexeme)) {
        return true;
      }
      if (internalProcessATOKStatus(atLexeme)) {
        commandCondition.signalAll();
        return true;
      }
      if (internalProcessATErrorStatus(atLexeme)) {
        commandCondition.signalAll();
        return true;
      }
      return internalProcessATResponse(atLexeme);
    } finally {
      commandLock.unlock();
    }
  }

  @Override
  public TATResult execute(final IATSubChannel atSubChannel) throws ChannelException, InterruptedException {
    commandLock.lock();
    try {
      if (executed) {
        throw new IllegalStateException("Each AT instance must be executed only once - this is some implementation issue!");
      }

      atSubChannel.writeATData(getCommandATData());
      if (!commandCondition.await(timeout, TimeUnit.MILLISECONDS)) {
        throw new ATEngineTimeoutException(String.format("[%s] - timeout for waiting response elapsed", this));
      }

      ATErrorStatusException exception = pollCommandATErrorStatusException();
      if (exception != null) {
        throw exception;
      }

      executed = true;

      TATResult result = getCommandATResult();
      if (result == null) {
        throw new IllegalStateException("AT result can't be null on success AT command execution - this is some implementation issue!");
      }
      return result;
    } finally {
      commandLock.unlock();
    }
  }

  @Override
  public boolean isWaitBeforeNextAT() {
    return true;
  }

  protected boolean internalProcessATEcho(final String lexeme) {
    return lexeme.equals(getCommandATData());
  }

  protected boolean internalProcessATOKStatus(final String lexeme) {
    Matcher matcher = atOkStatusPattern.matcher(lexeme);
    return matcher.matches();
  }

  protected boolean internalProcessATErrorStatus(final String lexeme) {
    Matcher matcher = atErrorStatusPattern.matcher(lexeme);
    if (matcher.matches()) {
      setCommandATErrorStatusException(new ATErrorStatusException(getCommandATData(), matcher.group()));
      return true;
    }
    return false;
  }

  protected void setCommandATErrorStatusException(final ATErrorStatusException exception) {
    this.exception = exception;
  }

  protected ATErrorStatusException pollCommandATErrorStatusException() {
    try {
      return exception;
    } finally {
      exception = null;
    }
  }

  protected abstract boolean internalProcessATResponse(String lexeme);

  protected abstract String getCommandATData();

  protected abstract TATResult getCommandATResult();

}
