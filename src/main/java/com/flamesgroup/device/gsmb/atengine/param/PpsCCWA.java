/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCCWA {

  private PpsCCWA() {
  }

  public enum Mode {

    DISABLE_CALL_WAITING(0),
    ENABLE_CALL_WAITING(1),
    QUERY_STATUS_OF_CALL_WAITING(2);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Class {

    VOICE(1),
    DATA(2),
    FAX(4),
    SMS(8),
    DATA_CIRCUIT_SYNC(16),
    DATA_CIRCUIT_ASYNC(32),
    DEDICATED_PACKET_ACCESS(64),
    DEDICATED_PAD_ACCESS(128);

    private final int value;

    Class(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Class getClassByValue(final int value) {
      for (Class c : Class.values()) {
        if (c.getValue() == value) {
          return c;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
