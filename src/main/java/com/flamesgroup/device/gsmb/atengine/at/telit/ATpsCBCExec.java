/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCBC.BatteryChargeLevel;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCBC.BatteryStatus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Battery Charge <br>
 * Execution command returns the current Battery Charge status
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=200">Paragraph 3.5.4.8.1.</a>
 */
public final class ATpsCBCExec extends SolidAT<ATpsCBCExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CBC: (?<bcs>[0-3]),(?<bcl>0|25|50|75|100)";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private BatteryStatus batteryStatus;
  private BatteryChargeLevel batteryChargeLevel;

  public ATpsCBCExec() {
    super("AT+CBC\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      batteryStatus = BatteryStatus.getBatteryStatusByValue(Integer.parseInt(matcher.group("bcs")));
      batteryChargeLevel = BatteryChargeLevel.getBatteryChargeLevelByValue(Integer.parseInt(matcher.group("bcl")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCBCExecResult getCommandATResult() {
    return new ATpsCBCExecResult(batteryStatus, batteryChargeLevel);
  }

}
