/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCLCC {

  private PpsCLCC() {
  }

  public enum CallState {

    ACTIVE(0),
    HELD(1),
    DIALING(2),
    ALERTING(3),
    INCOMING(4),
    WAITING(5);

    int value;

    CallState(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static CallState getStateByValue(final int value) {
      for (CallState state : CallState.values()) {
        if (state.getValue() == value) {
          return state;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Mode {

    VOICE(0),
    DATA(1),
    FAX(2),
    VOICE_FOLLOWED_BY_DATA_VOICE_MODE(3),
    ALTERNATING_VOICE_DATA_VOICE_MODE(4),
    ALTERNATING_VOICE_FAX_VOICE_MODE(5),
    VOICE_FOLLOWED_BY_DATA_DATA_MODE(6),
    ALTERNATING_VOICE_DATA_DATA_MODE(7),
    ALTERNATING_VOICE_FAX_FAX_MODE(8),
    UNKNOWN(9);

    int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  /*
   The Type-of-Address octet indicates the format of a phone number.
   The Type-of-Address octet is 8bit data.
   Note that bit no 7 should always be set to 1
   Bits 6, 5 and 4 denote the Type-of-number
   Bits 3, 2, 1, 0 denote the Numbering-Plan-Identification

   In current implementation only Type of number parsed.
   */
  public static final class TypeOfAddressOctet {

    private final int value;

    private TypeOfAddressOctet(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public TypeOfNumber getTypeOfNumber() {
      return TypeOfNumber.getTypeOfNumberFromTypeOfAddressOctet(value);
    }

    public NumberingPlanIdentification getNumberingPlanIdentification() {
      return NumberingPlanIdentification.getNumberingPlanIdentificationFromTypeOfAddressOctet(value);
    }

    @Override
    public boolean equals(final Object object) {
      if (object == this) {
        return true;
      }
      if (!(object instanceof TypeOfAddressOctet)) {
        return false;
      }
      TypeOfAddressOctet that = (TypeOfAddressOctet) object;

      return value == that.getValue();
    }

    @Override
    public int hashCode() {
      return value;
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) + ":[V=" + value + "]";
    }

    public static TypeOfAddressOctet getTypeOfAddressOctetByValue(final int value) {
      if (value > 0xFF || (value & 0b10000000) == 0) {
        throw new IllegalArgumentException("Incorrect value: " + value);
      }
      return new TypeOfAddressOctet(value);
    }

    public enum TypeOfNumber {

      UNKNOWN(0b000),
      INTERNATIONAL_NUMBER(0b001),
      NATIONAL_NUMBER(0b010),
      NETWORK_SPECIFIC_NUMBER(0b011),
      SUBSCRIBER_NUMBER(0b100),
      ALPHANUMERIC(0b101),
      ABBREVIATED_NUMBER(0b110),
      RESERVED_FOR_EXTENSION(0b111);

      int value;

      TypeOfNumber(final int value) {
        this.value = value;
      }

      public int getValue() {
        return value;
      }

      public static TypeOfNumber getTypeOfNumberFromTypeOfAddressOctet(final int value) {
        int typeOfNumberValue = (value >> 4) & 0b111;
        for (TypeOfNumber typeOfNumber : TypeOfNumber.values()) {
          if (typeOfNumber.getValue() == typeOfNumberValue) {
            return typeOfNumber;
          }
        }
        throw new IllegalArgumentException("Unsupported value: " + value);
      }

    }

    public enum NumberingPlanIdentification {

      UNKNOWN(0b0000),
      TELEPHONE_NUMBERING_PLAN(0b0001),
      DATA_NUMBERING_PLAN(0b0011),
      TELEX_NUMBERING_PLAN(0b0100),
      NATIONAL_NUMBERING_PLAN(0b1000),
      PRIVATE_NUMBERING_PLAN(0b1001),
      ERMES_NUMBERING_PLAN(0b1010),
      RESERVED_FOR_EXTENSION(0b1111);

      int value;

      NumberingPlanIdentification(final int value) {
        this.value = value;
      }

      public int getValue() {
        return value;
      }

      public static NumberingPlanIdentification getNumberingPlanIdentificationFromTypeOfAddressOctet(final int value) {
        int numberingPlanIdentificationValue = value & 0b1111;
        for (NumberingPlanIdentification numberingPlanIdentification : NumberingPlanIdentification.values()) {
          if (numberingPlanIdentification.getValue() == numberingPlanIdentificationValue) {
            return numberingPlanIdentification;
          }
        }
        throw new IllegalArgumentException("Unsupported value: " + value);
      }

    }

  }

}
