/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCMGF.SMSFormat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Message Format <br>
 * Read command reports the current value of the parameter
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=207">Paragraph 3.5.5.1.3.</a>
 */
public final class ATpsCMGFRead extends SolidAT<ATpsCMGFReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CMGF: (?<mode>[01])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private SMSFormat smsFormat;

  public ATpsCMGFRead() {
    super("AT+CMGF?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      smsFormat = SMSFormat.getFormatByValue(Integer.parseInt(matcher.group("mode")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCMGFReadResult getCommandATResult() {
    return new ATpsCMGFReadResult(smsFormat);
  }

}
