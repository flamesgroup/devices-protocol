/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper.isValueInRange;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsPDPContext;

import java.util.Objects;

/**
 * Define PDP Context <br>
 * Set command specifies PDP context parameter values for a PDP context identified by the (local) context identification parameter.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=189">Paragraph 3.5.4.7.5.</a>
 */
public class ATpsCGDCONTWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final int cid, final PpsPDPContext.PDPType pdpType, final String apn, final String pdpAddr, final PpsPDPContext.Dcomp dcomp,
      final PpsPDPContext.Hcomp hcomp) {
    if (!isValueInRange(cid, 1, 5)) {
      throw new IllegalArgumentException("Incorrect cid value must be form 1 to 5");
    }
    Objects.requireNonNull(pdpType, "pdpType mustn't be null");
    Objects.requireNonNull(apn, "apn mustn't be null");
    Objects.requireNonNull(pdpAddr, "pdpAddr mustn't be null");
    Objects.requireNonNull(dcomp, "dcomp mustn't be null");
    Objects.requireNonNull(hcomp, "hcomp mustn't be null");
    return "AT+CGDCONT=" + cid + ",\"" + pdpType.getValue() + "\",\"" + apn + "\",\"" + pdpAddr + "\"," + dcomp.getValue() + "," + hcomp.getValue() + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final int cid, final PpsPDPContext.PDPType pdpType, final String apn) {
    if (!isValueInRange(cid, 1, 5)) {
      throw new IllegalArgumentException("Incorrect cid value must be form 1 to 5");
    }
    Objects.requireNonNull(pdpType, "pdpType mustn't be null");
    Objects.requireNonNull(apn, "apn mustn't be null");
    return "AT+CGDCONT=" + cid + ",\"" + pdpType.getValue() + "\",\"" + apn + "\"\r";
  }

  public ATpsCGDCONTWrite(final int cid, final PpsPDPContext.PDPType pdpType, final String apn, final String pdpAddr, final PpsPDPContext.Dcomp dcomp, final PpsPDPContext.Hcomp hcomp) {
    super(buildCommandATDataWithParamCheck(cid, pdpType, apn, pdpAddr, dcomp, hcomp));
  }

  public ATpsCGDCONTWrite(final int cid, final PpsPDPContext.PDPType pdpType, final String apn) {
    super(buildCommandATDataWithParamCheck(cid, pdpType, apn));
  }

}
