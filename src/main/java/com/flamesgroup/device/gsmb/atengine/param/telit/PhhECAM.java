/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhECAM {

  private PhhECAM() {
  }

  public enum OnOff {

    DISABLE(0),
    ENABLE(1),
    ENABLE_WITH_CALLING_NUMBER(2);

    private final int value;

    OnOff(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum CallStatus {

    IDLE(0),
    CALLING(1), //MO
    CONNECTING(2), //MO
    ACTIVE(3),
    HOLD(4),
    WAITING(5), //MT
    ALERTING(6), //MT
    BUSY(7);

    private final int value;

    CallStatus(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static CallStatus getCallStatusByValue(final int value) {
      for (CallStatus status : CallStatus.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum CallType {

    VOICE(1),
    DATA(2);

    private final int value;

    CallType(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static CallType getCallTypeByValue(final int value) {
      for (CallType type : CallType.values()) {
        if (type.getValue() == value) {
          return type;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum TypeOfNumber {

    NATIONAL_NUMBER(129),
    INTERNATIONAL_NUMBER(145);

    private final int value;

    TypeOfNumber(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static TypeOfNumber getTypeOfNumberByValue(final int value) {
      for (TypeOfNumber type : TypeOfNumber.values()) {
        if (type.getValue() == value) {
          return type;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
