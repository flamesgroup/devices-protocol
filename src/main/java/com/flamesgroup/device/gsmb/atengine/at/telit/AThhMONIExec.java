/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Obtain GSM-related information from the whole set of seven cells in the
 * neighbour list of the serving cell (AT#MONI=7).
 */
public final class AThhMONIExec extends SolidAT<AThhMONIExecResult> {

  private static final String AT_RESPONSE_HEADER_TEMPLATE = "#MONI:[ ]*Cell[\\s]*BSIC[ ]*LAC[ ]*CellId[ ]*ARFCN[ ]*Power[ ]*C1[ ]*C2[ ]*TA[ ]*RxQual[ ]*PLMN";
  private static final String AT_RESPONSE_TEMPLATE =
      "#MONI:[ ]*(?<cell>[\\S]+)[ ]*(?<bsic>[0-7]{2}|FF)[ ]*(?<lac>[A-F_0-9]{4})[ ]*(?<cellid>[A-F_0-9]{4})[ ]*(?<arfcn>\\d+)[ ]*(?<power>-\\d+)dbm[ ]*(?<c1>-?\\d+)[ ]*(?<c2>-?\\d+)[ ]*(?:(?<ta>\\d+)[ ]*(?<rxqual>\\d+)[ ]*(?<plmn>.+?))?";

  private static final Pattern atResponseHeaderPattern = Pattern.compile(AT_RESPONSE_HEADER_TEMPLATE);
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private final List<AThhMONIExecResult.CellInfo> cellList = new ArrayList<>();
  private boolean processAnswerHeader = true;

  public AThhMONIExec() {
    super("AT#MONI\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    if (processAnswerHeader) {
      Matcher matcher = atResponseHeaderPattern.matcher(lexeme);
      if (matcher.matches()) {
        processAnswerHeader = false;
        return true;
      }
    } else {
      Matcher matcher = atResponsePattern.matcher(lexeme);
      if (matcher.matches()) {
        String cell = matcher.group("cell");
        int bsic = bsic2int(matcher.group("bsic"));
        int lac = lac2int(matcher.group("lac"));
        int cellId = cellid2int(matcher.group("cellid"));
        int arfcn = Integer.parseInt(matcher.group("arfcn"));
        int power = Integer.parseInt(matcher.group("power"));
        int c1 = Integer.parseInt(matcher.group("c1"));
        int c2 = Integer.parseInt(matcher.group("c2"));

        int ta = -1, rxqual = -1;
        String taStr = matcher.group("ta");
        if (taStr != null) {
          ta = Integer.parseInt(taStr);
        }
        String rxqualStr = matcher.group("rxqual");
        if (rxqualStr != null) {
          rxqual = Integer.parseInt(rxqualStr);
        }
        String plmn = matcher.group("plmn");

        cellList.add(new AThhMONIExecResult.CellInfo(cell, bsic, lac, cellId, arfcn, power, c1, c2, ta, rxqual, plmn));
        return true;
      }
    }
    return false;
  }

  @Override
  protected AThhMONIExecResult getCommandATResult() {
    return new AThhMONIExecResult(cellList);
  }

  private int bsic2int(final String bsicStr) {
    if (bsicStr.equals("FF")) {
      return 0;
    } else {
      return Integer.parseInt(bsicStr, 8);
    }
  }

  private int lac2int(final String lacStr) {
    if (lacStr.equals("FFFF")) {
      return 0;
    } else {
      return Integer.parseInt(lacStr, 16);
    }
  }

  private int cellid2int(final String cellidStr) {
    return Integer.parseInt(cellidStr, 16);
  }

}
