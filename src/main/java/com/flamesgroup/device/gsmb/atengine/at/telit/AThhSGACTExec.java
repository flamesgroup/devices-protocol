/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Context Activation<br>
 * Execution command is used to activate or deactivate either the GSM context or the specified PDP context.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=189">Paragraph 3.5.7.7.3.</a>
 */
public class AThhSGACTExec extends SolidAT<AThhSGACTExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "#SGACT: (?<ipAddress>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private String ipAddress;

  private static String buildCommandATDataWithParamCheck(final int cid, final int stat, final String userId, final String pwd) {
    if (!ATLexemeProcessHelper.isValueInRange(cid, 0, 5)) {
      throw new IllegalArgumentException(String.format("Incorrect cid value [%d] - must be in range of 0-5", cid));
    }
    if (!ATLexemeProcessHelper.isValueInRange(stat, 0, 1)) {
      throw new IllegalArgumentException(String.format("Incorrect stat value [%d] - must be in range of 0-1", stat));
    }
    if (userId != null && pwd != null) {
      return "AT#SGACT=" + cid + "," + stat + ",\"" + userId + "\",\"" + pwd + "\"\r";
    } else {
      return "AT#SGACT=" + cid + "," + stat + "\r";
    }
  }

  public AThhSGACTExec(final int cid, final int stat, final String userId, final String pwd) {
    super(buildCommandATDataWithParamCheck(cid, stat, userId, pwd), 150_000);
  }

  public AThhSGACTExec(final int cid, final int stat) {
    super(buildCommandATDataWithParamCheck(cid, stat, null, null), 150_000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      ipAddress = matcher.group("ipAddress");
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected AThhSGACTExecResult getCommandATResult() {
    return new AThhSGACTExecResult(ipAddress);
  }

}
