/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PpsCMUX {

  private PpsCMUX() {
  }

  public enum PortSpeed {

    BPS_19200(2),
    BPS_38400(3),
    BPS_57600(4),
    BPS_115200(5);

    private final int value;

    PortSpeed(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
