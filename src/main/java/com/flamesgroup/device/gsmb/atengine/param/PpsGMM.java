/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsGMM {

  private PpsGMM() {
  }

  public enum Model {

    TC35i("TC35i"),
    MC55i("MC55i"),
    GL865_QUAD("GL865-QUAD");

    private final String value;

    Model(final String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    public static Model getModelByName(final String value) {
      for (Model model : Model.values()) {
        if (model.getValue().equals(value)) {
          return model;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
