/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.http;

import java.io.Serializable;
import java.util.Objects;

public class HTTPResponse implements Serializable {

  private static final long serialVersionUID = -6119694463097617503L;

  private final String data;
  private final int httpStatusCode;
  private final String contentType;
  private final int dataSize;

  public HTTPResponse(final String data, final int httpStatusCode, final String contentType, final int dataSize) {
    this.data = data;
    this.httpStatusCode = httpStatusCode;
    this.contentType = contentType;
    this.dataSize = dataSize;
  }

  public String getData() {
    return data;
  }

  public int getHttpStatusCode() {
    return httpStatusCode;
  }

  public String getContentType() {
    return contentType;
  }

  public int getDataSize() {
    return dataSize;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof HTTPResponse))
      return false;
    HTTPResponse that = (HTTPResponse) o;
    return httpStatusCode == that.httpStatusCode &&
        Objects.equals(data, that.data) &&
        Objects.equals(contentType, that.contentType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, httpStatusCode, contentType);
  }

}
