/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.ATEngineException;
import com.flamesgroup.device.gsmb.atengine.at.ATErrorStatusException;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCLIP;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCLIR;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCallBarring;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCallForwarding;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCallWaiting;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshIMEI;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIP;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIR;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseUSSDSessionStartHashWrapper implements IUSSDSession {

  private static final String STARHASH_PREFIX_TEMPLATE = "^(?<prefix>[*#]){1,2}";
  private static final Pattern starhashPrefixPattern = Pattern.compile(STARHASH_PREFIX_TEMPLATE);

  private static final String CME_ERROR_PREFIX_TEMPLATE = "^\\+CME ERROR: ";

  private final BaseUSSDSession wrappedUSSDSession;
  private String starHashResult;

  public BaseUSSDSessionStartHashWrapper(final BaseUSSDSession wrappedUSSDSession) {
    this.wrappedUSSDSession = wrappedUSSDSession;
  }

  protected <T extends IATResult> T executeWithinWrappedUSSDSession(final IAT<T> command) throws ChannelException, InterruptedException {
    return wrappedUSSDSession.executeWithinUSSDSession(command);
  }

  @Override
  public void open(final String ussdString) throws ChannelException, InterruptedException {
    Matcher matcher = starhashPrefixPattern.matcher(ussdString);
    if (matcher.find()) {
      if (ATshIMEI.isIMEICode(ussdString)) {
        starHashResult = processIMEI();
        return;
      } else if (ATshCLIP.isCLIPCode(ussdString)) {
        starHashResult = processCLIP();
        return;
      } else if (ATshCLIR.isCLIRCode(ussdString)) {
        starHashResult = processCLIR();
        return;
      } else if (ATshCallForwarding.isCallForwardingCode(ussdString)) {
        starHashResult = processCallForwarding(ussdString, matcher.group("prefix"));
        return;
      } else if (ATshCallWaiting.isCallWaitingCode(ussdString)) {
        starHashResult = processCallWaiting(ussdString, matcher.group("prefix"));
        return;
      } else if (ATshCallBarring.isCallBarringCode(ussdString)) {
        starHashResult = processCallBarring(ussdString, matcher.group("prefix"));
        return;
      }
    }

    wrappedUSSDSession.open(ussdString);
  }

  @Override
  public void close() throws ChannelException, InterruptedException {
    if (starHashResult == null) {
      wrappedUSSDSession.close();
    } else {
      starHashResult = null;
    }
  }

  @Override
  public String readRequest() throws ATEngineException, InterruptedException {
    if (starHashResult == null) {
      return wrappedUSSDSession.readRequest();
    } else {
      return starHashResult;
    }
  }

  @Override
  public void writeResponse(final String response) throws ChannelException, InterruptedException {
    if (starHashResult == null) {
      wrappedUSSDSession.writeResponse(response);
    }
  }

  protected final String getCMEErrorMessage(final String cmeErrorString) {
    return cmeErrorString.replaceFirst(CME_ERROR_PREFIX_TEMPLATE, "");
  }

  private String processIMEI() throws ChannelException, InterruptedException {
    try {
      return executeIMEICommand();
    } catch (ATErrorStatusException e) {
      return "IMEI: " + getCMEErrorMessage(e.getStatus());
    }
  }

  private String processCLIP() throws ChannelException, InterruptedException {
    try {
      PshCLIP.Status status = executeCLIPCommand();
      switch (status) {
        case NOT_PROVISIONED:
          return "Incoming caller ID service is disabled";
        case PROVISIONED:
          return "Incoming caller ID service is enabled";
        case UNKNOWN:
          return "Incoming caller ID service is unknown";
        default:
          throw new AssertionError(); // we must not be here
      }
    } catch (ATErrorStatusException e) {
      return "CLIP: " + getCMEErrorMessage(e.getStatus());
    }
  }

  private String processCLIR() throws ChannelException, InterruptedException {
    try {
      PshCLIR.StatusOnNetwork status = executeCLIRCommand();
      switch (status) {
        case NOT_PROVISIONED:
          return "Outgoing caller ID service set to not provisioned";
        case PROVISIONED_PERMANENTLY:
          return "Outgoing caller ID service set to provisioned permanently";
        case UNKNOWN:
          return "Outgoing caller ID service is unknown";
        case TEMPORARY_MODE_PRESENTATION_RESTRICTED:
          return "Outgoing caller ID service set to presentation restricted";
        case TEMPORARY_MODE_PRESENTATION_ALLOWED:
          return "Outgoing caller ID service set to presentation allowed";
        default:
          throw new AssertionError(); // we must not be here
      }
    } catch (ATErrorStatusException e) {
      return "CLIR: " + getCMEErrorMessage(e.getStatus());
    }
  }

  private String processCallForwarding(final String code, final String prefix) throws ChannelException, InterruptedException {
    try {
      Map<ServiceClass, Boolean> serviceClasses = executeCallForwardingCommand(code);
      switch (prefix) {
        case "*":
          return "Call forwarding enabled";
        case "**":
          return "Call forwarding registered and enabled";
        case "#":
          return "Call forwarding disabled";
        case "##":
          return "Call forwarding unregistered and disabled";
        case "*#":
          Set<ServiceClass> activeServiceClasses = new HashSet<>();
          for (Map.Entry<ServiceClass, Boolean> entry : serviceClasses.entrySet()) {
            if (entry.getValue()) {
              activeServiceClasses.add(entry.getKey());
            }
          }

          if (activeServiceClasses.isEmpty()) {
            return "Call forwarding not enabled for all";
          } else {
            return "Call forwarding enabled for " + activeServiceClasses;
          }
        default:
          throw new AssertionError(); // we must not be here
      }
    } catch (ATErrorStatusException e) {
      return "Call forwarding service: " + getCMEErrorMessage(e.getStatus());
    }
  }

  private String processCallWaiting(final String code, final String prefix) throws ChannelException, InterruptedException {
    try {
      Map<ServiceClass, Boolean> serviceClasses = executeCallWaitingCommand(code);
      switch (prefix) {
        case "*":
          return "Call waiting enabled";
        case "#":
          return "Call waiting disabled";
        case "*#":
          Set<ServiceClass> activeServiceClasses = new HashSet<>();
          for (Map.Entry<ServiceClass, Boolean> entry : serviceClasses.entrySet()) {
            if (entry.getValue()) {
              activeServiceClasses.add(entry.getKey());
            }
          }

          if (activeServiceClasses.isEmpty()) {
            return "Call waiting not enabled for all";
          } else {
            return "Call waiting enabled for " + activeServiceClasses;
          }
        default:
          throw new AssertionError(); // we must not be here
      }
    } catch (ATErrorStatusException e) {
      return "Call waiting service: " + getCMEErrorMessage(e.getStatus());
    }
  }

  private String processCallBarring(final String code, final String prefix) throws ChannelException, InterruptedException {
    try {
      Map<ServiceClass, Boolean> serviceClasses = executeCallBarringCommand(code);
      switch (prefix) {
        case "*":
          return "Call barring enabled";
        case "#":
          return "Call barring disabled";
        case "*#":
          Set<ServiceClass> activeServiceClasses = new HashSet<>();
          for (Map.Entry<ServiceClass, Boolean> entry : serviceClasses.entrySet()) {
            if (entry.getValue()) {
              activeServiceClasses.add(entry.getKey());
            }
          }

          if (activeServiceClasses.isEmpty()) {
            return "Call barring not enabled for all";
          } else {
            return "Call barring enabled for " + activeServiceClasses;
          }
        default:
          return "";
      }
    } catch (ATErrorStatusException e) {
      return "Call barring service: " + getCMEErrorMessage(e.getStatus());
    }
  }

  protected abstract String executeIMEICommand() throws ChannelException, InterruptedException;

  protected abstract PshCLIP.Status executeCLIPCommand() throws ChannelException, InterruptedException;

  protected abstract PshCLIR.StatusOnNetwork executeCLIRCommand() throws ChannelException, InterruptedException;

  protected abstract Map<ServiceClass, Boolean> executeCallForwardingCommand(String code) throws ChannelException, InterruptedException;

  protected abstract Map<ServiceClass, Boolean> executeCallWaitingCommand(String code) throws ChannelException, InterruptedException;

  protected abstract Map<ServiceClass, Boolean> executeCallBarringCommand(String code) throws ChannelException, InterruptedException;

}
