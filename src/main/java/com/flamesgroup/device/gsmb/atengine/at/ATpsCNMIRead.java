/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BroadcastReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BufferedURCHandlingMethod;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSDeliveringOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSStatusReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.UrcBufferingOption;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * New Message Indications <br>
 * Read command returns the current parameter settings for +CNMI command
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=218">Paragraph 3.5.5.3.1.</a>
 */
public final class ATpsCNMIRead extends SolidAT<ATpsCNMIReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CNMI: (?<mode>[0-3]),(?<mt>[0-3]),(?<bm>[023]),(?<ds>[0-2]),(?<brf>[01])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private UrcBufferingOption urcBufferingOption;
  private SMSDeliveringOption smsDeliveringOption;
  private BroadcastReportingOption broadcastReportingOption;
  private SMSStatusReportingOption smsStatusReportingOption;
  private BufferedURCHandlingMethod bufferedURCHandlingMethod;

  public ATpsCNMIRead() {
    super("AT+CNMI?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      urcBufferingOption = UrcBufferingOption.getOptionByValue(Integer.parseInt(matcher.group("mode")));
      smsDeliveringOption = SMSDeliveringOption.getOptionByValue(Integer.parseInt(matcher.group("mt")));
      broadcastReportingOption = BroadcastReportingOption.getOptionByValue(Integer.parseInt(matcher.group("bm")));
      smsStatusReportingOption = SMSStatusReportingOption.getOptionByValue(Integer.parseInt(matcher.group("ds")));
      bufferedURCHandlingMethod = BufferedURCHandlingMethod.getOptionByValue(Integer.parseInt(matcher.group("brf")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCNMIReadResult getCommandATResult() {
    return new ATpsCNMIReadResult(urcBufferingOption, smsDeliveringOption, broadcastReportingOption, smsStatusReportingOption, bufferedURCHandlingMethod);
  }

}
