/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class AThhEQCELLReadResult implements IATResult {

  private final List<EqCellInfo> eqCellList;

  public AThhEQCELLReadResult(final List<EqCellInfo> eqCellList) {
    this.eqCellList = Collections.unmodifiableList(eqCellList);
  }

  public List<EqCellInfo> getEqCellList() {
    return eqCellList;
  }

  public static class EqCellInfo {

    private final int arfcn;
    private final int rxLevAdjust;

    public EqCellInfo(final int arfcn, final int rxLevAdjust) {
      this.arfcn = arfcn;
      this.rxLevAdjust = rxLevAdjust;
    }

    public int getArfcn() {
      return arfcn;
    }

    public int getRxLevAdjust() {
      return rxLevAdjust;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      EqCellInfo that = (EqCellInfo) o;
      return arfcn == that.arfcn &&
          rxLevAdjust == that.rxLevAdjust;
    }

    @Override
    public int hashCode() {
      return Objects.hash(arfcn, rxLevAdjust);
    }

  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AThhEQCELLReadResult that = (AThhEQCELLReadResult) o;
    return Objects.equals(eqCellList, that.eqCellList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eqCellList);
  }

}
