/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhTSVOL.ClassOfTones;

import java.util.EnumSet;
import java.util.Objects;

/**
 * Tone Classes Volume <br>
 * Set command is used to select the volume mode for one or more tone classes.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=650">Paragraph 3.5.7.21.2.4.</a>
 */
public final class AThhTSVOLWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final EnumSet<ClassOfTones> classOfTones, final boolean useVolume, final int volume) {
    Objects.requireNonNull(classOfTones, "classOfTones mustn't be null");
    if (classOfTones.isEmpty()) {
      throw new IllegalArgumentException("classOfTones mustn't be empty");
    }
    StringBuilder commandATDataLocal = new StringBuilder("AT#TSVOL=");
    int sum = 0;
    for (ClassOfTones c : classOfTones) {
      sum += c.getValue();
    }
    commandATDataLocal.append(sum);
    commandATDataLocal.append(',').append(useVolume ? 1 : 0);
    if (useVolume) {
      if (volume < 0) {
        throw new IllegalArgumentException(String.format("Incorrect volume value [%s] - must be greater or equal 0", volume));
      }
      commandATDataLocal.append(',').append(volume);
    }

    return commandATDataLocal.append("\r").toString();
  }

  public AThhTSVOLWrite(final EnumSet<ClassOfTones> classOfTones, final boolean useVolume, final int volume) {
    super(buildCommandATDataWithParamCheck(classOfTones, useVolume, volume));
  }

  public AThhTSVOLWrite(final EnumSet<ClassOfTones> classOfTones) {
    this(classOfTones, false, 0);
  }

}
