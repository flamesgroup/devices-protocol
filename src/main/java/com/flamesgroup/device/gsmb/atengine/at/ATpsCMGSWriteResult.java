/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import java.util.Objects;

public final class ATpsCMGSWriteResult implements IATResult {

  private int messageReference = -1;
  private String serviceCentreTimeStamp;
  private String ackPDU;

  public ATpsCMGSWriteResult(final int messageReference, final String serviceCentreTimeStampOrAckPDU, final boolean isTextMode) {
    this.messageReference = messageReference;
    if (isTextMode) {
      this.serviceCentreTimeStamp = serviceCentreTimeStampOrAckPDU;
    } else {
      ackPDU = serviceCentreTimeStampOrAckPDU;
    }
  }

  public int getMessageReference() {
    return messageReference;
  }

  public String getServiceCentreTimeStamp() {
    return serviceCentreTimeStamp;
  }

  public String getAckPDU() {
    return ackPDU;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCMGSWriteResult)) {
      return false;
    }
    ATpsCMGSWriteResult that = (ATpsCMGSWriteResult) object;

    return messageReference == that.getMessageReference()
        && Objects.equals(serviceCentreTimeStamp, that.getServiceCentreTimeStamp())
        && Objects.equals(ackPDU, that.getServiceCentreTimeStamp());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + messageReference;
    result = prime * result + Objects.hashCode(serviceCentreTimeStamp);
    result = prime * result + Objects.hashCode(ackPDU);
    return result;
  }

}
