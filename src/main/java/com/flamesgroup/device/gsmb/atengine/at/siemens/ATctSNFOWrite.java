/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

public final class ATctSNFOWrite extends EmptyResponseSolidAT {

  private static void checkValidCalibrates(final int from, final int to, final int[] values) {
    for (int i = 0; i < values.length; i++) {
      if (values[i] < from || values[i] > to) {
        throw new IllegalArgumentException(String.format("Incorrect outCalibrate[%d] value [%d] - must be in range of 0-32767", i, values[i]));
      }
    }
  }

  private static String buildCommandATDataWithParamCheck(final int outBbcGain, final int outCalibrate0, final int outCalibrate1, final int outCalibrate2,
      final int outCalibrate3, final int outCalibrate4, final int outStep, final int sideTone) {
    if (!ATLexemeProcessHelper.isValueInRange(outBbcGain, 0, 3)) {
      throw new IllegalArgumentException(String.format("Incorrect outBbcGain value [%d] - must be in range of 0-3", outBbcGain));
    }
    checkValidCalibrates(0, 32767, new int[] {outCalibrate0, outCalibrate1, outCalibrate2, outCalibrate3, outCalibrate4});
    if (!ATLexemeProcessHelper.isValueInRange(outStep, 0, 4)) {
      throw new IllegalArgumentException(String.format("Incorrect outStep value [%d] - must be in range of 0-4", outBbcGain));
    }
    if (!ATLexemeProcessHelper.isValueInRange(sideTone, 0, 32767)) {
      throw new IllegalArgumentException(String.format("Incorrect sideTone value [%d] - must be in range of 0-32767", outBbcGain));
    }

    StringBuilder commandATDataBuilder = new StringBuilder("AT^SNFO=");
    commandATDataBuilder.append(outBbcGain).append(',').
        append(outCalibrate0).append(',').
        append(outCalibrate1).append(',').
        append(outCalibrate2).append(',').
        append(outCalibrate3).append(',').
        append(outCalibrate4).append(',').
        append(outStep).append(',').
        append(sideTone);
    return commandATDataBuilder.append("\r").toString();
  }

  /**
   * @param outBbcGain    negative DAC gain (0-3)
   * @param outCalibrate0 formula to calculate the value of the 5 volume steps selectable
   *                      with parameter <code>outStep</code> (0-32767)
   * @param outCalibrate1 formula to calculate the value of the 5 volume steps selectable
   *                      with parameter <code>outStep</code> (0-32767)
   * @param outCalibrate2 formula to calculate the value of the 5 volume steps selectable
   *                      with parameter <code>outStep</code> (0-32767)
   * @param outCalibrate3 formula to calculate the value of the 5 volume steps selectable
   *                      with parameter <code>outStep</code> (0-32767)
   * @param outCalibrate4 formula to calculate the value of the 5 volume steps selectable
   *                      with parameter <code>outStep</code> (0-32767)
   * @param outStep       volume steps, each defined with<code> outCalibrate[n]</code> (0-4)
   * @param sideTone      multiplication factor for the sidetone gain (0-32767)
   */
  public ATctSNFOWrite(final int outBbcGain, final int outCalibrate0, final int outCalibrate1, final int outCalibrate2,
      final int outCalibrate3, final int outCalibrate4, final int outStep, final int sideTone) {
    super(buildCommandATDataWithParamCheck(outBbcGain, outCalibrate0, outCalibrate1, outCalibrate2, outCalibrate3, outCalibrate4, outStep, sideTone));
  }

}
