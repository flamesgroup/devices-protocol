/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;
import com.flamesgroup.device.gsmb.IATSubChannelHandler;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.urc.IURC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ATEngine implements IATEngine {

  private static final Logger logger = LoggerFactory.getLogger(ATEngine.class);

  private static final long WAIT_TIME_MILLIS = 200;
  private static final long DO_NOT_WAIT_TIME_MILLIS = -1;

  private final Lock engineLock = new ReentrantLock(true);

  private final IATSubChannel atSubChannel;

  private ActiveHandlersOrganizer activeHandlersOrganizer;

  private IAT currentAT;

  private long finishTimeMillis = DO_NOT_WAIT_TIME_MILLIS;

  private final String toS;

  public ATEngine(final IATSubChannel atSubChannel) {
    this(atSubChannel, null);
  }

  public ATEngine(final IATSubChannel atSubChannel, final String description) {
    Objects.requireNonNull(atSubChannel, "atSubChannel mustn't be null");

    this.atSubChannel = new ATSubChannelWrapper(atSubChannel);

    if (description == null) {
      toS = String.format("%s@%x", getClass().getSimpleName(), hashCode());
    } else {
      toS = String.format("%s@%x[%s]", getClass().getSimpleName(), hashCode(), description);
    }
  }

  public void start(final List<IURC> urcs, IATLexemeProcessor unprocessedATLexemeHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(urcs, "urcs mustn't be null");

    if (unprocessedATLexemeHandler == null) {
      unprocessedATLexemeHandler = new IATLexemeProcessor() {

        @Override
        public boolean process(final String atLexeme) {
          logger.warn("[{}] - can't process lexeme: [{}]", ATEngine.this, atLexeme);
          return false;
        }
      };
    }

    engineLock.lock();
    try {
      if (activeHandlersOrganizer != null) {
        throw new IllegalStateException(String.format("[%s] - already started", this));
      }

      activeHandlersOrganizer = new ActiveHandlersOrganizer(urcs, unprocessedATLexemeHandler);
      activeHandlersOrganizer.start();

      try {
        atSubChannel.start(activeHandlersOrganizer);
      } catch (ChannelException | InterruptedException e) {
        try {
          activeHandlersOrganizer.stop();
        } catch (InterruptedException ie) {
          e.addSuppressed(ie);
        }
        activeHandlersOrganizer = null;
        throw e;
      }
    } finally {
      engineLock.unlock();
    }
  }

  @Override
  public void start(final List<IURC> urcs) throws ChannelException, InterruptedException {
    start(urcs, null);
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    engineLock.lock();
    try {
      if (activeHandlersOrganizer == null) {
        throw new IllegalStateException(String.format("[%s] - already stopped", this));
      }

      try {
        atSubChannel.stop();
      } catch (ChannelException | InterruptedException e) {
        try {
          activeHandlersOrganizer.stop();
        } catch (InterruptedException ie) {
          e.addSuppressed(ie);
        }
        activeHandlersOrganizer = null;
        throw e;
      }

      try {
        activeHandlersOrganizer.stop();
      } finally {
        activeHandlersOrganizer = null;
      }
    } finally {
      engineLock.unlock();
    }
  }

  @Override
  public <T extends IATResult> T execute(final IAT<T> command) throws ChannelException, InterruptedException {
    engineLock.lock();
    try {
      currentAT = command;
      try {
        if (finishTimeMillis != DO_NOT_WAIT_TIME_MILLIS) {
          long waitTimeMillis = WAIT_TIME_MILLIS - (System.currentTimeMillis() - finishTimeMillis);
          if (waitTimeMillis > 0) {
            Thread.sleep(waitTimeMillis);
          }
        }

        T atResult = command.execute(atSubChannel);

        if (command.isWaitBeforeNextAT()) {
          finishTimeMillis = System.currentTimeMillis();
        } else {
          finishTimeMillis = DO_NOT_WAIT_TIME_MILLIS;
        }
        return atResult;
      } finally {
        currentAT = null;
      }
    } finally {
      engineLock.unlock();
    }
  }

  @Override
  public String toString() {
    return toS;
  }

  private class ActiveHandlersOrganizer implements IATSubChannelHandler, IATLexemeHandler, Runnable {

    private final List<IURC> urcs = new ArrayList<>();
    private final IATLexemeProcessor unprocessedATLexemeHandler;
    private final ATLexemeParser atLexemeParser;

    private final BlockingQueue<String> atDataQueue = new LinkedBlockingQueue<>();

    private volatile Thread activeOrganizerThread;

    public ActiveHandlersOrganizer(final List<IURC> urcs, final IATLexemeProcessor unprocessedATLexemeHandler) {
      this.urcs.addAll(urcs);
      this.unprocessedATLexemeHandler = unprocessedATLexemeHandler;

      this.atLexemeParser = new ATLexemeParser(new InjectedATCommandEchoExtractorFilter(this)); // filters must be added here
    }

    @Override
    public void handleATData(final String atData) {
      logger.trace("[{}] - handle AT data [{}]", ATEngine.this, atData);
      if (!atDataQueue.offer(atData)) {
        logger.error("[{}] - atData queue overflow", ATEngine.this);
      }
    }

    @Override
    public void handleATLexeme(final String atLexeme) {
      IAT currentATLocal = currentAT;
      if (currentATLocal != null) {
        if (currentATLocal.process(atLexeme)) {
          return;
        }
      }
      for (IURC urc : urcs) {
        if (urc.process(atLexeme)) {
          return;
        }
      }
      unprocessedATLexemeHandler.process(atLexeme);
    }

    @Override
    public void run() {
      Thread currentThread = Thread.currentThread();
      try {
        while (currentThread == activeOrganizerThread || !atDataQueue.isEmpty()) {
          atLexemeParser.parse(atDataQueue.take());
        }
      } catch (InterruptedException e) {
        logger.warn(String.format("[%s] - AT data thread was interrupted", ATEngine.this), e);
      }
    }

    public void start() {
      activeOrganizerThread = new Thread(this, "ActiveOrganizerThread (" + ATEngine.this + ")");
      activeOrganizerThread.start();
    }

    public void stop() throws InterruptedException {
      Thread activeOrganizerThreadLocal = activeOrganizerThread;
      activeOrganizerThread = null;
      handleATData(""); // handle empty ATData to soft interrupt thread only from sleep on queue
      activeOrganizerThreadLocal.join();
    }

  }

  private class ATSubChannelWrapper implements IATSubChannel {

    private final IATSubChannel subChannel;

    public ATSubChannelWrapper(final IATSubChannel subChannel) {
      this.subChannel = subChannel;
    }

    @Override
    public void start(final IATSubChannelHandler atSubChannelHandler) throws ChannelException, InterruptedException {
      subChannel.start(atSubChannelHandler);
    }

    @Override
    public void stop() throws ChannelException, InterruptedException {
      subChannel.stop();
    }

    @Override
    public void writeATData(final String atData) throws ChannelException, InterruptedException {
      logger.trace("[{}] - write AT data [{}]", ATEngine.this, atData);
      subChannel.writeATData(atData);
    }

  }

}
