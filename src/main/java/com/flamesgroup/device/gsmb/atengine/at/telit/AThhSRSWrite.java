/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

/**
 * Select Ringer Sound <br>
 * Set command sets the ringer sound.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=638">Paragraph 3.5.7.21.1.3.</a>
 */
public final class AThhSRSWrite extends EmptyResponseSolidAT {

  private static final int MIN_PLAYING_TIMER_VALUE_S = 0;
  private static final int MAX_PLAYING_TIMER_VALUE_S = 60;

  private static String buildCommandATDataWithParamCheck(final int ringingTone, final int playingTimer) {
    if (ringingTone < 0) {
      throw new IllegalArgumentException(String.format("Incorrect ringingTone value [%d] -  must be equals or greater then 0", ringingTone));
    }
    if (playingTimer < MIN_PLAYING_TIMER_VALUE_S || playingTimer > MAX_PLAYING_TIMER_VALUE_S) {
      throw new IllegalArgumentException(String.format("Incorrect playingTimer value [%d] s - must be in range of %d-%d",
          playingTimer, MIN_PLAYING_TIMER_VALUE_S, MAX_PLAYING_TIMER_VALUE_S));
    }

    return "AT#SRS=" + ringingTone + ',' + playingTimer + "\r";
  }

  public AThhSRSWrite() {
    super("AT#SRS=\r");
  }

  /**
   * @param ringingTone  0 - current ringing tone
   *                     1..max - ringing tone number, where max can be read by issuing the
   *                     Test command AT#SRS=?
   * @param playingTimer 0 - ringer is stopped (if present) and current ringer sound is
   *                     set.
   *                     1..60 - ringer sound playing for specified seconds and, if
   *                     ringingTone > 0, ringer sound ringingTone is set as default ringer
   *                     sound
   */
  public AThhSRSWrite(final int ringingTone, final int playingTimer) {
    super(buildCommandATDataWithParamCheck(ringingTone, playingTimer));
  }

}
