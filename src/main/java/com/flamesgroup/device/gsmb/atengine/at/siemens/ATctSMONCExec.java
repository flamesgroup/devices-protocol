/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ATctSMONCExec extends SolidAT<ATctSMONCExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\^SMONC: [\\S\\s]+";
  private static final String AT_RESPONSE_TEMPLATE_PART =
      "(?<mcc>\\d{3}),(?<mnc>\\d{2,3}),(?<lac>[a-fA-F_0-9]{4}),(?<cell>[a-fA-F_0-9]{4}),(?<bsic>[0-7]{2}),(?<chann>\\d+),(?<rssi>\\d+),(?<c1>\\d+|-),(?<c2>\\d+|-)";

  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);
  private static final Pattern atResponsePatternPart = Pattern.compile(AT_RESPONSE_TEMPLATE_PART);

  private final List<ATctSMONCExecResult.CellInfo> cellList = new ArrayList<>();

  public ATctSMONCExec() {
    super("AT^SMONC\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      Matcher matcherPartAnswer = atResponsePatternPart.matcher(lexeme);
      while (matcherPartAnswer.find()) {
        int mcc = Integer.parseInt(matcherPartAnswer.group("mcc"));
        int mnc = Integer.parseInt(matcherPartAnswer.group("mnc"));
        int lac = Integer.parseInt(matcherPartAnswer.group("lac"), 16);
        int cell = Integer.parseInt(matcherPartAnswer.group("cell"), 16);
        int bsic = Integer.parseInt(matcherPartAnswer.group("bsic"), 8);
        int chann = Integer.parseInt(matcherPartAnswer.group("chann"));
        int rssi = Integer.parseInt(matcherPartAnswer.group("rssi"));
        int c1 = -1, c2 = -1;
        String c1Str = matcherPartAnswer.group("c1");
        String c2Str = matcherPartAnswer.group("c2");
        if (!c1Str.equals("-")) {
          c1 = Integer.parseInt(matcherPartAnswer.group("c1"));
        }
        if (!c2Str.equals("-")) {
          c2 = Integer.parseInt(matcherPartAnswer.group("c2"));
        }

        cellList.add(new ATctSMONCExecResult.CellInfo(mcc, mnc, lac, cell, bsic, chann, rssi, c1, c2));
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATctSMONCExecResult getCommandATResult() {
    return new ATctSMONCExecResult(cellList);
  }

}
