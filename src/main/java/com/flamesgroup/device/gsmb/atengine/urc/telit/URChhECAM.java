/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM.CallStatus;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM.CallType;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM.TypeOfNumber;
import com.flamesgroup.device.gsmb.atengine.urc.IURC;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URChhECAM implements IURC {

  private static final String URC_TEMPLATE = "#ECAM: (?<ccid>\\d+),(?<ccstatus>[0-7]),(?<calltype>[12]),,,(\"(?<number>.+)\",(?<type>129|145))?";
  private static final Pattern urcPattern = Pattern.compile(URC_TEMPLATE);

  private final IURChhECAMHandler handler;

  public URChhECAM(final IURChhECAMHandler handler) {
    Objects.requireNonNull(handler, "handler mustn't be null");
    this.handler = handler;
  }

  @Override
  public boolean process(final String atLexeme) {
    Matcher matcher = urcPattern.matcher(atLexeme);
    if (matcher.matches()) {
      int ccid = Integer.parseInt(matcher.group("ccid"));
      int ccstatus = Integer.parseInt(matcher.group("ccstatus"));
      CallStatus callStatus = CallStatus.getCallStatusByValue(ccstatus);
      int calltype = Integer.parseInt(matcher.group("calltype"));
      CallType callType = CallType.getCallTypeByValue(calltype);
      String number = matcher.group("number");
      TypeOfNumber typeOfNumber = null;
      if (number != null) {
        int typeOfNumberInt = Integer.parseInt(matcher.group("type"));
        typeOfNumber = TypeOfNumber.getTypeOfNumberByValue(typeOfNumberInt);
      }

      handler.handleCallEvent(ccid, callStatus, callType, number, typeOfNumber);
      return true;
    } else {
      return false;
    }
  }

}
