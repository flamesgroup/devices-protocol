/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import com.flamesgroup.device.gsmb.atengine.param.PpsCREG;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URCpsCREG implements IURC {

  private static final String URC_TEMPLATE = "\\+CREG: (?<stat>[0-5])(,\"(?<lac>.+)\",\"(?<ci>.+)\")?";
  private static final Pattern urcPattern = Pattern.compile(URC_TEMPLATE);

  private final IURCpsCREGHandler handler;

  public URCpsCREG(final IURCpsCREGHandler handler) {
    Objects.requireNonNull(handler, "handler mustn't be null");
    this.handler = handler;
  }

  @Override
  public boolean process(final String atLexeme) {
    Matcher matcher = urcPattern.matcher(atLexeme);
    if (matcher.matches()) {
      int stat = Integer.parseInt(matcher.group("stat"));
      PpsCREG.Status status = PpsCREG.Status.getStatusByValue(stat);
      String lac = matcher.group("lac");
      String ci = matcher.group("ci");

      handler.handleNetworkRegistration(status, lac, ci);
      return true;
    } else {
      return false;
    }
  }

}
