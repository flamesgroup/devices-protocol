/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIR.StatusOnMobile;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIR.StatusOnNetwork;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ATshCLIR extends SolidAT<ATshCLIRResult> {

  private static final String COMMAND_TEMPLATE = "*#31#";

  private static final String AT_RESPONSE_TEMPLATE = "\\+CLIR: (?<n>[0-2]),(?<m>[0-4])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private StatusOnMobile statusOnMobile;
  private StatusOnNetwork statusOnNetwork;

  protected ATshCLIR(final String commandATData) {
    super(commandATData, 5000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      statusOnMobile = StatusOnMobile.getStatusByValue(Integer.parseInt(matcher.group("n")));
      statusOnNetwork = StatusOnNetwork.getStatusByValue(Integer.parseInt(matcher.group("m")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATshCLIRResult getCommandATResult() {
    return new ATshCLIRResult(statusOnMobile, statusOnNetwork);
  }

  public static boolean isCLIRCode(final String code) {
    return COMMAND_TEMPLATE.equals(code);
  }

}
