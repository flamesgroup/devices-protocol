/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PpsIPR {

  private PpsIPR() {
  }

  public enum Rate {

    RATE_0(0),
    RATE_300(300),
    RATE_1200(1200),
    RATE_2400(2400),
    RATE_4800(4800),
    RATE_9600(9600),
    RATE_19200(19200),
    RATE_38400(38400),
    RATE_57600(57600),
    RATE_115200(115200);

    private final int value;

    Rate(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
