/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ATshCallBarring extends com.flamesgroup.device.gsmb.atengine.at.starhash.ATshCallBarring<ATshCallBarringResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CLCK: (?<status>[01]),(?<class>[1-9]{1,3})";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private final List<ATshCallBarringResult.ATCallBarringInfo> callBarringInfoList = new ArrayList<>();

  public ATshCallBarring(final String code) {
    super("ATD" + code + "\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      boolean active = ATLexemeProcessHelper.intStringToBoolean(matcher.group("status"));
      Set<ServiceClass> serviceClasses = ServiceClass.getServiceClassesByValue(Integer.parseInt(matcher.group("class")));
      callBarringInfoList.add(new ATshCallBarringResult.ATCallBarringInfo(active, serviceClasses));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATshCallBarringResult getCommandATResult() {
    return new ATshCallBarringResult(callBarringInfoList);
  }

}
