/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import java.util.HashSet;
import java.util.Set;

public enum ServiceClass {

  VOICE(1, "Voice"),
  DATA(2, "Data"),
  FAX(4, "Fax"),
  SMS(8, "SMS"),
  SYNC(16, "Sync"),
  ASYNC(32, "Async"),
  PACKET(64, "Packet"),
  PAD(128, "PAD");

  private final int value;
  private final String name;

  ServiceClass(final int value, final String name) {
    this.value = value;
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }

  public int getValue() {
    return value;
  }

  public String getName() {
    return name;
  }

  public static Set<ServiceClass> getServiceClassesByValue(final int value) {
    Set<ServiceClass> result = new HashSet<>();
    int sumServiceClasses = value;

    ServiceClass[] serviceClasses = ServiceClass.values();
    for (int i = serviceClasses.length - 1; i >= 0; i--) {
      ServiceClass serviceClass = serviceClasses[i];
      if (serviceClass.getValue() <= sumServiceClasses) {
        result.add(serviceClass);
        sumServiceClasses -= serviceClass.getValue();
      }
    }

    return result;
  }

  public static ServiceClass getServiceClass(final String name) {
    for (ServiceClass serviceClass : ServiceClass.values()) {
      if (serviceClass.getName().equals(name)) {
        return serviceClass;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported service class name %s", name));
  }

}
