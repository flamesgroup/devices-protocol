/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ATpsCEERExec extends SolidAT<ATpsCEERExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\+CEER: (?<locationID>\\d+),(?<reason>\\d+),(?<ssRelease>\\d+)";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private int locationID;
  private int reason;
  private int ssRelease;

  public ATpsCEERExec() {
    super("AT+CEER\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      locationID = Integer.parseInt(matcher.group("locationID"));
      reason = Integer.parseInt(matcher.group("reason"));
      ssRelease = Integer.parseInt(matcher.group("ssRelease"));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATpsCEERExecResult getCommandATResult() {
    return new ATpsCEERExecResult(locationID, reason, ssRelease);
  }

}
