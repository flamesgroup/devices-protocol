/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extended error report for Network Reject cause <br>
 * Execution command causes the TA to return a numeric code which should offer the user of the TA a report for the last mobility management(MM) or
 * session management(SM) procedure not accepted by the network and a report of detach or deactivation causes from network.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=279">Paragraph 3.5.7.1.10.</a>
 */
public final class AThhCEERNETExec extends SolidAT<AThhCEERNETExecResult> {

  private static final String AT_RESPONSE_TEMPLATE = "#CEERNET: (?<code>\\d+)";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private int code;

  public AThhCEERNETExec() {
    super("AT#CEERNET\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      code = Integer.parseInt(matcher.group("code"));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected AThhCEERNETExecResult getCommandATResult() {
    return new AThhCEERNETExecResult(code);
  }

}
