/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

public final class AThhSSLENReadResult implements IATResult {

  private final boolean enable;

  public AThhSSLENReadResult(final boolean enable) {
    this.enable = enable;
  }

  public boolean isEnable() {
    return enable;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof AThhSSLENReadResult)) {
      return false;
    }
    final AThhSSLENReadResult that = (AThhSSLENReadResult) object;

    return enable == that.enable;
  }

  @Override
  public int hashCode() {
    return Boolean.hashCode(enable);
  }

}
