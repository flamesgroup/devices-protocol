/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Contains GSM-related information (AT#CSURVF = 0, AT#CSURVEXT = 0)
 */
public final class AThhCSURBaseExecResult implements IATResult {

  private final List<BcchCarrierInfo> bcchCarrierInfos;
  private final List<NonBcchCarrierInfo> nonBcchCarrierInfos;

  public AThhCSURBaseExecResult(final List<BcchCarrierInfo> bcchCarrierInfos, final List<NonBcchCarrierInfo> nonBcchCarrierInfos) {
    this.bcchCarrierInfos = Collections.unmodifiableList(bcchCarrierInfos);
    this.nonBcchCarrierInfos = Collections.unmodifiableList(nonBcchCarrierInfos);
  }

  public List<BcchCarrierInfo> getBcchCarrierInfos() {
    return bcchCarrierInfos;
  }

  public List<NonBcchCarrierInfo> getNonBcchCarrierInfos() {
    return nonBcchCarrierInfos;
  }

  public enum CellStatus {

    CELL_SUITABLE(0),
    CELL_LOW_PRIORITY(1),
    CELL_FORBIDDEN(2),
    CELL_BARRED(3),
    CELL_LOW_LEVEL(4),
    CELL_OTHER(5);

    private final int value;

    CellStatus(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static CellStatus getStatusByValue(final int value) {
      for (CellStatus status : CellStatus.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public static class BcchCarrierInfo {

    private final int arfcn;
    private final int bsic;
    private final int rxLev;
    private final float ber;
    private final int mcc;
    private final int mnc;
    private final int lac;
    private final int cellId;
    private final CellStatus cellStatus;
    private final List<Integer> validChannels;
    private final List<Integer> neighbourChannels;

    private String toS;

    /**
     * @param arfcn             absolute frequency channel number
     * @param bsic              base station identity code
     * @param rxLev             received signal level in dBm
     * @param ber               bit error rate in %
     * @param mcc               mobile country code
     * @param mnc               mobile network code
     * @param lac               location area code
     * @param cellId            cell identifier
     * @param cellStatus        cell status
     * @param validChannels     list of a valid arfcn
     * @param neighbourChannels list of a neighbour arfcn
     */
    public BcchCarrierInfo(final int arfcn, final int bsic, final int rxLev, final float ber, final int mcc, final int mnc,
        final int lac, final int cellId, final CellStatus cellStatus, final List<Integer> validChannels, final List<Integer> neighbourChannels) {
      this.arfcn = arfcn;
      this.bsic = bsic;
      this.rxLev = rxLev;
      this.ber = ber;
      this.mcc = mcc;
      this.mnc = mnc;
      this.lac = lac;
      this.cellId = cellId;
      this.cellStatus = cellStatus;
      this.validChannels = validChannels;
      this.neighbourChannels = neighbourChannels;
    }

    public BcchCarrierInfo(final int arfcn, final int bsic, final int rxLev, final float ber, final int mcc, final int mnc,
        final int lac, final int cellId, final CellStatus cellStatus, final List<Integer> validChannels) {
      this(arfcn, bsic, rxLev, ber, mcc, mnc, lac, cellId, cellStatus, validChannels, null);
    }

    public int getArfcn() {
      return arfcn;
    }

    public int getBsic() {
      return bsic;
    }

    public int getRxLev() {
      return rxLev;
    }

    public float getBer() {
      return ber;
    }

    public int getMcc() {
      return mcc;
    }

    public int getMnc() {
      return mnc;
    }

    public int getLac() {
      return lac;
    }

    public int getCellId() {
      return cellId;
    }

    public CellStatus getCellStatus() {
      return cellStatus;
    }

    public List<Integer> getValidChannels() {
      return validChannels;
    }

    public List<Integer> getNeighbourChannels() {
      return neighbourChannels;
    }

    @Override
    public boolean equals(final Object object) {
      if (this == object) {
        return true;
      }
      if (!(object instanceof BcchCarrierInfo)) {
        return false;
      }
      final BcchCarrierInfo that = (BcchCarrierInfo) object;

      return arfcn == that.arfcn
          && bsic == that.bsic
          && rxLev == that.rxLev
          && ber == that.ber
          && mcc == that.mcc
          && mnc == that.mnc
          && lac == that.lac
          && cellId == that.cellId
          && cellStatus == that.cellStatus
          && validChannels.equals(that.validChannels)
          && Objects.equals(neighbourChannels, that.neighbourChannels);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = arfcn;
      result = prime * result + bsic;
      result = prime * result + rxLev;
      result = prime * result + Float.hashCode(ber);
      result = prime * result + mcc;
      result = prime * result + mnc;
      result = prime * result + lac;
      result = prime * result + cellId;
      result = prime * result + cellStatus.hashCode();
      result = prime * result + validChannels.hashCode();
      result = prime * result + Objects.hashCode(neighbourChannels);
      return result;
    }

    @Override
    public String toString() {
      if (toS == null) {
        toS = String.format("%s@%x[arfcn:%d,bsic:%d,rxLev:%d,ber:%f,mcc:%d,mnc:%d,lac:%d,cellId:%d,cellStatus:%s,vch:%s,chs:%s]",
            getClass().getSimpleName(), hashCode(), arfcn, bsic, rxLev,
            ber, mcc, mnc, lac, cellId, cellStatus, validChannels, neighbourChannels);
      }
      return toS;
    }

  }

  public static class NonBcchCarrierInfo {

    private final int arfcn;
    private final int rxLev;

    private String toS;

    /**
     * @param arfcn absolute frequency channel number
     * @param rxLev received signal level in dBm
     */
    public NonBcchCarrierInfo(final int arfcn, final int rxLev) {
      this.arfcn = arfcn;
      this.rxLev = rxLev;
    }

    public int getArfcn() {
      return arfcn;
    }

    public int getRxLev() {
      return rxLev;
    }

    @Override
    public boolean equals(final Object object) {
      if (this == object) {
        return true;
      }
      if (!(object instanceof NonBcchCarrierInfo)) {
        return false;
      }
      final NonBcchCarrierInfo that = (NonBcchCarrierInfo) object;

      return arfcn == that.arfcn && rxLev == that.rxLev;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = arfcn;
      result = prime * result + rxLev;
      return result;
    }

    @Override
    public String toString() {
      if (toS == null) {
        toS = String.format("%s@%x[arfcn:%d,rxLev:%d]", getClass().getSimpleName(), hashCode(), arfcn, rxLev);
      }
      return toS;
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof AThhCSURBaseExecResult)) {
      return false;
    }
    final AThhCSURBaseExecResult that = (AThhCSURBaseExecResult) object;

    return Objects.equals(bcchCarrierInfos, that.bcchCarrierInfos) && Objects.equals(nonBcchCarrierInfos, that.nonBcchCarrierInfos);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(bcchCarrierInfos);
    result = prime * result + Objects.hashCode(nonBcchCarrierInfos);
    return result;
  }

}
