/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.siemens;

public final class PadD {

  private PadD() {
  }

  public enum DTRMode {

    DEVICE_IGNORES_DTR_TRANSITIONS_AT_AD_D0_EQUIVALENT_TO_AT_AD_D5(0),
    HIGH_TO_LOW_TRANSITION_CURRENT_CONNECTION_NOT_CLOSED(1),
    HIGH_TO_LOW_TRANSITION_CURRENT_CONNECTION_CLOSED(2);

    private final int value;

    DTRMode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
