/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ATshCallWaitingResult implements IATResult {

  private final List<CallWaitingInfo> callWaitingInfoList;

  public ATshCallWaitingResult(final List<CallWaitingInfo> callWaitingInfoList) {
    this.callWaitingInfoList = Collections.unmodifiableList(callWaitingInfoList);
  }

  public List<CallWaitingInfo> getCallWaitingInfoList() {
    return callWaitingInfoList;
  }

  public static class CallWaitingInfo {

    private final boolean active;
    private final Set<ServiceClass> serviceClasses;

    public CallWaitingInfo(final boolean active, final Set<ServiceClass> serviceClasses) {
      this.active = active;
      this.serviceClasses = serviceClasses;
    }

    public boolean isActive() {
      return active;
    }

    public Set<ServiceClass> getServiceClasses() {
      return serviceClasses;
    }

    @Override
    public boolean equals(final Object object) {
      if (this == object) {
        return true;
      }
      if (!(object instanceof CallWaitingInfo)) {
        return false;
      }
      CallWaitingInfo that = (CallWaitingInfo) object;

      return active == that.active
          && Objects.equals(serviceClasses, that.getServiceClasses());
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 3;
      result = prime * result + Boolean.hashCode(active);
      result = prime * result + Objects.hashCode(serviceClasses);
      return result;
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ATshCallWaitingResult)) {
      return false;
    }
    ATshCallWaitingResult that = (ATshCallWaitingResult) object;

    return Objects.equals(callWaitingInfoList, that.getCallWaitingInfoList());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(callWaitingInfoList);
    return result;
  }

}
