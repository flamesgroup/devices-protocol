/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import com.flamesgroup.device.gsmb.atengine.param.telit.PhhQSS.Status;
import com.flamesgroup.device.gsmb.atengine.urc.IURC;
import com.flamesgroup.device.gsmb.atengine.urc.IURChhQSSHandler;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URChhQSS implements IURC {

  private static final String URC_TEMPLATE = "#QSS: (?<status>[0-3])";
  private static final Pattern urcPattern = Pattern.compile(URC_TEMPLATE);

  private final IURChhQSSHandler handler;

  public URChhQSS(final IURChhQSSHandler handler) {
    Objects.requireNonNull(handler, "handler mustn't be null");
    this.handler = handler;
  }

  @Override
  public boolean process(final String atLexeme) {
    Matcher matcher = urcPattern.matcher(atLexeme);
    if (matcher.matches()) {
      Status status = Status.getStatusByValue(Integer.parseInt(matcher.group("status")));

      handler.handleStatus(status);
      return true;
    } else {
      return false;
    }
  }

}
