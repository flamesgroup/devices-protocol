/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.SMSStatus;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation only for PDU mode.
 */
public final class ATctSMGRWrite extends SolidAT<ATctSMGRWriteResult> {

  private static final String AT_RESPONSE_TEMPLATE1 = "\\^SMGR: (?<stat>[0-4]),(?<alpha>.+)?,(?<length>\\d+)";
  private static final String AT_RESPONSE_TEMPLATE2 = "(?<pdu>[a-fA-F_0-9]+)";

  private static final Pattern atResponsePattern1 = Pattern.compile(AT_RESPONSE_TEMPLATE1);
  private static final Pattern atResponsePattern2 = Pattern.compile(AT_RESPONSE_TEMPLATE2);

  private SMSStatus status;
  private String alpha;
  private int length;
  private String pdu;

  private boolean processFirstPart = true;

  public ATctSMGRWrite(final int index) {
    super("AT^SMGR=" + index + "\r", 5000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    if (processFirstPart) {
      Matcher matcher = atResponsePattern1.matcher(lexeme);
      if (matcher.matches()) {
        status = SMSStatus.getSMSStatusByValue(Integer.parseInt(matcher.group("stat")));
        alpha = matcher.group("alpha");
        length = Integer.parseInt(matcher.group("length"));
        processFirstPart = false;
        return true;
      }
    } else {
      Matcher matcher = atResponsePattern2.matcher(lexeme);
      if (matcher.matches()) {
        pdu = matcher.group("pdu");
        return true;
      }
    }
    return false;
  }

  @Override
  protected ATctSMGRWriteResult getCommandATResult() {
    return new ATctSMGRWriteResult(status, alpha, length, pdu);
  }

}
