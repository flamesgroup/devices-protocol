/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCSMS {

  private PpsCSMS() {
  }

  public enum MobileTerminatedMessages {

    TYPE_NOT_SUPPORTED(0),
    TYPE_SUPPORTED(1);

    private final int value;

    MobileTerminatedMessages(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static MobileTerminatedMessages getMobileTerminatedMessagesByValue(final int value) {
      for (MobileTerminatedMessages mt : MobileTerminatedMessages.values()) {
        if (mt.getValue() == value) {
          return mt;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum MobileOriginatedMessages {

    TYPE_NOT_SUPPORTED(0),
    TYPE_SUPPORTED(1);

    private final int value;

    MobileOriginatedMessages(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static MobileOriginatedMessages getMobileOriginatedMessagesByValue(final int value) {
      for (MobileOriginatedMessages mo : MobileOriginatedMessages.values()) {
        if (mo.getValue() == value) {
          return mo;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum BroadcastTypeMessages {

    TYPE_NOT_SUPPORTED(0),
    TYPE_SUPPORTED(1);

    private final int value;

    BroadcastTypeMessages(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static BroadcastTypeMessages getBroadcastTypeMessagesByValue(final int value) {
      for (BroadcastTypeMessages bm : BroadcastTypeMessages.values()) {
        if (bm.getValue() == value) {
          return bm;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
