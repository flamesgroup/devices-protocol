/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSRTC.Type;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSRTC.Volume;

import java.util.Objects;

public final class ATctSRTCWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final Type type) {
    Objects.requireNonNull(type, "type mustn't be null");
    return "AT^SRTC=" + type.getValue() + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final Volume volume) {
    Objects.requireNonNull(volume, "volume mustn't be null");
    return "AT^SRTC=," + volume.getValue() + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final Type type, final Volume volume) {
    Objects.requireNonNull(type, "mode mustn't be null");
    Objects.requireNonNull(volume, "volume mustn't be null");
    return "AT^SRTC=" + type.getValue() + "," + volume.getValue() + "\r";
  }

  public ATctSRTCWrite(final Type type) {
    super(buildCommandATDataWithParamCheck(type));
  }

  public ATctSRTCWrite(final Volume volume) {
    super(buildCommandATDataWithParamCheck(volume));
  }

  public ATctSRTCWrite(final Type type, final Volume volume) {
    super(buildCommandATDataWithParamCheck(type, volume));
  }

}
