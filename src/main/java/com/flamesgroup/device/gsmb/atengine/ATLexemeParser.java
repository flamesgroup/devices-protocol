/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ATLexemeParser {

  private static final String LEXEME_TEMPLATE = ""
      + "(?<gd>\r\n)" // general delimiter
      + "|"
      + "(?<id>" // interactive delimiter
      /**/ + "> " // SMS or USSD
      /**/ + "|"
      /**/ + (char) 0x09 + (char) 0x10 + (char) 0x19 + "EXIT: .{10,}" // siemens/cinterion exit code
      + ")";

  private static final Pattern lexemePattern = Pattern.compile(LEXEME_TEMPLATE);

  private final IATLexemeHandler atLexemeHandler;

  private final StringBuilder responseBuffer = new StringBuilder();

  ATLexemeParser(final IATLexemeHandler atLexemeHandler) {
    Objects.requireNonNull(atLexemeHandler, "atLexemeHandler mustn't be null");
    this.atLexemeHandler = atLexemeHandler;
  }

  public void parse(final CharSequence data) {
    responseBuffer.append(data);
    boolean lexemePatternMatch;
    do {
      lexemePatternMatch = false;
      if (responseBuffer.length() >= 2) {
        Matcher matcher = lexemePattern.matcher(responseBuffer);
        if (matcher.find()) {
          lexemePatternMatch = true;

          String gd = matcher.group("gd");
          String id = matcher.group("id");
          if (gd != null || id != null) {
            String lexeme = responseBuffer.substring(0, matcher.start());
            if (lexeme.length() > 0) {
              atLexemeHandler.handleATLexeme(lexeme);
            }
          }
          if (id != null) {
            atLexemeHandler.handleATLexeme(id);
          }

          responseBuffer.delete(0, matcher.end());
        }
      }
    } while (lexemePatternMatch);
  }

}
