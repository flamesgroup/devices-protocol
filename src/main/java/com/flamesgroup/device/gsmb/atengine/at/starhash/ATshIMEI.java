/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

public abstract class ATshIMEI extends SolidAT<ATshIMEIResult> {

  private static final String COMMAND_TEMPLATE = "*#06#";

  private String imei;

  public ATshIMEI(final String commandATData) {
    super(commandATData, 5000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    imei = lexeme;
    return true;
  }

  @Override
  protected ATshIMEIResult getCommandATResult() {
    return new ATshIMEIResult(imei);
  }

  public static boolean isIMEICode(final String code) {
    return COMMAND_TEMPLATE.equals(code);
  }

}
