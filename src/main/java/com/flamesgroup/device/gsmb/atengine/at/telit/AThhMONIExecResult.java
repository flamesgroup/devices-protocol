/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Contains GSM-related information from the whole set of seven cells in the
 * neighbour list of the serving cell (AT#MONI=7).
 */
public final class AThhMONIExecResult implements IATResult {

  private final List<CellInfo> cellList;

  public AThhMONIExecResult(final List<CellInfo> cellList) {
    this.cellList = Collections.unmodifiableList(cellList);
  }

  public List<CellInfo> getCellList() {
    return cellList;
  }

  public static class CellInfo {

    private final String cell;
    private final int bsic;
    private final int lac;
    private final int cellId;
    private final int arfcn;
    private final int power;
    private final int c1;
    private final int c2;
    private final int ta;
    private final int rxQual;
    private final String plmn;

    private String toS;

    /**
     * @param cell   ordinal number of the cell
     *               <pre>    S - serving cell<br/>    N1..N6 - neighbour cells</pre>
     * @param bsic   base station identity code
     * @param lac    location area code
     * @param cellId cell identifier
     * @param arfcn  absolute frequency channel number
     * @param power  rxLevel - received signal strength in dBm
     * @param c1     cell selection criteria
     * @param c2     cell reselection criteria
     * @param ta     timing advance
     * @param rxQual quality of reception
     * @param plmn   public land mobile network
     */
    public CellInfo(final String cell, final int bsic, final int lac, final int cellId, final int arfcn, final int power, final int c1, final int c2, final int ta, final int rxQual,
        final String plmn) {
      this.cell = cell;
      this.bsic = bsic;
      this.lac = lac;
      this.cellId = cellId;
      this.arfcn = arfcn;
      this.power = power;
      this.c1 = c1;
      this.c2 = c2;
      this.ta = ta;
      this.rxQual = rxQual;
      this.plmn = plmn;
    }

    public String getCell() {
      return cell;
    }

    public int getBsic() {
      return bsic;
    }

    public int getLac() {
      return lac;
    }

    public int getCellId() {
      return cellId;
    }

    public int getArfcn() {
      return arfcn;
    }

    public int getPower() {
      return power;
    }

    public int getC1() {
      return c1;
    }

    public int getC2() {
      return c2;
    }

    public int getTa() {
      return ta;
    }

    public int getRxQual() {
      return rxQual;
    }

    public String getPlmn() {
      return plmn;
    }

    @Override
    public boolean equals(final Object object) {
      if (object == this) {
        return true;
      }
      if (!(object instanceof CellInfo)) {
        return false;
      }
      CellInfo that = (CellInfo) object;

      return Objects.equals(cell, that.cell)
          && bsic == that.bsic
          && lac == that.lac
          && cellId == that.cellId
          && arfcn == that.arfcn
          && power == that.power
          && c1 == that.c1
          && c1 == that.c2
          && ta == that.ta
          && rxQual == that.rxQual
          && Objects.equals(plmn, that.plmn);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 3;
      result = prime * result + Objects.hashCode(cell);
      result = prime * result + bsic;
      result = prime * result + lac;
      result = prime * result + cellId;
      result = prime * result + arfcn;
      result = prime * result + power;
      result = prime * result + c1;
      result = prime * result + c1;
      result = prime * result + ta;
      result = prime * result + rxQual;
      result = prime * result + Objects.hashCode(plmn);
      return result;
    }

    @Override
    public String toString() {
      if (toS == null) {
        toS = String.format("%s@%x:[%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%s]", getClass().getSimpleName(), hashCode(), cell, bsic, lac, cellId, arfcn, power, c1, c2, ta, rxQual, plmn);
      }
      return toS;
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof AThhMONIExecResult)) {
      return false;
    }
    AThhMONIExecResult that = (AThhMONIExecResult) object;

    return Objects.equals(cellList, that.getCellList());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(cellList);
    return result;
  }

}
