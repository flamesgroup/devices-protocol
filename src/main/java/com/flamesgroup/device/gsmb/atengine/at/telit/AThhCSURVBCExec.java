/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

/**
 * BCCH Network Survey <br>
 * Execution command performs a quick network survey through M (maximum number of available frequencies depending on last selected band) channels.
 * The survey stops as soon as specified number of BCCH carriers are found.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=548">Paragraph 3.5.7.11.6.</a>
 */
public final class AThhCSURVBCExec extends AThhCSURVBaseExec {

  private static String buildCommandATDataWithParamCheck(final int numberBcchCarriers) {
    if (numberBcchCarriers <= 0) {
      throw new IllegalArgumentException("numberBcchCarriers must be more then 0");
    }
    return "AT#CSURVBC=" + numberBcchCarriers + "\r";
  }

  public AThhCSURVBCExec(final int numberBcchCarriers) {
    super(buildCommandATDataWithParamCheck(numberBcchCarriers));

  }

}
