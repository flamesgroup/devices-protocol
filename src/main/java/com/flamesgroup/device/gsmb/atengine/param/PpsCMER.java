/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCMER {

  private PpsCMER() {
  }

  public enum Mode {

    DISCARD_CIEV(0),
    DISCARD_CIEV_WHEN_TA_TE_LINK_IS_RESERVED(1),
    BUFFER_CIEV(2),
    FORWARD_CIEV(3);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
