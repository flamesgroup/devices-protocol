/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSMGO.SMSOverflowStatus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ATctSMGORead extends SolidAT<ATctSMGOReadResult> {

  private static final String AT_RESPONSE_TEMPLATE = "\\^SMGO: (?<n>[01]),(?<mode>[0-2])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private boolean n;
  private int mode;

  public ATctSMGORead() {
    super("AT^SMGO?\r");
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      n = ATLexemeProcessHelper.intStringToBoolean(matcher.group("n"));
      mode = Integer.parseInt(matcher.group("mode"));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATctSMGOReadResult getCommandATResult() {
    return new ATctSMGOReadResult(n, SMSOverflowStatus.getSMSOverflowStatusByValue(mode));
  }

}
