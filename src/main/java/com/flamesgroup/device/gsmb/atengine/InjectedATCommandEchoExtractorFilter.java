/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class InjectedATCommandEchoExtractorFilter implements IATLexemeHandler {

  private static final String INJECTED_AT_COMMAND_ECHO_EXTRACTOR_FILTER_TEMPLATE = ".+(AT.*\r).+";
  private static final Pattern injectedATCommandEchoExtractorFilterPattern = Pattern.compile(INJECTED_AT_COMMAND_ECHO_EXTRACTOR_FILTER_TEMPLATE);

  private final IATLexemeHandler atLexemeHandler;

  InjectedATCommandEchoExtractorFilter(final IATLexemeHandler atLexemeHandler) {
    this.atLexemeHandler = atLexemeHandler;
  }

  @Override
  public void handleATLexeme(String atLexeme) {
    Matcher matcher = injectedATCommandEchoExtractorFilterPattern.matcher(atLexeme);
    if (matcher.find()) {
      atLexemeHandler.handleATLexeme(matcher.group(1));

      atLexeme = atLexeme.substring(0, matcher.start(1)) + atLexeme.substring(matcher.end(1));
    }
    atLexemeHandler.handleATLexeme(atLexeme);
  }

}
