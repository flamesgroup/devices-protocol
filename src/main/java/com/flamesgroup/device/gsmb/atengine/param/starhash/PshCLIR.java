/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.starhash;

public final class PshCLIR {

  private PshCLIR() {
  }

  public enum StatusOnMobile {

    ACCORDING_TO_CLIR_SERVICE_NETWORK_STATUS(0),
    INVOCATION(1),
    SUPPRESSION(2);

    private final int value;

    StatusOnMobile(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static StatusOnMobile getStatusByValue(final int value) {
      for (StatusOnMobile status : StatusOnMobile.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum StatusOnNetwork {

    NOT_PROVISIONED(0),
    PROVISIONED_PERMANENTLY(1),
    UNKNOWN(2),
    TEMPORARY_MODE_PRESENTATION_RESTRICTED(3),
    TEMPORARY_MODE_PRESENTATION_ALLOWED(4);

    private final int value;

    StatusOnNetwork(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static StatusOnNetwork getStatusByValue(final int value) {
      for (StatusOnNetwork status : StatusOnNetwork.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
