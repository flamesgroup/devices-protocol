/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCSMS.MessagingService;

import java.util.Objects;

/**
 * Select Message Service <br>
 * Set command selects messaging service &lt;service&gt;. It returns the types of messages essaging &lt;service&gt; service. It returns the types of messages supported by the ME
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=202">Paragraph 3.5.5.1.1.</a>
 */
public final class ATpsCSMSWrite extends com.flamesgroup.device.gsmb.atengine.at.ATpsCSMSWrite {

  private static String buildCommandATDataWithParamCheck(final MessagingService messagingService) {
    Objects.requireNonNull(messagingService, "messagingService mustn't be null");
    return "AT+CSMS=" + messagingService.getValue() + "\r";
  }

  public ATpsCSMSWrite(final MessagingService messagingService) {
    super(buildCommandATDataWithParamCheck(messagingService));
  }

}
