/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.http;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.ATEngineException;
import com.flamesgroup.device.gsmb.atengine.ATEngineHelper;
import com.flamesgroup.device.gsmb.atengine.ATEngineTimeoutException;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhHTTPCFGWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhHTTPQRYWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSGACTExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSGACTExecResult;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSSLENExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSSLSECCFGExec;
import com.flamesgroup.device.gsmb.atengine.at.telit.AThhSSLSECDATAActionStoreWrite;
import com.flamesgroup.device.gsmb.atengine.at.telit.ATpsCGDCONTWrite;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhHTTPQRY;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLEN;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLSECCFG;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLSECDATA;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsPDPContext;
import com.flamesgroup.device.gsmb.atengine.urc.telit.IURChhHTTPRINGHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class HTTPSession implements IHTTPSession, IURChhHTTPRINGHandler {

  private static final Logger logger = LoggerFactory.getLogger(HTTPSession.class);

  private static final int HTTP_REQUEST_TIMEOUT = 120; // in seconds
  private static final int PROF_ID = 0;
  private static final int CID = 1;

  private final IATEngine atEngine;
  private final Lock sessionLock = new ReentrantLock();
  private final Condition requestCondition = sessionLock.newCondition();

  private boolean sessionOpen; // guarded by sessionLock
  private int httpStatusCode;
  private String contentType;
  private int dataSize;

  public HTTPSession(final IATEngine atEngine) {
    Objects.requireNonNull(atEngine, "atEngine mustn't be null");
    this.atEngine = atEngine;
  }

  @Override
  public void open(final String apn, final InetSocketAddress serverAddress, final String caCertificate) throws ChannelException, InterruptedException {
    Objects.requireNonNull(apn, "apn mustn't be null");
    Objects.requireNonNull(serverAddress, "serverAddress mustn't be null");
    sessionLock.lock();
    try {
      if (sessionOpen) {
        throw new IllegalStateException("Can't open HTTP session - HTTP session has been already opened");
      } else {
        atEngine.execute(new ATpsCGDCONTWrite(CID, PpsPDPContext.PDPType.IP, apn));
        AThhSGACTExecResult aThhSGACTExecResult = ATEngineHelper.execute(this, atEngine, new AThhSGACTExec(CID, 1), 5, 1000);
        if (aThhSGACTExecResult.getIpAddress() == null || aThhSGACTExecResult.getIpAddress().isEmpty()) {
          throw new ATEngineException(String.format("Can't connect to apn: [%s]", apn));
        }
        atEngine.execute(new AThhHTTPCFGWrite(PROF_ID));
        int sslEnable = 0;
        if (caCertificate != null && !caCertificate.isEmpty()) {
          sslEnable = 1;
          atEngine.execute(new AThhSSLENExec(PhhSSLEN.OnOff.ACTIVATE));
          atEngine.execute(new AThhSSLSECCFGExec(PhhSSLSECCFG.CipherSuite.CHIPER_SUITE_IS_CHOSEN_BY_REMOTE_SERVER, PhhSSLSECCFG.AuthMMode.MANAGE_SERVER_AUTHENTICATION));
          atEngine.execute(new AThhSSLSECDATAActionStoreWrite(PhhSSLSECDATA.DataType.CA_CERTIFICATE, caCertificate.getBytes().length, caCertificate));
          atEngine.execute(new AThhSSLENExec(PhhSSLEN.OnOff.DEACTIVATE));
        }
        atEngine.execute(new AThhHTTPCFGWrite(PROF_ID, serverAddress.getHostName(), serverAddress.getPort(), 0, sslEnable, HTTP_REQUEST_TIMEOUT, CID));
        sessionOpen = true;
      }
    } finally {
      sessionLock.unlock();
    }
  }

  @Override
  public HTTPResponse executeRequest(final String uri) throws ChannelException, InterruptedException {
    Objects.requireNonNull(uri, "uri mustn't be null");
    sessionLock.lock();
    try {
      if (!sessionOpen) {
        throw new IllegalStateException("Can't execute Request - HTTP session hasn't been opened");
      }
      atEngine.execute(new AThhHTTPQRYWrite(PROF_ID, PhhHTTPQRY.HttpMethod.GET, uri));
      if (!requestCondition.await(HTTP_REQUEST_TIMEOUT, TimeUnit.SECONDS)) {
        throw new ATEngineTimeoutException(String.format("[%s] - timeout for waiting response elapsed", this));
      }
      return new HTTPResponse(null, httpStatusCode, contentType, dataSize);
    } finally {
      sessionLock.unlock();
    }
  }

  @Override
  public void close() throws ChannelException, InterruptedException {
    sessionLock.lock();
    try {
      if (!sessionOpen) {
        throw new IllegalStateException("Can't close HTTP session - HTTP session hasn't been opened");
      }
      atEngine.execute(new AThhSGACTExec(CID, 0));
    } finally {
      sessionOpen = false;
      sessionLock.unlock();
    }
  }

  @Override
  public void handleHttpServerAnswer(final int profId, final int httpStatusCode, final String contentType, final int dataSize) {
    logger.debug("[{}] - handleHttpServerAnswer: profId:  [{}], httpStatusCode: [{}], contentType: [{}], dataSize: [{}]", this, profId, httpStatusCode, contentType, dataSize);
    sessionLock.lock();
    try {
      this.httpStatusCode = httpStatusCode;
      this.contentType = contentType;
      this.dataSize = dataSize;
      requestCondition.signalAll();
    } finally {
      sessionLock.unlock();
    }
  }

}
