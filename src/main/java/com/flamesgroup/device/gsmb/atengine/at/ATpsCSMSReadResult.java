/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.BroadcastTypeMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileOriginatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileTerminatedMessages;

import java.util.Objects;

public abstract class ATpsCSMSReadResult<TMessagingService extends Enum<TMessagingService>> implements IATResult {

  protected final TMessagingService messagingService;
  protected final MobileTerminatedMessages mobileTerminatedMessages;
  protected final MobileOriginatedMessages mobileOriginatedMessages;
  protected final BroadcastTypeMessages broadcastTypeMessages;

  public ATpsCSMSReadResult(final TMessagingService messagingService, final MobileTerminatedMessages mobileTerminatedMessages,
      final MobileOriginatedMessages mobileOriginatedMessages, final BroadcastTypeMessages broadcastTypeMessages) {
    this.messagingService = messagingService;
    this.mobileTerminatedMessages = mobileTerminatedMessages;
    this.mobileOriginatedMessages = mobileOriginatedMessages;
    this.broadcastTypeMessages = broadcastTypeMessages;
  }

  public TMessagingService getMessagingService() {
    return messagingService;
  }

  public MobileTerminatedMessages getMobileTerminatedMessages() {
    return mobileTerminatedMessages;
  }

  public MobileOriginatedMessages getMobileOriginatedMessages() {
    return mobileOriginatedMessages;
  }

  public BroadcastTypeMessages getBroadcastTypeMessages() {
    return broadcastTypeMessages;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCSMSReadResult)) {
      return false;
    }
    ATpsCSMSReadResult that = (ATpsCSMSReadResult) object;

    return Objects.equals(messagingService, that.getMessagingService())
        && Objects.equals(mobileTerminatedMessages, that.getMobileTerminatedMessages())
        && Objects.equals(mobileOriginatedMessages, that.getMobileOriginatedMessages())
        && Objects.equals(broadcastTypeMessages, that.getBroadcastTypeMessages());
  }

}
