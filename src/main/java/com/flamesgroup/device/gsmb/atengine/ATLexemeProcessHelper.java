/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

public final class ATLexemeProcessHelper {

  private ATLexemeProcessHelper() {
  }

  public static boolean isValueInRange(final int value, final int from, final int to) {
    return value >= from && value <= to;
  }

  /**
   * Converts String representation of Integer to boolean.
   *
   * @param str "1" or "0"
   * @return true in case of <code>str</code> is "1", false - "0"
   * @throws IllegalStateException if <code>str</code> neither "0" nor "1"
   */
  public static boolean intStringToBoolean(final String str) {
    switch (str) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        throw new IllegalStateException(String.format("Can't convert boolean string %s - check AT Response regular expression for correctness", str));
    }
  }

}
