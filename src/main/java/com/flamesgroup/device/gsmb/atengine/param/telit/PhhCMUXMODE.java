/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public class PhhCMUXMODE {

  private PhhCMUXMODE() {
  }

  public enum Mode {

    OLD_BREAK_OCTECT_FORMAT_AND_IGNORE_DTR_FEATURE_IS_DISABLED(0), //default
    NEW_BREAK_OCTECT_FORMAT_AND_IGNORE_DTR_FEATURE_IS_DISABLED(1),
    OLD_BREAK_OCTECT_FORMAT_AND_IGNORE_DTR_FEATURE_IS_ENABLED(4),
    NEW_BREAK_OCTECT_FORMAT_AND_IGNORE_DTR_FEATURE_IS_ENABLED(5);

    private final int value;

    Mode(final int value) {
      this.value = value;

    }

    public int getValue() {
      return value;
    }

  }

}
