/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.siemens;

public class PctSRTC {

  private PctSRTC() {
  }

  public enum Type {

    MutesTheCurrentlyPlayedToneImmediately(0),
    Sequence1(1),
    Sequence2(2),
    Sequence3(3),
    Sequence4(4),
    Sequence5(5),
    Sequence6(6),
    Sequence7(7);

    private final int type;

    Type(final int type) {
      this.type = type;
    }

    public int getValue() {
      return type;
    }

  }

  public enum Volume {

    Mute(0),
    VeryLow(1),
    IdenticalWith1(2),
    Low(3),
    IdenticalWith3(4),
    Middle(5),
    IdenticalWith5(6),
    High(7);

    private final int volume;

    Volume(final int volume) {
      this.volume = volume;
    }

    public int getValue() {
      return volume;
    }

  }

}
