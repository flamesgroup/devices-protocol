/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PpsCGCLASS.Class;

import java.util.Objects;

/**
 * GPRS Mobile Station Class <br>
 * Set command sets the GPRS class according to class parameter.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=184">Paragraph 3.5.4.7.1.</a>
 */
public final class ATpsCGCLASSWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final Class cgclass) {
    Objects.requireNonNull(cgclass, "cgclass mustn't be null");
    return "AT+CGCLASS=\"" + cgclass.getValue() + "\"\r";
  }

  public ATpsCGCLASSWrite(final Class cgclass) {
    super(buildCommandATDataWithParamCheck(cgclass), 15000);
  }

}
