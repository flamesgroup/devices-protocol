/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCREG {

  private PpsCREG() {
  }

  public enum Mode {

    DISABLE_CREG_URC(0),
    ENABLE_CREG_URC(1),
    ENABLE_CREG_URC_WITH_LOCATION_INFORMATION(2);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum Status {

    NOT_REGISTERED_NOT_SEARCHING(0),
    REGISTERED_TO_HOME_NETWORK(1),
    NOT_REGISTERED_AND_SEARCHING(2),
    REGISTRATION_DENIED(3),
    UNKNOWN(4),
    REGISTERED_ROAMING(5);

    private final int value;

    Status(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Status getStatusByValue(final int value) {
      for (Status status : Status.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum URCPresentMode {

    DISABLE_CREG_URC(0),
    ENABLE_CREG_URC_STAT(1),
    ENABLE_CREG_URC_STAT_LAC_CI(2);

    private final int value;

    URCPresentMode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static URCPresentMode getModeByValue(final int value) {
      for (URCPresentMode mode : URCPresentMode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
