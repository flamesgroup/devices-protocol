/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.siemens;

public final class PctSAIC {

  private PctSAIC() {
  }

  public enum IO {

    DIGITAL_IO(1),
    ANALOG_IO(2);

    private final int value;

    IO(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static IO getIOSelectionByValue(final int value) {
      for (IO io : IO.values()) {
        if (io.getValue() == value) {
          return io;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Microphone {

    MICROPHONE1(1),
    MICROPHONE2(2);

    private final int value;

    Microphone(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Microphone getMicrophoneSelectionByValue(final int value) {
      for (Microphone mic : Microphone.values()) {
        if (mic.getValue() == value) {
          return mic;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum EarpieceAmplifier {

    AMPLIFIER1(1),
    AMPLIFIER2(2),
    BOTH_AMPLIFIERS(3);

    private final int value;

    EarpieceAmplifier(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static EarpieceAmplifier getEarpieceAmplifierByValue(final int value) {
      for (EarpieceAmplifier amplifier : EarpieceAmplifier.values()) {
        if (amplifier.getValue() == value) {
          return amplifier;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
