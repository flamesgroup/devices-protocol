/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.URCPresentation;

import java.util.Objects;

/**
 * Unstructured Supplementary Service Data <br>
 * Set command allows control of the Unstructured Supplementary Service Data (USSD [GSM 02.90]).
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=124">Paragraph 3.5.4.3.13.</a>
 */
public final class ATpsCUSDWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final URCPresentation urcPresentation, final String ussdString, final int dataCodingScheme) {
    Objects.requireNonNull(urcPresentation, "urcPresentation mustn't be null");
    Objects.requireNonNull(urcPresentation, "ussdString mustn't be null");
    return "AT+CUSD=" + urcPresentation.getValue() + ",\"" + ussdString + "\"," + dataCodingScheme + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final URCPresentation urcPresentation, final String ussdString) {
    Objects.requireNonNull(urcPresentation, "urcPresentation mustn't be null");
    Objects.requireNonNull(urcPresentation, "ussdString mustn't be null");
    return "AT+CUSD=" + urcPresentation.getValue() + ",\"" + ussdString + "\"\r";
  }

  private static String buildCommandATDataWithParamCheck(final URCPresentation urcPresentation) {
    Objects.requireNonNull(urcPresentation, "urcPresentation mustn't be null");
    return "AT+CUSD=" + urcPresentation.getValue() + "\r";
  }

  public ATpsCUSDWrite(final URCPresentation urcPresentation, final String ussdString, final int dataCodingScheme) {
    super(buildCommandATDataWithParamCheck(urcPresentation, ussdString, dataCodingScheme));
  }

  public ATpsCUSDWrite(final URCPresentation urcPresentation, final String ussdString) {
    super(buildCommandATDataWithParamCheck(urcPresentation, ussdString));

  }

  public ATpsCUSDWrite(final URCPresentation urcPresentation) {
    super(buildCommandATDataWithParamCheck(urcPresentation));
  }

}
