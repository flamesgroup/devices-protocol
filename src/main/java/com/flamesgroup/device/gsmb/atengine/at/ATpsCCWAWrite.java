/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCCWA.Class;
import com.flamesgroup.device.gsmb.atengine.param.PpsCCWA.Mode;

import java.util.EnumSet;

/**
 * Call Waiting <br>
 * Set command allows the control of the call waiting supplementary service. Activation, deactivation, and status query are supported.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=119"> Paragraph 3.5.4.3.11.</a>
 */
public final class ATpsCCWAWrite extends EmptyResponseSolidAT {

  /**
   * @param enableURC          enables/disables the presentation of an URC
   * @param mode               enables/disables the service at network level.
   *                           {@link Mode#QUERY_STATUS_OF_CALL_WAITING} is not supported
   * @param classOfInformation is a sum of integers each representing a class
   *                           of information which the command refers to
   */
  private static String buildCommandATDataWithParamCheck(final Boolean enableURC, final Mode mode, final EnumSet<Class> classOfInformation) {
    if (mode != null && mode == Mode.QUERY_STATUS_OF_CALL_WAITING) {
      throw new IllegalArgumentException(String.format("Mode value [%s] isn't supported", mode));
    }
    StringBuilder commandATDataLocal = new StringBuilder("AT+CCWA=");
    if (enableURC != null) {
      commandATDataLocal.append(enableURC ? 1 : 0);
    }

    if (mode != null) {
      commandATDataLocal.append(',').append(mode.getValue());
    }

    if (classOfInformation != null && !classOfInformation.isEmpty()) {
      int sum = 0;
      for (Class c : classOfInformation) {
        sum += c.getValue();
      }
      if (mode == null) {
        commandATDataLocal.append(',');
      }
      commandATDataLocal.append(',').append(sum);
    }
    return commandATDataLocal.append("\r").toString();
  }

  public ATpsCCWAWrite(final Boolean enableURC, final Mode mode, final EnumSet<Class> classOfInformation) {
    super(buildCommandATDataWithParamCheck(enableURC, mode, classOfInformation), 60000);
  }

}
