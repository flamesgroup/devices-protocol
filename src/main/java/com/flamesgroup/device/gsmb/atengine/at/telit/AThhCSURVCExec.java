/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

/**
 * Network Survey <br>
 * Execution command allows to perform a quick survey through band channels, starting from start channel to end channel. If parameters are omitted, a full band
 * scan is performed.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=539">Paragraph 3.5.7.11.2.</a>
 */
public final class AThhCSURVCExec extends AThhCSURVBaseExec {

  private static String buildCommandATDataWithParamCheck(final int startChannel, final int endChannel) {
    if (startChannel < 0 || endChannel < 0) {
      return "AT#CSURVC\r";
    } else {
      return "AT#CSURVC=" + startChannel + "," + endChannel + "\r";
    }
  }

  public AThhCSURVCExec(final int startChannel, final int endChannel) {
    super(buildCommandATDataWithParamCheck(startChannel, endChannel));
  }

  public AThhCSURVCExec() {
    this(Integer.MIN_VALUE, Integer.MIN_VALUE);
  }

}
