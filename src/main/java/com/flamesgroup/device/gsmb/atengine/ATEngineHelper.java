/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.ATErrorStatusException;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ATEngineHelper {

  private static final Logger logger = LoggerFactory.getLogger(ATEngineHelper.class);

  private ATEngineHelper() {
  }

  public static <T extends IATResult> T execute(final Object context, final IATExecutor atExecutor,
      final IAT<T> command, final int attempts, final int attemptInterval) throws ChannelException, InterruptedException {
    int attempt = 0;
    while (true) {
      try {
        return atExecutor.execute(command);
      } catch (ATErrorStatusException e) {
        if (++attempt >= attempts) {
          throw e;
        }
        logger.debug("[{}] - fail to execute [{}] on attempt [{}], try one more time after attempt interval", context, command, attempt, e);
      }
      Thread.sleep(attemptInterval);
    }
  }

}
