/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhIMCDEN {

  private PhhIMCDEN() {
  }

  public enum Mode {

    DISABLE_IMSI_CATCHER_DETECTION(0),
    ENABLE_IMSI_CATCHER_DETECTION(1);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Status {

    LOCATION_AREA_UPDATE(0),
    IMSI_CATCHER_DETECTED(1),
    NO_SUITABLE_CELLS(2);

    private final int value;

    Status(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Status getStatusByValue(final int value) {
      for (Status status : Status.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
