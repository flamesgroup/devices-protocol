/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCOPS {

  private PpsCOPS() {
  }

  public enum Mode {

    AUTOMATIC(0),
    MANUAL_OPERATOR_SELECTION(1),
    MANUALLY_DEREGISTER_FROM_NETWORK(2),
    SET_ONLY_FORMAT(3),
    AUTOMATIC_OR_MANUAL_SELECTION(4);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Format {

    LONG_ALPHANUMERIC(0),
    NUMERIC(2);

    private final int value;

    Format(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Format getFormatByValue(final int value) {
      for (Format format : Format.values()) {
        if (format.getValue() == value) {
          return format;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
