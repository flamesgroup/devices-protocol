/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

import java.util.Objects;

/**
 * Configure HTTP parameters <br>
 * This command sets the parameters needed to the HTTP connection.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=351">Paragraph 3.5.7.16.1.</a>
 */
public class AThhHTTPCFGWrite extends EmptyResponseSolidAT {

  private static void checkProfId(final int profId) {
    if (!ATLexemeProcessHelper.isValueInRange(profId, 0, 2)) {
      throw new IllegalArgumentException(String.format("Incorrect profId value [%d] - must be in range of 0-2", profId));
    }
  }

  private static void checkCommonParam(final int profId, final String serverAddress, final int serverPort, final int authType, final int sslEnable, final int timeout, final int cid) {
    checkProfId(profId);
    Objects.requireNonNull(serverAddress, "serverAddress mustn't be null");
    if (!ATLexemeProcessHelper.isValueInRange(serverPort, 1, 65535)) {
      throw new IllegalArgumentException(String.format("Incorrect profId value [%d] - must be in range of 1-65535", serverPort));
    }
    if (!ATLexemeProcessHelper.isValueInRange(authType, 0, 1)) {
      throw new IllegalArgumentException(String.format("Incorrect authType value [%d] - must be in range of 0-1", authType));
    }
    if (!ATLexemeProcessHelper.isValueInRange(sslEnable, 0, 1)) {
      throw new IllegalArgumentException(String.format("Incorrect sslEnable value [%d] - must be in range of 0-1", authType));
    }
    if (!ATLexemeProcessHelper.isValueInRange(timeout, 1, 65535)) {
      throw new IllegalArgumentException(String.format("Incorrect timeout value [%d] - must be in range of 1-65535", serverPort));
    }
    if (!ATLexemeProcessHelper.isValueInRange(cid, 1, 5)) {
      throw new IllegalArgumentException(String.format("Incorrect cid value [%d] - must be in range of 1-5", profId));
    }
  }

  private static String buildCommandATDataWithParamCheck(final int profId, final String serverAddress, final int serverPort, final int authType, final String username, final String password,
      final int sslEnable, final int timeout, final int cid) {
    checkCommonParam(profId, serverAddress, serverPort, authType, sslEnable, timeout, cid);
    Objects.requireNonNull(username, "username mustn't be null");
    Objects.requireNonNull(password, "password mustn't be null");
    return "AT#HTTPCFG=" + profId + ",\"" + serverAddress + "\"," + serverPort + "," + authType + ",\"" + username + "\",\"" + password + "\"," + sslEnable + "," + timeout + "," + cid + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final int profId, final String serverAddress, final int serverPort, final int authType, final int sslEnable, final int timeout,
      final int cid) {
    checkCommonParam(profId, serverAddress, serverPort, authType, sslEnable, timeout, cid);
    return "AT#HTTPCFG=" + profId + ",\"" + serverAddress + "\"," + serverPort + "," + authType + ",,," + sslEnable + "," + timeout + "," + cid + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final int profId) {
    checkProfId(profId);
    return "AT#HTTPCFG=" + profId + "\r";
  }

  public AThhHTTPCFGWrite(final int profId, final String serverAddress, final int serverPort, final int authType, final String username, final String password, final int sslEnable, final int timeout,
      final int cid) {
    super(buildCommandATDataWithParamCheck(profId, serverAddress, serverPort, authType, username, password, sslEnable, timeout, cid));
  }

  public AThhHTTPCFGWrite(final int profId, final String serverAddress, final int serverPort, final int authType, final int sslEnable, final int timeout,
      final int cid) {
    super(buildCommandATDataWithParamCheck(profId, serverAddress, serverPort, authType, sslEnable, timeout, cid));
  }

  public AThhHTTPCFGWrite(final int profId) {
    super(buildCommandATDataWithParamCheck(profId));
  }

}
