/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BroadcastReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BufferedURCHandlingMethod;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSDeliveringOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSStatusReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.UrcBufferingOption;

/**
 * New Message Indications <br>
 * Set command selects the behaviour of the device on how the receiving of new messages from the network is indicated to the DTE.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=218">Paragraph 3.5.5.3.1.</a>
 */
public abstract class ATpsCNMIWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final UrcBufferingOption urcBufferingOption, final SMSDeliveringOption smsDeliveringOption,
      final BroadcastReportingOption broadcastReportingOption, final SMSStatusReportingOption smsStatusReportingOption,
      final BufferedURCHandlingMethod bufferedURCHandlingMethod) {
    StringBuilder commandATDataBuilder = new StringBuilder("AT+CNMI=");
    if (urcBufferingOption != null) {
      commandATDataBuilder.append(urcBufferingOption.getValue());
      if (smsDeliveringOption != null) {
        commandATDataBuilder.append(',').append(smsDeliveringOption.getValue());
        if (broadcastReportingOption != null) {
          commandATDataBuilder.append(',').append(broadcastReportingOption.getValue());
          if (smsStatusReportingOption != null) {
            commandATDataBuilder.append(',').append(smsStatusReportingOption.getValue());
            if (bufferedURCHandlingMethod != null) {
              commandATDataBuilder.append(',').append(bufferedURCHandlingMethod.getValue());
            }
          }
        }
      }
    }
    return commandATDataBuilder.append("\r").toString();
  }

  public ATpsCNMIWrite(final UrcBufferingOption urcBufferingOption, final SMSDeliveringOption smsDeliveringOption,
      final BroadcastReportingOption broadcastReportingOption, final SMSStatusReportingOption smsStatusReportingOption,
      final BufferedURCHandlingMethod bufferedURCHandlingMethod) {
    super(buildCommandATDataWithParamCheck(urcBufferingOption, smsDeliveringOption, broadcastReportingOption, smsStatusReportingOption, bufferedURCHandlingMethod));
  }

}
