/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class ATctSMONCExecResult implements IATResult {

  private final List<CellInfo> cellList;

  public ATctSMONCExecResult(final List<CellInfo> cellList) {
    this.cellList = Collections.unmodifiableList(cellList);
  }

  public List<CellInfo> getCellList() {
    return cellList;
  }

  public static class CellInfo {

    private final int mcc;
    private final int mnc;
    private final int lac;
    private final int cell;
    private final int bsic;
    private final int chann;
    private final int rssi;
    private final int c1;
    private final int c2;

    /**
     * @param mcc   mobile country code
     * @param mnc   mobile network code
     * @param lac   location area code
     * @param cell  cellId - cell identifier
     * @param bsic  base station identity code
     * @param chann arfcn - absolute frequency channel number
     * @param rssi  received signal strength indicator
     * @param c1    cell selection criteria
     * @param c2    cell reselection criteria
     */
    public CellInfo(final int mcc, final int mnc, final int lac, final int cell, final int bsic, final int chann, final int rssi, final int c1, final int c2) {
      this.mcc = mcc;
      this.mnc = mnc;
      this.lac = lac;
      this.cell = cell;
      this.bsic = bsic;
      this.chann = chann;
      this.rssi = rssi;
      this.c1 = c1;
      this.c2 = c2;
    }

    public int getMcc() {
      return mcc;
    }

    public int getMnc() {
      return mnc;
    }

    public int getLac() {
      return lac;
    }

    public int getCell() {
      return cell;
    }

    public int getBsic() {
      return bsic;
    }

    public int getChann() {
      return chann;
    }

    public int getRssi() {
      return rssi;
    }

    public int getC1() {
      return c1;
    }

    public int getC2() {
      return c2;
    }

    @Override
    public boolean equals(final Object object) {
      if (object == this) {
        return true;
      }
      if (!(object instanceof CellInfo)) {
        return false;
      }
      CellInfo that = (CellInfo) object;

      return mcc == that.mcc
          && mnc == that.mnc
          && lac == that.lac
          && cell == that.cell
          && bsic == that.bsic
          && chann == that.chann
          && rssi == that.rssi
          && c1 == that.c1
          && c2 == that.c2;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 3;
      result = prime * result + mcc;
      result = prime * result + mnc;
      result = prime * result + lac;
      result = prime * result + cell;
      result = prime * result + bsic;
      result = prime * result + chann;
      result = prime * result + rssi;
      result = prime * result + c1;
      result = prime * result + c2;
      return result;
    }

    @Override
    public String toString() {
      return String.format("%s@%x:[%d/%d/%d/%d/%d/%d/%d/%d/%d]", getClass().getSimpleName(), hashCode(), mcc, mnc, lac, cell, bsic, chann, rssi, c1, c2);
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATctSMONCExecResult)) {
      return false;
    }
    ATctSMONCExecResult that = (ATctSMONCExecResult) object;

    return Objects.equals(cellList, that.getCellList());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(cellList);
    return result;
  }

}
