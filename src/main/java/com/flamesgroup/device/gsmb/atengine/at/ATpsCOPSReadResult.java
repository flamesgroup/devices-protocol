/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Format;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Mode;

import java.util.Objects;

public final class ATpsCOPSReadResult implements IATResult {

  private final Mode mode;
  private final Format format;
  private final String operator;

  public ATpsCOPSReadResult(final Mode mode, final Format format, final String operator) {
    this.mode = mode;
    this.format = format;
    this.operator = operator;
  }

  public Mode getMode() {
    return mode;
  }

  public Format getFormat() {
    return format;
  }

  public String getOperator() {
    return operator;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATpsCOPSReadResult)) {
      return false;
    }
    ATpsCOPSReadResult that = (ATpsCOPSReadResult) object;

    return Objects.equals(mode, that.getMode())
        && Objects.equals(format, that.getFormat())
        && Objects.equals(operator, that.getOperator());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(mode);
    result = prime * result + Objects.hashCode(format);
    result = prime * result + Objects.hashCode(operator);
    return result;
  }

}
