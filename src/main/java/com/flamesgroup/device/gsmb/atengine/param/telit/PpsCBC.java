/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PpsCBC {

  private PpsCBC() {
  }

  public enum BatteryStatus {

    ME_IS_POWERED_BY_THE_BATTERY(0),
    ME_HAS_A_BATTERY_CONNECTED(1),
    ME_DOES_NOT_HAVE_A_BATTERY_CONNECTED(2),
    RECOGNIZED_POWER_FAULT(3);

    private final int value;

    BatteryStatus(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static BatteryStatus getBatteryStatusByValue(final int value) {
      for (BatteryStatus status : BatteryStatus.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum BatteryChargeLevel {

    EXHAUSTED(0),
    CHARGE_REMAINED_25(25),
    CHARGE_REMAINED_50(50),
    CHARGE_REMAINED_75(75),
    FULLY_CHARGED_100(100);

    private final int value;

    BatteryChargeLevel(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static BatteryChargeLevel getBatteryChargeLevelByValue(final int value) {
      for (BatteryChargeLevel level : BatteryChargeLevel.values()) {
        if (level.getValue() == value) {
          return level;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
