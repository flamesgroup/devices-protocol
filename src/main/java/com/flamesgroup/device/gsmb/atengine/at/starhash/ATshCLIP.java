/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIP;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIP.Status;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ATshCLIP extends SolidAT<ATshCLIPResult> {

  private static final String COMMAND_TEMPLATE = "*#30#";

  private static final String AT_RESPONSE_TEMPLATE = "\\+CLIP: (?<n>[01]),(?<m>[0-2])";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private boolean displayURC;
  private PshCLIP.Status status;

  protected ATshCLIP(final String commandATData) {
    super(commandATData, 5000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      displayURC = ATLexemeProcessHelper.intStringToBoolean(matcher.group("n"));
      status = Status.getStatusByValue(Integer.parseInt(matcher.group("m")));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATshCLIPResult getCommandATResult() {
    return new ATshCLIPResult(displayURC, status);
  }

  public static boolean isCLIPCode(final String code) {
    return COMMAND_TEMPLATE.equals(code);
  }

}
