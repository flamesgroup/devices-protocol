/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper.isValueInRange;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

/**
 * SIM Toolkit Interface Activation
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=555">Paragraph 3.5.7.12.1.</a>
 */
public final class AThhSTIAWrite extends EmptyResponseSolidAT {

  private static String buildCommandATDataWithParamCheck(final int mode) {
    if (!(isValueInRange(mode, 0, 3) || isValueInRange(mode, 17, 19) || isValueInRange(mode, 33, 35))) {
      throw new IllegalArgumentException("mode must be in ranges of (0-3,17-19,33-35), but it's " + mode);
    }

    return "AT#STIA=" + mode + "\r";
  }

  private static String buildCommandATDataWithParamCheck(final int mode, final int timeout) {
    if (!(isValueInRange(mode, 0, 3) || isValueInRange(mode, 17, 19) || isValueInRange(mode, 33, 35))) {
      throw new IllegalArgumentException("mode must be in ranges of (0-3,17-19,33-35), but it's " + mode);
    }
    if (!isValueInRange(timeout, 1, 255)) {
      throw new IllegalArgumentException("timeout must be in ranges of (1-255), but it's " + timeout);
    }

    return "AT#STIA=" + mode + "," + timeout + "\r";
  }

  public AThhSTIAWrite(final int mode) {
    super(buildCommandATDataWithParamCheck(mode));
  }

  public AThhSTIAWrite(final int mode, final int timeout) {
    super(buildCommandATDataWithParamCheck(mode, timeout));
  }

}
