/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param;

public final class PpsCUSD {

  private PpsCUSD() {
  }

  public enum URCPresentation {

    DISABLE(0),
    ENABLE(1),
    CANCEL_SESSION(2);

    private final int value;

    URCPresentation(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum Status {

    NO_FURTHER_USER_ACTION_REQUIRED(0, "no further user action required"),
    FURTHER_USER_ACTION_REQUIRED(1, "further user action required"),
    USSD_TERMINATED_BY_NETWORK(2, "USSD terminated by the network"),
    OTHER_LOCAL_CLIENT_HAS_RESPONDED(3, "other local client has responded"), //only telit supported
    OPERATION_NOT_SUPPORTED(4, "operation not supported"), //only telit supported
    NETWORK_TIME_OUT(5, "network time out"); //only telit supported

    private final int value;
    private final String description;

    Status(final int value, final String description) {
      this.value = value;
      this.description = description;
    }

    public int getValue() {
      return value;
    }

    public String getDescription() {
      return description;
    }

    public static Status getStatusByValue(final int value) {
      for (Status status : Status.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
