/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import com.flamesgroup.device.gsmb.atengine.param.PpsCIEV.Indicator;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URCpsCIEV implements IURC {

  private static final String URC_TEMPLATE = "\\+CIEV: (?<indDescr>signal|rssi),(?<indValue1>\\d+)(,(?<indValue2>\\d+))?";
  private static final Pattern urcPattern = Pattern.compile(URC_TEMPLATE);

  private final IURCpsCIEVHandler handler;

  public URCpsCIEV(final IURCpsCIEVHandler handler) {
    Objects.requireNonNull(handler, "handler mustn't be null");
    this.handler = handler;
  }

  @Override
  public boolean process(final String atLexeme) {
    Matcher matcher = urcPattern.matcher(atLexeme);
    if (matcher.matches()) {
      String indDescr = matcher.group("indDescr");
      int indValue1 = Integer.parseInt(matcher.group("indValue1"));
      int indValue2 = -1;
      if (matcher.group("indValue2") != null) {
        indValue2 = Integer.parseInt(matcher.group("indValue2"));
      }
      Indicator indicator = Indicator.getIndicatorByValue(indDescr);

      handler.handleIndicatorChanges(indicator, indValue1, indValue2);
      return true;
    } else {
      return false;
    }
  }

}
