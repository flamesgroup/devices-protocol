/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

/**
 * Lock to single BCCH ARFCN <br>
 * This command allows to set the single BCCH ARFCN the device must be locked to, selectable within those allowed for the specific product
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=343">Paragraph 3.5.7.1.164.</a>
 */
public final class AThhBCCHLOCKWrite extends EmptyResponseSolidAT {

  public static final int DISABLES_BCCH_LOCKING_ARFCN = 1024;

  private static String buildCommandATDataWithParamCheck(final int arfcn) {
    if (!(arfcn == 1024 ||
        ATLexemeProcessHelper.isValueInRange(arfcn, 0, 124) ||
        ATLexemeProcessHelper.isValueInRange(arfcn, 975, 1023) ||
        ATLexemeProcessHelper.isValueInRange(arfcn, 512, 885) ||
        ATLexemeProcessHelper.isValueInRange(arfcn, 128, 251))) {
      throw new IllegalArgumentException("arfcn must be in ranges of (1024, 0-124, 975-1023, 512-885, 128-251), but it's " + arfcn);
    }
    return "AT#BCCHLOCK=" + arfcn + "\r";
  }

  /**
   * Disables BCCH locking
   */
  public AThhBCCHLOCKWrite() {
    this(DISABLES_BCCH_LOCKING_ARFCN);
  }

  /**
   * Lock to single BCCH ARFCN
   *
   * @param arfcn assigned absolute radio-frequency channel number (1024, 0-124, 975-1023, 512-885, 128-251)
   */
  public AThhBCCHLOCKWrite(final int arfcn) {
    super(buildCommandATDataWithParamCheck(arfcn));
  }

}
