/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhHTTPQRY;

import java.util.Objects;

/**
 * Send HTTP GET, HEAD or DELETE request <br>
 * Execution command performs a GET, HEAD or DELETE request to HTTP server.
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=597">Paragraph 3.5.7.16.2.</a>
 */
public class AThhHTTPQRYWrite extends EmptyResponseSolidAT {

  private static void checkCommonParam(final int profId, final PhhHTTPQRY.HttpMethod httpMethod, final String resource) {
    if (!ATLexemeProcessHelper.isValueInRange(profId, 0, 2)) {
      throw new IllegalArgumentException(String.format("Incorrect profId value [%d] - must be in range of 0-2", profId));
    }
    Objects.requireNonNull(httpMethod, "httpMethod mustn't be null");
    Objects.requireNonNull(resource, "resource mustn't be null");
  }

  private static String buildCommandATDataWithParamCheck(final int profId, final PhhHTTPQRY.HttpMethod httpMethod, final String resource, final String extraHeaderLine) {
    checkCommonParam(profId, httpMethod, resource);
    Objects.requireNonNull(extraHeaderLine, "extraHeaderLine mustn't be null");
    return "AT#HTTPQRY=" + profId + "," + httpMethod.getValue() + ",\"" + resource + "\",\"" + extraHeaderLine + "\"\r";
  }

  private static String buildCommandATDataWithParamCheck(final int profId, final PhhHTTPQRY.HttpMethod httpMethod, final String resource) {
    checkCommonParam(profId, httpMethod, resource);
    return "AT#HTTPQRY=" + profId + "," + httpMethod.getValue() + ",\"" + resource + "\"\r";
  }

  public AThhHTTPQRYWrite(final int profId, final PhhHTTPQRY.HttpMethod httpMethod, final String resource) {
    super(buildCommandATDataWithParamCheck(profId, httpMethod, resource), 60_000);
  }

  public AThhHTTPQRYWrite(final int profId, final PhhHTTPQRY.HttpMethod httpMethod, final String resource, final String extraHeaderLine) {
    super(buildCommandATDataWithParamCheck(profId, httpMethod, resource, extraHeaderLine), 60_000);
  }

}
