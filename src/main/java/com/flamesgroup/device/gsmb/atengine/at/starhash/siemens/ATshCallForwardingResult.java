/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.siemens;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCallForwarding;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCallForwarding.Reason;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCallForwarding.Type;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public final class ATshCallForwardingResult implements IATResult {

  private final List<CallForwardingInfo> callForwardingInfoList;

  public ATshCallForwardingResult(final List<CallForwardingInfo> callForwardingInfoList) {
    this.callForwardingInfoList = Collections.unmodifiableList(callForwardingInfoList);
  }

  public List<CallForwardingInfo> getCallForwardingInfoList() {
    return callForwardingInfoList;
  }

  public static class CallForwardingInfo {

    private final Reason reason;
    private final boolean active;
    private final Set<ServiceClass> serviceClasses;
    private final String number;
    private final PshCallForwarding.Type type;
    private final long time;

    public CallForwardingInfo(final Reason reason, final boolean active, final Set<ServiceClass> serviceClasses) {
      this(reason, active, serviceClasses, null, null, -1);
    }

    public CallForwardingInfo(final Reason reason, final boolean active, final Set<ServiceClass> serviceClasses, final String number,
        final Type type, final long time) {
      this.reason = reason;
      this.active = active;
      this.serviceClasses = serviceClasses;
      this.number = number;
      this.type = type;
      this.time = time;
    }

    public Reason getReason() {
      return reason;
    }

    public boolean isActive() {
      return active;
    }

    public String getNumber() {
      return number;
    }

    public Type getType() {
      return type;
    }

    public long getTime() {
      return time;
    }

    public Set<ServiceClass> getServiceClasses() {
      return serviceClasses;
    }

    @Override
    public boolean equals(final Object object) {
      if (object == this) {
        return true;
      }
      if (!(object instanceof CallForwardingInfo)) {
        return false;
      }
      CallForwardingInfo that = (CallForwardingInfo) object;

      return Objects.equals(reason, that.getReason())
          && active == that.isActive()
          && Objects.equals(serviceClasses, that.getServiceClasses())
          && Objects.equals(number, that.getNumber())
          && Objects.equals(type, that.getType())
          && time == that.getTime();
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 3;
      result = prime * result + Objects.hashCode(reason);
      result = prime * result + Boolean.hashCode(active);
      result = prime * result + Objects.hashCode(serviceClasses);
      result = prime * result + Objects.hashCode(number);
      result = prime * result + Objects.hashCode(type);
      result = prime * result + Long.hashCode(time);
      return result;
    }

  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATshCallForwardingResult)) {
      return false;
    }
    ATshCallForwardingResult that = (ATshCallForwardingResult) object;

    return Objects.equals(callForwardingInfoList, that.getCallForwardingInfoList());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(callForwardingInfoList);
    return result;
  }

}
