/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ATshCallWaiting extends SolidAT<ATshCallWaitingResult> {

  private static final String COMMAND_TEMPLATE = "(?:(?:\\*#?)|(?:\\*?#))43(?:\\*[0-9]{0,2})?#";
  private static final Pattern commandPattern = Pattern.compile(COMMAND_TEMPLATE);

  private static final String AT_RESPONSE_TEMPLATE = "\\+CCWA: (?<status>[01]),(?<class>[1-9]{1,3})";
  private static final Pattern atResponsePattern = Pattern.compile(AT_RESPONSE_TEMPLATE);

  private final List<ATshCallWaitingResult.CallWaitingInfo> callWaitingInfos = new ArrayList<>();

  protected ATshCallWaiting(final String commandATData) {
    super(commandATData, 5000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    Matcher matcher = atResponsePattern.matcher(lexeme);
    if (matcher.matches()) {
      boolean active = ATLexemeProcessHelper.intStringToBoolean(matcher.group("status"));
      Set<ServiceClass> serviceClasses = ServiceClass.getServiceClassesByValue(Integer.parseInt(matcher.group("class")));
      callWaitingInfos.add(new ATshCallWaitingResult.CallWaitingInfo(active, serviceClasses));
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected ATshCallWaitingResult getCommandATResult() {
    return new ATshCallWaitingResult(callWaitingInfos);
  }

  public static boolean isCallWaitingCode(final String code) {
    return commandPattern.matcher(code).matches();
  }

}
