/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;

import java.util.Objects;

public final class USSDResponse extends EmptyResponseSolidAT {

  private final boolean close;

  private boolean receivedFirsResponse;

  private static String buildCommandATDataWithParamCheck(final String response, final boolean close) {
    Objects.requireNonNull(response, "response mustn't be null");
    String commandATData = response;
    if (close) {
      commandATData += ESC;
    } else {
      commandATData += CTRL_Z;
    }
    return commandATData;
  }

  public USSDResponse(final String response, final boolean close) {
    super(buildCommandATDataWithParamCheck(response, close));
    this.close = close;
  }

  @Override
  public boolean isWaitBeforeNextAT() {
    return false;
  }

  @Override
  protected boolean internalProcessATOKStatus(final String lexeme) {
    if (close && receivedFirsResponse && "+CME ERROR: ss not executed".equals(lexeme)) {
      return true;
    } else {
      return super.internalProcessATOKStatus(lexeme);
    }
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    if (close && "\r+CME ERROR: unknown".equals(lexeme)) {
      receivedFirsResponse = true;
      return true;
    } else {
      return false;
    }
  }

}
