/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhTSVOL {

  private PhhTSVOL() {
  }

  public enum ClassOfTones {

    GSM_TONES(1),
    RINGER_TONES(2),
    ALARM_TONES(4),
    SIGNALLING_TONES(8),
    DTMF_TONES(16),
    SIM_TOOLKIT_TONES(32),
    USER_DEFINED_TONES(64),
    DIAL_TONES(128),
    ALL_CLASSES(255);

    private final int value;

    ClassOfTones(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
