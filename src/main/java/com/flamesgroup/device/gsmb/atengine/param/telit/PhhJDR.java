/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.telit;

public final class PhhJDR {

  private PhhJDR() {
  }

  public enum Mode {

    DISABLE_JDR(0),
    ENABLE_GPIO2_JDR(1),
    ENABLE_URC_JDR(2),
    ENABLE_GPIO2_AND_URC_JDR(3),
    ENABLE_URC_JDR_WITH_3_SEC_INTERVAL(4),
    ENABLE_GPIO2_AND_URC_JDR_WITH_3_SEC_INTERVAL(5),
    ENABLE_URC_JDR_WITH_UNKNOWN_STATUS_10_0X_XXX_RELEASE(6);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static Mode getModeByValue(final int value) {
      for (Mode mode : Mode.values()) {
        if (mode.getValue() == value) {
          return mode;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

  public enum Status {

    JAMMED("JAMMED"), // Jammed condition detected
    OPERATIVE("OPERATIVE"), // Normal Operating condition restored
    UNKNOWN("UNKNOWN"); // default state before first successful PLMN searching

    private final String value;

    Status(final String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    public static Status getStatusByValue(final String value) {
      for (Status status : Status.values()) {
        if (status.getValue().equals(value)) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
