/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.ATEngineException;
import com.flamesgroup.device.gsmb.atengine.ATEngineTimeoutException;
import com.flamesgroup.device.gsmb.atengine.IATEngine;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.Status;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCUSDHandler;
import com.flamesgroup.device.helper.UCS2Convertor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BaseUSSDSession implements IUSSDSession, IURCpsCUSDHandler {

  private static final Logger logger = LoggerFactory.getLogger(BaseUSSDSession.class);

  private static final int TIMEOUT = 3 * 60000;

  protected final IATEngine atEngine;

  private final Lock sessionLock = new ReentrantLock();
  private final Lock requestLock = new ReentrantLock();
  private final Condition requestCondition = requestLock.newCondition();

  private boolean sessionOpen; // guarded by sessionLock
  private boolean sessionTerminatedByNetwork; // guarded by sessionLock
  private String ussdRequest; // guarded by requestLock
  private boolean ussdRequestSessionTerminatedByNetwork; // guarded by requestLock

  public BaseUSSDSession(final IATEngine atEngine) {
    this.atEngine = atEngine;
  }

  protected <T extends IATResult> T executeWithinUSSDSession(final IAT<T> command) throws ChannelException, InterruptedException {
    return atEngine.execute(command);
  }

  @Override
  public void open(final String ussdString) throws ChannelException, InterruptedException {
    sessionLock.lock();
    try {
      if (sessionOpen) {
        throw new IllegalStateException("Can't open USSD session - USSD session has been already opened");
      } else {
        executeOpenAction(ussdString);

        sessionOpen = true;
        sessionTerminatedByNetwork = false;
      }
    } finally {
      sessionLock.unlock();
    }
  }

  protected abstract void executeOpenAction(String ussdString) throws ChannelException, InterruptedException;

  @Override
  public void close() throws ChannelException, InterruptedException {
    sessionLock.lock();
    try {
      if (!sessionOpen) {
        throw new IllegalStateException("Can't close USSD session - USSD session hasn't been opened");
      } else {
        requestLock.lock();
        try {
          sessionTerminatedByNetwork = ussdRequestSessionTerminatedByNetwork;
        } finally {
          requestLock.unlock();
        }

        if (!sessionTerminatedByNetwork) {
          executeCloseAction();
        }
      }
    } finally {
      sessionOpen = false;
      sessionLock.unlock();
    }
  }

  protected abstract void executeCloseAction() throws ChannelException, InterruptedException;

  @Override
  public String readRequest() throws ATEngineException, InterruptedException {
    sessionLock.lock();
    try {
      if (!sessionOpen) {
        throw new IllegalStateException("Can't read request - USSD session hasn't been opened");
      } else {
        if (sessionTerminatedByNetwork) {
          throw new IllegalStateException("Can't read request - USSD session has been terminated by network");
        }

        String localUSSDRequest;
        requestLock.lock();
        try {
          if (ussdRequest == null) {
            if (!requestCondition.await(TIMEOUT, TimeUnit.MILLISECONDS)) {
              throw new ATEngineTimeoutException(String.format("[%s] - timeout for waiting response elapsed", this));
            }
          }
          localUSSDRequest = ussdRequest;
          ussdRequest = null;
          sessionTerminatedByNetwork = ussdRequestSessionTerminatedByNetwork;
        } finally {
          requestLock.unlock();
        }
        return localUSSDRequest;
      }
    } finally {
      sessionLock.unlock();
    }
  }

  @Override
  public void writeResponse(final String response) throws ChannelException, InterruptedException {
    sessionLock.lock();
    try {
      if (!sessionOpen) {
        throw new IllegalStateException("Can't write respons - USSD session hasn't been opened");
      } else {
        requestLock.lock();
        try {
          sessionTerminatedByNetwork = ussdRequestSessionTerminatedByNetwork;
        } finally {
          requestLock.unlock();
        }

        if (sessionTerminatedByNetwork) {
          throw new IllegalStateException("Can't write request - USSD session has been terminated by network");
        }

        executeResponseAction(response);
      }
      sessionOpen = true;
    } finally {
      sessionLock.unlock();
    }
  }

  protected abstract void executeResponseAction(final String response) throws ChannelException, InterruptedException;

  @Override
  public void handleUssd(final Status status, String message, final int dataCodingScheme) {
    int codingGroup = (dataCodingScheme & 0xF0) >> 4;
    if ((codingGroup >= 0b0000 && codingGroup <= 0b0100) || (codingGroup == 0b1111 && (dataCodingScheme & 0b0100) == 0)) {
      // GSM 03.38 default alphabet is used
      try {
        message = UCS2Convertor.hexToString(message); // use current TE character set which set in init method of *Unit
      } catch (UnsupportedEncodingException e) {
        logger.error(String.format("[%s] - can't parse URC [+CUSD] with message [%s], pass as is", this, message), e);
      }
    } else {
      logger.error("[{}] - correct processing message with coding scheme [{}] isn't implemented, pass as is", this, dataCodingScheme);
    }

    requestLock.lock();
    try {
      processUSSDHandler(status, message, dataCodingScheme);
    } finally {
      requestLock.unlock();
    }
  }

  protected void processUSSDHandler(final Status status, String message, final int dataCodingScheme) {
    if (message == null) {
      message = status.getDescription();
    }
    if (status == Status.FURTHER_USER_ACTION_REQUIRED) {
      setUssdRequestAndNotifyWaiter(message, false);
    } else {
      logger.debug("[{}] - [{}]", this, message);
      setUssdRequestAndNotifyWaiter(message, true);
    }
  }

  protected final void setUssdRequestAndNotifyWaiter(final String ussdRequest, final boolean ussdRequestSessionTerminatedByNetwork) {
    requestLock.lock();
    try {
      this.ussdRequest = ussdRequest;
      this.ussdRequestSessionTerminatedByNetwork = ussdRequestSessionTerminatedByNetwork;
      requestCondition.signalAll();
    } finally {
      requestLock.unlock();
    }
  }

}
