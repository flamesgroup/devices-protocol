/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIR.StatusOnMobile;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCLIR.StatusOnNetwork;

import java.util.Objects;

public final class ATshCLIRResult implements IATResult {

  private final StatusOnMobile statusOnMobile;
  private final StatusOnNetwork statusOnNetwork;

  public ATshCLIRResult(final StatusOnMobile statusOnMobile, final StatusOnNetwork status) {
    this.statusOnMobile = statusOnMobile;
    this.statusOnNetwork = status;
  }

  public StatusOnMobile getStatusOnMobile() {
    return statusOnMobile;
  }

  public StatusOnNetwork getStatusOnNetwork() {
    return statusOnNetwork;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof ATshCLIRResult)) {
      return false;
    }
    ATshCLIRResult that = (ATshCLIRResult) object;

    return Objects.equals(statusOnMobile, that.getStatusOnMobile())
        && Objects.equals(statusOnNetwork, that.getStatusOnNetwork());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + Objects.hashCode(statusOnMobile);
    result = prime * result + Objects.hashCode(statusOnNetwork);
    return result;
  }

}
