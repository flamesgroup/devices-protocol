/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import com.flamesgroup.device.gsmb.atengine.ATLexemeProcessHelper;
import com.flamesgroup.device.gsmb.atengine.at.SolidAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Receive HTTP server data <br>
 *
 * @see <a href="http://www.telit.com/fileadmin/user_upload/products/Downloads/2G/Telit_AT_Commands_Reference_Guide_r23.pdf#page=601">Paragraph 3.5.7.16.4.</a>
 */
public final class AThhHTTPRCVRead extends SolidAT<AThhHTTPRCVReadResult> {

  private static final String AT_RESPONSE_START_TEMPLATE = "<<<(?<htmlStartSource>(.*\\n*\\r*\\s*)*)";
  private static final String AT_RESPONSE_BODY_TEMPLATE = "(?<htmlBodySource>(.*\\n*\\r*\\s*)*)";

  private static final Pattern atResponseStartPattern = Pattern.compile(AT_RESPONSE_START_TEMPLATE);
  private static final Pattern atResponseBodyPattern = Pattern.compile(AT_RESPONSE_BODY_TEMPLATE);

  private String htmlSource;
  private boolean httpReadStarted;

  private static String buildCommandWithParamCheck(final int profId, final int maxByte) {
    if (!ATLexemeProcessHelper.isValueInRange(profId, 0, 2)) {
      throw new IllegalArgumentException(String.format("Incorrect profId value [%d] - must be in range of 0-2", profId));
    }
    if ((maxByte != 0 && maxByte < 64) || maxByte > 1500) {
      throw new IllegalArgumentException(String.format("Incorrect maxByte value [%d] - must be in range of 0,64-1500", maxByte));
    }
    return "AT#HTTPRCV=" + profId + "," + maxByte + "\r";
  }

  private static String buildCommandWithParamCheck(final int profId) {
    if (!ATLexemeProcessHelper.isValueInRange(profId, 0, 2)) {
      throw new IllegalArgumentException(String.format("Incorrect profId value [%d] - must be in range of 0-2", profId));
    }
    return "AT#HTTPRCV=" + profId + "\r";
  }

  public AThhHTTPRCVRead(final int profId, final int maxByte) {
    super(buildCommandWithParamCheck(profId, maxByte), 60_000);
  }

  public AThhHTTPRCVRead(final int profId) {
    super(buildCommandWithParamCheck(profId), 60_000);
  }

  @Override
  protected boolean internalProcessATResponse(final String lexeme) {
    if (httpReadStarted) {
      Matcher matcherBody = atResponseBodyPattern.matcher(lexeme);
      if (matcherBody.matches()) {
        htmlSource += matcherBody.group("htmlBodySource");
        return true;
      }
    } else {
      Matcher matcherStart = atResponseStartPattern.matcher(lexeme);
      if (matcherStart.matches()) {
        htmlSource = matcherStart.group("htmlStartSource");
        httpReadStarted = true;
        return true;
      }
    }
    return false;
  }

  @Override
  protected AThhHTTPRCVReadResult getCommandATResult() {
    return new AThhHTTPRCVReadResult(htmlSource);
  }

}
