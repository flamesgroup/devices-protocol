/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.param.siemens;

public final class PctSMGO {

  private PctSMGO() {
  }

  public enum SMSOverflowStatus {

    SPACE_AVAILABLE(0),
    SMS_BUFFER_FULL(1),
    SMS_BUFFER_FULL_AND_NEW_MESSAGE_WAIT_IN_SC(2);

    private final int value;

    SMSOverflowStatus(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public static SMSOverflowStatus getSMSOverflowStatusByValue(final int value) {
      for (SMSOverflowStatus status : SMSOverflowStatus.values()) {
        if (status.getValue() == value) {
          return status;
        }
      }
      throw new IllegalArgumentException("Unsupported value: " + value);
    }

  }

}
