/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.IndicationChannelEmpty;
import com.flamesgroup.device.commonb.LoggerChannel;
import com.flamesgroup.device.commonb.ServiceChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GSMB2Device extends GSMGenericDevice {

  private static final Logger logger = LoggerFactory.getLogger(GSMB2Device.class);

  public static final int CPU_ENTRY_COUNT = 3;
  public static final int GSM_ENTRY_COUNT = 2;

  private final boolean restricted;

  public GSMB2Device(final IMudpConnection mudpConnection, final DeviceUID deviceUid, final boolean restricted) {
    super(new IMudpConnection[] {mudpConnection}, deviceUid);
    this.restricted = restricted;
  }

  public GSMB2Device(final IMudpConnection mudpConnection, final DeviceUID deviceUid) {
    this(mudpConnection, deviceUid, false);
  }

  public GSMB2Device(final IMudpConnection mudpConnection) {
    this(mudpConnection, null);
  }

  @Override
  public DeviceType getDeviceType() {
    return restricted ? DeviceType.GSMB2_RESTRICTED : DeviceType.GSMB2;
  }

  @Override
  public int getCPUEntryCount() {
    return CPU_ENTRY_COUNT;
  }

  @Override
  protected ICPUEntry createCPUEntry(final int cpuEntryNumber, final DeviceUID deviceUid) {
    checkCPUEntryNumber(cpuEntryNumber);

    int realCpuN;
    if (cpuEntryNumber == 0) {
      realCpuN = 0;
    } else if (cpuEntryNumber == 1) {
      realCpuN = 9;
    } else {
      realCpuN = 1;
    }
    int serviceRealChannelN = 0;
    int loggerRealChannelN = 1;

    final IServiceChannel serviceChannel = new ServiceChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, serviceRealChannelN), cpuEntryNumber, deviceUid);
    final ILoggerChannel loggerChannel = new LoggerChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, loggerRealChannelN), cpuEntryNumber, deviceUid);
    return new CPUEntry(serviceChannel, loggerChannel, cpuEntryNumber, deviceUid);
  }

  @Override
  public int getGSMEntryCount() {
    return GSM_ENTRY_COUNT;
  }

  @Override
  protected IGSMEntry createGSMEntry(final int gsmEntryNumber, final DeviceUID deviceUid) {
    checkGSMEntryNumber(gsmEntryNumber);

    int realCpuN = gsmEntryNumber == 0 ? 9 : 1;
    int gsmRealChannelN = 2;
    int audioRealChannelN = 3;
    int scEmulatorRealChannelN = 4;

    final IGSMChannel gsmChannel = new GSMChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, gsmRealChannelN), gsmEntryNumber, deviceUid);
    final IAudioChannel audioChannel = new AudioChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, audioRealChannelN), gsmEntryNumber, deviceUid);
    final ISCEmulatorChannel scEmulatorChannel = new SCEmulatorChannel(createMudpChannelFor(0), createMudpChannelAddressTypeAFor(realCpuN, scEmulatorRealChannelN), gsmEntryNumber, deviceUid);
    return new GSMB2Entry(gsmChannel, audioChannel, scEmulatorChannel, gsmEntryNumber, deviceUid);
  }

  private static final class GSMB2Entry extends GSMBGenericEntry {

    public GSMB2Entry(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCEmulatorChannel scEmulatorChannel, final int gsmEntryNumber, final DeviceUID deviceUid) {
      super(gsmChannel, audioChannel, scEmulatorChannel, new IndicationChannelEmpty(), gsmEntryNumber, deviceUid);

      if (logger.isTraceEnabled()) {
        logger.trace("[{}] - created with [{}/{}/{}]", toString(), gsmChannel, audioChannel, scEmulatorChannel);
      }
    }

  }

}
