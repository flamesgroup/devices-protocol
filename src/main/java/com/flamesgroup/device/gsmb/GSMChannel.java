/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelBusyException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.gsmb.command.ATAlreadyStartedErrException;
import com.flamesgroup.device.gsmb.command.GetIMEICommand;
import com.flamesgroup.device.gsmb.command.SetIMEICommand;
import com.flamesgroup.device.gsmb.command.StartATCommand;
import com.flamesgroup.device.gsmb.command.StartTraceCommand;
import com.flamesgroup.device.gsmb.command.StopATCommand;
import com.flamesgroup.device.gsmb.command.StopTraceCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GSMChannel extends Channel implements IGSMChannel, IRawATSubChannel, IATSubChannel {

  private static final Logger logger = LoggerFactory.getLogger(GSMChannel.class);

  public static final byte COMMAND_AT_DATA = (byte) 0x00;
  public static final byte COMMAND_START_AT = (byte) 0x01;
  public static final byte COMMAND_STOP_AT = (byte) 0x02;
  public static final byte COMMAND_GET_IMEI = (byte) 0x03;
  public static final byte COMMAND_SET_IMEI = (byte) 0x04;
  public static final byte TRACE_DATA = (byte) 0x05;
  public static final byte COMMAND_START_TRACE = (byte) 0x06;
  public static final byte COMMAND_STOP_TRACE = (byte) 0x07;

  private final Lock atSubChannelLock = new ReentrantLock();
  private final StartATCommand startATCommand;
  private final StopATCommand stopATCommand;
  private final Lock getIMEILock = new ReentrantLock();
  private final GetIMEICommand getIMEICommand;
  private final Lock setIMEILock = new ReentrantLock();
  private final SetIMEICommand setIMEICommand;

  private final IChannelData atSubChannelData;

  private final ITraceSubChannel traceSubChannel;

  private final Charset atCharset = Charset.forName("ISO-8859-1");

  public GSMChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int gsmEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, gsmEntryNumber, deviceUid);

    startATCommand = new StartATCommand(COMMAND_START_AT, createChannelData(), 3000 + 100 + getMaxAttemptTimeout()); // real process time (ms): Telit 1180 (timeout on 3000), Cinterion 100
    stopATCommand = new StopATCommand(COMMAND_STOP_AT, createChannelData(), 1500 + 100 + getMaxAttemptTimeout()); // real process time (ms): Telit 1500, Cinterion 500
    getIMEICommand = new GetIMEICommand(COMMAND_GET_IMEI, createChannelData(), 20000 + getMaxAttemptTimeout()); // real process time (ms): Telit 5860 (max 20000), Cinterion 2195 (max 2250)
    setIMEICommand = new SetIMEICommand(COMMAND_SET_IMEI, createChannelData(), 165000 + getMaxAttemptTimeout()); // real process time (ms): Telit 6190 (max 22000), Cinterion 2497 (max 165000)

    atSubChannelData = createChannelData();

    traceSubChannel = new TraceSubChannel();
  }

  public GSMChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int gsmEntryNumber) {
    this(mudpChannel, mudpChannelAddress, gsmEntryNumber, null);
  }

  // IGSMChannel
  @Override
  public IRawATSubChannel getRawATSubChannel() {
    return this;
  }

  @Override
  public IATSubChannel getATSubChannel() {
    return this;
  }

  @Override
  public byte[] getIMEI() throws ChannelException, InterruptedException {
    final int attemptPeriodMillis = 5000;
    final int attemptDurationMillis = 60000;
    int attempt = 0;
    byte[] imei;
    ChannelBusyException channelBusyException;
    getIMEILock.lock();
    try {
      do {
        try {
          try {
            imei = getIMEICommand.execute(getIMEICommand.createRequest()).getIMEI();
          } catch (ATAlreadyStartedErrException e) {
            logger.info("[{}] - attempt to get IMEI on already started AT, stop AT channel", this);
            stop();
            imei = getIMEICommand.execute(getIMEICommand.createRequest()).getIMEI();
          }
          if (logger.isDebugEnabled()) { // to prevent unnecessary String instance creation
            logger.debug("[{}] - gotten IMEI [{}]", this, new String(imei));
          }
          return imei;
        } catch (ChannelBusyException e) {
          channelBusyException = e;
          logger.warn(String.format("[%s] - can't get IMEI during [%d] attempt", this, attempt + 1), e);
          Thread.sleep(attemptPeriodMillis);
        }
      } while (++attempt < attemptDurationMillis / attemptPeriodMillis);
    } finally {
      getIMEILock.unlock();
    }
    throw channelBusyException;
  }

  @Override
  public void setIMEI(final byte[] imei) throws ChannelException, InterruptedException {
    final int attemptPeriodMillis = 5000;
    final int attemptDurationMillis = 60000;
    int attempt = 0;
    ChannelBusyException channelBusyException;
    setIMEILock.lock();
    try {
      do {
        try {
          try {
            setIMEICommand.execute(setIMEICommand.createRequest().setImei(imei));
          } catch (ATAlreadyStartedErrException e) {
            logger.info("[{}] - attempt to set IMEI on already started AT, stop AT channel", this);
            stop();
            setIMEICommand.execute(setIMEICommand.createRequest().setImei(imei));
          }
          if (logger.isDebugEnabled()) { // to prevent unnecessary String instance creation
            logger.debug("[{}] - set IMEI [{}]", this, new String(imei));
          }
          return;
        } catch (ChannelBusyException e) {
          channelBusyException = e;
          logger.warn(String.format("[%s] - can't set IMEI during [%d] attempt", this, attempt + 1), e);
          Thread.sleep(attemptPeriodMillis);
        }
      } while (++attempt < attemptDurationMillis / attemptPeriodMillis);
    } finally {
      setIMEILock.unlock();
    }
    throw channelBusyException;
  }

  @Override
  public ITraceSubChannel getTraceSubChannel() {
    return traceSubChannel;
  }

  // IRawATSubChannel
  @Override
  public void start(final IRawATSubChannelHandler atRawSubChannelHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(atRawSubChannelHandler, "atRawSubChannelHandler must not be null");

    start(new RawATSubChannelDataHandler(atRawSubChannelHandler));
  }

  @Override
  public void writeRawATData(final ByteBuffer atDataBuffer) throws ChannelException, InterruptedException {
    atSubChannelLock.lock();
    try {
      channelDataSendReliableLoop(atSubChannelData, atDataBuffer);
    } finally {
      atSubChannelLock.unlock();
    }
  }

  // IATSubChannel
  @Override
  public void start(final IATSubChannelHandler atSubChannelHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(atSubChannelHandler, "atSubChannelHandler must not be null");

    start(new ATSubChannelDataHandler(atSubChannelHandler));
  }

  @Override
  public void writeATData(final String atData) throws ChannelException, InterruptedException {
    writeRawATData(atCharset.encode(atData));
  }

  // IRawATSubChannel && IATSubChannel
  private void start(final IChannelDataHandler channelDataHandler) throws ChannelException, InterruptedException {
    atSubChannelLock.lock();
    try {
      logger.debug("[{}] - starting AT", this);
      try {
        startATCommand.execute(startATCommand.createRequest());
      } catch (ATAlreadyStartedErrException e) {
        logger.info("[{}] - attempt to start already started AT", this);
        stopATCommand.execute(stopATCommand.createRequest());
        startATCommand.execute(startATCommand.createRequest());
      }
      atSubChannelData.attach(COMMAND_AT_DATA, channelDataHandler);
      logger.debug("[{}] - started AT", this);
    } finally {
      atSubChannelLock.unlock();
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    atSubChannelLock.lock();
    try {
      logger.debug("[{}] - stopping AT", this);
      try {
        stopATCommand.execute(stopATCommand.createRequest());
      } finally {
        atSubChannelData.detach();
      }
      logger.debug("[{}] - stopped AT", this);
    } finally {
      atSubChannelLock.unlock();
    }
  }

  // ITraceSubChannel (implement in internal class because of conflict with stop method in IRawATSubChannel && IATSubChannel)
  private final class TraceSubChannel implements ITraceSubChannel {

    private final Lock traceSubChannelLock = new ReentrantLock();
    private final StartTraceCommand startTraceCommand;
    private final StopTraceCommand stopTraceCommand;

    private final IChannelData traceChannelData;

    public TraceSubChannel() {
      startTraceCommand = new StartTraceCommand(COMMAND_START_TRACE, createChannelData(), 100 + getMaxAttemptTimeout());
      stopTraceCommand = new StopTraceCommand(COMMAND_STOP_TRACE, createChannelData(), 100 + getMaxAttemptTimeout());

      traceChannelData = createChannelData();
    }

    @Override
    public void start(final int speed, final ITraceSubChannelHandler traceSubChannelHandler) throws ChannelException, InterruptedException {
      Objects.requireNonNull(traceSubChannelHandler, "traceSubChannelHandler must not be null");

      StartTraceCommand.StartTraceRequest startTraceRequest = startTraceCommand.createRequest().setSpeed(speed);

      traceSubChannelLock.lock();
      try {
        logger.debug("[{}] - starting Trace", GSMChannel.this);
        try {
          startTraceCommand.execute(startTraceRequest);
        } catch (ATAlreadyStartedErrException e) {
          logger.info("[{}] - attempt to start already started Trace", GSMChannel.this);
          stopTraceCommand.execute(stopTraceCommand.createRequest());
          startTraceCommand.execute(startTraceRequest);
        }
        traceChannelData.attach(TRACE_DATA, new TraceSubChannelDataHandler(traceSubChannelHandler));
        logger.debug("[{}] - started Trace", GSMChannel.this);
      } finally {
        traceSubChannelLock.unlock();
      }
    }

    @Override
    public void stop() throws ChannelException, InterruptedException {
      traceSubChannelLock.lock();
      try {
        logger.debug("[{}] - stopping Trace", GSMChannel.this);
        try {
          stopTraceCommand.execute(stopTraceCommand.createRequest());
        } finally {
          traceChannelData.detach();
        }
        logger.debug("[{}] - stopped Trace", GSMChannel.this);
      } finally {
        traceSubChannelLock.unlock();
      }
    }

    @Override
    public void writeTraceData(final ByteBuffer traceBuffer) throws ChannelException, InterruptedException {
      traceSubChannelLock.lock();
      try {
        channelDataSendReliableLoop(traceChannelData, traceBuffer);
      } finally {
        traceSubChannelLock.unlock();
      }
    }

  }

  // internal methods
  private void channelDataSendReliableLoop(final IChannelData channelData, final ByteBuffer buffer) throws ChannelException, InterruptedException {
    while (buffer.hasRemaining()) {
      final ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
      while (bb.hasRemaining() && buffer.hasRemaining()) {
        bb.put(buffer.get());
      }
      bb.flip();

      channelData.sendReliable(bb);
    }
  }

  // internal classes
  private final class RawATSubChannelDataHandler implements IChannelDataHandler {

    private final IRawATSubChannelHandler rawATSubChannelHandler;

    public RawATSubChannelDataHandler(final IRawATSubChannelHandler rawATSubChannelHandler) {
      this.rawATSubChannelHandler = rawATSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + GSMChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      rawATSubChannelHandler.handleRawATData(data);
    }
  }

  private final class ATSubChannelDataHandler implements IChannelDataHandler {

    private final IATSubChannelHandler atSubChannelHandler;

    public ATSubChannelDataHandler(final IATSubChannelHandler atSubChannelHandler) {
      this.atSubChannelHandler = atSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + GSMChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      atSubChannelHandler.handleATData(atCharset.decode(data).toString());
    }
  }

  private final class TraceSubChannelDataHandler implements IChannelDataHandler {

    private final ITraceSubChannelHandler traceSubChannelHandler;

    public TraceSubChannelDataHandler(final ITraceSubChannelHandler traceSubChannelHandler) {
      this.traceSubChannelHandler = traceSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + GSMChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      traceSubChannelHandler.handleTraceData(data);
    }
  }

}
