/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import java.io.Serializable;

public class SCEmulatorState implements Serializable {

  private static final long serialVersionUID = 7999572287954986164L;

  private final SCEmulatorStatePrimary primaryState;
  private final SCEmulatorStateSecondary secondaryState;

  private final transient String toS;

  public SCEmulatorState(final SCEmulatorStatePrimary primaryState, final SCEmulatorStateSecondary secondaryState) {
    this.primaryState = primaryState;
    this.secondaryState = secondaryState;

    toS = String.format("%s@%x:[%s:%s]", getClass().getSimpleName(), hashCode(), primaryState, secondaryState);
  }

  public SCEmulatorState(final byte primaryState, final byte secondaryState) {
    this(SCEmulatorStatePrimary.valueOf(primaryState), SCEmulatorStateSecondary.valueOf(secondaryState));
  }

  public SCEmulatorStatePrimary getPrimaryState() {
    return primaryState;
  }

  public SCEmulatorStateSecondary getSecondaryState() {
    return secondaryState;
  }

  public enum SCEmulatorStatePrimary {

    NONE((byte) (0x00 & 0xFF)),
    IDLE((byte) (0x01 & 0xFF)),
    DEACTIVATION((byte) (0x02 & 0xFF)),
    COLD_RESET_INIT((byte) (0x03 & 0xFF)),
    COLD_RESET_WAIT_VCC((byte) (0x04 & 0xFF)),
    COLD_RESET_WAIT_CLK((byte) (0x05 & 0xFF)),
    COLD_RESET_WAIT_RST((byte) (0x06 & 0xFF)),
    ATR((byte) (0x07 & 0xFF)),
    PPS((byte) (0x08 & 0xFF)),
    APDU((byte) (0x09 & 0xFF));

    private final byte code;

    SCEmulatorStatePrimary(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static SCEmulatorStatePrimary valueOf(final byte code) {
      short codeUnsigned = (short) (code & 0xFF);
      if (codeUnsigned >= 0 && codeUnsigned < scEmulatorStatePrimaries.length) {
        SCEmulatorStatePrimary scEmulatorStatePrimary = scEmulatorStatePrimaries[codeUnsigned];
        if (scEmulatorStatePrimary != null) {
          return scEmulatorStatePrimary;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for SCEmulatorStatePrimary: 0x%02X", codeUnsigned));
    }

    private static final SCEmulatorStatePrimary[] scEmulatorStatePrimaries;

    static {
      short maxCodeUnsigned = 0;
      for (SCEmulatorStatePrimary scEmulatorStatePrimary : values()) {
        short codeUnsigned = (short) (scEmulatorStatePrimary.code & 0xFF);
        if (codeUnsigned > maxCodeUnsigned) {
          maxCodeUnsigned = codeUnsigned;
        }
      }
      scEmulatorStatePrimaries = new SCEmulatorStatePrimary[maxCodeUnsigned + 1];
      for (SCEmulatorStatePrimary scEmulatorStatePrimary : values()) {
        short codeUnsigned = (short) (scEmulatorStatePrimary.code & 0xFF);
        scEmulatorStatePrimaries[codeUnsigned] = scEmulatorStatePrimary;
      }
    }

  }

  public enum SCEmulatorStateSecondary {

    VCC_0V((byte) (0xF0 & 0xFF)),
    VCC_0V__1_8V((byte) (0xF1 & 0xFF)),
    VCC_0V__1_8V__0V((byte) (0xF2 & 0xFF)),
    VCC_0V__1_8V__0V__3_0V((byte) (0xF3 & 0xFF)),

    NONE((byte) (0x00 & 0xFF)),

    SEND_ATR((byte) (0x01 & 0xFF)),

    RECEIVE_PPS_PPSS_OR_APDU_CLA((byte) (0x20 & 0xFF)),
    RECEIVE_PPS_PPS0((byte) (0x21 & 0xFF)),
    RECEIVE_PPS_PPS1((byte) (0x22 & 0xFF)),
    RECEIVE_PPS_PPS2((byte) (0x23 & 0xFF)),
    RECEIVE_PPS_PPS3((byte) (0x24 & 0xFF)),
    RECEIVE_PPS_PCK((byte) (0x25 & 0xFF)),
    SEND_PPS((byte) (0x26 & 0xFF)),

    RECEIVE_APDU_CLA((byte) (0x40 & 0xFF)),
    RECEIVE_APDU_INS((byte) (0x41 & 0xFF)),
    RECEIVE_APDU_P1((byte) (0x42 & 0xFF)),
    RECEIVE_APDU_P2((byte) (0x43 & 0xFF)),
    RECEIVE_APDU_P3((byte) (0x44 & 0xFF)),
    PROCESS_APDU_COMMAND_HEADER((byte) (0x45 & 0xFF)),
    SEND_ACK_PROCEDURE_BYTE_FOR_APDU_COMMAND_DATA((byte) (0x46 & 0xFF)),
    RECEIVE_APDU_COMMAND_DATA((byte) (0x47 & 0xFF)),
    RENDER_AND_POST_APDU_COMMAND((byte) (0x48 & 0xFF)),
    PEND_AND_PARSE_APDU_RESPONSE((byte) (0x49 & 0xFF)),
    SEND_NULL_PROCEDURE_BYTE((byte) (0x4A & 0xFF)),
    SEND_ACK_FOR_APDU_RESPONSE_DATA_PROCEDURE_BYTE((byte) (0x4B & 0xFF)),
    SEND_APDU_RESPONSE_DATA((byte) (0x4C & 0xFF)),
    SEND_APDU_RESPONSE_HEADER((byte) (0x4D & 0xFF));

    private final byte code;

    SCEmulatorStateSecondary(final byte code) {
      this.code = code;
    }

    public byte getCode() {
      return code;
    }

    public static SCEmulatorStateSecondary valueOf(final byte code) {
      short codeUnsigned = (short) (code & 0xFF);
      if (codeUnsigned >= 0 && codeUnsigned < scEmulatorStateSecondaries.length) {
        SCEmulatorStateSecondary scEmulatorStatePrimary = scEmulatorStateSecondaries[codeUnsigned];
        if (scEmulatorStatePrimary != null) {
          return scEmulatorStatePrimary;
        }
      }
      throw new IllegalArgumentException(String.format("Unsupported code for SCEmulatorStateSecondary: 0x%02X", codeUnsigned));
    }

    private static final SCEmulatorStateSecondary[] scEmulatorStateSecondaries;

    static {
      short maxCodeUnsigned = 0;
      for (SCEmulatorStateSecondary scEmulatorStateSecondary : values()) {
        short codeUnsigned = (short) (scEmulatorStateSecondary.code & 0xFF);
        if (codeUnsigned > maxCodeUnsigned) {
          maxCodeUnsigned = codeUnsigned;
        }
      }
      scEmulatorStateSecondaries = new SCEmulatorStateSecondary[maxCodeUnsigned + 1];
      for (SCEmulatorStateSecondary scEmulatorStateSecondary : values()) {
        short codeUnsigned = (short) (scEmulatorStateSecondary.code & 0xFF);
        scEmulatorStateSecondaries[codeUnsigned] = scEmulatorStateSecondary;
      }
    }

  }

  @Override
  public String toString() {
    return toS;
  }

}
