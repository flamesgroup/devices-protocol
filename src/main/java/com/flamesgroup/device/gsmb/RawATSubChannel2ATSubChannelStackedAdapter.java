/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.channel.ChannelException;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class RawATSubChannel2ATSubChannelStackedAdapter implements IATSubChannel, IRawATSubChannel, IRawATSubChannelHandler {

  private final IRawATSubChannel rawATSubChannel;

  private final Lock lock = new ReentrantLock();
  private final Stack<IRawATSubChannelHandler> rawATSubChannelHandlerStack = new Stack<>();

  private final Charset atCharset = Charset.forName("ISO-8859-1");

  public RawATSubChannel2ATSubChannelStackedAdapter(final IRawATSubChannel rawATSubChannel) {
    Objects.requireNonNull(rawATSubChannel, "rawATSubChannel must not be null");

    this.rawATSubChannel = rawATSubChannel;
  }

  @Override
  public void start(final IATSubChannelHandler atSubChannelHandler) throws ChannelException, InterruptedException {
    start(new ATSubChannelHandlerWrapper(atSubChannelHandler));
  }

  @Override
  public void writeATData(final String atData) throws ChannelException, InterruptedException {
    writeRawATData(atCharset.encode(atData));
  }

  @Override
  public void start(final IRawATSubChannelHandler atRawSubChannelHandler) throws ChannelException, InterruptedException {
    lock.lock();
    try {
      rawATSubChannelHandlerStack.push(atRawSubChannelHandler);
      if (rawATSubChannelHandlerStack.size() == 1) {
        rawATSubChannel.start(this);
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    lock.lock();
    try {
      if (rawATSubChannelHandlerStack.size() == 1) {
        rawATSubChannel.stop();
      }
      rawATSubChannelHandlerStack.pop();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void writeRawATData(final ByteBuffer atDataBuffer) throws ChannelException, InterruptedException {
    rawATSubChannel.writeRawATData(atDataBuffer);
  }

  @Override
  public void handleRawATData(final ByteBuffer atDataBuffer) {
    IRawATSubChannelHandler rawATSubChannelHandler;
    lock.lock();
    try {
      rawATSubChannelHandler = rawATSubChannelHandlerStack.peek();
    } finally {
      lock.unlock();
    }

    rawATSubChannelHandler.handleRawATData(atDataBuffer);
  }

  private final class ATSubChannelHandlerWrapper implements IRawATSubChannelHandler {

    private final IATSubChannelHandler subChannelHandler;

    private ATSubChannelHandlerWrapper(final IATSubChannelHandler subChannelHandler) {
      this.subChannelHandler = subChannelHandler;
    }

    @Override
    public void handleRawATData(final ByteBuffer atDataBuffer) {
      subChannelHandler.handleATData(atCharset.decode(atDataBuffer).toString());
    }
  }

}
