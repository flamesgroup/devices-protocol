/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.IndicationChannel;
import com.flamesgroup.device.commonb.LoggerChannel;
import com.flamesgroup.device.commonb.ServiceChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.periphery.IPeripheryConnection;
import com.flamesgroup.jdev.DevException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class GSMBOXGenericDevice extends GSMGenericDevice {

  private static final Logger logger = LoggerFactory.getLogger(GSMBOXGenericDevice.class);

  private final IPeripheryConnection peripheryConnection;
  private final ScheduledExecutorService syncScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

  public GSMBOXGenericDevice(final IPeripheryConnection peripheryConnection, final IMudpConnection[] mudpConnections, final DeviceUID deviceUid) {
    super(mudpConnections, deviceUid);
    this.peripheryConnection = peripheryConnection;
  }

  @Override
  public void attach() throws MudpException, InterruptedException {
    try {
      peripheryConnection.resetAllCpus();
    } catch (DevException e) {
      throw new MudpException(e);
    }

    Thread.sleep(200); // give CPUs firmware time to init (necessary to prevent CRC errors after reattaching)

    super.attach();

    syncScheduledExecutorService.scheduleAtFixedRate(() -> {
      try {
        peripheryConnection.toggleSync();
      } catch (DevException e) {
        logger.warn("[{}] - can't toggle SYNC", GSMBOXGenericDevice.this, e);
      }
    }, 500, 500, TimeUnit.MILLISECONDS);
  }

  @Override
  public void detach() throws MudpException, InterruptedException {
    syncScheduledExecutorService.shutdown();

    super.detach();
  }

  protected int calculateRealConnectionNFor(final int entryNumber) {
    return entryNumber % getConnectionCount();
  }

  protected int calculateRealCpuNFor(final int entryNumber) {
    return entryNumber / getConnectionCount();
  }

  @Override
  protected ICPUEntry createCPUEntry(final int cpuEntryNumber, final DeviceUID deviceUid) {
    checkCPUEntryNumber(cpuEntryNumber);

    int entryNumber = cpuEntryNumber / 1;
    int realConnectionN = calculateRealConnectionNFor(entryNumber);
    int realCpuN = calculateRealCpuNFor(entryNumber);
    int serviceRealChannelN = 0;
    int loggerRealChannelN = 1;

    final IServiceChannel serviceChannel = new ServiceChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, serviceRealChannelN), cpuEntryNumber, deviceUid);
    final ILoggerChannel loggerChannel = new LoggerChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, loggerRealChannelN), cpuEntryNumber, deviceUid);
    return new CPUEntry(serviceChannel, loggerChannel, cpuEntryNumber, deviceUid);
  }

  @Override
  protected IGSMEntry createGSMEntry(final int gsmEntryNumber, final DeviceUID deviceUid) {
    checkGSMEntryNumber(gsmEntryNumber);

    int entryNumber = gsmEntryNumber / 1;
    int realConnectionN = calculateRealConnectionNFor(entryNumber);
    int realCpuN = calculateRealCpuNFor(entryNumber);
    int gsmRealChannelN = 2;
    int audioRealChannelN = 3;
    int indicationRealChannelN = 4;

    final IGSMChannel gsmChannel = new GSMChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, gsmRealChannelN), gsmEntryNumber, deviceUid);
    final IAudioChannel audioChannel = new AudioChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, audioRealChannelN), gsmEntryNumber, deviceUid);
    final IIndicationChannel indicationChannel =
        new IndicationChannel(createMudpChannelFor(realConnectionN), createMudpChannelAddressTypeBFor(realCpuN, indicationRealChannelN), gsmEntryNumber, deviceUid);
    return createGSMBOXEntry(gsmChannel, audioChannel, indicationChannel, gsmEntryNumber, deviceUid);
  }

  protected abstract IGSMEntry createGSMBOXEntry(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final IIndicationChannel indicationChannel, int gsmEntryNumber, DeviceUID deviceUid);

}
