/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.gsmb.command.AudioAlreadyStartedErrException;
import com.flamesgroup.device.gsmb.command.StartAudioCommand;
import com.flamesgroup.device.gsmb.command.StopAudioCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.EnumSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AudioChannel extends Channel implements IAudioChannel, IAudioSubChannel {

  private static final Logger logger = LoggerFactory.getLogger(AudioChannel.class);

  public static final byte COMMAND_AUDIO_DATA = (byte) 0x00;
  public static final byte COMMAND_START_AUDIO = (byte) 0x01;
  public static final byte COMMAND_STOP_AUDIO = (byte) 0x02;

  private static final int AUDIO_CHUNK_SIZE = 160;
  private static final int AUDIO_CHUNK_TIMESTAMP = 10;
  private static final int TIMESTAMP_SIZE = 4;

  private final Lock audioSubChannelLock = new ReentrantLock();
  private final StartAudioCommand startAudioCommand;
  private final StopAudioCommand stopAudioCommand;

  private final IChannelData audioSubChannelData;
  private final byte[] remainAudioData = new byte[AUDIO_CHUNK_SIZE];
  private int remainAudioDataLength;

  public AudioChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int gsmEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, gsmEntryNumber, deviceUid);

    startAudioCommand = new StartAudioCommand(COMMAND_START_AUDIO, createChannelData(), 10 + getMaxAttemptTimeout());
    stopAudioCommand = new StopAudioCommand(COMMAND_STOP_AUDIO, createChannelData(), 10 + getMaxAttemptTimeout());

    audioSubChannelData = createChannelData();
  }

  public AudioChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int gsmEntryNumber) {
    this(mudpChannel, mudpChannelAddress, gsmEntryNumber, null);
  }

  // IAudioChannel
  @Override
  public IAudioSubChannel getAudioSubChannel() {
    return this;
  }

  // IAudioSubChannel
  @Override
  public void start(final EnumSet<AudioSetting> settings, final IAudioSubChannelHandler audioSubChannelHandler) throws ChannelException, InterruptedException {
    if (audioSubChannelHandler == null) {
      throw new IllegalArgumentException("AudioSubChannelHandler must not be null");
    }

    StartAudioCommand.StartAudioRequest startAudioRequest = startAudioCommand.createRequest().setSettings(settings);

    audioSubChannelLock.lock();
    try {
      logger.debug("[{}] - starting Audio", this);
      try {
        startAudioCommand.execute(startAudioRequest);
      } catch (AudioAlreadyStartedErrException e) {
        logger.info("[{}] - attempt to start already started Audio", this);
        stopAudioCommand.execute(stopAudioCommand.createRequest());
        startAudioCommand.execute(startAudioRequest);
      }
      audioSubChannelData.attach(COMMAND_AUDIO_DATA, new AudioSubChannelDataHandler(audioSubChannelHandler));
      logger.debug("[{}] - started Audio", this);
    } finally {
      audioSubChannelLock.unlock();
    }

  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    audioSubChannelLock.lock();
    try {
      logger.debug("[{}] - stopping Audio", this);
      try {
        stopAudioCommand.execute(stopAudioCommand.createRequest());
      } finally {
        audioSubChannelData.detach();
      }
      logger.debug("[{}] - stopped Audio", this);
    } finally {
      audioSubChannelLock.unlock();
    }
  }

  @Override
  public void writeAudioData(final int timestamp, final byte[] audioData, int offset, final int length) throws ChannelException {
    audioSubChannelLock.lock();
    try {
      final int totalLength = length + remainAudioDataLength;
      int audioChunkCount = totalLength / AUDIO_CHUNK_SIZE;

      int actualTimestamp = timestamp - remainAudioDataLength / (AUDIO_CHUNK_SIZE / AUDIO_CHUNK_TIMESTAMP);

      for (int i = 0; i < audioChunkCount; i++) {
        final ByteBuffer sendByteBuffer = ByteBuffer.allocate(TIMESTAMP_SIZE + AUDIO_CHUNK_SIZE).order(ByteOrder.LITTLE_ENDIAN);
        sendByteBuffer.putInt(actualTimestamp);
        if (remainAudioDataLength == 0) {
          sendByteBuffer.put(audioData, offset, AUDIO_CHUNK_SIZE);
          offset += AUDIO_CHUNK_SIZE;
        } else {
          int remainAudioChunkSize = AUDIO_CHUNK_SIZE - remainAudioDataLength;
          sendByteBuffer.put(remainAudioData, 0, remainAudioDataLength);
          remainAudioDataLength = 0;

          sendByteBuffer.put(audioData, offset, remainAudioChunkSize);
          offset += remainAudioChunkSize;
        }
        sendByteBuffer.flip();

        audioSubChannelData.sendUnreliable(sendByteBuffer);

        actualTimestamp += AUDIO_CHUNK_TIMESTAMP;
      }

      remainAudioDataLength = totalLength % AUDIO_CHUNK_SIZE;
      System.arraycopy(audioData, offset, remainAudioData, 0, remainAudioDataLength);
    } finally {
      audioSubChannelLock.unlock();
    }
  }

  // internal classes
  private final class AudioSubChannelDataHandler implements IChannelDataHandler {

    private final IAudioSubChannelHandler audioSubChannelHandler;
    private final byte[] audioData = new byte[AUDIO_CHUNK_SIZE];

    public AudioSubChannelDataHandler(final IAudioSubChannelHandler audioSubChannelHandler) {
      this.audioSubChannelHandler = audioSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + AudioChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      int timestamp = data.getInt();
      data.get(audioData);
      audioSubChannelHandler.handleAudioData(timestamp, audioData, 0, audioData.length);
    }
  }

}
