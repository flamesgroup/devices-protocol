/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.Channel;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.gsmb.command.GetSCEmulatorStateCommand;
import com.flamesgroup.device.gsmb.command.SCEmulatorAlreadyStartedErrException;
import com.flamesgroup.device.gsmb.command.StartSCEmulatorCommand;
import com.flamesgroup.device.gsmb.command.StopSCEmulatorCommand;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpPacket;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SCEmulatorChannel extends Channel implements ISCEmulatorChannel, ISCEmulatorSubChannel {

  private final Logger logger = LoggerFactory.getLogger(SCEmulatorChannel.class);

  public static final byte COMMAND_GET_EMULATOR_AUTOMATE_STATE = (byte) 0x0;
  public static final byte COMMAND_START_EMULATOR = (byte) 0x1;
  public static final byte COMMAND_STOP_EMULATOR = (byte) 0x2;
  public static final byte APDU = (byte) 0x3;
  public static final byte ERROR_AUTOMATE_STATE = (byte) 0xF;

  private final Lock scEmulatorSubChannelLock = new ReentrantLock();
  private final StartSCEmulatorCommand startSCEmulatorCommand;
  private final StopSCEmulatorCommand stopSCEmulatorCommand;
  private final Lock getSCEmulatorStateLock = new ReentrantLock();
  private final GetSCEmulatorStateCommand getSCEmulatorStateCommand;

  private final IChannelData scEmulatorSubChannelAPDU;
  private final IChannelData scEmulatorSubChannelError;

  public SCEmulatorChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int gsmEntryNumber, final DeviceUID deviceUid) {
    super(mudpChannel, mudpChannelAddress, gsmEntryNumber, deviceUid);

    startSCEmulatorCommand = new StartSCEmulatorCommand(COMMAND_START_EMULATOR, createChannelData(), 100 + getMaxAttemptTimeout());
    stopSCEmulatorCommand = new StopSCEmulatorCommand(COMMAND_STOP_EMULATOR, createChannelData(), 100 + getMaxAttemptTimeout());
    getSCEmulatorStateCommand = new GetSCEmulatorStateCommand(COMMAND_GET_EMULATOR_AUTOMATE_STATE, createChannelData(), 75 + getMaxAttemptTimeout());

    scEmulatorSubChannelAPDU = createChannelData();
    scEmulatorSubChannelError = createChannelData();
  }

  public SCEmulatorChannel(final IMudpChannel mudpChannel, final MudpChannelAddress mudpChannelAddress, final int gsmEntryNumber) {
    this(mudpChannel, mudpChannelAddress, gsmEntryNumber, null);
  }

  // ISCEmulatorChannel
  @Override
  public ISCEmulatorSubChannel getSCEmulatorSubChannel() {
    return this;
  }

  // ISCEmulatorSubChannel
  @Override
  public void start(final ATR atr, final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler) throws ChannelException, InterruptedException {
    Objects.requireNonNull(atr, "ATR must not be null");
    Objects.requireNonNull(scEmulatorSubChannelHandler, "SCEmulatorSubChannelHandler must not be null");

    scEmulatorSubChannelLock.lock();
    try {
      logger.debug("[{}] - starting SCEmulator", this);
      scEmulatorSubChannelAPDU.attach(SCEmulatorChannel.APDU, new SCEmulatorSubChannelAPDUHandler(scEmulatorSubChannelHandler));
      scEmulatorSubChannelError.attach(SCEmulatorChannel.ERROR_AUTOMATE_STATE, new SCEmulatorSubChannelErrorHandler(scEmulatorSubChannelHandler));
      try {
        try {
          startSCEmulatorCommand.execute(startSCEmulatorCommand.createRequest().setATR(atr));
        } catch (SCEmulatorAlreadyStartedErrException e) {
          logger.info("[{}] - attempt to start already started SCReader", this);

          stopSCEmulatorCommand.execute(stopSCEmulatorCommand.createRequest());
          startSCEmulatorCommand.execute(startSCEmulatorCommand.createRequest().setATR(atr));
        }
      } catch (ChannelException | InterruptedException e) {
        scEmulatorSubChannelError.detach();
        scEmulatorSubChannelAPDU.detach();
        throw e;
      }
      logger.debug("[{}] - started SCEmulator", this);
    } finally {
      scEmulatorSubChannelLock.unlock();
    }
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    scEmulatorSubChannelLock.lock();
    try {
      logger.debug("[{}] - stopping SCEmulator", this);
      try {
        stopSCEmulatorCommand.execute(stopSCEmulatorCommand.createRequest());
      } finally {
        scEmulatorSubChannelError.detach();
        scEmulatorSubChannelAPDU.detach();
      }
      logger.debug("[{}] - stopped SCEmulator", this);
    } finally {
      scEmulatorSubChannelLock.unlock();
    }
  }

  @Override
  public void writeAPDUResponse(final APDUResponse response) throws ChannelException, InterruptedException {
    scEmulatorSubChannelLock.lock();
    try {
      final ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
      APDUResponse.encoding(response, bb);
      bb.flip();

      scEmulatorSubChannelAPDU.sendReliable(bb);
    } finally {
      scEmulatorSubChannelLock.unlock();
    }
  }

  @Override
  public SCEmulatorState getState() throws ChannelException, InterruptedException {
    getSCEmulatorStateLock.lock();
    try {
      SCEmulatorState state = getSCEmulatorStateCommand.execute(getSCEmulatorStateCommand.createRequest()).getState();
      logger.debug("[{}] - got SCEmulator [{}]", this, state);
      return state;
    } finally {
      getSCEmulatorStateLock.unlock();
    }
  }

  // internal classes
  private final class SCEmulatorSubChannelAPDUHandler implements IChannelDataHandler {

    private final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler;

    public SCEmulatorSubChannelAPDUHandler(final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler) {
      this.scEmulatorSubChannelHandler = scEmulatorSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + SCEmulatorChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      scEmulatorSubChannelHandler.handleAPDUCommand(APDUCommand.decoding(data));
    }

  }

  private final class SCEmulatorSubChannelErrorHandler implements IChannelDataHandler {

    private final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler;

    public SCEmulatorSubChannelErrorHandler(final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler) {
      this.scEmulatorSubChannelHandler = scEmulatorSubChannelHandler;
    }

    @Override
    public void handleErrCode(final ChannelErrCodeException e) {
      logger.warn("[" + SCEmulatorChannel.this + "] - handled unexpected error", e);
    }

    @Override
    public void handleReceive(final ByteBuffer data) {
      SCEmulatorError error = SCEmulatorError.valueOf(data.get());
      byte primaryState = data.get();
      byte secondaryState = data.get();
      SCEmulatorState state = new SCEmulatorState(primaryState, secondaryState);
      scEmulatorSubChannelHandler.handleErrorState(error, state);
    }

  }

}
