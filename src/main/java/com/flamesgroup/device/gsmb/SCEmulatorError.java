/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

public enum SCEmulatorError {

  NO_ERR((byte) (0x00 & 0xFF)),
  TIMEOUT((byte) (0x01 & 0xFF)),
  NOT_SUPPORT((byte) (0x02 & 0xFF)),
  REJECTED((byte) (0x03 & 0xFF));

  private final byte code;

  SCEmulatorError(final byte code) {
    this.code = code;
  }

  public byte getCode() {
    return code;
  }

  public static SCEmulatorError valueOf(final byte code) {
    short codeUnsigned = (short) (code & 0xFF);
    if (codeUnsigned >= 0 && codeUnsigned < scEmulatorErrors.length) {
      SCEmulatorError scEmulatorError = scEmulatorErrors[codeUnsigned];
      if (scEmulatorError != null) {
        return scEmulatorError;
      }
    }
    throw new IllegalArgumentException(String.format("Unsupported code for SCEmulatorError: 0x%02X", codeUnsigned));
  }

  private static final SCEmulatorError[] scEmulatorErrors;

  static {
    short maxCodeUnsigned = 0;
    for (SCEmulatorError scEmulatorError : values()) {
      short codeUnsigned = (short) (scEmulatorError.code & 0xFF);
      if (codeUnsigned > maxCodeUnsigned) {
        maxCodeUnsigned = codeUnsigned;
      }
    }
    scEmulatorErrors = new SCEmulatorError[maxCodeUnsigned + 1];
    for (SCEmulatorError scEmulatorError : values()) {
      short codeUnsigned = (short) (scEmulatorError.code & 0xFF);
      scEmulatorErrors[codeUnsigned] = scEmulatorError;
    }
  }

}
