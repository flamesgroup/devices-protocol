/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.periphery.IPeripheryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GSMBOX8Device extends GSMBOXGenericDevice {

  private static final Logger logger = LoggerFactory.getLogger(GSMBOX8Device.class);

  public static final int CPU_ENTRY_COUNT = 8;
  public static final int GSM_ENTRY_COUNT = 8;

  public GSMBOX8Device(final IPeripheryConnection peripheryConnection, final IMudpConnection mudpConnection0, final IMudpConnection mudpConnection1, final DeviceUID deviceUid) {
    super(peripheryConnection, new IMudpConnection[] {mudpConnection0, mudpConnection1}, deviceUid);
  }

  public GSMBOX8Device(final IPeripheryConnection peripheryConnection, final IMudpConnection mudpConnection0, final IMudpConnection mudpConnection1) {
    this(peripheryConnection, mudpConnection0, mudpConnection1, null);
  }

  @Override
  public DeviceType getDeviceType() {
    return DeviceType.GSMBOX8;
  }

  @Override
  public int getCPUEntryCount() {
    return CPU_ENTRY_COUNT;
  }

  @Override
  public int getGSMEntryCount() {
    return GSM_ENTRY_COUNT;
  }

  @Override
  protected IGSMEntry createGSMBOXEntry(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final IIndicationChannel indicationChannel,
      final int gsmEntryNumber, final DeviceUID deviceUid) {
    return new GSMBOX8Entry(gsmChannel, audioChannel, indicationChannel, gsmEntryNumber, deviceUid);
  }

  private static final class GSMBOX8Entry extends GSMBGenericEntry {

    public GSMBOX8Entry(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final IIndicationChannel indicationChannel, final int gsmEntryNumber, final DeviceUID deviceUid) {
      super(gsmChannel, audioChannel, null, indicationChannel, gsmEntryNumber, deviceUid); // GSMBOX8 implement ISCEmulatorChannel throw Remote SIM protocol

      if (logger.isTraceEnabled()) {
        logger.trace("[{}] - created with [{}/{}]", toString(), gsmChannel, audioChannel);
      }
    }

  }

}
