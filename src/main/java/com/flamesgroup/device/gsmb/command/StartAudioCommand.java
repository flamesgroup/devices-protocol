/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.gsmb.AudioSetting;
import com.flamesgroup.device.gsmb.command.StartAudioCommand.StartAudioRequest;
import com.flamesgroup.device.gsmb.command.StartAudioCommand.StartAudioResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.EnumSet;

public final class StartAudioCommand extends Command<StartAudioRequest, StartAudioResponse> {

  private static final byte ERRCODE_ALREADY_START_AUDIODATA_ERR = (byte) 0x80;

  public StartAudioCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_ALREADY_START_AUDIODATA_ERR) {
      return new AudioAlreadyStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public StartAudioRequest createRequest() {
    return new StartAudioRequest();
  }

  @Override
  public StartAudioResponse createResponse() {
    return new StartAudioResponse();
  }

  public static class StartAudioRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);

    private int settings;

    private StartAudioRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.putInt(settings);
      data.flip();
      return data;
    }

    public StartAudioRequest setSettings(final EnumSet<AudioSetting> settings) {
      this.settings = 0;
      if (settings != null) {
        for (AudioSetting setting : settings) {
          this.settings |= setting.getSettingFlag();
        }
      }
      return this;
    }
  }

  public static class StartAudioResponse implements ICommandResponse {

    private StartAudioResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
