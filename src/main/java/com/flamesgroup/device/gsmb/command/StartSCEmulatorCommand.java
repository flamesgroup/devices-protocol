/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.gsmb.command.StartSCEmulatorCommand.StartSCEmulatorRequest;
import com.flamesgroup.device.gsmb.command.StartSCEmulatorCommand.StartSCEmulatorResponse;
import com.flamesgroup.device.sc.ATR;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class StartSCEmulatorCommand extends Command<StartSCEmulatorRequest, StartSCEmulatorResponse> {

  private static final byte ERRCODE_ALREADY_START_EMULATOR_ERR = (byte) 0x80;

  public StartSCEmulatorCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_ALREADY_START_EMULATOR_ERR) {
      return new SCEmulatorAlreadyStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public StartSCEmulatorRequest createRequest() {
    return new StartSCEmulatorRequest();
  }

  @Override
  public StartSCEmulatorResponse createResponse() {
    return new StartSCEmulatorResponse();
  }

  public static class StartSCEmulatorRequest implements ICommandRequest {

    private ATR atr;
    private final ByteBuffer data = ByteBuffer.allocate(ATR.MAX_SIZE).order(ByteOrder.LITTLE_ENDIAN);

    private StartSCEmulatorRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      ATR.encoding(atr, data);
      data.flip();
      return data;
    }

    public StartSCEmulatorRequest setATR(final ATR atr) {
      if (atr == null) {
        throw new IllegalArgumentException("ATR must not be null");
      }
      this.atr = atr;
      return this;
    }

  }

  public static class StartSCEmulatorResponse implements ICommandResponse {

    private StartSCEmulatorResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
