/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.gsmb.command.SetIMEICommand.SetIMEIRequest;
import com.flamesgroup.device.gsmb.command.SetIMEICommand.SetIMEIResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class SetIMEICommand extends Command<SetIMEIRequest, SetIMEIResponse> {

  private static final byte ERRCODE_ALREADY_START_ATDATA_ERR = (byte) 0x80;
  private static final byte ERRCODE_SET_IMEI_ERR = (byte) 0xB0;

  public SetIMEICommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_SET_IMEI_ERR) {
      return new SetIMEIErrException(e.getErrCode());
    } else if (e.getErrCode() == ERRCODE_ALREADY_START_ATDATA_ERR) {
      return new ATAlreadyStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public SetIMEIRequest createRequest() {
    return new SetIMEIRequest();
  }

  @Override
  public SetIMEIResponse createResponse() {
    return new SetIMEIResponse();
  }

  public static class SetIMEIRequest implements ICommandRequest {

    private final byte[] imei = new byte[15];
    private final ByteBuffer data = ByteBuffer.allocate(imei.length).order(ByteOrder.LITTLE_ENDIAN);

    private SetIMEIRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      data.put(imei);
      data.flip();
      return data;
    }

    public SetIMEIRequest setImei(final byte[] imei) {
      if (imei.length != 14 && imei.length != 15) {
        throw new IllegalArgumentException(String.format("IMEI length must be 14 or 15 digits, but it's [%d]", imei.length));
      }
      for (byte b : imei) {
        if (!Character.isDigit(b)) {
          throw new IllegalArgumentException("IMEI must contain only digit chars");
        }
      }

      System.arraycopy(imei, 0, this.imei, 0, imei.length);
      return this;
    }

  }

  public static class SetIMEIResponse implements ICommandResponse {

    private SetIMEIResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length is " + data.remaining());
      }
    }

  }

}
