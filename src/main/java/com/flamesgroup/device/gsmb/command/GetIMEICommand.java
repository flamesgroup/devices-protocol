/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.gsmb.command.GetIMEICommand.GetIMEIRequest;
import com.flamesgroup.device.gsmb.command.GetIMEICommand.GetIMEIResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class GetIMEICommand extends Command<GetIMEIRequest, GetIMEIResponse> {

  private static final byte ERRCODE_ALREADY_START_ATDATA_ERR = (byte) 0x80;
  private static final byte ERRCODE_GET_IMEI_ERR = (byte) 0xA0;

  public GetIMEICommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_GET_IMEI_ERR) {
      return new GetIMEIErrException(e.getErrCode());
    } else if (e.getErrCode() == ERRCODE_ALREADY_START_ATDATA_ERR) {
      return new ATAlreadyStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public GetIMEIRequest createRequest() {
    return new GetIMEIRequest();
  }

  @Override
  public GetIMEIResponse createResponse() {
    return new GetIMEIResponse();
  }

  public static class GetIMEIRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private GetIMEIRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear().flip();
      return data;
    }

  }

  public static class GetIMEIResponse implements ICommandResponse {

    private byte[] imei;

    private GetIMEIResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      data.mark();
      byte[] imei = new byte[data.remaining()];
      data.get(imei);
      try {
        if (imei.length != 15) {
          throw new ChannelCommandDecodeException(String.format("IMEI length must be 15 digits, but it's [%d]", imei.length));
        }
        for (byte b : imei) {
          if (!Character.isDigit(b)) {
            throw new ChannelCommandDecodeException("IMEI must contain only digit chars");
          }
        }
      } catch (ChannelCommandDecodeException e) {
        data.reset();
        throw e;
      }

      this.imei = imei;
    }

    public byte[] getIMEI() {
      return imei;
    }

  }

}
