/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.gsmb.command.GetSCEmulatorStateCommand.GetSCEmulatorStateRequest;
import com.flamesgroup.device.gsmb.command.GetSCEmulatorStateCommand.GetSCEmulatorStateResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class GetSCEmulatorStateCommand extends Command<GetSCEmulatorStateRequest, GetSCEmulatorStateResponse> {

  private static final byte ERRCODE_NOT_START_EMULATOR_ERR = (byte) 0x90;

  public GetSCEmulatorStateCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_NOT_START_EMULATOR_ERR) {
      return new SCEmulatorNonStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public GetSCEmulatorStateRequest createRequest() {
    return new GetSCEmulatorStateRequest();
  }

  @Override
  public GetSCEmulatorStateResponse createResponse() {
    return new GetSCEmulatorStateResponse();
  }

  public static class GetSCEmulatorStateRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private GetSCEmulatorStateRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear();
      return data;
    }

  }

  public static class GetSCEmulatorStateResponse implements ICommandResponse {

    private SCEmulatorState state;

    private GetSCEmulatorStateResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 2) {
        throw new ChannelCommandDecodeException("State length must be 2, but it's " + data.remaining());
      }
      byte primaryState = data.get();
      byte secondaryState = data.get();
      state = new SCEmulatorState(primaryState, secondaryState);
    }

    public SCEmulatorState getState() {
      return state;
    }

  }

}
