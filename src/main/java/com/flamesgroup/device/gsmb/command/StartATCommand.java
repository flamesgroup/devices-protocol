/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.command;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelCommandEncodeException;
import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.ICommandRequest;
import com.flamesgroup.device.channel.ICommandResponse;
import com.flamesgroup.device.gsmb.command.StartATCommand.StartATRequest;
import com.flamesgroup.device.gsmb.command.StartATCommand.StartATResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class StartATCommand extends Command<StartATRequest, StartATResponse> {

  private static final byte ERRCODE_ALREADY_START_ATDATA_ERR = (byte) 0x80;

  public StartATCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    super(type, channelData, responseTimeout);
  }

  @Override
  protected ChannelErrCodeException specifyErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == ERRCODE_ALREADY_START_ATDATA_ERR) {
      return new ATAlreadyStartedErrException(e.getErrCode());
    }
    return super.specifyErrCode(e);
  }

  @Override
  public StartATRequest createRequest() {
    return new StartATRequest();
  }

  @Override
  public StartATResponse createResponse() {
    return new StartATResponse();
  }

  public static class StartATRequest implements ICommandRequest {

    private final ByteBuffer data = ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN);

    private StartATRequest() {
      super();
    }

    @Override
    public ByteBuffer toDataByteBuffer() throws ChannelCommandEncodeException {
      data.clear().flip();
      return data;
    }
  }

  public static class StartATResponse implements ICommandResponse {

    private StartATResponse() {
      super();
    }

    @Override
    public void fromDataByteBuffer(final ByteBuffer data) throws ChannelCommandDecodeException {
      if (data.remaining() != 0) {
        throw new ChannelCommandDecodeException("Response data must be empty, but it's length " + data.remaining());
      }
    }

  }

}
