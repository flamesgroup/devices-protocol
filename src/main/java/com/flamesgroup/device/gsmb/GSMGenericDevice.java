/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.IChannel;
import com.flamesgroup.device.commonb.Device;
import com.flamesgroup.device.commonb.Entry;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class GSMGenericDevice extends Device implements IGSMDevice {

  private static final Logger logger = LoggerFactory.getLogger(GSMGenericDevice.class);

  private List<IGSMEntry> gsmEntries = Collections.emptyList();

  public GSMGenericDevice(final IMudpConnection[] mudpConnections, final DeviceUID deviceUid) {
    super(mudpConnections, deviceUid);
  }

  protected abstract IGSMEntry createGSMEntry(int gsmEntryNumber, DeviceUID deviceUid);

  protected void checkGSMEntryNumber(final int gsmEntryNumber) {
    if (gsmEntryNumber < 0 || gsmEntryNumber >= getGSMEntryCount()) {
      throw new IllegalArgumentException("gsmEntryNumber must be >= 0 and < " + getGSMEntryCount());
    }
  }

  @Override
  public void attach() throws MudpException, InterruptedException {
    super.attach();

    List<IGSMEntry> gsmEntries = new ArrayList<>(getGSMEntryCount());
    for (int i = 0; i < getGSMEntryCount(); i++) {
      gsmEntries.add(createGSMEntry(i, deviceUid));
    }
    this.gsmEntries = Collections.unmodifiableList(gsmEntries);
  }

  @Override
  public void detach() throws MudpException, InterruptedException {
    for (IGSMEntry gsmEntry : gsmEntries) {
      for (IChannel channel : gsmEntry.getSupportedChannels()) {
        if (channel.isEnslaved()) {
          logger.warn("[{}] - is enslaved, will be nulled", channel);
        }
      }
    }
    gsmEntries = Collections.emptyList();

    super.detach();
  }

  @Override
  public List<IGSMEntry> getGSMEntries() {
    return gsmEntries;
  }

  protected static class GSMBGenericEntry extends Entry implements IGSMEntry {

    private final IGSMChannel gsmChannel;
    private final IAudioChannel audioChannel;
    private final ISCEmulatorChannel scEmulatorChannel;
    private final IIndicationChannel indicationChannel;

    public GSMBGenericEntry(final IGSMChannel gsmChannel, final IAudioChannel audioChannel, final ISCEmulatorChannel scEmulatorChannel, final IIndicationChannel indicationChannel,
        final int gsmEntryNumber, final DeviceUID deviceUid) {
      super(gsmEntryNumber, deviceUid);
      this.gsmChannel = gsmChannel;
      this.audioChannel = audioChannel;
      this.scEmulatorChannel = scEmulatorChannel;
      this.indicationChannel = indicationChannel;
    }

    @Override
    public List<IChannel> getSupportedChannels() {
      List<IChannel> supportedChannels = new ArrayList<>(3);
      if (gsmChannel != null) {
        supportedChannels.add(gsmChannel);
      }
      if (audioChannel != null) {
        supportedChannels.add(audioChannel);
      }
      if (scEmulatorChannel != null) {
        supportedChannels.add(scEmulatorChannel);
      }
      if (indicationChannel != null) {
        supportedChannels.add(indicationChannel);
      }
      return supportedChannels;
    }

    @Override
    public IGSMChannel getGSMChannel() {
      if (gsmChannel == null) {
        throw new UnsupportedOperationException();
      }
      return gsmChannel;
    }

    @Override
    public IAudioChannel getAudioChannel() {
      if (audioChannel == null) {
        throw new UnsupportedOperationException();
      }
      return audioChannel;
    }

    @Override
    public ISCEmulatorChannel getSCEmulatorChannel() {
      if (scEmulatorChannel == null) {
        throw new UnsupportedOperationException();
      }
      return scEmulatorChannel;
    }

    @Override
    public IIndicationChannel getIndicationChannel() {
      if (indicationChannel == null) {
        throw new UnsupportedOperationException();
      }
      return indicationChannel;
    }

  }

}
