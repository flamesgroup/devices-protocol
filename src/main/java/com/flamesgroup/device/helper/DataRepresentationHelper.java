/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

import java.nio.ByteBuffer;

public final class DataRepresentationHelper {

  private static final String DEFAULT_PREFIX = "0x";
  private static final String DEFAULT_DELIMITER = " ";
  private static final int DEFAULT_COLUMN_NUMBER = 16;

  private static final char[] nybble2hex = {
      '0', '1', '2', '3',
      '4', '5', '6', '7',
      '8', '9', 'A', 'B',
      'C', 'D', 'E', 'F',
  };

  private DataRepresentationHelper() {
  }

  public static void appendHexString(final StringBuilder sb, final byte b, final String prefix) {
    sb.append(prefix);
    for (int bit = Byte.SIZE - 4; bit >= 0; bit -= 4) {
      sb.append(nybble2hex[(b >> bit) & 0xF]);
    }
  }

  public static void appendHexString(final StringBuilder sb, final byte b) {
    appendHexString(sb, b, DEFAULT_PREFIX);
  }

  public static void appendHexString(final StringBuilder sb, final short s, final String prefix) {
    sb.append(prefix);
    for (int bit = Short.SIZE - 4; bit >= 0; bit -= 4) {
      sb.append(nybble2hex[(s >> bit) & 0xF]);
    }
  }

  public static void appendHexString(final StringBuilder sb, final short s) {
    appendHexString(sb, s, DEFAULT_PREFIX);
  }

  public static void appendHexString(final StringBuilder sb, final int i, final String prefix) {
    sb.append(prefix);
    for (int bit = Integer.SIZE - 4; bit >= 0; bit -= 4) {
      sb.append(nybble2hex[(i >> bit) & 0xF]);
    }
  }

  public static void appendHexString(final StringBuilder sb, final int i) {
    appendHexString(sb, i, DEFAULT_PREFIX);
  }

  public static void appendHexString(final StringBuilder sb, final long l, final String prefix) {
    sb.append(prefix);
    for (int bit = Long.SIZE - 4; bit >= 0; bit -= 4) {
      sb.append(nybble2hex[(int) ((l >> bit) & 0xF)]);
    }
  }

  public static void appendHexString(final StringBuilder sb, final long l) {
    appendHexString(sb, l, DEFAULT_PREFIX);
  }

  public static void appendHexArrayString(final StringBuilder sb, final ByteBuffer buffer, final String prefix, final String delimiter) {
    int markPosition = buffer.position();
    while (buffer.hasRemaining()) {
      appendHexString(sb, buffer.get(), prefix);
      if (buffer.hasRemaining()) {
        sb.append(delimiter);
      }
    }
    buffer.position(markPosition);
  }

  public static void appendHexArrayString(final StringBuilder sb, final ByteBuffer buffer) {
    appendHexArrayString(sb, buffer, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexArrayString(final StringBuilder sb, final byte[] array, final int offset, final int length, final String prefix, final String delimiter) {
    appendHexArrayString(sb, ByteBuffer.wrap(array, offset, length), prefix, delimiter);
  }

  public static void appendHexArrayString(final StringBuilder sb, final byte[] array, final int offset, final int length) {
    appendHexArrayString(sb, array, offset, length, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexArrayString(final StringBuilder sb, final byte[] array, final String prefix, final String delimiter) {
    appendHexArrayString(sb, array, 0, array.length, prefix, delimiter);
  }

  public static void appendHexArrayString(final StringBuilder sb, final byte[] array) {
    appendHexArrayString(sb, array, 0, array.length, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final ByteBuffer buffer, final int columnNumber, final String prefix, final String delimiter) {
    int markPosition = buffer.position();
    int remaining = buffer.remaining();
    int rowNumber = remaining / columnNumber + (remaining % columnNumber != 0 ? 1 : 0);
    for (int r = 0; r < rowNumber; r++) {
      for (int c = 0; c < columnNumber; c++) {
        appendHexString(sb, buffer.get(), prefix);
        if (buffer.hasRemaining()) {
          sb.append(delimiter);
        } else {
          break;
        }
      }
      if (buffer.hasRemaining()) {
        sb.append(System.lineSeparator());
      }
    }
    buffer.position(markPosition);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final ByteBuffer buffer, final int columnNumber) {
    appendHexMatrixString(sb, buffer, columnNumber, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final ByteBuffer buffer) {
    appendHexMatrixString(sb, buffer, DEFAULT_COLUMN_NUMBER, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final byte[] array, final int offset, final int length, final int columnNumber, final String prefix, final String delimiter) {
    appendHexMatrixString(sb, ByteBuffer.wrap(array, offset, length), columnNumber, prefix, delimiter);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final byte[] array, final int offset, final int length, final int columnNumber) {
    appendHexMatrixString(sb, array, offset, length, columnNumber, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final byte[] array, final int offset, final int length) {
    appendHexMatrixString(sb, array, offset, length, DEFAULT_COLUMN_NUMBER, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static void appendHexMatrixString(final StringBuilder sb, final byte[] array) {
    appendHexMatrixString(sb, array, 0, array.length, DEFAULT_COLUMN_NUMBER, DEFAULT_PREFIX, DEFAULT_DELIMITER);
  }

  public static String toHexString(final byte b) {
    StringBuilder sb = new StringBuilder();
    appendHexString(sb, b);
    return sb.toString();
  }

  public static String toHexString(final short s) {
    StringBuilder sb = new StringBuilder();
    appendHexString(sb, s);
    return sb.toString();
  }

  public static String toHexString(final int i) {
    StringBuilder sb = new StringBuilder();
    appendHexString(sb, i);
    return sb.toString();
  }

  public static String toHexString(final long l) {
    StringBuilder sb = new StringBuilder();
    appendHexString(sb, l);
    return sb.toString();
  }

  public static String toHexArrayString(final ByteBuffer buffer, final String prefix, final String delimiter) {
    StringBuilder sb = new StringBuilder();
    appendHexArrayString(sb, buffer, prefix, delimiter);
    return sb.toString();
  }

  public static String toHexArrayString(final ByteBuffer buffer) {
    StringBuilder sb = new StringBuilder();
    appendHexArrayString(sb, buffer);
    return sb.toString();
  }

  public static String toHexArrayString(final byte[] array, final int offset, final int length, final String prefix, final String delimiter) {
    StringBuilder sb = new StringBuilder();
    appendHexArrayString(sb, array, offset, length, prefix, delimiter);
    return sb.toString();
  }

  public static String toHexArrayString(final byte[] array, final int offset, final int length) {
    StringBuilder sb = new StringBuilder();
    appendHexArrayString(sb, array, offset, length);
    return sb.toString();
  }

  public static String toHexArrayString(final byte[] array, final String prefix, final String delimiter) {
    StringBuilder sb = new StringBuilder();
    appendHexArrayString(sb, array, prefix, delimiter);
    return sb.toString();
  }

  public static String toHexArrayString(final byte[] array) {
    StringBuilder sb = new StringBuilder();
    appendHexArrayString(sb, array);
    return sb.toString();
  }

  public static String toHexMatrixString(final ByteBuffer buffer) {
    StringBuilder sb = new StringBuilder();
    appendHexMatrixString(sb, buffer);
    return sb.toString();
  }

  public static String toHexMatrixString(final byte[] array, final int offset, final int length) {
    StringBuilder sb = new StringBuilder();
    appendHexMatrixString(sb, array, offset, length);
    return sb.toString();
  }

  public static String toHexMatrixString(final byte[] array) {
    StringBuilder sb = new StringBuilder();
    appendHexMatrixString(sb, array);
    return sb.toString();
  }

  public static byte[] toByteArray(final String hexString) {
    if (hexString.length() % 2 != 0) {
      throw new IllegalArgumentException(String.format("hexString must be of even length, but it's %d", hexString.length() % 2));
    }

    byte[] bytes = new byte[hexString.length() / 2];
    for (int i = 0; i < hexString.length(); i += 2) {
      bytes[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
    }
    return bytes;
  }

}
