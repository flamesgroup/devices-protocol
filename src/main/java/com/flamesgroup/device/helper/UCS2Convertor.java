/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

import java.io.UnsupportedEncodingException;

public final class UCS2Convertor {

  private static final int UTF16_BOM_HEX_LENGHT = 4;

  private UCS2Convertor() {
  }

  public static String hexToString(String hex) throws UnsupportedEncodingException {
    if (hex == null || hex.isEmpty()) {
      return hex;
    }

    hex = hex.substring(0, hex.length() - hex.length() % 4);
    byte[] bytes = new byte[hex.length() / 2];
    for (int i = 0; i < hex.length(); i += 2) {
      bytes[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
    }
    return new String(bytes, "UTF-16");
  }

  public static String stringtoHex(final String string) throws UnsupportedEncodingException {
    if (string == null || string.isEmpty()) {
      return string;
    }

    byte[] bytes = string.getBytes("UTF-16");
    StringBuilder builder = new StringBuilder();
    for (byte b : bytes) {
      int digit = b & 0xFF;
      if (digit < 0x10) {
        builder.append(0);
      }

      builder.append(Integer.toHexString(digit));
    }
    return builder.toString().substring(UTF16_BOM_HEX_LENGHT).toUpperCase();
  }

}
