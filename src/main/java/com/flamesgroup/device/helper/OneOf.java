/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

import java.io.Serializable;

public class OneOf<F, S> implements Serializable {

  private static final long serialVersionUID = 5141509555290278514L;

  private final F first;
  private final S second;

  private OneOf(final F f, final S s) {
    this.first = f;
    this.second = s;
  }

  public F first() {
    return first;
  }

  public S second() {
    return second;
  }

  public static <F, S> OneOf<F, S> isTheFirst(final F first) {
    return new OneOf<>(first, null);
  }

  public static <F, S> OneOf<F, S> isTheSecond(final S second) {
    return new OneOf<>(null, second);
  }
}
