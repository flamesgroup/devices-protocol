/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

public class ServiceInfoEncodingHelper {

  public static String firmwareVersionToString(final int version) {
    int versionSchemeN = version >>> 30;
    if (versionSchemeN == 1) {
      return new StringBuilder()
          .append((version >> 26) & 0xF).append('.')
          .append((version >> 18) & 0xFF).append('.')
          .append((version >> 10) & 0xFF).append('b')
          .append(version & 0x3FF).toString();
    } else if (versionSchemeN == 2) {
      return new StringBuilder()
          .append((version >> 24) & 0x3F).append('.')
          .append((version >> 16) & 0xFF).append('.')
          .append(version & 0xFFFF).toString();
    } else if (versionSchemeN == 3) {
      return new StringBuilder()
          .append(((version >> 29) & 0x01) == 1 ? 'R' : 'D').append('.')
          .append((version >> 24) & 0x1F).append('.')
          .append((version >> 16) & 0xFF).append('.')
          .append(version & 0xFFFF).toString();
    } else {
      return Integer.toString(version);
    }
  }

}
