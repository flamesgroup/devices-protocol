/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.IChannelDataHandler;

import java.nio.ByteBuffer;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public final class FakeChannelDataHandler implements IChannelDataHandler {

  private final Semaphore semaphore = new Semaphore(0);

  private final long receiveTimeout;
  private final ByteBuffer receiveData;
  private final byte errorCode;

  public FakeChannelDataHandler() {
    this(0, null, (byte) 0);
  }

  public FakeChannelDataHandler(final long receiveTimeout, final ByteBuffer receiveData, final byte errorCode) {
    this.receiveTimeout = receiveTimeout;
    this.receiveData = receiveData;
    this.errorCode = errorCode;
  }

  @Override
  public void handleReceive(final ByteBuffer data) {
    if (data.equals(receiveData)) {
      semaphore.release();
    }
  }

  @Override
  public void handleErrCode(final ChannelErrCodeException e) {
    if (e.getErrCode() == errorCode) {
      semaphore.release();
    }
  }

  public boolean isHandledReceive() {
    try {
      return semaphore.tryAcquire(receiveTimeout, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      return false;
    }
  }

  public boolean isHandledErrCode() {
    try {
      return semaphore.tryAcquire(receiveTimeout, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      return false;
    }
  }

}
