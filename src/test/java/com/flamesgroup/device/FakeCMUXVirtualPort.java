/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPort;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPortHandler;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FakeCMUXVirtualPort extends GenericByteBufferExchangerIterator<FakeCMUXVirtualPort.IterationData, FakeCMUXVirtualPort.Iteration> implements ICMUXVirtualPort {

  private ICMUXVirtualPortHandler cmuxVirtualPortHandler;

  public FakeCMUXVirtualPort() {
    this(Collections.emptyList());
  }

  public FakeCMUXVirtualPort(final FakeCMUXVirtualPort.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeCMUXVirtualPort(final List<FakeCMUXVirtualPort.Iteration> iterations) {
    super(iterations);
  }

  @Override
  public boolean isOpen() {
    return cmuxVirtualPortHandler != null;
  }

  @Override
  public void open(final ICMUXVirtualPortHandler cmuxVirtualPortHandler) throws ChannelException, InterruptedException {
    this.cmuxVirtualPortHandler = cmuxVirtualPortHandler;

    if (iterations.isEmpty()) {
      return;
    }

    startIterate();
  }

  @Override
  public void close() throws ChannelException, InterruptedException {
    cmuxVirtualPortHandler = null;
  }

  @Override
  public void sendData(final ByteBuffer data) throws ChannelException, InterruptedException {
    send(new IterationData(data));
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeCMUXVirtualPort.IterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {
      @Override
      public Exception call() throws Exception {
        try {
          cmuxVirtualPortHandler.handleData(iterationData.getData());
          return null;
        } catch (Exception e) {
          return e;
        }
      }
    });
  }

  public static class Iteration extends GenericByteBufferExchangerIterator.Iteration<FakeCMUXVirtualPort.IterationData> {

    public Iteration(final IterationData sendIterationData, final long timeout, final IterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class IterationData extends GenericByteBufferExchangerIterator.IterationData {

    public IterationData(final ByteBuffer data) {
      super(data);
    }

    public IterationData(final byte[] data) {
      super(data);
    }

  }

}
