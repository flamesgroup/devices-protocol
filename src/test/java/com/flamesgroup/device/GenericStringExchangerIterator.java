/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import java.util.List;

public abstract class GenericStringExchangerIterator<ID extends GenericStringExchangerIterator.IterationData, I extends GenericStringExchangerIterator.Iteration<ID>>
    extends GenericExchangerIterator<String, String, ID, ID, I> {

  protected GenericStringExchangerIterator(final List<I> iterations) {
    super(iterations);
  }

  public static class Iteration<ID extends IterationData> extends GenericExchangerIterator.Iteration<ID, ID> {

    public Iteration(final ID sendIterationData, final long timeout, final ID receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class IterationData extends GenericExchangerIterator.IterationData<String> {

    public IterationData(final String str) {
      super(str);
    }

    @Override
    public String toString() {
      return getData();
    }

  }

}
