/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public class LoggerChannelTest {

  @Test
  public void testReadJournalRecord() throws Exception {
    ByteBuffer recordNumberBuffer = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
    recordNumberBuffer.putShort((short) 0);
    recordNumberBuffer.rewind();
    FakeMudpChannel.IterationData sendIterationData = new FakeMudpChannel.IterationData((byte) 0x03, recordNumberBuffer);

    ByteBuffer journalRecordBuffer = ByteBuffer.allocate(64).order(ByteOrder.LITTLE_ENDIAN);
    journalRecordBuffer.putInt(1);
    journalRecordBuffer.putInt(2000);
    journalRecordBuffer.put("hello".getBytes(Charset.forName("ISO-8859-1")));
    journalRecordBuffer.rewind();
    FakeMudpChannel.IterationData receiveIterationData = new FakeMudpChannel.IterationData((byte) 0x03, journalRecordBuffer);

    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(sendIterationData, 5, receiveIterationData);
    ILoggerChannel loggerChannel = new LoggerChannel(new FakeMudpChannel(iteration), new MudpChannelAddress((byte) 0), 0);
    loggerChannel.enslave();
    JournalRecord record = loggerChannel.readJournalRecord((short) 0);
    assertNotNull(record);
    assertEquals(1, record.getIndex());
    assertEquals(2000, record.getTimestamp());
    assertEquals("hello", record.getComment());
    loggerChannel.free();
    assertFalse(loggerChannel.isEnslaved());
  }

  @Test(expected = ChannelCommandDecodeException.class)
  public void testReadJournalRecordWithZeroDataLength() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x03, ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN));
    ILoggerChannel loggerChannel = new LoggerChannel(new FakeMudpChannel(new FakeMudpChannel.Iteration(iterationData, 5, iterationData)), new MudpChannelAddress((byte) 0), 0);
    loggerChannel.enslave();
    loggerChannel.readJournalRecord((short) 0);
    assertFalse(loggerChannel.isEnslaved());
  }

  @Test
  public void testEraseJournal() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x04, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    ILoggerChannel loggerChannel = new LoggerChannel(new FakeMudpChannel(new FakeMudpChannel.Iteration(iterationData, 50, iterationData)), new MudpChannelAddress((byte) 0), 0);
    loggerChannel.enslave();
    loggerChannel.eraseJournal();
    loggerChannel.free();
    assertFalse(loggerChannel.isEnslaved());
  }

}
