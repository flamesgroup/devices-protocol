/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JournalRecordTest {

  @Test
  public void testJournalRecordIsNotEmpty() {
    JournalRecord notEmpty1 = new JournalRecord(1, 100, "qwerty");
    assertFalse(notEmpty1.isEmpty());
    JournalRecord notEmpty2 = new JournalRecord(1, -1, null);
    assertFalse(notEmpty2.isEmpty());
    JournalRecord notEmpty3 = new JournalRecord(-1, 100, null);
    assertFalse(notEmpty3.isEmpty());
  }

  @Test
  public void testJournalRecordIsEmpty() {
    JournalRecord empty1 = new JournalRecord(-1, -1, "qwerty");
    assertTrue(empty1.isEmpty());
    JournalRecord empty2 = new JournalRecord(-1, -1, null);
    assertTrue(empty2.isEmpty());
  }

}
