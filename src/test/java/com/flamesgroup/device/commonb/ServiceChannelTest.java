/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.ChannelInvalidCommandDataException;
import com.flamesgroup.device.commonb.command.ApplyFWErrException;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.IMudpChannelHandler;
import com.flamesgroup.device.protocol.mudp.IMudpConnection;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.mudp.MudpOptions;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ServiceChannelTest {

  @Test
  public void testGetInfo() throws Exception {
    ByteBuffer data = ByteBuffer.allocate(21).order(ByteOrder.LITTLE_ENDIAN);
    data.clear();
    data.putInt(8);
    data.putInt(23);
    data.putInt(31);
    int deviceUID = new DeviceUID(DeviceType.SIMB, 154).getUID();
    data.putInt(deviceUID);
    data.putInt(69);
    data.put((byte) 80);
    data.flip();

    IMudpConnection mudpConnection = mkMudpConnection((byte) 0x01, data);
    ServiceChannel serviceChannel = new ServiceChannel(mudpConnection.createChannel(), new MudpChannelAddress((byte) 0), 0);
    serviceChannel.enslave();
    ServiceInfo serviceInfo = serviceChannel.getInfo();
    assertEquals(FirmwareType.SIMB_LAN, serviceInfo.getFirmwareType());
    assertEquals(23, serviceInfo.getFirmwareVersion());
    assertEquals(31, serviceInfo.getFirmwareUpdaterVersion());
    assertEquals(DeviceType.SIMB, serviceInfo.getDeviceUID().getDeviceType());
    assertEquals(154, serviceInfo.getDeviceUID().getDeviceNumber());
    assertEquals(69, serviceInfo.getOsTime());
    assertEquals(80, serviceInfo.getCpuUsage());
  }

  @Test
  public void testReset() throws Exception {
    IMudpConnection mudpConnection = mkMudpConnection((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    ServiceChannel serviceChannel = new ServiceChannel(mudpConnection.createChannel(), new MudpChannelAddress((byte) 0), 0);
    serviceChannel.enslave();
    serviceChannel.reset(1250);
  }

  @Test(expected = ChannelInvalidCommandDataException.class)
  public void testError() throws Exception {
    IMudpConnection mudpConnection = mkMudpConnection((byte) 0x31, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    ServiceChannel serviceChannel = new ServiceChannel(mudpConnection.createChannel(), new MudpChannelAddress((byte) 0), 0);
    serviceChannel.enslave();
    serviceChannel.getInfo();
  }

  @Test(expected = ApplyFWErrException.class)
  public void testSpecificError() throws Exception {
    IMudpConnection mudpConnection = mkMudpConnection((byte) 0x85, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    ServiceChannel serviceChannel = new ServiceChannel(mudpConnection.createChannel(), new MudpChannelAddress((byte) 0), 0);
    serviceChannel.enslave();
    serviceChannel.applyFW(0);
  }

  @Test
  public void testChangeFWSerialNumberCommand() throws Exception {
    IMudpConnection mudpConnection = mkMudpConnection((byte) 0xA, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    ServiceChannel serviceChannel = new ServiceChannel(mudpConnection.createChannel(), new MudpChannelAddress((byte) 0), 0);
    serviceChannel.enslave();
    serviceChannel.changeFWSerialNumber(new byte[48]);
  }

  @Test
  public void testWriteCipheredFWBlock() throws Exception {
    IMudpConnection mudpConnection = mkMudpConnection(Arrays.asList(
        new DummyResponse((byte) 0x08, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN)),
        new DummyResponse((byte) 0x09, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN))));
    ServiceChannel serviceChannel = new ServiceChannel(mudpConnection.createChannel(), new MudpChannelAddress((byte) 0), 0);
    serviceChannel.enslave();
    serviceChannel.writeCipheredFWBlock((short) 0, new CipheredFWBlock());
  }

  private IMudpConnection mkMudpConnection(final byte handleType, final ByteBuffer handleData) {
    return mkMudpConnection(Arrays.asList(new DummyResponse(handleType, handleData)));
  }

  private IMudpConnection mkMudpConnection(final List<DummyResponse> dummyResponseList) {

    final Iterator<DummyResponse> dummyResponseIterator = dummyResponseList.iterator();

    return new IMudpConnection() {

      @Override
      public boolean isConnect() {
        return true;
      }

      @Override
      public void connect() throws MudpException {
      }

      @Override
      public void disconnect() throws MudpException {
      }

      @Override
      public IMudpChannel createChannel() {
        return new IMudpChannel() {

          private boolean enslaved;
          private IMudpChannelHandler mudpChannelHandler;

          @Override
          public boolean isEnslaved() {
            return enslaved;
          }

          @Override
          public void enslave(final MudpChannelAddress mudpChannelAddress, final IMudpChannelHandler mudpChannelHandler) {
            enslaved = true;
            this.mudpChannelHandler = mudpChannelHandler;
          }

          @Override
          public void sendReliable(final byte type, final ByteBuffer data) throws MudpException {
            new Thread(() -> {
              if (dummyResponseIterator.hasNext()) {
                DummyResponse dummyResponse = dummyResponseIterator.next();
                mudpChannelHandler.handleReceive(dummyResponse.getType(), dummyResponse.getData());
              }
            }).start();
          }

          @Override
          public void sendUnreliable(final byte type, final ByteBuffer data) throws MudpException {
            fail("ServiceChannel must not use unreliable send method");
          }

          @Override
          public MudpOptions getMudpOptions() {
            return new MudpOptions(1, 500, 0, 0);
          }

          @Override
          public void free() throws MudpException {
            enslaved = false;
          }
        };
      }

      @Override
      public MudpOptions getMudpOptions() {
        return new MudpOptions(1, 500, 0, 0);
      }

    };

  }

  private static class DummyResponse {

    private final byte type;
    private final ByteBuffer data;

    public DummyResponse(final byte type, final ByteBuffer data) {
      this.type = type;
      this.data = data;
    }

    public byte getType() {
      return type;
    }

    public ByteBuffer getData() {
      return data;
    }

  }

}
