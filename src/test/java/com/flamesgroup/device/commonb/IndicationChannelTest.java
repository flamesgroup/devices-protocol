/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.commonb;

import static org.junit.Assert.assertFalse;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class IndicationChannelTest {

  @Test
  public void testSetIndicationMode() throws Exception {
    final List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    {
      ByteBuffer bbIndicationMode = ByteBuffer.wrap(new byte[] {IndicationMode.READY.getN()}).order(ByteOrder.LITTLE_ENDIAN);
      FakeMudpChannel.IterationData setIndicationModeSendIterationData = new FakeMudpChannel.IterationData((byte) 0x00, bbIndicationMode);
      FakeMudpChannel.IterationData setIndicationModeReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));

      iterations.add(new FakeMudpChannel.Iteration(setIndicationModeSendIterationData, 50, setIndicationModeReceiveIterationData));
    }
    {
      ByteBuffer bbIndicationMode = ByteBuffer.wrap(new byte[] {IndicationMode.ACTIVE.getN()}).order(ByteOrder.LITTLE_ENDIAN);
      FakeMudpChannel.IterationData setIndicationModeSendIterationData = new FakeMudpChannel.IterationData((byte) 0x00, bbIndicationMode);
      FakeMudpChannel.IterationData setIndicationModeReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));

      iterations.add(new FakeMudpChannel.Iteration(setIndicationModeSendIterationData, 50, setIndicationModeReceiveIterationData));
    }

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    IIndicationChannel indicationChannel = new IndicationChannel(fakeMudpChannel, new MudpChannelAddress((byte) 0), 0);
    indicationChannel.enslave();
    indicationChannel.setIndicationMode(IndicationMode.READY);
    indicationChannel.setIndicationMode(IndicationMode.ACTIVE);
    indicationChannel.free();
    assertFalse(indicationChannel.isEnslaved());

    fakeMudpChannel.checkForException();
  }

}
