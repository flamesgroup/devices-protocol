/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class DataRepresentationHelperTest {

  private ByteBuffer buffer;

  @Before
  public void setUp() {
    buffer = ByteBuffer.allocate(64);
  }

  @After
  public void tearDown() {
    buffer = null;
  }

  @Test
  public void toHexArrayStringTest() {
    buffer.clear();

    buffer.put((byte) 0x01).put((byte) 0x02).put((byte) 0x03);

    buffer.flip();
    String hexArrayString = DataRepresentationHelper.toHexArrayString(buffer);

    assertEquals(hexArrayString, "0x01 0x02 0x03");
  }

}
