/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ServiceInfoEncodingHelperTest {

  @Test
  public void testFirmwareVersionSchemeFirst() throws Exception {
    assertEquals("1.3.0b40", ServiceInfoEncodingHelper.firmwareVersionToString(0x440C0028));
  }

  @Test
  public void testFirmwareVersionSchemeSecond() throws Exception {
    assertEquals("2.0.4", ServiceInfoEncodingHelper.firmwareVersionToString(0x82000004));
  }

  @Test
  public void testFirmwareVersionSchemeThird() throws Exception {
    assertEquals("D.2.0.4", ServiceInfoEncodingHelper.firmwareVersionToString(0xC2000004));
    assertEquals("R.2.0.4", ServiceInfoEncodingHelper.firmwareVersionToString(0xE2000004));
  }

  @Test
  public void testFirmwareVersionSchemeUndefined() throws Exception {
    assertEquals("1", ServiceInfoEncodingHelper.firmwareVersionToString(0x00000001));
  }

}
