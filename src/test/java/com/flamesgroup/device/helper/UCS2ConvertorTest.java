/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.helper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UCS2ConvertorTest {

  @Test
  public void testHexToString() throws Exception {
    assertEquals("Ваш  запит прийнято", UCS2Convertor.hexToString("0412043004480020002004370430043F043804420020043F044004380439043D044F0442043E"));
    assertEquals("Ваш  запит прийнят", UCS2Convertor.hexToString("0412043004480020002004370430043F043804420020043F044004380439043D044F0442043"));
    assertEquals("Ваш  запит прийнят", UCS2Convertor.hexToString("0412043004480020002004370430043F043804420020043F044004380439043D044F044204"));
    assertEquals("Ваш  запит прийнят", UCS2Convertor.hexToString("0412043004480020002004370430043F043804420020043F044004380439043D044F04420"));
    assertEquals("Ваш  запит прийнят", UCS2Convertor.hexToString("0412043004480020002004370430043F043804420020043F044004380439043D044F0442"));
    assertEquals("", UCS2Convertor.hexToString("041"));
    assertEquals("", UCS2Convertor.hexToString("04"));
    assertEquals("", UCS2Convertor.hexToString("0"));
    assertEquals("", UCS2Convertor.hexToString(""));
    assertEquals("Скористайтесь перевагами Beeline Menu у вашому телефоні. Інфо 067462",
        UCS2Convertor.hexToString("0421043A043E044004380441044204300439044204350441044C0020043F0435044004350432043004330430043C04380020004200650065006C0069006E0065"
            + "0020004D0065006E0075002004430020043204300448043E043C0443002004420435043B04350444043E043D0456002E00200406043D0444043E0020003000360037003400360032"));
    assertEquals(null, UCS2Convertor.hexToString(null));
  }

  @Test
  public void testStringToHex() throws Exception {
    assertEquals("0412043004480020002004370430043F043804420020043F044004380439043D044F0442043E", UCS2Convertor.stringtoHex("Ваш  запит прийнято"));
    assertEquals("", UCS2Convertor.stringtoHex(""));
    assertEquals(null, UCS2Convertor.stringtoHex(null));
  }

}
