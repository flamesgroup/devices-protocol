/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.channel;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeChannelDataHandler;
import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ChannelDataTest {

  @Test(expected = ChannelDataDetachedException.class)
  public void testIsAttachedSendReliable() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.sendReliable(ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
  }

  @Test(expected = ChannelDataDetachedException.class)
  public void testIsAttachedSendUnreliable() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.sendUnreliable(ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
  }

  @Test
  public void testAttach() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.attach((byte) 0x02, new FakeChannelDataHandler());
    assertTrue(channelData.isAttached());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAttachWithTypeLessThenMinValue() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.attach((byte) -1, new FakeChannelDataHandler());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAttachWithTypeGreatThenMaxValue() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.attach((byte) 0x10, new FakeChannelDataHandler());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAttachWithNoHandler() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.attach((byte) 0x02, null);
  }

  @Test
  public void testDetach() throws Exception {
    IChannelData channelData = new ChannelData(new Channel(new FakeMudpChannel(), new MudpChannelAddress()));
    channelData.attach((byte) 0x02, new FakeChannelDataHandler());
    assertTrue(channelData.isAttached());
    channelData.detach();
    assertFalse(channelData.isAttached());
  }

  @Test
  public void testSendReliable() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(iterationData, 50, iterationData);
    Channel channel = new Channel(new FakeMudpChannel(iteration), new MudpChannelAddress());
    channel.enslave();
    IChannelData channelData = new ChannelData(channel);
    FakeChannelDataHandler channelDataHandler = new FakeChannelDataHandler(200, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN), (byte) 0);
    channelData.attach((byte) 0x02, channelDataHandler);
    channelData.sendReliable(ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    assertTrue(channelDataHandler.isHandledReceive());
  }

  @Test
  public void testSendUnreliable() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(iterationData, 50, iterationData);
    Channel channel = new Channel(new FakeMudpChannel(iteration), new MudpChannelAddress());
    channel.enslave();
    IChannelData channelData = new ChannelData(channel);
    FakeChannelDataHandler channelDataHandler = new FakeChannelDataHandler(200, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN), (byte) 0);
    channelData.attach((byte) 0x02, channelDataHandler);
    channelData.sendUnreliable(ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    assertTrue(channelDataHandler.isHandledReceive());
  }

}
