/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.channel;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeChannelDataHandler;
import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ChannelTest {

  @Test
  public void testManipulateWithChannelDataHandlers() throws Exception {
    Channel channel = new Channel(new FakeMudpChannel(), new MudpChannelAddress());
    channel.addToAttachedChannelDataHandlers((byte) 0x02, new FakeChannelDataHandler());
    assertNotNull(channel.getAttachedChannelDataHandlerByType((byte) 0x02));
    assertNull(channel.getAttachedChannelDataHandlerByType((byte) 0x03));
    channel.removeFromAttachedChannelDataHandlers((byte) 0x02);
    assertNull(channel.getAttachedChannelDataHandlerByType((byte) 0x02));
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testCheckRangeOfChannelDataHandlers() throws Exception {
    Channel channel = new Channel(new FakeMudpChannel(), new MudpChannelAddress());
    assertNull(channel.getAttachedChannelDataHandlerByType((byte) 0xFF));
  }

  @Test
  public void testCheckCallbacksWithSendReliable() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(iterationData, 50, iterationData);
    Channel channel = new Channel(new FakeMudpChannel(iteration), new MudpChannelAddress());
    channel.enslave();
    FakeChannelDataHandler channelDataHandler = new FakeChannelDataHandler(200, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN), (byte) 0);
    channel.addToAttachedChannelDataHandlers((byte) 0x02, channelDataHandler);
    channel.sendReliable((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    assertTrue(channelDataHandler.isHandledReceive());
  }

  @Test
  public void testCheckCallbacksWithSendReliableAndError() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x12, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(iterationData, 50, iterationData);
    Channel channel = new Channel(new FakeMudpChannel(iteration), new MudpChannelAddress());
    channel.enslave();
    FakeChannelDataHandler channelDataHandler = new FakeChannelDataHandler(200, null, (byte) (0x12 & 0xF0));
    channel.addToAttachedChannelDataHandlers((byte) 0x02, channelDataHandler);
    channel.sendReliable((byte) 0x12, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    assertTrue(channelDataHandler.isHandledErrCode());
  }

  @Test
  public void testCheckCallbacksWithSendUnreliable() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(iterationData, 50, iterationData);
    Channel channel = new Channel(new FakeMudpChannel(iteration), new MudpChannelAddress());
    channel.enslave();
    FakeChannelDataHandler channelDataHandler = new FakeChannelDataHandler(200, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN), (byte) 0);
    channel.addToAttachedChannelDataHandlers((byte) 0x02, channelDataHandler);
    channel.sendUnreliable((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    assertTrue(channelDataHandler.isHandledReceive());
  }

  @Test
  public void testCheckCallbacksWithSendUnreliableAndError() throws Exception {
    FakeMudpChannel.IterationData iterationData = new FakeMudpChannel.IterationData((byte) 0x12, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(iterationData, 50, iterationData);
    Channel channel = new Channel(new FakeMudpChannel(iteration), new MudpChannelAddress());
    channel.enslave();
    FakeChannelDataHandler channelDataHandler = new FakeChannelDataHandler(200, null, (byte) (0x12 & 0xF0));
    channel.addToAttachedChannelDataHandlers((byte) 0x02, channelDataHandler);
    channel.sendUnreliable((byte) 0x12, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    assertTrue(channelDataHandler.isHandledErrCode());
  }

}
