/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.protocol.mudp.MudpIOException;
import com.flamesgroup.device.protocol.mudp.MudpPacket;
import com.flamesgroup.device.protocol.mudp.MudpPacketWriteException;

import java.util.List;

public class FakeMudpStreamWithThrowMudpIOExceptionOnWriteMudpPacket extends FakeMudpStream {

  private int throwOnIteration;

  public FakeMudpStreamWithThrowMudpIOExceptionOnWriteMudpPacket(final List<Iteration> iterations, final int throwOnIteration) {
    super(iterations);
    this.throwOnIteration = throwOnIteration;
  }

  @Override
  public void writeMudpPacket(final MudpPacket mudpPacket) throws MudpIOException, MudpPacketWriteException {
    if (throwOnIteration-- != 0) {
      super.writeMudpPacket(mudpPacket);
    } else {
      throw new MudpIOException();
    }
  }

}
