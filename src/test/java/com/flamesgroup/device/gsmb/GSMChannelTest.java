/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class GSMChannelTest {

  @Test
  public void testGetIMEI() throws Exception {
    ByteBuffer bbImei = ByteBuffer.wrap(new byte[] {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x39, 0x39, 0x39, 0x39, 0x39}).order(ByteOrder.LITTLE_ENDIAN);
    FakeMudpChannel.IterationData getIMEISendIterationData = new FakeMudpChannel.IterationData((byte) 0x03, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData getIMEIReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x03, bbImei);

    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(getIMEISendIterationData, 50, getIMEIReceiveIterationData);
    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(iteration), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();

    byte[] imei = gsmChannel.getIMEI();
    assertNotNull(imei);
    assertEquals(15, imei.length);
    assertArrayEquals(imei, bbImei.array());

    gsmChannel.free();
    assertFalse(gsmChannel.isEnslaved());
  }

  @Test(expected = ChannelCommandDecodeException.class)
  public void testGetIMEIWithIncorrectLength() throws Exception {
    ByteBuffer bbImei = ByteBuffer.wrap(new byte[] {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39}).order(ByteOrder.LITTLE_ENDIAN);
    FakeMudpChannel.IterationData getIMEISendIterationData = new FakeMudpChannel.IterationData((byte) 0x03, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData getIMEIReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x03, bbImei);

    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(getIMEISendIterationData, 50, getIMEIReceiveIterationData);
    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(iteration), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.getIMEI();
  }

  @Test(expected = ChannelCommandDecodeException.class)
  public void testGetIMEIWithIncorrectChars() throws Exception {
    ByteBuffer bbImei = ByteBuffer.wrap(new byte[] {0x29, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30}).order(ByteOrder.LITTLE_ENDIAN);
    FakeMudpChannel.IterationData getIMEISendIterationData = new FakeMudpChannel.IterationData((byte) 0x03, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData getIMEIReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x03, bbImei);

    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(getIMEISendIterationData, 50, getIMEIReceiveIterationData);
    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(iteration), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.getIMEI();
  }

  @Test
  public void testSetIMEI() throws Exception {
    ByteBuffer bbImei = ByteBuffer.wrap(new byte[] {0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30}).order(ByteOrder.LITTLE_ENDIAN);
    FakeMudpChannel.IterationData setIMEISendIterationData = new FakeMudpChannel.IterationData((byte) 0x04, bbImei);
    FakeMudpChannel.IterationData setIMEIReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x04, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));

    FakeMudpChannel.Iteration iteration = new FakeMudpChannel.Iteration(setIMEISendIterationData, 50, setIMEIReceiveIterationData);
    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(iteration), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.setIMEI(new byte[] {0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30});
    gsmChannel.free();
    assertFalse(gsmChannel.isEnslaved());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetIMEIWithIncorrectLength() throws Exception {
    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(new ArrayList<>()), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.setIMEI(new byte[10]);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetIMEIWithIncorrectChars() throws Exception {
    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(new ArrayList<>()), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.setIMEI(new byte[] {0x29, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30});
  }

}
