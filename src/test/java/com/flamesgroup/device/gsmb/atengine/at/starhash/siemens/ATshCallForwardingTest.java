/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import com.flamesgroup.device.gsmb.atengine.at.starhash.siemens.ATshCallForwardingResult.CallForwardingInfo;
import com.flamesgroup.device.gsmb.atengine.param.starhash.PshCallForwarding.Reason;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ATshCallForwardingTest {

  @Test
  public void testATCallForwarding() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "ATD*#21#;\r",
        0,
        "ATD*#21#;\r\r\n^SCCFC: 0,0,7\r\n\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "ATD*#21#;\r",
        0,
        "ATD*#21#;\r"
            + "\r\n^SCCFC: 0,0,1\r\n"
            + "\r\n^SCCFC: 0,0,2\r\n"
            + "\r\n^SCCFC: 0,0,4\r\n"
            + "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATshCallForwardingResult result = fakeATEngine.execute(new ATshCallForwarding("*#21#"));
    ATshCallForwardingResult result2 = fakeATEngine.execute(new ATshCallForwarding("*#21#"));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    assertEquals(1, result.getCallForwardingInfoList().size());

    CallForwardingInfo callForwardingInfo = result.getCallForwardingInfoList().get(0);
    assertNotNull("Unexpected value of callForwardingInfo", callForwardingInfo);
    assertEquals("Unexpected value of reason", Reason.UNCONDITIONAL, callForwardingInfo.getReason());
    assertFalse("Unexpected value of active", callForwardingInfo.isActive());
    assertEquals("Unexpected value of serviceClasses size", 3, callForwardingInfo.getServiceClasses().size());
    assertTrue("Unexpected value of serviceClass", callForwardingInfo.getServiceClasses().contains(ServiceClass.VOICE));
    assertTrue("Unexpected value of serviceClass", callForwardingInfo.getServiceClasses().contains(ServiceClass.DATA));
    assertTrue("Unexpected value of serviceClass", callForwardingInfo.getServiceClasses().contains(ServiceClass.FAX));

    assertEquals(1, result.getCallForwardingInfoList().size());

    callForwardingInfo = result2.getCallForwardingInfoList().get(0);
    assertNotNull("Unexpected value of callForwardingInfo", callForwardingInfo);
    assertEquals("Unexpected value of reason", Reason.UNCONDITIONAL, callForwardingInfo.getReason());
    assertFalse("Unexpected value of active", callForwardingInfo.isActive());
    assertEquals("Unexpected value of serviceClasses size", 1, callForwardingInfo.getServiceClasses().size());
    assertTrue("Unexpected value of serviceClass", callForwardingInfo.getServiceClasses().contains(ServiceClass.VOICE));

    callForwardingInfo = result2.getCallForwardingInfoList().get(1);
    assertNotNull("Unexpected value of callForwardingInfo", callForwardingInfo);
    assertEquals("Unexpected value of reason", Reason.UNCONDITIONAL, callForwardingInfo.getReason());
    assertFalse("Unexpected value of active", callForwardingInfo.isActive());
    assertEquals("Unexpected value of serviceClasses size", 1, callForwardingInfo.getServiceClasses().size());
    assertTrue("Unexpected value of serviceClass", callForwardingInfo.getServiceClasses().contains(ServiceClass.DATA));

    callForwardingInfo = result2.getCallForwardingInfoList().get(2);
    assertNotNull("Unexpected value of callForwardingInfo", callForwardingInfo);
    assertEquals("Unexpected value of reason", Reason.UNCONDITIONAL, callForwardingInfo.getReason());
    assertFalse("Unexpected value of active", callForwardingInfo.isActive());
    assertEquals("Unexpected value of serviceClasses size", 1, callForwardingInfo.getServiceClasses().size());
    assertTrue("Unexpected value of serviceClass", callForwardingInfo.getServiceClasses().contains(ServiceClass.FAX));
  }

}
