/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URChhSMOVTest {

  private final List<String> handlerValues = new ArrayList<>();

  private class URChhSMOVHandler implements IURChhSMOVHandler {

    @Override
    public void handleSMSOverflow(final String memory) {
      handlerValues.add(memory);
    }

  }

  @Test
  public void testURChhSMOVParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#SMOV: \"SM\"\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhSMOV(new URChhSMOVHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of memory", "\"SM\"", handlerValues.get(0));

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURChhSMOVUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#SMOV: \"ME\"\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhSMOV(new URChhSMOVHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("#SMOV: \"ME\"", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
