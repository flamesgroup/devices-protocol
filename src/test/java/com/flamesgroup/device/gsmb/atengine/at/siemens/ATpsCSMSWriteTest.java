/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATpsCSMSWriteResult;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.BroadcastTypeMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileOriginatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileTerminatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PpsCSMS.MessagingService;
import org.junit.Test;

import java.util.ArrayList;

public class ATpsCSMSWriteTest {

  @Test
  public void testATpsCSMSWriteParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CSMS=1\r",
        0,
        "\r\n+CSMS: 1,1,1\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCSMSWriteResult result = fakeATEngine.execute(new ATpsCSMSWrite(MessagingService.COMPATIBLE_WITH_GSM_07_05_PHASE_2_PLUS_VERSION));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of mobileTerminatedMessages", MobileTerminatedMessages.TYPE_SUPPORTED, result.getMobileTerminatedMessages());
    assertEquals("Unexpected value of mobileOriginatedMessages", MobileOriginatedMessages.TYPE_SUPPORTED, result.getMobileOriginatedMessages());
    assertEquals("Unexpected value of broadcastTypeMessages", BroadcastTypeMessages.TYPE_SUPPORTED, result.getBroadcastTypeMessages());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
