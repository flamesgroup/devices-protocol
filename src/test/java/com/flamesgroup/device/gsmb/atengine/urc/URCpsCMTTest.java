/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.unit.sms.SMSHelper;
import org.junit.Test;

public class URCpsCMTTest {

  private static final String PDU = "0791836003000080040C9183907320132300005190920155532104D4F29C0E";

  private Integer handledSMSLength;
  private String handledSMSPdu;

  private final class URCpsCMTHandler implements IURCpsCMTHandler {

    @Override
    public void handleSMS(final int length, final String pdu) {
      handledSMSLength = length;
      handledSMSPdu = pdu;
    }

  }

  @Test
  public void testSiemensURCpsCMTParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMT: ,23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU", PDU, handledSMSPdu);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testSiemensURCpsCMTUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMTI: ,23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("+CMTI: ,23", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

  @Test
  public void testSiemensURCpsCMTCheckSizeOfPduUserData() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMT: ,23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU length", SMSHelper.getPduSize(PDU), (int) handledSMSLength);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testTelitURCpsCMTParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMT: \"\",23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU", PDU, handledSMSPdu);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testTelitURCpsCMTParsingPDUWithAlpha() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMT: \"life3).!/*&$g\",23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU", PDU, handledSMSPdu);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testTelitURCpsCMTUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMTI: ,23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("+CMTI: ,23", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

  @Test
  public void testTelitURCpsCMTCheckSizeOfPduUserData() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CMT: \"\",23\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMT(new URCpsCMTHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU length", SMSHelper.getPduSize(PDU), (int) handledSMSLength);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
