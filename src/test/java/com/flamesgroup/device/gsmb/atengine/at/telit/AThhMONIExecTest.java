/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AThhMONIExecTest {

  @Test
  public void testAThhMONIExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#MONI\r",
        0,
        "\r\n#MONI: Cell  BSIC  LAC  CellId  ARFCN    Power  C1  C2  TA  RxQual  PLMN\r\n" +
            "#MONI:  S    20   2776   0228     698   -62dbm  44  45   1     0    life:)\r\n" +
            "#MONI: N1    21   2776   0227     700   -81dbm  25  25 \r\n" +
            "#MONI: N2    05   2776   0069      76   -92dbm  14  -16 \r\n" +
            "#MONI: N3    25   2776   0295     708   -92dbm  14  14 \r\n" +
            "#MONI: N4    04   2776   0065     703   -93dbm  13  13 \r\n" +
            "#MONI: N5    21   2776   008E     711   -99dbm   7   7 \r\n" +
            "#MONI: N6    24   2776   006A      82   -103dbm  3  -27 \r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhMONIExecResult result = fakeATEngine.execute(new AThhMONIExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(7, result.getCellList().size());

    List<AThhMONIExecResult.CellInfo> cellInfoList = result.getCellList();
    AThhMONIExecResult.CellInfo cellInfo = cellInfoList.get(0);
    assertEquals("Unexpected value of cell", "S", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "20", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "228", Integer.toHexString(cellInfo.getCellId()));
    assertEquals("Unexpected value of ARFCN", 698, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -62, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 44, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 45, cellInfo.getC2());
    assertEquals("Unexpected value of TA", 1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", 0, cellInfo.getRxQual());
    assertEquals("Unexpected value of PLMN", "life:)", cellInfo.getPlmn());

    cellInfo = cellInfoList.get(1);
    assertEquals("Unexpected value of cell", "N1", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "21", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "227", Integer.toHexString(cellInfo.getCellId()));
    assertEquals("Unexpected value of ARFCN", 700, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -81, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 25, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 25, cellInfo.getC2());
    assertEquals("Unexpected value of TA", -1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", -1, cellInfo.getRxQual());
    assertNull("Unexpected value of PLMN", cellInfo.getPlmn());

    cellInfo = cellInfoList.get(2);
    assertEquals("Unexpected value of cell", "N2", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "5", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "69", Integer.toHexString(cellInfo.getCellId()));
    assertEquals("Unexpected value of ARFCN", 76, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -92, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 14, cellInfo.getC1());
    assertEquals("Unexpected value of C2", -16, cellInfo.getC2());
    assertEquals("Unexpected value of TA", -1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", -1, cellInfo.getRxQual());
    assertNull("Unexpected value of PLMN", cellInfo.getPlmn());

    cellInfo = cellInfoList.get(3);
    assertEquals("Unexpected value of cell", "N3", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "25", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "295", Integer.toHexString(cellInfo.getCellId()));
    assertEquals("Unexpected value of ARFCN", 708, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -92, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 14, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 14, cellInfo.getC2());
    assertEquals("Unexpected value of TA", -1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", -1, cellInfo.getRxQual());
    assertNull("Unexpected value of PLMN", cellInfo.getPlmn());

    cellInfo = cellInfoList.get(4);
    assertEquals("Unexpected value of cell", "N4", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "4", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "65", Integer.toHexString(cellInfo.getCellId()));
    assertEquals("Unexpected value of ARFCN", 703, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -93, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 13, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 13, cellInfo.getC2());
    assertEquals("Unexpected value of TA", -1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", -1, cellInfo.getRxQual());
    assertNull("Unexpected value of PLMN", cellInfo.getPlmn());

    cellInfo = cellInfoList.get(5);
    assertEquals("Unexpected value of cell", "N5", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "21", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "8E", Integer.toHexString(cellInfo.getCellId()).toUpperCase());
    assertEquals("Unexpected value of ARFCN", 711, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -99, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 7, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 7, cellInfo.getC2());
    assertEquals("Unexpected value of TA", -1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", -1, cellInfo.getRxQual());
    assertNull("Unexpected value of PLMN", cellInfo.getPlmn());

    cellInfo = cellInfoList.get(6);
    assertEquals("Unexpected value of cell", "N6", cellInfo.getCell());
    assertEquals("Unexpected value of BSIC", "24", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cellId", "6A", Integer.toHexString(cellInfo.getCellId()).toUpperCase());
    assertEquals("Unexpected value of ARFCN", 82, cellInfo.getArfcn());
    assertEquals("Unexpected value of power", -103, cellInfo.getPower());
    assertEquals("Unexpected value of C1", 3, cellInfo.getC1());
    assertEquals("Unexpected value of C2", -27, cellInfo.getC2());
    assertEquals("Unexpected value of TA", -1, cellInfo.getTa());
    assertEquals("Unexpected value of rxQual", -1, cellInfo.getRxQual());
    assertNull("Unexpected value of PLMN", cellInfo.getPlmn());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhMONIExecSpecificParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#MONI\r",
        0,
        "\r\n#MONI: Cell  BSIC  LAC  CellId  ARFCN    Power  C1  C2  TA  RxQual  PLMN\r\n" +
            "#MONI:  S    72  07EA   0CA4     513   -75dbm  15  55   1     0    U Mobile\r\n" +
            "#MONI: N1    FF   2776   0227     700   -81dbm  25  25 \r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhMONIExecResult result = fakeATEngine.execute(new AThhMONIExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    assertEquals("Unexpected value of PLMN", "U Mobile", result.getCellList().get(0).getPlmn());
    assertEquals("Unexpected value of BSIC", 0, result.getCellList().get(1).getBsic());
  }

  @Test
  public void testAThhMONIExecNotOctalFormatParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#MONI\r",
        0,
        "\r\n#MONI: Cell  BSIC  LAC  CellId  ARFCN    Power  C1  C2  TA  RxQual  PLMN\r\n" +
            "#MONI:  S    B7  07EA   0CA4     513   -75dbm  15  55   1     0    U Mobile\r\n" +
            "#MONI: N1    96   2776   0227     700   -81dbm  25  25 \r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhMONIExecResult result = fakeATEngine.execute(new AThhMONIExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, result.getCellList().size());
    assertEquals(2, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
