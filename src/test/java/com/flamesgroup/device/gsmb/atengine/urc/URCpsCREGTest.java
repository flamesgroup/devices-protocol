/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class URCpsCREGTest {

  private final List<URCpsCREGHandlerValue> handlerValues = new ArrayList<>();

  private static class URCpsCREGHandlerValue {
    private final PpsCREG.Status status;
    private final String locationAreaCode;
    private final String cellID;

    public URCpsCREGHandlerValue(final PpsCREG.Status status, final String locationAreaCode, final String cellID) {
      this.status = status;
      this.locationAreaCode = locationAreaCode;
      this.cellID = cellID;
    }

    public PpsCREG.Status getStatus() {
      return status;
    }

    public String getLocationAreaCode() {
      return locationAreaCode;
    }

    public String getCellID() {
      return cellID;
    }
  }

  private class URCpsCREGHandler implements IURCpsCREGHandler {

    @Override
    public void handleNetworkRegistration(final PpsCREG.Status status, final String locationAreaCode, final String cellID) {
      handlerValues.add(new URCpsCREGHandlerValue(status, locationAreaCode, cellID));
    }

  }

  @Test
  public void testURCpsCREGParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CREG: 0\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CREG: 1,\"2776\",\"0228\"\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new URCpsCREG(new URCpsCREGHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of status", PpsCREG.Status.NOT_REGISTERED_NOT_SEARCHING, handlerValues.get(0).getStatus());

    assertEquals("Unexpected value of status", PpsCREG.Status.REGISTERED_TO_HOME_NETWORK, handlerValues.get(1).getStatus());
    assertEquals("Unexpected value of locationAreaCode", "2776", handlerValues.get(1).getLocationAreaCode());
    assertEquals("Unexpected value of cellID", "0228", handlerValues.get(1).getCellID());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
