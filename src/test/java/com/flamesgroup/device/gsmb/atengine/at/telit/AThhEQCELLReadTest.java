/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.Collections;

public class AThhEQCELLReadTest {

  @Test
  public void testAThhEQCELLReadNotEmptyListParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#EQCELL?\r",
        0,
        "\r\n#EQCELL: 1,30,50\r\n" +
            "#EQCELL: 1,50,-10\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    AThhEQCELLReadResult result = fakeATEngine.execute(new AThhEQCELLRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(2, result.getEqCellList().size());

    AThhEQCELLReadResult.EqCellInfo eqCellInfo;
    eqCellInfo = result.getEqCellList().get(0);
    assertEquals(30, eqCellInfo.getArfcn());
    assertEquals(50, eqCellInfo.getRxLevAdjust());

    eqCellInfo = result.getEqCellList().get(1);
    assertEquals(50, eqCellInfo.getArfcn());
    assertEquals(-10, eqCellInfo.getRxLevAdjust());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhEQCELLReadEmptyListParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#EQCELL?\r",
        0,
        "\r\n#EQCELL: 0\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    AThhEQCELLReadResult result = fakeATEngine.execute(new AThhEQCELLRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, result.getEqCellList().size());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
