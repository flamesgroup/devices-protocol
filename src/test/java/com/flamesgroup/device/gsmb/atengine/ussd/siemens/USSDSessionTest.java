/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.AT;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCUSD;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class USSDSessionTest {

  @Test(expected = IllegalStateException.class)
  public void testUSSDSessionTryToOpenAfterSessionOpened() throws ChannelException, InterruptedException {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "ATD*111#;\r",
        0,
        "ATD*111#;\r\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    USSDSession ussdSession = new USSDSession(fakeATEngine);
    URCpsCUSD cusd = new URCpsCUSD(ussdSession);
    fakeATEngine.start(cusd);

    ussdSession.open("*111#");
    ussdSession.open("*123#");

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testUSSDSessionUSSDTerminatedByNetwork() throws ChannelException, InterruptedException {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "ATD*111#;\r",
        0,
        "ATD*111#;\r\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 2,\"00420061006C0061006E0073002000" +
            "30002E0030003000680072006E002C00200062006F006E0075007" +
            "300200030002E0030003000680072006E002E\",15\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    USSDSession ussdSession = new USSDSession(fakeATEngine);
    URCpsCUSD cusd = new URCpsCUSD(ussdSession);
    fakeATEngine.start(cusd);

    ussdSession.open("*111#");
    String request = ussdSession.readRequest();
    assertEquals("Balans 0.00hrn, bonus 0.00hrn.", request);

    boolean exceptionHasBeenThrown = false;
    try {
      ussdSession.writeResponse("1");
    } catch (IllegalStateException ex) {
      exceptionHasBeenThrown = true;
    }
    assertTrue(exceptionHasBeenThrown);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testUSSDSessionFutureUserActionRequired() throws ChannelException, InterruptedException {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "ATD*123#;\r",
        0,
        "ATD*123#;\r\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 1,\"0031002E004F007000790073000A0032002E00" +
            "5000690064006B006C0069007500630068007900740079000A00" +
            "33002E005600690064006B006C00690075006300680079007400" +
            "79000A0034002E00560061007200740069007300740027000A00" +
            "30002E004E0061007A00610064000A\",15\r\n> "));
    iterations.add(new FakeATSubChannel.Iteration(
        "1" + AT.CTRL_Z,
        0,
        "1" + AT.CTRL_Z + "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 1,\"0049006E007400650072006E006500" +
            "74002000540079007A006800640065006E0027003A00200035003" +
            "00020004D00420020006D006F00620069006C006E006F0067006F" +
            "00200049006E007400650072006E0065007400750020006C00790" +
            "073006800650020007A006100200035002000670072006E002000" +
            "28007A0020005000440056002C002000620065007A00200050004" +
            "600290020006B006F007A0068006E00690020003700200064006E" +
            "00690076002E000A0030002E004E0061007A00610064\",15\r\n> "));
    iterations.add(new FakeATSubChannel.Iteration(
        "" + AT.ESC,
        0,
        "" + AT.ESC + "\r\n\r+CME ERROR: unknown\r\n\r\n+CME ERROR: ss not executed\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    USSDSession ussdSession = new USSDSession(fakeATEngine);
    URCpsCUSD cusd = new URCpsCUSD(ussdSession);
    fakeATEngine.start(cusd);

    ussdSession.open("*123#");
    String request = ussdSession.readRequest();
    assertEquals("1.Opys\n2.Pidkliuchyty\n3.Vidkliuchyty\n4.Vartist'\n0.Nazad\n", request);

    ussdSession.writeResponse("1");
    request = ussdSession.readRequest();
    assertEquals("Internet Tyzhden': 50 MB mobilnogo Internetu lyshe za 5 grn (z PDV, bez PF) kozhni 7 dniv.\n0.Nazad", request);

    ussdSession.executeCloseAction();
  }

}
