/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.SMSStatus;
import org.junit.Test;

import java.util.ArrayList;

public class ATctSMGRWriteTest {

  @Test
  public void testATctSMGRWriteParsing() throws Exception {
    FakeATSubChannel.Iteration iteration = new FakeATSubChannel.Iteration(
        "AT^SMGR=1\r",
        0,
        "\r\n^SMGR: 1,,30\r\n0791836003000080040C918360536260910000312191112004800CC8329BFD06DDDF72363904\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(iteration);
    fakeATEngine.start(new ArrayList<>());

    ATctSMGRWriteResult result = fakeATEngine.execute(new ATctSMGRWrite(1));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of status", SMSStatus.READ, result.getStatus());
    assertNull("Unexpected value of alpha", result.getAlpha());
    assertEquals("Unexpected value of length", 30, result.getLength());
    assertEquals("Unexpected value of PDU", "0791836003000080040C918360536260910000312191112004800CC8329BFD06DDDF72363904", result.getPDU());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
