/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATEmptyResult;
import org.junit.Test;

import java.util.ArrayList;

public class ATctSMSOExecTest {

  @Test
  public void testATctSMSOExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT^SMSO\r",
        0,
        "\r\n^SMSO: MS OFF\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATEmptyResult result = fakeATEngine.execute(new ATctSMSOExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertNotNull(result);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testATctSMSOExecWithWrongAnswerParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT^SMSO\r",
        0,
        "\r\n^SMSO: MS OFFF\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(new ATctSMSOExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);

    assertEquals("^SMSO: MS OFFF", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
