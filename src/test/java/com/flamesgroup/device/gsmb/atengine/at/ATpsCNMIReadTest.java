/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.BroadcastReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSDeliveringOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.SMSStatusReportingOption;
import com.flamesgroup.device.gsmb.atengine.param.PpsCNMI.UrcBufferingOption;
import org.junit.Test;

import java.util.ArrayList;

public class ATpsCNMIReadTest {

  @Test
  public void testATpsCNMIReadParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CNMI?\r",
        0,
        "\r\n+CNMI: 0,0,0,0,1\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCNMIReadResult result = fakeATEngine.execute(new ATpsCNMIRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of urcBufferingOption", UrcBufferingOption.BUFFER_URCS_IN_TA, result.getURCBufferingOption());
    assertEquals("Unexpected value of smsDeliveringOption", SMSDeliveringOption.NO_SMS_DELIVER_ROUTED_TO_TA, result.getSMSDeliveringOption());
    assertEquals("Unexpected value of broadcastReportingOption", BroadcastReportingOption.NO_CBM_INDICATIONS_ROUTED_TO_TE, result.getBroadcastReportingOption());
    assertEquals("Unexpected value of smsStatusReportingOption", SMSStatusReportingOption.NO_SMS_STATUS_REPORT_ROUTED_TO_TE, result.getSMSStatusReportingOption());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
