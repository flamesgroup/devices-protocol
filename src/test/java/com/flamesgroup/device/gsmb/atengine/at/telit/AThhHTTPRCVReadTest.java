/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.Collections;

public class AThhHTTPRCVReadTest {

  @Test
  public void testAThhHTTPRCVRead() throws Exception {
    String htmlSource = "<!doctype html>\r\n"
        + "<html>\r\n"
        + "<head>\r\n"
        + "    <title>Example Domain</title>\r\n"
        + "    <meta charset=\"utf-8\" />\r\n"
        + "    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\r\n"
        + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\r\n"
        + "    <style type=\"text/css\">\r\n"
        + "    body {\r\n"
        + "        background-color: #f0f0f2;\r\n"
        + "        margin: 0;\r\n"
        + "        padding: 0;\r\n"
        + "        font-family: \"Open Sans\", \"Helvetica Neue\", Helvetica, Arial, sans-serif;\r\n"
        + "    }\r\n"
        + "    div {\r\n"
        + "        width: 600px;\r\n"
        + "        margin: 5em auto;\r\n"
        + "        padding: 50px;\r\n"
        + "        background-color: #fff;\r\n"
        + "        border-radius: 1em;\r\n"
        + "    }\r\n"
        + "    a:link, a:visited {\r\n"
        + "        color: #38488f;\r\n"
        + "        text-decoration: none;\r\n"
        + "    }\r\n"
        + "    @media (max-width: 700px) {\r\n"
        + "        body {\r\n"
        + "            background-color: #fff;\r\n"
        + "        }\r\n"
        + "        div {\r\n"
        + "            width: auto;\r\n"
        + "            margin: 0 auto;\r\n"
        + "            border-radius: 0;\r\n"
        + "            padding: 1em;\r\n"
        + "        }\r\n"
        + "    }\r\n"
        + "    </style>\r\n"
        + "</head>\r\n"
        + "<body>\r\n"
        + "<div>\r\n"
        + "    <h1>Example Domain</h1>\r\n"
        + "    <p>This domain is established to be used for illustrative examples in documents. You may use this\r\n"
        + "    domain in examples without prior coordination or asking for permission.</p>\r\n"
        + "    <p><a href=\"http://www.iana.org/domains/example\">More information...</a></p>\r\n"
        + "</div>\r\n"
        + "</body>\r\n"
        + "</html>\r\n";
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#HTTPRCV=0\r",
        0,
        "\r\n<<<" + htmlSource +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    fakeATEngine.execute(new AThhHTTPRCVRead(0));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }


  @Test
  public void testAThhHTTPRCVReadFail() throws Exception {
    String htmlSource = "<!doctype html>\r\n"
        + "<html>\r\n"
        + "<head>\r\n"
        + "    <title>Example Domain</title>\r\n"
        + "    <meta charset=\"utf-8\" />\r\n"
        + "    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\r\n"
        + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\r\n"
        + "</head>\r\n"
        + "<body>\r\n"
        + "</body>\r\n"
        + "</html>\r\n";
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#HTTPRCV=0\r",
        0,
        "\r\n" + htmlSource +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    fakeATEngine.execute(new AThhHTTPRCVRead(0));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertNotEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhHTTPRCVReadWithSetMaxReadByte() throws Exception {
    String htmlSource = "<!doctype html>\r\n"
        + "<html>\r\n"
        + "<head>\r\n"
        + "    <title>Example Domain</title>\r\n"
        + "    <meta charset=\"utf-8\" />\r\n"
        + "    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\r\n"
        + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\r\n"
        + "</head>\r\n"
        + "<body>\r\n"
        + "</body>\r\n"
        + "</html>\r\n";
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#HTTPRCV=0,1000\r",
        0,
        "\r\n<<<" + htmlSource +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    fakeATEngine.execute(new AThhHTTPRCVRead(0, 1000));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhHTTPRCVReadCheckMaxMaxBytesValueInRange() throws Exception {
    new AThhHTTPRCVRead(0, 0);
    new AThhHTTPRCVRead(0, 64);
    new AThhHTTPRCVRead(0, 1500);
    new AThhHTTPRCVRead(0, 1000);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAThhHTTPRCVReadCheckMaxMaxBytesValueOutRangeLover() throws Exception {
    new AThhHTTPRCVRead(0, 63);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAThhHTTPRCVReadCheckMaxMaxBytesValueOutRangeBigger() throws Exception {
    new AThhHTTPRCVRead(0, 1501);
  }

}
