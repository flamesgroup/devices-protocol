/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsGMM.Model;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ATpsGMMExecTest {

  @Test
  public void testATpsGMMExecParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMM\r",
        0,
        "\r\nMC55i\r\n\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMM\r",
        0,
        "\r\nGL865-QUAD\r\n\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATpsGMMExecResult atpsGMMExecResultSiemens = fakeATEngine.execute(new ATpsGMMExec());
    ATpsGMMExecResult atpsGMMExecResultTelit = fakeATEngine.execute(new ATpsGMMExec());

    fakeATEngine.stop();

    fakeATEngine.checkForException();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
    assertEquals("Unexpected value of model", Model.MC55i, atpsGMMExecResultSiemens.getModel());
    assertEquals("Unexpected value of model", Model.GL865_QUAD, atpsGMMExecResultTelit.getModel());
  }

}
