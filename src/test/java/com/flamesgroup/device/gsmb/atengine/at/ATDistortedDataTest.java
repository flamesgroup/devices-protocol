/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCIEV.Indicator;
import com.flamesgroup.device.gsmb.atengine.param.PpsCLCC;
import com.flamesgroup.device.gsmb.atengine.urc.IURCpsCIEVHandler;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCIEV;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATDistortedDataTest {

  private final class URCpsCIEVValue {

    private final Indicator indicator;
    private final int value1;
    private final int value2;

    private URCpsCIEVValue(final Indicator indicator, final int value1, final int value2) {
      this.indicator = indicator;
      this.value1 = value1;
      this.value2 = value2;
    }

    public Indicator getIndicator() {
      return indicator;
    }

    public int getValue1() {
      return value1;
    }

    public int getValue2() {
      return value2;
    }

  }

  @Test
  public void testATDistortedDataParsing1() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CLCC\r",
        0,
        "\r\nAT+EV: rssi,2\r\n" +
            "\r\n+CLCC: 1,0,0,0,0,\"1310639357474024\",129,\"\"\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCLCCExecResult result = fakeATEngine.execute(new ATpsCLCCExec());
    assertEquals(1, result.getCallList().size());
    assertEquals(PpsCLCC.CallState.ACTIVE, result.getCallList().get(0).getState());
    assertEquals("1310639357474024", result.getCallList().get(0).getNumber());

    assertEquals(1, fakeATEngine.getUnprocessedATLexemes().size());
    assertEquals("AT+EV: rssi,2", fakeATEngine.getUnprocessedATLexemes().get(0));

    fakeATEngine.checkForException();
    fakeATEngine.stop();
  }

  @Test
  public void testATDistortedDataParsing2() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CLCC\r",
        0,
        "\r\nssi,2\r\n" +
            "\r\n+CLCC: 1,0,0,0,0,\"1310639357474024\",129,\"\"\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCLCCExecResult result = fakeATEngine.execute(new ATpsCLCCExec());
    assertEquals(1, result.getCallList().size());
    assertEquals(PpsCLCC.CallState.ACTIVE, result.getCallList().get(0).getState());
    assertEquals("1310639357474024", result.getCallList().get(0).getNumber());

    assertEquals(1, fakeATEngine.getUnprocessedATLexemes().size());
    assertEquals("ssi,2", fakeATEngine.getUnprocessedATLexemes().get(0));

    fakeATEngine.checkForException();
    fakeATEngine.stop();
  }

  @Test
  public void testATDistortedDataParsing3() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CLCC\r",
        0,
        "\r\n+CIEV: rssi,2\r\n" +
            "\r\n+CLCC: 1,0,0,0,0,\"1310639357474024\",129,\"\"\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);

    final List<URCpsCIEVValue> cievHandlerValues = new ArrayList<>();
    fakeATEngine.start(new URCpsCIEV(new IURCpsCIEVHandler() {

      @Override
      public void handleIndicatorChanges(final Indicator indicator, final int value1, final int value2) {
        cievHandlerValues.add(new URCpsCIEVValue(indicator, value1, value2));
      }

    }));

    ATpsCLCCExecResult result = fakeATEngine.execute(new ATpsCLCCExec());
    assertEquals(1, result.getCallList().size());
    assertEquals(PpsCLCC.CallState.ACTIVE, result.getCallList().get(0).getState());
    assertEquals("1310639357474024", result.getCallList().get(0).getNumber());

    assertTrue(fakeATEngine.getUnprocessedATLexemes().isEmpty());

    assertEquals(1, cievHandlerValues.size());
    assertEquals(Indicator.RSSI, cievHandlerValues.get(0).getIndicator());
    assertEquals(2, cievHandlerValues.get(0).getValue1());

    fakeATEngine.checkForException();
    fakeATEngine.stop();
  }

}
