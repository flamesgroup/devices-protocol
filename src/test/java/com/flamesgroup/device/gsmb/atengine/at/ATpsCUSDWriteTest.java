/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.URCPresentation;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ATpsCUSDWriteTest {

  @Test
  public void testATpsCUSDWriteParsingWithEcho() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CUSD=1,\"*111#\"\r",
        0,
        "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CUSD=1,\"*111#\",15\r",
        0,
        "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CUSD=2\r",
        0,
        "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(new ATpsCUSDWrite(URCPresentation.ENABLE, "*111#"));
    fakeATEngine.execute(new ATpsCUSDWrite(URCPresentation.ENABLE, "*111#", 15));
    fakeATEngine.execute(new ATpsCUSDWrite(URCPresentation.CANCEL_SESSION));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
