/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URCpsCDSITest {

  private final List<URCpsCDSIHandlerValue> handlerValues = new ArrayList<>();

  private class URCpsCDSIHandlerValue {

    private final String memoryStorage;
    private final int index;

    private URCpsCDSIHandlerValue(final String memoryStorage, final int index) {
      this.memoryStorage = memoryStorage;
      this.index = index;
    }

    public String getMemoryStorage() {
      return memoryStorage;
    }

    public int getIndex() {
      return index;
    }

  }

  private final class URCpsCDSIHandler implements IURCpsCDSIHandler {

    @Override
    public void handleSMSStatusReport(final String memoryStorage, final int index) {
      handlerValues.add(new URCpsCDSIHandlerValue(memoryStorage, index));
    }

  }

  @Test
  public void testURCpsCDSIParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CDSI: \"MT\",72\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCDSI(new URCpsCDSIHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of memoryStorage", "MT", handlerValues.get(0).getMemoryStorage());
    assertEquals("Unexpected value of index", 72, handlerValues.get(0).getIndex());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURCpsCDSIUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CDSI: \"ME\",72\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCDSI(new URCpsCDSIHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("+CDSI: \"ME\",72", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
