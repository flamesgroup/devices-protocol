/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.siemens;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD.Status;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCUSDBaseTest;
import com.flamesgroup.device.helper.UCS2Convertor;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class URCpsCUSDTest {

  @Test
  public void testURCpsCUSDParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 2,\"0059006F00750072002000630075007200720065006E0" +
            "074002000620061006C0061006E00630065002000690073002000" +
            "24002000320032002E00310030\",0\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 2\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 1,\"0031002E004F007000790073000A0032002E005000690" +
            "064006B006C0069007500630068007900740079000A0033002E00" +
            "5600690064006B006C0069007500630068007900740079000A003" +
            "4002E00560061007200740069007300740027000A0030002E004E" +
            "0061007A00610064000A\",15> \r\n"));
    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    URCpsCUSDBaseTest.URCpsCUSDHandler cusdHandler = new URCpsCUSDBaseTest.URCpsCUSDHandler();
    fakeATEngine.start(new URCpsCUSD(cusdHandler));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of status", Status.USSD_TERMINATED_BY_NETWORK, cusdHandler.getHandledUssd().get(0).getStatus());
    assertEquals("Unexpected value of message", "Your current balance is $ 22.10", UCS2Convertor.hexToString(cusdHandler.getHandledUssd().get(0).getMessage()));
    assertEquals("Unexpected value of dataCodingScheme", 0, cusdHandler.getHandledUssd().get(0).getDataCodingScheme());

    assertEquals("Unexpected value of status", Status.USSD_TERMINATED_BY_NETWORK, cusdHandler.getHandledUssd().get(1).getStatus());
    assertEquals("Unexpected value of message", null, UCS2Convertor.hexToString(cusdHandler.getHandledUssd().get(1).getMessage()));
    assertEquals("Unexpected value of dataCodingScheme", 0, cusdHandler.getHandledUssd().get(1).getDataCodingScheme());

    assertEquals("Unexpected value of status", Status.FURTHER_USER_ACTION_REQUIRED, cusdHandler.getHandledUssd().get(2).getStatus());
    assertEquals("Unexpected value of message", "1.Opys\n2.Pidkliuchyty\n3.Vidkliuchyty\n4.Vartist'\n0.Nazad\n", UCS2Convertor.hexToString(cusdHandler.getHandledUssd().get(2).getMessage()));
    assertEquals("Unexpected value of dataCodingScheme", 15, cusdHandler.getHandledUssd().get(2).getDataCodingScheme());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
