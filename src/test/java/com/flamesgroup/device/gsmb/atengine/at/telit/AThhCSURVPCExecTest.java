/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AThhCSURVPCExecTest {

  @Test
  public void testAThhCSURVPCExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#CSURVPC=25506\r",
        0,
        "\r\nNetwork survey started ...\r\n" +
            "697,17,-40,0.00,255,06,10104,552,0,3,533 687 704,20,64 69 71 73 79 512 520 522 524 525 532 535 549 689 694 697 700 702 706 710\r\n\r\n\r\n" +
            "Network survey ended\r\n\r\n\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhCSURBaseExecResult result = fakeATEngine.execute(new AThhCSURVPCExec(0x25506));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    List<AThhCSURBaseExecResult.BcchCarrierInfo> bcchCarrierInfos = result.getBcchCarrierInfos();
    List<AThhCSURBaseExecResult.NonBcchCarrierInfo> nonBcchCarrierInfos = result.getNonBcchCarrierInfos();

    assertEquals(1, bcchCarrierInfos.size());
    assertEquals(0, nonBcchCarrierInfos.size());

    AThhCSURBaseExecResult.BcchCarrierInfo bcchCarrierInfo = bcchCarrierInfos.get(0);
    assertEquals("Unexpected value of ARFCN", 697, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 17, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -40, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 0, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "6", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 10104, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 552, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(0), bcchCarrierInfo.getCellStatus());
    assertEquals("Unexpected value of validChannels", Arrays.asList(533, 687, 704), bcchCarrierInfo.getValidChannels());
    assertEquals("Unexpected value of neighbourChannels",
        Arrays.asList(64, 69, 71, 73, 79, 512, 520, 522, 524, 525, 532, 535, 549, 689, 694, 697, 700, 702, 706, 710),
        bcchCarrierInfo.getNeighbourChannels());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAThhCSURVPCExecNumberOfChannelsEqual0() {
    new AThhCSURVPCExec(Integer.MIN_VALUE);
  }

}
