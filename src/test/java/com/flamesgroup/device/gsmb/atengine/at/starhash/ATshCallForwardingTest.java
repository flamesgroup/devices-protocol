/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ATshCallForwardingTest {

  @Test
  public void testATCallForwardingTemplate() {
    assertTrue(ATshCallForwarding.isCallForwardingCode("*21#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("#21#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*#21#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("**21#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("##21#"));

    assertTrue(ATshCallForwarding.isCallForwardingCode("*21*#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*21**#"));

    assertTrue(ATshCallForwarding.isCallForwardingCode("*21**11#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*21*0934842999#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*21*+0934842999#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*21*0934842999*#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*21*0934842999*11#"));

    assertTrue(ATshCallForwarding.isCallForwardingCode("*61***#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*61***1000#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*61**11*1000#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*61*+0934842999#"));
    assertTrue(ATshCallForwarding.isCallForwardingCode("*61*0934842999*11*1000#"));

    assertFalse(ATshCallForwarding.isCallForwardingCode("*67***#"));
    assertFalse(ATshCallForwarding.isCallForwardingCode("*21*+#"));
    assertFalse(ATshCallForwarding.isCallForwardingCode("*61*+#"));
  }

}
