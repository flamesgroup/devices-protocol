/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URCpsCUSDBaseTest {

  public static class URCpsCUSDHandlerValue {

    private final PpsCUSD.Status status;
    private final String message;
    private final int dataCodingScheme;

    public URCpsCUSDHandlerValue(final PpsCUSD.Status status, final String message, final int dataCodingScheme) {
      this.status = status;
      this.message = message;
      this.dataCodingScheme = dataCodingScheme;
    }

    public PpsCUSD.Status getStatus() {
      return status;
    }

    public String getMessage() {
      return message;
    }

    public int getDataCodingScheme() {
      return dataCodingScheme;
    }

  }

  public static class URCpsCUSDHandler implements IURCpsCUSDHandler {

    private final List<URCpsCUSDHandlerValue> handledValues = new ArrayList<>();

    @Override
    public void handleUssd(final PpsCUSD.Status status, final String message, final int dataCodingScheme) {
      handledValues.add(new URCpsCUSDHandlerValue(status, message, dataCodingScheme));
    }

    public List<URCpsCUSDHandlerValue> getHandledUssd() {
      return handledValues;
    }
  }

  private static class URCpsCUSD extends com.flamesgroup.device.gsmb.atengine.urc.URCpsCUSD {

    private URCpsCUSD(final IURCpsCUSDHandler handler) {
      super(handler);
    }

  }

  @Test
  public void testURCpsCUSDBaseWithSpecialSymbolsParsing1() throws Exception {
    FakeATSubChannel.Iteration iteration = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 0,\"RM0: Your credit balance is below RM2. Top up " +
            "RM30 at 7 Eleven or any U Mobile dealers now and get a FREE Revive Isotonic dri" +
            "nk, valid until 07/10. TnCs apply.\r\",15\r\n");
    FakeATEngine fakeATEngine = new FakeATEngine(iteration);
    URCpsCUSDBaseTest.URCpsCUSDHandler cusdHandler = new URCpsCUSDBaseTest.URCpsCUSDHandler();
    fakeATEngine.start(new URCpsCUSD(cusdHandler));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of status", PpsCUSD.Status.NO_FURTHER_USER_ACTION_REQUIRED, cusdHandler.getHandledUssd().get(0).getStatus());
    assertEquals("Unexpected value of message",
        "RM0: Your credit balance is below RM2. Top up RM30 at 7 Eleven or any U Mobile dealers now and get a FREE Revive Isotonic drink, valid until 07/10. TnCs apply.\r",
        cusdHandler.getHandledUssd().get(0).getMessage());
    assertEquals("Unexpected value of dataCodingScheme", 15, cusdHandler.getHandledUssd().get(0).getDataCodingScheme());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURCpsCUSDBaseWithSpecialSymbolsParsing2() throws Exception {
    FakeATSubChannel.Iteration iteration = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 1,\"\n1. Please enter coupon number\n0. Back\n\r\",0\r\n");
    FakeATEngine fakeATEngine = new FakeATEngine(iteration);
    URCpsCUSDBaseTest.URCpsCUSDHandler cusdHandler = new URCpsCUSDBaseTest.URCpsCUSDHandler();
    fakeATEngine.start(new URCpsCUSD(cusdHandler));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of status", PpsCUSD.Status.FURTHER_USER_ACTION_REQUIRED, cusdHandler.getHandledUssd().get(0).getStatus());
    assertEquals("Unexpected value of message",
        "\n1. Please enter coupon number\n0. Back\n\r",
        cusdHandler.getHandledUssd().get(0).getMessage());
    assertEquals("Unexpected value of dataCodingScheme", 0, cusdHandler.getHandledUssd().get(0).getDataCodingScheme());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
