/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.at.ATErrorStatusException;
import com.flamesgroup.device.gsmb.atengine.at.EmptyResponseSolidAT;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ATEngineTest {

  @Test
  public void testExecute() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT\r",
        0,
        "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(Collections.emptyList());

    fakeATEngine.execute(new ATEchoExecForTest());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testExecuteError() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT\r",
        0,
        "\r\nERROR\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(Collections.emptyList());

    Exception exception = null;
    try {
      fakeATEngine.execute(new ATEchoExecForTest());
    } catch (Exception e) {
      exception = e;
    }

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    assertNotNull(exception);
    assertEquals(ATErrorStatusException.class, exception.getClass());
  }

  @Test
  public void testExecuteTimeout() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT\r",
        0,
        null));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(Collections.emptyList());

    Exception exception = null;
    try {
      fakeATEngine.execute(new ATEchoExecForTest());
    } catch (Exception e) {
      exception = e;
    }

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    assertNotNull(exception);
    assertEquals(ATEngineTimeoutException.class, exception.getClass());
  }

  public static final class ATEchoExecForTest extends EmptyResponseSolidAT {

    public ATEchoExecForTest() {
      super("AT\r", 100);
    }

  }

}
