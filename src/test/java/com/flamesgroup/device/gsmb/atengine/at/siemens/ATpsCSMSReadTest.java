/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.BroadcastTypeMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileOriginatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.PpsCSMS.MobileTerminatedMessages;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PpsCSMS.MessagingService;
import org.junit.Test;

import java.util.ArrayList;

public class ATpsCSMSReadTest {

  @Test
  public void testATpsCSMSReadParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CSMS?\r",
        0,
        "\r\n+CSMS: 1,1,1,1\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCSMSReadResult result = fakeATEngine.execute(new ATpsCSMSRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of messagingService", result.getMessagingService(), MessagingService.COMPATIBLE_WITH_GSM_07_05_PHASE_2_PLUS_VERSION);
    assertEquals("Unexpected value of mobileTerminatedMessages", result.getMobileTerminatedMessages(), MobileTerminatedMessages.TYPE_SUPPORTED);
    assertEquals("Unexpected value of mobileOriginatedMessages", result.getMobileOriginatedMessages(), MobileOriginatedMessages.TYPE_SUPPORTED);
    assertEquals("Unexpected value of broadcastTypeMessages", result.getBroadcastTypeMessages(), BroadcastTypeMessages.TYPE_SUPPORTED);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
