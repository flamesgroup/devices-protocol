/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.ussd;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCUSD;
import com.flamesgroup.device.gsmb.atengine.urc.URCpsCUSDBaseTest;
import com.flamesgroup.device.gsmb.atengine.urc.siemens.URCpsCUSD;
import org.junit.Test;

public class BaseUSSDTest {

  @Test
  public void testURCpsCUSDWithSpecificDCS() throws Exception {
    FakeATSubChannel.Iteration iteration = (new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CUSD: 2,\"0059006F007500720020003000370038003400370032" +
            "0032003100300037002000620061006C0061006E006300650" +
            "02000690073002000410046004E0020003400390036002E00" +
            "360030002C002000760061006C0069006400200075006E007" +
            "40069006C002000300031002F00300033002F003200300031" +
            "0035002E\",72\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iteration);

    final URCpsCUSDBaseTest.URCpsCUSDHandler cusdHandler = new URCpsCUSDBaseTest.URCpsCUSDHandler();
    fakeATEngine.start(new URCpsCUSD(new BaseUSSDSession(fakeATEngine) {

      @Override
      protected void executeOpenAction(final String ussdString) throws ChannelException, InterruptedException {
      }

      @Override
      protected void executeCloseAction() throws ChannelException, InterruptedException {
      }

      @Override
      protected void executeResponseAction(final String response) throws ChannelException, InterruptedException {
      }

      @Override
      protected void processUSSDHandler(final PpsCUSD.Status status, final String message, final int dataCodingScheme) {
        cusdHandler.handleUssd(status, message, dataCodingScheme);
      }
    }));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of status", PpsCUSD.Status.USSD_TERMINATED_BY_NETWORK, cusdHandler.getHandledUssd().get(0).getStatus());
    assertEquals("Unexpected value of message", "Your 0784722107 balance is AFN 496.60, valid until 01/03/2015.", cusdHandler.getHandledUssd().get(0).getMessage());
    assertEquals("Unexpected value of dataCodingScheme", 72, cusdHandler.getHandledUssd().get(0).getDataCodingScheme());
  }

}
