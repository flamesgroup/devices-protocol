/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.at.IAT;
import com.flamesgroup.device.gsmb.atengine.at.IATResult;
import com.flamesgroup.device.gsmb.atengine.urc.IURC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FakeATEngine implements IATEngine {

  private final FakeATSubChannel fakeATSubChannel;
  private final ATEngine atEngine;
  private final List<String> unprocessedATLexemes = new ArrayList<>();

  public FakeATEngine(final FakeATSubChannel.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeATEngine(final List<FakeATSubChannel.Iteration> iterations) {
    fakeATSubChannel = new FakeATSubChannel(iterations);
    atEngine = new ATEngine(fakeATSubChannel);
  }

  @Override
  public void start(final List<IURC> urcs) throws ChannelException, InterruptedException {
    atEngine.start(urcs, new IATLexemeProcessor() {
      @Override
      public boolean process(final String atLexeme) {
        unprocessedATLexemes.add(atLexeme);
        return false;
      }
    });
  }

  public void start(final IURC urc) throws ChannelException, InterruptedException {
    start(Arrays.asList(new IURC[] {urc}));
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    atEngine.stop();
  }

  @Override
  public <T extends IATResult> T execute(final IAT<T> command) throws ChannelException, InterruptedException {
    return atEngine.execute(command);
  }

  public List<String> getUnprocessedATLexemes() {
    return unprocessedATLexemes;
  }

  public void checkForException() throws Exception {
    fakeATSubChannel.checkForException();
  }

  public void waitForIterationNumber(final int n) throws Exception {
    fakeATSubChannel.waitForIterationNumber(n);
  }
}
