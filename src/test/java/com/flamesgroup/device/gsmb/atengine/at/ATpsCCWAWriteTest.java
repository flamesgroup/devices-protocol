/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCCWA.Class;
import com.flamesgroup.device.gsmb.atengine.param.PpsCCWA.Mode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class ATpsCCWAWriteTest {

  @Test
  public void testATpsCCWAWriteParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CCWA=,0,1\r",
        0,
        "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CCWA=1,1\r",
        0,
        "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CCWA=,,7\r",
        0,
        "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(new ATpsCCWAWrite(null, Mode.DISABLE_CALL_WAITING, EnumSet.of(Class.VOICE)));
    fakeATEngine.execute(new ATpsCCWAWrite(Boolean.TRUE, Mode.ENABLE_CALL_WAITING, null));
    fakeATEngine.execute(new ATpsCCWAWrite(null, null, EnumSet.of(Class.VOICE, Class.DATA, Class.FAX)));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testATpsCMEEWriteAssemblageWithWrongParams() {
    new ATpsCCWAWrite(Boolean.TRUE, Mode.QUERY_STATUS_OF_CALL_WAITING, null);
  }

}
