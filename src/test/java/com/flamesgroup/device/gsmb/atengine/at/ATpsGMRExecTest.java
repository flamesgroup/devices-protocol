/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATpsGMRExecTest {

  @Test
  public void testATpsGMRExecParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMR\r",
        0,
        "\r\n10.01.141\r\n\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMR\r",
        0,
        "\r\n10.01.142-B012\r\n\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMR\r",
        0,
        "\r\nREVISION 01.003\r\n\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATpsGMRExecResult atpsGMRExecResultTelit = fakeATEngine.execute(new ATpsGMRExec());
    ATpsGMRExecResult atpsGMRExecResultTelitTemporary = fakeATEngine.execute(new ATpsGMRExec());
    ATpsGMRExecResult atpsGMRExecResultSiemens = fakeATEngine.execute(new ATpsGMRExec());

    fakeATEngine.stop();

    fakeATEngine.checkForException();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
    assertEquals("Unexpected value of revision", "10.01.141", atpsGMRExecResultTelit.getRevision());
    assertEquals("Unexpected value of revision", "10.01.142-B012", atpsGMRExecResultTelitTemporary.getRevision());
    assertEquals("Unexpected value of revision", "01.003", atpsGMRExecResultSiemens.getRevision());
  }

}
