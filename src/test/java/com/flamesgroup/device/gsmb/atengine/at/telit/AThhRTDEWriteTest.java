/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;

public class AThhRTDEWriteTest {

  @Test
  public void testAThhCSURVPCExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#RTDE=0000000000000000000000000000000000000000000000000000000000000000\r",
        0,
        "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(new AThhRTDEWrite("0000000000000000000000000000000000000000000000000000000000000000"));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test(expected = NullPointerException.class)
  public void testAThhRTDEWriteNPE() {
    new AThhRTDEWrite(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAThhRTDEWriteIAE1() {
    new AThhRTDEWrite("");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAThhRTDEWriteIAE2() {
    new AThhRTDEWrite("1");
  }

}
