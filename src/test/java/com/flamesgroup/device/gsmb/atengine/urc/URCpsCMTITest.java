/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URCpsCMTITest {

  private final List<URCpsCMTIValue> handlerValues = new ArrayList<>();

  private class URCpsCMTIValue {

    private final String memoryStorage;
    private final int index;

    private URCpsCMTIValue(final String memoryStorage, final int index) {
      this.memoryStorage = memoryStorage;
      this.index = index;
    }

    public String getMemoryStorage() {
      return memoryStorage;
    }

    public int getIndex() {
      return index;
    }

  }

  private final class URCpsCMTIHandler implements IURCpsCMTIHandler {

    @Override
    public void handleSMS(final String memoryStorage, final int index) {
      handlerValues.add(new URCpsCMTIValue(memoryStorage, index));
    }

  }

  @Test
  public void testURCpsCMTIParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CMTI: \"MT\",2\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMTI(new URCpsCMTIHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of memoryStorage", "MT", handlerValues.get(0).getMemoryStorage());
    assertEquals("Unexpected value of index", 2, handlerValues.get(0).getIndex());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURCpsCMTIUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CMTI: \"ME\",2\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCMTI(new URCpsCMTIHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("+CMTI: \"ME\",2", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
