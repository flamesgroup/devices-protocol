/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG.Status;
import com.flamesgroup.device.gsmb.atengine.param.PpsCREG.URCPresentMode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATpsCREGReadTest {

  @Test
  public void testATpsCREGReadParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CREG?\r",
        0,
        "\r\n+CREG: 2,4,\"0145\",\"291A\"\r\n" +
            "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CREG?\r",
        0,
        "\r\n+CREG: 0,2\r\n" +
            "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATpsCREGReadResult result = fakeATEngine.execute(new ATpsCREGRead());
    assertEquals("Unexpected value of urcPresentationMode", URCPresentMode.ENABLE_CREG_URC_STAT_LAC_CI, result.getURCPresentationMode());
    assertEquals("Unexpected value of status", Status.UNKNOWN, result.getStat());
    assertEquals("Unexpected value of locationAreaCode", "0145", result.getLocationAreaCode());
    assertEquals("Unexpected value of cellID", "291A", result.getCellID());

    result = fakeATEngine.execute(new ATpsCREGRead());
    assertEquals("Unexpected value of urcPresentationMode", URCPresentMode.DISABLE_CREG_URC, result.getURCPresentationMode());
    assertEquals("Unexpected value of stat", Status.NOT_REGISTERED_AND_SEARCHING, result.getStat());
    assertNull("Unexpected value of locationAreaCode", result.getLocationAreaCode());
    assertNull("Unexpected value of cellID", result.getCellID());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
