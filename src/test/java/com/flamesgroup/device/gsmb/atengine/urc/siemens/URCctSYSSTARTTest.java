/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.siemens;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.urc.URCctSYSSTART;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class URCctSYSSTARTTest {

  @Test
  public void parsingURCctSYSSTART() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "8^SYSSTART\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\00^SYSSTART\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\00bbbb\00\00 P^SYSSTART\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "^SYSSTART\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "ÿ^SYSSTART\r\n"));

    final AtomicInteger sysstartAtomicCounter = new AtomicInteger();
    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new URCctSYSSTART(sysstartAtomicCounter::incrementAndGet));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of received ^SYSSTART count", 5, sysstartAtomicCounter.get());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
