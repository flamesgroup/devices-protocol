/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.unit.sms.SMSHelper;
import org.junit.Test;

public class URCpsCDSTest {

  private static final String PDU = "07918360030000700637038121F351903251745121519032517451210000";

  private Integer handledSMSStatusReportLength;
  private String handledSMSStatusReportPdu;

  private final class URCpsCDSHandler implements IURCpsCDSHandler {

    @Override
    public void handleSMSStatusReport(final int length, final String pdu) {
      handledSMSStatusReportLength = length;
      handledSMSStatusReportPdu = pdu;
    }

  }

  @Test
  public void testURCpsCDSParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CDS: 22\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCDS(new URCpsCDSHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU", PDU, handledSMSStatusReportPdu);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURCpsCDSUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CDSI: 22\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCDS(new URCpsCDSHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("+CDSI: 22", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

  @Test
  public void testURCpsCDSCheckSizeOfPduUserData() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(null, 0, "\r\n+CDS: 22\r\n" + PDU + "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCDS(new URCpsCDSHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of PDU length", SMSHelper.getPduSize(PDU), (int) handledSMSStatusReportLength);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
