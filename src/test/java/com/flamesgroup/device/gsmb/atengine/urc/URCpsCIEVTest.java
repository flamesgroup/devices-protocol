/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCIEV.Indicator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URCpsCIEVTest {

  List<URCpsCIEVValue> handlerValues = new ArrayList<>();

  private class URCpsCIEVValue {

    private final Indicator indicator;
    private final int value1;
    private final int value2;

    private URCpsCIEVValue(final Indicator indicator, final int value1, final int value2) {
      this.indicator = indicator;
      this.value1 = value1;
      this.value2 = value2;
    }

    public Indicator getIndicator() {
      return indicator;
    }

    public int getValue1() {
      return value1;
    }

    public int getValue2() {
      return value2;
    }

  }

  private final class URCpsCIEVHandler implements IURCpsCIEVHandler {

    @Override
    public void handleIndicatorChanges(final Indicator indicator, final int value1, final int value2) {
      handlerValues.add(new URCpsCIEVValue(indicator, value1, value2));
    }
  }

  @Test
  public void testURCpsCIEVParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CIEV: signal,99\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CIEV: rssi,3\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new URCpsCIEV(new URCpsCIEVHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of indicator", Indicator.SIGNAL, handlerValues.get(0).getIndicator());
    assertEquals("Unexpected value of value1", 99, handlerValues.get(0).getValue1());
    assertEquals("Unexpected value of value2", -1, handlerValues.get(0).getValue2());

    assertEquals("Unexpected value of indicator", Indicator.RSSI, handlerValues.get(1).getIndicator());
    assertEquals("Unexpected value of value1", 3, handlerValues.get(1).getValue1());
    assertEquals("Unexpected value of value2", -1, handlerValues.get(1).getValue2());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURCpsCIEVUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n+CIEV: ssi,3\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCpsCIEV(new URCpsCIEVHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("+CIEV: ssi,3", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
