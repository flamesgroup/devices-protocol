/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class URChhRSENTest {

  private final List<Boolean> handlerValues = new ArrayList<>();

  private class URChhRSENHandler implements IURChhRSENHandler {

    @Override
    public void handleRemoteSIMEnable(final boolean connected) {
      handlerValues.add(connected);
    }

  }

  @Test
  public void testURChhRSENSuccessfulParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#RSEN: 1\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#RSEN: 0\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new URChhRSEN(new URChhRSENHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue("Should be connected", handlerValues.get(0));
    assertFalse("Should be disconnected", handlerValues.get(1));

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURChhRSENUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#RSEN: 2\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhRSEN(new URChhRSENHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("#RSEN: 2", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
