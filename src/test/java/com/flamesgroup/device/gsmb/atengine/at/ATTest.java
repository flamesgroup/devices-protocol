/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.ATEngineTimeoutException;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class ATTest {

  private FakeAT at;

  @Before
  public void setUP() {
    at = new FakeAT();
  }

  @Test
  public void testATParsingOKStatus() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    IATResult result = fakeATEngine.execute(at);
    assertThat(result, instanceOf(FakeATResult.class));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testATParsingEchoOKStatus() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        at.getCommandATData() + "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    IATResult result = fakeATEngine.execute(at);
    assertThat(result, instanceOf(FakeATResult.class));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test(expected = ATErrorStatusException.class)
  public void testATParsingErrorStatus() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\nERROR\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(at);
    try {
      fakeATEngine.checkForException();
    } finally {
      fakeATEngine.stop();
    }
  }

  @Test(expected = ATErrorStatusException.class)
  public void testATParsingCMEErrorStatus() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\n+CME ERROR: unknown\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(at);
    try {
      fakeATEngine.checkForException();
    } finally {
      fakeATEngine.stop();
    }
  }

  @Test(expected = ATErrorStatusException.class)
  public void testATParsingCMSErrorStatus() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\n+CMS ERROR: unknown\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(at);
    try {
      fakeATEngine.checkForException();
    } finally {
      fakeATEngine.stop();
    }
  }

  @Test(expected = ATEngineTimeoutException.class)
  public void testATParsingTimeoutResponse() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    try {
      fakeATEngine.execute(at);
    } finally {
      fakeATEngine.stop();
    }
  }

  @Test(expected = ATEngineTimeoutException.class)
  public void testATParsingInvalidResponse() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\nAOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(at);
    try {
      fakeATEngine.checkForException();
    } finally {
      fakeATEngine.stop();
    }
  }

  @Test(expected = IllegalStateException.class)
  public void testATDoubleExecution() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        at.getCommandATData(),
        0,
        "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(at);
    fakeATEngine.checkForException();
    try {
      fakeATEngine.execute(at);
    } finally {
      fakeATEngine.stop();
    }
  }

  private class FakeAT extends AT<FakeATResult> {

    private FakeAT() {
      super(200);
    }

    @Override
    protected boolean internalProcessATResponse(final String lexeme) {
      throw new IllegalStateException();
    }

    @Override
    protected String getCommandATData() {
      return "AT\r";
    }

    @Override
    protected FakeATResult getCommandATResult() {
      return new FakeATResult();
    }

  }

  private class FakeATResult implements IATResult {

  }

}
