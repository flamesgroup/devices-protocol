/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ServiceClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class ATshCallBarringTest {

  @Test
  public void testATCallWaiting() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "ATD*#33#\r",
        0,
        "\r\n+CLCK: 0,1\r\n" +
            "\r\n+CLCK: 0,2\r\n" +
            "\r\n+CLCK: 0,4\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATshCallBarringResult result = fakeATEngine.execute(new ATshCallBarring("*#33#"));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    List<ATshCallBarringResult.ATCallBarringInfo> callBarringInfos = result.getCallBarringInfoList();

    assertEquals("Unexpected value of size callBarringInfos", 3, callBarringInfos.size());

    assertFalse("Unexpected value of active", callBarringInfos.get(0).isActive());
    assertEquals("Unexpected value of service class", EnumSet.of(ServiceClass.VOICE), callBarringInfos.get(0).getServiceClasses());

    assertFalse("Unexpected value of active", callBarringInfos.get(1).isActive());
    assertEquals("Unexpected value of service class", EnumSet.of(ServiceClass.DATA), callBarringInfos.get(1).getServiceClasses());

    assertFalse("Unexpected value of active", callBarringInfos.get(2).isActive());
    assertEquals("Unexpected value of service class", EnumSet.of(ServiceClass.FAX), callBarringInfos.get(2).getServiceClasses());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testATCallWaitingWithCombinationServiceClasses() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "ATD*#35#\r",
        0,
        "\r\n+CLCK: 1,7\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATshCallBarringResult result = fakeATEngine.execute(new ATshCallBarring("*#35#"));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    List<ATshCallBarringResult.ATCallBarringInfo> callBarringInfos = result.getCallBarringInfoList();

    assertEquals("Unexpected value of size callBarringInfos", 1, callBarringInfos.size());

    assertTrue("Unexpected value of active", callBarringInfos.get(0).isActive());
    assertEquals("Unexpected value of service classes", EnumSet.of(ServiceClass.VOICE, ServiceClass.DATA, ServiceClass.FAX),
        callBarringInfos.get(0).getServiceClasses());
  }

}
