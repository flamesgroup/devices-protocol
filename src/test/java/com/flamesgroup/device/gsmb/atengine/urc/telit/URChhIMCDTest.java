/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhIMCDEN.Status;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URChhIMCDTest {

  private final List<Status> handlerValues = new ArrayList<>();

  private class URChhIMCDHandler implements IURChhIMCDHandler {

    @Override
    public void handleStatus(final Status status) {
      handlerValues.add(status);
    }

  }

  @Test
  public void testURChhIMCDParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#IMCD: 1\r\n"); // IMSI_CATCHER_DETECTED

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhIMCD(new URChhIMCDHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(Status.IMSI_CATCHER_DETECTED, handlerValues.get(0));

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURChhIMCDParsingFail() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#IMCD: 5\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhIMCD(new URChhIMCDHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("#IMCD: 5", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
