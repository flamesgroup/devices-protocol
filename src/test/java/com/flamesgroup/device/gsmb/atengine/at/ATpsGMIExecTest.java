/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsGMI.Manufacturer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ATpsGMIExecTest {

  @Test
  public void testATpsGMIExecParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMI\r",
        0,
        "\r\nSIEMENS\r\n\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+GMI\r",
        0,
        "\r\nTelit\r\n\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATpsGMIExecResult atpsCMIExecResultSiemens = fakeATEngine.execute(new ATpsGMIExec());
    ATpsGMIExecResult atpsCMIExecResultTelit = fakeATEngine.execute(new ATpsGMIExec());

    fakeATEngine.stop();

    fakeATEngine.checkForException();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    assertEquals("Unexpected value of model", Manufacturer.SIEMENS, atpsCMIExecResultSiemens.getManufacturer());
    assertEquals("Unexpected value of model", Manufacturer.TELIT, atpsCMIExecResultTelit.getManufacturer());
  }

}
