/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AThhCSURVCExecTest {

  @Test
  public void testAThhCSURVCExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#CSURVC\r",
        0,
        "\r\nNetwork survey started ...\r\n" +
            "697,17,-40,0.00,255,06,10104,552,0,3,533 687 704,20,64 69 71 73 79 512 520 522 524 525 532 535 549 689 694 697 700 702 706 710\r\n\r\n\r\n" +
            "698,-58\r\n\r\n\r\n" +
            "569,15,-92,19.06,255,03,30179,8262,0,0,7,28 30 35 44 92 93 98\r\n\r\n\r\n" +
            "44,16,-60,0.00,255,03,30179,31,0,3,26 56 101\r\n\r\n\r\n" +
            "512,62,-64,0.00,255,06,10104,551,0,3,684 874 876 \r\n\r\n\r\n" +
            "63,8,-92,2.09,255,06,10102,746,0,2,69 84,0\r\n\r\n\r\n" +
            "770,-108\r\n\r\n\r\n" +
            "Network survey ended\r\n\r\n\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhCSURBaseExecResult result = fakeATEngine.execute(new AThhCSURVCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    List<AThhCSURBaseExecResult.BcchCarrierInfo> bcchCarrierInfos = result.getBcchCarrierInfos();
    List<AThhCSURBaseExecResult.NonBcchCarrierInfo> nonBcchCarrierInfos = result.getNonBcchCarrierInfos();

    assertEquals(5, bcchCarrierInfos.size());
    assertEquals(2, nonBcchCarrierInfos.size());

    AThhCSURBaseExecResult.BcchCarrierInfo bcchCarrierInfo = bcchCarrierInfos.get(0);
    assertEquals("Unexpected value of ARFCN", 697, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 17, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -40, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 0, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "6", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 10104, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 552, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(0), bcchCarrierInfo.getCellStatus());
    assertEquals("Unexpected value of validChannels", Arrays.asList(533, 687, 704), bcchCarrierInfo.getValidChannels());
    assertEquals("Unexpected value of neighbourChannels",
        Arrays.asList(64, 69, 71, 73, 79, 512, 520, 522, 524, 525, 532, 535, 549, 689, 694, 697, 700, 702, 706, 710),
        bcchCarrierInfo.getNeighbourChannels());

    bcchCarrierInfo = bcchCarrierInfos.get(1);
    assertEquals("Unexpected value of ARFCN", 569, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 15, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -92, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 19.06, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "3", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 30179, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 8262, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(0), bcchCarrierInfo.getCellStatus());
    assertNull("Unexpected value of validChannels", bcchCarrierInfo.getValidChannels());
    assertEquals("Unexpected value of neighbourChannels", Arrays.asList(28, 30, 35, 44, 92, 93, 98), bcchCarrierInfo.getNeighbourChannels());

    bcchCarrierInfo = bcchCarrierInfos.get(2);
    assertEquals("Unexpected value of ARFCN", 44, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 16, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -60, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 0, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "3", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 30179, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 31, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(0), bcchCarrierInfo.getCellStatus());
    assertEquals("Unexpected value of validChannels", Arrays.asList(26, 56, 101), bcchCarrierInfo.getValidChannels());
    assertNull("Unexpected value of neighbourChannels", bcchCarrierInfo.getNeighbourChannels());

    bcchCarrierInfo = bcchCarrierInfos.get(3);
    assertEquals("Unexpected value of ARFCN", 512, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 62, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -64, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 0, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "6", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 10104, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 551, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(0), bcchCarrierInfo.getCellStatus());
    assertEquals("Unexpected value of validChannels", Arrays.asList(684, 874, 876), bcchCarrierInfo.getValidChannels());
    assertNull("Unexpected value of neighbourChannels", bcchCarrierInfo.getNeighbourChannels());

    bcchCarrierInfo = bcchCarrierInfos.get(4);
    assertEquals("Unexpected value of ARFCN", 63, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 8, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -92, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 2.09, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "6", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 10102, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 746, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(0), bcchCarrierInfo.getCellStatus());
    assertEquals("Unexpected value of validChannels", Arrays.asList(69, 84), bcchCarrierInfo.getValidChannels());
    assertNull("Unexpected value of neighbourChannels", bcchCarrierInfo.getNeighbourChannels());

    AThhCSURBaseExecResult.NonBcchCarrierInfo nonBcchCarrierInfo = nonBcchCarrierInfos.get(0);
    assertEquals("Unexpected value of ARFCN", 698, nonBcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of rxLev", -58, nonBcchCarrierInfo.getRxLev());

    nonBcchCarrierInfo = nonBcchCarrierInfos.get(1);
    assertEquals("Unexpected value of ARFCN", 770, nonBcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of rxLev", -108, nonBcchCarrierInfo.getRxLev());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhCSURVCExecWithParamParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#CSURVC=100,200\r",
        0,
        "\r\nNetwork survey started ...\r\n" +
            "102,-58\r\n\r\n\r\n" +
            "Network survey ended\r\n\r\n\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhCSURBaseExecResult result = fakeATEngine.execute(new AThhCSURVCExec(100, 200));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    List<AThhCSURBaseExecResult.BcchCarrierInfo> bcchCarrierInfos = result.getBcchCarrierInfos();
    List<AThhCSURBaseExecResult.NonBcchCarrierInfo> nonBcchCarrierInfos = result.getNonBcchCarrierInfos();

    assertEquals(0, bcchCarrierInfos.size());
    assertEquals(1, nonBcchCarrierInfos.size());

    AThhCSURBaseExecResult.NonBcchCarrierInfo nonBcchCarrierInfo = nonBcchCarrierInfos.get(0);
    assertEquals("Unexpected value of ARFCN", 102, nonBcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of rxLev", -58, nonBcchCarrierInfo.getRxLev());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhCSURVCExecWithNullOfArfcnn() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#CSURVC\r",
        0,
        "\r\nNetwork survey started ...\r\n" +
            "99,31,-99,12.10,255,06,10932,4305,5,0,1,99\r\n\r\n\r\n" +
            "111,131,-99,12.10,255,06,10932,4305,5,0\r\n\r\n\r\n" +
            "Network survey ended\r\n\r\n\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhCSURBaseExecResult result = fakeATEngine.execute(new AThhCSURVCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    List<AThhCSURBaseExecResult.BcchCarrierInfo> bcchCarrierInfos = result.getBcchCarrierInfos();
    List<AThhCSURBaseExecResult.NonBcchCarrierInfo> nonBcchCarrierInfos = result.getNonBcchCarrierInfos();

    assertEquals(2, bcchCarrierInfos.size());
    assertEquals(0, nonBcchCarrierInfos.size());

    AThhCSURBaseExecResult.BcchCarrierInfo bcchCarrierInfo = bcchCarrierInfos.get(0);
    assertEquals("Unexpected value of ARFCN", 99, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 31, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -99, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 12.10, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "6", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 10932, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 4305, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(5), bcchCarrierInfo.getCellStatus());
    assertNull("Unexpected value of validChannels", bcchCarrierInfo.getValidChannels());
    assertEquals("Unexpected value of neighbourChannels", Collections.singletonList(99), bcchCarrierInfo.getNeighbourChannels());

    bcchCarrierInfo = bcchCarrierInfos.get(1);
    assertEquals("Unexpected value of ARFCN", 111, bcchCarrierInfo.getArfcn());
    assertEquals("Unexpected value of BSIC", 131, bcchCarrierInfo.getBsic());
    assertEquals("Unexpected value of rxLev", -99, bcchCarrierInfo.getRxLev());
    assertEquals("Unexpected value of BER", 12.10, bcchCarrierInfo.getBer(), 2);
    assertEquals("Unexpected value of MCC", "255", Integer.toHexString(bcchCarrierInfo.getMcc()));
    assertEquals("Unexpected value of MNC", "6", Integer.toHexString(bcchCarrierInfo.getMnc()));
    assertEquals("Unexpected value of LAC", 10932, bcchCarrierInfo.getLac());
    assertEquals("Unexpected value of cellId", 4305, bcchCarrierInfo.getCellId());
    assertEquals("Unexpected value of cellStatus", AThhCSURBaseExecResult.CellStatus.getStatusByValue(5), bcchCarrierInfo.getCellStatus());
    assertNull("Unexpected value of validChannels", bcchCarrierInfo.getValidChannels());
    assertNull("Unexpected value of neighbourChannels", bcchCarrierInfo.getNeighbourChannels());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
