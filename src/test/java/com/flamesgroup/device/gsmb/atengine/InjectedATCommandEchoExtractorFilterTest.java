/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class InjectedATCommandEchoExtractorFilterTest {

  private List<String> handledATLexemes;
  private IATLexemeHandler atLexemeHandler;
  private ATLexemeParser atLexemeParser;

  @Before
  public void setUp() {
    handledATLexemes = new ArrayList<>();
    atLexemeHandler = new IATLexemeHandler() {
      @Override
      public void handleATLexeme(final String atLexeme) {
        handledATLexemes.add(atLexeme);
      }
    };
    atLexemeParser = new ATLexemeParser(new InjectedATCommandEchoExtractorFilter(atLexemeHandler));
  }

  @Test
  public void testParseURCWithInjectedATCommandEcho() {
    CharSequence responseData = "\r\n+CUSD: 0,\"0052004D0030003A00200059006F007500720020006300"
        + "720065006400690074002000620061006C0061006E00630065002000690073002000620065006C0"
        + "06F007700200052004D0032002E00200054006F007000200075007000200052004D003300300020"
        + "006100740020003700200045006C006500760065006E0020006F007200200061006E00790020005"
        + "50020004D006F00620069006C00650020006400650061006C0065007200730020006E006F007700"
        + "200061006E0064002000670065AT+CLCC\r00740020006100200046005200450045002000520065"
        + "0076006900760065002000490073006F0074006F006E006900630020006400720069006E006B002"
        + "C002000760061006C0069006400200075006E00740069006C002000300037002F00310030002E00"
        + "200054006E004300730020006100700070006C0079002E\",15\r\n\r\nOK\r\n";

    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"AT+CLCC\r",
            "+CUSD: 0,\"0052004D0030003A00200059006F007500720020006300"
                + "720065006400690074002000620061006C0061006E00630065002000690073002000620065006C0"
                + "06F007700200052004D0032002E00200054006F007000200075007000200052004D003300300020"
                + "006100740020003700200045006C006500760065006E0020006F007200200061006E00790020005"
                + "50020004D006F00620069006C00650020006400650061006C0065007200730020006E006F007700"
                + "200061006E006400200067006500740020006100200046005200450045002000520065"
                + "0076006900760065002000490073006F0074006F006E006900630020006400720069006E006B002"
                + "C002000760061006C0069006400200075006E00740069006C002000300037002F00310030002E00"
                + "200054006E004300730020006100700070006C0079002E\",15",
            "OK"}
        , handledATLexemes.toArray());
    handledATLexemes.clear();
  }

}
