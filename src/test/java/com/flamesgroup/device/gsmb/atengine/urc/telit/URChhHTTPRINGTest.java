/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicReference;

public class URChhHTTPRINGTest {

  private final AtomicReference<URChhHTTPRINGHandlerValue> urChhHTTPRINGHandlerValue = new AtomicReference<>();

  private static class URChhHTTPRINGHandlerValue {

    private final int profId;
    private final int httpStatusCode;
    private final String contentType;
    private final int dataSize;

    private URChhHTTPRINGHandlerValue(final int profId, final int httpStatusCode, final String contentType, final int dataSize) {
      this.profId = profId;
      this.httpStatusCode = httpStatusCode;
      this.contentType = contentType;
      this.dataSize = dataSize;
    }

    private int getProfId() {
      return profId;
    }

    private int getHttpStatusCode() {
      return httpStatusCode;
    }

    private String getContentType() {
      return contentType;
    }

    private int getDataSize() {
      return dataSize;
    }

  }

  private class URChhHTTPRINGHandler implements IURChhHTTPRINGHandler {

    @Override
    public void handleHttpServerAnswer(final int profId, final int httpStatusCode, final String contentType, final int dataSize) {
      urChhHTTPRINGHandlerValue.set(new URChhHTTPRINGHandlerValue(profId, httpStatusCode, contentType, dataSize));
    }

  }

  @Test
  public void testURChhHTTPRINGSuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#HTTPRING: 0,200,\"text/html\",1270\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhHTTPRING(new URChhHTTPRINGHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("unexpected value of profId", 0, urChhHTTPRINGHandlerValue.get().getProfId());
    assertEquals("unexpected value of httpStatusCode", 200, urChhHTTPRINGHandlerValue.get().getHttpStatusCode());
    assertEquals("unexpected value of contentType ", "text/html", urChhHTTPRINGHandlerValue.get().getContentType());
    assertEquals("unexpected value of dataSize", 1270, urChhHTTPRINGHandlerValue.get().getDataSize());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURChhHTTPRINGSuccessfulEmptyContentTypeParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#HTTPRING: 0,302,\"\",0\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhHTTPRING(new URChhHTTPRINGHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("unexpected value of profId", 0, urChhHTTPRINGHandlerValue.get().getProfId());
    assertEquals("unexpected value of httpStatusCode", 302, urChhHTTPRINGHandlerValue.get().getHttpStatusCode());
    assertEquals("unexpected value of contentType ", "", urChhHTTPRINGHandlerValue.get().getContentType());
    assertEquals("unexpected value of dataSize", 0, urChhHTTPRINGHandlerValue.get().getDataSize());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURChhHTTPRINGUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#HTTPRING: 0,200,\"text/html\"\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhHTTPRING(new URChhHTTPRINGHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("#HTTPRING: 0,200,\"text/html\"", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
