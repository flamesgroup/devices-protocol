/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.starhash.siemens;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.starhash.ATshIMEIResult;
import org.junit.Test;

import java.util.ArrayList;

public class ATshIMEITest {

  @Test
  public void testATQueryIMEI() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "ATD*#06#;\r",
        0,
        "ATD*#06#;\r\r\n353331969598491\r\n\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATshIMEIResult result = fakeATEngine.execute(new ATshIMEI());
    assertEquals("Unexpected value of IMEI", "353331969598491", result.getIMEI());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
