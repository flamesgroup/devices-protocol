/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSMGO.SMSOverflowStatus;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class URCctSMGOTest {

  private final List<SMSOverflowStatus> handlerValues = new ArrayList<>();

  private class URCctSMGOHandler implements IURCctSMGOHandler {

    @Override
    public void handleSMSOverflow(final SMSOverflowStatus status) {
      handlerValues.add(status);
    }

  }

  @Test
  public void testURCctSMGOParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n^SMGO: 2\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCctSMGO(new URCctSMGOHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of smsOverflowStatus", SMSOverflowStatus.SMS_BUFFER_FULL_AND_NEW_MESSAGE_WAIT_IN_SC, handlerValues.get(0));

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURCctSMGOParsingError() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n^SMGO: 3\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URCctSMGO(new URCctSMGOHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("^SMGO: 3", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
