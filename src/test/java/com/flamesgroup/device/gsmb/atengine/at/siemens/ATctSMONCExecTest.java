/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.siemens.ATctSMONCExecResult.CellInfo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATctSMONCExecTest {

  @Test
  public void testATctSMONCExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT^SMONC\r",
        0,
        "\r\n^SMONC: 255,06,2776,0228,20,698,29,26,26," +
            "255,06,2776,0227,21,700,22,19,19," +
            "255,06,2776,0295,25,708,14,11,11," +
            "255,06,2776,0000,21,711,12,9,9," +
            "255,06,2776,0065,04,703,8,5,5," +
            "255,06,0000,0000,24,706,7,-,-," +
            "000,000,0000,0000,00,0,0,-,-\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATctSMONCExecResult result = fakeATEngine.execute(new ATctSMONCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(7, result.getCellList().size());

    List<CellInfo> cellInfoList = result.getCellList();
    CellInfo cellInfo = cellInfoList.get(0);
    assertEquals("Unexpected value of MCC", 255, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 6, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "228", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "20", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 698, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 29, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", 26, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 26, cellInfo.getC2());

    cellInfo = cellInfoList.get(1);
    assertEquals("Unexpected value of MCC", 255, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 6, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "227", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "21", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 700, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 22, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", 19, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 19, cellInfo.getC2());

    cellInfo = cellInfoList.get(2);
    assertEquals("Unexpected value of MCC", 255, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 6, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "295", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "25", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 708, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 14, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", 11, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 11, cellInfo.getC2());

    cellInfo = cellInfoList.get(3);
    assertEquals("Unexpected value of MCC", 255, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 6, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "0", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "21", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 711, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 12, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", 9, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 9, cellInfo.getC2());

    cellInfo = cellInfoList.get(4);
    assertEquals("Unexpected value of MCC", 255, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 6, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "2776", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "65", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "4", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 703, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 8, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", 5, cellInfo.getC1());
    assertEquals("Unexpected value of C2", 5, cellInfo.getC2());

    cellInfo = cellInfoList.get(5);
    assertEquals("Unexpected value of MCC", 255, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 6, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "0", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "0", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "24", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 706, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 7, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", -1, cellInfo.getC1());
    assertEquals("Unexpected value of C2", -1, cellInfo.getC2());

    cellInfo = cellInfoList.get(6);
    assertEquals("Unexpected value of MCC", 0, cellInfo.getMcc());
    assertEquals("Unexpected value of MNC", 0, cellInfo.getMnc());
    assertEquals("Unexpected value of LAC", "0", Integer.toHexString(cellInfo.getLac()));
    assertEquals("Unexpected value of cell", "0", Integer.toHexString(cellInfo.getCell()));
    assertEquals("Unexpected value of BSIC", "0", Integer.toOctalString(cellInfo.getBsic()));
    assertEquals("Unexpected value of channel", 0, cellInfo.getChann());
    assertEquals("Unexpected value of RSSI", 0, cellInfo.getRssi());
    assertEquals("Unexpected value of C1", -1, cellInfo.getC1());
    assertEquals("Unexpected value of C2", -1, cellInfo.getC2());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testATctSMONCExecNotOctalFormatParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT^SMONC\r",
        0,
        "\r\n^SMONC: 255,06,2776,0228,B7,698,29,26,26," +
            "255,06,2776,0227,96,700,22,19,19\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATctSMONCExecResult result = fakeATEngine.execute(new ATctSMONCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, result.getCellList().size());
    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
