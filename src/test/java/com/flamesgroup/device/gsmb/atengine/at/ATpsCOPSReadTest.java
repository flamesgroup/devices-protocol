/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS;
import com.flamesgroup.device.gsmb.atengine.param.PpsCOPS.Mode;
import org.junit.Test;

import java.util.ArrayList;

public class ATpsCOPSReadTest {

  @Test
  public void testATpsCOPSReadParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+COPS?\r",
        0,
        "\r\n+COPS: 0,0,\"life:)\"\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCOPSReadResult result = fakeATEngine.execute(new ATpsCOPSRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of mode", Mode.AUTOMATIC, result.getMode());
    assertEquals("Unexpected value of format", PpsCOPS.Format.LONG_ALPHANUMERIC, result.getFormat());
    assertEquals("Unexpected value of operator", "life:)", result.getOperator());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testATpsCOPSReadWithEmptyOperatorParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+COPS?\r",
        0,
        "\r\n+COPS: 1,0,\"\"\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCOPSReadResult result = fakeATEngine.execute(new ATpsCOPSRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of mode", Mode.MANUAL_OPERATOR_SELECTION, result.getMode());
    assertEquals("Unexpected value of format", PpsCOPS.Format.LONG_ALPHANUMERIC, result.getFormat());
    assertEquals("Unexpected value of operator", "", result.getOperator());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
