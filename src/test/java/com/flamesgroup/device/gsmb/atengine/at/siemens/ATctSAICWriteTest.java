/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.ATEmptyResult;
import com.flamesgroup.device.gsmb.atengine.param.siemens.PctSAIC;
import org.junit.Test;

import java.util.ArrayList;

public class ATctSAICWriteTest {

  @Test
  public void testATctSAICWriteParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT^SAIC=1,1,1\r",
        0,
        "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATEmptyResult result = fakeATEngine.execute(new ATctSAICWrite(PctSAIC.IO.DIGITAL_IO, PctSAIC.Microphone.MICROPHONE1, PctSAIC.EarpieceAmplifier.AMPLIFIER1));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertNotNull("Unexpected result", result);

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
