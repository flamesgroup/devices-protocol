/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhCBC;
import org.junit.Test;

import java.util.ArrayList;

public class AThhCBCExecTest {

  @Test
  public void testAThhCBCExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#CBC\r",
        0,
        "\r\n#CBC: 0,75\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    AThhCBCExecResult result = fakeATEngine.execute(new AThhCBCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of chargerState", PhhCBC.ChargerState.CHARGER_NOT_CONNECTED, result.getChargerState());
    assertEquals("Unexpected value of batteryChargeLevel", 75, result.getBatteryVoltage());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
