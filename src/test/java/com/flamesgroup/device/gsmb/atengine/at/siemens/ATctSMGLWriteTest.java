/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.siemens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.at.SMSInfo;
import com.flamesgroup.device.gsmb.atengine.at.SMSStatus;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATctSMGLWriteTest {

  @Test
  public void testATpsSMGLWriteParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT^SMGL=4\r",
        0,
        "\r\n^SMGL: 1,1,,30\r\n0791836003000080040C918360536260910000312191112004800CC8329BFD06DDDF72363904\r\n" +
            "\r\n^SMGL: 2,1,,30\r\n0791836003000070040C918360536260910000312191116025800CC8329BFD06DDDF72363904\r\n" +
            "\r\n^SMGL: 3,0,,29\r\n0791836003000070240C918360536260910000312132413274800BF4F29C0E9AB7E7A1500\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATctSMGLWriteResult result = fakeATEngine.execute(new ATctSMGLWrite(SMSStatus.ALL));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("Unexpected value of sms list size", 3, result.getSmsList().size());

    List<SMSInfo> smsInfoList = result.getSmsList();
    SMSInfo smsInfo = smsInfoList.get(0);
    assertEquals("Unexpected value of index", 1, smsInfo.getIndex());
    assertEquals("Unexpected value of status" + SMSStatus.READ, SMSStatus.READ, smsInfo.getStatus());
    assertNull("Unexpected value of  alpha", smsInfo.getAlpha());
    assertEquals("Unexpected value of length", 30, smsInfo.getLength());
    assertEquals("Unexpected value of PDU", "0791836003000080040C918360536260910000312191112004800CC8329BFD06DDDF72363904", smsInfo.getPDU());

    smsInfo = smsInfoList.get(1);
    assertEquals("Unexpected value of index", 2, smsInfo.getIndex());
    assertEquals("Unexpected value of status" + SMSStatus.READ, SMSStatus.READ, smsInfo.getStatus());
    assertNull("Unexpected value of alpha", smsInfo.getAlpha());
    assertEquals("Unexpected value of length", 30, smsInfo.getLength());
    assertEquals("Unexpected value of PDU", "0791836003000070040C918360536260910000312191116025800CC8329BFD06DDDF72363904", smsInfo.getPDU());

    smsInfo = smsInfoList.get(2);
    assertEquals("Unexpected value of index", 3, smsInfo.getIndex());
    assertEquals("Unexpected value of status" + SMSStatus.UNREAD, SMSStatus.UNREAD, smsInfo.getStatus());
    assertNull("Unexpected value of alpha", smsInfo.getAlpha());
    assertEquals("Unexpected value of length", 29, smsInfo.getLength());
    assertEquals("Unexpected value of PDU", "0791836003000070240C918360536260910000312132413274800BF4F29C0E9AB7E7A1500", smsInfo.getPDU());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
