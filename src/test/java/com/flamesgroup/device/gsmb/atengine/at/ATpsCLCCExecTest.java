/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.PpsCLCC;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATpsCLCCExecTest {

  @Test
  public void testATpsCLCCExecParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT+CLCC\r",
        0,
        "\r\n+CLCC: 1,0,0,0,0,\"0934942971\",145\r\n" +
            "\r\n+CLCC: 1,1,0,0,1,\"0934942988\",128\r\n" +
            "\r\n+CLCC: 1,1,4,0,0,\"67170416\",161\r\n" +
            "\r\n+CLCC: 1,0,0,0,0,\"\",145,\"\"\r\n" +
            "\r\n+CLCC: 1,1,2,0,0\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCLCCExecResult result = fakeATEngine.execute(new ATpsCLCCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(5, result.getCallList().size());

    assertEquals(PpsCLCC.TypeOfAddressOctet.TypeOfNumber.INTERNATIONAL_NUMBER, result.getCallList().get(0).getTypeOfAddressOctet().getTypeOfNumber());
    assertEquals(PpsCLCC.TypeOfAddressOctet.NumberingPlanIdentification.TELEPHONE_NUMBERING_PLAN, result.getCallList().get(0).getTypeOfAddressOctet().getNumberingPlanIdentification());
    assertEquals(PpsCLCC.TypeOfAddressOctet.TypeOfNumber.UNKNOWN, result.getCallList().get(1).getTypeOfAddressOctet().getTypeOfNumber());
    assertEquals(PpsCLCC.TypeOfAddressOctet.NumberingPlanIdentification.UNKNOWN, result.getCallList().get(1).getTypeOfAddressOctet().getNumberingPlanIdentification());
    assertEquals(PpsCLCC.TypeOfAddressOctet.TypeOfNumber.NATIONAL_NUMBER, result.getCallList().get(2).getTypeOfAddressOctet().getTypeOfNumber());
    assertEquals(PpsCLCC.TypeOfAddressOctet.NumberingPlanIdentification.TELEPHONE_NUMBERING_PLAN, result.getCallList().get(2).getTypeOfAddressOctet().getNumberingPlanIdentification());
    assertNull(result.getCallList().get(4).getTypeOfAddressOctet());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testATpsCLCCExecParsingTheSameResult() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new ArrayList<>();

    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CLCC\r",
        0,
        "\r\n+CLCC: 1,0,0,0,0,\"0934942971\",145\r\n" +
            "\r\nOK\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CLCC\r",
        0,
        "\r\n+CLCC: 1,0,0,0,0,\"0934942971\",145\r\n" +
            "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATpsCLCCExecResult firstResult = fakeATEngine.execute(new ATpsCLCCExec());
    ATpsCLCCExecResult secondResult = fakeATEngine.execute(new ATpsCLCCExec());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(firstResult.getCallList(), secondResult.getCallList());
  }

}
