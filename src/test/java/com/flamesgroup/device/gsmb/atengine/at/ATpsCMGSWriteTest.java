/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ATpsCMGSWriteTest {

  @Test
  public void testATpsCMGSWriteParsingWithEcho() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT+CMGS=35\r",
        0,
        "AT+CMGS=35\r\r\n> "
    ));
    iterations.add(new FakeATSubChannel.Iteration(
        "0031000B810839842479F00000FF18F4F29C0E9AB7E720B3FCDD069DE76D50FB4DAEB3CB" + (char) 0x1A,
        0,
        "0031000B810839842479F00000FF18F4F29C0E9AB7E720B3FCDD069DE76D50FB4DAEB3CB" + (char) 0x1A +
            "\r\n+CMGS: 64\r\n\r" +
            "\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    ATpsCMGSWriteResult result = fakeATEngine.execute(new ATpsCMGSWrite(35, "0031000B810839842479F00000FF18F4F29C0E9AB7E720B3FCDD069DE76D50FB4DAEB3CB"));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(64, result.getMessageReference());
    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
