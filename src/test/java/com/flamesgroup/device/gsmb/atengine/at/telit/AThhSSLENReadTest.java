/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.*;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.Collections;

public class AThhSSLENReadTest {

  @Test
  public void testAThhSSLENReadTrue() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#SSLEN?\r",
        0,
        "\r\n#SSLEN: 1,1\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    AThhSSLENReadResult result = fakeATEngine.execute(new AThhSSLENRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(true, result.isEnable());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhSSLENReadFalse() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#SSLEN?\r",
        0,
        "\r\n#SSLEN: 1,0\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    AThhSSLENReadResult result = fakeATEngine.execute(new AThhSSLENRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(false, result.isEnable());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testAThhSSLENReadFail() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        "AT#SSLEN?\r",
        0,
        "\r\n#SSLEN: 1,3\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(Collections.emptyList());

    AThhSSLENReadResult result = fakeATEngine.execute(new AThhSSLENRead());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(1, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
