/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at.telit;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhSSLSECDATA;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AThhSSLSECDATAActionStoreWriteTest {

  @Test
  public void testAThhSSLSECDATAWriteParsingWithEcho() throws Exception {
    URL resource = getClass().getClassLoader().getResource("CACertificate.crt");
    if (resource == null) {
      throw new FileNotFoundException("can't find resource for test");
    }
    byte[] caBytes = Files.readAllBytes(Paths.get(resource.toURI()));
    String caCertificate = new String(caBytes);
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        "AT#SSLSECDATA=1,1,1," + caBytes.length + "\r",
        0,
        "\r\n> "
    ));
    iterations.add(new FakeATSubChannel.Iteration(
        caCertificate + (char) 0x1A,
        0,
        "\r\nOK\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new ArrayList<>());

    fakeATEngine.execute(new AThhSSLSECDATAActionStoreWrite(PhhSSLSECDATA.DataType.CA_CERTIFICATE, caBytes.length, caCertificate));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

}
