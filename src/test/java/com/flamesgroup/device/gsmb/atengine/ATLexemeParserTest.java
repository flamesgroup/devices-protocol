/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ATLexemeParserTest {

  private List<String> handledATLexemes;
  private ATLexemeParser atLexemeParser;

  @Before
  public void setUp() {
    handledATLexemes = new ArrayList<>();
    atLexemeParser = new ATLexemeParser(new IATLexemeHandler() {

      @Override
      public void handleATLexeme(final String atLexeme) {
        handledATLexemes.add(atLexeme);
      }
    });
  }

  @Test
  public void testParse() {
    CharSequence responseData;

    responseData = "AT\r\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"AT\r", "OK"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "AT\r\r\nERROR\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"AT\r", "ERROR"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "ATA\r\r\nNO CARRIER\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"ATA\r", "NO CARRIER"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "ATT\r\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"ATT\r", "OK"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "AT+CMEE?\r\r\n+CMEE: 0\r\n\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"AT+CMEE?\r", "+CMEE: 0", "OK"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "ATI\r\r\nSIEMENS\r\nMC55i\r\nREVISION 01.003\r\n\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"ATI\r", "SIEMENS", "MC55i", "REVISION 01.003", "OK"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "AT&V\r\r\nACTIVE PROFILE:"
        + "\r\nE1 Q0 V1 X4 &C1 &D2 &S0 \\Q0 "
        + "\r\nS0:000 S3:013 S4:010 S5:008 S6:000 S7:060 S8:000 S10:002 S18:000 "
        + "\r\n+CBST: 7,0,1 "
        + "\r\n+CRLP: 61,61,78,6 "
        + "\r\n+CR: 0 "
        + "\r\n+FCLASS: 0 "
        + "\r\n+ILRR: 0 "
        + "\r\n+IPR: 57600 "
        + "\r\n+CMEE: 0 "
        + "\r\n^SCKS: 0,0 "
        + "\r\n+CGSMS: 3\r\n"
        + "\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {
            "AT&V\r", "ACTIVE PROFILE:",
            "E1 Q0 V1 X4 &C1 &D2 &S0 \\Q0 ",
            "S0:000 S3:013 S4:010 S5:008 S6:000 S7:060 S8:000 S10:002 S18:000 ",
            "+CBST: 7,0,1 ",
            "+CRLP: 61,61,78,6 ",
            "+CR: 0 ",
            "+FCLASS: 0 ",
            "+ILRR: 0 ",
            "+IPR: 57600 ",
            "+CMEE: 0 ",
            "^SCKS: 0,0 ",
            "+CGSMS: 3",
            "OK"}
        , handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "AT^SBC?\r\r\n^SBC: 0,0,95\r\n\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"AT^SBC?\r", "^SBC: 0,0,95", "OK"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    responseData = "AT+CREG?\r\r\n+CREG: 2,4,\"0145\",\"291A\"\r\n\r\nOK\r\n";
    atLexemeParser.parse(responseData);
    assertArrayEquals(new String[] {"AT+CREG?\r", "+CREG: 2,4,\"0145\",\"291A\"", "OK"}, handledATLexemes.toArray());
    handledATLexemes.clear();
  }

  @Test
  public void testParseDataReceivePartly() {
    CharSequence atResponseData1Part1 = "AT+CMEE?\r\r\n+CMEE: 0\r\n";
    CharSequence atResponseData1Part2 = "\r\nOK\r\n\r\nRING\r\n^SHUTDOWN\r\n";
    atLexemeParser.parse(atResponseData1Part1);
    atLexemeParser.parse(atResponseData1Part2);
    assertArrayEquals(new String[] {"AT+CMEE?\r", "+CMEE: 0", "OK", "RING", "^SHUTDOWN"}, handledATLexemes.toArray());
    handledATLexemes.clear();

    CharSequence atResponseData2Part1 = "AT&V\r\r\nACTIVE PROFILE:"
        + "\r\nE1 Q0 V1 X4 &C1 &D2 &S0 \\Q0 "
        + "\r\nS0:000 S3:013 S4:010 S5:008 S6:000 S7:060 S8:000 S10:002 S18:000 ";
    CharSequence atResponseData2Part2 = "\r\n+CBST: 7,0,1 "
        + "\r\n+CRLP: 61,61,78,6 "
        + "\r\n+CR: 0 "
        + "\r\n+FCLASS: 0 "
        + "\r\n+ILRR: 1 "
        + "\r\n+IPR: 57600 "
        + "\r\n+CMEE: 0 ";
    CharSequence atResponseData2Part3 = "\r\n^SCKS: 0,0 "
        + "\r\n+CGSMS: 3\r\n"
        + "\r\nOK\r\n";
    atLexemeParser.parse(atResponseData2Part1);
    atLexemeParser.parse(atResponseData2Part2);
    atLexemeParser.parse(atResponseData2Part3);
    assertArrayEquals(new String[] {"" +
            "AT&V\r", "ACTIVE PROFILE:",
            "E1 Q0 V1 X4 &C1 &D2 &S0 \\Q0 ",
            "S0:000 S3:013 S4:010 S5:008 S6:000 S7:060 S8:000 S10:002 S18:000 ",
            "+CBST: 7,0,1 ",
            "+CRLP: 61,61,78,6 ",
            "+CR: 0 ",
            "+FCLASS: 0 ",
            "+ILRR: 1 ",
            "+IPR: 57600 ",
            "+CMEE: 0 ",
            "^SCKS: 0,0 ",
            "+CGSMS: 3",
            "OK"}
        , handledATLexemes.toArray());
    handledATLexemes.clear();
  }

}
