/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.urc.telit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM.CallStatus;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM.CallType;
import com.flamesgroup.device.gsmb.atengine.param.telit.PhhECAM.TypeOfNumber;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class URChhECAMTest {

  private final List<URChhECAMHandlerValue> handlerValues = new ArrayList<>();

  private static class URChhECAMHandlerValue {

    private final int callId;
    private final CallStatus callStatus;
    private final CallType callType;
    private final String calledNumber;
    private final TypeOfNumber typeOfNumber;

    private URChhECAMHandlerValue(final int callId, final CallStatus callStatus, final CallType callType, final String calledNumber,
        final TypeOfNumber typeOfNumber) {
      this.callId = callId;
      this.callStatus = callStatus;
      this.callType = callType;
      this.calledNumber = calledNumber;
      this.typeOfNumber = typeOfNumber;
    }

    public int getCallId() {
      return callId;
    }

    public CallStatus getCallStatus() {
      return callStatus;
    }

    public CallType getCallType() {
      return callType;
    }

    public String getCalledNumber() {
      return calledNumber;
    }

    public TypeOfNumber getTypeOfNumber() {
      return typeOfNumber;
    }

  }

  private class URChhECAMHandler implements IURChhECAMHandler {

    @Override
    public void handleCallEvent(final int callId, final CallStatus callStatus, final CallType callType, final String calledNumber,
        final TypeOfNumber typeOfNumber) {
      handlerValues.add(new URChhECAMHandlerValue(callId, callStatus, callType, calledNumber, typeOfNumber));
    }

  }

  @Test
  public void testURChhECAMSuccessfulParsing() throws Exception {
    List<FakeATSubChannel.Iteration> iterations = new LinkedList<>();
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#ECAM: 0,1,2,,,\"+8911959XXXX\",129\r\n"));
    iterations.add(new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#ECAM: 0,2,2,,,\r\n"));

    FakeATEngine fakeATEngine = new FakeATEngine(iterations);
    fakeATEngine.start(new URChhECAM(new URChhECAMHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals("unexpected value of callId", 0, handlerValues.get(0).getCallId());
    assertEquals("unexpected value of callStatus", CallStatus.CALLING, handlerValues.get(0).getCallStatus());
    assertEquals("unexpected value of callType ", CallType.DATA, handlerValues.get(0).getCallType());
    assertEquals("unexpected value of calledNumber", "+8911959XXXX", handlerValues.get(0).getCalledNumber());
    assertEquals("unexpected value of typeOfNumber", TypeOfNumber.NATIONAL_NUMBER, handlerValues.get(0).getTypeOfNumber());

    assertEquals("unexpected value of callId", 0, handlerValues.get(1).getCallId());
    assertEquals("unexpected value of callStatus", CallStatus.CONNECTING, handlerValues.get(1).getCallStatus());
    assertEquals("unexpected value of callType ", CallType.DATA, handlerValues.get(1).getCallType());
    assertNull("unexpected value of calledNumber", handlerValues.get(1).getCalledNumber());
    assertNull("unexpected value of typeOfNumber", handlerValues.get(1).getTypeOfNumber());

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testURChhECAMUnsuccessfulParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration(
        null,
        0,
        "\r\n#ECAM: 0,2,1,,,\"+8911959XXXX\"\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new URChhECAM(new URChhECAMHandler()));

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertTrue(fakeATEngine.getUnprocessedATLexemes().size() > 0);
    assertEquals("#ECAM: 0,2,1,,,\"+8911959XXXX\"", fakeATEngine.getUnprocessedATLexemes().get(0));
  }

}
