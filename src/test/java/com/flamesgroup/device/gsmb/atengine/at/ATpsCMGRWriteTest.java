/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb.atengine.at;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeATSubChannel;
import com.flamesgroup.device.gsmb.atengine.FakeATEngine;
import org.junit.Test;

import java.util.ArrayList;

public class ATpsCMGRWriteTest {

  @Test
  public void testATpsCMGRWriteParsing() throws Exception {
    FakeATSubChannel.Iteration i = new FakeATSubChannel.Iteration("AT+CMGR=1\r",
        0,
        "\r\n+CMGR: 1,,30" +
            "\r\n0791836003000080040C918360536260910000312191112004800CC8329BFD06DDDF72363904\r\n" +
            "\r\nOK\r\n");

    FakeATEngine fakeATEngine = new FakeATEngine(i);
    fakeATEngine.start(new ArrayList<>());

    ATpsCMGRWriteResult result = fakeATEngine.execute(new ATpsCMGRWrite(1));
    assertEquals("Unexpected value of status", SMSStatus.READ, result.getStatus());
    assertNull("Unexpected value of alpha", result.getAlpha());
    assertEquals("Unexpected value of length", 30, result.getLength());
    assertEquals("Unexpected value of PDU", "0791836003000080040C918360536260910000312191112004800CC8329BFD06DDDF72363904", result.getPDU());

    fakeATEngine.checkForException();
    fakeATEngine.stop();

    assertEquals(0, fakeATEngine.getUnprocessedATLexemes().size());
  }

  @Test
  public void testEquals() throws Exception {
    ATpsCMGRWriteResult result = new ATpsCMGRWriteResult(SMSStatus.READ, "qwerty", 1, "asdf");
    ATpsCMGRWriteResult same = new ATpsCMGRWriteResult(SMSStatus.READ, "qwerty", 1, "asdf");
    ATpsCMGRWriteResult similar = new ATpsCMGRWriteResult(SMSStatus.UNREAD, "qwerty", 1, "asdf");
    ATpsCMGRWriteResult broken1 = new ATpsCMGRWriteResult(SMSStatus.READ, null, 1, "asdf");
    ATpsCMGRWriteResult broken1same = new ATpsCMGRWriteResult(SMSStatus.READ, null, 1, "asdf");
    ATpsCMGRWriteResult broken2 = new ATpsCMGRWriteResult(SMSStatus.READ, "qwerty", 1, null);

    assertTrue(result.equals(same));
    assertFalse(result.equals(similar));
    assertFalse(result.equals(null));
    assertFalse(result.equals(broken1));
    assertFalse(result.equals(broken2));
    assertTrue(broken1.equals(broken1same));
  }

}
