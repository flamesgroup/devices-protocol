/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpPacket;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ATSubChannelTest {

  @Test
  public void testStart() throws Exception {
    List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    FakeMudpChannel.IterationData startIterationData = new FakeMudpChannel.IterationData((byte) 0x01, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(startIterationData, 50, startIterationData));

    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(iterations), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.getATSubChannel().start(atData -> {});
    assertTrue(gsmChannel.isEnslaved());
    gsmChannel.free();
    assertFalse(gsmChannel.isEnslaved());
  }

  @Test
  public void testStop() throws Exception {
    List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    FakeMudpChannel.IterationData startIterationData = new FakeMudpChannel.IterationData((byte) 0x01, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(startIterationData, 50, startIterationData));

    FakeMudpChannel.IterationData stopIterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(stopIterationData, 50, stopIterationData));

    IGSMChannel gsmChannel = new GSMChannel(new FakeMudpChannel(iterations), new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.getATSubChannel().start(atData -> {});
    assertTrue(gsmChannel.isEnslaved());
    gsmChannel.getATSubChannel().stop();
    gsmChannel.free();
    assertFalse(gsmChannel.isEnslaved());
  }

  @Test
  public void testWriteATData() throws Exception {
    List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    FakeMudpChannel.IterationData startIterationData = new FakeMudpChannel.IterationData((byte) 0x01, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(startIterationData, 50, startIterationData));

    FakeMudpChannel.IterationData atSendIterationData = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.wrap("AT\r\n".getBytes()).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData atReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.wrap("AT\r\r\nOK\r\n".getBytes()).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(atSendIterationData, 50, atReceiveIterationData));

    FakeMudpChannel.IterationData stopIterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(stopIterationData, 50, stopIterationData));

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    final StringBuilder handleATData = new StringBuilder();
    IGSMChannel gsmChannel = new GSMChannel(fakeMudpChannel, new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.getATSubChannel().start(handleATData::append);
    assertTrue(gsmChannel.isEnslaved());
    gsmChannel.getATSubChannel().writeATData("AT\r\n");
    fakeMudpChannel.waitForIterationNumber(2);
    assertEquals("AT\r\r\nOK\r\n", handleATData.toString());
    gsmChannel.getATSubChannel().stop();
    gsmChannel.free();
    assertFalse(gsmChannel.isEnslaved());

    fakeMudpChannel.checkForException();
  }

  @Test
  public void testWriteRawATData() throws Exception {
    byte[] rawATData = new byte[MudpPacket.DATA_MAX_LENGTH * 2 + 1];
    Arrays.fill(rawATData, 0, MudpPacket.DATA_MAX_LENGTH, (byte) 0x01);
    Arrays.fill(rawATData, MudpPacket.DATA_MAX_LENGTH, MudpPacket.DATA_MAX_LENGTH * 2, (byte) 0x02);
    Arrays.fill(rawATData, MudpPacket.DATA_MAX_LENGTH * 2, MudpPacket.DATA_MAX_LENGTH * 2 + 1, (byte) 0x03);

    List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    FakeMudpChannel.IterationData startIterationData = new FakeMudpChannel.IterationData((byte) 0x01, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(startIterationData, 5, startIterationData));

    byte[] rawATData1 = new byte[MudpPacket.DATA_MAX_LENGTH];
    Arrays.fill(rawATData1, (byte) 0x01);
    FakeMudpChannel.IterationData atSendIterationData1 = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.wrap(rawATData1).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(atSendIterationData1, 5, null));

    byte[] rawATData2 = new byte[MudpPacket.DATA_MAX_LENGTH];
    Arrays.fill(rawATData2, (byte) 0x02);
    FakeMudpChannel.IterationData atSendIterationData2 = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.wrap(rawATData2).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(atSendIterationData2, 5, null));

    byte[] rawATData3 = new byte[1];
    Arrays.fill(rawATData3, (byte) 0x03);
    FakeMudpChannel.IterationData atSendIterationData3 = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.wrap(rawATData3).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(atSendIterationData3, 5, null));

    FakeMudpChannel.IterationData atReceiveIterationData = new FakeMudpChannel.IterationData((byte) 0x00, ByteBuffer.wrap(new byte[] {(byte) 0x55}).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(null, 50, atReceiveIterationData));

    FakeMudpChannel.IterationData stopIterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(stopIterationData, 5, stopIterationData));

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    final ByteBuffer handleRawATDataBuffer = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
    IGSMChannel gsmChannel = new GSMChannel(fakeMudpChannel, new MudpChannelAddress((byte) 0), 0);
    gsmChannel.enslave();
    gsmChannel.getRawATSubChannel().start(atDataBuffer -> {
      handleRawATDataBuffer.put(atDataBuffer);
      handleRawATDataBuffer.flip();
    });
    assertTrue(gsmChannel.isEnslaved());
    gsmChannel.getRawATSubChannel().writeRawATData(ByteBuffer.wrap(rawATData).order(ByteOrder.LITTLE_ENDIAN));
    fakeMudpChannel.waitForIterationNumber(5);
    assertEquals(ByteBuffer.wrap(new byte[] {(byte) 0x55}).order(ByteOrder.LITTLE_ENDIAN), handleRawATDataBuffer);
    gsmChannel.getATSubChannel().stop();
    gsmChannel.free();
    assertFalse(gsmChannel.isEnslaved());

    fakeMudpChannel.checkForException();
  }

}
