/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.gsmb;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AudioChannelTest {

  private List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
  private final IAudioSubChannelHandler emptyAudioSubChannelHandler = (timestamp, audioData, offset, length) -> {
  };

  @Before
  public void setUp() throws Exception {
    iterations = new ArrayList<>();
  }

  @After
  public void tearDown() throws Exception {
  }

  private void addStartIteration(final List<FakeMudpChannel.Iteration> iterations) {
    FakeMudpChannel.IterationData startRequestIterationData = new FakeMudpChannel.IterationData((byte) 0x01, ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData startResponseIterationData = new FakeMudpChannel.IterationData((byte) 0x01, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(startRequestIterationData, 5, startResponseIterationData));
  }

  private void addStopIteration(final List<FakeMudpChannel.Iteration> iterations) {
    FakeMudpChannel.IterationData stopIterationData = new FakeMudpChannel.IterationData((byte) 0x02, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(stopIterationData, 5, stopIterationData));
  }

  private void addAudioDataIteration(final List<FakeMudpChannel.Iteration> iterations, final ByteBuffer sendAudioData, final ByteBuffer receivedAudioData) {
    FakeMudpChannel.IterationData sendIterationData;
    if (sendAudioData != null) {
      sendIterationData = new FakeMudpChannel.IterationData((byte) 0x00, sendAudioData);
    } else {
      sendIterationData = null;
    }
    FakeMudpChannel.IterationData receivedIterationData;
    if (receivedAudioData != null) {
      receivedIterationData = new FakeMudpChannel.IterationData((byte) 0x00, receivedAudioData);
    } else {
      receivedIterationData = null;
    }
    iterations.add(new FakeMudpChannel.Iteration(sendIterationData, 0, receivedIterationData));
  }

  private ByteBuffer createAudioData(final int timestamp, final byte[] data) {
    ByteBuffer bb = ByteBuffer.allocate(164).order(ByteOrder.LITTLE_ENDIAN);
    bb.putInt(timestamp);
    bb.put(data);
    bb.flip();
    return bb;
  }

  @Test
  public void writeSingleAudioChunk() throws Exception {
    addStartIteration(iterations);
    addAudioDataIteration(iterations, createAudioData(0, new byte[160]), null);
    addStopIteration(iterations);

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    AudioChannel audioChannel = new AudioChannel(fakeMudpChannel, new MudpChannelAddress(), 0);
    audioChannel.enslave();
    audioChannel.start(null, emptyAudioSubChannelHandler);

    byte[] data = new byte[160];
    audioChannel.writeAudioData(0, data, 0, data.length);

    audioChannel.stop();

    fakeMudpChannel.checkForException();
  }

  @Test
  public void writeTwoAudioChunk() throws Exception {
    addStartIteration(iterations);
    addAudioDataIteration(iterations, createAudioData(0, new byte[160]), null);
    addAudioDataIteration(iterations, createAudioData(10, new byte[160]), null);
    addStopIteration(iterations);

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    AudioChannel audioChannel = new AudioChannel(fakeMudpChannel, new MudpChannelAddress(), 0);
    audioChannel.enslave();
    audioChannel.start(null, emptyAudioSubChannelHandler);

    byte[] data = new byte[320];
    audioChannel.writeAudioData(0, data, 0, data.length);

    audioChannel.stop();

    fakeMudpChannel.checkForException();
  }

  @Test
  public void writeTwoAudioChunkWithRemainData() throws Exception {
    addStartIteration(iterations);
    addAudioDataIteration(iterations, createAudioData(0, new byte[160]), null);
    addAudioDataIteration(iterations, createAudioData(10, new byte[160]), null);
    addStopIteration(iterations);

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    AudioChannel audioChannel = new AudioChannel(fakeMudpChannel, new MudpChannelAddress(), 0);
    audioChannel.enslave();
    audioChannel.start(null, emptyAudioSubChannelHandler);

    byte[] data = new byte[340];
    audioChannel.writeAudioData(0, data, 0, data.length);

    audioChannel.stop();

    fakeMudpChannel.checkForException();
  }

  @Test
  public void writeTwoAudioChunkWithShiftedTimestamp() throws Exception {
    addStartIteration(iterations);
    addAudioDataIteration(iterations, createAudioData(10, new byte[160]), null);
    addAudioDataIteration(iterations, createAudioData(20, new byte[160]), null);
    addStopIteration(iterations);

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    AudioChannel audioChannel = new AudioChannel(fakeMudpChannel, new MudpChannelAddress(), 0);
    audioChannel.enslave();
    audioChannel.start(null, emptyAudioSubChannelHandler);

    byte[] data = new byte[320];
    audioChannel.writeAudioData(10, data, 0, data.length);

    audioChannel.stop();

    fakeMudpChannel.checkForException();
  }

  @Test
  public void writeThreeAudioChunkWithRemainDataAndShiftedTimestamp() throws Exception {
    addStartIteration(iterations);
    {
      byte[] sendData = new byte[160];
      Arrays.fill(sendData, (byte) 0x1);
      addAudioDataIteration(iterations, createAudioData(10, sendData), null);
    }
    {
      byte[] sendData = new byte[160];
      Arrays.fill(sendData, (byte) 0x1);
      addAudioDataIteration(iterations, createAudioData(20, sendData), null);
    }
    {
      byte[] sendData = new byte[160];
      Arrays.fill(sendData, 0, 40, (byte) 0x1);
      Arrays.fill(sendData, 40, sendData.length, (byte) 0x2);
      addAudioDataIteration(iterations, createAudioData(28, sendData), null);
    }
    {
      byte[] sendData = new byte[160];
      Arrays.fill(sendData, 0, 40, (byte) 0x2);
      Arrays.fill(sendData, 40, sendData.length, (byte) 0x3);
      addAudioDataIteration(iterations, createAudioData(38, sendData), null);
    }
    addStopIteration(iterations);

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);

    AudioChannel audioChannel = new AudioChannel(fakeMudpChannel, new MudpChannelAddress(), 0);
    audioChannel.enslave();
    audioChannel.start(null, emptyAudioSubChannelHandler);

    {
      byte[] data = new byte[360];
      Arrays.fill(data, (byte) 0x1);
      audioChannel.writeAudioData(10, data, 0, data.length);
    }
    {
      byte[] data = new byte[160];
      Arrays.fill(data, (byte) 0x2);
      audioChannel.writeAudioData(30, data, 0, data.length);
    }
    {
      byte[] data = new byte[120];
      Arrays.fill(data, (byte) 0x3);
      audioChannel.writeAudioData(40, data, 0, data.length);
    }

    audioChannel.stop();

    fakeMudpChannel.checkForException();
  }

}
