/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.simb.ISCReaderChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderState;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FakeSCReaderChannel extends
    GenericFakeSCChannel<FakeSCReaderChannel.SendIterationData, FakeSCReaderChannel.ReceiveIterationData, FakeSCReaderChannel.Iteration> implements
    ISCReaderChannel, ISCReaderSubChannel {

  private static final ATR atr = new ATR(
      new byte[] {(byte) 0x3B, (byte) 0x1E, (byte) 0x95, (byte) 0x80, (byte) 0x67, (byte) 0x32, (byte) 0x54, (byte) 0x4D, (byte) 0x4D, (byte) 0x0E, (byte) 0x23, (byte) 0x30, (byte) 0x71, (byte) 0x32,
          (byte) 0x82, (byte) 0x9F, (byte) 0x00});

  private boolean state;

  private ISCReaderSubChannelHandler scReaderSubChannelHandler;

  public FakeSCReaderChannel() {
    this(Collections.emptyList());
  }

  public FakeSCReaderChannel(final FakeSCReaderChannel.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeSCReaderChannel(final List<FakeSCReaderChannel.Iteration> iterations) {
    super(iterations);
  }

  @Override
  public boolean isEnslaved() {
    return state;
  }

  @Override
  public void enslave() throws ChannelException, InterruptedException {
    state = true;
  }

  @Override
  public void free() throws ChannelException {
    state = false;

  }

  @Override
  public IChannelData createChannelData() {
    throw new UnsupportedOperationException();
  }

  @Override
  public ISCReaderSubChannel getSCReaderSubChannel() {
    return this;
  }

  @Override
  public ATR start(final ISCReaderSubChannelHandler scReaderSubChannelHandler) throws ChannelException, InterruptedException {
    this.scReaderSubChannelHandler = scReaderSubChannelHandler;

    if (!iterations.isEmpty()) {
      startIterate();
    }

    return atr;
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    scReaderSubChannelHandler = null;

  }

  @Override
  public void writeAPDUCommand(final APDUCommand command) throws ChannelException, InterruptedException {
    send(new SendIterationData(command));
  }

  @Override
  public SCReaderState getState() throws ChannelException, InterruptedException {
    throw new UnsupportedOperationException();
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeSCReaderChannel.ReceiveIterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {
      @Override
      public Exception call() throws Exception {
        try {
          scReaderSubChannelHandler.handleAPDUResponse(iterationData.getData());
          return null;

        } catch (Exception e) {
          return e;
        }
      }
    });
  }

  public static class Iteration extends GenericFakeSCChannel.Iteration<FakeSCReaderChannel.SendIterationData, FakeSCReaderChannel.ReceiveIterationData> {

    public Iteration(final SendIterationData sendIterationData, final long timeout, final ReceiveIterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class SendIterationData extends GenericFakeSCChannel.SendIterationData {

    public SendIterationData(final APDUCommand data) {
      super(data);
    }

  }

  public static class ReceiveIterationData extends GenericFakeSCChannel.ReceiveIterationData {

    public ReceiveIterationData(final APDUResponse data) {
      super(data);
    }

  }

}
