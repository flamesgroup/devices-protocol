/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelErrCodeException;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.helper.DataRepresentationHelper;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FakeChannelData extends GenericByteBufferExchangerIterator<FakeChannelData.IterationData, FakeChannelData.Iteration> implements IChannelData {

  private IChannelDataHandler channelDataHandler;

  public FakeChannelData() {
    this(Collections.emptyList());
  }

  public FakeChannelData(final FakeChannelData.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeChannelData(final List<FakeChannelData.Iteration> iterations) {
    super(iterations);
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeChannelData.IterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {
      @Override
      public Exception call() throws Exception {
        try {
          // Maximum one of next handle* must be call
          ByteBuffer receivedData = iterationData.getData();
          if (receivedData != null) {
            channelDataHandler.handleReceive(receivedData);
          }
          ChannelErrCodeException receivedErrCode = iterationData.getReceivedErrCode();
          if (receivedErrCode != null) {
            channelDataHandler.handleErrCode(receivedErrCode);
          }
          return null;
        } catch (Exception e) {
          return e;
        }
      }
    });
  }

  @Override
  public boolean isAttached() {
    return channelDataHandler != null;
  }

  @Override
  public void attach(final byte type, final IChannelDataHandler channelDataHandler) throws ChannelException {
    this.channelDataHandler = channelDataHandler;

    if (iterations.isEmpty()) {
      return;
    }

    startIterate();
  }

  @Override
  public void detach() {
    this.channelDataHandler = null;
  }

  @Override
  public void sendReliable(final ByteBuffer data) throws ChannelException, InterruptedException {
    send(new IterationData(data));
  }

  @Override
  public void sendUnreliable(final ByteBuffer data) throws ChannelException {
    try {
      send(new IterationData(data));
    } catch (InterruptedException e) {
      throw new ChannelException(e);
    }
  }

  public static class Iteration extends GenericByteBufferExchangerIterator.Iteration<FakeChannelData.IterationData> {

    public Iteration(final IterationData sendIterationData, final long timeout, final IterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class IterationData extends GenericByteBufferExchangerIterator.IterationData {

    private final ChannelErrCodeException receivedErrCode;

    public IterationData(final ByteBuffer data) {
      super(data);
      this.receivedErrCode = null;
    }

    public IterationData(final byte[] data) {
      super(data);
      this.receivedErrCode = null;
    }

    public IterationData(final ChannelErrCodeException receivedErrCode) {
      super((ByteBuffer) null);
      this.receivedErrCode = receivedErrCode;
    }

    public ChannelErrCodeException getReceivedErrCode() {
      return receivedErrCode;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this, receivedErrCode);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
        return true;
      if (!(obj instanceof IterationData))
        return false;
      IterationData that = (IterationData) obj;
      return Objects.equals(this.receivedErrCode, that.receivedErrCode) && super.equals(that);
    }

    @Override
    public String toString() {
      if (receivedErrCode == null) {
        return DataRepresentationHelper.toHexArrayString(getData());
      } else {
        return receivedErrCode.toString();
      }
    }

  }

}
