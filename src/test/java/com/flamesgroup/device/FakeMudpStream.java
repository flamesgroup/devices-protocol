/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.protocol.mudp.IMudpStream;
import com.flamesgroup.device.protocol.mudp.MudpClosedStreamException;
import com.flamesgroup.device.protocol.mudp.MudpIOException;
import com.flamesgroup.device.protocol.mudp.MudpPacket;
import com.flamesgroup.device.protocol.mudp.MudpPacketReadException;
import com.flamesgroup.device.protocol.mudp.MudpPacketWriteException;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingDeque;

public class FakeMudpStream extends GenericMudpPacketExchangerIterator<FakeMudpStream.IterationData, FakeMudpStream.Iteration> implements IMudpStream {

  private final BlockingQueue<MudpPacket> bqMudpPackets = new LinkedBlockingDeque<>();

  private final MudpPacket closeMudpPacket = new MudpPacket(0);

  private boolean open;

  public FakeMudpStream(final List<Iteration> iterations) {
    super(iterations);
  }

  @Override
  public boolean isOpen() {
    return open;
  }

  @Override
  public void open() throws MudpIOException {
    open = true;

    startIterate();
  }

  @Override
  public void close() throws MudpIOException {
    open = false;
    try {
      bqMudpPackets.put(closeMudpPacket);
    } catch (InterruptedException e) {
    }
  }

  @Override
  public MudpPacket readMudpPacket() throws MudpIOException, MudpPacketReadException {
    try {
      final MudpPacket mudpPacket = bqMudpPackets.take();
      if (closeMudpPacket == mudpPacket) {
        throw new MudpClosedStreamException();
      } else {
        return mudpPacket;
      }
    } catch (InterruptedException e) {
      throw new MudpIOException(e);
    }
  }

  @Override
  public void writeMudpPacket(final MudpPacket mudpPacket) throws MudpIOException, MudpPacketWriteException {
    try {
      send(new IterationData(mudpPacket));
    } catch (InterruptedException e) {
      throw new MudpPacketWriteException(e);
    }
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeMudpStream.IterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {

      @Override
      public Exception call() throws Exception {
        try {
          bqMudpPackets.put(iterationData.getData());
          return null;
        } catch (Exception e) {
          return e;
        }
      }

    });
  }

  public static class Iteration extends GenericMudpPacketExchangerIterator.Iteration<FakeMudpStream.IterationData> {

    public Iteration(final IterationData sendIterationData, final long timeout, final IterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

    public Iteration(final MudpPacket sendData, final long timeout, final MudpPacket receivedData) {
      super(sendData == null ? null : new IterationData(sendData), timeout, receivedData == null ? null : new IterationData(receivedData));
    }

  }

  public static class IterationData extends GenericMudpPacketExchangerIterator.IterationData {

    public IterationData(final MudpPacket mudpPacket) {
      super(mudpPacket);
    }

  }

}
