/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IRawATSubChannel;
import com.flamesgroup.device.gsmb.IRawATSubChannelHandler;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FakeRawATSubChannel extends GenericByteBufferExchangerIterator<FakeRawATSubChannel.IterationData, FakeRawATSubChannel.Iteration> implements IRawATSubChannel {

  private IRawATSubChannelHandler rawATSubChannelHandler;

  public FakeRawATSubChannel() {
    this(Collections.emptyList());
  }

  public FakeRawATSubChannel(final FakeRawATSubChannel.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeRawATSubChannel(final List<FakeRawATSubChannel.Iteration> iterations) {
    super(iterations);
  }

  @Override
  public void start(final IRawATSubChannelHandler rawATSubChannelHandler) throws ChannelException {
    this.rawATSubChannelHandler = rawATSubChannelHandler;

    if (iterations.isEmpty()) {
      return;
    }

    startIterate();
  }

  @Override
  public void stop() {
    this.rawATSubChannelHandler = null;
  }

  @Override
  public void writeRawATData(final ByteBuffer atDataBuffer) throws ChannelException, InterruptedException {
    send(new IterationData(atDataBuffer));
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeRawATSubChannel.IterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {
      @Override
      public Exception call() throws Exception {
        try {
          rawATSubChannelHandler.handleRawATData(iterationData.getData());
          return null;
        } catch (Exception e) {
          return e;
        }
      }
    });
  }

  public static class Iteration extends GenericByteBufferExchangerIterator.Iteration<FakeRawATSubChannel.IterationData> {

    public Iteration(final IterationData sendIterationData, final long timeout, final IterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class IterationData extends GenericByteBufferExchangerIterator.IterationData {

    public IterationData(final ByteBuffer data) {
      super(data);
    }

    public IterationData(final byte[] data) {
      super(data);
    }

  }

}
