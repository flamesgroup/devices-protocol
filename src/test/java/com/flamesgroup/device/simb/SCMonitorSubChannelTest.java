/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SCMonitorSubChannelTest {

  @Test
  public void testHandlersStates() throws Exception {
    Semaphore handlePresentStateFalseSemaphore = new Semaphore(0);
    Semaphore handlePresentStateTrueSemaphore = new Semaphore(0);

    FakeMudpChannel.IterationData startIterationData = new FakeMudpChannel.IterationData(SCMonitorChannel.COMMAND_START_MONITOR, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData stopIterationData = new FakeMudpChannel.IterationData(SCMonitorChannel.COMMAND_STOP_MONITOR, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));

    List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeMudpChannel.Iteration(
        startIterationData,
        9,
        startIterationData));
    iterations.add(new FakeMudpChannel.Iteration(
        null,
        1,
        new FakeMudpChannel.IterationData(SCMonitorChannel.PRESENT_STATE, ByteBuffer.wrap(new byte[] {0x00}).order(ByteOrder.LITTLE_ENDIAN))));
    iterations.add(new FakeMudpChannel.Iteration(
        null,
        1,
        new FakeMudpChannel.IterationData(SCMonitorChannel.PRESENT_STATE, ByteBuffer.wrap(new byte[] {0x01}).order(ByteOrder.LITTLE_ENDIAN))));
    iterations.add(new FakeMudpChannel.Iteration(
        null,
        1,
        new FakeMudpChannel.IterationData(SCMonitorChannel.PRESENT_STATE, ByteBuffer.wrap(new byte[] {0x00}).order(ByteOrder.LITTLE_ENDIAN))));
    iterations.add(new FakeMudpChannel.Iteration(
        stopIterationData,
        0,
        stopIterationData));

    FakeMudpChannel fakeMudpChannel = new FakeMudpChannel(iterations);
    ISCMonitorChannel scMonitorChannel = new SCMonitorChannel(fakeMudpChannel, new MudpChannelAddress((byte) 0), 0);
    scMonitorChannel.enslave();
    assertTrue(scMonitorChannel.isEnslaved());
    scMonitorChannel.getSCMonitorSubChannel().start(new ISCMonitorSubChannelHandler() {

      @Override
      public void handlePresentState(final boolean presentState) {
        if (!presentState) {
          handlePresentStateFalseSemaphore.release();
        } else {
          handlePresentStateTrueSemaphore.release();
        }
      }

    });

    fakeMudpChannel.waitForIterationNumber(4);

    assertEquals(2, handlePresentStateFalseSemaphore.availablePermits());
    assertEquals(1, handlePresentStateTrueSemaphore.availablePermits());

    scMonitorChannel.getSCMonitorSubChannel().stop();
    scMonitorChannel.free();
    assertFalse(scMonitorChannel.isEnslaved());

    fakeMudpChannel.checkForException();
  }

}
