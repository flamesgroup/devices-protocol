/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.simb;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpChannel;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.simb.SCReaderState.SCReaderStatePrimary;
import com.flamesgroup.device.simb.SCReaderState.SCReaderStateSecondary;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class SCReaderSubChannelTest {

  @Test
  public void testGetState() throws Exception {
    List<FakeMudpChannel.Iteration> iterations = new ArrayList<>();
    FakeMudpChannel.IterationData startIterationData = new FakeMudpChannel.IterationData(SCReaderChannel.COMMAND_START_READER, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    iterations.add(new FakeMudpChannel.Iteration(startIterationData, 50, startIterationData));

    ByteBuffer atrBuffer = ByteBuffer.wrap(new byte[] {0x00, 0x01, 0x03}).order(ByteOrder.LITTLE_ENDIAN);
    FakeMudpChannel.IterationData atrIterationData = new FakeMudpChannel.IterationData(SCReaderChannel.APDU, atrBuffer);
    iterations.add(new FakeMudpChannel.Iteration(null, 50, atrIterationData));

    ByteBuffer bbState = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
    bbState.put(SCReaderStatePrimary.NONE.getCode());
    bbState.put(SCReaderStateSecondary.NONE.getCode());
    bbState.flip();

    FakeMudpChannel.IterationData stateSendIterationData = new FakeMudpChannel.IterationData(SCReaderChannel.COMMAND_GET_READER_AUTOMATE_STATE, ByteBuffer.allocate(0).order(ByteOrder.LITTLE_ENDIAN));
    FakeMudpChannel.IterationData stateReceiveIterationData = new FakeMudpChannel.IterationData(SCReaderChannel.COMMAND_GET_READER_AUTOMATE_STATE, bbState);
    iterations.add(new FakeMudpChannel.Iteration(stateSendIterationData, 50, stateReceiveIterationData));

    ByteBuffer bbIndicationMode = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
    bbIndicationMode.put(IndicationMode.BUSINESS_PROBLEM.getN());
    bbIndicationMode.flip();

    ISCReaderChannel channel = new SCReaderChannel(new FakeMudpChannel(iterations), new MudpChannelAddress((byte) 0), 0);
    channel.enslave();
    ATR atr = channel.getSCReaderSubChannel().start(new ISCReaderSubChannelHandler() {
      @Override
      public void handleAPDUResponse(final APDUResponse response) {
      }

      @Override
      public void handleErrorState(final SCReaderError error, final SCReaderState state) {
      }
    });
    assertTrue(channel.isEnslaved());
    assertArrayEquals(new byte[] {0x00, 0x01, 0x03}, atr.getData());

    SCReaderState state = channel.getSCReaderSubChannel().getState();
    assertNotNull(state);
    assertEquals(SCReaderStatePrimary.NONE, state.getPrimaryState());
    assertEquals(SCReaderStateSecondary.NONE, state.getSecondaryState());

    channel.free();
    assertFalse(channel.isEnslaved());
  }

}
