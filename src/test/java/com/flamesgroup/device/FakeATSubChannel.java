/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IATSubChannel;
import com.flamesgroup.device.gsmb.IATSubChannelHandler;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FakeATSubChannel extends GenericStringExchangerIterator<FakeATSubChannel.IterationData, FakeATSubChannel.Iteration> implements IATSubChannel {

  private IATSubChannelHandler atSubChannelHandler;

  public FakeATSubChannel() {
    this(Collections.emptyList());
  }

  public FakeATSubChannel(final FakeATSubChannel.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeATSubChannel(final List<Iteration> iterations) {
    super(iterations);
  }

  @Override
  public void start(final IATSubChannelHandler atSubChannelHandler) throws ChannelException {
    this.atSubChannelHandler = atSubChannelHandler;

    if (iterations.isEmpty()) {
      return;
    }

    startIterate();
  }

  @Override
  public void stop() {
    this.atSubChannelHandler = null;
  }

  @Override
  public void writeATData(final String atData) throws ChannelException, InterruptedException {
    send(new IterationData(atData));
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeATSubChannel.IterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {
      @Override
      public Exception call() throws Exception {
        try {
          atSubChannelHandler.handleATData(iterationData.getData());
          return null;
        } catch (Exception e) {
          return e;
        }
      }
    });
  }

  public static class Iteration extends GenericStringExchangerIterator.Iteration<FakeATSubChannel.IterationData> {

    public Iteration(final IterationData sendIterationData, final long timeout, final IterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

    public Iteration(final String sendData, final long timeout, final String receivedData) {
      super(sendData != null ? new IterationData(sendData) : null, timeout, receivedData != null ? new IterationData(receivedData) : null);
    }

  }

  public static class IterationData extends GenericStringExchangerIterator.IterationData {

    public IterationData(final String str) {
      super(str);
    }

  }

}
