/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import static org.junit.Assert.assertNotNull;

import com.flamesgroup.device.channel.ChannelCommandDecodeException;
import com.flamesgroup.device.channel.ChannelTimeoutException;
import org.junit.Test;

public class CommandTest {

  @Test
  public void testCommandAnswerBeforeAwait() throws Exception {
    FakeCommand command = new FakeCommand((byte) 0, new FakeEchoChannelData(0), 5);
    FakeCommandResponse response = command.execute(command.createRequest());
    assertNotNull(response);
  }

  @Test
  public void testCommandAnswerWithAwait() throws Exception {
    FakeCommand command = new FakeCommand((byte) 0, new FakeEchoChannelData(50), 200);
    FakeCommandResponse response = command.execute(command.createRequest());
    assertNotNull(response);
  }

  @Test(expected = ChannelCommandDecodeException.class)
  public void testCommandAnswerWithException() throws Exception {
    FakeCommand command = new FakeCommand((byte) 0, new FakeEchoChannelData(0), 15, true);
    command.execute(command.createRequest());
  }

  @Test(expected = ChannelTimeoutException.class)
  public void testCommandTimeout() throws Exception {
    FakeCommand command = new FakeCommand((byte) 0, new FakeEchoChannelData(30), 15, false);
    command.execute(command.createRequest());
  }

}
