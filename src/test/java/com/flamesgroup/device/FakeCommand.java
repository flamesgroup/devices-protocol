/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.Command;
import com.flamesgroup.device.channel.IChannelData;

public class FakeCommand extends Command<FakeCommandRequest, FakeCommandResponse> {

  private final boolean exceptionMode;

  public FakeCommand(final byte type, final IChannelData channelData, final long responseTimeout) {
    this(type, channelData, responseTimeout, false);
  }

  public FakeCommand(final byte type, final IChannelData channelData, final long responseTimeout, final boolean exceptionMode) {
    super(type, channelData, responseTimeout);
    this.exceptionMode = exceptionMode;
  }

  @Override
  public FakeCommandRequest createRequest() {
    return new FakeCommandRequest();
  }

  @Override
  public FakeCommandResponse createResponse() {
    return new FakeCommandResponse(exceptionMode);
  }

}
