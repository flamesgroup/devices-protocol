/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.helper.DataRepresentationHelper;
import com.flamesgroup.device.protocol.mudp.IMudpChannel;
import com.flamesgroup.device.protocol.mudp.IMudpChannelHandler;
import com.flamesgroup.device.protocol.mudp.MudpChannelAddress;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.protocol.mudp.MudpOptions;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FakeMudpChannel extends GenericByteBufferExchangerIterator<FakeMudpChannel.IterationData, FakeMudpChannel.Iteration> implements IMudpChannel {

  private IMudpChannelHandler mudpChannelHandler;

  public FakeMudpChannel() {
    this(Collections.emptyList());
  }

  public FakeMudpChannel(final FakeMudpChannel.Iteration iteration) {
    this(Collections.singletonList(iteration));
  }

  public FakeMudpChannel(final List<FakeMudpChannel.Iteration> iterations) {
    super(iterations);
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final FakeMudpChannel.IterationData iterationData) {
    return new FutureTask<>(new Callable<Exception>() {
      @Override
      public Exception call() throws Exception {
        try {
          mudpChannelHandler.handleReceive(iterationData.getDataType(), iterationData.getData());
          return null;
        } catch (Exception e) {
          return e;
        }
      }
    });
  }

  @Override
  public boolean isEnslaved() {
    return mudpChannelHandler != null;
  }

  @Override
  public void enslave(final MudpChannelAddress mudpChannelAddress, final IMudpChannelHandler mudpChannelHandler) throws MudpException, InterruptedException {
    this.mudpChannelHandler = mudpChannelHandler;

    if (iterations.isEmpty()) {
      return;
    }

    startIterate();
  }

  @Override
  public void free() throws MudpException {
    mudpChannelHandler = null;
  }

  @Override
  public void sendReliable(final byte type, final ByteBuffer data) throws MudpException, InterruptedException {
    send(new IterationData(type, data));
  }

  @Override
  public void sendUnreliable(final byte type, final ByteBuffer data) throws MudpException {
    try {
      send(new IterationData(type, data));
    } catch (InterruptedException e) {
      throw new MudpException(e);
    }
  }

  @Override
  public MudpOptions getMudpOptions() {
    return new MudpOptions(1, 500, 0, 0);
  }

  public static class Iteration extends GenericByteBufferExchangerIterator.Iteration<FakeMudpChannel.IterationData> {

    public Iteration(final IterationData sendIterationData, final long timeout, final IterationData receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class IterationData extends GenericByteBufferExchangerIterator.IterationData {

    private final byte dataType;

    public IterationData(final byte dataType, final ByteBuffer data) {
      super(data);
      this.dataType = dataType;
    }

    public byte getDataType() {
      return dataType;
    }

    @Override
    public int hashCode() {
      return super.hashCode() + dataType << 24;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
        return true;
      if (!(obj instanceof IterationData))
        return false;
      IterationData that = (IterationData) obj;
      return this.dataType == that.dataType && super.equals(that);
    }

    @Override
    public String toString() {
      return String.format("0x%02X", dataType) + " | " + DataRepresentationHelper.toHexArrayString(getData());
    }

  }

}
