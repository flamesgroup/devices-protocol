/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;

import java.util.List;

public abstract class GenericFakeSCChannel<SID extends GenericFakeSCChannel.SendIterationData, RID extends GenericFakeSCChannel.ReceiveIterationData, I extends GenericFakeSCChannel.Iteration<SID, RID>>
    extends GenericExchangerIterator<APDUCommand, APDUResponse, SID, RID, I> {

  protected GenericFakeSCChannel(final List<I> iterations) {
    super(iterations);
  }

  public static class Iteration<SID extends SendIterationData, RID extends ReceiveIterationData> extends
      GenericExchangerIterator.Iteration<SID, RID> {

    public Iteration(final SID sendIterationData, final long timeout, final RID receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class SendIterationData extends GenericExchangerIterator.IterationData<APDUCommand> {

    public SendIterationData(final APDUCommand data) {
      super(data);
    }

  }

  public static class ReceiveIterationData extends GenericExchangerIterator.IterationData<APDUResponse> {

    public ReceiveIterationData(final APDUResponse data) {
      super(data);
    }

  }

}
