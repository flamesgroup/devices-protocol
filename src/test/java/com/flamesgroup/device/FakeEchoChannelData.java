/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;

import java.nio.ByteBuffer;

public class FakeEchoChannelData implements IChannelData {

  private final int timeout;

  private IChannelDataHandler channelDataHandler;

  private boolean attached;

  public FakeEchoChannelData(final int timeout) {
    this.timeout = timeout;
  }

  @Override
  public boolean isAttached() {
    return attached;
  }

  @Override
  public void attach(final byte sendType, final IChannelDataHandler channelDataHandler) throws ChannelException {
    this.channelDataHandler = channelDataHandler;
    attached = true;
  }

  @Override
  public void detach() {
    attached = false;
  }

  @Override
  public void sendReliable(final ByteBuffer data) throws ChannelException {
    if (timeout == 0) {
      channelDataHandler.handleReceive(data);
    } else {
      new Thread(() -> {
        try {
          Thread.sleep(timeout);
        } catch (InterruptedException e) {
        }
        channelDataHandler.handleReceive(data);
      }).start();
    }
  }

  @Override
  public void sendUnreliable(final ByteBuffer data) throws ChannelException {
    sendReliable(data);
  }

}
