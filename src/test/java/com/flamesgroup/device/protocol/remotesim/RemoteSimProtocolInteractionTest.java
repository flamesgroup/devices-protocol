/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.remotesim;

import com.flamesgroup.device.FakeCMUXVirtualPort;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class RemoteSimProtocolInteractionTest {

  private final Semaphore apduCommandRequestSemaphore = new Semaphore(0);
  private APDUCommand apduCommandRequest;

  @Test
  public void checkTest() throws Exception {
    List<FakeCMUXVirtualPort.Iteration> iterations = new LinkedList<>();
    // SIM Client (module) sends Connection Request: MsgId = 0x00, Number of Parameters = 0x01, Par1 Id = 0x00 (MaxMsgSize), Parameter1 Length = 0x02, Parameter1 Value = 0x01, 0x2C (300 bytes)
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        null,
        10,
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x01, (byte) 0x2C, (byte) 0x00, (byte) 0x00})
    ));
    // SIM Server answers with Connection Response: MsgId = 0x01, Number of Parameters = 0x01, Par1 Id = 0x01 (ConnectionStatus), Parameter1 Length = 0x01, Parameter1 Value = 0x04 (OK, ongoing call)
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x01, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x00}),
        10,
        null
    ));
    // SIM Server sends Status Indication: MsgId = 0x11, Number of Parameters = 0x01, Par1 Id = 0x08 (StatusChange), Parameter1 Length = 0x01, Parameter1 Value = 0x04 (Card inserted)
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x11, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x08, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x00}),
        10,
        null
    ));
    // SIM Client (module) sends Power SIM on Request: MsgId = 0x0B, Number of Parameters = 0x00
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        null,
        10,
        new FakeCMUXVirtualPort.IterationData(new byte[] {(byte) 0x0B, (byte) 0x00, (byte) 0x00, (byte) 0x00})
    ));
    // SIM Server answers with Power SIM on Response: MsgId = 0x0C, Number of Parameters = 0x01, Par1 Id = 0x02 (ResultCode), Parameter1 Length = 0x01, Parameter1 Value = 0x00 (OK, request processed correctly)
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x0C, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}),
        10,
        null
    ));
    // SIM Client (module) sends Transfer ATR Request: MsgId = 0x07, Number of Parameters = 0x00
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        null,
        10,
        new FakeCMUXVirtualPort.IterationData(new byte[] {(byte) 0x07, (byte) 0x00, (byte) 0x00, (byte) 0x00})
    ));
    // SIM Server answers with Transfer ATR Response: MsgId = 0x08, Number of Parameters = 0x02, Par1 Id = 0x02 (ResultCode), Parameter1 Length = 0x01, Parameter1 Value = 0x00 (OK, request processed correctly),
    // Par2 Id = 0x06 (ATR), Parameter2 Length = 0x14, Parameter2 Value = 0x3B, 0xBE, 0x94, 0x00, 0x40, 0x14, 0x47, 0x47, 0x33, 0x53, 0x33, 0x45, 0x48, 0x41, 0x54, 0x4C, 0x39, 0x31, 0x30, 0x30
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x08, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x06, (byte) 0x00,
            (byte) 0x00, (byte) 0x14, (byte) 0x3B, (byte) 0xBE, (byte) 0x94, (byte) 0x00, (byte) 0x40, (byte) 0x14, (byte) 0x47, (byte) 0x47, (byte) 0x33, (byte) 0x53, (byte) 0x33, (byte) 0x45,
            (byte) 0x48, (byte) 0x41, (byte) 0x54, (byte) 0x4C, (byte) 0x39, (byte) 0x31, (byte) 0x30, (byte) 0x30}),
        10,
        null
    ));
    // SIM Client (module) sends Transfer APDU Request: MsgId = 0x05, Number of Parameters = 0x01, Par1 Id = 0x04 (CommandAPDU), Parameter1 Length = 0x07, Parameter1 Value = 0xA0, 0xA4, 0x00, 0x00, 0x02, 0x7F, 0x20
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        null,
        10,
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x05, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x07, (byte) 0xA0, (byte) 0xA4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x7F,
            (byte) 0x20, (byte) 0x00})
    ));
    // SIM Server answers with Transfer APDU Response: MsgId = 0x06, Number of Parameters = 0x02, Par1 Id = 0x02 (ResultCode), Parameter1 Length = 0x01, Parameter1 Value = 0x00 (OK, request processed correctly)
    // Par2 Id = 0x05 (ResponseAPDU), Parameter2 Length = 0x02, Parameter2 Value = 0x9F 0x19
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        new FakeCMUXVirtualPort.IterationData(new byte[] {
            (byte) 0x06, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0x00,
            (byte) 0x00, (byte) 0x02, (byte) 0x9F, (byte) 0x19, (byte) 0x00, (byte) 0x00}),
        10,
        null
    ));
    // SIM Server (module) sends Disconnection Request: MsgId = 0x02, Number of Parameters = 0x00
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        null,
        10,
        new FakeCMUXVirtualPort.IterationData(new byte[] {(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00})
    ));
    // SIM Server answers with Disconnection Response: MsgId = 0x03, Number of Parameters = 0x00
    iterations.add(new FakeCMUXVirtualPort.Iteration(
        new FakeCMUXVirtualPort.IterationData(new byte[] {(byte) 0x03, (byte) 0x00, (byte) 0x00, (byte) 0x00}),
        10,
        null
    ));

    FakeCMUXVirtualPort fakeCMUXVirtualPort = new FakeCMUXVirtualPort(iterations);
    IRemoteSimConnection remoteSimConnection = new RemoteSimConnection(fakeCMUXVirtualPort);
    remoteSimConnection.connect(new ATR(new byte[] {
            (byte) 0x3B, (byte) 0xBE, (byte) 0x94, (byte) 0x00, (byte) 0x40, (byte) 0x14, (byte) 0x47, (byte) 0x47, (byte) 0x33, (byte) 0x53, (byte) 0x33, (byte) 0x45, (byte) 0x48, (byte) 0x41,
            (byte) 0x54, (byte) 0x4C, (byte) 0x39, (byte) 0x31, (byte) 0x30, (byte) 0x30}),
        new IRemoteSimConnectionHandler() {
          @Override
          public void handleAPDUCommand(final APDUCommand apduCommand) {
            apduCommandRequest = apduCommand;
            apduCommandRequestSemaphore.release();
          }
        });

    fakeCMUXVirtualPort.waitForIterationNumber(8);

    remoteSimConnection.transferAPDUResponse(APDUResponse.decoding(ByteBuffer.wrap(new byte[] {(byte) 0x9F, 0x19})));

    apduCommandRequestSemaphore.acquire();

    Assert.assertEquals(APDUCommand.decoding(ByteBuffer.wrap(new byte[] {(byte) 0xA0, (byte) 0xA4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x7F, (byte) 0x20})), apduCommandRequest);

    fakeCMUXVirtualPort.checkForException();
  }

}
