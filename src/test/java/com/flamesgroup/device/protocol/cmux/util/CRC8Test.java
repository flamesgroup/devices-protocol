/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CRC8Test {

  @Test
  public void checkTest() {
    // DLC establishment on Control Channel
    byte address = 0x03;
    byte control = 0x3F;
    short lengthIndicator = 0x01;

    assertEquals((byte) 0x1C, CRC8.checkTransmitterCode(address, control, lengthIndicator));
    assertTrue(CRC8.checkReceiverCode((byte) 0x1C, address, control, lengthIndicator));
  }

}
