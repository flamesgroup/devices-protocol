/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class UIHCommandTest {

  @Test
  public void decodeUIHCommandWithMaxValueLengthTest() throws Exception {
    short valueLength = UIHCommand.MAX_VALUE_LENGTH;
    byte type = (byte) (0x08 << 2);
    short lengthIndicator = (short) (valueLength << 1);

    ByteBuffer bb = ByteBuffer.allocate(300).order(ByteOrder.LITTLE_ENDIAN);

    bb.put(type);
    bb.putShort(lengthIndicator);
    for (int i = 0; i < valueLength; i++) {
      bb.put((byte) 0);
    }
    bb.rewind();

    UIHCommand uihCommand = UIHCommand.decoding(bb);

    assertFalse(uihCommand.isTypeEA());
    assertFalse(uihCommand.isTypeCR());
    assertEquals(UIHCommandType.TEST, uihCommand.getCommandType());
    assertEquals(valueLength, uihCommand.getValue().remaining());

  }

  @Test
  public void decodeRealUIHCommandWithMSCTest() throws Exception {
    ByteBuffer bb =
        ByteBuffer.wrap(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE1, (byte) 0x05, (byte) 0x07, (byte) 0x0C, (byte) 0xFB, (byte) 0xF9}).order(ByteOrder.LITTLE_ENDIAN);

    CMUXFrame cmuxFrame = CMUXFrame.decoding(bb);

    assertTrue(cmuxFrame.isAddressEA());
    assertTrue(cmuxFrame.isAddressCR());
    assertEquals(cmuxFrame.getAddressDLCI(), 0);

    assertFalse(cmuxFrame.isControlPF());
    assertEquals(cmuxFrame.getControlFrameType(), FrameType.UIH);

    assertEquals(cmuxFrame.getInformationField().remaining(), 4);

    UIHCommand uihCommand = UIHCommand.decoding(cmuxFrame.getInformationField());

    assertEquals(uihCommand.getCommandType(), UIHCommandType.MSC);

    UIHCommandMSC uihCommandMSC = UIHCommandMSC.decoding(uihCommand.getValue());

    assertEquals(uihCommandMSC.getAddressDLCI(), 1);
    assertFalse(uihCommandMSC.getModemStatus().isDCD());
    assertFalse(uihCommandMSC.getModemStatus().isRING());
    assertTrue(uihCommandMSC.getModemStatus().isCTS());
    assertTrue(uihCommandMSC.getModemStatus().isDSR());
    assertFalse(uihCommandMSC.getModemStatus().isFC());
  }

}
