/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.protocol.cmux.util.CRC8;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public class CMUXFrameTest {

  @Test
  public void decodeRealCMUXFrameWithCorrectDateTest() throws Exception {
    ByteBuffer bb = ByteBuffer.wrap(
        new byte[] {(byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0xC9, (byte) 0x0D, (byte) 0x0A, (byte) 0x2B, (byte) 0x43, (byte) 0x55, (byte) 0x53, (byte) 0x44, (byte) 0x3A, (byte) 0x20,
            (byte) 0x32, (byte) 0x2C, (byte) 0x22, (byte) 0x42, (byte) 0x61, (byte) 0x6C, (byte) 0x61, (byte) 0x6E, (byte) 0x73, (byte) 0x20, (byte) 0x30, (byte) 0x2E, (byte) 0x31, (byte) 0x30,
            (byte) 0x68, (byte) 0x72, (byte) 0x6E, (byte) 0x2C, (byte) 0x20, (byte) 0x62, (byte) 0x6F, (byte) 0x6E, (byte) 0x75, (byte) 0x73, (byte) 0x20, (byte) 0x30, (byte) 0x2E, (byte) 0x30,
            (byte) 0x30, (byte) 0x68, (byte) 0x72, (byte) 0x6E, (byte) 0x2E, (byte) 0x20, (byte) 0x44, (byte) 0x65, (byte) 0x74, (byte) 0x61, (byte) 0x6C, (byte) 0x69, (byte) 0x3A, (byte) 0x20,
            (byte) 0x2A, (byte) 0x31, (byte) 0x32, (byte) 0x31, (byte) 0x23, (byte) 0x0A, (byte) 0x2A, (byte) 0x2A, (byte) 0x2A, (byte) 0x0A, (byte) 0x4F, (byte) 0x74, (byte) 0x72, (byte) 0x79,
            (byte) 0x6D, (byte) 0x61, (byte) 0x79, (byte) 0x74, (byte) 0x65, (byte) 0x20, (byte) 0x31, (byte) 0x30, (byte) 0x67, (byte) 0x72, (byte) 0x6E, (byte) 0x20, (byte) 0x7A, (byte) 0x20,
            (byte) 0x6C, (byte) 0x69, (byte) 0x66, (byte) 0x65, (byte) 0x3A, (byte) 0x29, (byte) 0x50, (byte) 0x6F, (byte) 0x7A, (byte) 0x79, (byte) 0x6B, (byte) 0x61, (byte) 0x2E, (byte) 0x20,
            (byte) 0x5A, (byte) 0x61, (byte) 0x6D, (byte) 0x6F, (byte) 0x76, (byte) 0x79, (byte) 0x74, (byte) 0xC8, (byte) 0xF9, (byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0x25, (byte) 0x79,
            (byte) 0x20, (byte) 0x2A, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x2A, (byte) 0x31, (byte) 0x32, (byte) 0x31, (byte) 0x32, (byte) 0x23, (byte) 0x22, (byte) 0x2C, (byte) 0x31,
            (byte) 0x35, (byte) 0x0D, (byte) 0x0A, (byte) 0x69, (byte) 0xF9}).order(ByteOrder.LITTLE_ENDIAN);

    CMUXFrame cmuxFrame = CMUXFrame.decoding(bb);

    assertEquals((byte) 0x01, cmuxFrame.getAddressDLCI());
    assertTrue(cmuxFrame.isAddressEA());
    assertFalse(cmuxFrame.isAddressCR());
    assertEquals(FrameType.UIH, cmuxFrame.getControlFrameType());
    assertFalse(cmuxFrame.isControlPF());
    assertEquals("\r\n+CUSD: 2,\"Balans 0.10hrn, bonus 0.00hrn. Detali: *121#\n***\nOtrymayte 10grn z life:)Pozyka. Zamovyt",
        Charset.forName("ISO-8859-1").decode(cmuxFrame.getInformationField()).toString());
  }

  @Test
  public void decodeCMUXFrameWithMaxInformationFieldLengthTest() throws Exception {
    short informationFieldLength = CMUXFrame.MAX_INFORMATION_FIELD_LENGTH;
    byte address = (byte) 0;
    byte control = (byte) 0b11101111;
    short lengthIndicator = (short) (informationFieldLength << 1);

    ByteBuffer bb = ByteBuffer.allocate(300).order(ByteOrder.LITTLE_ENDIAN);

    bb.put((byte) 0xF9);
    bb.put(address);
    bb.put(control);
    bb.putShort(lengthIndicator);
    for (int i = 0; i < informationFieldLength; i++) {
      bb.put((byte) 0);
    }
    bb.put(CRC8.checkTransmitterCode(address, control, lengthIndicator));
    bb.put((byte) 0xF9);
    bb.rewind();

    CMUXFrame cmuxFrame = CMUXFrame.decoding(bb);

    assertEquals(FrameType.UIH, cmuxFrame.getControlFrameType());
    assertEquals(informationFieldLength, cmuxFrame.getInformationField().remaining());
  }

  @Test
  public void decodeRealCMUXFrameWithMalformedData1() throws Exception {
    ByteBuffer bb = ByteBuffer.wrap(new byte[] {
        (byte) 0x33, (byte) 0x35, (byte) 0x22, (byte) 0x2C, (byte) 0x31, (byte) 0x32, (byte) 0x39, (byte) 0x2C, (byte) 0x22, (byte) 0x22, (byte) 0x0D, (byte) 0x0A, (byte) 0x0D, (byte) 0x0A,
        (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A, (byte) 0x19, (byte) 0xF9,
        (byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0x23, (byte) 0x0D, (byte) 0x0A, (byte) 0x2B, (byte) 0x43, (byte) 0x49, (byte) 0x45, (byte) 0x56, (byte) 0x3A, (byte) 0x20, (byte) 0x72,
        (byte) 0x73, (byte) 0x73, (byte) 0x69, (byte) 0x2C, (byte) 0x33, (byte) 0x0D, (byte) 0x0A, (byte) 0x8D, (byte) 0xF9
    }).order(ByteOrder.LITTLE_ENDIAN);

    boolean exceptionFired = false;
    try {
      CMUXFrame.decoding(bb);
    } catch (CMUXFrameCodingException e) {
      exceptionFired = true;
    } finally {
      assertTrue(exceptionFired);
    }

    CMUXFrame cmuxFrame = CMUXFrame.decoding(bb);

    assertEquals((byte) 0x01, cmuxFrame.getAddressDLCI());
    assertTrue(cmuxFrame.isAddressEA());
    assertFalse(cmuxFrame.isAddressCR());
    assertEquals(FrameType.UIH, cmuxFrame.getControlFrameType());
    assertFalse(cmuxFrame.isControlPF());
    assertEquals("\r\n+CIEV: rssi,3\r\n",
        Charset.forName("ISO-8859-1").decode(cmuxFrame.getInformationField()).toString());
  }

  @Test
  public void decodeRealCMUXFrameWithMalformedData2() throws Exception {
    ByteBuffer bb = ByteBuffer.wrap(new byte[] {
        (byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0x1D, (byte) 0x41, (byte) 0x54, (byte) 0x2B, (byte) 0x43, (byte) 0x4C, (byte) 0x43, (byte) 0x43, (byte) 0x0D, (byte) 0x0D, (byte) 0x0A,
        (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A, (byte) 0x43, (byte) 0xF9,
        (byte) 0xF9,
        (byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0x1D, (byte) 0x41, (byte) 0x54, (byte) 0x2B, (byte) 0x43, (byte) 0x4C, (byte) 0x43, (byte) 0x43, (byte) 0x0D, (byte) 0x0D, (byte) 0x0A,
        (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A, (byte) 0x43, (byte) 0xF9
    }).order(ByteOrder.LITTLE_ENDIAN);

    CMUXFrame[] cmuxFrames = new CMUXFrame[2];

    cmuxFrames[0] = CMUXFrame.decoding(bb);

    boolean exceptionFired = false;
    try {
      CMUXFrame.decoding(bb);
    } catch (CMUXFrameCodingException e) {
      e.printStackTrace();
      exceptionFired = true;
    } finally {
      assertTrue(exceptionFired);
    }

    cmuxFrames[1] = CMUXFrame.decoding(bb);

    for (CMUXFrame cmuxFrame : cmuxFrames) {
      assertEquals((byte) 0x01, cmuxFrame.getAddressDLCI());
      assertTrue(cmuxFrame.isAddressEA());
      assertFalse(cmuxFrame.isAddressCR());
      assertEquals(FrameType.UIH, cmuxFrame.getControlFrameType());
      assertFalse(cmuxFrame.isControlPF());
      assertEquals("AT+CLCC\r\r\nOK\r\n",
          Charset.forName("ISO-8859-1").decode(cmuxFrame.getInformationField()).toString());
    }
  }

  @Test
  public void decodeRealCMUXFrameWithMalformedData3() throws Exception {
    ByteBuffer bb = ByteBuffer.wrap(new byte[] {
        (byte) 0xF9, (byte) 0x09, (byte) 0xEF, (byte) 0x21, (byte) 0x05, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0xA0, (byte) 0xB2,
        (byte) 0xBD, (byte) 0x04, (byte) 0x1E, (byte) 0x00, (byte) 0x00, (byte) 0xF9,
        (byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0x69, (byte) 0x0D, (byte) 0x0A, (byte) 0x2B, (byte) 0x43, (byte) 0x4C, (byte) 0x43, (byte) 0x43, (byte) 0x3A, (byte) 0x20, (byte) 0x31,
        (byte) 0x2C, (byte) 0x30, (byte) 0x2C, (byte) 0x33, (byte) 0x2C, (byte) 0x30, (byte) 0x2C, (byte) 0x30, (byte) 0x2C, (byte) 0x22, (byte) 0x31, (byte) 0x33, (byte) 0x31, (byte) 0x30,
        (byte) 0x36, (byte) 0x33, (byte) 0x39, (byte) 0x31, (byte) 0x30, (byte) 0x38, (byte) 0x37, (byte) 0x36, (byte) 0x30, (byte) 0x38, (byte) 0x30, (byte) 0x33, (byte) 0x22, (byte) 0x2C,
        (byte) 0x31, (byte) 0x32, (byte) 0x39, (byte) 0x2C, (byte) 0x22, (byte) 0x22, (byte) 0x0D, (byte) 0x0A, (byte) 0x0D, (byte) 0x0A, (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A,
        (byte) 0x10, (byte) 0xF9
    }).order(ByteOrder.LITTLE_ENDIAN);

    boolean exceptionFired1 = false;
    try {
      CMUXFrame.decoding(bb);
    } catch (CMUXFrameCodingException e) {
      e.printStackTrace();
      exceptionFired1 = true;
    } finally {
      assertTrue(exceptionFired1);
    }
    boolean exceptionFired2 = false;
    try {
      CMUXFrame.decoding(bb);
    } catch (CMUXFrameCodingException e) {
      e.printStackTrace();
      exceptionFired2 = true;
    } finally {
      assertTrue(exceptionFired2);
    }

    CMUXFrame cmuxFrame = CMUXFrame.decoding(bb);

    assertEquals((byte) 0x01, cmuxFrame.getAddressDLCI());
    assertTrue(cmuxFrame.isAddressEA());
    assertFalse(cmuxFrame.isAddressCR());
    assertEquals(FrameType.UIH, cmuxFrame.getControlFrameType());
    assertFalse(cmuxFrame.isControlPF());
    assertEquals("\r\n+CLCC: 1,0,3,0,0,\"1310639108760803\",129,\"\"\r\n\r\nOK\r\n",
        Charset.forName("ISO-8859-1").decode(cmuxFrame.getInformationField()).toString());
  }

}
