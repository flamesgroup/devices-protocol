/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cmux;

import com.flamesgroup.device.FakeRawATSubChannel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class CMUXProtocolInteractionTest {

  private Semaphore atAnswerSemaphore;
  private String atAnswer;

  @Before
  public void init() {
    atAnswerSemaphore = new Semaphore(0);
    atAnswer = null;
  }

  @Test
  public void telitCMUXImplementationUserGuideDataTest() throws Exception {
    List<FakeRawATSubChannel.Iteration> iterations = new LinkedList<>();
    // HDLC establishment on Control Channel
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0x3F, (byte) 0x01, (byte) 0x1C, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0x73, (byte) 0x01, (byte) 0xD7, (byte) 0xF9})));
    // HDLC establishment on Virtual Channel #2 (Open Virtual Port #2):
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x0B, (byte) 0x3F, (byte) 0x01, (byte) 0x59, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x0B, (byte) 0x73, (byte) 0x01, (byte) 0x92, (byte) 0xF9})));
    // User Application sends the module the Virtual V.24 Signal status concerning its Logical Port (COMx) connected to Virtual Channel #2: FC=0, RTS=0, DTR=1
    // Module answers with the just received Virtual V.24 Signal status. C/R bit belonging to the TYPE octet is negated
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE3, (byte) 0x05, (byte) 0x0B, (byte) 0x0D, (byte) 0xFB, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE1, (byte) 0x05, (byte) 0x0B, (byte) 0x0D, (byte) 0xFB, (byte) 0xF9})));
    // Module sends its Virtual V.24 Signal status concerning Virtual Port #2: FC=0, DSR=1, CTS=1, RING=1, DCD=0.
    iterations.add(new FakeRawATSubChannel.Iteration(
        null,
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE3, (byte) 0x05, (byte) 0x0B, (byte) 0x4D, (byte) 0xFB, (byte) 0xF9})));
    // User Application answers with the just received Virtual V.24 Signal status. C/R bit belonging to the TYPE octet is negated.
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE1, (byte) 0x05, (byte) 0x0B, (byte) 0x4D, (byte) 0xFB, (byte) 0xF9}),
        10,
        null));
    // User Application sends the AT Command: AT+CGMR
    // Module answers the AT command result: <CR><LF>07.02.504<CR><LF><CR><LF><OK>
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {
            (byte) 0xF9, (byte) 0x0B, (byte) 0xEF, (byte) 0x11, (byte) 0x41, (byte) 0x54, (byte) 0x2B, (byte) 0x43, (byte) 0x47, (byte) 0x4D, (byte) 0x52, (byte) 0x0D, (byte) 0xAC, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {
            (byte) 0xF9, (byte) 0x09, (byte) 0xEF, (byte) 0x27, (byte) 0x0D, (byte) 0x0A, (byte) 0x30, (byte) 0x37, (byte) 0x2E, (byte) 0x30, (byte) 0x32, (byte) 0x2E, (byte) 0x35, (byte) 0x30,
            (byte) 0x34, (byte) 0x0D, (byte) 0x0A, (byte) 0x0D, (byte) 0x0A, (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A, (byte) 0x0D, (byte) 0xF9})));

    // Disconnect HDLC establishment on Virtual Channel #2 (Disconnect from Virtual Port #2)
    // After DISC command sending UIH frame is received instead of UA frame which is received after receiving UIH frame, it's for testing real situation
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x0b, (byte) 0x53, (byte) 0x01, (byte) 0xb8, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {
            (byte) 0xF9, (byte) 0x09, (byte) 0xEF, (byte) 0x21, (byte) 0x05, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x07, (byte) 0xA0, (byte) 0xA4,
            (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x6F, (byte) 0x7E, (byte) 0x00, (byte) 0xE9, (byte) 0xF9})));
    iterations.add(new FakeRawATSubChannel.Iteration(
        null,
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x0b, (byte) 0x73, (byte) 0x01, (byte) 0x92, (byte) 0xF9})));

    FakeRawATSubChannel fakeRawATSubChannel = new FakeRawATSubChannel(iterations);

    ICMUXConnection cmuxConnection = new CMUXConnection(fakeRawATSubChannel, 500);

    cmuxConnection.connect();

    ICMUXVirtualPort vp = cmuxConnection.getVirtualPorts().get(1);

    vp.open(new ICMUXVirtualPortHandler() {
      @Override
      public void handleData(final ByteBuffer data) {
        atAnswer = Charset.forName("ISO-8859-1").decode(data).toString();
        atAnswerSemaphore.release();
      }
    });

    fakeRawATSubChannel.waitForIterationNumber(5);

    vp.sendData(Charset.forName("ISO-8859-1").encode("AT+CGMR\r"));

    atAnswerSemaphore.acquire();

    Assert.assertEquals("\r\n07.02.504\r\n\r\nOK\r\n", atAnswer);

    vp.close();

    fakeRawATSubChannel.checkForException();
  }

  @Test
  public void malformedDataTest() throws Exception {
    List<FakeRawATSubChannel.Iteration> iterations = new LinkedList<>();
    // HDLC establishment on Control Channel
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0x3F, (byte) 0x01, (byte) 0x1C, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0x73, (byte) 0x01, (byte) 0xD7, (byte) 0xF9})));
    // HDLC establishment on Virtual Channel #1 (Open Virtual Port #1):
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x07, (byte) 0x3F, (byte) 0x01, (byte) 0xDE, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x07, (byte) 0x73, (byte) 0x01, (byte) 0x15, (byte) 0xF9})));
    // User Application sends the module the Virtual V.24 Signal status concerning its Logical Port (COMx) connected to Virtual Channel #2: FC=0, RTS=0, DTR=1
    // Module answers with the just received Virtual V.24 Signal status. C/R bit belonging to the TYPE octet is negated
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE3, (byte) 0x05, (byte) 0x07, (byte) 0x0D, (byte) 0xFB, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x03, (byte) 0xEF, (byte) 0x09, (byte) 0xE1, (byte) 0x05, (byte) 0x07, (byte) 0x0D, (byte) 0xFB, (byte) 0xF9})));

    // Malformed Data
    iterations.add(new FakeRawATSubChannel.Iteration(
        null,
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {
            (byte) 0x33, (byte) 0x35, (byte) 0x22, (byte) 0x2C, (byte) 0x31, (byte) 0x32, (byte) 0x39, (byte) 0x2C, (byte) 0x22, (byte) 0x22, (byte) 0x0D, (byte) 0x0A, (byte) 0x0D,
            (byte) 0x0A, (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A, (byte) 0x19, (byte) 0xF9})));
    iterations.add(new FakeRawATSubChannel.Iteration(
        null,
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9})));
    iterations.add(new FakeRawATSubChannel.Iteration(
        null,
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {
            (byte) 0xF9, (byte) 0x09, (byte) 0xEF, (byte) 0x21, (byte) 0x05, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0xA0, (byte) 0xB2,
            (byte) 0xBD, (byte) 0x04, (byte) 0x1E, (byte) 0x00, (byte) 0x00, (byte) 0xF9,
            (byte) 0xF9, (byte) 0x05, (byte) 0xEF, (byte) 0x69, (byte) 0x0D, (byte) 0x0A, (byte) 0x2B, (byte) 0x43, (byte) 0x4C, (byte) 0x43, (byte) 0x43, (byte) 0x3A, (byte) 0x20, (byte) 0x31,
            (byte) 0x2C, (byte) 0x30, (byte) 0x2C, (byte) 0x33, (byte) 0x2C, (byte) 0x30, (byte) 0x2C, (byte) 0x30, (byte) 0x2C, (byte) 0x22, (byte) 0x31, (byte) 0x33, (byte) 0x31, (byte) 0x30,
            (byte) 0x36, (byte) 0x33, (byte) 0x39, (byte) 0x31, (byte) 0x30, (byte) 0x38, (byte) 0x37, (byte) 0x36, (byte) 0x30, (byte) 0x38, (byte) 0x30, (byte) 0x33, (byte) 0x22, (byte) 0x2C,
            (byte) 0x31, (byte) 0x32, (byte) 0x39, (byte) 0x2C, (byte) 0x22, (byte) 0x22, (byte) 0x0D, (byte) 0x0A, (byte) 0x0D, (byte) 0x0A, (byte) 0x4F, (byte) 0x4B, (byte) 0x0D, (byte) 0x0A,
            (byte) 0x10, (byte) 0xF9})));

    // Disconnect HDLC establishment on Virtual Channel #1 (Disconnect from Virtual Port #1)
    // After DISC command sending UIH frame is received instead of UA frame which is received after receiving UIH frame, it's for testing real situation
    iterations.add(new FakeRawATSubChannel.Iteration(
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x07, (byte) 0x53, (byte) 0x01, (byte) 0x3F, (byte) 0xF9}),
        10,
        new FakeRawATSubChannel.IterationData(new byte[] {(byte) 0xF9, (byte) 0x07, (byte) 0x73, (byte) 0x01, (byte) 0x15, (byte) 0xF9})));

    FakeRawATSubChannel fakeRawATSubChannel = new FakeRawATSubChannel(iterations);

    ICMUXConnection cmuxConnection = new CMUXConnection(fakeRawATSubChannel, 500);

    cmuxConnection.connect();

    ICMUXVirtualPort vp = cmuxConnection.getVirtualPorts().get(0);

    vp.open(new ICMUXVirtualPortHandler() {
      @Override
      public void handleData(final ByteBuffer data) {
        atAnswer = Charset.forName("ISO-8859-1").decode(data).toString();
        atAnswerSemaphore.release();
      }
    });

    atAnswerSemaphore.acquire();

    Assert.assertEquals("\r\n+CLCC: 1,0,3,0,0,\"1310639108760803\",129,\"\"\r\n\r\nOK\r\n", atAnswer);

    vp.close();

    fakeRawATSubChannel.checkForException();
  }

}
