/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public abstract class MudpTest {

  protected final byte version = MudpConnection.MUDP_PROTOCOL_VERSION;

  protected ByteBuffer createEEData(final MudpOptions mudpOptions) {
    ByteBuffer eeBuffer = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN);
    eeBuffer.clear();
    eeBuffer.put((byte) mudpOptions.getAttemptCount());
    eeBuffer.putShort((short) mudpOptions.getAttemptTimeout());
    eeBuffer.putShort((short) mudpOptions.getRetransmitTimeout());
    eeBuffer.flip();
    return eeBuffer;
  }

  protected ByteBuffer createData(final int capacity) {
    ByteBuffer data = ByteBuffer.allocate(capacity).order(ByteOrder.LITTLE_ENDIAN);
    byte b = 0;
    while (data.hasRemaining()) {
      data.put(b++);
    }
    data.flip();
    return data;
  }

  protected ByteBuffer createData() {
    return createData(10);
  }

  static protected class DummyMudpChannelHandler implements IMudpChannelHandler {
    @Override
    public void handleReceive(final byte type, final ByteBuffer data) {
    }
  }

}
