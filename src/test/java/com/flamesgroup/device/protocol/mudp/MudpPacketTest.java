/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class MudpPacketTest {

  public MudpPacketTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void flagsTest() {
    MudpPacket mudpPacket = new MudpPacket();

    assertFalse(mudpPacket.isMSFlag());
    assertTrue(mudpPacket.setMSFlag(true).isMSFlag());
    assertFalse(mudpPacket.setMSFlag(false).isMSFlag());

    assertFalse(mudpPacket.isEEFlag());
    assertTrue(mudpPacket.setEEFlag(true).isEEFlag());
    assertFalse(mudpPacket.setEEFlag(false).isEEFlag());

    assertFalse(mudpPacket.isACKFlag());
    assertTrue(mudpPacket.setACKFlag(true).isACKFlag());
    assertFalse(mudpPacket.setACKFlag(false).isACKFlag());

    assertFalse(mudpPacket.isERRFlag());
    assertTrue(mudpPacket.setERRFlag(true).isERRFlag());
    assertFalse(mudpPacket.setERRFlag(false).isERRFlag());

    assertFalse(mudpPacket.isREFlag());
    assertTrue(mudpPacket.setREFlag(true).isREFlag());
    assertFalse(mudpPacket.setREFlag(false).isREFlag());

    assertEquals(mudpPacket.getSequence(), 0);
    assertEquals(mudpPacket.setSequence((byte) 0).getSequence(), 0);
    assertEquals(mudpPacket.setSequence((byte) 1).getSequence(), 1);
    assertEquals(mudpPacket.setSequence((byte) 2).getSequence(), 0);
    assertEquals(mudpPacket.setSequence((byte) 3).getSequence(), 1);
  }

  @Test
  public void flagsCombinationTest() {
    MudpPacket mudpPacket = new MudpPacket();

    assertFalse(mudpPacket.isMSFlag());
    assertFalse(mudpPacket.isEEFlag());
    assertFalse(mudpPacket.isACKFlag());
    assertFalse(mudpPacket.isERRFlag());
    assertFalse(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 0);

    mudpPacket.setSequence((byte) 1);

    assertFalse(mudpPacket.isMSFlag());
    assertFalse(mudpPacket.isEEFlag());
    assertFalse(mudpPacket.isACKFlag());
    assertFalse(mudpPacket.isERRFlag());
    assertFalse(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 1);

    mudpPacket.setSequence((byte) 0);

    assertFalse(mudpPacket.isMSFlag());
    assertFalse(mudpPacket.isEEFlag());
    assertFalse(mudpPacket.isACKFlag());
    assertFalse(mudpPacket.isERRFlag());
    assertFalse(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 0);

    mudpPacket.setMSFlag(true).setEEFlag(true).setACKFlag(true).setERRFlag(true).setREFlag(true);

    assertTrue(mudpPacket.isMSFlag());
    assertTrue(mudpPacket.isEEFlag());
    assertTrue(mudpPacket.isACKFlag());
    assertTrue(mudpPacket.isERRFlag());
    assertTrue(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 0);

    mudpPacket.setSequence((byte) 1);

    assertTrue(mudpPacket.isMSFlag());
    assertTrue(mudpPacket.isEEFlag());
    assertTrue(mudpPacket.isACKFlag());
    assertTrue(mudpPacket.isERRFlag());
    assertTrue(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 1);

    mudpPacket.setSequence((byte) 0);

    assertTrue(mudpPacket.isMSFlag());
    assertTrue(mudpPacket.isEEFlag());
    assertTrue(mudpPacket.isACKFlag());
    assertTrue(mudpPacket.isERRFlag());
    assertTrue(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 0);

    mudpPacket.clearFlags();

    assertFalse(mudpPacket.isMSFlag());
    assertFalse(mudpPacket.isEEFlag());
    assertFalse(mudpPacket.isACKFlag());
    assertFalse(mudpPacket.isERRFlag());
    assertFalse(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 0);

    mudpPacket.setSequence((byte) 1).clearFlags();

    assertFalse(mudpPacket.isMSFlag());
    assertFalse(mudpPacket.isEEFlag());
    assertFalse(mudpPacket.isACKFlag());
    assertFalse(mudpPacket.isERRFlag());
    assertFalse(mudpPacket.isREFlag());
    assertEquals(mudpPacket.getSequence(), 1);
  }

  @Test
  public void packetWithEEReadWriteTest() throws MudpPacketWriteException, MudpPacketReadException {
    ByteBuffer expect_bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 0x11);
    expect_bb.put((byte) (0x80 | 0x40));
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.putShort((short) 5);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 2);
    expect_bb.put((byte) 3);
    expect_bb.put((byte) 4);
    expect_bb.put((byte) 5);
    expect_bb.flip();

    MudpPacket mp1 = new MudpPacket();
    mp1.setVersion((byte) 1).setChannel((byte) 0x11).setMSFlag(true).setEEFlag(true);
    mp1.getData().clear();
    mp1.getData().put((byte) 1).put((byte) 2).put((byte) 3).put((byte) 4).put((byte) 5);
    mp1.getData().flip();

    ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    MudpPacket.encoding(mp1, bb);
    bb.flip();

    assertEquals(expect_bb, bb);

    MudpPacket mp2 = MudpPacket.decoding(bb);

    assertEquals(mp1, mp2);
  }

  @Test
  public void packetWithACKReadWriteTest() throws MudpPacketWriteException, MudpPacketReadException {
    ByteBuffer expect_bb = ByteBuffer.allocate(MudpPacket.SERVICE_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 0x22);
    expect_bb.put((byte) (0x80 | 0x20));
    expect_bb.put((byte) 0);
    expect_bb.flip();

    MudpPacket mp1 = new MudpPacket();
    mp1.setVersion((byte) 1).setChannel((byte) 0x22).setMSFlag(true).setACKFlag(true);

    ByteBuffer bb = ByteBuffer.allocate(MudpPacket.SERVICE_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    MudpPacket.encoding(mp1, bb);
    bb.flip();

    assertEquals(expect_bb, bb);

    MudpPacket mp2 = MudpPacket.decoding(bb);

    assertEquals(mp1, mp2);
  }

  @Test
  public void packetWithNotREDataReadWriteTest() throws MudpPacketWriteException, MudpPacketReadException {
    ByteBuffer expect_bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 0xBB);
    expect_bb.put((byte) (0x80));
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.putShort((short) 3);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 2);
    expect_bb.put((byte) 3);
    expect_bb.flip();

    MudpPacket mp1 = new MudpPacket();
    mp1.setVersion((byte) 1).setChannel((byte) 0xBB).setMSFlag(true);
    mp1.getData().clear();
    mp1.getData().put((byte) 1).put((byte) 2).put((byte) 3);
    mp1.getData().flip();

    ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    MudpPacket.encoding(mp1, bb);
    bb.flip();

    assertEquals(expect_bb, bb);

    MudpPacket mp2 = MudpPacket.decoding(bb);

    assertEquals(mp1, mp2);
  }

  @Test
  public void packetWithREDataReadWriteTest() throws MudpPacketWriteException, MudpPacketReadException {
    ByteBuffer expect_bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 0xCC);
    expect_bb.put((byte) (0x80 | 0x02));
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.putShort((short) 4);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 2);
    expect_bb.put((byte) 3);
    expect_bb.put((byte) 4);
    expect_bb.flip();

    MudpPacket mp1 = new MudpPacket();
    mp1.setVersion((byte) 1).setChannel((byte) 0xCC).setMSFlag(true).setREFlag(true);
    mp1.getData().clear();
    mp1.getData().put((byte) 1).put((byte) 2).put((byte) 3).put((byte) 4);
    mp1.getData().flip();

    ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    MudpPacket.encoding(mp1, bb);
    bb.flip();

    assertEquals(expect_bb, bb);

    MudpPacket mp2 = MudpPacket.decoding(bb);

    assertEquals(mp1, mp2);
  }

  @Test
  public void packetWithDataDataReadWriteTest() throws MudpPacketWriteException, MudpPacketReadException {
    ByteBuffer expect_bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 0xDD);
    expect_bb.put((byte) (0x00 | 0x02));
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.putShort((short) 260);
    for (int i = 0; i < 260; i++) {
      expect_bb.put((byte) i);
    }
    expect_bb.flip();

    MudpPacket mp1 = new MudpPacket();
    mp1.setVersion((byte) 1).setChannel((byte) 0xDD).setMSFlag(false).setREFlag(true);
    mp1.getData().clear();
    for (int i = 0; i < 260; i++) {
      mp1.getData().put((byte) i);
    }
    mp1.getData().flip();

    ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    MudpPacket.encoding(mp1, bb);
    bb.flip();

    assertEquals(expect_bb, bb);

    MudpPacket mp2 = MudpPacket.decoding(bb);

    assertEquals(mp1, mp2);
  }

  @Test
  public void packetMasqueradingReadWriteTest() throws MudpPacketWriteException, MudpPacketReadException {
    ByteBuffer expect_bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    expect_bb.put((byte) 1);
    expect_bb.put((byte) 0x01);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.put((byte) 0);
    expect_bb.putShort((short) 1);
    expect_bb.put((byte) 1);
    expect_bb.flip();

    MudpPacket mp1 = new MudpPacket();
    mp1.setVersion((byte) 1).setChannel((byte) 0x81);
    mp1.getData().clear();
    mp1.getData().put((byte) 1);
    mp1.getData().flip();

    ByteBuffer bb = ByteBuffer.allocate(MudpPacket.DATA_PACKET_MAX_LENGTH).order(ByteOrder.LITTLE_ENDIAN);
    MudpPacket.encoding(mp1, bb, channel -> (byte) (~0x80 & channel));
    bb.flip();

    assertEquals(expect_bb, bb);

    MudpPacket mp2 = MudpPacket.decoding(bb, channel -> (byte) (0x80 | channel));

    assertEquals(mp1, mp2);
  }

}
