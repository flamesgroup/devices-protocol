/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpStream;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MudpSendReliableTest extends MudpTest {

  @Test
  public void processOfNoLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1).setSequence((byte) 0),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setSequence((byte) 0)));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 2).setSequence((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setSequence((byte) 1)));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 3).setSequence((byte) 2),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setSequence((byte) 2)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    assertTrue(channel.isEnslaved());

    channel.sendReliable((byte) 1, createData());
    channel.sendReliable((byte) 2, createData());
    channel.sendReliable((byte) 3, createData());

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

  @Test
  public void processOfRequestOrAckLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(3, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    assertTrue(channel.isEnslaved());

    channel.sendReliable((byte) 1, createData());

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

  @Test(expected = MudpErrCodeException.class)
  public void generateMudpErrCodeException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setERRFlag(true).setErrCode(MudpChannel.ERRCODE_NO_ERR)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    try {
      channel.sendReliable((byte) 1, createData());
    } finally {
      fakeMudpStream.checkForException();
    }
  }

  @Test(expected = MudpChannelBusyException.class)
  public void generateMudpChannelBusyException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setERRFlag(true).setErrCode(MudpChannel.ERRCODE_CHANNEL_BUSY)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    try {
      channel.sendReliable((byte) 1, createData());
    } finally {
      fakeMudpStream.checkForException();
    }
  }

  @Test
  public void processOfMudpChannelBusyException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 3, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setERRFlag(true).setErrCode(MudpChannel.ERRCODE_CHANNEL_BUSY)));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setERRFlag(true).setErrCode(MudpChannel.ERRCODE_CHANNEL_BUSY)));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    channel.sendReliable((byte) 1, createData());

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

  @Test(expected = MudpChannelTimeoutException.class)
  public void generateMudpChannelTimeoutExceptionOnAllRequestOrAckLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(3, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1).setSequence((byte) 0),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1).setSequence((byte) 0),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1).setSequence((byte) 0),
        0,
        null));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    assertTrue(channel.isEnslaved());

    try {
      channel.sendReliable((byte) 1, createData());
    } finally {
      fakeMudpStream.checkForException();
    }
  }

}
