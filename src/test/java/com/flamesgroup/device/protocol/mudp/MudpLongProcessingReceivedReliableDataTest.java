/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import com.flamesgroup.device.FakeMudpStream;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class MudpLongProcessingReceivedReliableDataTest extends MudpTest {

  @Test(expected = MudpChannelTimeoutException.class)
  public void simulateLongHandlingOfReceivedREDataWithGenerateMudpChannelTimeoutException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));

    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true), // send RE
        5,
        new MudpPacket(createData()).setVersion(version).setREFlag(true))); // receive RE

    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(0).setVersion(version).setMSFlag(true).setREFlag(true).setACKFlag(true), // send ACK for received RE
        10,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true))); // receive ACK for sent RE

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new IMudpChannelHandler() {
      @Override
      public void handleReceive(final byte type, final ByteBuffer data) {
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
        }
      }
    });

    try {
      channel.sendReliable((byte) 0, createData());
    } finally {
      fakeMudpStream.checkForException();
    }
  }

}
