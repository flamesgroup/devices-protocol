/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpStream;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MudpSendReliableUnusualSituationsTest extends MudpTest {

  @Test
  public void processOfDuplicateAck() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(2, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true)));
    iterations.add(new FakeMudpStream.Iteration(
        null,
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true)));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setSequence((byte) 1).setType((byte) 2),
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true).setSequence((byte) 1)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    channel.sendReliable((byte) 1, createData());
    channel.sendReliable((byte) 2, createData());

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

  @Test
  public void processOfPrematureAck() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(2, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // RE
    iterations.add(new FakeMudpStream.Iteration(
        null,
        0,
        new MudpPacket(0).setVersion(version).setREFlag(true).setACKFlag(true)));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setREFlag(true).setType((byte) 1),
        0,
        null));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    channel.sendReliable((byte) 1, createData());

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

}
