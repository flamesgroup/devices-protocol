/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpStream;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class MudpSendUnreliableTest extends MudpTest {

  private Semaphore semaphore;

  private byte receivedType;
  private ByteBuffer receivedData;

  @Before
  public void init() {
    semaphore = new Semaphore(0);
  }

  @Test
  public void processOfEchoNoLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // notRE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setType((byte) 1),
        0,
        new MudpPacket(createData()).setVersion(version).setType((byte) 1)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new IMudpChannelHandler() {
      @Override
      public void handleReceive(final byte type, final ByteBuffer data) {
        receivedType = type;
        receivedData = data;
        semaphore.release();
      }
    });

    assertTrue(channel.isEnslaved());

    channel.sendUnreliable((byte) 1, createData());

    semaphore.acquire();

    assertEquals((byte) 1, receivedType);
    assertEquals(createData(), receivedData);

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

  @Test
  public void processOfEchoLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));
    // notRE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createData()).setVersion(version).setMSFlag(true).setType((byte) 1),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        (MudpPacket) null,
        5,
        (MudpPacket) null));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new IMudpChannelHandler() {
      @Override
      public void handleReceive(final byte type, final ByteBuffer data) {
        receivedType = type;
        receivedData = data;
        semaphore.release();
      }
    });

    assertTrue(channel.isEnslaved());

    channel.sendUnreliable((byte) 1, createData());

    fakeMudpStream.waitForIterationNumber(2);

    assertEquals(0, semaphore.availablePermits());

    channel.free();
    connection.disconnect();

    fakeMudpStream.checkForException();
  }

}
