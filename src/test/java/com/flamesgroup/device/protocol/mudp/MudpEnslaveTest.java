/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.mudp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.FakeMudpStream;
import com.flamesgroup.device.FakeMudpStreamWithThrowMudpIOExceptionOnWriteMudpPacket;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MudpEnslaveTest extends MudpTest {

  @Test
  public void processOfNoLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    assertTrue(channel.isEnslaved());

    channel.free();
    assertFalse(channel.isEnslaved());

    connection.disconnect();
    assertFalse(connection.isConnect());

    fakeMudpStream.checkForException();
  }

  @Test
  public void processOfRequestOrAckLost() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(3, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        null));
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());

    assertTrue(channel.isEnslaved());

    channel.free();
    assertFalse(channel.isEnslaved());

    connection.disconnect();
    assertFalse(connection.isConnect());

    fakeMudpStream.checkForException();
  }

  @Test(expected = MudpErrCodeException.class)
  public void generateMudpErrCodeException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket().setVersion(version).setEEFlag(true).setACKFlag(true).setERRFlag(true)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    try {
      channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());
    } finally {
      fakeMudpStream.checkForException();
    }
  }

  @Test
  public void reEnslaveAfterMudpIOException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    //
    // On this iteration (index 0) must be throw MudpIOException from writeMudpPacket method call
    //
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket(0).setVersion(version).setEEFlag(true).setACKFlag(true)));

    FakeMudpStream fakeMudpStream = new FakeMudpStreamWithThrowMudpIOExceptionOnWriteMudpPacket(iterations, 0);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);

    connection.connect();

    IMudpChannel channel = connection.createChannel();
    boolean enslaveErrorOnFirstIteration = false;
    try {
      channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());
    } catch (MudpIOException e) {
      enslaveErrorOnFirstIteration = true;
    }

    assertTrue("Expect throw MudpIOException", enslaveErrorOnFirstIteration);

    channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());
    assertTrue(channel.isEnslaved());

    fakeMudpStream.checkForException();
  }

  @Test(expected = MudpLicenceFailureException.class)
  public void processMudpLicenceErrorException() throws Exception {
    final MudpOptions mudpOptions = new MudpOptions(0, 200, 0, 250);
    final List<FakeMudpStream.Iteration> iterations = new ArrayList<>();

    // EE
    iterations.add(new FakeMudpStream.Iteration(
        new MudpPacket(createEEData(mudpOptions)).setVersion(version).setMSFlag(true).setEEFlag(true),
        0,
        new MudpPacket().setVersion(version).setEEFlag(true).setACKFlag(true).setERRFlag(true).setErrCode(MudpChannel.ERRCODE_LICENCE_FAILURE)));

    FakeMudpStream fakeMudpStream = new FakeMudpStream(iterations);
    IMudpConnection connection = new MudpConnection(fakeMudpStream, mudpOptions);
    connection.connect();

    assertTrue(connection.isConnect());

    IMudpChannel channel = connection.createChannel();
    try {
      channel.enslave(new MudpChannelAddress((byte) 0), new DummyMudpChannelHandler());
    } finally {
      fakeMudpStream.checkForException();
    }
  }

}
