/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.util;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class CRC32Test {

  private final CRC32 crc32 = new CRC32();

  private final byte bytes[] = "123456789".getBytes();

  @Before
  public void beforeTest() {
    crc32.reset();
  }

  @Test
  public void updateInt() {
    for (byte b : bytes) {
      crc32.update(b);
    }
    assertEquals(0xCBF43926L, crc32.getValue());
  }

  @Test
  public void updateByteArray() {
    crc32.update(bytes, 0, bytes.length);
    assertEquals(0xCBF43926L, crc32.getValue());
  }

  @Test
  public void updateByteBuffer() {
    crc32.update(ByteBuffer.wrap(bytes));
    assertEquals(0xCBF43926L, crc32.getValue());
  }

}
