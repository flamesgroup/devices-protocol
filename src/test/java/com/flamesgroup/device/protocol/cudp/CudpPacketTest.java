/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CudpPacketTest {

  @Test
  public void flagsTest() {
    CudpPacket cudpPacket = new CudpPacket();

    assertFalse(cudpPacket.isBTFlag());
    assertTrue(cudpPacket.setBTFlag(true).isBTFlag());
    assertFalse(cudpPacket.setBTFlag(false).isBTFlag());

    assertFalse(cudpPacket.isACKFlag());
    assertTrue(cudpPacket.setACKFlag(true).isACKFlag());
    assertFalse(cudpPacket.setACKFlag(false).isACKFlag());

    assertFalse(cudpPacket.isERRFlag());
    assertTrue(cudpPacket.setERRFlag(true).isERRFlag());
    assertFalse(cudpPacket.setERRFlag(false).isERRFlag());
  }

  @Test
  public void flagsCombinationTest() {
    CudpPacket cudpPacket = new CudpPacket();

    assertFalse(cudpPacket.isBTFlag());
    assertFalse(cudpPacket.isACKFlag());
    assertFalse(cudpPacket.isERRFlag());

    cudpPacket.setBTFlag(true).setACKFlag(true).setERRFlag(true);

    assertTrue(cudpPacket.isBTFlag());
    assertTrue(cudpPacket.isACKFlag());
    assertTrue(cudpPacket.isERRFlag());

    cudpPacket.clearFlags();

    assertFalse(cudpPacket.isBTFlag());
    assertFalse(cudpPacket.isACKFlag());
    assertFalse(cudpPacket.isERRFlag());
  }

}
