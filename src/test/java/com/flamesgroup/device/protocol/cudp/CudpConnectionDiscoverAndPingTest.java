/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class CudpConnectionDiscoverAndPingTest {

  @Test
  public void discoverDeviceTest() throws Exception {
    Map<DeviceUID, SocketAddress> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), null);
    deviceUIDs.put(new DeviceUID(2), null);

    CudpConnection connection = new CudpConnection(new FakeDiscoverAndPingCudpStream(deviceUIDs, 0), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    Set<DeviceUID> receivedDeviceUIDs = connection.getDeviceUIDs(100);

    assertFalse(receivedDeviceUIDs.isEmpty());
    assertEquals(2, receivedDeviceUIDs.size());

    assertTrue(receivedDeviceUIDs.containsAll(deviceUIDs.keySet()));
    connection.disconnect();
  }

  @Test
  public void pingByUIDTest() throws Exception {
    Map<DeviceUID, SocketAddress> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), null);
    deviceUIDs.put(new DeviceUID(2), null);

    CudpConnection connection = new CudpConnection(new FakeDiscoverAndPingCudpStream(deviceUIDs, 0), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.ping(new DeviceUID(2));
    connection.disconnect();
  }

  @Test(expected = IllegalArgumentException.class)
  public void pingBySpecialUIDTest() throws Exception {
    CudpConnection connection = new CudpConnection(new FakeDiscoverAndPingCudpStream(new HashMap<>(), 0), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.ping(new DeviceUID(0));
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void pingByUIDTimeoutExceptionTest() throws Exception {
    Map<DeviceUID, SocketAddress> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), null);
    deviceUIDs.put(new DeviceUID(2), null);

    CudpConnection connection = new CudpConnection(new FakeDiscoverAndPingCudpStream(deviceUIDs, 0), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.ping(new DeviceUID(3));
  }

  @Test
  public void pingByAdrressTest() throws Exception {
    Map<DeviceUID, SocketAddress> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), new InetSocketAddress("192.168.100.1", 0));
    deviceUIDs.put(new DeviceUID(2), new InetSocketAddress("192.168.100.2", 0));

    CudpConnection connection = new CudpConnection(new FakeDiscoverAndPingCudpStream(deviceUIDs, 10), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    long pingTimeout = connection.ping(new InetSocketAddress("192.168.100.2", 0));
    assertTrue(pingTimeout >= 10);

    connection.disconnect();
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void pingByAdrressTimeoutExceptionTest() throws Exception {
    Map<DeviceUID, SocketAddress> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), new InetSocketAddress("192.168.100.1", 0));
    deviceUIDs.put(new DeviceUID(2), new InetSocketAddress("192.168.100.2", 0));

    CudpConnection connection = new CudpConnection(new FakeDiscoverAndPingCudpStream(deviceUIDs, 0), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.ping(new InetSocketAddress("192.168.100.3", 0));
  }

  private static final class FakeDiscoverAndPingCudpStream extends FakeCudpStream.DefaultReceivedFakeCudpStream {

    private static final DeviceUID UID_SPECIAL_COMMON = new DeviceUID(0x00000000);
    private static final DeviceUID UID_BROADCAST_PING = new DeviceUID(DeviceType.SERVER, 0x00FFFFFF);

    private final Map<DeviceUID, SocketAddress> deviceUIDs;
    private final long pingTimeout;

    private final List<DeviceUID> discoverDeviceUIDs;

    private SocketAddress remoteAddress;

    private FakeDiscoverAndPingCudpStream(final Map<DeviceUID, SocketAddress> deviceUIDs, final long pingTimeout) {
      super(deviceUIDs.keySet());
      this.deviceUIDs = deviceUIDs;
      this.pingTimeout = pingTimeout;

      discoverDeviceUIDs = new ArrayList<>(deviceUIDs.keySet());
    }

    @Override
    byte getCudpPacketType() {
      return CudpPacket.TYPE_PING;
    }

    @Override
    boolean handleReceived(final CudpPacket cudpPacket, final AtomicBoolean waitCommand) {
      if (cudpPacket.getSUID() != UID_SPECIAL_COMMON.getUID()) {
        return super.handleReceived(cudpPacket, waitCommand);
      }

      if (cudpPacket.getDUID() == UID_BROADCAST_PING.getUID()) { //discover
        cudpPacket.setSUID(discoverDeviceUIDs.remove(0).getUID());
        if (discoverDeviceUIDs.isEmpty()) {
          waitCommand.set(true);
        }
      } else { //ping by address
        waitCommand.set(true);
        if (!deviceUIDs.containsValue(remoteAddress)) {
          return false;
        }

        DeviceUID deviceUID = getDeviceUID(remoteAddress);
        if (deviceUID == null) {
          throw new IllegalStateException("There is no deviceUID for remoteAddress " + remoteAddress);
        }

        cudpPacket.setSUID(deviceUID.getUID());
        if (pingTimeout > 0) {
          try {
            Thread.sleep(pingTimeout);
          } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage());
          }
        }
      }
      return true;
    }

    @Override
    void handleSended(final SocketAddress socketAddress, final CudpPacket receivedCudpPacket, final CudpPacket sendedCudpPacket) {
      sendedCudpPacket.setACKFlag(true);
      sendedCudpPacket.setTimestamp(receivedCudpPacket.getTimestamp());
      remoteAddress = socketAddress;
    }

    private DeviceUID getDeviceUID(final SocketAddress remoteAddress) {
      for (Map.Entry<DeviceUID, SocketAddress> entry : deviceUIDs.entrySet()) {
        if (entry.getValue().equals(remoteAddress)) {
          return entry.getKey();
        }
      }
      return null;
    }
  }

}
