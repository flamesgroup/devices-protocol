/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.DeviceUID;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

public class CudpConnectionGetExpireTimeTest {

  @Test
  public void getExpireTimeByUIDTest() throws Exception {
    long expireTime = System.currentTimeMillis();
    Map<DeviceUID, Long> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), expireTime);

    CudpConnection connection = new CudpConnection(new FakeGetExpireTimeStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    long receivedExpireTime = connection.getExpireTime(new DeviceUID(1)).getValue();
    assertEquals(expireTime, receivedExpireTime);

    connection.disconnect();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getExpireTimeBySpecialUIDTest() throws Exception {
    CudpConnection connection = new CudpConnection(new FakeGetExpireTimeStream(new HashMap<>()), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.getExpireTime(new DeviceUID(0));
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void getExpireTimeByUIDTimeoutExceptionTest() throws Exception {
    Map<DeviceUID, Long> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(1), (long) 0);

    CudpConnection connection = new CudpConnection(new FakeGetExpireTimeStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.getExpireTime(new DeviceUID(2));
  }

  private final class FakeGetExpireTimeStream extends FakeCudpStream.DefaultReceivedFakeCudpStream {

    private final Map<DeviceUID, Long> deviceUIDs;

    private FakeGetExpireTimeStream(final Map<DeviceUID, Long> deviceUIDs) {
      super(deviceUIDs.keySet());
      this.deviceUIDs = deviceUIDs;
    }

    @Override
    byte getCudpPacketType() {
      return CudpPacket.TYPE_GET_EXPIRE_TIME;
    }

    @Override
    void handleSended(final SocketAddress socketAddress, final CudpPacket receivedCudpPacket, final CudpPacket sendedCudpPacket) {
      DeviceUID deviceUID = new DeviceUID(receivedCudpPacket.getDUID());
      if (deviceUIDs.containsKey(deviceUID)) {
        sendedCudpPacket.setACKFlag(true);
        sendedCudpPacket.setExpireTime(deviceUIDs.get(deviceUID));
      }
    }
  }

}
