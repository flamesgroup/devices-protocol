/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import static org.junit.Assert.assertNotNull;

import com.flamesgroup.device.protocol.cudp.CudpConnectionTimeoutException;
import org.junit.Test;

public class AtomicCudpCommandTest {

  @Test
  public void testCommandAnswerBeforeAwait() throws Exception {
    FakeAtomicCudpCommand command = new FakeAtomicCudpCommand(new FakeCudpLink(0), 5);
    FakeCudpCommandResponse response = command.execute(null, command.createRequest(null, null));
    assertNotNull(response);
  }

  @Test
  public void testCommandAnswerWithAwait() throws Exception {
    FakeAtomicCudpCommand command = new FakeAtomicCudpCommand(new FakeCudpLink(25), 200);
    FakeCudpCommandResponse response = command.execute(null, command.createRequest(null, null));
    assertNotNull(response);
  }

  @Test(expected = CudpCommandException.class)
  public void testCommandAnswerWithException() throws Exception {
    FakeAtomicCudpCommand command = new FakeAtomicCudpCommand(new FakeCudpLink(0), 5, true);
    command.execute(null, command.createRequest(null, null));
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void testCommandTimeout() throws Exception {
    FakeAtomicCudpCommand command = new FakeAtomicCudpCommand(new FakeCudpLink(30), 15);
    command.execute(null, command.createRequest(null, null));
  }

}
