/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpPacket;
import com.flamesgroup.device.protocol.cudp.ICudpLink;
import com.flamesgroup.device.protocol.cudp.ICudpLinkHandler;

import java.net.SocketAddress;

public class FakeCudpLink implements ICudpLink {

  private final int timeout;

  private ICudpLinkHandler cudpLinkHandler;

  private boolean linked;

  public FakeCudpLink(final int timeout) {
    this.timeout = timeout;
  }

  @Override
  public boolean isLinked() {
    return linked;
  }

  @Override
  public void link(final DeviceUID duid, final ICudpLinkHandler cudpLinkHandler) throws CudpException {
    linked = true;
    this.cudpLinkHandler = cudpLinkHandler;
  }

  @Override
  public void unlink() {
    linked = false;
  }

  @Override
  public void send(final SocketAddress remoteAddress, final CudpPacket packet) throws CudpException {
    if (timeout == 0) {
      cudpLinkHandler.handleReceive(remoteAddress, packet);
    } else {
      new Thread(() -> {
        try {
          Thread.sleep(timeout);
        } catch (InterruptedException e) {
        }
        cudpLinkHandler.handleReceive(remoteAddress, packet);
      }).start();
    }
  }

}
