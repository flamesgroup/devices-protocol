/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.DeviceUID;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.HashSet;
import java.util.Set;

public class CudpConnectionUpdateExpireTimeTest {

  @Test
  public void updateExpireTimeByUIDTest() throws Exception {
    Set<DeviceUID> deviceUIDs = new HashSet<>();
    deviceUIDs.add(new DeviceUID(1));
    deviceUIDs.add(new DeviceUID(2));

    CudpConnection connection = new CudpConnection(new FakeUpdateExpireTimeCudpStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.updateExpireTime(new DeviceUID(2), new byte[48]);
    connection.disconnect();
  }

  @Test(expected = IllegalArgumentException.class)
  public void updateExpireTimeByCipheredIllegalSizeTest() throws Exception {
    CudpConnection connection = new CudpConnection(new FakeUpdateExpireTimeCudpStream(null), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));
    connection.updateExpireTime(new DeviceUID(2), new byte[50]);
  }

  @Test(expected = IllegalArgumentException.class)
  public void updateExpireTimeBySpecialUIDTest() throws Exception {
    CudpConnection connection = new CudpConnection(new FakeUpdateExpireTimeCudpStream(null), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));
    connection.updateExpireTime(new DeviceUID(0), new byte[48]);
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void updateExpireTimeTimeoutExceptionTest() throws Exception {
    Set<DeviceUID> deviceUIDs = new HashSet<>();
    deviceUIDs.add(new DeviceUID(1));
    deviceUIDs.add(new DeviceUID(2));

    CudpConnection connection = new CudpConnection(new FakeUpdateExpireTimeCudpStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.updateExpireTime(new DeviceUID(3), new byte[48]);
  }

  private final class FakeUpdateExpireTimeCudpStream extends FakeCudpStream.DefaultReceivedFakeCudpStream {

    public FakeUpdateExpireTimeCudpStream(final Set<DeviceUID> deviceUIDs) {
      super(deviceUIDs);
    }

    @Override
    byte getCudpPacketType() {
      return CudpPacket.TYPE_UPDATE_EXPIRE_TIME;
    }

    @Override
    void handleSended(final SocketAddress socketAddress, final CudpPacket receivedCudpPacket, final CudpPacket sendedCudpPacket) {
      sendedCudpPacket.setKey(receivedCudpPacket.getKey());
    }

  }

}
