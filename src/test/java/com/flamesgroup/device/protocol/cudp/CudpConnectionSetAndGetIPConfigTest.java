/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.DeviceUID;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class CudpConnectionSetAndGetIPConfigTest {

  private static InetAddress mask;
  private static InetAddress gateway;
  private static InetAddress masterIP;

  @BeforeClass
  public static void init() throws UnknownHostException {
    mask = InetAddress.getByName("255.255.255.0");
    gateway = InetAddress.getByName("192.168.100.1");
    masterIP = InetAddress.getByName("0.0.0.0");
  }

  @Test
  public void getIPConfigByUIDTest() throws Exception {
    IPConfig ipConfig = new IPConfig(InetAddress.getByName("192.168.100.3"), mask, gateway, masterIP, UUID.randomUUID());
    Map<DeviceUID, IPConfig> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(2), ipConfig);

    CudpConnection connection = new CudpConnection(new FakeGetIPConfigCudpStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    IPConfig receivedIPConfig = connection.getIPConfig(new DeviceUID(2));
    assertEquals(ipConfig, receivedIPConfig);

    connection.disconnect();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getIPConfigBySpecialUIDTest() throws Exception {
    CudpConnection connection = new CudpConnection(new FakeGetIPConfigCudpStream(new HashMap<>()), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.getIPConfig(new DeviceUID(0));
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void getIPConfigByUIDTimeoutExceptionTest() throws Exception {
    IPConfig ipConfig = new IPConfig(InetAddress.getByName("192.168.100.3"), mask, gateway, masterIP, UUID.randomUUID());
    Map<DeviceUID, IPConfig> deviceUIDs = new HashMap<>();
    deviceUIDs.put(new DeviceUID(2), ipConfig);

    CudpConnection connection = new CudpConnection(new FakeGetIPConfigCudpStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.getIPConfig(new DeviceUID(3));
  }

  @Test(expected = IllegalArgumentException.class)
  public void setIPConfigBySpecialUIDTest() throws Exception {
    CudpConnection connection = new CudpConnection(new FakeSetIPConfigCudpStream(null), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    connection.setIPConfig(new DeviceUID(0), new IPConfig(InetAddress.getByName("192.168.100.2"), mask, gateway, masterIP, UUID.randomUUID()));
  }

  @Test(expected = CudpConnectionTimeoutException.class)
  public void setIPConfigByUIDTimeoutExceptionTest() throws Exception {
    Set<DeviceUID> deviceUIDs = new HashSet<>();
    deviceUIDs.add(new DeviceUID(1));
    deviceUIDs.add(new DeviceUID(2));

    CudpConnection connection = new CudpConnection(new FakeSetIPConfigCudpStream(deviceUIDs), 200);
    connection.connect(new InetSocketAddress("0.0.0.0", 0));

    IPConfig ipConfig = new IPConfig(InetAddress.getByName("192.168.100.2"), mask, gateway, masterIP, UUID.randomUUID());
    connection.setIPConfig(new DeviceUID(3), ipConfig);
  }

  private final class FakeGetIPConfigCudpStream extends FakeCudpStream.DefaultReceivedFakeCudpStream {

    private final Map<DeviceUID, IPConfig> deviceUIDs;

    public FakeGetIPConfigCudpStream(final Map<DeviceUID, IPConfig> deviceUIDs) {
      super(deviceUIDs.keySet());
      this.deviceUIDs = deviceUIDs;
    }

    @Override
    byte getCudpPacketType() {
      return CudpPacket.TYPE_GET_IP_CONFIG;
    }

    @Override
    void handleSended(final SocketAddress socketAddress, final CudpPacket receivedCudpPacket, final CudpPacket sendedCudpPacket) {
      DeviceUID deviceUID = new DeviceUID(receivedCudpPacket.getDUID());
      if (deviceUIDs.keySet().contains(deviceUID)) {
        IPConfig ipConfig = deviceUIDs.get(deviceUID);
        sendedCudpPacket.setACKFlag(true);
        sendedCudpPacket.setIP(ipConfig.getIP());
        sendedCudpPacket.setMask(ipConfig.getMask());
        sendedCudpPacket.setGateway(ipConfig.getGateway());
        sendedCudpPacket.setMasterIP(ipConfig.getMasterIP());
        sendedCudpPacket.setUUID(ipConfig.getUUID());
      }
    }
  }

  private class FakeSetIPConfigCudpStream extends FakeCudpStream.DefaultReceivedFakeCudpStream {

    public FakeSetIPConfigCudpStream(final Set<DeviceUID> deviceUIDs) {
      super(deviceUIDs);
    }

    @Override
    byte getCudpPacketType() {
      return CudpPacket.TYPE_SET_IP_CONFIG;
    }

    @Override
    void handleSended(final SocketAddress socketAddress, final CudpPacket receivedCudpPacket, final CudpPacket sendedCudpPacket) {
      sendedCudpPacket.setIP(receivedCudpPacket.getIP());
      sendedCudpPacket.setMask(receivedCudpPacket.getMask());
      sendedCudpPacket.setGateway(receivedCudpPacket.getGateway());
      sendedCudpPacket.setMasterIP(receivedCudpPacket.getMasterIP());
      sendedCudpPacket.setUUID(receivedCudpPacket.getUUID());
      sendedCudpPacket.setCurrentTime(receivedCudpPacket.getCurrentTime());
    }
  }

}
