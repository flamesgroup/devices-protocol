/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

public class IPConfigTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void ipNPETest() throws UnknownHostException {
    expectedException.expect(NullPointerException.class);
    expectedException.expectMessage("IP address must not be null");

    InetAddress ia = InetAddress.getByName("0.0.0.0");
    new IPConfig(null, ia, ia, ia, UUID.randomUUID());
  }

  @Test
  public void maskNPETest() throws UnknownHostException {
    expectedException.expect(NullPointerException.class);
    expectedException.expectMessage("Subnet mask must not be null");

    InetAddress ia = InetAddress.getByName("0.0.0.0");
    new IPConfig(ia, null, ia, ia, UUID.randomUUID());
  }

  @Test
  public void gatewayNPETest() throws UnknownHostException {
    expectedException.expect(NullPointerException.class);
    expectedException.expectMessage("Gateway address must not be null");

    InetAddress ia = InetAddress.getByName("0.0.0.0");
    new IPConfig(ia, ia, null, ia, UUID.randomUUID());
  }

  @Test
  public void masterIPNPETest() throws UnknownHostException {
    expectedException.expect(NullPointerException.class);
    expectedException.expectMessage("Master IP address must not be null");

    InetAddress ia = InetAddress.getByName("0.0.0.0");
    new IPConfig(ia, ia, ia, null, UUID.randomUUID());
  }

  @Test
  public void equalsTest() throws UnknownHostException {
    InetAddress ia1 = InetAddress.getByName("0.0.0.0");
    InetAddress ia2 = InetAddress.getByName("1.1.1.1");

    UUID uuid = UUID.randomUUID();
    IPConfig ipConfig1 = new IPConfig(ia1, ia1, ia1, ia1, uuid);
    IPConfig ipConfig2 = new IPConfig(ia1, ia1, ia1, ia1, uuid);
    IPConfig ipConfig3 = new IPConfig(ia2, ia1, ia2, ia1, uuid);

    assertThat(ipConfig1, equalTo(ipConfig2));
    assertThat(ipConfig1, not(equalTo(ipConfig3)));
    assertThat(ipConfig3, not(equalTo(ipConfig1)));
  }

  @Test
  public void hashCodeTest() throws UnknownHostException {
    InetAddress ia1 = InetAddress.getByName("0.0.0.0");
    InetAddress ia2 = InetAddress.getByName("1.1.1.1");

    UUID uuid = UUID.randomUUID();
    IPConfig ipConfig1 = new IPConfig(ia1, ia1, ia1, ia1, uuid);
    IPConfig ipConfig2 = new IPConfig(ia1, ia1, ia1, ia1, uuid);
    IPConfig ipConfig3 = new IPConfig(ia2, ia1, ia2, ia1, uuid);

    assertThat(ipConfig1.hashCode(), equalTo(ipConfig2.hashCode()));
    assertThat(ipConfig1.hashCode(), not(equalTo(ipConfig3.hashCode())));
  }

}
