/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import com.flamesgroup.device.DeviceUID;

import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class FakeCudpStream implements ICudpStream {

  private final Semaphore semaphore = new Semaphore(0);
  private final ByteBuffer buffer = ByteBuffer.allocate(76).order(ByteOrder.LITTLE_ENDIAN);
  private final AtomicBoolean waitCommand = new AtomicBoolean(true);

  private boolean isOpen;

  abstract byte getCudpPacketType();

  abstract boolean handleReceived(CudpPacket cudpPacket, AtomicBoolean waitCommand);

  abstract void handleSended(SocketAddress socketAddress, CudpPacket receivedCudpPacket, CudpPacket sendedCudpPacket);

  @Override
  public boolean isOpen() {
    return isOpen;
  }

  @Override
  public void open(final SocketAddress localSocketAddress) throws CudpIOException {
    isOpen = true;
  }

  @Override
  public void close() throws CudpIOException {
    isOpen = false;
  }

  @Override
  public SocketAddress receiveCudpPacket(final CudpPacket cudpPacket) throws CudpIOException, CudpPacketReadException {
    while (true) {
      if (waitCommand.get()) {
        try {
          semaphore.acquire();
        } catch (InterruptedException e) {
          throw new CudpIOException(new ClosedChannelException());
        }
        waitCommand.set(false);
      }

      cudpPacket.readFromByteBuffer(buffer);
      buffer.rewind();

      if (handleReceived(cudpPacket, waitCommand)) {
        return null;
      }
    }
  }

  @Override
  public void sendCudpPacket(final SocketAddress socketAddress, final CudpPacket cudpPacket) throws CudpIOException, CudpPacketWriteException {
    if (cudpPacket.getType() != getCudpPacketType()) {
      return;
    }

    CudpPacket sendedCudpPacket = new CudpPacket();
    sendedCudpPacket.setVersion(cudpPacket.getVersion());
    sendedCudpPacket.setType(cudpPacket.getType());
    sendedCudpPacket.setSUID(cudpPacket.getDUID());
    sendedCudpPacket.setDUID(cudpPacket.getSUID());

    handleSended(socketAddress, cudpPacket, sendedCudpPacket);

    buffer.clear();
    sendedCudpPacket.writeToByteBuffer(buffer);
    buffer.flip();

    semaphore.release();
  }

  static abstract class DefaultReceivedFakeCudpStream extends FakeCudpStream {

    private final Set<DeviceUID> deviceUIDs;
    private final Set<Byte> ackTypes = new HashSet<>();

    DefaultReceivedFakeCudpStream(final Set<DeviceUID> deviceUIDs) {
      this.deviceUIDs = deviceUIDs;

      ackTypes.add(CudpPacket.TYPE_SET_IP_CONFIG);
      ackTypes.add(CudpPacket.TYPE_RESET);
      ackTypes.add(CudpPacket.TYPE_UPDATE_EXPIRE_TIME);
    }

    @Override
    boolean handleReceived(final CudpPacket cudpPacket, final AtomicBoolean waitCommand) {
      for (DeviceUID deviceUID : deviceUIDs) {
        if (deviceUID.getUID() == cudpPacket.getSUID()) {
          waitCommand.set(true);
          if (ackTypes.contains(cudpPacket.getType())) {
            cudpPacket.setACKFlag(true);
          }
          return true;
        }
      }
      return false;
    }
  }

}
