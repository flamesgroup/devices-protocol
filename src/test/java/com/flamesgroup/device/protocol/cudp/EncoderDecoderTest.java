/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

public class EncoderDecoderTest {

  private CudpPacket cudpPacket1;
  private CudpPacket cudpPacket2;
  private ByteBuffer buffer;

  public EncoderDecoderTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() {
    cudpPacket1 = new CudpPacket();
    cudpPacket2 = new CudpPacket();
    buffer = ByteBuffer.allocate(64).order(ByteOrder.LITTLE_ENDIAN);
  }

  @After
  public void tearDown() {
    cudpPacket1 = null;
    cudpPacket2 = null;
    buffer = null;
  }

  @Test
  public void cudpPacketPingTest() throws CudpException {
    cudpPacket1.setDUID((short) 111);
    cudpPacket1.setSUID((short) 222);
    cudpPacket1.setType(CudpPacket.TYPE_PING);
    cudpPacket1.setErrCode((byte) 0);
    cudpPacket1.setTimestamp(1234567890);

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketPingAckTest() throws CudpException {
    cudpPacket1.setDUID((short) 333);
    cudpPacket1.setSUID((short) 444);
    cudpPacket1.setType(CudpPacket.TYPE_PING);
    cudpPacket1.setACKFlag(true);
    cudpPacket1.setErrCode((byte) 0);
    cudpPacket1.setTimestamp(987654321);

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketSetIPConfigTest() throws CudpException, UnknownHostException {
    cudpPacket1.setDUID((short) 333);
    cudpPacket1.setSUID((short) 444);
    cudpPacket1.setType(CudpPacket.TYPE_SET_IP_CONFIG);
    cudpPacket1.setErrCode((byte) 0);
    cudpPacket1.setIP((Inet4Address) InetAddress.getByName("192.168.0.1"));
    cudpPacket1.setMask((Inet4Address) InetAddress.getByAddress(new byte[] {5, 6, 7, 8}));
    cudpPacket1.setGateway((Inet4Address) InetAddress.getByAddress(new byte[] {9, 10, 11, 12}));
    cudpPacket1.setMasterIP((Inet4Address) InetAddress.getByAddress(new byte[] {13, 14, 15, 16}));
    cudpPacket1.setUUID(UUID.randomUUID());
    cudpPacket1.setCurrentTime(System.currentTimeMillis());

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketSetIPConfigAckTest() throws CudpException {
    cudpPacket1.setDUID((short) 333);
    cudpPacket1.setSUID((short) 444);
    cudpPacket1.setType(CudpPacket.TYPE_SET_IP_CONFIG);
    cudpPacket1.setACKFlag(true);
    cudpPacket1.setErrCode((byte) 0);

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketGetIPConfigTest() throws CudpException {
    cudpPacket1.setDUID((short) 111);
    cudpPacket1.setSUID((short) 222);
    cudpPacket1.setType(CudpPacket.TYPE_GET_IP_CONFIG);
    cudpPacket1.setErrCode((byte) 0);

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketGetIPConfigAckTest() throws CudpException, UnknownHostException {
    cudpPacket1.setDUID((short) 111);
    cudpPacket1.setSUID((short) 222);
    cudpPacket1.setType(CudpPacket.TYPE_GET_IP_CONFIG);
    cudpPacket1.setACKFlag(true);
    cudpPacket1.setErrCode((byte) 0);
    cudpPacket1.setIP((Inet4Address) InetAddress.getByAddress(new byte[] {1, 2, 3, 4}));
    cudpPacket1.setMask((Inet4Address) InetAddress.getByAddress(new byte[] {5, 6, 7, 8}));
    cudpPacket1.setGateway((Inet4Address) InetAddress.getByAddress(new byte[] {9, 10, 11, 12}));
    cudpPacket1.setMasterIP((Inet4Address) InetAddress.getByAddress(new byte[] {13, 14, 15, 16}));
    cudpPacket1.setUUID(UUID.randomUUID());

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketRstTest() throws CudpException {
    cudpPacket1.setDUID((short) 111);
    cudpPacket1.setSUID((short) 222);
    cudpPacket1.setType(CudpPacket.TYPE_RESET);
    cudpPacket1.setErrCode((byte) 0);
    cudpPacket1.setDelay(1234);

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

  @Test
  public void cudpPacketRstAckTest() throws CudpException {
    cudpPacket1.setDUID((short) 111);
    cudpPacket1.setSUID((short) 222);
    cudpPacket1.setType(CudpPacket.TYPE_RESET);
    cudpPacket1.setACKFlag(true);
    cudpPacket1.setErrCode((byte) 10);

    buffer.clear();
    cudpPacket1.writeToByteBuffer(buffer);
    buffer.flip();
    cudpPacket2.readFromByteBuffer(buffer);

    assertEquals(cudpPacket1, cudpPacket2);
  }

}
