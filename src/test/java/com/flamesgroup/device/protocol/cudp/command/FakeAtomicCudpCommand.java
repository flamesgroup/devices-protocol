/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.protocol.cudp.command;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.protocol.cudp.CudpErrCodeException;
import com.flamesgroup.device.protocol.cudp.ICudpLink;

public class FakeAtomicCudpCommand extends AtomicCudpCommand<FakeCudpCommandRequest, FakeCudpCommandResponse> {

  private final boolean exceptionMode;

  public FakeAtomicCudpCommand(final ICudpLink cudpLink, final long responseTimeout) {
    this(cudpLink, responseTimeout, false);
  }

  public FakeAtomicCudpCommand(final ICudpLink cudpLink, final long responseTimeout, final boolean exceptionMode) {
    super(cudpLink, responseTimeout);
    this.exceptionMode = exceptionMode;
  }

  @Override
  protected CudpErrCodeException specifyErrCode(final CudpErrCodeException e) {
    return new CudpErrCodeException((byte) 1);
  }

  @Override
  public FakeCudpCommandRequest createRequest(final DeviceUID sduid, final DeviceUID dduid) {
    return new FakeCudpCommandRequest();
  }

  @Override
  public FakeCudpCommandResponse createResponse() {
    return new FakeCudpCommandResponse(exceptionMode);
  }

}
