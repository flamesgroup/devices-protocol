/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DeviceUIDTest {

  @Test
  public void testDeviceUIDValueFromCanonicalName() {
    assertEquals(new DeviceUID(DeviceType.GSMB3, 100), DeviceUID.valueFromCanonicalName("GSMB3-100"));
    assertEquals(new DeviceUID(DeviceType.FTB, 0), DeviceUID.valueFromCanonicalName("FTB-0"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIncorrectDeviceUIDValueFromCanonicalName1() {
    DeviceUID.valueFromCanonicalName("A-A");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIncorrectDeviceUIDValueFromCanonicalName2() {
    DeviceUID.valueFromCanonicalName("FTB-0-0");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIncorrectDeviceUIDValueFromCanonicalName3() {
    DeviceUID.valueFromCanonicalName("FFF-0");
  }

  @Test
  public void constructCorrectDeviceUIDFromInteger() {
    DeviceUID deviceUID;

    deviceUID = new DeviceUID(0x60000001);
    assertEquals(DeviceType.GSMB3, deviceUID.getDeviceType());
    assertEquals(1, deviceUID.getDeviceNumber());

    deviceUID = new DeviceUID(0x30000064);
    assertEquals(DeviceType.SIMB, deviceUID.getDeviceType());
    assertEquals(100, deviceUID.getDeviceNumber());

    deviceUID = new DeviceUID(0xA00000C8);
    assertEquals(DeviceType.GSMB_RESTRICTED, deviceUID.getDeviceType());
    assertEquals(200, deviceUID.getDeviceNumber());

    deviceUID = new DeviceUID(0x780003E8);
    assertEquals(DeviceType.SIMBOX120, deviceUID.getDeviceType());
    assertEquals(1000, deviceUID.getDeviceNumber());

    deviceUID = new DeviceUID(0x710007D0);
    assertEquals(DeviceType.GSMBOX4, deviceUID.getDeviceType());
    assertEquals(2000, deviceUID.getDeviceNumber());
  }

  @Test
  public void constructCorrectDeviceUIDFromDeviceTypeAndInteger() {
    DeviceUID deviceUID;

    deviceUID = new DeviceUID(DeviceType.FTB, 1);
    assertEquals(0x50000001, deviceUID.getUID());

    deviceUID = new DeviceUID(DeviceType.GSMB3_RESTRICTED, 100);
    assertEquals(0xE0000064, deviceUID.getUID());

    deviceUID = new DeviceUID(DeviceType.GSMB2, 10);
    assertEquals(0x4000000A, deviceUID.getUID());

    deviceUID = new DeviceUID(DeviceType.GSMBOX4, 3);
    assertEquals(0x71000003, deviceUID.getUID());

    deviceUID = new DeviceUID(DeviceType.SIMBOX120, 5);
    assertEquals(0x78000005, deviceUID.getUID());
  }

  @Test
  public void testCompare() {
    DeviceUID a = new DeviceUID(DeviceType.SIMB, 1000);
    DeviceUID b = new DeviceUID(DeviceType.SIMB, 0);
    DeviceUID c = new DeviceUID(DeviceType.SIMB_RESTRICTED, 1000);

    assertTrue(a.compareTo(b) > 0); // a > b
    assertTrue(b.compareTo(c) > 0); // b > c
    assertTrue(a.compareTo(c) > 0); // a > c
  }

}
