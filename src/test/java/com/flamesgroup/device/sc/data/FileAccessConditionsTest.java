/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc.data;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.device.sc.AccessCondition;
import com.flamesgroup.device.sc.FileAccessConditions;
import org.junit.Test;

import java.nio.ByteBuffer;

public class FileAccessConditionsTest {

  @Test
  public void testWrite() {
    FileAccessConditions fac = new FileAccessConditions();
    fac.setIncrease(AccessCondition.ADM5);
    fac.setInvalidate(AccessCondition.ALWAYS);
    fac.setRead(AccessCondition.CHV1);
    fac.setRehabilitate(AccessCondition.CHV2);
    fac.setUpdate(AccessCondition.NEVER);

    ByteBuffer bb = ByteBuffer.allocate(300);
    FileAccessConditions.encoding(fac, bb);
    bb.flip();
    byte[] data = new byte[bb.remaining()];
    bb.get(data);
    assertEquals((byte) 0x1f, data[0]);
    assertEquals((byte) 0x80, data[1]);
    assertEquals((byte) 0x20, data[2]);
  }

}
