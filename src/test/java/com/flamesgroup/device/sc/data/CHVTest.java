/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc.data;

import static org.junit.Assert.assertArrayEquals;

import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.UnblockCHV;
import org.junit.Test;

import java.nio.ByteBuffer;

public class CHVTest {

  @Test
  public void testCHV() throws Exception {
    CHV chv = new CHV("12345");
    ByteBuffer bb = ByteBuffer.allocate(300);
    CHV.encoding(chv, bb);
    bb.flip();
    byte[] data = new byte[bb.remaining()];
    bb.get(data);
    assertArrayEquals(new byte[] {49, 50, 51, 52, 53, -1, -1, -1}, data);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTooLongCHV() {
    new CHV("987654321");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTooShortCHV() {
    new CHV("123");
  }

  @Test
  public void testUnblockCHV() throws Exception {
    UnblockCHV chv = new UnblockCHV("12345678");
    ByteBuffer bb = ByteBuffer.allocate(300);
    CHV.encoding(chv, bb);
    bb.flip();
    byte[] data = new byte[bb.remaining()];
    bb.get(data);
    assertArrayEquals(new byte[] {49, 50, 51, 52, 53, 54, 55, 56}, data);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testUnblockCHVWrongLength() throws Exception {
    new UnblockCHV("123456");
  }

}
