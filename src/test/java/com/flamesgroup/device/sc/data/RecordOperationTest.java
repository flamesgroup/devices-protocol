/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.device.sc.data;

import static org.junit.Assert.assertArrayEquals;

import com.flamesgroup.device.sc.RecordOperation;
import com.flamesgroup.device.sc.RecordOperation.RecordOperationMode;
import org.junit.Test;

import java.nio.ByteBuffer;

public class RecordOperationTest {

  @Test
  public void test() throws Exception {
    RecordOperation op = new RecordOperation(RecordOperationMode.PREV_RECORD_MODE, 15);
    ByteBuffer bb = ByteBuffer.allocate(300);
    RecordOperation.encoding(op, bb);
    bb.flip();
    byte[] data = new byte[bb.remaining()];
    bb.get(data);
    assertArrayEquals(new byte[] {0, 3}, data);

    op = new RecordOperation(RecordOperationMode.NEXT_RECORD_MODE, 20);
    bb.clear();
    RecordOperation.encoding(op, bb);
    bb.flip();
    data = new byte[bb.remaining()];
    bb.get(data);
    assertArrayEquals(new byte[] {0, 2}, data);

    op = new RecordOperation(RecordOperationMode.ABSOLUTE_MODE, 30);
    bb.clear();
    RecordOperation.encoding(op, bb);
    bb.flip();
    data = new byte[bb.remaining()];
    bb.get(data);
    assertArrayEquals(new byte[] {30, 4}, data);
  }

  @Test(expected = NullPointerException.class)
  public void testConstructorParametersCheck1() {
    new RecordOperation(null, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructorParametersCheck2() {
    new RecordOperation(RecordOperationMode.ABSOLUTE_MODE, -1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructorParametersCheck3() {
    new RecordOperation(RecordOperationMode.ABSOLUTE_MODE, 256);
  }

}
