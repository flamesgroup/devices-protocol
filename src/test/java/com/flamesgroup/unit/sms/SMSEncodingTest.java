/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import static org.junit.Assert.assertEquals;

import org.ajwcc.pduUtils.gsm3040.Pdu;
import org.ajwcc.pduUtils.gsm3040.PduParser;
import org.ajwcc.pduUtils.gsm3040.SmsDeliveryPdu;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMSEncodingTest {

  private static PduParser parser;

  @BeforeClass
  public static void init() {
    parser = new PduParser();
  }

  @Test
  public void test7bitEncoding() throws SMSException {
    Pdu pdu = parser.parsePdu("0791836003000080040C9183607306370900001170229170512109C8329BFD0EAD5C2A");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSEncoding.ENC7BIT, sms.getEncoding());
    assertEquals("Hello!+.*", sms.getMessage());
  }

  @Test
  public void test8bitEncoding() throws SMSException {
    Pdu pdu = parser.parsePdu("0791836003000080040C918360730637090004117022917051210948656C6C6F212B2E2A");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSEncoding.ENC8BIT, sms.getEncoding());
    assertEquals("Hello!+.*", sms.getMessage());
  }

  @Test
  public void testUCS2Encoding() throws SMSException {
    Pdu pdu = parser.parsePdu("0791836003000080040C9183607306370900081170229170512114041F044004380432043504420021002B002E002A");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSEncoding.ENCUCS2, sms.getEncoding());
    assertEquals("Привет!+.*", sms.getMessage());
  }

}
