/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.unit.PhoneNumber;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class SMSTest {

  @Test
  public void testSMSUTF() throws UnsupportedEncodingException, PDUException {
    String phoneNumber = "+15125551234";
    String text = "Lorem Ipsum - это текст - рыба, часто используемый в печати и вэб-дизайне. "
        + "Lorem Ipsum является стандартной рыбой для текстов на латини еще с начала XVI века";

    int i = 0;
    List<String> list = SMSHelper.createStringPdus(null, new PhoneNumber(phoneNumber), text, 1, SMSMessageClass.NONE, -1, false);
    for (String pduStr : list) {
      int length = SMSHelper.getPduSize(pduStr);
      String pduString;

      if (0 == i) {
        pduString = "0051000B915121551532F40008FF8C050003010301004C006F00720065006D00200049007000730075"
            + "006D0020002D0020044D0442043E002004420435043A044104420020002D00200440044B04310"
            + "430002C00200447043004410442043E002004380441043F043E043B044C043704430435043C04"
            + "4B0439002004320020043F043504470430044204380020043800200432044D0431002D0434";

        assertEquals(154, length);
        assertEquals(pduString, pduStr);
      }

      if (1 == i) {
        pduString = "0051000B915121551532F40008FF8C0500030103020438043704300439043D0435002E0020004C006F0"
            + "0720065006D00200049007000730075006D0020044F0432043B044F043504420441044F002004"
            + "4104420430043D0434043004400442043D043E043900200440044B0431043E043900200434043"
            + "B044F002004420435043A04410442043E04320020043D04300020043B043004420438043D";

        assertEquals(154, length);
        assertEquals(pduString, pduStr);
      }

      if (2 == i) {
        pduString = "0051000B915121551532F40008FF3405000301030304380020043504490435002004410020043D043004"
            + "470430043B04300020005800560049002004320435043A0430";

        assertEquals(66, length);
        assertEquals(pduString, pduStr);
      }
      i++;
    }
    assertEquals(3, i);
  }

  @Test
  public void testSMSGSM0338() throws UnsupportedEncodingException, PDUException {
    String text = "hello world";
    String phoneNumber = "+15125551234";

    int i = 0;
    List<String> list = SMSHelper.createStringPdus(null, new PhoneNumber(phoneNumber), text, 2, SMSMessageClass.NONE, -1, false);
    for (String pduStr : list) {
      assertEquals(24, SMSHelper.getPduSize(pduStr));
      assertEquals("0011000B915121551532F40000FF0BE8329BFD06DDDF723619", pduStr);
      i++;
    }
    assertEquals(1, i);

    //------------------------- concatenated SMS
    phoneNumber = "+15125551234";
    text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the "
        + "industry's standard dummy text ever since the 1500s, when an unknown printer took a galley"
        + " of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap "
        + "into electronic typesetting, remaining essentially unchanged.";

    i = 0;
    list = SMSHelper.createStringPdus(null, new PhoneNumber(phoneNumber), text, 2, SMSMessageClass.NONE, -1, false);
    for (String pduStr : list) {
      int length = SMSHelper.getPduSize(pduStr);
      String pduString;

      if (0 == i) {
        pduString = "0051000B915121551532F40000FFA0050003020301986F79B90D4AC2E7F536283D07CDD36D383B0F22D"
            + "7DBED3C885EC6D3416F33888E2E83E0F2B49B9E769F4161371944CFC3CBF3329D9E769F416937"
            + "B93EA7CBF32E10F32D2FB74149F8BCDE06A1C37390B85C7683E8E83228ED26D7E77479FE3407C"
            + "DE96137392C2783C8F5763B0FA297F17450D95E9683E669F7B80CA2A3CBA0580D069BB340";

        assertEquals(154, length);
        assertEquals(pduString, pduStr);
      }

      if (1 == i) {
        pduString = "0051000B915121551532F40000FFA0050003020302EEE8B21B147683EAEEB5FB7D7783E0F2B49B5E9683"
            + "E8EFF71A14069DC36C76390F7A9B41F43CBC0C0ABBC9A0F9581E6E8BD96532284D07D1DFA07678"
            + "5D068541F43CBC0C9AC3CBE374BBEC0689DFEFB50B94A483D0E139685E97DBD3F63219E47ED341"
            + "6F373B0F32A7ED65D0B8ECA6D7E5E9F29C0512D7E9A0307BFE06D1D16510BB1C8683D2";

        assertEquals(154, length);
        assertEquals(pduString, pduStr);
      }

      if (2 == i) {
        pduString = "0051000B915121551532F40000FF43050003020303DCF437A8CC2E8FE9F2B73B3D06D1F3F0F2BC4CA7A7"
            + "DD6716485E6E87D3EEB4FB0C2ACFE765373D1D66B3F3A0BA7B8C0EBBCF65B20B";

        assertEquals(73, length);
        assertEquals(pduString, pduStr);
      }
      i++;
    }
    assertEquals(3, i);
  }

  @Test
  public void test7bitConcatenatedSMSParts() {
    // Text length of each message is equal for max message length for concatenated message for 7bit = 153 (7-bit) chars
    String text = "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHel"
        + "ByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeByeBye";

    List<String> pdus = SMSHelper.createStringPdus(null, new PhoneNumber("+15125551234"), text, 1, SMSMessageClass.NONE, -1, false);
    assertEquals(2, pdus.size());

    // The last text length is greater than max message length for concatenated message for 7bit = 153 (7-bit) chars
    text += "Bye";
    pdus = SMSHelper.createStringPdus(null, new PhoneNumber("+15125551234"), text, 1, SMSMessageClass.NONE, -1, false);
    assertEquals(3, pdus.size());
  }

  @Test
  public void testUCS2ConcatenatedSMSParts() {
    // Text length of each message is equal for max message length for concatenated message for UCS2 = 67 (16-bit) chars
    String text = "ПриветПриветПриветПриветПриветПриветПриветПриветПриветПриветПриветП"
        + "ПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПокаПок";

    List<String> pdus = SMSHelper.createStringPdus(null, new PhoneNumber("+15125551234"), text, 1, SMSMessageClass.NONE, -1, false);
    assertEquals(2, pdus.size());

    // The last text length is greater than max message length for concatenated message for UCS2 = 67 (16-bit) chars
    text += "a";
    SMSHelper.createStringPdus(null, new PhoneNumber("+15125551234"), text, 1, SMSMessageClass.NONE, -1, false);
    assertEquals(2, pdus.size());
  }

}
