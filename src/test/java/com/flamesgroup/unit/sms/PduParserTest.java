/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import static org.junit.Assert.assertNull;

import org.ajwcc.pduUtils.gsm3040.Pdu;
import org.ajwcc.pduUtils.gsm3040.PduParser;
import org.junit.Test;

public class PduParserTest {

  @Test
  public void testPduWithoutSMSCNumber() {
    String pduString = "01800006D04DEA140008111010000000007E0420043004370432043B043504470435043D0438044F0020043200200412043004480435043C002004420435043B04350444043E043D043500200432002004400430043704"
        + "340435043B04350020004D006900720020004D00540053002004320020043C0435043D044E002004420435043B04350444043E043D0430002E";

    PduParser pduParser = new PduParser();
    Pdu pdu = pduParser.parsePdu(pduString);

    assertNull(pdu.getSmscAddress());
  }

}
