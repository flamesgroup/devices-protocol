/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sms;

import static org.junit.Assert.assertEquals;

import org.ajwcc.pduUtils.gsm3040.Pdu;
import org.ajwcc.pduUtils.gsm3040.PduParser;
import org.ajwcc.pduUtils.gsm3040.SmsDeliveryPdu;
import org.junit.BeforeClass;
import org.junit.Test;

public class SMSClassTest {

  private static PduParser parser;

  @BeforeClass
  public static void init() {
    parser = new PduParser();
  }

  @Test
  public void testClass3() throws SMSException {
    Pdu pdu = parser.parsePdu(
        "079112623699024000038155F500136120322104250047D2F2181D969FCBA0B2D95C1ED3EBE57208347CBBE77536BDAC07D9DF747919E47ED7EDE5701D347FB3C965D0591EA6D7D3F472BBECA683C275D0A856A38D00");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSMessageClass.CLASS3_TE, sms.getMessageClass());
  }

  @Test
  public void testClass2() throws SMSException {
    Pdu pdu = parser.parsePdu(
        "079112623699024000038155F500126120322104250047D2F2181D969FCBA0B2D95C1ED3EBE57208347CBBE77536BDAC07D9DF747919E47ED7EDE5701D347FB3C965D0591EA6D7D3F472BBECA683C275D0A856A38D00");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSMessageClass.CLASS2_SIM, sms.getMessageClass());
  }

  @Test
  public void testClass1() throws SMSException {
    Pdu pdu = parser.parsePdu(
        "079112623699024000038155F500116120322104250047D2F2181D969FCBA0B2D95C1ED3EBE57208347CBBE77536BDAC07D9DF747919E47ED7EDE5701D347FB3C965D0591EA6D7D3F472BBECA683C275D0A856A38D00");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSMessageClass.CLASS1_ME, sms.getMessageClass());
  }

  @Test
  public void testClass0() throws SMSException {
    Pdu pdu = parser.parsePdu(
        "079112623699024000038155F500106120322104250047D2F2181D969FCBA0B2D95C1ED3EBE57208347CBBE77536BDAC07D9DF747919E47ED7EDE5701D347FB3C965D0591EA6D7D3F472BBECA683C275D0A856A38D00");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSMessageClass.CLASS0_FLASH, sms.getMessageClass());
  }

  @Test
  public void testClassNone() throws SMSException {
    Pdu pdu = parser.parsePdu(
        "079112623699024000038155F500006120322104250047D2F2181D969FCBA0B2D95C1ED3EBE57208347CBBE77536BDAC07D9DF747919E47ED7EDE5701D347FB3C965D0591EA6D7D3F472BBECA683C275D0A856A38D00");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSMessageClass.NONE, sms.getMessageClass());
  }

  @Test
  public void testNotPresentClass() throws SMSException {
    Pdu pdu = parser.parsePdu(
        "079112623699024000038155F500036120322104250047D2F2181D969FCBA0B2D95C1ED3EBE57208347CBBE77536BDAC07D9DF747919E47ED7EDE5701D347FB3C965D0591EA6D7D3F472BBECA683C275D0A856A38D00");
    SMSMessageInbound sms = SMSFactory.createInboundSMSFromPdu((SmsDeliveryPdu) pdu);

    assertEquals(SMSMessageClass.NONE, sms.getMessageClass());
  }

}
