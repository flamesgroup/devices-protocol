/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit.sc;

import com.flamesgroup.device.FakeSCReaderChannel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class SIMCardUnitTest {

  // 100 ms is magical delay which used to simulate a situation
  // when we get APDU response at once after writing APDU command
  private static final long DELAY_ON_WRITE = 100;

  private static class FakeSCReaderChannelWithDelayedOnWrite extends FakeSCReaderChannel {

    public FakeSCReaderChannelWithDelayedOnWrite(final List<FakeSCReaderChannel.Iteration> iterations) {
      super(iterations);
    }

    @Override
    public void writeAPDUCommand(final APDUCommand command) throws ChannelException, InterruptedException {
      super.writeAPDUCommand(command);
      Thread.sleep(DELAY_ON_WRITE);
    }

  }

  @Test
  public void testLightningFastApduCommandResponse() throws Exception {
    final List<FakeSCReaderChannel.Iteration> iterations = new LinkedList<>();

    // Read ICCID
    // Select MF directory
    iterations.add(new FakeSCReaderChannel.Iteration(
        new FakeSCReaderChannel.SendIterationData(
            new APDUCommand(APDUCommand.InstructionClass.GSM_APPLICATION, APDUCommand.Instruction.SELECT, (byte) 0x00, (byte) 0x00, (byte) 0x02, new byte[] {(byte) 0x3F, (byte) 0x00})),
        0,
        new FakeSCReaderChannel.ReceiveIterationData(
            new APDUResponse(new byte[] {}, (byte) 0x9F, (byte) 0x1B))));
    // Get response - MF directory description
    iterations.add(new FakeSCReaderChannel.Iteration(
        new FakeSCReaderChannel.SendIterationData(
            new APDUCommand(APDUCommand.InstructionClass.GSM_APPLICATION, APDUCommand.Instruction.GET_RESPONSE, (byte) 0x00, (byte) 0x00, (byte) 0x1B, new byte[] {})),
        0,
        new FakeSCReaderChannel.ReceiveIterationData(
            new APDUResponse(
                new byte[] {(byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0xD2, (byte) 0x3F, (byte) 0x00, (byte) 0x01, (byte) 0xFF, (byte) 0xFF, (byte) 0xAA, (byte) 0xFF, (byte) 0x01, (byte) 0x0E,
                    (byte) 0xB7, (byte) 0x03, (byte) 0x0B, (byte) 0x05, (byte) 0x00, (byte) 0x83, (byte) 0x8A, (byte) 0x83, (byte) 0x8A, (byte) 0x00, (byte) 0x03, (byte) 0x83, (byte) 0x83,
                    (byte) 0xFF},
                (byte) 0x90, (byte) 0x00))));
    // Select ICCID file
    iterations.add(new FakeSCReaderChannel.Iteration(
        new FakeSCReaderChannel.SendIterationData(
            new APDUCommand(APDUCommand.InstructionClass.GSM_APPLICATION, APDUCommand.Instruction.SELECT, (byte) 0x00, (byte) 0x00, (byte) 0x02, new byte[] {(byte) 0x2F, (byte) 0xE2})),
        0,
        new FakeSCReaderChannel.ReceiveIterationData(
            new APDUResponse(new byte[] {}, (byte) 0x90, (byte) 0x0F))));
    //  Get response - ICCID file description
    iterations.add(new FakeSCReaderChannel.Iteration(
        new FakeSCReaderChannel.SendIterationData(
            new APDUCommand(APDUCommand.InstructionClass.GSM_APPLICATION, APDUCommand.Instruction.GET_RESPONSE, (byte) 0x00, (byte) 0x00, (byte) 0x0F, new byte[] {})),
        0,
        new FakeSCReaderChannel.ReceiveIterationData(
            new APDUResponse(
                new byte[] {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x0A, (byte) 0x2F, (byte) 0xE2, (byte) 0x04, (byte) 0x00, (byte) 0x05, (byte) 0xFF, (byte) 0x55, (byte) 0x01, (byte) 0x02,
                    (byte) 0x00, 0x00},
                (byte) 0x90, (byte) 0x00))));
    // Read binary (read ICCID file contents)
    iterations.add(new FakeSCReaderChannel.Iteration(
        new FakeSCReaderChannel.SendIterationData(
            new APDUCommand(APDUCommand.InstructionClass.GSM_APPLICATION, APDUCommand.Instruction.READ_BINARY, (byte) 0x00, (byte) 0x00, (byte) 0x0A, new byte[] {})),
        0,
        new FakeSCReaderChannel.ReceiveIterationData(
            new APDUResponse(new byte[] {(byte) 0x98, (byte) 0x83, (byte) 0x00, (byte) 0x61, (byte) 0x08, (byte) 0x04, (byte) 0x67, (byte) 0x55, (byte) 0x03, (byte) 0xF8}, (byte) 0x90,
                (byte) 0x00))));

    FakeSCReaderChannel fakeSCReaderChannel = new FakeSCReaderChannelWithDelayedOnWrite(iterations);
    SIMCardUnit simCardUnit = new SIMCardUnit(fakeSCReaderChannel);

    simCardUnit.turnOn();
    simCardUnit.readICCID();
    simCardUnit.turnOff();

    fakeSCReaderChannel.checkForException();
  }

}
