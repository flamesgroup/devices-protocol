/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class PhoneNumberIncorrectParametersTest {

  private final String number;

  public PhoneNumberIncorrectParametersTest(final String number) {
    this.number = number;
  }

  @Parameters
  public static Collection<Object[]> phoneNumberValues() {
    return Arrays.asList(new Object[][] {{"!@%^&?()[]`~"}, {"+ 380977211213"}, {"+A380977211213"}, {"80977b11213"}});
  }

  @Test(expected = IllegalArgumentException.class)
  public void testPhoneNumber() {
    new PhoneNumber(number);
  }

}
