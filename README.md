# devices-protocol

Low-level java library to communicate with ANTRAX hardware.

## Overview

**devices-protocol** implements all necessary interfaces to communicate with all ANTRAX hardware.

Currently ANTRAX hardware could be split in two main groups by communication mechanism:
* Ethernet (GSMB, GSMB2, GSMB3 and SIMB)
* SPI (GSMBOX4, GSMBOX8, SIMBOX60 and SIMBOX120)

To exchange data with all ANTRAX hardware used MUDP protocol, which implemented over Ethernet and SPI.
To configure GSMB, GSMB2, GSMB3 and SIMB ANTRAX hardware CUDP protocol is used, which is implemented only over Ethernet and uses UDP broadcast.
To configure GSMBOX4, GSMBOX8, SIMBOX60 and SIMBOX120 ANTRAX hardware uses only GPIO.

On top of MUDP protocol different logical channels are implemented:
* _common_
  * service - used to execute service operation with each CPU entry
  * logger - used to get runtime logging and read journal from each CPU entry
  * indication (only for GSMBOX4, GSMBOX8, SIMBOX60 and SIMBOX120) - used to provide necessary indication on GSM or SIM entry 
* _GSM_
  * gsm - used to communicate with GSM module by AT commands and execute some service operation
  * audio - used to exchange audio data with GSM module (directly or through DAC)
  * scemulator (only for GSMB and GSMB2) - used to emulate SIM Card for old GSM modules
* _SIM_
  * screader - used to read SIM Card
  * scmonitor (only for SIMBOX60 and SIMBOX120) - used to monitor after SIM Holder state

On top of logical channels two type of units are implemented:
* Mobile Station unit
* SIM Card unit

This is a high level interfaces used by ANTRAX software.

Also FTB device type exists in ANTRAX environment. It is internal device used to firmware and test GSMB3 and SIMB devices. 

## Structure

**devices-protocol** uses standard [Maven](https://maven.apache.org) project layout.

Under `org.ajwcc.pduUtils` namespace source codes of PduUtils Library licensed under Apache License, Version 2.0 are located.
It was added in source tree, because PduUtils Library does not have proper Maven artifact.

## Build

**devices-protocol** could be compiled, installed and deployed with regular [Maven commands](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html).

## Usage examples

Usage examples of channel API could be found in `com.flamesgroup.device.tool` namespace, where basic device management tools are implemented. 

Usage examples of unit API could be found in [antrax](https://gitlab.com/flamesgroup/antrax.git) project.

## Questions

If you have any questions about **devices-protocol**, feel free to create issue with it.
